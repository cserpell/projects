class dadoj
{
	static public boolean cambiar(int[][] tab, int n, int[] puntox, int[] puntoy)
	{
		if(n >= 13)
			return true;
		if(!cambiar(tab, n+1, puntox, puntoy))
			return false;
		if(tab[puntox[n]][puntoy[n]] == 5)
		{
			tab[puntox[n]][puntoy[n]] = 1;
			return true;
		}
		tab[puntox[n]][puntoy[n]]++;
		return false;
	}
	static public boolean condicion(int[][] t, int[] x, int[] y)
	{
		if(t[x[4]][y[4]] == t[x[5]][y[5]] ||
		   t[x[4]][y[4]] == t[x[6]][y[6]] ||
		   t[x[4]][y[4]] == t[x[7]][y[7]] ||
		   t[x[4]][y[4]] == t[x[8]][y[8]] ||
		   t[x[5]][y[5]] == t[x[6]][y[6]] ||
		   t[x[5]][y[5]] == t[x[7]][y[7]] ||
		   t[x[5]][y[5]] == t[x[8]][y[8]] ||
		   t[x[6]][y[6]] == t[x[7]][y[7]] ||
		   t[x[6]][y[6]] == t[x[8]][y[8]] ||
		   t[x[7]][y[7]] == t[x[8]][y[8]])
			return false;
		if(t[x[0]][y[0]] == t[x[2]][y[2]] ||
		   t[x[0]][y[0]] == t[x[6]][y[6]] ||
		   t[x[0]][y[0]] == t[x[10]][y[10]] ||
		   t[x[0]][y[0]] == t[x[12]][y[12]] ||
		   t[x[2]][y[2]] == t[x[6]][y[6]] ||
		   t[x[2]][y[2]] == t[x[10]][y[10]] ||
		   t[x[2]][y[2]] == t[x[12]][y[12]] ||
		   t[x[6]][y[6]] == t[x[10]][y[10]] ||
		   t[x[6]][y[6]] == t[x[12]][y[12]] ||
		   t[x[10]][y[10]] == t[x[12]][y[12]])
			return false;
		if(t[x[8]][y[8]] == t[x[11]][y[11]] ||
		   t[x[8]][y[8]] == t[x[12]][y[12]] ||
		   t[x[12]][y[12]] == t[x[11]][y[11]])
			return false;
		if(t[x[4]][y[4]] == t[x[9]][y[9]] ||
		   t[x[4]][y[4]] == t[x[12]][y[12]] ||
		   t[x[12]][y[12]] == t[x[9]][y[9]])
			return false;
		if(t[x[3]][y[3]] == t[x[7]][y[7]] ||
		   t[x[3]][y[3]] == t[x[11]][y[11]] ||
		   t[x[11]][y[11]] == t[x[7]][y[7]])
			return false;
		if(t[x[0]][y[0]] == t[x[1]][y[1]] ||
		   t[x[0]][y[0]] == t[x[4]][y[4]] ||
		   t[x[4]][y[4]] == t[x[1]][y[1]])
			return false;
		if(t[x[3]][y[3]] == t[x[6]][y[6]] ||
		   t[x[3]][y[3]] == t[x[9]][y[9]] ||
		   t[x[9]][y[9]] == t[x[6]][y[6]])
			return false;
		if(t[x[6]][y[6]] == t[x[1]][y[1]] ||
		   t[x[6]][y[6]] == t[x[11]][y[11]] ||
		   t[x[11]][y[11]] == t[x[1]][y[1]])
			return false;
		if(t[x[0]][y[0]] == t[x[3]][y[3]] ||
		   t[x[0]][y[0]] == t[x[8]][y[8]] ||
		   t[x[3]][y[3]] == t[x[8]][y[8]])
			return false;
		if(t[x[2]][y[2]] == t[x[1]][y[1]] ||
		   t[x[2]][y[2]] == t[x[3]][y[3]] ||
		   t[x[3]][y[3]] == t[x[1]][y[1]])
			return false;
		if(t[x[5]][y[5]] == t[x[1]][y[1]] ||
		   t[x[5]][y[5]] == t[x[9]][y[9]] ||
		   t[x[9]][y[9]] == t[x[1]][y[1]])
			return false;
		return true;
	}
	static public void main(String[] args)
	{
		int[][] tab = new int[5][5];
		int[] puntox = new int[13];
		int[] puntoy = new int[13];
		int n = 0;
		
		puntox[0] = 2;
		puntoy[0] = 0;
		puntox[1] = 1;
		puntoy[1] = 1;
		puntox[2] = 2;
		puntoy[2] = 1;
		puntox[3] = 3;
		puntoy[3] = 1;
		puntox[4] = 0;
		puntoy[4] = 2;
		puntox[5] = 1;
		puntoy[5] = 2;
		puntox[6] = 2;
		puntoy[6] = 2;
		puntox[7] = 3;
		puntoy[7] = 2;
		puntox[8] = 4;
		puntoy[8] = 2;
		puntox[9] = 1;
		puntoy[9] = 3;
		puntox[10] = 2;
		puntoy[10] = 3;
		puntox[11] = 3;
		puntoy[11] = 3;
		puntox[12] = 2;
		puntoy[12] = 4;
		
		for(int i = 0; i < 13; i++)
			tab[puntox[i]][puntoy[i]] = 1;
		for(int i = 0; i < 1220703125; i++)
		{
			if(condicion(tab, puntox, puntoy))
			{
				for(int j = 0; j < 13; j++)
					System.out.print(tab[puntox[j]][puntoy[j]] + ", ");
				System.out.println("");
				n++;
			}
			if(cambiar(tab, 0, puntox, puntoy))
			{
				System.out.println("SALE");
				break;
			}
		}
		System.out.println("Encontrados: " + n);
	}
}
