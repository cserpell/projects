#include <iostream>
#include <stdlib.h>

using namespace std;

void parray(char * arr, int len) {
  cout << "./test1.sh ";
  for(int a = 0; a < len; a++) {
    cout << arr[a];
  }
  cout << " Hc2lTtgU";
  cout << endl;
}

void trynext(char * arr, int pos, int MAXLEN) {
  if(pos > 0)
    parray(arr, pos);
  if(pos == MAXLEN) {
    // new to try
 //   parray(arr, MAXLEN);
    return;
  }
  if(pos == 1) {
    cerr << "Siguiente " << arr[0] << endl;
  }
  for(int i = 0; i <= 9; i++) {
    arr[pos] = i + '0';
    trynext(arr, pos + 1, MAXLEN);
  }
  for(int i = 0; i < 26; i++) {
    arr[pos] = i + 'a';
    trynext(arr, pos + 1, MAXLEN);
  }
  for(int i = 0; i < 26; i++) {
    arr[pos] = i + 'A';
    trynext(arr, pos + 1, MAXLEN);
  }
}

int main() {
  int pos = 0;
  char * arr = (char *)malloc(sizeof(char)*9);
  int MAXLEN = 6;
  trynext(arr, 0, MAXLEN);
  return 0;
}
