//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#define MAXDIRS (32)
#define MAXEXTS (16)
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TEdit *NombreOrigen;
        TButton *Buscar;
        TMemo *Resultado;
        TMemo *Directorios;
        TMemo *Extensiones;
        TButton *CargarDirExt;
        TCheckBox *ResArchivo;
        TEdit *NombreOrigenDirExt;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TEdit *NombreDestino;
        TLabel *Label4;
        TStatusBar *StatusBar;
        void __fastcall BuscarClick(TObject *Sender);
        void __fastcall CargarDirExtClick(TObject *Sender);
        void __fastcall ResArchivoClick(TObject *Sender);
private:	// User declarations
  int pos; // N�mero total de nombres escritos en la lista
  int NumDirs;
  int NumExts;
  string Dir[MAXDIRS];
  string Ext[MAXEXTS];
  bool cargados;
  int LeeArchivo(string NombreArchivo, string *Lista, bool prim);
  bool LeerDirExt(void);
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
