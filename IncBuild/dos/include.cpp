
#pragma hdrstop
#include <fstream.h>
#include <stdio.h>
#include <string.h>
//#include <condefs.h>
//---------------------------------------------------------------------------
#pragma argsused
#define DIR_1 ("C:\\Archiv~1\\Borland\\CBuild~1\\Include\\")
#define DIR_2 ("C:\\Archiv~1\\Borland\\CBuild~1\\Include\\Vcl\\")
#define EXT_1 (".h")
#define EXT_2 (".hpp")
int pos; // Sirve como n�mero total de nombres escritos en la lista
//---------------------------------------------------------------------------
/* Lee el archivo dado y retorna la cantidad de "includes" que encontr�.
   Cuando encuentra un "include" Agrega el nombre de archivo a la lista
   y llama nuevamente a esta rutina con el nuevo archivo. */
int LeeArchivo(string NombreArchivo, string *Lista, bool prim)
{
  ifstream Archivo;
  string temp;
  int num;

  if(!prim)
  {
    temp = DIR_1 + NombreArchivo;
    Archivo.open(temp.c_str());
    if(!Archivo)
    {
      temp = DIR_1 + NombreArchivo + EXT_1;
      Archivo.open(temp.c_str());
      if(!Archivo)
      {
        temp = DIR_1 + NombreArchivo + EXT_2;
        Archivo.open(temp.c_str());
        if(!Archivo)
        {
          temp = DIR_2 + NombreArchivo;
          Archivo.open(temp.c_str());
          if(!Archivo)
          {
            temp = DIR_2 + NombreArchivo + EXT_1;
            Archivo.open(temp.c_str());
            if(!Archivo)
            {
              temp = DIR_2 + NombreArchivo + EXT_2;
              Archivo.open(temp.c_str());
              if(!Archivo)
              {
                cout << ("No se encontro el archivo " + NombreArchivo + "\n");
                Lista[pos] = "N.E." + NombreArchivo;
                pos++;
                return 0;
              }
            }
          }
        }
      }
    }
  } else
  {
    Archivo.open(NombreArchivo.c_str());
    if(!Archivo)
      return 0;
  }
  num = 0;
  do
  {
    Archivo >> temp;
    if(temp == "#include")
    {
      string nuevo;
      bool term;
      int ct;

      Archivo >> temp;
      ct = (int)temp.length() - 2;
      nuevo.resize(ct);
      for(int i = 0; i < ct; i++)       // Copio el archivo a "nuevo"
        nuevo[i] = temp[i + 1];
      term = false;
      for(int i = 0; i < pos; i++)      // Veo si ya est� en la lista
      {
        if(nuevo == Lista[i])
          term = true;
      }
      if(!term)
      {
        num++;
        Lista[pos] = nuevo;
        pos++;
        num += LeeArchivo(nuevo, Lista, false);
      }
    }
  } while(!Archivo.eof());
  Archivo.close();
  return num;
}
//---------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  ofstream Archivo;
  string Todos[500];
  string Origen;
  int num;

  if(argc < 2)
  {
    printf("Uso:  include (ARCHIVO DE INICIO)\n");
    return 0;
  }
  pos = 0;
  Origen = argv[1];
  num = LeeArchivo(Origen, Todos, true);
  Archivo.open("inc.dat");
  if(!Archivo)
  {
    printf("Error al abrir el archivo para listar los archivos leidos.\n");
    return 0;
  }
//  Archivo << IntToStr(num);
  for(int i = 0; i < pos; i++)
  {
    Archivo << Todos[i];
    Archivo << "\n";
  }
  Archivo.close();
  return 0;
}
//---------------------------------------------------------------------------

