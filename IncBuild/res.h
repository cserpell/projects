#include <string.h>
#define MAXDIRS (32)
#define MAXEXTS (16)

class Resultado
{
 private:
  int pos; // N�mero total de nombres escritos en la lista
  int NumDirs;
  int NumExts;
  char * Dir[MAXDIRS];
  char * Ext[MAXEXTS];
 public:
  bool LeeDirsExts(char * NombreArchivo);
  int LeeArchivo(char * NombreArchivo, char * Lista[], bool prim = false);
  int Total(void);
};

