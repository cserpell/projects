//---------------------------------------------------------------------------
#include <vcl.h>
#include <fstream.h>
#include <string.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TForm1 *Form1;
// Transforma el string orig, cambi�ndole los "_" por " ".
string SacarRayas(string orig)
{
  string dest;
  int len;

  len = orig.length();
  dest.resize(len);
  for(int i = 0; i < len; i++)
  {
    dest[i] = orig[i];
    if(dest[i] == '_')
      dest[i] = ' ';
  }
  return dest;
}       /*
bool CompararLow(string s1, string s2)
{
  int l1, l2;

  l1 = s1.length();
  l2 = s2.length();
  if(l1 != l2)
    return false;
  for(int i = 0; i < l1; i++)
  {
    if(s1[i] >= 'A' && s1[i] <= 'Z')
      s1[i] += 'a' - 'A';
    if(s2[i] >= 'A' && s2[i] <= 'Z')
      s2[i] += 'a' - 'A';
    if(s1[i] != s2[i])
      return false;
  }
  return true;
}     */
string BajarStr(string orig)
{
  string dest;
  int len;

  len = orig.length();
  dest.resize(len);
  for(int i = 0; i < len; i++)
  {
    if(orig[i] >= 'A' && orig[i] <= 'Z')
      dest[i] = orig[i] + 'a' - 'A';
    else
      dest[i] = orig[i];
  }
  return dest;
}
/* Lee el archivo dado y retorna la cantidad de "includes" que encontr�.
   Cuando encuentra un "include" Agrega el nombre de archivo a la lista
   y llama nuevamente a esta rutina con el nuevo archivo. */
int TForm1::LeeArchivo(string NombreArchivo, string *Lista, bool prim)
{
  ifstream Archivo;
  string temp;
  int num;
  bool enc;

  enc = false;
  if(!prim)
  {
    for(int i = 0; i < NumDirs; i++)
    {
      temp = Dir[i] + NombreArchivo;
      Archivo.open(temp.c_str());
      if(!Archivo)
      {
        for(int j = 0; j < NumExts; j++)
        {
          temp = Dir[i] + NombreArchivo + Ext[j];
          Archivo.open(temp.c_str());
          if(Archivo)
          {
            enc = true;
            break;
          }
        }
        if(enc)
          break;
      }
      else
      {
        enc = true;
        break;
      }
    }
    if(!enc)
    {
      string nuevo;

      nuevo = "N.E. " + NombreArchivo;
      for(int i = 0; i < pos; i++)      // Veo si ya est� en la lista
        if(nuevo == Lista[i])
          return 0;

      Lista[pos] = nuevo;
      Resultado->Lines->Append(Lista[pos].c_str());
      pos++;
      return 0;
    }
  } else
  {
    pos = 0;
    Archivo.open(NombreArchivo.c_str());
    if(!Archivo)
    {
      Application->MessageBoxA("Error al abrir el archivo especificado.",
                                                                    "Error", 0);
      return 0;
    }
  }

  AnsiString str;

  str = "Leyendo el archivo ";
  str += NombreArchivo.c_str();
  str += ".";
  StatusBar->Panels->Items[0]->Text = str;
  Repaint();

  Lista[pos] = NombreArchivo;
  Resultado->Lines->Append(Lista[pos].c_str());
  pos++;
  num = 0;

  Application->ProcessMessages();
  do
  {
    Archivo >> temp;
    if(temp == "#include")
    {
      string nuevo;
      bool term;
      int ct;
      int i;

      Archivo >> temp;
      ct = temp.length() - 2;
      nuevo.resize(ct);
      nuevo = temp.substr(1, ct);   // Copio el archivo a "nuevo"
/*      for(i = 0; i < ct; i++)
        nuevo[i] = temp[i + 1];*/
      term = false;
      nuevo = BajarStr(nuevo);
      for(i = 0; i < pos; i++)      // Veo si ya est� en la lista
        if(nuevo == Lista[i])
          term = true;
      if(!term)
      {
        int ret;

        num++;
        ret = LeeArchivo(nuevo, Lista, false);
        if(ret)
          num += ret;
      }
    }
  } while(!Archivo.eof());
  Archivo.close();
  return num;
}

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
  pos = 0;
  NumExts = 0;
  NumDirs = 0;
  cargados = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BuscarClick(TObject *Sender)
{
  ofstream Archivo;
  string Todos[512];
  string Origen;
  int num;

  if(!cargados)
  {
    CargarDirExtClick(Sender);
    if(!cargados)
      return;
  }
  if(!LeerDirExt())
    return;

  Origen = NombreOrigen->Text.c_str();
  num = LeeArchivo(Origen, Todos, true);
  if(ResArchivo->Checked)
  {
    Archivo.open(NombreDestino->Text.c_str());
    if(!Archivo)
    {
      Application->MessageBoxA(
         "Error al abrir el archivo para escribir los resultados.", "Error", 0);
      return;
    }

    Archivo << IntToStr(num).c_str();
    Archivo << "\n";
    for(int i = 0; i < pos; i++)
    {
      Archivo << Todos[i];
      Archivo << "\n";
    }
    Archivo.close();
  }
  Application->MessageBoxA("Finaliz� la b�squeda de archivos.", "Fin", 0);
}
//---------------------------------------------------------------------------
bool TForm1::LeerDirExt(void)
{
  if(Directorios->Lines->Count >= MAXDIRS)
  {
    AnsiString str;

    str = "No se pueden agregar m�s de " + IntToStr(MAXDIRS) + " directorios.";
    Application->MessageBoxA(str.c_str(), "M�ximo", 0);
    return false;
  }
  for(NumDirs = 0; NumDirs < Directorios->Lines->Count; NumDirs++)
  {
    Dir[NumDirs] = (void *)Directorios->Lines->Objects[NumDirs];
  }
  if(Extensiones->Lines->Count >= MAXEXTS)
  {
    AnsiString str;

    str = "No se pueden agregar m�s de " + IntToStr(MAXDIRS) + " extesiones.";
    Application->MessageBoxA(str.c_str(), "M�ximo", 0);
    return false;
  }
  for(NumExts = 0; NumDirs < Extensiones->Lines->Count; NumExts++)
  {
    Ext[NumExts] = Directorios->Lines->Objects[NumExts];
  }
  return true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::CargarDirExtClick(TObject *Sender)
{
  ifstream Archivo;
  string temp;

  Archivo.open(NombreOrigenDirExt->Text.c_str());
  if(!Archivo)
  {
    Application->MessageBoxA(
   "�Error al abrir el archivo con los directorios y extensiones!", "Error", 0);
    return;
  }

  cargados = true;
  // Leo los directorios
  do
  {
    if(Archivo.eof())
      return;
    Archivo >> temp;
    if(!temp.length())
      continue;
    if(temp[0] != ';')
    {
      Directorios->Lines->Append(SacarRayas(temp).c_str());
      continue;
    }
    if(temp.length() == 1)
      continue;
    if(temp[0] == ';' && temp[1] == ';')
      break;
  } while(1);

  // Leo las extensiones
  do
  {
    if(Archivo.eof())
      return;
    Archivo >> temp;
    if(!temp.length())
      continue;
    if(temp[0] != ';')
      Extensiones->Lines->Append(SacarRayas(temp).c_str());
  } while(1);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ResArchivoClick(TObject *Sender)
{
  if(ResArchivo->Checked)
    NombreDestino->Enabled = true;
  else
    NombreDestino->Enabled = false;
}
//---------------------------------------------------------------------------
