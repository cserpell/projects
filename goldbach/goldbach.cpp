#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

class primeTable {
    char *primes;
  public:
    primeTable(int TAM) {
        int i;
        
        primes = new char[TAM];
        for(i = 0; i < TAM; i++)
            primes[i] = 2;
        primes[0] = 1;
    }
    
    ~primeTable() {
        delete [] primes;
    }
    // Suponiendo n > 2 impar y los primos anteriores ya calculados
    int isPrime(int n) {
        int k, d, sq, m, r;
    
        k = (n - 3)/2;
        if(primes[k] != 2)
            return primes[k];
        sq = (int)sqrt((float)n);
        d = 3;
        m = 0;
        while(d <= sq) {
            // if(isPrime(d)) { // Descomentar esta linea si se quiere hacer recursivo
            if(primes[m]) {
                r = n/d;
                if(r*d == n) {
                    primes[k] = 0;
                    return 0;
                }
            }
            m++;
            d += 2;
        }
        primes[k] = 1;
        return 1;
    }
};

int main(int argc, char *argv[]) {
    int i, j, lim, k;
    primeTable *P;
    
    if(argc < 2) {
        cout << "Uso: " << argv[0] << " limite" << endl;
        return 0;
    }
    istringstream strtmp(argv[1]);
    strtmp >> lim;
    if(lim % 2)
        lim += 1;
    for(i = 6; i <= lim; i += 2)
        cout << i << " ";
    cout << endl;
    P = new primeTable(lim/2);
    for(j = 3; j < lim - 1; j += 2) {
        for(k = 0; k < (j - 3)/2; k++)
            cout << "  ";
        if(P->isPrime(j))
            for(i = 3; i < lim - j + 1; i += 2)
                cout << P->isPrime(i) << " ";
        else
            for(i = 3; i < lim - j + 1; i += 2)
                cout << "0 ";
        cout << endl;
    }
    return 0;
}
