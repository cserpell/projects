"""Problem 204."""
import bisect
import math

import all_primes


def get_hamming_type(type_num, max_num, hts=None):
    ttuple = (type_num, max_num)
    if hts is None:
        hts = {1: 1}
    if ttuple not in hts:
        nums = max_num
        last = bisect.bisect_right(all_primes.PRIMES, max_num)
        for prime in reversed(all_primes.PRIMES[:last]):
            if prime > max_num:
                continue
            if prime <= type_num:
                break
            num_divs = max_num // prime
            num_divs = get_hamming_type(type_num, num_divs, hts=hts)
            nums -= num_divs
        print(f'Hamming type {type_num} less or equal than {max_num} are {nums}')
        hts[ttuple] = nums
    return hts[ttuple]


def get_all_factor_with_primes(primes_list, pos, current_num, max_num):
    if current_num > max_num:
        return 0
    if pos == len(primes_list):
        return 1
    base_p = primes_list[pos]
    max_p = int(math.log(max_num / current_num) / math.log(base_p))
    factors = 0
    for nnn in range(max_p + 1):
        factors += get_all_factor_with_primes(
                primes_list, pos + 1, current_num * (base_p ** nnn), max_num)
    return factors


def main():
    """Main execution point."""
    """
    get_hamming_type(5, 15)
    print('**************')
    get_hamming_type(5, 100000000)
    print('**************')
    get_hamming_type(100, 1000000000)
    """
    num = get_all_factor_with_primes([2, 3, 5], 0, 1, 15)
    print(f'For type 5, max 15, there are {num} numbers')
    num = get_all_factor_with_primes([2, 3, 5], 0, 1, 100000000)
    print(f'For type 5, max 100000000, there are {num} numbers')
    num = get_all_factor_with_primes([prime for prime in all_primes.PRIMES if prime <= 100], 0, 1, 1000000000)
    print(f'For type 100, max 1000000000, there are {num} numbers')


if __name__ == '__main__':
    main()
