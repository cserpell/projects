import decimal
import math


decimal.getcontext().prec = 100


def get_next_in_seq(remainder):
  inverse = decimal.Decimal(1.) / remainder
  intinverse = int(inverse)
  return intinverse, inverse - decimal.Decimal(intinverse)


def sumr(a, b):
  nn = a[0] * b[1] + a[1] * b[0]
  dd = a[1] * b[1]
  return nn, dd
#  gg = gcd(nn, dd)
#  return nn / gg, dd / gg


def get_seq_value(sequence):
  value = (0, 1)
  for pos in range(len(sequence) - 1, 0, -1):
    nvalue = sumr(value, (sequence[pos], 1))
    value = (nvalue[1], nvalue[0])  # 1 / value
  return sumr((sequence[0], 1), value)


def get_seq_value2(sequence, approxs):
  if len(sequence) == 1:
    return sequence[0], 1
  if len(sequence) == 2:
    return sumr((sequence[0], 1), (1, sequence[1]))
  value = (sequence[-1] * approxs[-1][0] + approxs[-2][0],
           sequence[-1] * approxs[-1][1] + approxs[-2][1])
  return value[0], value[1]
#  gg = gcd(value[0], value[1])
#  return value[0] / gg, value[1] / gg


def gcd(a, b):
  if a < b:
    return gcd(b, a)
  while a % b != 0:
    rem = a % b
    a = b
    b = rem
  return b


class P66(object):
  """Problem 66."""

  def __init__(self, stop_n=1000):
    self._stop_n = stop_n
    self._max_x = 0
    self._max_d = 0

  def get_approx(self, number_d):
    objective = decimal.Decimal(number_d) ** decimal.Decimal(0.5)
    sequence = []
    approxs = []
    reminder = decimal.Decimal(1.) / objective
    iters = 0
    while iters <= 10000:
      next_in_seq, reminder = get_next_in_seq(reminder)
      sequence.append(next_in_seq)
      value = get_seq_value2(sequence, approxs)
      approxs.append(value)
      if value[0] * value[0] - number_d * value[1] * value[1] == 1:
        # Solution found!
        gg = gcd(value[0], value[1])
        value = (value[0] / gg, value[1] / gg)
        print 'For %s, sol: x = %s, y = %s -- seq length %s' % (number_d, value[0], value[1], len(sequence))
        if value[0] > self._max_x:
          self._max_x = value[0]
          self._max_d = number_d
        return
      iters += 1
    print '*** %s max iter reached' % number_d

  def main(self):
    number_d = 1
    while number_d < self._stop_n:
      number_d += 1
      try_sq = int(math.sqrt(number_d))
      if try_sq * try_sq == number_d:
        continue
      self.get_approx(number_d)
    print 'End with max_x = %s in d = %s' % (self._max_x, self._max_d)


if __name__ == '__main__':
  P66().main()

