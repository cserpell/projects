import gmpy
import sys

# print sys.argv

if len(sys.argv) < 2 :
  sequence = ""
  print "WARNING: Missing sequence"
else :
  sequence = sys.argv[1] # Input sequence with U, D or d

seqlen = len(sequence)
c0 = 1 # component multiplying
c1 = 0 # component constant
cd = 1 # dividing component
for k in sequence : # iterate on characters
  if k == 'U' :
    c0 = 4 * c0
    c1 = 4 * c1 + 2 * cd
    cd = 3 * cd
  elif k == 'D' :
    cd = 3 * cd
  elif k == 'd' :
    c0 = 2 * c0
    c1 = 2 * c1 - cd
    cd = 3 * cd

#print "Final"
print str(c0) + " * a + " + str(c1)
print "----------------------------"
print str(cd) + " = 3 ** " + str(seqlen)

a = 0
while a < 1000000000 :
  numer = cd * a - c1
  if numer % c0 == 0 :
    print str(a) + " -> " + str((cd * a - c1) / c0)
  a = a + 1
