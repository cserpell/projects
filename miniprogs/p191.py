

SEQ_NO = {
  0: 1,
  1: 2,
  2: 4,
  3: 7,
}


def seq_no(n):
  if n in SEQ_NO:
    return SEQ_NO[n]
  newseq = seq_no(n - 1) + seq_no(n - 2) + seq_no(n - 3)
  SEQ_NO[n] = newseq
  return newseq


def seq(n):
  tot = seq_no(n)
  for i in xrange(0, n):
    tot += seq_no(n - i - 1) * seq_no(i)
  return tot


def main():
  print seq(4)
  print seq(30)


if __name__ == '__main__':
  main()

