import all_primes

NUMS = {0: [1,1,0,1,1,1,1],
        1: [0,0,0,1,0,0,1],
        2: [1,0,1,1,1,1,0],
        3: [1,0,1,1,0,1,1],
        4: [0,1,1,1,0,0,1],
        5: [1,1,1,0,0,1,1],
        6: [1,1,1,0,1,1,1],
        7: [1,1,0,1,0,0,1],
        8: [1,1,1,1,1,1,1],
        9: [1,1,1,1,0,1,1]}
TENS = [
    10000000,
    1000000,
    100000,
    10000,
    1000,
    100,
    10,
    1]


def dig_decomp(num, dict_decomp):
    if num not in dict_decomp:
        vals = []
        started = False
        for tens in TENS:
            if tens > num:
                if started:
                    vals.append(0)
                continue
            qqq = num // tens
            vals.append(qqq)
            started = True
            num = num % tens
        dict_decomp[num] = vals
    return dict_decomp[num]


def dig_sum(num, dict_sums, dict_decomp):
    if num not in dict_sums:
        vals = dig_decomp(num, dict_decomp)
        dict_sums[num] = sum(vals)
    return dict_sums[num]


def get_max(num, dict_max, dict_sums, dict_decomp):
    if num not in dict_max:
        vals = dig_decomp(num, dict_decomp)
        # print(f'Decomp: {vals}')
        cost = 2 * sum(sum(NUMS[dig]) for dig in vals)
        # print(f'Cost: {cost}')
        dsum = dig_sum(num, dict_sums, dict_decomp)
        # print(f'Digsum: {dsum}')
        if dsum != num:
            cost += get_max(dsum, dict_max, dict_sums, dict_decomp)
        # print(f'Final cost: {cost}')
        dict_max[num] = cost
    return dict_max[num]


def get_sam(num, dict_sam, dict_sums, dict_decomp, current_on=None):
    # if num not in dict_sam:
    vals = dig_decomp(num, dict_decomp)
    print(f'Decomp: {vals}')
    if current_on is None:
        current_on = [[0, 0, 0, 0, 0, 0, 0] for _ in range(8)]
    print(f'Current on: {current_on}')
    required = [NUMS[dig] for dig in vals]
    required = [[0, 0, 0, 0, 0, 0, 0] for _ in range(8 - len(vals))] + required
    print(f'Required: {required}')
    difs_p = 0
    difs_n = 0
    for dpos in range(8):
        for light in range(7):
            if required[dpos][light] == 0 and current_on[dpos][light] == 1:
                difs_n += 1
            elif required[dpos][light] == 1 and current_on[dpos][light] == 0:
                difs_p += 1
    cost = difs_n + difs_p
    print(f'Cost: {cost}')
    dsum = dig_sum(num, dict_sums, dict_decomp)
    print(f'Digsum: {dsum}')
    if dsum != num:
        cost += get_sam(dsum, dict_sam, dict_sums, dict_decomp, current_on=required)
    else:
        cost += sum(sum(required[dpos]) for dpos in range(8))
    print(f'Final cost: {cost}')
    return cost
    # dict_sam[num] = cost
    # return dict_sam[num]


def main():
    print('Getting primes...')
    primes = [num for num in all_primes.PRIMES if 10000000 <= num <= 20000000]
    dict_sam = {}
    dict_max = {}
    dict_sums = {}
    dict_decomp = {}
    print('Sam 137')
    print(get_max(137, dict_max, dict_sums, dict_decomp))
    print('Max 137')
    print(get_sam(137, dict_sam, dict_sums, dict_decomp))
    tot_dif = 0
    for prime in primes:
        max_res = get_max(prime, dict_max, dict_sums, dict_decomp)
        sam_res = get_sam(prime, dict_sam, dict_sums, dict_decomp)
        print(f'Prime {prime} sam = {max_res} max = {sam_res}')
        tot_dif += max_res - sam_res
        # break
    print(tot_dif)


if __name__ == '__main__':
    main()

