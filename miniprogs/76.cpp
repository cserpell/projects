#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

int recur(int n, int max) {
    if(n == 1)
        return 1;
    int ret = 0;
    int i = max;
    if(n < max) {
        ret++;
        i = n - 1;
    }
    for(; i >= 1; i--) {
        int dif = n - i;
        ret += recur(dif, i);
    }
    return ret;
}

int main() {
    int res = recur(100, 99);
    cout << res << endl;
	return 0;
}
