#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

class c164 {
 public:
   int * npos;
   int * sumaant;
   unsigned long long sumapos[25][10];
   int nlen;
   unsigned long long ultimameta;
   unsigned long long cpos;
   c164(int len) {
     nlen = len;
     npos = new int[len + 3];
     sumaant = new int[len + 3];
     npos[0] = 0;
     npos[1] = 0;
     npos[2] = 1;
     sumaant[0] = 0;
     sumaant[1] = 0;
     sumaant[2] = 0;
     cpos = 0;
     ultimameta = 1;
     for(int i = 0; i < len + 3; i++) {
     for(int j = 0; j < 10; j++) {
       sumapos[i][j] = 0;
     }
     }
   }
   void contar(int pos) {
     if(pos > nlen) {
//       cpos++;
       return;
     }
     if(sumapos[pos + 1][sumaant[pos + 1]] != 0) {
       cpos += sumapos[pos + 1][sumaant[pos + 1]];
       return;
     }
     if(pos == nlen) {
       unsigned long long ante = cpos;
       cpos += 9 - sumaant[pos + 1] + 1;
       sumapos[pos + 1][sumaant[pos + 1]] = cpos - ante;
       cout << pos << "," << sumaant[pos + 1] << " = " << sumapos[pos + 1][sumaant[pos + 1]] << endl;
       return;
     }
     int i = 0;
     if(pos == 1)
       i = 1;
     unsigned long long ante = cpos;
     for( ; i <= 9 - sumaant[pos + 1]; i++) {
       npos[pos + 1] = i;
       sumaant[pos + 2] = npos[pos + 1] + npos[pos];
       contar(pos + 1);
     }
     sumapos[pos + 1][sumaant[pos + 1]] = cpos - ante;
     cout << pos << "," << sumaant[pos + 1] << " = " << sumapos[pos + 1][sumaant[pos + 1]] << endl;
     if(cpos > ultimameta) {
   //    cout << cpos << endl;
       ultimameta = ultimameta*2;
     }
   }
};

int main() {
    c164 * nc = new c164(20);
    nc->contar(1);
    cout << nc->cpos << endl;
    return 0;
}
