#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

int main() {
    int cual = -1;
    int cuantas = -1;
    for(int p = 3; p < 1000; p++) {
        int sols = 0;
        std::cout << p << ":" << endl;
        for(int i = 1; i < p; i++) {
            for(int j = i; j < p - i; j++) {
                int k = p - i - j;
                if(i*i + j*j == k*k) { // es solucion
                    sols++;
                    cout << i << "," << j << "," << k << " es sol" << endl;
                }
            }
        }
        if(sols > cuantas) {
            cual = p;
            cuantas = sols;
        }
    }
    std::cout << cual << std::endl;
	return 0;
}
