#include <iostream>
#include <cmath>
#include <set>

using namespace std;

int main() {
    unsigned long long canttotal = 0;
    for(int n = 1; n < 10; n++) {
        cout << "Nivel " << n << endl;
        unsigned long long cantnivel = 0;
        unsigned long long val = 10;
        for(int j = 1; j < n; j++)
            val *= 10;
        unsigned long long val2 = 2*val;
        for(unsigned long long a = 1; a <= val2; a++) {
            unsigned long long cotainf = val/a;
            cotainf++;
            unsigned long long cotasup = val2/a;
            for(unsigned long long p = cotainf; p <= cotasup; p++) {
                unsigned long long bden = p*a - val;
                unsigned long long bnum = val*a;
                unsigned long long b = bnum/bden;
                if(bnum > bden*b)
                    continue;
                cantnivel++;
            }
        }
        cout << "TOTAL Nivel " << n << ": " << cantnivel << endl;
        canttotal += cantnivel;
    }
    cout << canttotal << endl;
    return 0;
}
