import java.util.*;
import java.io.*;
import java.math.*;
import java.lang.*;

class c154 {
    BigInteger[] facts;
    int calchasta;
    BigInteger modul;
    c154(int n) {
      facts = new BigInteger[n + 1];
      calchasta = 0;
      facts[0] = BigInteger.valueOf(1);
      modul = BigInteger.valueOf(1000000);
      modul = modul.multiply(modul); // 10^12
    }
    BigInteger calcular(int hasta) {
      for(int j = calchasta + 1; j <= hasta; j++) {
        facts[j] = facts[j - 1].multiply(BigInteger.valueOf(j));
        if((j % 1000) == 0)
          System.out.println(j);
      }
      calchasta = hasta;
      return facts[hasta];
    }
    boolean sino(int n, int i, int j, int k) {
      if(i + j + k != n)
        return false;
      BigInteger res = facts[n].divide(facts[i]).divide(facts[j]).divide(facts[k]);
      if(res.mod(modul).compareTo(BigInteger.ZERO) == 0)
        return true;
      return false;
    }
    static public void main(String[] args) {
      int n = 200000;
      if(args.length > 0)
        n = Integer.parseInt(args[0]);
      //c154 cc = new c154(n);
      //cc.calcular(n);
      //System.out.println("Fin calculo factoriales");
      BigInteger total = BigInteger.ZERO;
      BigInteger lasti0 = BigInteger.ONE;
      BigInteger modul = BigInteger.valueOf(1000000);
      modul = modul.multiply(modul);
      for(int i = 0; i <= n; i++) {
        if(i != 0)
          lasti0 = lasti0.multiply(BigInteger.valueOf(n - i + 1)).divide(BigInteger.valueOf(i));
        BigInteger last = lasti0;
        for(int j = 0; j <= n - i; j++) {
          if(j != 0) {
            last = last.multiply(BigInteger.valueOf(n - j - i + 1)).divide(BigInteger.valueOf(j));
          }
          if(last.mod(modul).compareTo(BigInteger.ZERO) == 0) {
            total = total.add(BigInteger.ONE);
          }
 //         System.out.print(last + " , ");
        }
        if((i % 100) == 0)
          System.out.println(i);
      }
      System.out.println(total);
    }
}
