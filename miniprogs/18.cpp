#include <iostream>
#include <sstream>
#include <cmath>

int *dijkstra(int **mat, int tam) {
    int llegado[tam];
    int *costo;
    int cmin[tam];
    int pred[tam];
    
    costo = new int[tam];
    for(int i = 0; i < tam; i++) {
        llegado[i] = 0;
        costo[i] = 1;
        cmin[i] = 1;
        pred[i] = -1;
    }
    llegado[0] = 1;
    costo[0] = 0;
    for(int i = 0; i < tam; i++) {
        if(mat[0][i]) {
            cmin[i] = -mat[0][i];
            pred[i] = 0;
        }
    }
    for(int i = 0; i < tam; i++) {
        int ccc = 0;
        int cual = -1;
        for(int j = 0; j < tam; j++) {
            if(!llegado[j] && cmin[j] != 1 && cmin[j] < ccc) {
                cual = j;
                ccc = cmin[j];
            }
        }
        std::cout << "sera escogido el " << cual << " con costo " << cmin[cual] << std::endl;
        if(cual != -1) {
            costo[cual] = costo[pred[cual]] + cmin[cual];
            llegado[cual] = 1;
            for(int p = 0; p < tam; p++) {
                if(!llegado[p] && mat[cual][p]) {
                    if(cmin[p] == 1 || /*costo[cual]*/ - mat[cual][p] < cmin[p]) {
                        cmin[p] = /*costo[cual]*/ - mat[cual][p];
                        pred[p] = cual;
                    }
                }
            }
        }
    }
    return costo;
}

int main() {
    int pisos;
    std::cin >> pisos;
    int cant = (pisos*(pisos + 1))/2;
    int **mat;
    mat = new int *[cant];
    for(int i = 0; i < cant; i++)
        mat[i] = new int[cant];
    for(int i = 0; i < cant; i++) {
        for(int j = 0; j < cant; j++)
            mat[i][j] = 0;
    }
    int aux;
    std::cin >> aux;
    int desde = 0;
    int hasta = 1;
    int anthasta = -1;
    int bla = 1;
    for(int i = 1; i < cant ; i++) {
        std::cin >> aux;
        if(desde != hasta)
            mat[desde][i] = aux;
        if(desde - 1 != anthasta) {
            mat[desde - 1][i] = aux;
        }
        if(desde != hasta)
            desde++;
        else {
            anthasta += bla;
            hasta += bla + 1;
            bla++;
        }
    }
    int *res = dijkstra(mat, cant);
    int max = 0;
    int cuma = -1;
    for(int i = 0; i < bla; i++) {
        if(-res[cant - i - 1] > max) {
            max = -res[cant - i - 1];
            cuma = cant - i - 1;
        }
        std::cout << res[cant - i - 1] << std::endl;
    }
    std::cout << "Max: " << max << std::endl;
 /*   for(int i = 0; i < cant; i++) {
        for(int j = 0; j < cant; j++) {
            std::cout << mat[i][j] << " ";
        }
        std::cout << std::endl;
    }*/
	return 0;
}
