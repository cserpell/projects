#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long tabla[11];
unsigned long long tablaval[11];

void llenar_tabla() {
    unsigned long long t1 = 1;
    unsigned long long t2 = 1;
    for(int i = 0; i < 11; i++) {
        tabla[i] = t1;
        tablaval[i] = t2;
        t1 *= 7;
        t2 *= 28;
    }
}

unsigned long long cantidad_total(unsigned long long has, int nivel) {
    if(has == 0)
        return 0;
    if(nivel < 0) {
        std::cout << " ERROR !! has = " << has << std::endl;
        return 0;
    }
    unsigned long long val = tabla[nivel];
    if(val > has) {
        return cantidad_total(has, nivel - 1);
    }
    unsigned long long ret = 0;
    unsigned long long mult = 1;
    while(has >= val) {
        ret += tablaval[nivel]*mult;
        has -= val;
        mult++;
    }
    return ret + mult*cantidad_total(has, nivel - 1);
}

int main() {
    llenar_tabla();
    std::cout << "1 : " << cantidad_total(1, 10) << std::endl;
    std::cout << "10 : " << cantidad_total(10, 10) << std::endl;
    std::cout << "7 : " << cantidad_total(7, 10) << std::endl;
    std::cout << "100 : " << cantidad_total(100, 10) << std::endl;
    std::cout << "1000000000 : " << cantidad_total(1000000000LL, 10) << std::endl;
    return 0;
}
