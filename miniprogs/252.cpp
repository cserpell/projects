#include <iostream>
#include <cmath>
#include <set>
#include <map>
#include <list>

class point {
 public:
  int x;
  int y;
  point(int xx, int yy) {
    x = xx;
    y = yy;
  }
};

int area_t(point *p1, point *p2, point *p3) {
  return p1->x*p2->y - p1->y*p2->x
       + p2->x*p3->y - p2->y*p3->x
       + p3->x*p1->y - p3->y*p1->x;
}

class hole {
 public:
  list<point *> * ptos;
  int cantptos;
  hole() {
    cantptos = 0;
    ptos = new list<point *>();
  }
  void add_point(point * c) {
    // look where it goes
    list<point *>::iterator it = ptos.begin();
    cantptos++;
    if(it != ptos.end()) {
      // lista no vacia
      point * last = *it;
      it++;
      while(it != ptos.end()) {
        point * nlast = *it;
        int area = area_t(last, nlast, c);
        if(area < 0) {
          // va aqui !!
          ptos.insert(it, c);
          return;
        }
        if(area == 0) {
          // colineales, ver si esta entremedio
          int clx = c->x - last->x;
          int cly = c->y - last->y;
          int cnx = c->x - nlast->x;
          int cny = c->y - nlast->y;
          if(clx*ccnx + cly*cny <= 0) {
            // entremedio, aqui va !
            // si es 0 es porque es igual al ultimo asi que da lo mismo
            // agregarlo ahi
            // (a menos que sea igual al primero, esperemos que eso no
            // suceda nunca)
            ptos.insert(it, c);
            return;
          }
        }
        last = nlast;
        it++;
      }
      // se agregara al final
    }
    ptos.insert(it, c);
  }
  bool is_inside(point * c) {
    // look where it goes
    list<point *>::iterator it = ptos.begin();
    point * a;
    point * b;
    point * firs;
    if(it != ptos.end()) {
      // lista no vacia
      firs = *it;
      a = firs;
      it++;
      cantptos++;
      while(it != ptos.end()) {
        b = *it;
        int area = area_t(a, b, c);
        if(area < 0) {
          // va afuera !!
          return false;
        }
        a = b;
        it++;
      }
      // se compara con el final
      b = firs;
      if(area_t(a, b, c) < 0) {
        // va afuera !!
        return false;
      }
    }
    return true;
  }
};

int x[4];
int y[4];

void llenar_xy() {
  x[0] = 1;
  x[1] = 4;
  x[2] = 16;
  x[3] = 64;
  y[0] = 0;
  y[1] = 256;
  y[2] = 256*256;
  y[3] = 256*256*256;
}

map<unsigned int, tab *> tableros;

class tab {
 public:
  unsigned int color;
  unsigned long long suma_checksums;
  int px;
  int py;
  int dist;
  int orden;
  int maneras;
  tab(int d, int orden) {
    dist = d;
    orden = o;
    maneras = 0;
    suma_checksums = 0;
  }
  void agregar_manera(unsigned long long check) {
    suma_checksums += check;
    maneras++;
  }
  bool posible_arr() {
    return py > 0;
  }
  unsigned int cambiar(int p_act, int p_dest);
    unsigned int c_aux = color & (p_dest * 3);
    unsigned int res = color - c_aux;
    c_aux = c_aux >> p_dest;
    c_aux = c_aux << p_act;
    res = res | c_aux;
    return res;
  }
  unsigned int color_arr() {
    return cambiar(y[py]*x[px], y[py - 1]*x[px]);
  }
  bool posible_izq() {
    return px > 0;
  }
  unsigned int color_izq() {
    return cambiar(y[py]*x[px], y[py]*x[px - 1]);
  }
  bool posible_aba() {
    return py < 3;
  }
  unsigned int color_aba() {
    return cambiar(y[py]*x[px], y[py + 1]*x[px]);
  }
  bool posible_der() {
    return px < 3;
  }
  unsigned int color_der() {
    return cambiar(y[py]*x[px], y[py]*x[px + 1]);
  }
  bool operator<(const tab & otro) {
  //  if(dist == otro.dist)
      return color < otro.color;
  //  return dist < otro.dist;
  }
  bool operator>(const tab & otro) {
  //  if(dist == otro.dist)
      return color > otro.color;
  //  return dist > otro.dist;
  }
  bool operator==(const tab & otro) {
    return color == otro.color;
  }
};

int main() {
    enteros primos;
    for(unsigned long long j = 3; j < 1000000; j += 2) {
        isPrime(primos, j);
    }
    primos.insert(2);
    
    for(int i = 0; i < 10000000; i++) {
        dist[i] = 0;
    }
    unsigned long long val = 1;
    unsigned long long lval = 1;
/*    unsigned long long cant = 1;
    enteros::iterator it = primos.begin();
    while(cant < 100) {
        lval = val;
        val *= *it;
        cant *= 2;
        it++;
    }
    cout << val << " -> " << cant << endl;
*/
lval = 1;
val = 1000000;

    unsigned long long minval = 0;
    unsigned long long cual;
    for(unsigned long long i = lval; i < val; i++) {
        unsigned long long count;
        //count = distintos(primos, i);
        count = 0;
        for(unsigned long long b = 1; b <= i; b++) {
            unsigned long long x = i + b;
            unsigned long long y = (i*x)/b;
            if(i*(x + y) == x*y) {
    //            cout << i << " -> x: " << x << " y: " << y << endl;
                count++;
            }
        }
        if(count > minval) {
            minval = count;
            cout << i << " " << count << endl;// << (2*count - 1) << endl;
            cual = i;
        }
        if(minval > 1000)
            break;
//        cout << i << " " << count << endl;// << (2*count - 1) << endl;
    }
    cout << cual << " -> " << minval << endl;
	return 0;
}
