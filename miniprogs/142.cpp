#include <iostream>
#include <cmath>
#include <set>

using namespace std;

set<unsigned long long> cuads;

void llenar(unsigned long long hasta) {
    for(unsigned long long i = 1; i <= hasta; i++) {
        cuads.insert(i*i);
    }
}

int verificar(unsigned long long x, unsigned long long y, unsigned long long z) {
    if(cuads.find(x + y) == cuads.end())
        return 0;
    if(cuads.find(x - y) == cuads.end())
        return 0;
    if(cuads.find(x + z) == cuads.end())
        return 0;
/*    if(cuads.find(x - z) == cuads.end())
        return 0;
  */  if(cuads.find(y + z) == cuads.end())
        return 0;
/*    if(cuads.find(y - z) == cuads.end())
        return 0;
  */  return 1;
}

int main() {
    llenar(1000);
    unsigned long long menorsuma = -1;
    cout << menorsuma << endl;
    for(unsigned long long z = 1; z < 1000000; z++) {
        cout << z << endl;
        if(z % 2 == 0) {
        for(set<unsigned long long>::iterator it1 = cuads.begin(); it1 != cuads.end(); it1++) {
            unsigned long long y = (*it1) + z;
            if(cuads.find(y + z) == cuads.end())
                continue;
/*            if(cuads.find(y - z) == cuads.end())  // SIEMPRE CIERTO
                continue;*/
            set<unsigned long long>::iterator it2 = it1;
            for(it2++; it2 != cuads.end(); it2++) {
                unsigned long long x = z + (*it2);
                if(cuads.find(x + y) == cuads.end())
                    continue;
                if(cuads.find(x - y) == cuads.end())
                    continue;
                if(cuads.find(x + z) == cuads.end())
                    continue;
            /*    if(cuads.find(x - z) == cuads.end())  // SIEMPRE CIERTO
                    continue;*/
                unsigned long long suma = (x + y + z);
                cout << x << " " << y << " " << z << " = " << suma << endl;
                if(suma < menorsuma)
                    menorsuma = suma;
            }
        }
        } else {
        for(set<unsigned long long>::iterator it1 = cuads.begin(); it1 != cuads.end(); it1++, it1++) {
            unsigned long long y = (*it1) + z;
            if(cuads.find(y + z) == cuads.end())
                continue;
/*            if(cuads.find(y - z) == cuads.end())  // SIEMPRE CIERTO
                continue;*/
            set<unsigned long long>::iterator it2 = it1;
            it2++;
            for(it2++; it2 != cuads.end(); it2++, it2++) {
                unsigned long long x = z + (*it2);
                if(cuads.find(x + y) == cuads.end())
                    continue;
                if(cuads.find(x - y) == cuads.end())
                    continue;
                if(cuads.find(x + z) == cuads.end())
                    continue;
            /*    if(cuads.find(x - z) == cuads.end())  // SIEMPRE CIERTO
                    continue;*/
                unsigned long long suma = (x + y + z);
                cout << x << " " << y << " " << z << " = " << suma << endl;
                if(suma < menorsuma)
                    menorsuma = suma;
            }
        }
        }
    }
    cout << menorsuma << endl;
    return 0;
}
