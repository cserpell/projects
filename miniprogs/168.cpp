#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long llenar(unsigned long long num, int l, set<unsigned long long> &s,
         unsigned long long *pot) {
   // unsigned long long sq = ((unsigned long long)sqrt(num)) + 1;
    unsigned long long mmax = pot[l - 1] - 1;
  /*  if(sq < mmax)
        mmax = sq;
    */unsigned long long c = 0;
    for(unsigned long long i = pot[l - 2]; i <= mmax; i++)
        if((num % i) == 0) {
            s.insert(i);
            c++;
        }
    return c;
}

int main() {
    unsigned long long sumtot = 0;
    int MAXL = 8;
    unsigned long long * pot;
    pot = new unsigned long long[MAXL + 1];
    pot[0] = 1;
    for(int i = 1; i < MAXL + 1; i++)
        pot[i] = pot[i - 1]*10;
    unsigned long long k;
    unsigned long long d0;
    unsigned long long totdiv;
    set<unsigned long long> cuales;
/*    for(k = 1; k <= 9; k++) {
        for(int l = 1; l < MAXL + 2; l++) {
            totdiv = (pot[l - 1] - k);
            cuales.clear();
            llenar(totdiv, cuales);
            for(d0 = 1; d0 <= 9; d0++) {
                set<unsigned long long>::iterator it;
                for(it = cuales.begin(); it != cuales.end(); it++) {
                    if(totdiv == (*it)*(10*k - 1)) {
                        cout << k << " " << d0 << " " << l << " " << (*it) << endl;
                    }
                }
            }
        }
    }
  */  for(k = 1; k <= 9; k++) {
        for(d0 = 1; d0 <= 9; d0++) {
            for(int l = 1; l < MAXL + 2; l++) {
                totdiv = (pot[l - 1] - k)*d0;
                cuales.clear();
                llenar(totdiv, l, cuales, pot);
                set<unsigned long long>::iterator it;
                for(it = cuales.begin(); it != cuales.end(); it++) {
                    if(totdiv == (*it)*(10*k - 1)) {
                        cout << k << " " << d0 << " " << l << " " << (*it) << d0 << endl;
                        sumtot = (sumtot + (10*(*it) + d0)%pot[5])%pot[5];
                    }
                }
            }
        }
    }
    cout << "TOT : " << sumtot << endl;
    return 0;
}
