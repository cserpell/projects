#include <iostream>
#include <sstream>

int main() {
	unsigned long long fact = 1;
	unsigned long long suma = 0;
	for(unsigned int i = 1; i < 100; i++) {
		fact *= i;
		std::cout << "f = " << fact << std::endl;
	}
	std::stringstream str;
	str << fact;
	std::string s = str.str();
	for(int i = 0; i < s.length(); i++) {
		std::cout << "sumando";
		suma += (unsigned int)(s.c_str()[i] - '0');
	}
	std::cout << suma << std::endl;
	return 0;
}
