import math

MOD = 1000000007


def f_s(nnn):
    min_len = int(nnn / 9)
    the_pow = pow(10, min_len)
    first_dig = nnn % 9
    return the_pow * (first_dig + 1) - 1


def fl_s(nnn):
    min_len = int(nnn / 9)
    first_dig = nnn % 9
    return first_dig, min_len


def big_s(k):
    many, last_pos = fl_s(k)
    last_base = pow(10, last_pos, MOD)
    # print('k: {} s(k) = {} log(s(k)) = {}'.format(k, last, last_pos))
    t_sum = 0
    """
    base = 1
    for pos in range(min(last_pos, 13)):
        t_sum += (base * (9 * (many + 9 * (last_pos - (pos + 1))) + 5 * 9)) % MOD
        # print(t_sum)
        base *= 10
        base %= MOD
    """
    t_sum = (many + 6) * (last_base - 1) - 9 * last_pos % MOD
    # t_sum = (many + 9 * last_pos - 4) * (last_base - 1) - (10 - last_pos * last_base + (last_pos - 1) * last_base * 10) % MOD
    t_sum += (last_base * int(many * (many + 1) / 2)) % MOD
    # print(t_sum)
    return t_sum


def next_fib(f1, f2):
    return f1 + f2


def main():
    f1 = 0
    f2 = 1
    print('Check: s(10) = {}'.format(f_s(10)))
    print('Check: S(2) = {}'.format(big_s(2)))
    print('Check: S(9) = {}'.format(big_s(9)))
    print('Check: S(20) = {}'.format(big_s(20)))
    tot_sum = 0
    for i in range(2, 91):
        ff = next_fib(f1, f2)
        print('current fib {}: {}'.format(i, ff))
        tot_sum += big_s(ff) % MOD
        f1 = f2
        f2 = ff
        print('sum so far: {}'.format(tot_sum))


if __name__ == '__main__':
  main()

