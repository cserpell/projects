#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

enteros todos;

unsigned long long maxt = -1;
double lmax = log((unsigned long long)-1);

void llenarTabla(int n) {
    double d = lmax/log(n);
    unsigned long long val = n;
    for(int i = 1; i < d; i++) {
        unsigned long long val2 = val;
        int suma = 0;
        while(val2 != 0) {
            suma += (val2 % 10);
            val2 /= 10;
        }
        if(suma == n)
            todos.insert(val);
        val *= n;
    }
}

int main() {
    for(int i = 2; i < 577; i++) {
        llenarTabla(i);
    }
    int i = -8;
    for(enteros::iterator it = todos.begin(); it != todos.end(); it++) {
        cout << i << " -> " << (*it) << endl;
        i++;
    }
	return 0;
}
