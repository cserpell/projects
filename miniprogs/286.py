import sys

def getP (n, k, Calc) :
  if n < 0 :
    return 0
  if k < 0 :
    return 0
  if n > k :
    return 0
  return Calc[k][n]

q = 51.0

if len(sys.argv) > 1 :
  q = float(sys.argv[1])

MAXS = 50
Calc = []
i = 0
while i <= MAXS :
  Calc.append([0] * (MAXS + 1))
  i = i + 1
Calc[0][0] = 1.0

i = 1
while i <= MAXS :
  j = 0
  while j <= i :
    Calc[i][j] = getP(j - 1, i - 1, Calc) * (q - i) + getP(j, i - 1, Calc) * i
    Calc[i][j] = Calc[i][j] / q
    j = j + 1
  i = i + 1

print Calc[50][20]

