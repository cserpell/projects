"""Problem 549."""
import copy


def factor(number):
  the_list = set([number])
  i = 2
  while i * i <= number:
    if number % i == 0:
      the_list.add(i)
      the_list.add(number / i)
    i += 1
  return the_list


def check_prime(number):
  if number == 2:
    return True
  if number % 2 == 0:
    return False
  i = 3
  while i * i <= number:
    if number % i == 0:
      return False
    i += 2
  return True


class P549(object):
  """Problem 549."""

  def __init__(self, max_pot):
    self._last_number = 1
    self._covered = set([1])
    self._assigned = {1: 1}
    self._sum = 0
    self._max_num = 10 ** max_pot
    self._from_primes = set()

  def fill_primes(self):
    number = 3
    while number <= self._max_num:
      print 'checking ' + str(number)
      if check_prime(number):
        self.add_num(number, number, self._covered)
        self._from_primes.add(number)
      number += 2
    self.add_num(2, 2, self._covered)
    self._from_primes.add(2)

  def add_num(self, new_number, number, covered):
    self._assigned[new_number] = number
    self._sum += number
    covered.add(new_number)

  def iterate(self):
    number = self._last_number + 1
    new_covered = copy.copy(self._covered)
    for covered in sorted(self._covered):
      if covered >= number and covered in self._from_primes:
        continue
      for divisor in factor(number):
        new_number = divisor * covered
        if new_number <= self._max_num and new_number not in self._assigned:
          self.add_num(new_number, number, new_covered)
    self._covered = new_covered
    self._last_number = number        

  def print_last(self):
    print 'last number: ' + str(self._last_number)
    #print 'assigned: ' + str(self._assigned)
    print 'sum: ' + str(self._sum)

  def run(self):
    self.fill_primes()
    print 'Finished checking primes'
    while len(self._assigned) < self._max_num:
      self.iterate()
      self.print_last()
    print 'Sum: ' + str(self._sum)


if __name__ == '__main__':
  P549(8).run()

