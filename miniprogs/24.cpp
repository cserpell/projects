#include <iostream>
#include <sstream>
#include <cmath>

int fact(int n) {
    int res = 1;
    for(int i = 2; i <= n; i++)
        res *= i;
    return res;
}

int main() {
    int ocupados[10];
    for(int i = 0; i < 10; i++)
        ocupados[i] = 0;
    int resto = 1000000;
    for(int i = 10; i > 0; i--) {
        int ff = fact(i - 1);
        int div = resto / ff;
        int cont = -1;
        int p;
        for(p = 0; p < 10; p++) {
            if(!ocupados[p]) {
                cont++;
            }
            if(cont == div)
                break;
        }
        ocupados[p] = i;
        resto = resto - div*ff;
    }
    for(int i = 0; i < 10; i++)
        std::cout << i << ": " << ocupados[i] << std::endl;
	return 0;
}
