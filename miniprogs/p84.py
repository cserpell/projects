"""Problem 84."""
import random


class Error(Exception):
  """Sanity errors."""


class P84(object):
  """Problem 84."""

  JAIL_POS = 10
  TAB = [{'name': 'GO', 'passes': 1},
         {'name': 'A1', 'passes': 0},
         {'name': 'CC1', 'passes': 0},
         {'name': 'A2', 'passes': 0},
         {'name': 'T1', 'passes': 0},
         {'name': 'R1', 'passes': 0},
         {'name': 'B1', 'passes': 0},
         {'name': 'CH1', 'passes': 0},
         {'name': 'B2', 'passes': 0},
         {'name': 'B3', 'passes': 0},
         {'name': 'JAIL', 'passes': 0},
         {'name': 'C1', 'passes': 0},
         {'name': 'U1', 'passes': 0},
         {'name': 'C2', 'passes': 0},
         {'name': 'C3', 'passes': 0},
         {'name': 'R2', 'passes': 0},
         {'name': 'D1', 'passes': 0},
         {'name': 'CC2', 'passes': 0},
         {'name': 'D2', 'passes': 0},
         {'name': 'D3', 'passes': 0},
         {'name': 'FP', 'passes': 0},
         {'name': 'E1', 'passes': 0},
         {'name': 'CH2', 'passes': 0},
         {'name': 'E2', 'passes': 0},
         {'name': 'E3', 'passes': 0},
         {'name': 'R3', 'passes': 0},
         {'name': 'F1', 'passes': 0},
         {'name': 'F2', 'passes': 0},
         {'name': 'U2', 'passes': 0},
         {'name': 'F3', 'passes': 0},
         {'name': 'G2J', 'passes': 0},
         {'name': 'G1', 'passes': 0},
         {'name': 'G2', 'passes': 0},
         {'name': 'CC3', 'passes': 0},
         {'name': 'G3', 'passes': 0},
         {'name': 'R4', 'passes': 0},
         {'name': 'CH3', 'passes': 0},
         {'name': 'H1', 'passes': 0},
         {'name': 'T2', 'passes': 0},
         {'name': 'H2', 'passes': 0}]
  CC_CARDS = ['GO', 'JAIL'] + ['x'] * 14
  CH_CARDS = ['GO', 'JAIL', 'C1', 'E3', 'H2', 'R1', 'R', 'R', 'U', '-3'] + ['x'] * 6

  def __init__(self, dice_side=4, number_iters=1000000):
    self._dice_side = dice_side
    self._number_iters = number_iters
    self._current_pos = 0
    self._size = len(self.TAB)
    self._doubles = 0
    self._ccs = []
    self._chs = []

  def tab_sanity(self):
    numbers_seen = set()
    names_seen = set()
    for number in range(self._size):
      if self.TAB[number]['name'] in names_seen:
        raise Error('Name %s is repeated' % self.TAB[number]['name'])
      if self.TAB[number]['pos'] in numbers_seen:
        raise Error('Number %s is repeated' % self.TAB[number]['pos'])
      numbers_seen.add(self.TAB[number]['pos'])
      names_seen.add(self.TAB[number]['name'])
    if self.TAB[self.JAIL_POS]['name'] != 'JAIL':
      raise Error('Jail not located at %s' % self.JAIL_POS)

  def start_tab(self):
    for number in range(self._size):
      self.TAB[number]['npos'] = number
      self.TAB[number]['pos'] = '%02d' % number

  def start_cards(self):
    while len(self.CC_CARDS) > 0:
      one = random.randint(0, len(self.CC_CARDS) - 1)
      self._ccs.append(self.CC_CARDS[one])
      self.CC_CARDS = self.CC_CARDS[:one] + self.CC_CARDS[(one + 1):]
    while len(self.CH_CARDS) > 0:
      one = random.randint(0, len(self.CH_CARDS) - 1)
      self._chs.append(self.CH_CARDS[one])
      self.CH_CARDS = self.CH_CARDS[:one] + self.CH_CARDS[(one + 1):]

  def spass(self):
    self.TAB[self._current_pos]['passes'] += 1

  def go_to_jail(self):
    self._current_pos = self.JAIL_POS

  def llog(self, additional):
    """Bla."""
    # print 'position %s name %s - %s' % (self._current_pos, self.TAB[self._current_pos]['name'], additional)

  def process_cc(self):
    card = self._ccs[0]
    self._ccs = self._ccs[1:] + [card]
    if card == 'GO':
      self._current_pos = 0
      self.llog('card CC GO')
      return
    if card == 'JAIL':
      self._current_pos = self.JAIL_POS
      self.llog('card CC JAIL')
      return
    self.llog('card CC %s' % card)

  def process_ch(self):
    card = self._chs[0]
    self._chs = self._chs[1:] + [card]
    if card == 'GO':
      self._current_pos = 0
      self.llog('card CH GO')
      return
    if card == 'JAIL':
      self._current_pos = self.JAIL_POS
      self.llog('card CH JAIL')
      return
    if card == 'C1':
      self._current_pos = 11  # self.JAIL_POS + 1
      assert(self.TAB[self._current_pos]['name'] == 'C1')
      self.llog('card CH C1')
      return
    if card == 'E3':
      self._current_pos = 24
      assert(self.TAB[self._current_pos]['name'] == 'E3')
      self.llog('card CH E3')
      return
    if card == 'H2':
      self._current_pos = 39
      assert(self.TAB[self._current_pos]['name'] == 'H2')
      self.llog('card CH H2')
      return
    if card == 'R1':
      self._current_pos = 5
      assert(self.TAB[self._current_pos]['name'] == 'R1')
      self.llog('card CH R1')
      return
    if card == 'R':
      while not self.TAB[self._current_pos]['name'].startswith('R'):
        self._current_pos = (self._current_pos + 1) % self._size
      self.llog('card CH R')
      return
    if card == 'U':
      while not self.TAB[self._current_pos]['name'].startswith('U'):
        self._current_pos = (self._current_pos + 1) % self._size
      self.llog('card CH U')
      return
    if card == '-3':
      self._current_pos = (self._current_pos - 3) % self._size
      if self.TAB[self._current_pos]['name'].startswith('CC'):
        # print 'Special case!'
        self.process_cc()
        return
      self.llog('card CH -3')
      return
    self.llog('card CH %s' % card)

  def roll_dices(self):
    dice1 = random.randint(1, self._dice_side)
    dice2 = random.randint(1, self._dice_side)
    # print '%s %s' % (dice1, dice2)
    is_double = bool(dice1 == dice2)
    if is_double:
      if self._doubles == 2:
        self._doubles = 0
        self.go_to_jail()
        self.llog('doubles 3 times')
        return
      else:
        self._doubles += 1
    else:
      self._doubles = 0
    self._current_pos = (self._current_pos + dice1 + dice2) % self._size
    if self.TAB[self._current_pos]['name'] == 'G2J':
      self.go_to_jail()
      self.llog('fell into G2J')
      return
    if self.TAB[self._current_pos]['name'].startswith('CC'):
      self.process_cc()
      return
    if self.TAB[self._current_pos]['name'].startswith('CH'):
      self.process_ch()
      return
    self.llog('normal movement')

  def get_max(self, excluding=None):
    if not excluding:
      excluding = []
    mm = -1
    nm = -1
    total_passes = 0
    for number in range(self._size):
      total_passes += self.TAB[number]['passes']
      if number not in excluding and self.TAB[number]['passes'] > mm:
        mm = self.TAB[number]['passes']
        nm = number
    print 'Position %s with freq %s' % (nm, 100 * float(mm) / total_passes)
    return nm

  def main(self):
    self.start_tab()
    self.start_cards()
    self.tab_sanity()
    iters = 0
    while iters < self._number_iters:
      self.roll_dices()
      self.spass()
      iters += 1
    print 'Ending'
    max1 = self.get_max()
    max2 = self.get_max(excluding=[max1])
    max3 = self.get_max(excluding=[max1, max2])
    print '%s%s%s' % (self.TAB[max1]['pos'], self.TAB[max2]['pos'], self.TAB[max3]['pos'])


if __name__ == '__main__':
  P84().main()

