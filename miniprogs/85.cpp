#include <iostream>
#include <cmath>
#include <set>

using namespace std;

int main() {
    int cualk = -1;
    int cualm = -1;
    int mindif = 10000000;
    for(int k = 1; k < 2100; k++) {
        for(int m = k; m < 2100; m++) {
            int cuantos = (k*(k + 1))/2;
            cuantos *= (m*(m + 1))/2;
            int ndif = 2000000 - cuantos;
            if(ndif < 0)
                ndif = -ndif;
            if(ndif < mindif) {
                cualk = k;
                cualm = m;
                mindif = ndif;
            }
        }
    }
 	std::cout << cualk << ", " << cualm << " = " << (cualk*cualm) << std::endl;
	return 0;
}
