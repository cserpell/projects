#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <list>

using namespace std;

int condn[22][17];

int corr[22];

int disponible[17][10];

int cantcondiciones;

int cualenpos[17];

void itera(int cantpos, int cantcondiciones, int mmax) {
  /*  for(int i = mmax - 1; i >= 0; i--) {
      if(cualenpos[i] >= 0)
        cout << cualenpos[i];
      else
        cout << "_";
    }
    cout << endl;*/
    if(cantpos == cantcondiciones) {
      // Lo logro?
      for(int i = mmax - 1; i >= 0; i--) {
        if(cualenpos[i] == -1)
          return;
      }
      for(int i = mmax - 1; i >= 0; i--)
        cout << cualenpos[i];
      cout << "**" << endl;
      return;
    }
    int correctasactuales = corr[cantpos];
    int i = cantpos;
    for(int j1 = 0; j1 < mmax; j1++) {
      if((((cualenpos[j1] == -1) && (condn[i][j1] >= 0)) ||
         ((cualenpos[j1] >=  0) && (condn[i][j1] == cualenpos[j1]) ))
         && (disponible[j1][condn[i][j1]] > 0)
         ) {
        int last1 = cualenpos[j1];
        cualenpos[j1] = condn[i][j1];
        if(correctasactuales > 1) {
          for(int j2 = j1 + 1; j2 < mmax; j2++) {
            if((((cualenpos[j2] == -1) && (condn[i][j2] >= 0)) ||
         ((cualenpos[j2] >=  0) && (condn[i][j2] == cualenpos[j2]) ))
         && (disponible[j2][condn[i][j2]] > 0)
         ) {
              int last2 = cualenpos[j2];
              cualenpos[j2] = condn[i][j2];
              if(correctasactuales > 2) {
                for(int j3 = j2 + 1; j3 < mmax; j3++) {
                  if((((cualenpos[j3] == -1) && (condn[i][j3] >= 0)) ||
         ((cualenpos[j3] >=  0) && (condn[i][j3] == cualenpos[j3]) ))
         && (disponible[j3][condn[i][j3]] > 0)
         ) {
                    int last3 = cualenpos[j3];
                    cualenpos[j3] = condn[i][j3];
                    int disptemp[23];
                    bool terminar = false;
                    for(int ll = 0; ll < mmax; ll++)
                      if((ll != j1) && (ll != j2) && (ll != j3)) {
                        if((cualenpos[ll] >= 0) && (condn[i][ll] == cualenpos[ll])) {
                          // No funciona
                          for(int lm = 0; lm < ll; lm++)
                            if((lm != j1) && (lm != j2) && (lm != j3))
                              disponible[lm][condn[i][lm]] = disptemp[lm];
                          terminar = true;
                          break;
                        }
                        disptemp[ll] = disponible[ll][condn[i][ll]];
                        disponible[ll][condn[i][ll]] = 0;
                      }
                    if(!terminar) {
                      itera(cantpos + 1, cantcondiciones, mmax);
                      for(int ll = 0; ll < mmax; ll++)
                        if((ll != j1) && (ll != j2) && (ll != j3))
                          disponible[ll][condn[i][ll]] = disptemp[ll];
                    }
                    cualenpos[j3] = last3;
                  }
                }
              } else {
                    int disptemp[23];
                    bool terminar = false;
                    for(int ll = 0; ll < mmax; ll++)
                      if((ll != j1) && (ll != j2)) {
                        if((cualenpos[ll] >= 0) && (condn[i][ll] == cualenpos[ll])) {
                          // No funciona
                          for(int lm = 0; lm < ll; lm++)
                            if((lm != j1) && (lm != j2))
                              disponible[lm][condn[i][lm]] = disptemp[lm];
                          terminar = true;
                          break;
                        }
                        disptemp[ll] = disponible[ll][condn[i][ll]];
                        disponible[ll][condn[i][ll]] = 0;
                      }
                    if(!terminar) {
                      itera(cantpos + 1, cantcondiciones, mmax);
                      for(int ll = 0; ll < mmax; ll++)
                        if((ll != j1) && (ll != j2))
                          disponible[ll][condn[i][ll]] = disptemp[ll];
                    }
              }
              cualenpos[j2] = last2;
            }
          }
        } else {
                    int disptemp[23];
                    bool terminar = false;
                    for(int ll = 0; ll < mmax; ll++)
                      if((ll != j1)) {
                        if((cualenpos[ll] >= 0) && (condn[i][ll] == cualenpos[ll])) {
                          // No funciona
                          for(int lm = 0; lm < ll; lm++)
                            if((lm != j1))
                              disponible[lm][condn[i][lm]] = disptemp[lm];
                          terminar = true;
                          break;
                        }
                        disptemp[ll] = disponible[ll][condn[i][ll]];
                        disponible[ll][condn[i][ll]] = 0;
                      }
                    if(!terminar) {
                      itera(cantpos + 1, cantcondiciones, mmax);
                      for(int ll = 0; ll < mmax; ll++)
                        if((ll != j1))
                          disponible[ll][condn[i][ll]] = disptemp[ll];
                    }
        }
        cualenpos[j1] = last1;
      }
    }
}

int main(int argc, char *argv[]) {
    for(int i = 0; i < 22; i++) {
      for(int j = 0; j < 10; j++)
        condn[i][j] = -1;
      corr[i] = -1;
    }
    for(int i = 0; i < 17; i++) {
      for(int j = 0; j < 10; j++)
        disponible[i][j] = 1;
    }
    cantcondiciones = 0;
    int cantleidas = 0;
    unsigned long long nn;
    int NMAX = 16;
    if(argc > 1)
      NMAX = atoi(argv[1]);
    int CMAX = 22;
    if(argc > 2)
      CMAX = atoi(argv[2]);
    while(cantleidas < CMAX) {
      cin >> nn;
      int correctas = nn % 10;
      nn = (nn - correctas)/10;
      cout << nn;
      cout << " ;" << correctas << " correct" << endl;
      if(correctas == 0) {
        for(int pos = 0; pos < NMAX; pos++) {
          int cual = nn % 10;
          disponible[pos][cual] = 0;
          nn = (nn - cual)/10;
        }
      } else {
        for(int pos = 0; pos < NMAX; pos++) {
          int cual = nn % 10;
          condn[cantcondiciones][pos] = cual;
          nn = (nn - cual)/10;
        }
        corr[cantcondiciones] = correctas;
        cantcondiciones++;
      }
      cantleidas++;
    }
    // Limpia las no disponibles
    for(int i = 0; i < cantcondiciones; i++) {
      for(int j = 0; j < NMAX; j++) {
        if(disponible[j][condn[i][j]] == 0)
          condn[i][j] = -1;
      }
    }
    // Prueba todo...
    for(int i = 0; i < NMAX; i++)
      cualenpos[i] = -1;
    itera(0, cantcondiciones, NMAX);

	return 0;
}
