#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

int main() {
    int *tabla;
    
    tabla = new int[1000];
    for(int i = 0; i < 1000; i++)
        tabla[i] = 0;
    int cual = 0;
    for(int i = 0; i < 40; i++) {
        tabla[cual] = 1;
        cual += i;
    }
    string str;
    std::cin >> str;
    int oldpos = 0;
    int contador = 0;
    while(1) {
        int pos = str.find(',', oldpos);
        if(pos == string::npos) {
            break;
        }
        string palabra = str.substr(oldpos + 1, pos - oldpos - 2);
        oldpos = pos + 1;
        int valor = 0;
        for(int i = 0; i < palabra.length(); i++) {
            valor += (palabra.c_str()[i] - 'A') + 1;
        }
        std::cout << palabra << " -> " << valor;
        if(tabla[valor]) {
            contador++;
            std::cout << " es triangular!";
        }
        std::cout << std::endl;
    }
    std::cout << contador << std::endl;
	return 0;
}
