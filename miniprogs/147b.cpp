#include <iostream>
#include <cmath>
#include <set>

using namespace std;

long long mmin(long long a, long long b) {
    if(a < b)
        return a;
    return b;
}

long long mmax(long long a, long long b) {
    if(a < b)
        return b;
    return a;
}

unsigned long long suma1(long long n, long long m) {
    unsigned long long ret = 0;
    for(long long p = 1; p <= n; p++)
        ret += (m + 1)*p;
    return ret;
}

unsigned long long suma2(long long n, long long m) {
    unsigned long long ret = 0;
    for(long long l = 1; l <= 2*n - 1; l++)
        for(long long p = 1; p <= n - l/2; p++)
            ret += mmin(2*p - 1, mmax(2*m - l + 1, 0));
    return ret;
}

unsigned long long suma3(long long n, long long m) {
    unsigned long long ret = 0;
    for(long long l = 1; l <= 2*n - 2; l++)
        for(long long p = 1; p <= n - (l + 1)/2; p++)
            ret += mmin(2*p, mmax(2*m + 2 - l, 0));
    return ret;
}

unsigned long long tabla[50][50];

unsigned long long cantidad_total(long long n, long long m) {
    if(m == 0 && n == 1) {
        tabla[1][1] = 1;
        return tabla[1][1];
    }
    if(m == 0) {
        tabla[n][1] = tabla[1][n];
        return tabla[n][1];
    }
    unsigned long long s1, s2, s3;
    s1 = suma1(n, m);
    s2 = suma2(n, m);
    s3 = suma3(n, m);
    tabla[n][m + 1] = tabla[n][m] + s1 + s2 + s3;
    return tabla[n][m + 1];
}

int main() {
    for(long long n = 1; n < 50; n++)
        for(long long m = 0; m < 50; m++) {
            cantidad_total(n, m);
            std::cout << n << ", " << (m + 1) << ": " << tabla[n][m + 1] << std::endl;
        }
    unsigned long long total = 0;
    for(int i = 1; i <= 47; i++)
        for(int j = 1; j <= 43; j++)
            total += tabla[i][j];
    std::cout << "Total : " << total << std::endl;
    return 0;
}
