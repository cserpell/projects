#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long increm[101][10];
unsigned long long decrem[101][10];
unsigned long long bouncy[101];

int main() {
    for(int i = 0; i < 10; i++) {
        increm[1][i] = 1;
        decrem[1][i] = 1;
    }
    for(int i = 2; i <= 100; i++) {
        for(int j = 0; j < 10; j++) {
            unsigned long long suma = 0;
            for(int k = j; k < 10; k++) {
//                for(int prof = 1; prof < i; prof++)
                    suma += increm[i - 1][k];
            }
            increm[i][j] = suma;
            cout << "i[" << i << "][" << j << "]=" << increm[i][j] << " ";
        }
        for(int j = 0; j < 10; j++) {
            unsigned long long suma = 0;
            for(int k = j; k >= 0; k--) {
//                for(int prof = 1; prof < i; prof++)
                    suma += decrem[i - 1][k];
            }
            decrem[i][j] = suma;
            cout << "d[" << i << "][" << j << "]=" << decrem[i][j] << " ";
        }
        cout << endl;
    }
    bouncy[1] = 0;
    bouncy[2] = 0;
    unsigned long long cmany = 0;
    for(int i = 1; i <= 100; i++) {
     /*   bouncy[i] = bouncy[i - 1];
        for(int j = 1; j < 10; j++) {
            bouncy[i] += increm[i - 1][j]*(9 - j);
            bouncy[i] += decrem[i - 1][j]*(j - 1);
        }
        cout << i << ": " << bouncy[i] << endl;*/
        for(int j = 1; j < 10; j++) {
            cmany += increm[i][j];
            cmany += decrem[i][j];
        }
        cmany -= 9;
        cout << i << ": no bouncy = " << cmany << endl;
    }
	return 0;
}
