

def mcd(a, b):
  # Here a >= b
  if b == 1:
    return 1
  m = a / b
  c = m * b
  if c == a:
    return b
  return mcd(b, a - c)


def l(n):
  m = int((n + 1) ** 0.5)
  while m <= n / 2:
    if mcd(n, m) == 1 and ((m * m) % n) == 1:
      return n - m
    m += 1
  return 1


def ss(n):
  s = 0
  m = 3
  while m <= n:
    if (m % 10000) == 0:
      print m
    s += l(m)
    m += 1


def main():
  print l(15)
  print l(100)
  print l(7)
  print ss(2 * 10000000)


if __name__ == '__main__':
  main()

