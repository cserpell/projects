
MAX_PER = 1000

SQUARES_SO_FAR = [(1, 1)]

def test_sqrt(number):
  sqrt = number ** 0.5
  isqrt = int(sqrt)
  if isqrt == sqrt:
    return isqrt
  return 0


def main():
  max_l = MAX_PER / 2
  maxr = 0
  maxl = 0
  tot = 0
  for a in range(1, max_l):
    print 'Testing starting with %s -- so far %s' % (a, tot)
    print 'selected max l %s r %s' % (maxl, maxr)
    for b in range(a, int((MAX_PER - a) / 2) + 1):
      min_q = a * (b - a) + 1
      max_q1 = a * b - 1
      max_q2 = int(2 * a * b * float(MAX_PER - a - b) / float(MAX_PER))
      if max_q1 <= max_q2:
        maxl += 1
      else:
        maxr += 1
      max_q = min(max_q1, max_q2)
      for q in range(min_q, max_q + 1):
        up_c = q * (a + b)
        down_c = 2 * a * b - q
        if up_c % down_c:
          continue
        # Candidate
        c = up_c / down_c
        s1 = test_sqrt(a * c - q)
        if not s1:
          continue
        s2 = test_sqrt(a * b - q)
        if not s2:
          continue
        s3 = test_sqrt(b * c - q)
        if not s3:
          continue
        print '%s %s %s -- %s %s %s' % (a, b, c, s1, s2, s3)
        tot += a + b + c + s1 + s2 + s3
  print tot


if __name__ == '__main__':
  main()

