import java.util.*;
import java.io.*;
import java.math.*;

class c25 {
    static public void main(String[] args) {
//        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        BigInteger f0 = new BigInteger("" + 0);
        BigInteger f1 = new BigInteger("" + 1);
        BigInteger f2;
        int calculado = 1;
        int largo = f1.toString().length();
        while(largo < 1000) {
            f2 = f1.add(f0);
            f0 = f1;
            f1 = f2;
            calculado++;
            largo = f1.toString().length();
        }
        System.out.println(calculado);
    }
}
