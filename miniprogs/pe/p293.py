def read_primes (filename):
  f = open(filename, "r")
  primes = []
  for l in f:
    primes.append(int(l[0:len(l) - 1]))
  return primes

def binsear (primes, q):
  ini = 0
  fin = len(primes) - 1
  while ini < fin:
    p = (fin - ini)/2 + ini
    if primes[p] < q:
      ini = p + 1
    else :
      fin = p
  if ini != fin:
    print "Something nasty here, please check"
    system.exit(1)
  return primes[ini]

def iter (primes, pos, lastnum, numlim):
  if pos >= len(primes):
    return 0
  actual = primes[pos]
  snum = lastnum*actual
  sumatot = 0
  while snum < numlim:
    # Test snum
    rres = binsear(primes, snum + 2)
    print "%d -> %d (next prime %d)" % (snum, rres - snum, rres)
    sumatot = sumatot + rres - snum
    # Try following
    sumatot = sumatot + iter (primes, pos + 1, snum, numlim)
    snum = snum*actual
  return sumatot

myprim = read_primes("primes109.txt")
print iter(myprim, 0, 1, 1000000000)
