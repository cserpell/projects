import math

def Largo(n):
  return math.ceil(math.log(1. * (n + 1), 10))

def GetPos(n):
  l = Largo(n)
  #print n, l
  #print  (n - 1)*l - (math.pow(10, l - 1) - 1) + 1
  return (n - 1)*l - (math.pow(10, l - 1) - 1) + 1

def Gen (n, j):
  little_part = 0
  big_part = 0
  l = Largo(n)
  bas = math.pow(10, j)
  big_bas = math.pow(10, j + l)
  while True:
    little_part = 0
    while little_part < bas:
      res = big_part + n*bas + little_part
      print "generator of %d and %d returning %d" % (n, j, res)
      yield res
      little_part = little_part + 1
    big_part = big_part + big_bas

def Calculate(queryn):
  print "Calculating for %d" % queryn
  all_generators = []
  all_values = []
  mark_mins = []
  maxexp = -1
  maxexp = maxexp + 1
  ng = Gen(queryn, maxexp)
  all_generators.append(ng)
  all_values.append(all_generators[maxexp].next())
  maxexp = maxexp + 1
  ng = Gen(queryn, maxexp)
  all_generators.append(ng)
  all_values.append(all_generators[maxexp].next())
  mark_mins.append(0)
  mark_mins.append(0)
  totseen = 0
  while True:
    amin = all_values[0]
    for i in range(0, maxexp + 1):
      if all_values[i] < amin:
        amin = all_values[i]
    for i in range(0, maxexp + 1):
      if all_values[i] == amin:
        mark_mins[i] = 1
        totseen = totseen + 1
      else:
        mark_mins[i] = 0
    while mark_mins[maxexp]:
      maxexp = maxexp + 1
      ng = Gen(queryn, maxexp)
      all_generators.append(ng)
      all_values.append(all_generators[maxexp].next())
      if all_values[maxexp] == amin:
        mark_mins.append(1)
        totseen = totseen + 1
      else:
        mark_mins.append(0)
    if totseen >= queryn:
      i = maxexp
      while i >= 0:
        if mark_mins[i]:
          if totseen == queryn:
            rpos = GetPos(all_values[i] + 1) - Largo(queryn) - i
            return (all_values[i], i, rpos)
          totseen = totseen - 1
        i = i - 1
    for i in range(0, maxexp + 1):
      if mark_mins[i]:
        all_values[i] = all_generators[i].next()

print Calculate(2)
print Calculate(3)
print Calculate(5)
print Calculate(12)
#print Calculate(7780)
