import math

def numpos (x, y):
  if x == 0 and y == 0:
    return 0
  if x > y:
    (x, y) = (y, x)
  k = math.floor(math.log(y))
  li = math.pow(2, k)
  if x < li:
    return numpos(y - li, x) + li
  else :
    return numpos(y - li, x - li)

def numpos2 (x, y):
  return x ^ y

i = 1
limii = math.pow(2, 30)
tott = 0
while i <= limii:
  if i % 100000 == 0:
    print i
  if numpos2(i, 2*i) == 3*i:
    tott = tott + 1
  i = i + 1
print tott
