import sys
import math

if(len(sys.argv) < 2):
  print "Usage: comprimes.py max_limit"

max_lim = int(sys.argv[1])

primes = [2]
print "2"
i = 3
while i < max_lim:
  is_prime = 1
  for j in primes:
    if j*j > i:
      break
    if (i % j) == 0:
      is_prime = 0
      break
  if is_prime:
    print i
    primes.append(i)
  i = i + 2
