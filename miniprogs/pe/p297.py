def genfibons (n):
  i = 1
  res = [1, 2]
  while res[i] < n:
    res.append(res[i] + res[i - 1])
    i = i + 1
  return res

fibs = genfibons(1000001)

def funz (n):
  if n == 0:
    return 0
  lastbest = 1
  for i in range(1, len(fibs)):
    if fibs[i] > n:
      break
    lastbest = fibs[i]
  return 1 + funz(n - lastbest)

sumf = [1, 2]
for i in range(2, len(fibs)):
  sumf.append(sumf[i - 1] + sumf[i - 2] + fibs[i - 2] - 1)

def sumz (n):
  print "Iterating for %d" % n
  if n == 0:
    return 0
  if n < 0:
    print "ERROR %d" % n
    return -1
  lastbest = 0
  for i in range(1, len(fibs)):
    if fibs[i] > n:
      break
    lastbest = i
  print "Sending iteration for %d" % (n - fibs[lastbest])
  return sumf[lastbest] + sumz(n - fibs[lastbest]) + n - fibs[lastbest]

def pn(n):
  print "%d -> %d  --  %d" % (n, funz(n), sumz(n))
for i in range(1, 20):
  pn(i)
pn(100)
print "sumz(10^6) -> %d" % sumz(1000000)
