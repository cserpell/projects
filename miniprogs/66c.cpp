#include <iostream>
#include <cmath>

int main() {
    unsigned long long maxx = 0;
    int which = 0;
    for(int D = 2; D <= 1000; D++) {
//        for(unsigned long long x = maxx; 
        unsigned long long p = (unsigned long long)sqrt(D);
        if(p*p == D)
            continue;
        unsigned long long q = 1;
        for(unsigned long long y = 1; y < 100000000000LL; y++) {
            q += D*(2*y - 1);
            unsigned long long x = (unsigned long long)sqrt(q);
            if(x*x == q) {
                if(x > maxx) {
                    which = D;
                    maxx = x;
                }
                std::cout << D << ": " << x << " , " << y << "\t" << which << ": " << maxx << std::endl;
                break;
            }
        }
    }
    std::cout << which << std::endl;
    return 0;
}
