#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

int isPrime(int *tabla, unsigned int n) {
    unsigned int hasta = ((int)sqrt(n)) + 1;
    for(unsigned int i = 3; i <= hasta; i += 2) {
        if(tabla[i] && (n % i) == 0) {
            tabla[n] = 0;
            return 0;
        }
    }
    tabla[n] = 1;
    return 1;
}

int main() {
    int *tabla;
    
    tabla = new int[10000];
    for(unsigned int i = 0; i < 10000; i ++) {
        tabla[i] = 0;
    }
    for(unsigned int i = 3; i < 10000; i += 2) {
        isPrime(tabla, i);
    }
    tabla[2] = 1;
    int *cands;
    int *can;
    can = new int[10000];
    cands = new int[10000];
    for(int i = 0; i < 10000; i ++) {
        can[i] = 0;
    }
    int numc = 0;
    for(int d1 = 1; d1 <= 9; d1++) {
        for(int d2 = 0; d2 <= 9; d2++) {
      /*      if(d2 == d1)
                continue;
        */    for(int d3 = 0; d3 <= 9; d3++) {
          /*      if(d3 == d1)
                    continue;
                if(d3 == d2)
                    continue;
            */    for(int d4 = 0; d4 <= 9; d4++) {
              /*      if(d4 == d1)
                        continue;
                    if(d4 == d2)
                        continue;
                    if(d4 == d3)
                        continue;
                */    if(!tabla[d1*1000 + d2*100 + d3*10 + d4])
                        continue;
                    std::cout << "candidato: " << (d1*1000 + d2*100 + d3*10 + d4) << std::endl;
                    cands[numc] = d1*1000 + d2*100 + d3*10 + d4;
                    can[cands[numc]] = (int)(pow(2, d1) + pow(2, d2) + pow(2, d3) + pow(2, d4));
                    numc++;
                }
            }
        }
    }
    std::cout << "numc: " << numc << std::endl;
    for(int i = 0; i < numc; i++) {
        for(int j = i + 1; j < numc; j++) {
            int dif = cands[j] - cands[i];
            int otro = cands[j] + dif;
            if(otro >= 10000)
                continue;
            if(!can[otro])
                continue;
            if(can[cands[i]] == can[cands[j]] && can[cands[i]] == can[otro])
                std::cout << cands[i] << " " << cands[j] << " " << otro << std::endl;
        }
    }
//    std::cout << maxcant << ": " << (cuala*cualb) << std::endl;
	return 0;
}
