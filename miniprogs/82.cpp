#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

void recur(int nivel, int **mat, int **llenar) {
    if(nivel == 80) {
        return;
    }
    if(nivel == 0) {
        for(int i = 0; i < 80; i++) {
            llenar[0][i] = mat[0][i];
        }
        recur(1, mat, llenar);
        return;
    }
    int *ocupado = new int[80];
    for(int i = 0; i < 80; i++)
        ocupado[i] = 0;
    for(int j = 0; j < 80; j++) {
        int min = -1;
        int cmin = 99999999;
        for(int i = 0; i < 80; i++) {
            if(ocupado[i])
                continue;
            if(llenar[nivel - 1][i] < cmin) {
                cmin = llenar[nivel - 1][i];
                min = i;
            }
            if(i - 1 >= 0 && ocupado[i - 1]) {
                if(llenar[nivel][i - 1] < cmin) {
                    cmin = llenar[nivel][i - 1];
                    min = i;
                }
            }
            if(i + 1 < 80 && ocupado[i + 1]) {
                if(llenar[nivel][i + 1] < cmin) {
                    cmin = llenar[nivel][i + 1];
                    min = i;
                }
            }
        }
        ocupado[min] = 1;
        llenar[nivel][min] = mat[nivel][min] + cmin;
    }
    delete [] ocupado;
    recur(nivel + 1, mat, llenar);
}

int main() {
    int **mat = new int *[80];
    int **llenar = new int *[80];
    for(int i = 0; i < 80; i++) {
        mat[i] = new int[80];
        llenar[i] = new int[80];
    }
    for(int i = 0; i < 80; i++) {
        string str;
        cin >> str;
        int upos = -1;
        for(int j = 0; j < 80; j++) {
            int nupos = str.find(",", upos + 1);
            int a = atoi(str.substr(upos + 1, nupos - upos - 1).c_str());
            mat[j][i] = a;
            cout << a << ",";
            upos = nupos;
        }
        cout << endl;
    }
    recur(0, mat, llenar);
    int min = -1;
    int cmin = 99999999;
    for(int i = 0; i < 80; i++) {
        if(llenar[79][i] < cmin) {
            cmin = llenar[79][i];
            min = i;
        }
    }
    cout << endl;
    for(int i = 0; i < 80; i++) {
        for(int j = 0; j < 80; j++) {
            cout << llenar[i][j] << ",";
        }
        cout << endl;
    }
    cout << cmin << endl;
	return 0;
}
