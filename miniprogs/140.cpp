#include <iostream>
#include <cmath>

using namespace std;

int isCuad(unsigned long long n) {
    unsigned long long b = 1 + 14*n + 5*n*n;
    unsigned long long a = (unsigned long long)sqrt(b);
    if(a*a == b)
        return 1;
    return 0;
}

int main() {
    unsigned long long i;
    unsigned long long suma = 0;
    int cant = 1;
    for(i = 2; i < 100000000000LL; i++) {
        if(isCuad(i)) {
            suma += i;
            cout << cant << ": " << i << " -> " << suma << endl;
            cant++;
        }
    }
	return 0;
}
