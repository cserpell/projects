#include <iostream>
#include <sstream>

int main() {
    int grilla[20][20];
    int maxprod = 0;
    for(int i = 0; i < 20; i++)
        for(int j = 0; j < 20; j++)
            std::cin >> grilla[i][j];
    for(int i = 0; i < 20; i++) {
        for(int j = 0; j < 20; j++) {
            if(i < 17) {
                int prod = grilla[i][j]*grilla[i+1][j]*grilla[i+2][j]*grilla[i+3][j];
                if(prod > maxprod)
                    maxprod = prod;
            }
            if(j < 17) {
                int prod = grilla[i][j]*grilla[i][j+1]*grilla[i][j+2]*grilla[i][j+3];
                if(prod > maxprod)
                    maxprod = prod;
            }
            if(i < 17 && j < 17) {
                int prod = grilla[i][j]*grilla[i+1][j+1]*grilla[i+2][j+2]*grilla[i+3][j+3];
                if(prod > maxprod)
                    maxprod = prod;
            }
            if(j < 17 && i > 2) {
                int prod = grilla[i][j]*grilla[i-1][j+1]*grilla[i-2][j+2]*grilla[i-3][j+3];
                if(prod > maxprod)
                    maxprod = prod;
            }
	    }
	}
	std::cout << maxprod << std::endl;
	return 0;
}
