"""Problem 101."""
import numpy


def pot(a, b):
  if b == 0:
    return 1
  if b == 1:
    return a
  mid = b / 2
  if 2 * mid == b:
    mm = pot(a, mid)
    return mm * mm
  return pot(a, mid) * pot(a, mid + 1)


class P101(object):
  """Problem 101."""

  def __init__(self, max_deg=10):
    self._sum = 0
    self._last = 0
    self._max_deg = max_deg
    self._g = []

  def init_g(self, function):
    ite = 1
    while ite <= self._max_deg:
      self._g.append(function(ite))
      ite += 1
    print self._g

  def get_m(self, size):
    m = numpy.zeros((size, size), dtype=numpy.int64)
    ite = 1
    while ite <= size:
      ite2 = 0
      num = 1
      while ite2 < size:
        m[ite - 1][ite2] = num
        num *= ite
        ite2 +=1
      ite += 1
    return m

  def get_vect(self, size):
    vect = numpy.zeros(size, dtype=numpy.int64)
    num = 1
    ite = 0
    while ite < size:
      vect[ite] = num
      num *= size + 1
      ite += 1
    return vect

  def print_iters(self, size, vect, m, minv, gvect):
    print '===================== size %s =====================' % size
    print 'Vector %s' % vect
    print 'G %s' % gvect
    print 'Matrix'
    print '%s' % m
    print 'Inverse Matrix'
    print '%s' % minv
  
  def solve_one(self, size):
    vect = self.get_vect(size)
    m = self.get_m(size)
    minv = numpy.linalg.inv(m)
    gvect = numpy.array(self._g[:size], dtype=numpy.int64)
    self.print_iters(size, vect, m, minv, gvect)
    return numpy.dot(vect, numpy.dot(minv, gvect))

  def main(self):
    def function(num):
      return num * num * num

    def function2(num):
      total = 0
      for e in range(0, 11):
        total += pot(-num, e)
      return total

    self.init_g(function2)
    total = 0
    for iters in xrange(1, 11):
      result = self.solve_one(iters)
      total += result
      print 'For %s result %s -- total %s' % (iters, result, total)


if __name__ == '__main__':
  P101().main()

