#include <iostream>
#include <cmath>
#include <set>

using namespace std;

int probando[12001];
int mejores[12001];

void calcular(int largomax, int maxnumeroeniteracion, int pos, int multhastaahora, int sumahastaahora) {
  if(pos >= largomax) {
    if(multhastaahora == sumahastaahora) {
      if(multhastaahora < mejores[largomax])
        mejores[largomax] = sumahastaahora;
      return;
    }
    return;
  }
  int realsuma = sumahastaahora + largomax - pos;
  if(multhastaahora > realsuma) {
    return;
  }
  if(multhastaahora == realsuma) {
    if(multhastaahora < mejores[largomax])
      mejores[largomax] = realsuma;
    return;
  }
  if(multhastaahora >= mejores[largomax])  // multhastaahora es < realsuma aqui
    return;
  for(int i = maxnumeroeniteracion; i > 1; i--) {
   // probando[pos] = i;
    int nmult = multhastaahora*i;
    int nsuma = sumahastaahora + i;
/*    if(nmult >= mejores[largomax])
      continue;*/
//    calcular(largomax, i, pos + 1, nmult, nsuma);
    calcular(largomax, (nsuma + largomax - pos - 1)/(nmult - 1), pos + 1, nmult, nsuma);
  }
}

int main() {
    for(int i = 1; i <= 12000; i++) {
      mejores[i] = 2*i;
    }
    for(int i = 2; i <= 12000; i++) {
       calcular(i, i, 0, 1, 0);
       cout << i << ": " << mejores[i] << endl;
    }
    cout << "FIN" << endl;
	return 0;
}
