#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

int isAbundant(int n) {
    int suma = 1;
    for(int i = 2; i <= n/2; i++) {
        if(n%i == 0)
            suma += i;
    }
    if(suma > n)
        return 1;
    cout << n << " no es abundante" << endl;
    return 0;
}

int main() {
    int *tabla;
    int *tabla2;
    
    tabla = new int[30000];
    tabla2 = new int[30000];
    tabla[0] = 0;
    tabla[1] = 0;
    for(int i = 2; i < 30000; i++) {
        tabla[i] = isAbundant(i);
        tabla2[i] = 0;
    }
    cout << "tabla llenada" << endl;
    for(int i = 2; i < 30000; i++) {
        if(tabla[i]) {
            for(int j = i; j < 30000; j++) {
                if(i + j < 30000) {
                    if(tabla[j]) {
                        tabla2[i + j] = 1;
                    }
                }
            }
        }
    }
    int suma = 0;
    for(int i = 1; i < 30000; i++)
        if(!tabla2[i]) {
            cout << i << " no se puede" << endl;
            suma += i;
        }
    std::cout << suma << endl;
	return 0;
}
