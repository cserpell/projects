#include <iostream>
#include <cmath>
#include <set>
#include <queue>

using namespace std;

class circulo {
 public:
  double R;
  int iteracion;
  circulo * p1;
  circulo * p2;
  circulo * p3;
  circulo(double r) {
    iteracion = 0;
    R = r;
    p1 = NULL;
    p2 = NULL;
    p3 = NULL;
  }
  circulo(circulo * pp1, circulo * pp2, circulo * pp3, int iter) {
    iteracion = iter;
    double k1 = 1./pp1->R;
    double k2 = 1./pp2->R;
    double k3 = 1./pp3->R;
    double c1 = 2.*sqrt(k1*k2 + k2*k3 + k1*k3);
    double c2 = k1 + k2 + k3;
    double R1 = 2;
    if((c1 - c2) != 0)
      R1 = 1./(c1 - c2);
    double R2 = 1./(c1 + c2);
    R = 5;
    if(R1 > 0)
      R = R1;
    if((R2 > 0) && (R2 < R))
      R = R2;
    p1 = pp1;
    p2 = pp2;
    p3 = pp3;
  //  cout << "Nuevo circulo, radio = " << R << " desde " << R1 << " , " << R2 << endl;
  }
};

int main() {
    queue<circulo *> circulos;
  //  set<circulo *> finales;
    double radiogrande = 1. + 2.*sqrt(3.)/3.;
    circulo * inicio1 = new circulo(-radiogrande);
    circulo * inicio2 = new circulo(1.);
    circulo * inicio3 = new circulo(1.);
    circulo * inicio4 = new circulo(1.);
  //  finales.insert(inicio2);
  //  finales.insert(inicio3);
  //  finales.insert(inicio4);
  
    double area = 3;
    
    cout << radiogrande << endl;
    
    circulo * inicio11 = new circulo(inicio1, inicio2, inicio3, 1);
    circulo * inicio12 = new circulo(inicio1, inicio3, inicio4, 1);
    circulo * inicio13 = new circulo(inicio1, inicio2, inicio4, 1);
    circulo * inicio14 = new circulo(inicio2, inicio3, inicio4, 1);
    circulos.push(inicio11);
    circulos.push(inicio12);
    circulos.push(inicio13);
    circulos.push(inicio14);
    while(!circulos.empty()) {
      circulo * actual = circulos.front();
      circulos.pop();
      area += actual->R*actual->R;
      if(actual->iteracion != 10) {
        circulo * nuevo1 = new circulo(actual, actual->p1, actual->p2, actual->iteracion + 1);
        circulo * nuevo2 = new circulo(actual, actual->p2, actual->p3, actual->iteracion + 1);
        circulo * nuevo3 = new circulo(actual, actual->p1, actual->p3, actual->iteracion + 1);
        circulos.push(nuevo1);
        circulos.push(nuevo2);
        circulos.push(nuevo3);
      }
    }
    cout << area << " de " << (radiogrande*radiogrande) << endl;
    double total = 1. - area/(radiogrande*radiogrande);
    printf("%1.8f\n", total);
//    cout << total << endl;
	return 0;
}
