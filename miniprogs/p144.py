"""Problem 144."""


def dot(a, b):
  return a[0] * b[0] + a[1] * b[1]


def norm(vect):
  return vect
  nn = dot(vect, vect) ** 0.5
  return [vect[0] / nn, vect[1] / nn]


class P144(object):
  """Problem 144."""

  def __init__(self):
    self._last_n = 0
    self._p = [1.4, -9.6]
    self._d = [1.4, -9.6 - 10.1]
    #self._p = [0., -10.]
    #self._d = [0., -1.]

  def iter_one(self):
    up = 4. * self._p[0] * self._d[0] + self._p[1] * self._d[1]
    down = 16. * self._p[0] * self._p[0] + self._p[1] * self._p[1]
    factor = 2. * up / down
    #if self._last_n < 10:
    #  print factor
    self._d = [self._d[0] - factor * 4. * self._p[0],
               self._d[1] - factor * self._p[1]]
    #print 'd = %s' % self._d
    #print 'p = %s' % self._p
    other_factor = ((2. * (4. * self._p[0] * self._d[0] + self._p[1] * self._d[1]))
                    / (4. * self._d[0] * self._d[0] + self._d[1] * self._d[1]))
    #print '%s' % other_factor
    #print '%s' % self._p
    self._p = [self._p[0] - other_factor * self._d[0],
               self._p[1] - other_factor * self._d[1]]
    #print '%s' % self._p

  def print_state(self):
    print 'After %s impacts: p = %s d = %s' % (self._last_n, self._p, self._d)
    #print 'Safety check 4 px^2 + py^2 = %s' % (self._p[0] * self._p[0] * 4. + self._p[1] * self._p[1])
  
  def main(self):
    while True:
      if -0.01 <= self._p[0] <= 0.01 and self._p[1] > 0:
        print 'It went out after %s impacts!' % self._last_n
        break
      self._last_n += 1
      if self._last_n <= 10 or self._last_n % 1000000 == 0:
        self.print_state()
      self.iter_one()


if __name__ == '__main__':
  P144().main()

