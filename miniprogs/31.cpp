#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

int *valornivel;
int valoractual;
int contadas;

void recur(int nivel) {
    if(nivel == 8) {
        if(valoractual == 200)
            contadas++;
        return;
    }
    for(int i = 0; i <= (200 - valoractual)/valornivel[nivel]; i++) {
        valoractual += i*valornivel[nivel];
        recur(nivel + 1);
        valoractual -= i*valornivel[nivel];
    }
    return;
}

int main() {
    valornivel = new int[8];
    valornivel[0] = 1;
    valornivel[1] = 2;
    valornivel[2] = 5;
    valornivel[3] = 10;
    valornivel[4] = 20;
    valornivel[5] = 50;
    valornivel[6] = 100;
    valornivel[7] = 200;
    valoractual = 0;
    contadas = 0;
    recur(0);
    std::cout << contadas << std::endl;
	return 0;
}
