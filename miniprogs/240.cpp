#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long cantmanerasC[241][21][13][13];

unsigned long long calcular(unsigned long long cuantas, unsigned long long nd, unsigned long long nm, unsigned long long nmin) {
    if(nd <= 0)
      return 0;
    if(nd == 1) {
      if(cuantas >= nmin && cuantas <= nm)
        return 1;
      return 0;
    }
    if(cuantas < nd*nmin)
      return 0;
    unsigned long long res = 0;
    for(unsigned long long i = nmin; i <= nm && i < cuantas; i++) {
      //cout << "agregando " << i << " = [" << (cuantas - i) << "][" << (nd - 1) << "]" << endl;
        res = res + cantmanerasC[cuantas - i][nd - 1][nm][nmin];
    }
    return res;
}

int main() {

    unsigned long long maxvalor = 6;
    unsigned long long cant_dados = 5;

    unsigned long long suma_buscada = 15;
    unsigned long long dados_max = 3;
    
    for(unsigned long long mm = 1; mm <= maxvalor; mm++) {
    for(unsigned long long k = 1; k <= maxvalor; k++) {
      for(unsigned long long j = 1; j <= cant_dados; j++) {
        for(unsigned long long i = 0; i <= maxvalor*cant_dados; i++) {
          unsigned long long res = calcular(i, j, k, mm);
          cout << i << " , " << j << " , de " << mm << " a " << k << " -> " << res << endl;
          cantmanerasC[i][j][k][mm] = res;
        }
      }
    }
    }
    
    unsigned long long tot_ganadas = 0;
    for(unsigned long long j = maxvalor; j >= 1; j--) {
      unsigned long long tot_aux = cantmanerasC[suma_buscada][dados_max][maxvalor][j]; // min j, hasta 12
      if(j != maxvalor)
        tot_aux -= cantmanerasC[suma_buscada][dados_max][maxvalor][j+1];
      unsigned long long tot_mult = 0;
      for(unsigned long long k = 1; k <= maxvalor*(cant_dados - dados_max); k++) {
        tot_mult += cantmanerasC[k][cant_dados - dados_max][j][1]; // min 1, hasta j
      }
      tot_ganadas += tot_aux*tot_mult;
    }
    cout << tot_ganadas << endl;
   cout << "FIN" << endl;
	return 0;
}
