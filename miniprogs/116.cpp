#include <iostream>
#include <cmath>

using namespace std;

#define SIZE  55
#define SIZE1 56

unsigned long long pascal[SIZE][SIZE1];

void llenar() {
    for(int i = 0; i < SIZE; i++)
        for(int j = 0; j < SIZE1; j++)
            pascal[i][j] = 0;
    pascal[0][0] = 1;
    for(int i = 1; i < SIZE; i++) {
        for(int j = 0; j <= i; j++) {
            if(j == 0) {
                pascal[i][j] = 1;
                continue;
            }
            pascal[i][j] = pascal[i - 1][j] + pascal[i - 1][j - 1];
        }
    }
}

int main() {
    llenar();
    unsigned long long tamtotal = 50;
    unsigned long long negros;
    unsigned long long canttotal = 0;
    unsigned long long rojos = 1;
    unsigned long long cantrojos = 0;
    while(rojos*2 <= tamtotal) {
        negros = tamtotal - 2*rojos;
        unsigned long long num = pascal[negros + rojos][rojos];
/*        for(int i = negros + rojos; i > negros; i--)
            num *= i;
        for(int i = rojos; i > 1; i--)
            num /= i;*/
        cantrojos += num;
        cout << "rojos " << rojos << " -> " << num << endl;
        rojos++;
    }
    canttotal += cantrojos;

    rojos = 1;
    cantrojos = 0;
    while(rojos*3 <= tamtotal) {
        negros = tamtotal - 3*rojos;
        unsigned long long num = pascal[negros + rojos][rojos];
/*        for(int i = negros + rojos; i > negros; i--)
            num *= i;
        for(int i = rojos; i > 1; i--)
            num /= i;*/
        cantrojos += num;
        cout << "verdes " << rojos << " -> " << num << endl;
        rojos++;
    }
    canttotal += cantrojos;

    rojos = 1;
    cantrojos = 0;
    while(rojos*4 <= tamtotal) {
        negros = tamtotal - 4*rojos;
        unsigned long long num = pascal[negros + rojos][rojos];
/*        for(int i = negros + rojos; i > negros; i--)
            num *= i;
        for(int i = rojos; i > 1; i--)
            num /= i;*/
        cantrojos += num;
        cout << "azules " << rojos << " -> " << num << endl;
        rojos++;
    }
    canttotal += cantrojos;

    cout << canttotal << endl;    
	return 0;
}
