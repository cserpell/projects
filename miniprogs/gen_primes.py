PRIMES = [2]


def init_primes(max_p):
  candidate = 3
  while candidate <= max_p:
    is_prime = True
    i = 0
    sqrt_candidate = int(candidate ** 0.5)
    while i < len(PRIMES) and PRIMES[i] <= sqrt_candidate:
      if candidate % PRIMES[i] == 0:
        is_prime = False
        break
      i += 1
    if is_prime:
      PRIMES.append(candidate)
    candidate += 2


def print_primes():
  print('PRIMES = [' + (','.join(str(prime) for prime in PRIMES)) + ']')


def main():
  init_primes(100000000)
  print_primes()


if __name__ == '__main__':
  main()

