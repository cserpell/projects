import decimal

decimal.getcontext().prec = 20

MAX_N = 1000000L


def calc(n):
  last_2 = decimal.Decimal(1) / 2
  last_1 = decimal.Decimal(2) / 3
  nn = 4L
  while nn <= n:
    next = (last_1 * (nn - 1) * (nn - 4) + 2 * last_2 * (nn - 2)) / (nn * (nn - 3))
    last_2 = last_1
    last_1 = next
    nn += 1L
  return last_1


def main():
  print calc(MAX_N)


if __name__ == '__main__':
  main()

