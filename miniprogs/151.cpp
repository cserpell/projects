#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>

using namespace std;

class hoja {
 public:
  int size;
  int id;
  hoja(int siz, int i) {
    size = siz;
    id = i;
  }
  int operator<(const hoja &otra) {
    return id < otra.id;
  }
};

vector<hoja> todas;

int hojas[5];
int total_hojas;
int unasola;

void sacar_una() {
  if(total_hojas == 1) {
 //   cout << "Quedaba una ..." << endl;
    unasola++;
  }
  int pos = ((double)rand()/RAND_MAX)*total_hojas;
  int pactual = 0;
  int tothasta = 0;
  for(int i = 0; i < 5; i++) {
    if(hojas[i] + tothasta > pos) {
      pactual = i;
      break;
    }
    tothasta = tothasta + hojas[i];
  }
  hojas[pactual]--;
  total_hojas--;
//  cout << "Sacando tamagno " << pactual << endl;
  if(pactual == 4)
    return;
  for(int i = pactual + 1; i < 5; i++) {
    hojas[i]++;
    total_hojas++;
  }
}

int itera1() {
  hojas[0] = 1;
  hojas[1] = 0;
  hojas[2] = 0;
  hojas[3] = 0;
  hojas[4] = 0;
  total_hojas = 1;
  unasola = 0;
  for(int i = 0; i < 16; i++)
    sacar_una();
  return unasola - 2;
}

int main(int argc, char *argv[]) {
    unsigned long long an = 0;
    int total_i = 10000000;
    if(argc > 1)
      total_i = atoi(argv[1]);
    int paso = total_i/100;
    int apaso = paso;
    for(int i = 0; i < total_i; i++) {
      if(i >= apaso) {
        cout << i << endl;
        apaso += paso;
      }
      int aa = itera1();
      if(aa < 0) {
        cout << "Error, menor que 0" << endl;
        aa = 0;
      }
      an = an + aa;
     // cout << aa << endl;
    }
    printf("Final : %1.7f\n", (((double)an)/total_i));
    return 0;
}
