#include <iostream>
#include <cmath>

int main() {
	int sonono[1000000];
	int primo = 3;
	unsigned long long maxprimo = 2;
	unsigned long long cantprimos = 2;
	int cantsaltos = 0;
	for(int i = 0; i < 1000000; i++)
		sonono[i] = 0;
	while(primo < 1000000) {
		int noes = 0;
		for(int i = 3; i <= sqrt(primo) + 3; i += 2) {
			if(sonono[i] && primo % i == 0) {
				noes = 1;
				primo += 2;
				break;
			}
		}
		if(noes)
			continue;
		sonono[primo] = 1;
		cantprimos += (unsigned int)primo;
		if(cantprimos < maxprimo) {
			cantsaltos++;
		}
		maxprimo = cantprimos;
		primo += 2;
	}
	std::cout << cantprimos << " " << cantsaltos << std::endl;
}

