import java.util.*;
import java.io.*;
import java.math.*;
import java.lang.*;

class c230 {
    public BigInteger[] Fibs;
    public int nfibs;
    public String A;
    public String B;
    public BigInteger slen;
    public BigInteger slen2;
    public c230(int n, String s1, String s2) {
      Fibs = new BigInteger[n + 1];
      Fibs[0] = BigInteger.valueOf(0);
      Fibs[1] = BigInteger.valueOf(1);
      for(int i = 2; i <= n; i++)
        Fibs[i] = Fibs[i-1].add(Fibs[i-2]);
      nfibs = n;
      A = s1;
      B = s2;
      slen = BigInteger.valueOf(A.length());
      slen2 = slen.multiply(BigInteger.valueOf(2));
      if(A.length() != B.length()) {
        System.err.println("ERROR largos distintos");
        System.exit(0);
      }
    }
    
    public char search_digit_from_level(BigInteger pos, int level) {
      if(Fibs[level].multiply(slen).compareTo(pos) < 0) {
        System.err.println("ERROR llamada invalida " + level + " : " + pos);
        System.err.println("ERROR " + Fibs[level]);
        System.exit(0);
      }
      if(level == 1)
        return A.charAt(pos.intValue() - 1);
      if(level == 2)
        return B.charAt(pos.intValue() - 1);
      if(pos.compareTo(Fibs[level - 2].multiply(slen)) > 0)
        return search_digit_from_level(pos.subtract(Fibs[level - 2].multiply(slen)), level - 1);
      return search_digit_from_level(pos, level - 2);
    }
    public char search_digit(BigInteger pos) {
      if(pos.compareTo(slen2) <= 0) {
        if(pos.compareTo(slen) <= 0)
          return A.charAt(pos.intValue() - 1);
        return B.charAt(pos.intValue() - slen.intValue() - 1);
      }
      int i = 4;
      for(; i <= nfibs; i++) {
        // Ve si pos cabe aqui
        if(Fibs[i].multiply(slen).compareTo(pos) >= 0)
          break;
      }
      if(Fibs[i].multiply(slen).compareTo(pos) < 0) {
        System.err.println("ERROR se necesitan fibonaccis mas largos");
        System.exit(0);
      }
      return search_digit_from_level(pos, i);
    }

    static public void main(String[] args) throws IOException {
      String AA = "1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679";
      String BB = "8214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196";
//      String AA = "1415926535";
  //    String BB = "8979323846";
      c230 cc = new c230(200, AA, BB);
    
    //System.out.println(cc.search_digit(BigInteger.valueOf(35)));

      char[] res = new char[20];
       for(int i = 0; i <= 17; i++) {
            BigInteger nuevo = BigInteger.valueOf(i).multiply(BigInteger.valueOf(19)).add(BigInteger.valueOf(127)).multiply(BigInteger.valueOf(7).pow(i));
            res[i] = cc.search_digit(nuevo);
            System.out.println(res[i]);
        }
        for(int i = 17; i >= 0; i--) {
          System.out.print(res[i]);
        }
        System.out.println("");
    }
}
