#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int main() {
    unsigned long long i;
    for(i = 3; i < 1000000; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    unsigned long long suma = 0;
    for(unsigned long long val = 6; val < 1000000; val++) {
        if(sidivide(val)) {
            suma += val;
            cout << val << "  Suma parcial = " << suma << endl;
        }
    }
    cout << "Suma = " << suma << endl;
	return 0;
}
