"""Problem 387."""
import collections
import copy


def check_prime(number):
  if number == 2:
    return True
  if number % 2 == 0:
    return False
  i = 3
  while i * i <= number:
    if number % i == 0:
      return False
    i += 2
  return True


class P387(object):
  """Problem 387."""

  def __init__(self):
    self._last_length = 1
    #self._lists = [(0, 0)]
    #self._last_strongs = []
    #self._last_primes = []
    self._sum_of_primes = 0
    self._lists = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9)]
    self._last_strongs = set()
    self._last_primes = []

  def make_next(self):
    next_list = []
    next_strongs = set()
    next_primes = []
    for number, number_sum in self._lists:
      for new_digit in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
        candidate = number * 10 + new_digit
        candidate_sum = number_sum + new_digit
        if number in self._last_strongs and check_prime(candidate):
          next_primes.append(candidate)
        if candidate != 0 and candidate % candidate_sum == 0:  # Es divisible
          next_list.append((candidate, candidate_sum))
          if check_prime(candidate / candidate_sum):
            next_strongs.add(candidate)
    self._last_primes = next_primes
    self._sum_of_primes += sum(next_primes)
    self._last_strongs = next_strongs
    self._lists = next_list
    self._last_length += 1

  def print_last(self):
    print 'length ' + str(self._last_length)
    print 'Sum of primes ' + str(self._sum_of_primes)
    print 'Primes ' + str(self._last_primes)
    print 'Strongs ' + str(self._last_strongs)
    print 'List ' + str(self._lists)

  def run(self, length):
    while self._last_length < length:
      self.make_next()
      # self.print_last()
    print 'Sum of primes: ' + str(self._sum_of_primes)


if __name__ == '__main__':
  P387().run(14)

