import sys

NPOINTS = 20

def get_area (points, areas, i, j, k) :
  if areas.has_key( (i, j, k) ) :
    return areas[ (i, j, k) ]
  a = area2(points[i], points[j], points[k])
  areas[(i, j, k)] = a
  areas[(j, k, i)] = a
  areas[(k, i, j)] = a
  areas[(i, k, j)] = -a
  areas[(k, j, i)] = -a
  areas[(j, i, k)] = -a
  return a

if len(sys.argv) > 1 :
  NPOINTS = int(sys.argv[1])

def area2 (p1, p2, p3) :
  ar = p1[0] * p2[1] - p2[0] * p1[1]
  ar = ar + p2[0] * p3[1] - p3[0] * p2[1]
  ar = ar + p3[0] * p1[1] - p1[0] * p3[1]
  return ar

s0 = 290797
s1 = (s0 * s0) % 50515093
s0 = (s1 * s1) % 50515093

i = 1
points = []
while i <= NPOINTS :
  t0 = ( s1 % 2000 ) - 1000
  t1 = ( s0 % 2000 ) - 1000
  points.append( ( t0 , t1 ) )
  s1 = (s0 * s0) % 50515093
  s0 = (s1 * s1) % 50515093
  i = i + 1

# Here the list of points is ready
for p in points :
  print str(p)

# Compute all areas at once
areas = {}

# Now, a list of admisible triangles!

triangles = []

added_points = []


i = 0
while i < NPOINTS :
  j = i + 1
  print j
  while j < NPOINTS :
    k = j + 1
    #print k
    while k < NPOINTS :
      admissible = True
      o = 0
      while o < NPOINTS :
        if o != i and o != j and o != k :
          # Check whether the point is inside the triangle or not
          sminus = 0
          splus = 0
          slin = 0
          a = get_area( points, areas, i, j, o )
          if a > 0 :
            splus = splus + 1
          elif a < 0 :
            sminus = sminus + 1
          else :
            slin = 0
          a = get_area( points, areas, j, k, o )
          if a > 0 :
            splus = splus + 1
          elif a < 0 :
            sminus = sminus + 1
          else :
            slin = 0
          a = get_area( points, areas, k, i, o )
          if a > 0 :
            splus = splus + 1
          elif a < 0 :
            sminus = sminus + 1
          else :
            slin = 0
          if sminus == 0 or splus == 0 :
            admissible = False
            break
        o = o + 1
      if admissible :
        a = get_area( points, areas, i, j, k )
        if a < 0 :
          triangles.append( ( i , k , j ) )
        elif a >= 0 :
          triangles.append( ( i , j , k ) )
      k = k + 1
    j = j + 1
  i = i + 1

for t in triangles :
  print str(points[t[0]]) + " - " + str(points[t[1]]) + " - " + str(points[t[2]]) + " - " + str(areas[ ( t[0], t[1], t[2] ) ])

print "Admissible triangles: " + str(len(triangles))
