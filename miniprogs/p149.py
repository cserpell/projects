"""Problem 149."""


def next_pseudo(current_nums, pos_k):
    if pos_k <= 55:
        return ((100003 - 200003 * pos_k + 300007 * pos_k * pos_k * pos_k) % 1000000) - 500000
    return ((current_nums[pos_k - 24 - 1] +
             current_nums[pos_k - 55 - 1] + 1000000) % 1000000) - 500000


def fill_nums(max_k):
    current_nums = []
    for pos_k in range(1, max_k + 1):
        current_nums.append(next_pseudo(current_nums, pos_k))
    return current_nums


def get_max(table, wid):
    max_sum = 0
    for pos in range(wid):
        for start in range(wid):
            if table[start][pos] < 0:
                continue
            the_sum = 0
            for end in range(start, wid):
                the_sum += table[end][pos]
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(wid):
            if table[pos][start] < 0:
                continue
            the_sum = 0
            for end in range(start, wid):
                the_sum += table[pos][end]
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(0, wid - pos):
            if table[start + pos][start] < 0:
                continue
            the_sum = 0
            for end in range(start, wid - pos):
                the_sum += table[end + pos][end]
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(0, wid - pos):
            if table[start][start + pos] < 0:
                continue
            the_sum = 0
            for end in range(start, wid - pos):
                the_sum += table[end][end + pos]
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(0, pos + 1):
            if table[pos - start][start] < 0:
                continue
            the_sum = 0
            for end in range(start, pos + 1):
                the_sum += table[pos - end][end]
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(0, wid - pos):
            if table[wid - start - 1][pos + start] < 0:
                continue
            the_sum = 0
            for end in range(start, wid - pos):
                the_sum += table[wid - end - 1][pos + end]
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    print(max_sum)
    return max_sum


def get_max_old(table, wid):
    max_sum = 0
    for pos in range(wid):
        for start in range(wid):
            for end in range(start, wid):
                the_sum = sum([table[y][pos] for y in range(start, end + 1)])
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(wid):
            for end in range(start, wid):
                the_sum = sum([table[pos][x] for x in range(start, end + 1)])
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(0, wid - pos):
            for end in range(start, wid - pos):
                the_sum = sum([table[x + pos][x] for x in range(start, end + 1)])
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(0, wid - pos):
            for end in range(start, wid - pos):
                the_sum = sum([table[x][x + pos] for x in range(start, end + 1)])
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(0, pos + 1):
            for end in range(start, pos + 1):
                the_sum = sum([table[pos - x][x] for x in range(start, end + 1)])
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    for pos in range(wid):
        for start in range(0, wid - pos):
            for end in range(start, wid - pos):
                the_sum = sum([table[wid - x - 1][pos + x] for x in range(start, end + 1)])
                if the_sum > max_sum:
                    max_sum = the_sum
                    print(f'so far {max_sum}')
    print(max_sum)
    return max_sum


def main():
    get_max([[-2, 5, 3, 2],
             [9, -6, 5, 1],
             [3, 2, 7, 3],
             [-1, 8, -4, 8]], 4)
    wid = 2000
    nums = fill_nums(wid * wid)
    print(nums[10 - 1])
    print(nums[100 - 1])
    table = [[nums[x + y * wid] for x in range(wid)] for y in range(wid)]
    get_max(table, wid)


if __name__ == '__main__':
    main()

