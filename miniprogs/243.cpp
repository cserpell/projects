#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if((n % (*it)) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

unsigned long long canttotal;

void test(unsigned long long hastaahora, unsigned long long num, unsigned long long np, enteros & actual) {
  if(hastaahora >= num) {
    return;
  }
  if(np >= num) {
    canttotal++;
  //  cout << hastaahora << ",";
    return;
  }
  unsigned long long nuevonum = hastaahora;
  enteros::iterator it2 = actual.find(np);
  it2++;
  while(nuevonum < num) {
    test(nuevonum, num, *it2, actual);
    nuevonum *= np;
  }
}

int main() {
    enteros primos;

    for(unsigned long long i = 3; i < 100000; i += 2) {
        isPrime(primos, i);
    }
    primos.insert(2);
    cout << "Fin llenado de primos" << endl;
    unsigned long long num = 2;
    while(num < 100000) {
      if(primos.find(num) != primos.end()) {
        num++;
        continue;
      }
      cout << "Probando " << num << endl;
      enteros actual;
      for(enteros::iterator it = primos.begin(); it != primos.end(); it++) {
        if((*it) >= num)
          break;
        if((num % (*it)) != 0)
          actual.insert(*it);
      }
      actual.insert(num);
      canttotal = 0;
      test(1, num, (*(actual.begin())), actual);
   //   cout << endl;
      double frac = ((double)canttotal)/(num - 1);
    //  cout << frac << endl;
  //    if(frac < 4./10) {
      if(frac < 15499./94744) { //0.163588196
        break;
      }
      num++;
    }
    cout << num << endl;
    return 0;
}
