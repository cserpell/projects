#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>

using namespace std;

int maneras[1000002];

int main(int argc, char *argv[]) {
    unsigned long long cuantos = 0;
    unsigned long long n = 3;
    unsigned long long tot_buscado = 1000000;
    if(argc > 1)
      tot_buscado = atoi(argv[1]);
    for(int i = 0; i <= tot_buscado; i++)
      maneras[i] = 0;
    for(; n <= tot_buscado/2; n++) {
  //    unsigned long long mink = 1;
     // unsigned long long maxk = n/2;
   //   if((n % 2) == 0)
 //       maxk--;
      //if(n*n > tot_buscado) {
    //    maxk = (n - ceil(sqrt(n*n - tot_buscado)))/2;
  //    }
//      unsigned long long tot = maxk - mink + 1;
     // cout << tot << endl;
   //   cuantos += tot;
 //     continue;
      unsigned long long m = 1;
      if((n % 2) == 0)
        m = 2;
      for(; m < n; m += 2) {
        unsigned long long tiles = n*n - m*m;
        if(tiles <= tot_buscado) {
          maneras[tiles]++;
          cuantos++;
        }
      }
    }
    int ultimo[16];
    for(int i = 0; i <= tot_buscado; i++) {
      if(maneras[i] <= 15) {
        ultimo[maneras[i]]++;
      }
    }
    for(int i = 0; i <= 15; i++) {
      cout << i << ": " << ultimo[i] << endl;
    }
    cout << cuantos << endl;
    return 0;
}
