

class P305:
  def __init__(self):
    self._seq = []
    self._next_n = 1
    self._current_pos = 0L
    self._cut_buf = 1000000

  def more_seq(self):
    rep = str(self._next_n)
    self._seq += [r for r in rep]
    self._next_n += 1

  def cut_seq(self, up_to):
    del self._seq[0:up_to]
    self._current_pos += up_to

  def next_ocur(self, n, pos):
    rep = str(n)
    initial = 0
    for i in range(1, len(rep)):
      if rep[i] == rep[0]:
        initial = i
        break
    seq_pos = pos - self._current_pos
    rep_pos = 0
    while rep_pos < len(rep):
      #print 'R %s S %s' % (rep_pos, seq_pos)
      if seq_pos >= len(self._seq):
        self.more_seq()
        continue
      if rep[rep_pos] == self._seq[seq_pos]:
        rep_pos += 1
        seq_pos += 1
        continue
      if initial:
        seq_pos -= rep_pos - initial
      else:
        seq_pos -= rep_pos - 1
      rep_pos = 0
      if seq_pos > self._cut_buf:
        self.cut_seq(self._cut_buf)
        seq_pos -= self._cut_buf
    return self._current_pos + seq_pos - rep_pos + 1


def count(n):
  a = P305()
  pos = 0L
  for i in range(n):
    pos = a.next_ocur(n, pos)
  print 'f(%s) = %s' % (n, pos)
  return pos


def main():
  count(1)
  count(5)
  count(12)
  count(7780)
  s = 0
  for i in range(1, 14):
    s += count(3 ** i)
  print s


if __name__ == '__main__':
  main()
