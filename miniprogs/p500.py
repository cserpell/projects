print 'Importing primes'
import all_primes
print 'Ready'


# Numero encontrado: 203888087


def bin_search(tuples_list, arg_pos, value, first_pos=None, last_pos=None):
    """Search over a list of tuples for last value equal or under given value.
    @param tuples_list: list of tuples ordered by values in arg_pos index
    @param arg_pos: index in tuple to consider as order
    @param value: it returns last index of list which value is less or equal
    @param first_pos: optional starting pos to search, default 0
    @param last_pos: optional ending pos to search, default last list element
    """
    if first_pos is None:
        first_pos = 0
    if last_pos is None:
        last_pos = len(tuples_list) - 1
    first = first_pos
    last = last_pos
    while first < last:
        mid_pos = (first + last) / 2
        mid_value = tuples_list[mid_pos][arg_pos]
        if mid_value < value:
            first = mid_pos + 1
        elif mid_value > value:
            last = mid_pos - 1
        elif mid_value == value:
            return mid_pos
    if first_pos <= last <= last_pos:
        if tuples_list[last][arg_pos] > value:
            last -= 1
    if first_pos <= last <= last_pos:
        return last
    return -1


class P500(object):

  def __init__(self):
    self.divisors = 500500
    self.included = []
    self.candidates = [(prime, prime)
                       for prime in all_primes.PRIMES[0:self.divisors]]
    self.modulo = 500500507L

  def m_mod(self, one, two):
    return (one * two) % self.modulo

  def insert_candidate(self, candidate, base):
    p = bin_search(self.candidates, 0, candidate)
    self.candidates = (self.candidates[0:(p + 1)] + [(candidate, base)] +
                       self.candidates[(p + 1):])

  def main(self):
    total = 0L
    number = 1L
    while total < self.divisors:
      # Take first in self.candidates
      first, base = self.candidates[0]
      self.candidates = self.candidates[1:]
      new_candidate = self.m_mod(first, base)
      number = self.m_mod(number, first)
      total += 1L
      self.insert_candidate(new_candidate, base)
      print total
    print number


if __name__ == '__main__':
  p = P500()
  p.main()

