#include <iostream>
#include <cmath>
#include <set>

using namespace std;

int wires[1000001];

unsigned long long mcd(unsigned long long a, unsigned long long b) {
    while(b != 0) {
        unsigned long long aux = a % b;
        a = b;
        b = aux;
    }
    return a;
}

int main() {
    for(int i = 0; i <= 1000000; i++) {
        wires[i] = 0;
    }
    for(unsigned long long m = 1; m <= 10000; m++) {
        for(unsigned long long n = 1; n < m; n++) {
            unsigned long long a = m*m - n*n;
            unsigned long long b = 2*m*n;
            unsigned long long c = m*m + n*n;
            unsigned long long per = a + b + c;
            if(per > 1000000)
                break;
            if(mcd(m, n) != 1)
                continue;
            if((m % 2) != 0 && (n % 2) != 0)
                continue;
            for(int i = 1; i < 1000000; i++) {
                unsigned long long na = i*a;
                unsigned long long nb = i*b;
                unsigned long long nc = i*c;
                unsigned long long nper = i*per;
                if(nper > 1000000)
                    break;
                wires[nper]++;
            }
        }
    }
    int cantidad = 0;
    for(int i = 1; i <= 1000000; i++) {
        if(wires[i] == 1) {
            cantidad++;
        }
    }
    cout << cantidad << endl;
    return 0;
}
