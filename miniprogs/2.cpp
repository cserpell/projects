#include <iostream>

int main() {
	int sonono[1000000];
	int primo = 3;
	int maxprimo = 0;
	int cantprimos = 1;
	for(int i = 0; i < 1000000; i++)
		sonono[i] = 0;
	while(cantprimos < 10001) {
		int noes = 0;
		for(int i = 3; i <= maxprimo; i++) {
			if(sonono[i] && primo % i == 0) {
				noes = 1;
				primo += 2;
				break;
			}
		}
		if(noes)
			continue;
		sonono[primo] = 1;
		maxprimo = primo;
		cantprimos++;
		primo += 2;
	}
	std::cout << (primo - 2) << std::endl;
}

