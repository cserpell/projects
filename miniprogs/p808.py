"""Problem 800."""
import itertools
import math

import all_primes


PRIMES_SET = set(all_primes.PRIMES)
SQ_PRIMES_SET = set(prime * prime for prime in all_primes.PRIMES)
COMPUTED_DIGS = {0: [0]}
TENS = [10 ** i for i in range(20)]  # int(math.log10(all_primes.PRIMES[-1])) + 1)]


def get_digs(num, min_len=1):
    if num not in COMPUTED_DIGS:
        lll = int(math.log10(num))
        if lll == 0:
            COMPUTED_DIGS[num] = [num]
        else:
            half = lll // 2
            if half != lll:
                # print(f'Dividing by {TENS[half + 1]}')
                COMPUTED_DIGS[num] = get_digs(num // TENS[half + 1], min_len=min_len - half - 1) + get_digs(num % TENS[half + 1], min_len=half + 1)
            else:
                digs = []
                while num > 0:
                    rem = num % 10
                    digs.insert(0, rem)
                    num = (num - rem) // 10
                COMPUTED_DIGS[num] = digs
    return [0 for _ in range(min_len - len(COMPUTED_DIGS[num]))] + COMPUTED_DIGS[num]


def main(big_n):
    """Main execution point."""
    the_sum = 0
    num_found = 0
    for prime in all_primes.PRIMES:
        square = prime * prime
        digs = get_digs(square)
        rdigs = list(reversed(digs))
        ldigs = len(digs)
        check_palin = True
        for pos in range(ldigs):
            if digs[pos] != rdigs[pos]:
                check_palin = False
                break
        if check_palin:
            continue
        new_num = sum(TENS[ldigs - pos - 1] * rdigs[pos] for pos in range(ldigs))
        if new_num not in SQ_PRIMES_SET:
            continue
        print(f'Found a reversible prime: {prime} -> {square} -> {new_num}')
        the_sum += square
        num_found += 1
        if num_found == big_n:
            break
    print(the_sum)


if __name__ == '__main__':
    main(50)

