#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int sidivide(unsigned long long numero) {
    if(numero % 2 == 0)
        return 0;
    if(numero % 5 == 0)
        return 0;
    if(primos.find(numero) != primos.end())
        return 0;
    unsigned long long repeticiones = 1;
    unsigned long long resto = 1;
    enteros lista;
    while(resto != 0) {
        while(resto < numero) {
        /*    if(lista.find(resto) != lista.end())
                return 0; */ // Se supone q no ocurre ...
            lista.insert(resto);
            resto = resto*10 + 1;
            repeticiones++;
        }
        resto = resto % numero;
    }
    if((numero - 1) % repeticiones == 0)
        return 1;
    return 0;
}

int main() {
    unsigned long long i;
    for(i = 3; i < 1000000; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    unsigned long long suma = 0;
    for(unsigned long long val = 6; val < 1000000; val++) {
        if(sidivide(val)) {
            suma += val;
            cout << val << "  Suma parcial = " << suma << endl;
        }
    }
    cout << "Suma = " << suma << endl;
	return 0;
}
