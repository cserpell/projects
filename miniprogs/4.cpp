#include <iostream>
#include <sstream>

int main() {
	int maximo = 0;
	for(int i = 100; i < 1000; i++) {
		for(int j = 100; j < 1000; j++) {
			int res = i*j;
			std::ostringstream str;
			str << res;
			std::string s = str.str();
			int noes = 0;
			for(int k = 0; k < s.length()/2; k++) {
				if(s.c_str()[k] != s.c_str()[s.length() 
- k - 1]) {
					noes = 1;
					break;
				}
			}
			if(noes)
				continue;

			if(res > maximo) {
				maximo = res;
			}
		}
	}
	std::cout << maximo << std::endl;
	return 0;
}
