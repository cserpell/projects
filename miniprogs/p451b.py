import all_primes

REPORT_STEP = 100000
PRIMES_SET = set()


def init_primes():
  for prime in all_primes.PRIMES:
    PRIMES_SET.add(prime)


def factor(number):
  result = {}
  i = 0
  while i < len(all_primes.PRIMES) and number > 1:
    if number in PRIMES_SET:
      if number not in result:
        result[number] = 0
      result[number] += 1
      break
    if number % all_primes.PRIMES[i] == 0:
      if all_primes.PRIMES[i] not in result:
        result[all_primes.PRIMES[i]] = 0
      result[all_primes.PRIMES[i]] += 1
      number = number / all_primes.PRIMES[i]
      continue
    i += 1
  return result


def mcd(a, b):
  # Here a >= b
  if b == 1:
    return 1
  m = a / b
  c = m * b
  if c == a:
    return b
  return mcd(b, a - c)


def l(n):
  m = int((n + 1) ** 0.5)
  while m <= n / 2:
    if mcd(n, m) == 1 and ((m * m) % n) == 1:
      return n - m
    m += 1
  return 1


def l2(n):
  fact = factor(n)
  max_div = max(fact.keys())
  if max_div == n:
    return 1
  max_div = max_div ** fact[max_div]
  ma = max_div
  while ma < n:
    m1 = ma - 1
    m2 = ma + 1
    if ((m1 * m1) % n) == 1:
      return n - m1
    if ((m2 * m2) % n) == 1:
      return n - m2
    ma += max_div
  return 1


def ss(n):
  s = 0
  m = 3
  while m <= n:
    if (m % REPORT_STEP) == 0:
      print m
    s += l2(m)
    m += 1
  return s


def main():
  init_primes()
  
  print 'l(7) = %s' % l2(7)
  print 'l(15) = %s' % l2(15)
  print 'l(100) = %s' % l2(100)
  print ss(2 * 10000000)


if __name__ == '__main__':
  main()

