import sys

TOPE = 20
if len(sys.argv) > 1 :
  TOPE = int(sys.argv[1])

primes = []

primes.append(2)
np = 3
while np < TOPE :
  thisprime = True
  for p in primes :
    if ( np % p ) == 0 :
      thisprime = False
      break
  if thisprime :
    primes.append(np)
  np = np + 2

used = []
finalnum = 1
for p in primes :
  finalnum = finalnum * p
  used.append(False)

print primes
print finalnum
print len(primes)

remaining = []
remaining.append(finalnum * finalnum)

i = 1
while i < len(primes) :
  remaining.append(remaining[i - 1] / (primes[i - 1] * primes[i - 1]))
  i = i + 1

#print remaining

def probardesdepos ( pos, primes, valactual, finalnum, bestval ) :
  if pos >= len(primes) :
    # Ended, check value
    return bestval
  if pos == 10 :
    print "Height 10"
  # Check if the destination is reachable
  if valactual * remaining[pos] < bestval :
    # Bad, if we choose all remaining primes, we cannot do it
    return bestval
  newvalactual = valactual * primes[pos] * primes[pos]
  if newvalactual <= finalnum :
    # Valid number
    if newvalactual > bestval :
      bestval = newvalactual
      print "New best: " + str(bestval)
      print "Objective:" + str(finalnum)
    nnew1 = probardesdepos ( pos + 1, primes, newvalactual, finalnum, bestval )
    if nnew1 > bestval :
      bestval = nnew1
      print "New best: " + str(bestval)
      print "Objective:" + str(finalnum)
    nnew2 = probardesdepos ( pos + 1, primes, valactual, finalnum, bestval )
    if nnew2 > bestval :
      bestval = nnew2
      print "New best: " + str(bestval)
      print "Objective:" + str(finalnum)
    return bestval
  # Ninguno mas que pruebe podra funcionar, estan ordenados de menor a mayor
  return bestval

val = probardesdepos ( 0 , primes, 1, finalnum, 1)

print "Result: " + str(val)
