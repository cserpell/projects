import decimal
import math

decimal.getcontext().prec = 100
decimal.getcontext().rounding = decimal.ROUND_FLOOR

ONE = decimal.Decimal(1)


def mult(prev_as, fpos, tpos):
    if fpos > tpos:
        return ONE
    ppp = ONE
    for i in range(fpos, tpos + 1):
        ppp *= prev_as[i - 1][0]
    return ppp


def next_a(prev_as, init_b):
    n = len(prev_as) + 1
    second = sum([
        mult(prev_as, p, n - 1) * (prev_as[p - 1][0] - 1)
        for p in range(1, n)])
    first = init_b * mult(prev_as, 1, n - 1)
    num = first - second
    new_a = num.to_integral_value()
    len_a = ONE
    # len_a = math.ceil(math.log10(num + 1))
    return new_a, len_a


def main():
    prev_as = [(decimal.Decimal(2), 1)]
    init_b = decimal.Decimal('2.2235610193135541061731771')
    # init_b = decimal.Decimal('1.1111111111111111111111111')
    # init_b = decimal.Decimal('2.956938891377988')
    for i in range(20):
        new_a, len_a = next_a(prev_as, init_b)
        prev_as.append((new_a, len_a))
        print('Next a: {}'.format(new_a))
        print('number so far: {}.{}'.format(prev_as[0][0], ''.join([str(v[0].to_integral_value()) for v in prev_as[1:]])))
        print('Remember b is: {}'.format(init_b))
        print('Length of b is: .123456789012345678901234')


if __name__ == '__main__':
  main()

