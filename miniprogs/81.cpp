#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

void recur(int nivel, int **mat, int **llenar) {
    if(nivel == 80) {
        return;
    }
    if(nivel == 0) {
        llenar[0][0] = mat[0][0];
        for(int i = 1; i < 80; i++) {
            llenar[i][0] = llenar[i - 1][0] + mat[i][0];
            llenar[0][i] = llenar[0][i - 1] + mat[0][i];
        }
        recur(1, mat, llenar);
        return;
    }
    for(int i = nivel; i < 80; i++) {
        if(llenar[i - 1][nivel] < llenar[i][nivel - 1])
            llenar[i][nivel] = llenar[i - 1][nivel] + mat[i][nivel];
        else
            llenar[i][nivel] = llenar[i][nivel - 1] + mat[i][nivel];
        if(llenar[nivel - 1][i] < llenar[nivel][i - 1])
            llenar[nivel][i] = llenar[nivel - 1][i] + mat[nivel][i];
        else
            llenar[nivel][i] = llenar[nivel][i - 1] + mat[nivel][i];
    }
    recur(nivel + 1, mat, llenar);
}

int main() {
    int **mat = new int *[80];
    int **llenar = new int *[80];
    for(int i = 0; i < 80; i++) {
        mat[i] = new int[80];
        llenar[i] = new int[80];
    }
    for(int i = 0; i < 80; i++) {
        string str;
        cin >> str;
        int upos = -1;
        for(int j = 0; j < 80; j++) {
            int nupos = str.find(",", upos + 1);
            int a = atoi(str.substr(upos + 1, nupos - upos - 1).c_str());
            mat[j][i] = a;
            cout << a << ",";
            upos = nupos;
        }
        cout << endl;
    }
    recur(0, mat, llenar);
    cout << llenar[79][79] << endl;
	return 0;
}
