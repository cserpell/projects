#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int sidivide(unsigned long long numero) {
    unsigned long long repeticiones = 1;
    unsigned long long resto = 1;
    enteros lista;
    while(resto != 0) {
        while(resto < numero) {
            if(lista.find(resto) != lista.end())
                return 0;
            lista.insert(resto);
            resto = resto*10 + 1;
            repeticiones++;
        }
        resto = resto % numero;
    }
    for(enteros::iterator it1 = primos.begin(); it1 != primos.end() && (*it1) <= repeticiones; it1++) {
        if((*it1) == 2 || (*it1) == 5)
            continue;
        if((repeticiones % (*it1)) == 0)
            return 0;
    }
    return 1;
}

int main() {
    unsigned long long i;
    for(i = 3; i < 100000; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    unsigned long long suma = 0;
    enteros::iterator it1 = primos.begin();
    suma += (*it1);
    it1++; // chao con el 2
    suma += (*it1);
    it1++; // chao con el 3
    suma += (*it1);
    it1++; // chao con el 5
    for(; it1 != primos.end(); it1++) {
        if(!sidivide(*it1)) {
            cout << (*it1) << endl;
            suma += (*it1);
            cout << "Suma parcial = " << suma << endl;
        }
    }
    cout << "Suma = " << suma << endl;
	return 0;
}
