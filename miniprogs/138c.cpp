#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long mcd(unsigned long long a, unsigned long long b) {
    while(b != 0) {
        unsigned long long aux = a % b;
        a = b;
        b = aux;
    }
    return a;
}

int main() {
    unsigned long long lastlargo2 = 0;
    unsigned long long lastlargo1 = 16;
    unsigned long long lastc = 17;
    unsigned long long suma = lastc;
    int type = 1;
    for(int i = 0; i < 20; i++) {
        unsigned long long nuevo = lastc*16 + lastlargo2;
        lastlargo2 = lastlargo1;
        lastlargo1 = nuevo;
        lastc =
             sqrt((double)((nuevo/2)*(nuevo/2) + (nuevo + type)*(nuevo + type)));
        suma += lastc;
        cout << (nuevo + type) << " . " << nuevo << " = " <<
            lastc <<  " .. " << suma << endl;
        type *= -1;
    }
    return 0;
}
