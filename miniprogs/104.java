import java.util.*;
import java.io.*;
import java.math.*;

class c104 {
    static public boolean isPandig(BigInteger B) {
        String a = B.toString();
        if(a.length() < 9)
            return false;
        boolean[] ocupado = new boolean[10];
        int p;
        for(int i = 0; i < 9; i++) {
            p = a.charAt(i) - '0';
            if(p == 0)
                return false;
            if(ocupado[p])
                return false;
            
            ocupado[p] = true;
        }
        ocupado = new boolean[10];
        for(int i = 0; i < 9; i++) {
            p = a.charAt(a.length() - i - 1) - '0';
            if(p == 0)
                return false;
            if(ocupado[p])
                return false;
            ocupado[p] = true;
        }
        return true;
    }
    static public void main(String[] args) throws IOException {
        int encual = 2;
        BigInteger F1 = new BigInteger("1");
        BigInteger F2 = new BigInteger("1");
        while(!isPandig(F2)) {
            BigInteger aux = F1.add(F2);
            F1 = F2;
            F2 = aux;
            encual++;
            System.out.println(encual);
        }
        System.out.println(F2);
        System.out.println(encual);
    }
}
