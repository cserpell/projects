import <iostream>

class TreePriorityQueueNode<T, V> {
    public:
        T element;
        V value;
        TreePriorityQueueNode<T, V>* parent;
        TreePriorityQueueNode<T, V>* childA;
        TreePriorityQueueNode<T, V>* childB;
};

class TreePriorityQueue<T, V> {
    private:
        TreePriorityQueueNode<T, V>* root;

        TreePriorityQueueNode<T, V>* getLeftmost(
                TreePriorityQueueNode<T, V>* from) {
            if (from == NULL) {
                return NULL;
            }
            if (from->childA != NULL) {
                return getLeftmost(from->childA);
            }
            return from;
        }
        TreePriorityQueueNode<T, V>* getRightmost(
                TreePriorityQueueNode<T, V>* from) {
            if (from == NULL) {
                return NULL;
            }
            if (from->childB != NULL) {
                return getRightmost(from->childA);
            }
            return from;
        }
    public:
        T getFirst() {
            if (root == NULL) {
                return NULL;
            }
            
        }
};
