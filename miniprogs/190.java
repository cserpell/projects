import java.util.*;
import java.io.*;
import java.math.*;

class c199 {
    static public void main(String[] args) {
        BigInteger lastmult = BigInteger.ONE;
        BigInteger suma = BigInteger.ZERO;
        for(int i = 2; i <= 15; i++) {
            int expon = (i*(i + 1))/2;
            BigInteger multnum = BigInteger.valueOf(2).pow(expon);
            BigInteger multden = BigInteger.valueOf(i + 1).pow(expon);
            lastmult = lastmult.multiply(BigInteger.valueOf(i).pow(i));
            multnum = multnum.multiply(lastmult);
            System.out.println("N: " + multnum);
            System.out.println("D: " + multden);
            BigDecimal bd = new BigDecimal(multnum);
            BigDecimal result = bd.divideToIntegralValue(new BigDecimal(multden));
            System.out.println(result);
            suma = suma.add(result.toBigInteger());
        }
        System.out.println("FIN : " + suma);
    }
}
