import java.io.*;
import java.util.*;
import java.math.*;

class c58 {
static boolean isPrime(BigInteger n) {
    return n.isProbablePrime(10000);
}
static public void main(String[] st) {
    BigInteger num = BigInteger.valueOf(1);
    int total = 0;
    for(int j = 2; j < (int)Math.sqrt(1000000000); j += 2) {
        BigInteger s = BigInteger.valueOf(j);
        num = num.add(s);
        if(isPrime(num))
            total++;
        num = num.add(s);
        if(isPrime(num))
            total++;
        num = num.add(s);
        if(isPrime(num))
            total++;
        num = num.add(s);
        if(isPrime(num))
            total++;
    	System.out.println(j + " : " + ((100*total)/(2*j + 1)) );
	}
}
}
