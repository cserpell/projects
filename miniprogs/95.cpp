#include <iostream>
#include <cmath>
#include <set>

#define MM 1000000

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

unsigned long long *tabla;

unsigned long long phi(unsigned long long n) {
    unsigned long long val = n;
    unsigned long long ret = 1;
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        unsigned long long prim = (*it);

/*        if((val % prim) == 0) {
            nuevo = val / prim;
            if((nuevo % prim) == 0
        }
*/
        unsigned long long nuevo = prim;
        while((val % prim) == 0) {
            val /= prim;
            nuevo *= prim;
        }
        if(nuevo != prim) {
            ret *= (nuevo - 1)/(prim - 1);
        }
        if(val == 1)
            break;
    }
    if(ret == 1)
        primos.insert(n);
    else
        ret -= n;
    if(ret <= MM)
        tabla[n] = ret;
    else
        tabla[n] = 0;
    return ret;
}

int main() {
    tabla = new unsigned long long[MM + 1];
    if(tabla == NULL) {
        cout << "No pudo..." << endl;
        return 0;
    }
    tabla[1] = 1;
    unsigned long long i;
    unsigned long long cual = 0;
    unsigned long long maxlen = 0;
    for(i = 2; i <= MM; i++) {
//        phi(i);
        cout << i << " " << phi(i) << endl;
    }
    enteros lista;
    for(i = 2; i <= MM; i++) {
        lista.clear();
        unsigned long long inicial = i;
        unsigned long long next = tabla[inicial];
        unsigned long long largo = 1;
        lista.insert(next);
        while(next != inicial && next != 0 && next != 1) {
            next = tabla[next];
            largo++;
            if(lista.find(next) != lista.end()) {
                next = 0;
                break;
            }
            lista.insert(next);
        }
        if(next == 0 || next == 1)
            continue;
        cout << i << " tiene largo " << largo;
        if(largo > maxlen) {
            maxlen = largo;
            cual = i;
        }
    }
    cout << "El maximo " << maxlen << " fue en " << cual << endl;
	return 0;
}
