#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

int next(int resto, int n) {
    return (resto*10) % n;
}

int search(int *lista, int cant, int nuevo) {
    for(int i = 0; i < cant; i++)
        if(lista[i] == nuevo)
            return 1;
    return 0;
}

int main() {
    int *lista;
    int tam, resto;
    int mejor = 0;
    int maxtam = 0;
    for(int i = 1; i < 1000; i++) {
        lista = new int[1000];
        tam = 0;
        resto = 1;
        //std::cout << resto << std::endl;
        while(resto != 0 && !search(lista, tam, resto)) {
            lista[tam] = resto;
            tam++;
            resto = next(resto, i);
            //std::cout << resto << std::endl;
        }
        if(tam > maxtam) {
            maxtam = tam;
            mejor = i;
        }
    }
    std::cout << mejor << std::endl;
	return 0;
}
