import java.util.*;
import java.io.*;
import java.math.*;

class c111 {
    static public int CDIGS;
    static public int recur(int pos, int faltan, int[] a, int cual) {
        if(CDIGS - pos < faltan)
            return 0;
        if(pos == CDIGS) {
            BigInteger b = BigInteger.valueOf(0);
            int cantactual = 0;
            for(int i = 0; i < CDIGS; i++) {
                b = b.multiply(BigInteger.valueOf(10)).add(BigInteger.valueOf(a[i]));
                if(a[i] == cual) {
                    cantactual++;
                }
            }
         //   System.out.print(b + "..");
            if(b.isProbablePrime(10000)) {
                if(cantactual > maxD[cual]) {
                    System.out.println(b + " cambia " + cual + " a " + cantactual);
                    int afaltan = cantactual - maxD[cual];
                    maxD[cual] = cantactual;
                    sumaD[cual] = b;
                    return afaltan;
                } else if(maxD[cual] == cantactual) { // cantactual == maxD[i]
                    System.out.println(b + " tiene " + cantactual);
                    sumaD[cual] = sumaD[cual].add(b);
                    return 0;
                }
            }
//            System.out.println("ERROR!!");
            return 0;
        }
        int i = 0;
        int afaltan = faltan;
        if(pos == 0)
            i = 1;
        for( ; i < 10; i++) {
            a[pos] = i;
            if(i == cual) {
                afaltan += recur(pos + 1, afaltan - 1, a, cual);
            } else {
                afaltan += recur(pos + 1, afaltan, a, cual);
            }
        }
        return afaltan - faltan;
    }
    static public BigInteger[] sumaD;
    static public int[] maxD;
    static public int[] actualD;
    static public void main(String[] args) throws IOException {
        CDIGS = 10;
        sumaD = new BigInteger[10];
        maxD = new int[10];
        for(int i = 0; i < 10; i++) {
            sumaD[i] = BigInteger.valueOf(0);
            maxD[i] = 6;
        }
        int[] a = new int[CDIGS];
        for(int i = 0; i < 10; i++) {
            recur(0, 6, a, i);
        }
        BigInteger sumaTot = BigInteger.valueOf(0);
        for(int i = 0; i < 10; i++) {
            System.out.println("suma[" + i + "] = " + sumaD[i]);
            sumaTot = sumaTot.add(sumaD[i]);
        }
        System.out.println(sumaTot);
    }
}
