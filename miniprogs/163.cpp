#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long canttotal;

int main() {
    unsigned long long d[37];
    d[0] = 0;
    d[1] = 16;
    d[2] = 104;
    for(int i = 3; i <= 36; i++) {
        unsigned long long ni = 3*(d[i - 1] - d[i - 2]) + 2*d[i - 3] + 16;
        cout << i << ": " << ni << endl;
        d[i] = ni;
    }
    return 0;
}
