#include <iostream>
#include <sstream>
#include <cmath>

void mult(int *digs) {
    int res = 0;
    for(int i = 0; i < 10; i++) {
        int pp = digs[i]*2 + res;
        if(pp >= 10) {
            digs[i] = pp - 10;
            res = 1;
        } else {
            digs[i] = pp;
            res = 0;
        }
    }
}

int main() {
    int *digs;
    
    digs = new int[10];
    digs[0] = 3;
    digs[1] = 3;
    digs[2] = 4;
    digs[3] = 8;
    digs[4] = 2;
    digs[5] = 0;
    digs[6] = 0;
    digs[7] = 0;
    digs[8] = 0;
    digs[9] = 0;
    for(int i = 1; i <= 7830457; i++) {
        mult(digs);
    }
    for(int i = 9; i >= 0; i--)
        std::cout << digs[i];
    std::cout << std::endl;
	return 0;
}
