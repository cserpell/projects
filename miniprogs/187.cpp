#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int main() {
    unsigned long long i;
    unsigned long long cc = 1000000LL;
    for(i = 3; i <  100000000LL/2; i += 2) { //15; i += 2) {
        isPrime(i);
        if(i > cc) {
          cout << "." << (i/1000000) << endl;
          cc += 1000000;
        }
    }
    primos.insert(2);
    enteros finales;
    unsigned long long tot = 0;
    for(enteros::iterator it = primos.begin(); it != primos.end(); it++) {
      unsigned long long np1 = *it;
     // cout << "Iterando para " << np1 << endl;
    for(enteros::iterator it2 = primos.begin(); it2 != primos.end(); it2++) {
      unsigned long long np2 = *it2;
      if(np2 > np1)
        break;
      unsigned long long res = np1*np2;
//      if(res >= 30)//100000000LL)
      if(res >= 100000000LL)
        break;
  //    cout << np1 << "," << np2 << endl;
      tot++;
  //    finales.insert(res);
    }
    }

//    cout << "Suma = " << tot << " en cjto : " << finales.size() << endl;
    cout << "Suma = " << tot << endl;
	return 0;
}
