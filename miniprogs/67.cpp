#include <iostream>
#include <sstream>
#include <cmath>

int *dijkstra(int **mat, int tam) {
    int llegado[tam];
    int *costo;
    int cmin[tam];
    int pred[tam];
    
    costo = new int[tam];
    for(int i = 0; i < tam; i++) {
        llegado[i] = 0;
        costo[i] = 1;
        cmin[i] = 1;
        pred[i] = -1;
    }
    llegado[0] = 1;
    costo[0] = 0;
    for(int i = 0; i < tam; i++) {
        if(mat[0][i]) {
            cmin[i] = -mat[0][i];
            pred[i] = 0;
        }
    }
    for(int i = 0; i < tam; i++) {
        int ccc = 0;
        int cual = -1;
        for(int j = 0; j < tam; j++) {
            if(!llegado[j] && cmin[j] != 1 && cmin[j] < ccc) {
                cual = j;
                ccc = cmin[j];
            }
        }
        std::cout << "sera escogido el " << cual << " con costo " << cmin[cual] << std::endl;
        if(cual != -1) {
            costo[cual] = costo[pred[cual]] + cmin[cual];
            llegado[cual] = 1;
            for(int p = 0; p < tam; p++) {
                if(!llegado[p] && mat[cual][p]) {
                    if(cmin[p] == 1 || /*costo[cual]*/ - mat[cual][p] < cmin[p]) {
                        cmin[p] = /*costo[cual]*/ - mat[cual][p];
                        pred[p] = cual;
                    }
                }
            }
        }
    }
    return costo;
}

int buscamax(int *val, int pisos) {
    for(int i = pisos - 1; i > 0; i--) {
        int inipiso = (i*(i - 1))/2;
        for(int j = 0; j < i; j++) {
            if(val[inipiso + i + j] > val[inipiso + i + j + 1])
                val[inipiso + j] = val[inipiso + j] + val[inipiso + i + j];
            else
                val[inipiso + j] = val[inipiso + j] + val[inipiso + i + j + 1];
        }
    }
    return val[0];
}

int main() {
    int pisos;
    std::cin >> pisos;
    int cant = (pisos*(pisos + 1))/2;
    int *val;
    val = new int[cant];
    int aux;
    for(int i = 0; i < cant ; i++) {
        std::cin >> aux;
        val[i] = aux;
    }
    int res = buscamax(val, pisos);
    std::cout << res << std::endl;
	return 0;
}
