#include <iostream>
#include <sstream>

int main() {
    int sss = 0;
    for(int i = 2; i < 10000000; i++) {
        int d0 = i / 1000000;
        int d1 = (i % 1000000) / 100000;
        int d2 = (i % 100000) / 10000;
        int d3 = (i % 10000) / 1000;
        int d4 = (i % 1000) / 100;
        int d5 = (i % 100) / 10;
        int d6 = i % 10;
        int ss = d1*d1*d1*d1*d1 + d2*d2*d2*d2*d2 + d3*d3*d3*d3*d3 + d4*d4*d4*d4*d4 + d5*d5*d5*d5*d5 + d6*d6*d6*d6*d6 + d0*d0*d0*d0*d0;
        if(ss == i) {
            std::cout << i << std::endl;
            sss += i;
        }
    }
	std::cout << sss << std::endl;
	return 0;
}
