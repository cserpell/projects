#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

unsigned long long pot[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000,
        100000000, 1000000000, 10000000000LL,
                              100000000000LL};
enteros cubos;

int usado[15];
int pos[15];
int digs[15];
int cantidad;

enteros listaactual;

int checkSame(unsigned long long c, int largo) {
    int largo2 = (int)((log((double)c)/log(10.)) + 1.);
    if(largo2 != largo || largo2 >= 13)
        return -1;
    for(int i = 0; i < largo; i++)
        usado[i] = 0;
    for(int j = 0; j < largo; j++) {
        int actual;
        if(j != 11)
            actual = (c % pot[j + 1]) / pot[j];
        else
            actual = c / pot[j];
        int seteao = 0;
        for(int p = 0; p < largo; p++) {
            if(digs[p] == actual && usado[p] == 0) {
                seteao = 1;
                usado[p] = 1;
                break;
            }
        }
        if(!seteao) {
            return 0;
        }
    }
    return 1;
}

void checkTodo(unsigned long long cual, int largo) {
    enteros::iterator it;
    for(it = cubos.begin(); it != cubos.end(); ) {
       // cout << (*it) << " ";
        int res = checkSame(*it, largo);
        if(res == -1)
            return;
        if(res == 1) {
            cubos.erase(*it);
            cantidad++;
            it = cubos.begin();
        }
        if(res == 0)
            it++;
    }
}

void recur(int posr, int largo) {
    if(posr == largo) {
        unsigned long long a = 0;
        for(int i = 0; i < largo; i++) {
            if(digs[i] == 0 && pos[i] == largo - 1)
                return;
            a += digs[i]*pot[pos[i]];
    //        cout << i << ":" << digs[i] << ":" << pos[i] << " ";
        }        
      //      cout << " " << a << "  ";
        if(cubos.find(a) != cubos.end()) { // && listaactual.find(a) == listaactual.end()) {
        //    cout << "SI ";
     //       listaactual.insert(a);
            cubos.erase(a);
            cantidad++;
        }
        return;
    }
    for(int i = 0; i < largo; i++) {
        if(usado[i] || (posr == largo - 1 && digs[i] == 0))
            continue;
        usado[i] = 1;
        pos[i] = posr;
        recur(posr + 1, largo);
        usado[i] = 0;
    }
}

int main() {
    unsigned long long i;
    for(i = 1; i < 100000; i++) {
        cubos.insert(i*i*i);
    }
    for(enteros::iterator it = cubos.begin(); it != cubos.end(); it = cubos.begin()) {
        unsigned long long val = *it;
        int largo = (int)((log((double)val)/log(10.)) + 1.);
        if(largo == 13) {
            cout << "Nos pasamos..." << endl;
            break;
        }
        for(int j = 0; j < largo; j++) {
            if(j != 11)
                digs[j] = (val % pot[j + 1]) / pot[j];
            else
                digs[j] = val / pot[j];
            usado[j] = 0;
        }
        cantidad = 0;
    //    listaactual.clear();
        checkTodo(val, largo);
 //       recur(0, largo);
//        if(cantidad >= 2) {
            cout << val << " - " << largo << " -> " << cantidad << endl;;
            if(cantidad == 5)
                break;
  //      }
        if(cantidad == 0)
            cubos.erase(val);
    }
	return 0;
}
