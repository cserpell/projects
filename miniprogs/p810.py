"""Problem 810."""
class Error(Exception):
    """Raised when division by 0."""


def mult(one, two):
    tot_sum = 0
    last_one = one
    while two > 0:
        last_bit = two & 1
        if last_bit:
            new_val = last_one
            tot_sum = tot_sum ^ last_one
        last_one = last_one << 1
        two = two >> 1
    return tot_sum


def num_log(num):
    tot = 0
    while num > 0:
        num = num >> 1
        tot += 1
    return tot
    

def div(one, two):
    """Returns quotient and reminder."""
    l2 = num_log(two)
    if l2 == 0:
        raise Error('Division by 0')
    l1 = num_log(one)
    if l1 < l2:
        return 0, one
    if l1 == l2:
        return 1, one ^ two
    ntwo = two
    quo = 1
    while l2 < l1:
        ntwo = ntwo << 1
        l2 += 1
        quo = quo << 1
    nquo, rem = div(one ^ ntwo, two)
    return quo + nquo, rem


def check_associative(one, two, three):
    c1 = mult(mult(one, two), three)
    c2 = mult(one, mult(two, three))
    print(f'Check associative {one} x {two} x {three} -> {c1 == c2}')


def check_commutative(one, two):
    a = mult(one, two)
    b = mult(two, one)
    print(f'Check commutative {one} x {two} -> {a == b}')


def check_prime(primes, num):
    tot_log = num_log(num)
    max_n = 1 << (tot_log // 2 + 1)
    for prime in primes:
        if prime >= max_n:
            break
        quo, rem = div(num, prime)
        if rem == 0:
            return False
    return True


def fill_primes(max_n):
    n_primes = 1
    primes = [2]
    num = 3
    while n_primes < max_n:
        if check_prime(primes, num):
            primes.append(num)
            n_primes += 1
            print(f'{num} is prime! Total so far {n_primes}')
        num += 2


def print_division(one, two):
    print(f'Division {one} / {two} = {div(one, two)}')


def main():
    """Main execution point."""
    print(f'Mult 7 x 3 = {mult(7, 3)}')
    check_commutative(1, 2)
    check_commutative(4, 2)
    check_commutative(5, 7)
    check_commutative(9, 11)
    check_associative(1, 2, 3)
    check_associative(2, 3, 5)
    check_associative(6, 9, 3)
    check_associative(11, 2, 4)
    print_division(1, 2)
    print_division(4, 2)
    print_division(5, 7)
    print_division(7, 5)
    print_division(5, 3)
    print_division(9, 11)
    print_division(11, 9)
    print_division(9, 7)
    print_division(9, 3)
    fill_primes(10)
    fill_primes(5000000)


if __name__ == '__main__':
    main()
