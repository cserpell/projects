#include <iostream>
#include <cmath>

using namespace std;

int isCuad(unsigned long long n) {
    unsigned long long b = 1 + 2*n + 5*n*n;
    unsigned long long a = (unsigned long long)sqrt(b);
    if(a*a == b)
        return 1;
    return 0;
}

int main() {
    unsigned long long i;
    int cant = 1;
    for(i = 2; i < 5000000000; i++) {
        if(isCuad(i)) {
            cout << cant << ": " << i << endl;
            cant++;
        }
    }
	return 0;
}
