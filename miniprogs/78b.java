import java.util.*;
import java.lang.*;
import java.io.*;
import java.math.*;

class c78b {
    static public BigInteger[][] cantmaneras;
    static public int maxcalculado78;
    static public BigInteger BI0;
    static public BigInteger BI1;
    static public BigInteger BI1000000;
    static public void calcular(int para, int hasta) {
        int nit = 1;
        BigInteger cantidadmaneras = BI0;
        while(nit <= hasta) {
            int dif = para - nit;
            cantidadmaneras = cantidadmaneras.add(cantmaneras[dif][nit]);
            nit++;
        }
        cantmaneras[para][hasta] = cantidadmaneras;
    }
    static public void main(String[] args) throws IOException {
        int CANT = 2000;
        BI0 = BigInteger.valueOf(0);
        BI1 = BigInteger.valueOf(1);
        BI1000000 = BigInteger.valueOf(1000000);
        cantmaneras = new BigInteger[CANT][];
        for(int i = 0; i < CANT; i++) {
            cantmaneras[i] = new BigInteger[CANT];
            cantmaneras[0][i] = BI1;
        }

        for(int i = 1; i < CANT; i++) {
            for(int j = 0; j <= i; j++)
                calcular(i, j);
            for(int j = i + 1; j < CANT; j++)
                cantmaneras[i][j] = cantmaneras[i][i];
            System.out.println(i + ": " + cantmaneras[i][i]);
            if(cantmaneras[i][i].mod(BI1000000).compareTo(BI0) == 0)
                break;
        }
        System.out.println("FIN");
    }
}
