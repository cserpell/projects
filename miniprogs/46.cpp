#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((unsigned long long)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int main() {
    enteros primos;

    for(unsigned long long i = 3; i < 1000000; i += 2) {
        isPrime(primos, i);
    }
    primos.insert(2);
    unsigned long long impar = 3;
    while(1) {
        if(primos.end() == primos.find(impar)) {
            bool sepuede = 0;
            for(enteros::iterator it = primos.begin(); it != primos.end(); it++) {
                unsigned long long valor = *it;
                for(unsigned long long cuad = 1; ; cuad++) {
                    unsigned long long numero = 2*cuad*cuad + valor;
                    if(numero > impar)
                        break;
                    if(numero == impar) {
                        sepuede = 1;
                        break;
                    }
                }
                if(sepuede)
                    break;
            }
            if(!sepuede)
                break;
        }
        impar += 2;   
    }
 	std::cout << impar << std::endl;
	return 0;
}
