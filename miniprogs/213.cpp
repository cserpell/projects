#include <iostream>
#include <cmath>
#include <set>
#include <queue>

#define TAM 30

using namespace std;

class tablero {
 public:
  double p[TAM][TAM];
  double pactual(int x, int y) {
    if(x < 1 || y < 1 || x > TAM || y > TAM)
      return 0;
    return p[x - 1][y - 1];
  }
  double esp() {
    double sumat = 0;
    for(int i = 0; i < TAM ; i++)
    for(int j = 0; j < TAM ; j++)
      sumat += p[i][j];
    return sumat/(TAM*TAM);
  }
  tablero() {
    for(int i = 0; i < TAM ; i++)
    for(int j = 0; j < TAM ; j++)
      p[i][j] = 0;
  }
  void llenar() {
    for(int i = 0; i < TAM ; i++)
    for(int j = 0; j < TAM ; j++)
      p[i][j] = 1.;
  }
  void propagar(int x, int y, tablero * otro) {
    if(x < 1 || y < 1 || x > TAM || y > TAM)
      return;
    double pac = p[x - 1][y - 1];
    if(x > 1 && y > 1 && x < TAM && y < TAM) {
      otro->p[x - 1][y - 2] += pac/4.;
      otro->p[x - 1][y] += pac/4.;
      otro->p[x - 2][y - 1] += pac/4.;
      otro->p[x][y - 1] += pac/4.;
    } else
    if(x > 1 && y == 1 && x < TAM) {
      otro->p[x - 1][y] += pac/3.;
      otro->p[x - 2][y - 1] += pac/3.;
      otro->p[x][y - 1] += pac/3.;
    } else
    if(y > 1 && x == 1 && x < TAM) {
      otro->p[x - 1][y - 2] += pac/3.;
      otro->p[x - 1][y] += pac/3.;
      otro->p[x][y - 1] += pac/3.;
    } else
    if(x == TAM && y > 1 && y < TAM) {
      otro->p[x - 1][y] += pac/3.;
      otro->p[x - 1][y - 2] += pac/3.;
      otro->p[x - 2][y - 1] += pac/3.;
    } else
    if(x > 1 && y == TAM && x < TAM) {
      otro->p[x - 1][y - 2] += pac/3.;
      otro->p[x - 2][y - 1] += pac/3.;
      otro->p[x][y - 1] += pac/3.;
    } else
    if(x == 1 && y == 1) {
      otro->p[x - 1][y] += pac/2.;
      otro->p[x][y - 1] += pac/2.;
    } else
    if(x == 1 && y == TAM) {
      otro->p[x - 1][y - 2] += pac/2.;
      otro->p[x][y - 1] += pac/2.;
    } else
    if(x == TAM && y == 1) {
      otro->p[x - 1][y] += pac/2.;
      otro->p[x - 2][y - 1] += pac/2.;
    } else
    if(x == TAM && y == TAM) {
      otro->p[x - 1][y - 2] += pac/2.;
      otro->p[x - 2][y - 1] += pac/2.;
    }
  }
};

int main() {
    tablaro * inicial = new tablero();
    queue<circulo *> circulos;
  //  set<circulo *> finales;
    double radiogrande = 1. + 2.*sqrt(3.)/3.;
    circulo * inicio1 = new circulo(-radiogrande);
    circulo * inicio2 = new circulo(1.);
    circulo * inicio3 = new circulo(1.);
    circulo * inicio4 = new circulo(1.);
  //  finales.insert(inicio2);
  //  finales.insert(inicio3);
  //  finales.insert(inicio4);
  
    double area = 3;
    
    cout << radiogrande << endl;
    
    circulo * inicio11 = new circulo(inicio1, inicio2, inicio3, 1);
    circulo * inicio12 = new circulo(inicio1, inicio3, inicio4, 1);
    circulo * inicio13 = new circulo(inicio1, inicio2, inicio4, 1);
    circulo * inicio14 = new circulo(inicio2, inicio3, inicio4, 1);
    circulos.push(inicio11);
    circulos.push(inicio12);
    circulos.push(inicio13);
    circulos.push(inicio14);
    while(!circulos.empty()) {
      circulo * actual = circulos.front();
      circulos.pop();
      area += actual->R*actual->R;
      if(actual->iteracion != 10) {
        circulo * nuevo1 = new circulo(actual, actual->p1, actual->p2, actual->iteracion + 1);
        circulo * nuevo2 = new circulo(actual, actual->p2, actual->p3, actual->iteracion + 1);
        circulo * nuevo3 = new circulo(actual, actual->p1, actual->p3, actual->iteracion + 1);
        circulos.push(nuevo1);
        circulos.push(nuevo2);
        circulos.push(nuevo3);
      }
    }
    cout << area << " de " << (radiogrande*radiogrande) << endl;
    double total = 1. - area/(radiogrande*radiogrande);
    printf("%1.8f\n", total);
//    cout << total << endl;
	return 0;
}
