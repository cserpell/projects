#include <iostream>
#include <cmath>
#include <set>

using namespace std;

int num[100];
int maxnivel;
int counta;
int total;

int recur(int nivel) {
    if(nivel == maxnivel) {
        total++;
        int lastI = 10;
        int lastD = 0;
        int isIncr = 1;
        int isDec = 1;
        for(int i = 0; i < maxnivel; i++) {
            if(num[i] >= lastD) {
                lastD = num[i];
            } else {
                isDec = 0;
            }
            if(num[i] <= lastI) {
                lastI = num[i];
            } else {
                isIncr = 0;
            }
        }
        if(isIncr || isDec) {
            return (100*counta == 99*total);
        }
        counta++;
        return (100*counta == 99*total);
    }
    for(int i = (nivel > 0)?0:1 ; i < 10; i++) {
        num[nivel] = i;
        if(recur(nivel + 1))
            return 1;
    }
    return 0;
}

int main() {
    counta = 0;
    total = 0;
    for(int i = 1; i < 100; i++) {
        maxnivel = i;
        recur(0);
        cout << counta << " of " << total << endl;
    }
	return 0;
}
