import sys

TOPE = 9
if len(sys.argv) > 1 :
  TOPE = int(sys.argv[1])

reachables = {}

i = 1
while i <= TOPE :
  reachables[(i,i)] = {float(i):str(i)}
  i = i + 1

#print reachables

MAXLEN = TOPE - 1
i = 1
while i <= MAXLEN :
  j = 1
  while j <= TOPE :
    k = j + i
    if k <= TOPE :
      # Now computing reachables starting from j and ending in k
      p = j
      reachables[(j,k)] = {}
      while p < k :
        for r1 in reachables[(j, p)].keys() :
          for r2 in reachables[(p + 1, k)].keys() :
            r1v = reachables[(j,p)][r1]
            r2v = reachables[(p + 1,k)][r2]
            reachables[(j,k)][r1 + r2] = "(" + r1v + ")+(" + r2v + ")"
            reachables[(j,k)][r1 * r2] = "(" + r1v + ")*(" + r2v + ")"
     #       if r1 > r2 :
            reachables[(j,k)][r1 - r2] = "(" + r1v + ")-(" + r2v + ")"
#            if (r1 % r2) == 0 :
            if abs(r2 - 0) > 0.05 :
              reachables[(j,k)][r1 / r2] = "(" + r1v + ")/(" + r2v + ")"
        p = p + 1
      # Now, add interactions with concatenations
      nleft = j
      hto = nleft + 1
      while hto <= k :
        nleft = nleft * 10 + hto
        hto = hto + 1
        """
        reachables[(j,k)][nleft] = 1
        if hto < k :
          for r2 in reachables[(hto + 1, k)] :
            reachables[(j,k)][nleft + r2] = 1
            reachables[(j,k)][nleft * r2] = 1
            if nleft > r2 :
              reachables[(j,k)][nleft - r2] = 1
            if (nleft % r2) == 0 :
              reachables[(j,k)][nleft / r2] = 1
        """
      reachables[(j,k)][float(nleft)] = str(nleft)
    j = j + 1
  i = i + 1

#print reachables
treac = reachables[(1, TOPE)]
tk = treac.keys()
tk.sort()
nkeys = {}
for a in tk :
  if (abs(int(a) - a) < 0.0000001) and (int(a) > 0):
    print treac[a] + " -> " + str(int(a))
    nkeys[int(a)] = 1
ts = 0
for a in nkeys.keys() :
  ts = ts + a
print ts
