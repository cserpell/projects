#include <iostream>
#include <fstream>
#include <list>
#include <cmath>

using namespace std;

class ppar {
  public:
    int num;
    char l1;
    char l2;
    ppar(int n, char la, char lb) {
        num = n;
        l1 = la;
        l2 = lb;
    }
};

int main() {
    list<ppar *> lista;
    while(cin) {
        int num;
        string lett;
        cin >> num;
        if(cin.eof())
            break;
        cin >> lett;
        lista.push_front(new ppar(num, lett.at(0), lett.at(1)));
//        cout << num << endl;
  //      cout << lett << endl;
    }
    int maxnum = 26*26;
    for(int p1 = -maxnum; p1 < maxnum; p1++)
    {
        for(int p2 = -maxnum; p2 < maxnum; p2++)
        {
            for(int p3 = -maxnum; p3 < maxnum; p3++)
            {
                list<ppar *>::iterator it = lista.begin();
                bool found = true;
                while(found && it != lista.end()) {
                    ppar * p = *it;
                    int searched = p->l1*26 + p->l2;
                    int val = (p1*p->num*p->num + p2*p->num * p3)%maxnum;
                    if(val != searched) {
                        found = false;
                        break;
                    }
                    it++;
                }
                if(found) {
                    cout << "p1 = " << p1;// << endl;
                    cout << "\t";
                    cout << "p2 = " << p2;// << endl;
                    cout << "\t";
                    cout << "p3 = " << p3 << endl;
                }
            }
        }
    }
}
