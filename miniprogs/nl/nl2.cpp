#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <cmath>

using namespace std;

class ppar {
  public:
    int num;
    char l1;
    char l2;
    ppar(int n, char la, char lb) {
        num = n;
        l1 = la;
        l2 = lb;
    }
};

class nodo {
  public:
    bool esHoja;
    int nHijos;
    nodo **hijos;
    string nombre;
    virtual int eval() = 0;
    virtual nodo * duplicar() = 0;
    void print(int desv) {
        for(int i = 0; i < desv; i++)
            cout << " ";
        cout << nombre << endl;
//        cout << nHijos << endl;
        for(int i = 0; i < nHijos; i++)
            hijos[i]->print(desv + 1);
    }
    void setHijo(int pos, nodo *nuevo) {
        hijos[pos] = nuevo;
    }
    void eliminar() {
        if(nHijos > 0) {
            for(int i = 0; i < nHijos; i++) {
                hijos[i]->eliminar();
                delete hijos[i];
            }
            delete hijos;
        }
    }
};

class constante : public nodo {
  private:
    int valor;
  public:
    constante(int val) {
        valor = val;
        esHoja = true;
        nHijos = 0;
        nombre = "";
        stringstream ss;
        ss << valor;
        nombre.append(ss.str());
    }
    int eval() {
//        cout << "Retornando " << nombre << " : " << valor << endl;
        return valor;
    }
    nodo * duplicar() {
        constante * nuevo = new constante(valor);
        return nuevo;
    }
};

class funcion : public nodo {
  public:
    funcion(string nomb) {
        nombre = "";
        nombre.append(nomb);
        esHoja = false;
        if(nombre.compare("if") == 0) {
            nHijos = 3;
            hijos = new nodo *[3];
        } else if(nombre.compare("+") == 0) {
            nHijos = 2;
            hijos = new nodo *[2];
        } else if(nombre.compare("-") == 0) {
            nHijos = 2;
            hijos = new nodo *[2];
        } else if(nombre.compare("*") == 0) {
            nHijos = 2;
            hijos = new nodo *[2];
        } else if(nombre.compare("/") == 0) {
            nHijos = 2;
            hijos = new nodo *[2];
        } else if(nombre.compare("Q") == 0) {
            nHijos = 1;
            hijos = new nodo *[1];
        }
            else {
        // Nunca deberia llegar aqui
            cout << "ERROR 1 " << nombre << endl;
                exit(0);
        nombre = "vacio";
        nHijos = 0;
        esHoja = true;
            }
    }
    int eval() {
        if(nombre.compare("if") == 0) {
            int v1 = hijos[0]->eval();
            int v2 = hijos[1]->eval();
            int v3 = hijos[2]->eval();
            if(v1 != 0)
                return v2;
            return v3;
        } else if(nombre.compare("+") == 0) {
            int v1 = hijos[0]->eval();
            int v2 = hijos[1]->eval();
            return v1 + v2;
        } else if(nombre.compare("-") == 0) {
            int v1 = hijos[0]->eval();
            int v2 = hijos[1]->eval();
            return v1 - v2;
        } else if(nombre.compare("*") == 0) {
            int v1 = hijos[0]->eval();
            int v2 = hijos[1]->eval();
            return v1 * v2;
        } else if(nombre.compare("/") == 0) {
            int v1 = hijos[0]->eval();
            int v2 = hijos[1]->eval();
            if(v2 != 0)
                return v1 / v2;
            else
                return v1;
        } else if(nombre.compare("Q") == 0) {
            int v1 = hijos[0]->eval();
            return v1*v1;
        }
        // Nunca deberia llegar aqui
        cout << "ERROR 2" << endl;
        exit(0);
        return 0;
    }
    nodo * duplicar() {
        funcion * nuevo = new funcion(nombre);
        for(int i = 0; i < nHijos; i++) {
            nuevo->hijos[i] = hijos[i]->duplicar();
        }
        return nuevo;
    }
};

class parametro : public nodo {
    private:
    int indice;
    int *entradas;
  public:
    parametro(int indiceParametro, int *entrada) {
        entradas = entrada;
        indice = indiceParametro;
        esHoja = true;
        nHijos = 0;
        nombre = "p";
        stringstream ss;
        ss << indice;
        nombre.append(ss.str());
    }
    int eval() {
//        cout << "Retornando " << nombre << " : " << entradas[indice] << endl;
        return entradas[indice];
    }
    nodo * duplicar() {
        parametro * nuevo = new parametro(indice, entradas);
        return nuevo;
    }
};

nodo ** comparar(nodo **raices, int cantidad, list<ppar *> &lista, int *entrada, int cantidadSalida) {
    if((cantidadSalida > cantidad) || (cantidadSalida <= 0))
        return NULL;
    if(cantidadSalida == cantidad)
        return raices;
    int *difs = new int[cantidad];
    for(int i = 0; i < cantidad; i++) {
//        cout << "evaluando arbol " << i << endl;
        list<ppar *>::iterator it = lista.begin();
        int dif = 0;
        while(it != lista.end()) {
            ppar * p = *it;
            int searched = (p->l1 - 'a')*26 + (p->l2 - 'a');
            *entrada = p->num;
            int resultado = (raices[i]->eval())%(26*26);
            if(resultado > searched)
                dif += resultado - searched;
            else
                dif += searched - resultado;
            it++;
 //           cout << "evaluda respuesta (" << i << ").. dif " << dif << " search " << searched << endl;
        }
        cout << "evaludo arbol " << i << " .. dif " << dif << endl;
        difs[i] = dif;
    }
    nodo ** results = new nodo *[cantidad];
    for(int j = 0; j < cantidadSalida; j++) {
        int minDif = -1;
        int minPos = -1;
        for(int i = 0; i < cantidad; i++) {
            if((difs[i] != -1 && difs[i] < minDif) ||
               (minDif == -1 && difs[i] != -1)) {
                 minDif = difs[i];
                 minPos = i;
            }
        }
        if(minDif == 0) {
            raices[minPos]->print(0);
            exit(0);
        }
//        cout << "llenando arbol de salida " << j << " con original " << minPos << endl;
        results[j] = raices[minPos];
        difs[minPos] = -1;
    }
    cout << "se borraran los arboles que no siguen" << endl;
    for(int i = 0; i < cantidad; i++) {
        if(difs[i] != -1) {
//            cout << "borrardo arboles original " << i << " " << raices[i]->nombre << endl;
            raices[i]->eliminar();
            delete raices[i];
        }
    }
    return results;
}

nodo * generarArbol(int maxProf, float probParam, float probOp, int maxParam, int *entrada) {
    nodo * nuevo;
    if(maxProf > 1) {
        float rn1 = ((float)rand())/RAND_MAX;
        if(rn1 <= probOp) {
            float rn2 = ((float)rand())/RAND_MAX;
            if(rn2 < 0.1) {
                nuevo = new funcion("if");
            } else if(rn2 < 0.3) {
                nuevo = new funcion("+");
            } else if(rn2 < 0.5) {
                nuevo = new funcion("-");
            } else if(rn2 < 0.7) {
                nuevo = new funcion("*");
            } else if(rn2 < 0.9) {
                nuevo = new funcion("/");
            } else {
                nuevo = new funcion("Q");
            }
            for(int i = 0; i < nuevo->nHijos; i++) {
                nuevo->hijos[i] = generarArbol(maxProf - 1, probParam, probOp, maxParam, entrada);
            }
            return nuevo;
        }
    }
    float rn3 = ((float)rand())/RAND_MAX;
    if(rn3 <= probParam) {
        int rn4 = maxParam*(((float)rand())/RAND_MAX);
        if(rn4 != 0) {
            cout << "ERROR 3 , agregando " << rn4 << endl;
            exit(0);
        }
        nuevo = new parametro(rn4, entrada);
    } else {
        int rn4 = 26*26*(((float)rand())/RAND_MAX);
        nuevo = new constante(rn4);
    }
    return nuevo;
}

nodo * crossover(nodo * n1, nodo * n2, float probCross) {
    float rn1 = ((float)rand())/RAND_MAX;
    if(rn1 <= probCross)
        return n2->duplicar();
    nodo * nuevo = n1->duplicar();
    if(n2->nHijos > 0) {
        for(int i = 0; i < n1->nHijos; i++) {
            int rn2 = n2->nHijos*(((float)rand())/RAND_MAX);
            nuevo->hijos[i]->eliminar();
            delete nuevo->hijos[i];
            nuevo->hijos[i] = crossover(n1->hijos[i], n2->hijos[rn2], probCross);
        }
    }
    return nuevo;
}

nodo * mutacion(nodo * n1, float probMutar, int *entrada, int nivel) {
    float rn1 = ((float)rand())/RAND_MAX;
    if(nivel < 1)
        nivel = 1;
    if(rn1 <= probMutar) {
        return generarArbol(nivel, 0.25, 0.7, 1, entrada);
    }
    nodo * nuevo = n1->duplicar();
    for(int i = 0; i < n1->nHijos; i++) {
        nuevo->hijos[i]->eliminar();
        delete nuevo->hijos[i];
        nuevo->hijos[i] = mutacion(n1->hijos[i], probMutar, entrada, nivel - 1);
    }
    return nuevo;
}


int main() {
    list<ppar *> lista;
    while(cin) {
        int num;
        string lett;
        cin >> num;
        if(cin.eof())
            break;
        cin >> lett;
        lista.push_front(new ppar(num, lett.at(0), lett.at(1)));
        cout << num << endl;
        cout << lett << endl;
    }
    int maxR = 350;
    int maxProf = 10;
    
    int entrada = 0;
    nodo ** raices1 = new nodo *[maxR];
    for(int j = 0; j < maxR; j++) {
        raices1[j] = generarArbol(maxProf, 0.25, 0.7, 1, &entrada);
    }
    int iteraciones = 0;
    while(iteraciones < 10000) {
        cout << "iteracion " << iteraciones << endl;
        nodo ** nuevoraices = comparar(raices1, maxR, lista, &entrada, 15);
        cout << "crossover" << endl;
        int base = 10;
        for(int j = 0; j < 2; j++) {
            for(int i = 0; i < 10; i++) {
                for(int k = 0; k < 10; k++) {
//                cout << "crossover " << base << endl;
                    nuevoraices[base] = crossover(nuevoraices[i], nuevoraices[k], 0.4);
                    base++;
                }
            }
        }
        cout << "mutacion " << base << endl;
        for(int j = 0; j < 8; j++) {
            for(int i = 0; i < 8; i++) {
                nuevoraices[base] = mutacion(nuevoraices[i], 0.35, &entrada, maxProf);
                base++;
            }
        }
        cout << "generando mas azar " << base << endl;
        while(base < maxR) {
            nuevoraices[base] = generarArbol(maxProf, 0.25, 0.7, 1, &entrada);
            base++;
        }
        raices1 = nuevoraices;
        iteraciones++;
    }
    raices1[0]->print(0);
    return 0;
}
