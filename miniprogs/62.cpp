#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

unsigned long long pot[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000,
        100000000, 1000000000};
enteros cubos;

int usado[11];
int pos[11];
int digs[11];
int cantidad;

enteros listaactual;

void recur(int posr, int largo) {
    if(posr == largo) {
        unsigned long long a = 0;
        for(int i = 0; i < largo; i++) {
            if(digs[i] == 0 && pos[i] == largo - 1)
                return;
            a += digs[i]*pot[pos[i]];
    //        cout << i << ":" << digs[i] << ":" << pos[i] << " ";
        }        
      //      cout << " " << a << "  ";
        if(cubos.find(a) != cubos.end()) { // && listaactual.find(a) == listaactual.end()) {
        //    cout << "SI ";
     //       listaactual.insert(a);
            cubos.erase(a);
            cantidad++;
        }
        return;
    }
    for(int i = 0; i < largo; i++) {
        if(usado[i] || (posr == largo - 1 && digs[i] == 0))
            continue;
        usado[i] = 1;
        pos[i] = posr;
        recur(posr + 1, largo);
        usado[i] = 0;
    }
}

int main() {
    unsigned long long i;
    for(i = 1; i < 100000; i++) {
        cubos.insert(i*i*i);
    }
    for(enteros::iterator it = cubos.begin(); it != cubos.end(); it = cubos.begin()) {
        unsigned long long val = *it;
        int largo = (int)((log((double)val)/log(10.)) + 1.);
        if(largo == 11) {
            cout << "Nos pasamos..." << endl;
            break;
        }
        for(int j = 0; j < largo; j++) {
            if(j != 9)
                digs[j] = (val % pot[j + 1]) / pot[j];
            else
                digs[j] = val / pot[j];
            usado[j] = 0;
        }
        cantidad = 0;
    //    listaactual.clear();
        recur(0, largo);
//        if(cantidad >= 2) {
            cout << val << " - " << largo << " -> " << cantidad << endl;;
            if(cantidad == 5)
                break;
  //      }
        if(cantidad == 0)
            cubos.erase(val);
    }
	return 0;
}
