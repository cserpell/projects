import java.util.*;
import java.io.*;
import java.math.*;

class Frac {
    public BigInteger num;
    public BigInteger den;
    public Frac(BigInteger n1, BigInteger d1) {
        num = n1;
        den = d1;
    }
}

class c57 {
    static public BigInteger mcd(BigInteger a, BigInteger b) {
        BigInteger na = new BigInteger(a.toString());
        BigInteger nb = new BigInteger(b.toString());
        while(nb.compareTo(BigInteger.valueOf(0)) != 0) {
            BigInteger aux = na.mod(nb);
            na = nb;
            nb = aux;
        }
     //   System.out.println("mcd: " + na);
        return na;
    }
    static public Frac repetir(Frac anterior) {
        Frac nueva = new Frac(anterior.den, anterior.num);
   //     System.out.println("1/f: " + nueva.num + " " + nueva.den);
        nueva.num = nueva.num.add(nueva.den.multiply(BigInteger.valueOf(2)));
   //     System.out.println("1/f + 2: " + nueva.num + " " + nueva.den);
        BigInteger mm = mcd(nueva.num, nueva.den);
        nueva.num = nueva.num.divide(mm);
        nueva.den = nueva.den.divide(mm);
  //      System.out.println("mcd 1/f + 2: " + nueva.num + " " + nueva.den);
        return nueva;
    }
    static public Frac resultado(int cant) {
        Frac ini = new Frac(BigInteger.valueOf(2), BigInteger.valueOf(1));
        for(int i = 0; i < cant; i++)
            ini = repetir(ini);
        ini.num = ini.num.subtract(ini.den);
        return ini;
    }
    static public void main(String[] args) throws IOException {
        int contador = 0;
        for(int i = 1; i <= 1000; i++) {
            System.out.print(i + ": ");
            Frac res = resultado(i);
            System.out.println(res.num + " , " + res.den);
            if(res.num.toString().length() > res.den.toString().length())
                contador++;
        }
        System.out.println(contador);
    }
}
