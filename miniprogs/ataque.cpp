#include <iostream>
#include <set>

using namespace std;

const int MAXDADOS = 10;
const int MAXTAMDADO = 12;

double prob[MAXDADOS][MAXDADOS][MAXTAMDADO][MAXTAMDADO];

double probabil(int ataque, int defensa, int tamdadoa, int tamdadod) {
    if(prob[ataque][defensa][tamdadoa][tamdadod] > -0.5) {
        return prob[ataque][defensa][tamdadoa][tamdadod];
    }
    int mayora, mayorb;
    double res = 0.0;
    for(int mayora = 1; mayora <= tamdadoa; mayora++) {
        for(int mayord = 1; mayord < mayora && mayord < tamdadod; mayord++) {
            if(defensa == 1 || ataque == 1) {
                res += 1./(tamdadoa*tamdadod);
            } else {
                res += (1./(tamdadoa*tamdadod))*probabil(ataque - 1, defensa - 1, mayora, mayord);
            }
        }
    }
    return prob[ataque][defensa][tamdadoa][tamdadod] = res;
}

void setProbabil() {
    for(int i1 = 0; i1 < MAXDADOS; i1++)
    for(int i2 = 0; i2 < MAXDADOS; i2++)
    for(int i3 = 0; i3 < MAXTAMDADO; i3++)
    for(int i4 = 0; i4 < MAXTAMDADO; i4++)
        prob[i1][i2][i3][i4] = -1.;
}

int main() {
    cout << probabil(1, 1, 6, 6) << " 1 1 6 6" << endl;
    cout << probabil(2, 1, 6, 6) << " 2 1 6 6" << endl;
    cout << probabil(2, 2, 6, 6) << " 2 2 6 6" << endl;
    cout << probabil(1, 2, 6, 6) << " 1 2 6 6" << endl;
    cout << probabil(3, 1, 6, 6) << " 3 1 6 6" << endl;
    cout << probabil(3, 2, 6, 6) << " 3 2 6 6" << endl;
    cout << probabil(3, 3, 6, 6) << " 3 3 6 6" << endl;
    cout << probabil(2, 3, 6, 6) << " 2 3 6 6" << endl;
    cout << probabil(1, 3, 6, 6) << " 1 3 6 6" << endl;
    return 0;
}
