import sys

NP = 20
if len(sys.argv) > 1 :
  NP = int(sys.argv[1])

def f_fact (n) :
  if n == 0 :
    return 1
  if n == 1 :
    return 1
  if n == 2 :
    return 2
  if n == 3 :
    return 6
  if n == 4 :
    return 24
  if n == 5 :
    return 120
  if n == 6 :
    return 720
  if n == 7 :
    return 5040
  if n == 8 :
    return 40320
  if n == 9 :
    return 362880
  print "ERROR !!!!"
  quit()

def f_f ( n ) :
  fac = 0
  while n > 0 :
    r = n % 10
    fac = fac + f_fact(r)
    n = ( n - r ) / 10
  return fac

def f_s ( n ) :
  fac = 0
  while n > 0 :
    r = n % 10
    fac = fac + r
    n = ( n - r ) / 10
  return fac

"""
def f_f ( n , hf ) :
  if hf.has_key(n) :
    return hf[n]
  r = n % 10
  fac = f_fact(r)
  fac = fac + f_f( ( n - r ) / 10, hf )
  hf[n] = fac
  return hf[n]

def f_s ( n , hs ) :
  if hs.has_key(n) :
    return hs[n]
  fac = n % 10
  fac = fac + f_s( (n - fac) / 10, hs )
  hs[n] = fac
  return fac
"""

def f_g ( n , hsf , ans, lastcalc ) :
  if ans.has_key(n) :
    return ans[n]
  i = lastcalc
  while True:
    if not hsf.has_key(i) :
      hsf[i] = f_s(f_f(i))
      if hsf[i] <= 150 and not ans.has_key(hsf[i]) :
        ans[hsf[i]] = i
    if hsf[i] == n :
      return i
    i = i + 1

totsum = 0

ans = {}

i = 1
while len(ans) < NP :
  a = f_s(f_f(i))
  if a <= NP and not ans.has_key(a) :
    ans[a] = i
    print "Respuesta para " + str(a) + " : " + str(i) + " . Largo : " + str(len(ans))
  i = i + 1

i = 1
while i <= NP :
  sg_i = f_s( ans[i] )
  totsum = totsum + sg_i
  i = i + 1

print str(totsum)

