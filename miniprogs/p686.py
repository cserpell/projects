import math

LL = math.log2(123)
LU = math.log2(124)
L5 = math.log2(5)


def lim_d(d, ll):
    return ll + d * (L5 + 1)


def check_d(d):
    min_lim = lim_d(d, LL)
    max_lim = lim_d(d, LU)
    aa = int(max_lim)
    if aa >= min_lim:
        return aa
    return -1


def main():
    d = 1
    num = 0
    while num < 678911:
        ccd = check_d(d)
        if ccd > 0:
            num += 1
            print('p(123, {}) = {}'.format(num, ccd))
        d += 1


if __name__ == '__main__':
  main()

