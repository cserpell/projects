#include <iostream>
#include <cmath>

int ocupado[10];
int arr[9];
int p10[9] = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};

void recur(int pos) {
    if(pos == 9) {
        // it's a pandigital
//        std::cout << "pandigital!" << std::endl;
        int f1;
        for(int p = 1; p < 9; p++) {
            int val1 = 0;
            for(int i = 0; i < p; i++)
                val1 += arr[i]*p10[p - i - 1];
//            std::cout << "preerror!" << val1 << std::endl;
            if(val1 <= 0) {
                std::cout << "ERROR !";
                return;
            }
            int mult = 1;
            int apos = 0;
            while(1) {
//                std::cout << "while1";
                int nval = val1*mult;
                int pnval = (int)(log(nval)/log(10));
                f1 = 0;
                while(nval != 0) {
//                    std::cout << "whilenval";
                    if(apos == 9) {
                        f1 = 1;
                        break;
                    }
                    int extr = nval/p10[pnval];
//                    std::cout << nval << " " << extr << " " << pnval << " " << apos << std::endl;
                    if(arr[apos] != extr) {
                        f1 = 1;
                        break;
                    }
                    apos++;
                    nval -= extr*p10[pnval];
                    pnval--;
                }
                if(f1)
                    break;
                if(apos == 9) {
                    for(int i = 0; i < 9; i++)
                        std::cout << arr[i];
                    std::cout << std::endl;
                    return;
                }
                mult++;
            }
        }
        return;
    }
    for(int i = 9; i >= 1; i--) {
        if(!ocupado[i]) {
            arr[pos] = i;
            ocupado[i] = 1;
            recur(pos + 1);
            ocupado[i] = 0;
        }
    }
}
int main() {
    for(int i = 1; i < 10; i++)
        ocupado[i] = 0;
    recur(0);
    return 0;
}
