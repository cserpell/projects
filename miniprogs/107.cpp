#include <iostream>
#include <set>
#include <string>
#include <cstdlib>

using namespace std;

class arista {
  public:
    int inicio;
    int fin;
    int costo;
    bool less(arista & b) {
        if(this->costo < b.costo)
            return true;
        if(this->inicio == b.inicio) {
            if(this->fin == b.fin)
                return false;
            return this->fin < b.fin;
        }
        if(this->inicio == b.fin) {
            if(this->fin == b.inicio)
                return false;
            return this->fin < b.inicio;
        }
        return false;
    }
    arista(int a, int b, int c) {
        inicio = a;
        fin = b;
        costo = c;
    }
};

struct laris {
    bool operator()(const arista * a, const arista * b)
    {
        if(a->costo < b->costo)
            return true;
        if(a->inicio == b->inicio) {
            if(a->fin == b->fin)
                return false;
            return a->fin < b->fin;
        }
        if(a->inicio == b->fin) {
            if(a->fin == b->inicio)
                return false;
            return a->fin < b->inicio;
        }
        return false;
    }
};

typedef set< arista *, laris > cjto;
typedef set< int > puntos;

cjto conj;
puntos pto;

int mat[40][40];

void addPoint(int cual) {
    cout << "agrenando pto " << cual << endl;
    cjto::iterator it;
    cjto::iterator it2;
    it = conj.begin();
    while(it != conj.end()) {
        if((*it)->inicio == cual || (*it)->fin == cual) {
            cout << "se saca " << (*it)->inicio << "-" << (*it)->fin << " " << (*it)->costo << endl;
            it2 = it;
            it2++;
            conj.erase(it);
            it = it2;
            continue;
        }
        it++;
    }
    for(int i = 0; i < 40; i++) {
        if(i == cual)
            continue;
        if(pto.find(i) != pto.end()) {  
      /*      mat[i][cual] = -1;
            mat[cual][i] = -1;*/
            continue;
        }
        if(mat[cual][i] != -1) {
            cout << "se agrega " << cual << "-" << i << " " << mat[cual][i] << endl;
            conj.insert(new arista(cual, i, mat[cual][i]));
        }
    }
}

int main() {
    int imin = -1;
    int jmin = -1;
    int min = 999;
    int costototal = 0;
    for(int i = 0; i < 40; i++) {
        string str;
        cin >> str;
        int upos = 0;
        for(int j = 0; j <= i; j++) {
            int npos = str.find(',', upos + 1);
            string sub;
            if(npos == string::npos) {
                sub = str.substr(upos + 1);
            } else {
                if(upos != 0)
                    sub = str.substr(upos + 1, npos - upos - 1);
                else
                    sub = str.substr(upos, npos - upos);
            }
//            std::cout << npos << ":" << sub << " ";
            if(sub.c_str()[0] != '-') {
                mat[i][j] = atoi(sub.c_str());
                mat[j][i] = mat[i][j];
                if(mat[i][j] < min) {
                    min = mat[i][j];
                    imin = i;
                    jmin = j;
                }
                costototal += mat[i][j];
            } else {
                mat[i][j] = -1;
                mat[j][i] = -1;
            }
            upos = npos;
        }
    }
/*    for(int i = 0; i < 40; i++) {
        for(int j = 0; j < 40; j++) {
            std::cout << mat[i][j] << " ";
        }
        cout << endl;
    }*/
    int costo = 0;
    pto.insert(imin);
    addPoint(imin);
    for(int i = 1; i < 40; i++) {
        cjto::iterator it = conj.begin();
        arista * ar = (*it);
        cout << "Agregando .. " << ar->inicio << "-" << ar->fin << " " << ar->costo << endl;
        if(pto.find(ar->inicio) != pto.end() && pto.find(ar->fin) == pto.end()) {
            pto.insert(ar->fin);
            costo += mat[ar->inicio][ar->fin];
            addPoint(ar->fin);
        } else if (pto.find(ar->inicio) == pto.end() && pto.find(ar->fin) != pto.end()) {
            pto.insert(ar->inicio);
            costo += mat[ar->fin][ar->inicio];
            addPoint(ar->inicio);
        } else {
            cout << "ERROR! " << ar->inicio << "-" << ar->fin << " " << ar->costo << endl;
        }
    }
    cout << costo << endl;
    cout << (costototal - costo) << endl;
    return 0;
}
