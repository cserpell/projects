#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

unsigned long long mcd(unsigned long long a, unsigned long long b) {
    while(b != 0) {
        unsigned long long aux = a % b;
        a = b;
        b = aux;
    }
    return a;
}

int main() {
    unsigned long long cantidad = 0;
    for(unsigned long long m = 1; m <= 10000; m++) {
        for(unsigned long long n = 1; n < m; n++) {
            unsigned long long a = m*m - n*n;
            unsigned long long b = 2*m*n;
            unsigned long long c = m*m + n*n;
            unsigned long long per = a + b + c;
            if(per >= 100000000)
                break;
            if(mcd(m, n) != 1)
                continue;
            if((m % 2) != 0 && (n % 2) != 0)
                continue;
            /* A relative pythagorean triplet */
            unsigned long long dif;
            if(a < b)
                dif = b - a;
            else
                dif = a - b;
            if((c % dif) != 0)
                continue;
            unsigned long long much = 100000000/per;
            cantidad += much;
            cout << a << " . " << b << " = " << c << " -> " << much << endl;
        }
    }
    cout << cantidad << endl;
    return 0;
}
