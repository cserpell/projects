#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

unsigned long long pot[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};
int acambiar[8];
int digs[8];
int largo;
int maxcant;
unsigned long long cualprimo;
unsigned long long primoo;

void rec(int p, int desde) {
    if(p == 0) {
        int cant = 0;
        for(int i = 0; i <= 9; i++) {
            if(i == 0 && acambiar[largo - 1] == 1)
                continue;
            unsigned long long valor = 0;
            for(int j = 0; j < largo; j++) {
                if(acambiar[j])
                    valor += i*pot[j];
                else
                    valor += digs[j]*pot[j];
            }
            if(primos.find(valor) != primos.end())
                cant++;
        }
        if(cant > maxcant) {
            if(cant == 8) {
                cout << primoo << " ES EL FIN!!!!!!!!!!!!!" << endl;
            }
            cout << primoo << " tiene " << cant << endl;
            maxcant = cant;
            cualprimo = primoo;
        }
        return;
    }
    for(int i = desde; i < largo; i++) {
        if(acambiar[i])
            continue;
        acambiar[i] = 1;
        rec(p - 1, i + 1);
        acambiar[i] = 0;
    }
}

int main() {
    for(unsigned long long i = 3; i < 1000000; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    for(enteros::iterator it = primos.begin(); it != primos.end(); it++) {
        largo = (int)(log(*it)/log(10)) + 1;
        maxcant = -1;
        for(int j = 0; j < largo; j++)
            digs[j] = ((*it) % pot[j + 1])/pot[j];
        for(int i = 1; i < largo; i++) {
            for(int j = 0; j < largo; j++)
                acambiar[j] = 0;
            primoo = *it;
            rec(i, 0);
        }
    }
	return 0;
}
