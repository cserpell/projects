
MAX_N = 1000000L
MODULO = 1000000007L


def pot(number, p):
  if p == 0:
    return 1L
  if p == 1:
    return number
  half_pot = pot(number, p / 2)
  half_pot = (half_pot * half_pot) % MODULO
  if p % 2:
    return (half_pot * number) % MODULO
  return half_pot


def calc(n):
  tot = 0L
  for k in range(1L, n + 1L):
    print 'Computing for k = %s .. total so far %s' % (k, tot)
    constant = (1L - k * k) % MODULO
    next = constant
    for p in range(1L, n + 1L):
      tot = (tot + next) % MODULO
      next = (next * constant) % MODULO
  return tot


def main():
  print calc(MAX_N)


if __name__ == '__main__':
  main()

