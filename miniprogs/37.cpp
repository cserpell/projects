#include <iostream>
#include <sstream>
#include <cmath>

int isPrime(int *tabla, unsigned int n) {
    unsigned int hasta = ((int)sqrt(n)) + 1;
    for(unsigned int i = 3; i <= hasta; i += 2) {
        if(tabla[i] && (n % i) == 0) {
            tabla[n] = 0;
            return 0;
        }
    }
    tabla[n] = 1;
    return 1;
}

int isCircular(int *tabla, unsigned int n) {
    if(!tabla[n])
        return 0;
    unsigned int dm2, dm1, d0, d1, d2, d3, d4, d5, d6;
    
    dm2 = n/100000000;
    dm1 =(n % 100000000)/10000000;
    d0 = (n % 10000000)/1000000;
    d1 = (n % 1000000)/100000;
    d2 = (n % 100000)/10000;
    d3 = (n % 10000)/1000;
    d4 = (n % 1000)/100;
    d5 = (n % 100)/10;
    d6 = n % 10;
    if(dm2 == 0) {
    if(dm1 == 0) {
    if(d0 == 0) {
    if(d1 == 0) {
        if(d2 == 0) {
            if(d3 == 0) {
                if(d4 == 0) {
                    if(d5 == 0) {
                        return 0;
                    }
                    if(!tabla[d6])
                        return 0;
                    if(!tabla[d5])
                        return 0;
                    return 1;
                }
                if(!tabla[10*d5 + d6])
                    return 0;
                if(!tabla[10*d4 + d5])
                    return 0;
                if(!tabla[d6])
                    return 0;
                if(!tabla[d4])
                    return 0;
                return 1;
            }
            if(!tabla[100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100*d3 + 10*d4 + d5])
                return 0;
            if(!tabla[10*d5 + d6])
                return 0;
            if(!tabla[10*d3 + d4])
                return 0;
            if(!tabla[d6])
                return 0;
            if(!tabla[d3])
                return 0;
            return 1;
        }
            if(!tabla[1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[1000*d2 + 100*d3 + 10*d4 + d5])
                return 0;
            if(!tabla[100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100*d2 + 10*d3 + d4])
                return 0;
            if(!tabla[10*d5 + d6])
                return 0;
            if(!tabla[10*d2 + d3])
                return 0;
            if(!tabla[d6])
                return 0;
            if(!tabla[d2])
                return 0;
        return 1;
    }
            if(!tabla[10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[10000*d1 + 1000*d2 + 100*d3 + 10*d4 + d5])
                return 0;
            if(!tabla[1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[1000*d1 + 100*d2 + 10*d3 + d4])
                return 0;
            if(!tabla[100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100*d1 + 10*d2 + d3])
                return 0;
            if(!tabla[10*d5 + d6])
                return 0;
            if(!tabla[10*d1 + d2])
                return 0;
            if(!tabla[d6])
                return 0;
            if(!tabla[d1])
                return 0;
    return 1;
    }
            if(!tabla[100000*d1 + 10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100000*d0 + 10000*d1 + 1000*d2 + 100*d3 + 10*d4 + d5])
                return 0;
            if(!tabla[10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[10000*d0 + 1000*d1 + 100*d2 + 10*d3 + d4])
                return 0;
            if(!tabla[1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[1000*d0 + 100*d1 + 10*d2 + d3])
                return 0;
            if(!tabla[100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100*d0 + 10*d1 + d2])
                return 0;
            if(!tabla[10*d5 + d6])
                return 0;
            if(!tabla[10*d0 + d1])
                return 0;
            if(!tabla[d6])
                return 0;
            if(!tabla[d0])
                return 0;
    return 1;
    }
            if(!tabla[1000000*d0 + 100000*d1 + 10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[1000000*dm1 + 100000*d0 + 10000*d1 + 1000*d2 + 100*d3 + 10*d4 + d5])
                return 0;
            if(!tabla[100000*d1 + 10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100000*dm1 + 10000*d0 + 1000*d1 + 100*d2 + 10*d3 + d4])
                return 0;
            if(!tabla[10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[10000*dm1 + 1000*d0 + 100*d1 + 10*d2 + d3])
                return 0;
            if(!tabla[1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[1000*dm1 + 100*d0 + 10*d1 + d2])
                return 0;
            if(!tabla[100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100*dm1 + 10*d0 + d1])
                return 0;
            if(!tabla[10*d5 + d6])
                return 0;
            if(!tabla[10*dm1 + d0])
                return 0;
            if(!tabla[d6])
                return 0;
            if(!tabla[dm1])
                return 0;
    return 1;
    }
            if(!tabla[10000000*dm1 + 1000000*d0 + 100000*d1 + 10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[10000000*dm2 + 1000000*dm1 + 100000*d0 + 10000*d1 + 1000*d2 + 100*d3 + 10*d4 + d5])
                return 0;
            if(!tabla[1000000*d0 + 100000*d1 + 10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[1000000*dm2 + 100000*dm1 + 10000*d0 + 1000*d1 + 100*d2 + 10*d3 + d4])
                return 0;
            if(!tabla[100000*d1 + 10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100000*dm2 + 10000*dm1 + 1000*d0 + 100*d1 + 10*d2 + d3])
                return 0;
            if(!tabla[10000*d2 + 1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[10000*dm2 + 1000*dm2 + 100*d0 + 10*d1 + d2])
                return 0;
            if(!tabla[1000*d3 + 100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[1000*dm2 + 100*dm1 + 10*d0 + d1])
                return 0;
            if(!tabla[100*d4 + 10*d5 + d6])
                return 0;
            if(!tabla[100*dm2 + 10*dm1 + d0])
                return 0;
            if(!tabla[10*d5 + d6])
                return 0;
            if(!tabla[10*dm2 + dm1])
                return 0;
            if(!tabla[d6])
                return 0;
            if(!tabla[dm2])
                return 0;
    return 1;
}

int main() {
    int *tabla;
    
    tabla = new int[100000000];
    for(unsigned int i = 0; i < 1000000; i ++) {
        tabla[i] = 0;
//        std::cout << i << " es " << r << std::endl;
    }
    for(unsigned int i = 3; i < 1000000; i += 2) {
        int r = isPrime(tabla, i);
//        std::cout << i << " es " << r << std::endl;
    }
    tabla[2] = 1;
    int cant = 0;
    for(unsigned int i = 3; i < 1000000; i += 2) {
        if(isCircular(tabla, i)) {
            cant += i;
            std::cout << i << " es de los buscados" << std::endl;
        }
    }
    std::cout << cant << std::endl;
	return 0;
}
