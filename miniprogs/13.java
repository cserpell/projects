import java.util.*;
import java.io.*;
import java.math.*;

class c13 {
    static public void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        BigInteger suma = new BigInteger("" + 0);
        for(int i = 1; i <= 100; i++) {
            suma = suma.add(new BigInteger(in.readLine()));
        }
        String s = suma.toString();
        System.out.println(s.substring(0, 10));
    }
}
