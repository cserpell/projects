

class P122(object):
  """Problem 122."""

  def __init__(self, stop_n=200):
    self._stop_n = stop_n
    self._last = 2
    self._ways = {1: [set()], 2: [set([2])]}
    self._numbers = {1: 0, 2: 1}

  def get_next(self):
    next = self._last + 1
    minimum = next - 1  # Worst way to do it
    tmp_ways = []
    for pair in range(1, next / 2 + 1):
      mirror = next - pair
      if self._numbers[pair] >= minimum or self._numbers[mirror] >= minimum:
        continue
      for way1 in self._ways[pair]:
        for way2 in self._ways[mirror]:
          new_way = way1 | way2
          new_way.add(next)
          if len(new_way) < minimum:
            minimum = len(new_way)
            tmp_ways = []
          if len(new_way) == minimum:
            tmp_ways.append(new_way)
    self._last = next
    self._ways[next] = tmp_ways
    self._numbers[next] = minimum

  def main(self):
    while self._last < self._stop_n:
      self.get_next()
      print '%s: %s' % (self._last, self._numbers[self._last])
    print 'End %s' % (sum(self._numbers[a] for a in range(1, self._stop_n + 1)))


if __name__ == '__main__':
  P122().main()

