#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<int> enteros;

int isPrime(enteros & primos, int n) {
    int hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}    

int radical(enteros & primos, int val) {
    int queda = val;
    int radic = 1;
    enteros::iterator it = primos.begin();
    while(queda > 1) {
//        cout << "entrando while1" << endl;
        while((queda % (*it)) != 0) {
            it++;
        }
        radic *= (*it);
//        cout << "entrando while2" << endl;
        while((queda % (*it)) == 0) {
            queda /= (*it);
        }
    }
    for(int j = 0; j < 6 - ((int)(log(radic)/log(10))); j++)
        cout << "0";
    cout << radic << " <- " << val << endl;
    return radic;
}

int main() {
    enteros primos;
    for(int j = 3; j < 150000; j += 2) {
        isPrime(primos, j);
    }
    primos.insert(2);
    
//    list<int> radii;
    for(int i = 1; i <= 100000; i++) {
        radical(primos, i);
    }
   // radii.sort();
	return 0;
}
