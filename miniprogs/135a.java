import java.util.*;
import java.lang.*;
import java.io.*;
import java.math.*;

class c135 {
static public BigInteger cte0;
static public BigInteger ctem1;
static public BigInteger cte2;
static public BigInteger cte3;
static public BigInteger cte1000000;

static public void main(String[] args) {
    int[] tabla = new int[1000000];
    cte0 = BigInteger.valueOf(0);
    ctem1 = BigInteger.valueOf(-1);
    cte2 = BigInteger.valueOf(2);
    cte3 = BigInteger.valueOf(3);
    cte1000000 = BigInteger.valueOf(1000000);
    for(int z = 0; z < 1000000; z++) {
        BigInteger Z = BigInteger.valueOf(z);
        for(int d = z/3; d < 1000000; d++) {
            BigInteger D = BigInteger.valueOf(d);
            BigInteger Res = Z.multiply(Z).subtract(Z.multiply(D).multiply(cte2)).subtract(D.multiply(D).multiply(cte3)).multiply(ctem1);
            if(Res.compareTo(cte0) < 0) {
                System.out.println("Ojo");
                continue;
            }
            if(Res.compareTo(cte1000000) >= 0) 
                break;
            tabla[(int)Res.longValue()]++;
        }
    }
    int count = 0;
    for(int i = 0; i < 1000000; i++)
        if(tabla[i] == 10) {
            System.out.println(i);
            count++;
        }
    System.out.println(count);
}
}
