"""Problem 243."""
import bisect
import copy

import all_primes


def gcd(a, b):
    if b > a:
        return gcd(b, a)
    r = a % b
    if r == 0:
        return b
    return gcd(b, r)


def check(num, factors, obj, min_so_far):
    all_nums = set(range(1, num))
    for factor in factors:
        if factor in all_nums:
            all_nums.remove(factor)
        for prime in all_primes.PRIMES:
            nnum = factor * prime
            if nnum >= num:
                break
            while nnum < num:
                if nnum in all_nums:
                    all_nums.remove(nnum)
                nnum *= prime
    resil = len(all_nums) / (num - 1)
    if resil < min_so_far:
        # print(f'Min {num} is {resil} {all_nums}')
        print(f'Min {num} is {resil} with factors {factors}')
        min_so_far = resil
    if resil < obj:
        print(f'Found {num} with resilients {all_nums}')
        return -1
    return min_so_far


def prod(iterable):
    res = 1
    for aaa in iterable:
        res *= aaa
    return res


def check_old2(num, factors, obj, min_so_far):
    all_nums = set([1])
    for prime in all_primes.PRIMES:
        if prime >= num:
            break
        if gcd(prime, num) != 1:
            continue
        currents = sorted(copy.copy(all_nums))
        prop = prime
        while prop < num:
            for lasts in currents:
                cand = prop * lasts
                if cand >= num:
                    break
                # if cand in all_nums:
                #     print(f'{cand} was already there...')
                all_nums.add(cand)
                if len(all_nums) / (num - 1) >= min_so_far:
                    return min_so_far
            prop *= prime
    resil = len(all_nums) / (num - 1)
    if resil < min_so_far:
        # print(f'Min {num} is {resil} {all_nums}')
        print(f'Min {num} is {resil} with factors {factors}')
        min_so_far = resil
    if resil < obj:
        print(f'Found {num} with resilients {all_nums}')
        return -1
    return min_so_far


def show_factors(num):
    factors = []
    for prime in all_primes.PRIMES:
        if prime > num:
            break
        while num % prime == 0:
            factors.append(prime)
            num /= prime
    print(factors)


def check_old3(num, factors, obj, min_so_far):
    all_nums = [1]
    added = 1
    for prime in all_primes.PRIMES:
        if prime >= num:
            break
        if prime in factors:
            continue
        currents = copy.copy(all_nums)
        prop = prime
        while prop < num:
            last = bisect.bisect_right(currents, num / prop)
            added += last
            if added / (num - 1) >= min_so_far:
                return min_so_far
            for next_last in currents[:last]:
                cand = prop * next_last
                bisect.insort_right(all_nums, cand)
            prop *= prime
    print(f'{added} for {num} ({factors})')
    show_factors(added)
    resil = added / (num - 1)
    if resil < min_so_far:
        # print(f'Min {num} is {resil} {all_nums}')
        print(f'Min {num} is {resil} with factors {factors}')
        min_so_far = resil
    if resil < obj:
        print(f'Found {num} with resilients {all_nums}')
        return -1
    return min_so_far


def check_old(num, factors, obj, min_so_far):
    old_factor = 1
    added = 1
    dif_factors = 0
    for factor in factors:
        if factor == old_factor:
            added *= factor
        else:
            old_factor = factor
            added *= factor - 1
    # print(f'{added} for {num} ({factors})')
    resil = added / (num - 1)
    if resil < min_so_far:
        # print(f'Min {num} is {resil} {all_nums}')
        # print(f'Min {num} is {resil} with factors {factors}')
        min_so_far = resil
    if resil < obj:
        print(f'Found {num}')  # with resilients {all_nums}')
        return -1
    return min_so_far


def recur_len(max_len, current, current_num, current_prime_pos, obj, min_so_far):
    if len(current) == max_len:
        return check_old(current_num, current, obj, min_so_far)
    for ppos in range(max(current_prime_pos, 0), current_prime_pos + 2):
        current.append(all_primes.PRIMES[ppos])
        min_so_far = recur_len(max_len, current, current_num * all_primes.PRIMES[ppos], ppos, obj, min_so_far)
        if min_so_far == -1:
            return -1
        current.pop(len(current) - 1)
    return min_so_far


def main():
    for length in range(1, 100):
        print(f'Length {length}')
        if recur_len(length, [], 1, -1, 4 / 10, 2) == -1:
            break
    for length in range(8, 100):
        print(f'Length {length}')
        recur_len(length, [], 1, -1, 15499 / 94744, 2)
        #     break


if __name__ == '__main__':
    main()

