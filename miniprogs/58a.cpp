#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int main() {
    enteros primos;

    for(unsigned long long i = 3; i < 1000000000; i += 2) {
        isPrime(primos, i);
    }
    primos.insert(2);
    int total = 0;
    unsigned long long num = 1;
    for(int j = 2; j < (int)sqrt(1000000000); j += 2) {
        num += j;
        if(primos.find(num) != primos.end())
            total++;
        num += j;
        if(primos.find(num) != primos.end())
            total++;
        num += j;
        if(primos.find(num) != primos.end())
            total++;
        num += j;
        if(primos.find(num) != primos.end())
            total++;
    	std::cout << j << " : " << ((100*total)/(2*j + 1)) << std::endl;
	}
	return 0;
}
