import java.util.*;
import java.io.*;
import java.math.*;

class c74 {
    static BigInteger[] factd;
    static public BigInteger sumaD(BigInteger A) {
        String a = A.toString();
        BigInteger B = BigInteger.valueOf(0);
        for(int i = 0; i < a.length(); i++)
            B = B.add(factd[a.charAt(i) - '0']);
        return B;
    }
    static public void main(String[] args) throws IOException {
        factd = new BigInteger[10];
        factd[0] = BigInteger.valueOf(1);
        factd[1] = BigInteger.valueOf(1);
        factd[2] = BigInteger.valueOf(2);
        factd[3] = BigInteger.valueOf(6);
        factd[4] = BigInteger.valueOf(24);
        factd[5] = BigInteger.valueOf(120);
        factd[6] = BigInteger.valueOf(720);
        factd[7] = BigInteger.valueOf(5040);
        factd[8] = BigInteger.valueOf(40320);
        factd[9] = BigInteger.valueOf(362880);
        int cant = 0;
        for(int i = 3; i < 1000000; i++) {
            int tamcadena = 0;
            BigInteger actual = BigInteger.valueOf(i);
            while(true) {
                if(actual.compareTo(BigInteger.valueOf(169)) == 0) {
                    tamcadena += 3;
                    break;
                }
                if(actual.compareTo(BigInteger.valueOf(871)) == 0) {
                    tamcadena += 2;
                    break;
                }
                if(actual.compareTo(BigInteger.valueOf(872)) == 0) {
                    tamcadena += 2;
                    break;
                }
                if(actual.compareTo(BigInteger.valueOf(145)) == 0) {
                    tamcadena += 1;
                    break;
                }
                if(actual.compareTo(BigInteger.valueOf(2)) == 0) {
                    tamcadena += 1;
                    break;
                }
                if(actual.compareTo(BigInteger.valueOf(1)) == 0) {
                    tamcadena += 1;
                    break;
                }
                if(actual.compareTo(BigInteger.valueOf(40585)) == 0) {
                    tamcadena += 1;
                    break;
                }
                tamcadena++;
                if(tamcadena >= 60)
                    System.out.println("Algo pasa " + actual);
                actual = sumaD(actual);
            }
            if(tamcadena == 60)
                cant++;
        }
        System.out.println(cant);
    }
}
