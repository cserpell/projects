#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

int mcd(int a, int b) {
    int na = a;
    int nb = b;
    int aux;
    while(nb != 0) {
        aux = na%nb;
        na = nb;
        nb = aux;
    }
    return na;
}

int phi(int n) {
    int cant = 1;
    for(int i = 2; i < n; i++) {
        if(mcd(n, i) == 1)
            cant++;
    }
//    cout << "saliendo" << endl;
    return cant;
}

int main() {
    double max = 0;
    int cual = 0;
    for(int i = 2; i <= 1000000; i++) {
        if(i%10000 == 0)
            std::cout << i << " -> " << cual << endl;
        double aa = ((double)i)/phi(i);
        if(aa > max) {
            max = aa;
            cual = i;
        }
    }
    std::cout << cual << endl;
	return 0;
}
