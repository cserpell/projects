"""Problem 828."""
import copy
import itertools

MOD = 1005075251
FILE = 'p828_number_challenges.txt'

class Error(Exception):
    """Error raised by tree methods."""


def run_problems(file_name):
    tot_sum = 0
    nnn = 1
    with open(file_name) as the_file:
        while True:
            line = the_file.readline()
            if not line:
                break
            target, nums = line.split(':')
            target = int(target)
            nums = [int(nnn) for nnn in nums.split(',')]
            score = solve_one(target, nums)
            print(f'For problem {target} : {nums}, score is {score}')
            score = (pow(3, nnn, MOD) * score) % MOD
            tot_sum = (tot_sum + score) % MOD
            nnn += 1
    return tot_sum


class Tree(object):
    def __init__(self, num_pos=None, ops_pos=None, left=None, right=None):
        if left is None and right is not None:
            raise Error('Inconsistency with sides...')
        if left is not None and right is None:
            raise Error('Inconsistency with sides...')
        if left is None and num_pos is None:
            raise Error('Missing num pos')
        if left is not None and ops_pos is None:
            raise Error('Missing ops pos')
        self.parent = None
        self.left = left
        self.right = right
        self.leaf = bool(left is None and right is None)
        if self.left is not None:
            self.left.parent = self
        if self.right is not None:
            self.right.parent = self
        self.num_pos = num_pos
        self.ops_pos = ops_pos

    def traverse(self, permutation, ops):
        if self.leaf:
            return permutation[self.num_pos]
        val_left = self.left.traverse(permutation, ops)
        if val_left == -1:
            return -1
        val_right = self.right.traverse(permutation, ops)
        if val_right == -1:
            return -1
        op = ops[self.ops_pos]
        if op == '-' and val_right >= val_left:
            # negative
            return -1
        if op == '/' and (val_right > val_left or val_left % val_right != 0):
            return -1
        if op == '+':
            return val_left + val_right
        if op == '-':
            return val_left - val_right
        if op == '/':
            return val_left // val_right
        return val_left * val_right


def build_all_trees(start_pos, end_pos):
    if end_pos == start_pos:
        return [Tree(num_pos=start_pos)]
    trees = []
    for length in range(1, end_pos - start_pos + 1):
        left_trees = build_all_trees(start_pos, start_pos + length - 1)
        right_trees = build_all_trees(start_pos + length, end_pos)
        for left in left_trees:
            for right in right_trees:
                lcopy = copy.deepcopy(left)
                rcopy = copy.deepcopy(right)
                trees.append(Tree(ops_pos=start_pos + length - 1, left=lcopy, right=rcopy))
    return trees


def solve_one(target, nums):
    min_score = 1000000
    for length in range(1, 6 + 1):
        trees = build_all_trees(0, length - 1)
        # print(trees)
        for permutation in itertools.permutations(nums, length):
            cmin = sum(permutation)
            if cmin >= min_score:
                continue
            for ops in itertools.product('+-/*', repeat=length - 1):
                for tree in trees:
                    res = tree.traverse(permutation, ops)
                    if res == -1 or res != target:
                        continue
                    if cmin < min_score:
                        min_score = cmin
                        print(f'{cmin} {permutation} - {ops}')
    if min_score == 1000000:
        return 0
    return min_score


def main():
    print(run_problems(FILE))


if __name__ == '__main__':
    main()

