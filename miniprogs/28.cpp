#include <iostream>
#include <sstream>

int main() {
    int grilla[1001][1001];
    int nivel = 1;
    int num = 2;
    while(nivel < 501) {
        for(int i = -nivel + 1; i <= nivel; i++) {
            grilla[i + 500][500 - nivel] = num;
            num++;
        }
        for(int i = -nivel + 1; i <= nivel; i++) {
            grilla[500 + nivel][500 + i] = num;
            num++;
        }
        for(int i = -nivel + 1; i <= nivel; i++) {
            grilla[500 - i][500 + nivel] = num;
            num++;
        }
        for(int i = -nivel + 1; i <= nivel; i++) {
            grilla[500 - nivel][500 - i] = num;
            num++;
        }
        nivel++;
    }
    int suma = 1;
    for(int i = 1; i <= 500; i++) {
        std::cout << grilla[500 - i][500 - i] << std::endl;
        suma += grilla[500 - i][500 - i];
        std::cout << grilla[500 - i][500 + i] << std::endl;
        suma += grilla[500 - i][500 + i];
        std::cout << grilla[500 + i][500 - i] << std::endl;
        suma += grilla[500 + i][500 - i];
        std::cout << grilla[500 + i][500 + i] << std::endl;
        suma += grilla[500 + i][500 + i];
    }
	std::cout << suma << std::endl;
	return 0;
}
