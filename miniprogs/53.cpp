#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int triang[101][102];
    for(int i = 0; i < 101; i++)
        for(int j = 0; j < 102; j++)
            triang[i][j] = 0;
    triang[0][0] = 1;
    for(int i = 1; i < 101; i++) {
        for(int j = 0; j <= i; j++) {
            if(j == 0) {
                triang[i][j] = 1;
                continue;
            }
            if(triang[i - 1][j] == -1) {
                triang[i][j] = -1;
                continue;
            }
            if(triang[i - 1][j - 1] == -1) {
                triang[i][j] = -1;
                continue;
            }
            triang[i][j] = triang[i - 1][j] + triang[i - 1][j - 1];
            if(triang[i][j] > 1000000)
                triang[i][j] = -1;
        }
    }
    int contador = 0;
    for(int i = 0; i < 101; i++)
        for(int j = 0; j < 102; j++)
            if(triang[i][j] == -1)
                contador++;
    cout << contador << endl;
}
