import java.util.*;
import java.io.*;
import java.math.*;

class Frac {
    public BigInteger num;
    public BigInteger den;
    public Frac(BigInteger n1, BigInteger d1) {
        num = n1;
        den = d1;
    }
}

class c65 {
    static public int[] tabla;
    static public BigInteger mcd(BigInteger a, BigInteger b) {
        BigInteger na = new BigInteger(a.toString());
        BigInteger nb = new BigInteger(b.toString());
        while(nb.compareTo(BigInteger.valueOf(0)) != 0) {
            BigInteger aux = na.mod(nb);
            na = nb;
            nb = aux;
        }
     //   System.out.println("mcd: " + na);
        return na;
    }
    static public Frac repetir(Frac anterior, int fuera) {
        Frac nueva = new Frac(anterior.den, anterior.num);
        nueva.num = nueva.num.add(nueva.den.multiply(BigInteger.valueOf(fuera)));
        BigInteger mm = mcd(nueva.num, nueva.den);
        nueva.num = nueva.num.divide(mm);
        nueva.den = nueva.den.divide(mm);
        return nueva;
    }
    static public Frac resultado() {
        Frac ini = new Frac(BigInteger.valueOf(1), BigInteger.valueOf(1));
        for(int i = 1; i < 100; i++)
            ini = repetir(ini, tabla[99 - i]);
        return ini;
    }
    static public void main(String[] args) throws IOException {
        tabla = new int[300];
        tabla[0] = 2;
        for(int i = 0; i < 50; i++) {
            tabla[3*i + 1] = 1;
            tabla[3*i + 2] = 2*i + 2;
            tabla[3*i + 3] = 1;
        }
        Frac res = resultado();
        System.out.println(res.num + " , " + res.den);
        int suma = 0;
        String a = res.num.toString();
        for(int i = 0; i < a.length(); i++)
            suma += (a.charAt(i) - '0');
        System.out.println(suma);
    }
}
