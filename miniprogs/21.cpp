#include <iostream>
#include <sstream>

int main() {
    int amic[10010];
    int calc[10010];
    for(int i = 0; i <= 10000; i++) {
        amic[i] = 0;
        calc[i] = 0;
    }
    for(int i = 2; i <= 10000; i++) {
        if(calc[i])
            continue;
        int suma = 0;
        for(int j = 1; j <= i/2; j++) {
            if(i%j == 0) {
                suma += j;
            }
        }
        if(suma > 10000)
            continue;
        calc[i] = suma;
        if(!calc[suma]) {
            int sumab = 0;
            for(int j = 1; j <= suma/2; j++) {
                if(suma%j == 0) {
                    sumab += j;
                }
            }
            if(sumab > 10000)
                continue;
            calc[suma] = sumab;
        }
        if(calc[suma] == i) {
            amic[i] = 1;
            amic[suma] = 1;
            std::cout << i << "->" << calc[i] << " es con " << suma << "->" << calc[suma] << std::endl;
        }
    }
    
    int sum = 0;
    for(int i = 0; i <= 10000; i++) {
        if(amic[i])
            sum += i;
    }
	std::cout << sum << std::endl;
	return 0;
}
