// Aritmetica de intervalos
#include <stdio.h>
#include <stdlib.h>

// Ahora tengo que hacer las operaciones para que el redondeo
// sea como debe ser

/* La idea es representar los pto. flotante asi:
    s = signo, e = exponente, f = fraccion
   (s2)(e1)...(e1)(f1)...(f1)(s2)(e2)...(e2)(f2)...(f2)
   tomaremos LE = 7 y LF = 24 para ocupar en total 8 bytes
*/

#define LE (7)
#define LF (24)
#define SIGPOS (1 << 31)      // 2^31 100000000...0
#define EXPPOS (16777216)     // 2^24 000000010...0
#define EXPAND (2130706432)   //      011111110...0
#define MINFRA (8388608)      //      0000000010..0
#define FRAAND (16777215)     //      000000001...1
#define Q      (64)           //      01000000

typedef struct infloat
{
  float min;
  float max;
  long int nmin;
  long int nmax;
} infloat;

infloat inneg(infloat a)
{
  infloat res;

  res.min = -a.max;
  res.max = -a.min;
  res.nmin = a.nmax ^ SIGPOS;   // OK
  res.nmax = a.nmin ^ SIGPOS;
}

int incmp(infloat a, infloat b)
{
  // CASOS:  ma Ma mb Mb   a < b
  if(a.max < b.min)
    return -1;
  //         mb Mb ma Ma   b < a
  if(b.max < a.min)
    return 1;
  if(a.min < b.min)
  {
    //       ma mb Mb Ma   A contiene B
    if(b.max < a.max)
      return 2;
    //       ma mb Ma Mb   cruzados
    return 0;
  }
  //         mb ma Ma Mb   B contiene A
  if(a.max < b.max)
    return -2;
  //         mb ma Mb Ma   cruzados
  return 0;
}

unsigned char nexp(long int n)
{
  return (n & EXPAND) >> LF;
}

long int nfra(long int n)
{
  return (n & FRAAND) | (n & SIGPOS);
}

long int normalize(unsigned char exp, long int fra)
{
  int nn;//  bool nn;

  if(!fra)
    return 0;

  nn = 0;//  nn = false;
  if(fra & SIGPOS)
  {
    nn = 1;//    nn = true;
    fra ^= SIGPOS;
    printf("cs %d\n", fra);
  }
  while(fra > FRAAND)
  {
    printf("<- %d\n", fra);
    fra >>= 1;      // REDONDEOS ??
    exp++;
  }
  while(fra < MINFRA)
  {
    printf("-> %d\n", fra);
    fra <<= 1;      // REDONDEOS ??
    exp--;
  }
  if(nn)
    fra ^= SIGPOS;
  return fra | (exp << LF);
}

long int sumapar(long int n1, long int n2)
{
  unsigned char e1, e2;
  long int res, f1, f2;

  e1 = (n1 & EXPAND) >> LF;
  e2 = (n2 & EXPAND) >> LF;
  if(e1 < e2)  // Intercambio
  {
    long int temp;
    unsigned char etemp;

    temp = n1;
    n1 = n2;
    n2 = temp;
    etemp = e1;
    e1 = e2;
    e2 = etemp;
  }
  if(e1 != e2)
  {
    if(e1 - e2 < LF)         // Diferencia aceptable
    {
      f2 = n2 & FRAAND;
      f2 = f2 >> (e1 - e2);    // Aqui hay que aproximar, como? ahi veo...
      f2 = f2 | (n2 & SIGPOS);
    } else
      return n1;               // Aqui hay que aproximar, como? ahi veo...
  } else
    f2 = (n2 & FRAAND) | (n2 & SIGPOS);
  f1 = (n1 & FRAAND) | (n1 & SIGPOS);
  f1 += f2;     // Sumo ********
  return normalize(e1, f1);  // Normalizo
}

infloat insum(infloat a, infloat b)
{
  infloat res;

  res.min = a.min + b.min;   // ** D
  res.max = a.max + b.max;   // ** U
  res.nmin = sumapar(a.nmin, b.nmin);
  res.nmax = sumapar(a.nmax, b.nmax);
  return res;
}

infloat indec(infloat a, infloat b) // insum(a, inneg(b))
{
  infloat res;

  res.min = a.min - b.max;   // ** D
  res.max = a.max - b.min;   // ** U
  return res;
}

infloat inmul(infloat a, infloat b)
{
  infloat res;

  res.min = a.min * b.min;   // ** D
  res.max = a.max * b.max;   // ** U
  return res;
}

infloat indiv(infloat a, infloat b)
{
  infloat res;

  res.min = a.min / b.max;   // ** D
  res.max = a.max / b.min;   // ** U
  return res;
}

int main()
{
  char s[40];
  long int a, b, c, d;
  long int p1, p2, pp;

  printf("Un numero:");
  gets(s);
  a = atoi(s);
  printf("Su exponente:");
  gets(s);
  b = atoi(s);
  printf("%d    %d\n", b + Q, a);
  if(a < 0)
    a = (-a) | SIGPOS;
  p1 = normalize(b + Q, a);
  printf("%d    %d\n", nfra(p1), nexp(p1));
  printf("Otro numero:");
  gets(s);
  c = atoi(s);
  printf("Su exponente:");
  gets(s);
  d = atoi(s);
  if(c < 0)
    c = (-c) | SIGPOS;
  p2 = normalize(d + Q, c);
  pp = sumapar(p1, p2);
  printf("%d    %d\n", nfra(p1), nexp(p1));
  printf("%d    %d\n", nfra(p2), nexp(p2));
  printf("%d    %d\n", nfra(pp), nexp(pp));
  gets(s);
  return 0;
}

