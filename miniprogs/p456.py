
def side(p1, p2):  # , p3):
  #a = (p1[0] * p3[1] + p2[0] * p1[1] + p3[0] * p2[1] -
  #     p1[0] * p2[1] - p2[0] * p3[1] - p3[0] * p1[1])
  a = (p2[0] * p1[1] - p1[0] * p2[1])
  if a > 0:
    return 1
  if a < 0:
    return -1
  return 0  # In line


def test_has_origin(p1, p2, p3):
  s1 = side(p1, p2)  # , (0, 0))
  if s1 == 0:
    return False
  s2 = side(p2, p3)  # , (0, 0))
  if s2 == 0:
    return False
  if s1 != s2:
    return False
  s3 = side(p3, p1)  # , (0, 0))
  if s3 == 0:
    return False
  if s1 != s3:
    return False
  return True


def modpot(a, b, mod):
  if b == 0:
    return 1
  if b == 1:
    return a % mod
  met = b / 2
  m2 = modpot(a, met, mod)
  if 2 * met == b:
    return (m2 * m2) % mod
  return (m2 * m2 * a) % mod


class A:

  def __init__(self):
    self._p = [(-14913, -6630)]
    self._last_n = 1
    self._c = [0]

  def get_new_point(self):
    new_p_x = modpot(1248, self._last_n + 1, 32323) - 16161
    new_p_y = modpot(8421, self._last_n + 1, 30103) - 15051
    return (new_p_x, new_p_y)

  def increment_n(self):
    new_point = self.get_new_point()
    self._p.append(new_point)
    new_triangles = 0
    for i in range(self._last_n):
      s3 = side(new_point, self._p[i])
      if s3 == 0:
        continue
      for j in range(i + 1, self._last_n):
        s2 = side(self._p[j], new_point)
        if s2 == 0:
          continue
        if s2 != s3:
          continue
        s1 = side(self._p[i], self._p[j])
        if s1 != s2:
          continue
        new_triangles += 1
    new_c = new_triangles + self._c[-1]
    self._c.append(new_c)
    self._last_n += 1

  def c_n(self, n):
    while self._last_n < n:
      self.increment_n()
    return self._c[n - 1]


def main():
  a = A()
  for n in [8, 600, 40000]:
    print '%s %s' % (n, a.c_n(n))
    # print '%s' % a._p


if __name__ == '__main__':
  main()
