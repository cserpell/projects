import java.util.*;
import java.io.*;
import java.math.*;

class c89 {
    static public int valor(char c) {
      switch(c) {
       case 'M':
        return 1000;
       case 'D':
        return 500;
       case 'C':
        return 100;
       case 'L':
        return 50;
       case 'X':
        return 10;
       case 'V':
        return 5;
       case 'I':
        return 1;
       default:
        break;
      }
      return -1;
    }
    static public String imprime(int n) {
      String res = "";
      while(n > 1000) {
        res = res + "M";
        n = n - 1000;
      }
      if(n >= 900) {
        res = res + "CM";
        n = n - 900;
      }
      if(n >= 500) {
        res = res + "D";
        n = n - 500;
      }
      if(n >= 400) {
        res = res + "CD";
        n = n - 400;
      }
      while(n >= 100) {
        res = res + "C";
        n = n - 100;
      }
      if(n >= 90) {
        res = res + "XC";
        n = n - 90;
      }
      if(n >= 50) {
        res = res + "L";
        n = n - 50;
      }
      if(n >= 40) {
        res = res + "XL";
        n = n - 40;
      }
      while(n >= 10) {
        res = res + "X";
        n = n - 10;
      }
      if(n >= 9) {
        res = res + "IX";
        n = n - 9;
      }
      if(n >= 5) {
        res = res + "V";
        n = n - 5;
      }
      if(n >= 4) {
        res = res + "IV";
        n = n - 4;
      }
      while(n >= 1) {
        res = res + "I";
        n = n - 1;
      }
      if(n != 0) {
        System.err.println("ERROR " + n);
        System.exit(0);
      }
      return res;
    }
    static public void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int suma = 0;
        while(sc.hasNext()) {
          String thisn = sc.next();
          int lorig = thisn.length();
          if(lorig == 0)
            break;
          int num = 0;
          char last = 'K';
          boolean lastadded = true;
          for(int i = 0; i < thisn.length(); i++) {
            char este = thisn.charAt(i);
            if(este == last) {
              if(!lastadded)
                num += valor(last);
              num += valor(este);
              lastadded = true;
            } else {
              if(!lastadded) {
                if(valor(este) < valor(last))
                  num += valor(last);
                else {
                  num += valor(este) - valor(last);
                  lastadded = true;
                }
              } else
                lastadded = false;
              last = este;
            }
          }

          if(!lastadded)
            num += valor(last);
          
          String ires = imprime(num);
          
          int flen = ires.length();
          System.out.println(ires);
          
          suma += lorig - flen;
        }
        
        System.out.println(suma);
    }
}
