#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int main() {
    enteros primos;

    for(unsigned long long i = 3; i < 100000000; i += 2) {
        isPrime(primos, i);
    }
    primos.insert(2);
    unsigned long long num = 0;
    unsigned long long pot = 10;
    enteros::iterator it = primos.begin();
    it++;
    it++;
    enteros::iterator it2 = it;
    it2++;
    while((*it) < 1000000) {
        if((*it) > pot)
            pot *= 10;
        for(enteros::iterator it3 = primos.begin(); it3 != primos.end(); it3++) {
            unsigned long long val = (*it2)*(*it3);
            if((val % pot) == (*it)) {
                num += val;
                cout << val << " , " << (*it) << " " << (*it2) << " * " << (*it3) << endl;
                break;
            }
        }
        it++;
        it2++;
	}
	cout << num << endl;
	return 0;
}
