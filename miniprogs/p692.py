"""Problem 692."""
import bisect

WINNING = {}


def get_winning(n, m):
    if n <= 2 * m:
        return True
    ttuple = (n, m)
    if ttuple not in WINNING:
        is_win = False
        for sel in range(1, 2 * m + 1):
            if not get_winning(n - sel, sel):
                is_win = True
                break
        WINNING[ttuple] = is_win
    return WINNING[ttuple]


def compute_min_for(n):
    if n <= 1:
        return n
    for m in range(1, n // 3 + 1):
        if not get_winning(n - m, m):
            return m
    return n


def print_mat(mat_n):
    print(f'For {max_n}:')
    for line in mat_n:
        print(line)


def show_mat(max_n):
    mat_n = [[1 if get_winning(n, m) else 0 for m in range(1, max_n + 1)] for n in range(1, max_n + 1)]
    # print_mat(mat_n)
    sum_far = sum(compute_min_for(n) for n in range(1, max_n + 1))
    print(f'sum G({max_n}) = {sum_far} .. diff = {compute_min_for(max_n)}')


FIBS = [1, 2]
FIBS_SET = {1, 2}
GFIBS = {1: 1, 2: 3}


def get_last_fib(max_n):
    while max_n > FIBS[-1]:
        new_val = FIBS[-2] + FIBS[-1]
        # print(f'new fib {new_val}')
        FIBS.append(new_val)
        FIBS_SET.add(new_val)
    if max_n in FIBS_SET:
        return max_n
    return FIBS[bisect.bisect_right(FIBS, max_n) - 1]


def get_gval(num):
    if num < 1:
        return 0
    if num == 1:
        return 1
    last_fib = get_last_fib(num)
    # print(f'last fib of {num} is {last_fib}')
    if last_fib == num:
        if not last_fib in GFIBS:
            GFIBS[last_fib] = get_gval(last_fib - 1) + last_fib
        return GFIBS[last_fib]
    return get_gval(last_fib) + get_gval(num - last_fib)


def main():
    for nn in range(1, 64):
        show_mat(nn)
        print(f'estimated G({nn}) = {get_gval(nn)}')
    # show_mat(1)
    # show_mat(4)
    # show_mat(8)
    # show_mat(13)
    # show_mat(17)
    # show_mat(18)
    # show_mat(40)
    # show_mat(63)
    print(get_gval(23416728348467685))


if __name__ == '__main__':
    main()
