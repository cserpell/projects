
def rev_str(string):
  l = len(string)
  return ''.join([string[l - i - 1] for i in range(l)])


def seq(n):
  if n == 2:
    return set(['BA'])
  prev = seq(n - 1)
  news = set()
  for p in prev:
    new_prev = ''.join([chr(ord(c) + 1) for c in p])
    for i in range(len(p) - 1):
      news.add(''.join([rev_str(new_prev[i + 1:]), 'A', new_prev[:i + 1]]))
  # print '%s : %s' % (n, len(news))
  return news


def main():
  print str(seq(4))
  print str(sorted(seq(11))[2010])


if __name__ == '__main__':
  main()

