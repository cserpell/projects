"""Problem 109."""


THROWS = {}
THROWS.update({f'S{num}': num for num in range(1, 21)})
THROWS.update({f'D{num}': 2 * num for num in range(1, 21)})
THROWS.update({f'T{num}': 3 * num for num in range(1, 21)})
THROWS.update({'0': 0})
THROWS.update({'S25': 25, 'D25': 2 * 25})


def build_all_checkouts():
    all_throws = sorted(THROWS.keys())
    checkouts = {}
    for l1 in all_throws:
        for l2 in all_throws:
            for l3 in all_throws:
                if not l3.startswith('D'):
                    continue
                if l1 == '0' and l2 == '0':
                    the_set = tuple([l3])
                elif l1 == '0':
                    the_set = tuple([l2, l3])
                elif l2 == '0':
                    the_set = tuple([l1, l3])
                else:
                    the_set = tuple(sorted([l1, l2]) + [l3])
                sum_set = sum([THROWS[val] for val in the_set])
                checkouts[the_set] = sum_set
    return checkouts


def main():
    checkouts = build_all_checkouts()
    print(len(checkouts))
    num = 0
    num6 = 0
    for key, val in checkouts.items():
        if val < 100:
            num += 1
            if val == 6:
                num6 += 1
    print(num)
    print(num6)


if __name__ == '__main__':
    main()
