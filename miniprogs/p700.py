"""Problem 700."""
I = 1504170715041707
Q = 4503599627370517


def next_n(ttt):
    if ttt == 0:
        return 0
    frac = Q / I
    next_k = 1
    while True:
        min_n = int(frac * next_k - ttt / I)
        max_n = int(frac * next_k)
        num = (((min_n % Q) * I) % Q + ttt) % Q
        for nnn in range(min_n, max_n + 1):
            if num < ttt:
                return num
            num = (num + I) % Q
        next_k += 1


def get_last_values(max_val):
    accomp = pow(I, Q - 2, Q)
    print(f'Pre last n would be {accomp}')
    out_list = []
    for val in range(1, max_val):
        nnn = (accomp * val) % Q
        out_list.append((nnn, val))
    out_list = sorted(out_list, key=lambda x: x[0])
    min_val = max_val
    eulercoin_sum = 0
    for nnn, val in out_list:
        if val < min_val:
            print(f'New eulercoin {val} for n = {nnn}')
            eulercoin_sum += val
            min_val = val
            if min_val == 1:
                break
    return eulercoin_sum


def main():
    sum_eul = I
    num = I
    while True:
        num = next_n(num)
        if num == 0:
            break
        print(f'Next Eulercoin: {num}')
        sum_eul += num
        print(f'Sum so far: {sum_eul}')
        if num < 100000000:
            sum_eul += get_last_values(num)
            break
    print(sum_eul)


if __name__ == '__main__':
    main()
