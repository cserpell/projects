"""
0 : 1777
1 : 3157729
2 : 52437441
3 : 18628481
4 : 4367361
Repeating 4367361 at 62503
"""

SEEN = {}


def test(m, n):
  tes = 1
  SEEN[1] = 0
  for i in xrange(1, n):
    tes = (tes * m) % 100000000
    if i < 5:
      print '%s : %s' % (i, tes)
    if tes in SEEN:
      print 'Repeating %s at %s ' % (tes, i)
      print m
      print (m ** i) % 100000000
      break
    SEEN[tes] = i


def potmod(a, b, m):
  if b == 0:
    return 1
  if b == 1:
    return a
  c = b / 2
  d = potmod(a, c, m)
  res = (d * d) % m
  if 2 * c != b:
    res = (res * a) % m
  return res


def elev(n):
  prev = 1777
  while n > 2:
    prev = potmod(1777, prev, 1250000)
    n -= 1
  return potmod(1777, prev, 100000000)


def main():
  # test(1777, 10000000)
  print elev(1)
  print elev(2) % 100000000
  print elev(1855)


if __name__ == '__main__':
  main()

