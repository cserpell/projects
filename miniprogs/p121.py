"""Problem 121."""
import itertools
import math


def compute(max_len):
    total = 0
    denom = math.factorial(max_len + 1)
    print(denom)
    print(math.ceil((max_len + 1) / 2))
    for mini_len in range(math.ceil((max_len + 1) / 2), max_len + 1):
        for permut in itertools.combinations(list(range(1, max_len + 1)), max_len - mini_len):
            prob = 1
            print(permut)
            for num in permut:
                prob *= num
            total += prob / denom
    print(total)
    nums = 1 // total
    print(nums)


compute(4)
print('***************')
compute(15)
