"""Problem 719."""
import itertools
import math


COMPUTED_DIGS = {0: [0]}
TENS = [10 ** i for i in range(13)]
PSUMS = {tuple(): 0, (0,): 0}


def get_digs(num, min_len=1):
    if num not in COMPUTED_DIGS:
        lll = int(math.log10(num))
        if lll == 0:
            COMPUTED_DIGS[num] = [num]
        else:
            half = lll // 2
            if half != lll:
                # print(f'Dividing by {TENS[half + 1]}')
                COMPUTED_DIGS[num] = get_digs(num // TENS[half + 1], min_len=min_len - half - 1) + get_digs(num % TENS[half + 1], min_len=half + 1)
            else:
                digs = []
                while num > 0:
                    rem = num % 10
                    digs.insert(0, rem)
                    num = (num - rem) // 10
                COMPUTED_DIGS[num] = digs
    return [0 for _ in range(min_len - len(COMPUTED_DIGS[num]))] + COMPUTED_DIGS[num]


def get_partial_sum(digs):
    ttuple = tuple(digs)
    if ttuple not in PSUMS:
        ldigs = len(digs)
        if ldigs > 1:
            half = ldigs // 2
            PSUMS[ttuple] = TENS[half] * get_partial_sum(digs[:ldigs - half]) + get_partial_sum(digs[ldigs - half:])
        else:
            pre_num = 0
            for dig in digs:
                pre_num *= 10
                pre_num += dig
            PSUMS[ttuple] = pre_num
    return PSUMS[ttuple]


def main(big_n):
    """Main execution point."""
    tot_sum = 0
    for xxx in range(1, int(math.sqrt(big_n)) + 1):
        nnn = xxx ** 2
        # print(f'Trying x = {xxx}, n = {nnn}')
        # digs = get_digs(nnn)
        len_digs = int(math.ceil(math.log10(nnn + 1)))
        list_pos = list(range(len_digs - 1))
        # print(f'{digs} of len {len_digs} and positions {list_pos}')
        found = False
        for numb_comb in range(1, len_digs):
            # print(f'Exploring numb com {numb_comb}')
            for divs in itertools.combinations(list_pos, numb_comb):
                # print(f'{divs}')
                sum_num = 0
                for div_pos, one_div in enumerate(divs):
                    pre_num = (nnn // TENS[len_digs - (one_div + 1)]) % TENS[one_div + 1 - (divs[div_pos - 1] + 1 if div_pos != 0 else 0)]
                    # pre_num = get_partial_sum(digs[divs[div_pos - 1] + 1 if div_pos != 0 else 0:one_div + 1])
                    # print(pre_num)
                    sum_num += pre_num
                pre_num = nnn % TENS[len_digs - (divs[-1] + 1 if numb_comb > 0 else 0)]
                # pre_num = get_partial_sum(digs[divs[-1] + 1 if numb_comb > 0 else 0:len_digs])
                # print(pre_num)
                sum_num += pre_num
                # print(f'The sum is {sum_num}')
                if sum_num == xxx:
                    print(f'Match in n = {nnn}, divs = {divs}, sum is {sum_num}')
                    tot_sum += nnn
                    found = True
                    break
            if found:
                break
    print(tot_sum)


if __name__ == '__main__':
    main(1000000000000)

