#include <iostream>
#include <cmath>
#include <set>

using namespace std;

int quigon[10];
int used[10];

void rec(int pos) {
    if(pos == 10) {
        int suma[5];
        // filled, verify
        suma[0] = quigon[0] + quigon[2] + quigon[4] + 3;
        suma[1] = quigon[1] + quigon[4] + quigon[7] + 3;
        suma[2] = quigon[6] + quigon[7] + quigon[8] + 3;
        suma[3] = quigon[3] + quigon[6] + quigon[9] + 3;
        suma[4] = quigon[5] + quigon[3] + quigon[2] + 3;
        if(suma[0] == suma[1] && suma[1] == suma[2] && suma[2] == suma[3]
            && suma[3] == suma[4]) {
            if(quigon[0] < quigon[1] && quigon[0] < quigon[8] &&
                quigon[0] < quigon[9] && quigon[0] < quigon[5]) {
                cout << "0: ";
            }
            if(quigon[1] < quigon[0] && quigon[1] < quigon[8] &&
                quigon[1] < quigon[9] && quigon[1] < quigon[5]) {
                cout << "1: ";
            }
            if(quigon[8] < quigon[0] && quigon[8] < quigon[1] &&
                quigon[8] < quigon[9] && quigon[8] < quigon[5]) {
                cout << "8: ";
            }
            if(quigon[9] < quigon[0] && quigon[9] < quigon[1] &&
                quigon[9] < quigon[8] && quigon[9] < quigon[5]) {
                cout << "9: ";
            }
            if(quigon[5] < quigon[0] && quigon[5] < quigon[1] &&
                quigon[5] < quigon[8] && quigon[5] < quigon[9]) {
                cout << "5: ";
            }
            cout << (quigon[0]+1) << " " <<
            (quigon[2]+1) << " " <<
            (quigon[4]+1) << " " <<
            (quigon[1]+1) << " " <<
            (quigon[4]+1) << " " <<
            (quigon[7]+1) << " " <<
            (quigon[8]+1) << " " <<
            (quigon[7]+1) << " " <<
            (quigon[6]+1) << " " <<
            (quigon[9]+1) << " " <<
            (quigon[6]+1) << " " <<
            (quigon[3]+1) << " " <<
            (quigon[5]+1) << " " <<
            (quigon[3]+1) << " " <<
            (quigon[2]+1) << " " << endl;
        }
        return;
    }
    for(int i = 0; i < 10; i++) {
        if(used[i])
            continue;
        used[i] = 1;
        quigon[pos] = i;
        rec(pos + 1);
        used[i] = 0;
    }
}

int main() {
    for(int i = 0; i < 10; i++)
        used[i] = 0;
    rec(0);
    return 0;
}
