#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long mcd(unsigned long long a, unsigned long long b) {
    while(b != 0) {
        unsigned long long aux = a % b;
        a = b;
        b = aux;
    }
    return a;
}

int main() {
    unsigned long long cantidad = 0;
    for(unsigned long long m = 1; m <= 1000000000LL; m++) {
        for(unsigned long long n = 1; n < m; n++) {
            unsigned long long a = m*m - n*n;
            unsigned long long b = 2*m*n;
            unsigned long long c = m*m + n*n;
            unsigned long long per = a + b + c;
            if(per >= 100000000000000LL)
                break;
            if((m % 2) != 0 && (n % 2) != 0)
                continue;
            if(mcd(m, n) != 1)
                continue;
            /* A relative pythagorean triplet */
            long long dif1;
            long long dif2;
            dif1 = b - 2*a;
	    if(dif1 == 1 || dif1 == -1) {
		cantidad += c;
		cout << (2*a) << " . " << b << " = " << c <<  " .. " << cantidad << endl;	    		    
	    }
            dif2 = a - 2*b;
	    if(dif2 == 1 || dif2 == -1) {
		cantidad += c;
		cout << a << " . " << (2*b) << " = " << c <<  " .. " << cantidad << endl;	    		    
	    }
        }
    }
    cout << cantidad << endl;
    return 0;
}
