#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <list>

using namespace std;

int dijkstra(int *mat) {
    list<int> vecinos[80*80];
    int visitado[80*80];
    for(int i = 0; i < 80; i++) {
        for(int j = 0; j < 80; j++) {
            if(i > 0)
                vecinos[80*i + j].push_back(80*(i - 1) + j);
            if(j > 0)
                vecinos[80*i + j].push_back(80*i + (j - 1));
            if(i < 79)
                vecinos[80*i + j].push_back(80*(i + 1) + j);
            if(j < 79)
                vecinos[80*i + j].push_back(80*i + (j + 1));
            visitado[80*i + j] = 0;
        }
    }
    int val[80*80];
    visitado[0] = 1;
    for(int i = 1; i < 80*80; i++)
        val[i] = -1;
    val[0] = mat[0];
    val[1] = mat[1] + mat[0];
    val[80] = mat[80] + mat[0];
    cout << "hola" << endl;
    for(int i = 0; i < 80*80 - 1; i++) {
        int min = -1;
        int cual = -1;
        for(int j = 0; j < 80*80; j++) {
            if(visitado[j])
                continue;
             if(min == -1 || (val[j] != -1 && val[j] < min)) {
                min = val[j];
                cual = j;
             }
        }
        cout << min << " <- " << cual << endl;
        visitado[cual] = 1;
        for(list<int>::iterator it = vecinos[cual].begin(); it != vecinos[cual].end(); it++) {
            int n = *it;
            if(visitado[n])
                continue;
            if(val[n] == -1 || val[n] > val[cual] + mat[n]) {
                val[n] = val[cual] + mat[n];            
            }
        }
    }
    return val[80*80 - 1];
}

int main() {
    int *mat = new int[80*80];
    for(int i = 0; i < 80; i++) {
        string str;
        cin >> str;
        int upos = -1;
        for(int j = 0; j < 80; j++) {
            int nupos = str.find(",", upos + 1);
            int a = atoi(str.substr(upos + 1, nupos - upos - 1).c_str());
            mat[j + 80*i] = a;
            cout << a << ",";
            upos = nupos;
        }
        cout << endl;
    }
    cout << dijkstra(mat) << endl;
	return 0;
}
