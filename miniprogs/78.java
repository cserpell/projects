import java.util.*;
import java.lang.*;
import java.io.*;
import java.math.*;

class c78 {
    static public BigInteger[][] suma78;
    static public int maxcalculado78;
    static public BigInteger BI0;
    static public BigInteger BI1;
    static public BigInteger BI1000000;
    static public BigInteger f78r(int cantidad, int tammax) {
        if(cantidad == 0)
            return BI1;
        if(cantidad < tammax)
            return f78r(cantidad, cantidad);
        if(suma78[cantidad][tammax] != null)
            return suma78[cantidad][tammax];
        BigInteger valor = BI0;
        int mmax = tammax;
        if(cantidad < tammax)
            mmax = cantidad;
        for(int i = mmax; i > 0; i--) {
            valor = valor.add(f78r(cantidad - i, i));
        }
        suma78[cantidad][tammax] = valor;
        return valor;
    }
    static public void main(String[] args) throws IOException {
        int CANT = 5000;
        suma78 = new BigInteger[CANT][];
        for(int i = 1; i < CANT; i++) {
            suma78[i] = new BigInteger[i + 1];
            for(int j = 1; j <= i; j++) {
                suma78[i][j] = null;
            }
        }
        BI0 = BigInteger.valueOf(0);
        BI1 = BigInteger.valueOf(1);
        BI1000000 = BigInteger.valueOf(1000000);
        maxcalculado78 = 0;
        for(int i = 1; i < CANT; i++) {
            f78r(i, i);
            maxcalculado78 = i;
            System.out.println(i + ": " + suma78[i][i]);
            if((suma78[i][i]).mod(BI1000000).compareTo(BI0) == 0)
                break;
        }
    }
}
