import java.io.*;
import java.math.*;
import java.util.*;
import java.lang.*;

class c140 {

static public BigInteger BI0;
static public BigInteger BI1;
static public BigInteger BI2;
static public BigInteger BI5;
static public BigInteger BI14;

    static boolean raiz(BigInteger n) {
        // orig = 5*n*n + 14*n + 1
        BigInteger orig = n.multiply(n).multiply(BI5).add(n.multiply(BI14)).add(BI1);
        BigInteger ret = orig.divide(BI2);
        BigInteger limsup = orig;
        BigInteger lret = BI1;
        BigInteger liminf = BI1;
        while(true) {
            BigInteger res = ret.multiply(ret);
            int rr = res.compareTo(orig);
            if(rr == 0)
                return true;
            if(limsup.compareTo(liminf) == 0)
                return false;
            if(ret.compareTo(lret) == 0)
                return false;
            lret = ret;
            if(rr > 0) {
                limsup = ret;
                ret = liminf.add(ret).divide(BI2);
            } else {
                liminf = ret;
                ret = limsup.add(ret).divide(BI2);
            }
        }
    }
    
static public void main(String[] args) {
    BI1 = BigInteger.valueOf(1);
    BI2 = BigInteger.valueOf(2);
    BI5 = BigInteger.valueOf(5);
    BI14 = BigInteger.valueOf(14);
    BI0 = BigInteger.valueOf(0);
    BigInteger i = BigInteger.valueOf(5);
    BigInteger suma = BI2;
    BigInteger lastDif = BI2;
    BigInteger actualDif = BigInteger.valueOf(3);
    BigInteger actualDif2 = BI0;
    int cant = 2;
//    int cuatro = 1;
    while(true) {
   /*     i = i.add(BI1);
        actualDif = actualDif.add(BI1);*/
  /*      cuatro++;
        if(cuatro == 3)
            continue;
        if(cuatro == 4)
            cuatro = 0;
    */    if(raiz(i)) {
            suma = suma.add(i);
            System.out.println(cant + ": " + i + " -> " + suma + "  ad = " + actualDif);
            cant++;
            BigInteger nuevaDif = actualDif.multiply(BI5).add(actualDif.subtract(lastDif));
            BigInteger nuevaDif2 = nuevaDif.add(actualDif.add(lastDif).subtract(actualDif2));
            i = i.add(nuevaDif);
            suma = suma.add(i);
            System.out.println(cant + ": " + i + " -> " + suma + "  ld = " + lastDif);
            cant++;
            i = i.add(nuevaDif2);
            lastDif = nuevaDif;
            actualDif2 = actualDif;
            actualDif = nuevaDif2;
            if(cant > 32)
                break;
        }
    }
}

}
