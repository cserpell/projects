#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int gcd(int a, int b)
{
    if( a < 0 ) a = -a;
    if( b < 0 ) b = -b;
    while(b != 0)
    {
        int t = b;
        b = a % b;
        a = t;
    }
    return a;
}

int main(int argc, char **argv)
{
    int p,q,n,s;

    n = atoi(argv[1]);

    p = 0;
    s = 0;
    while(1)
    {
        p++;
        if( p*p > n/2 )
            break;
        if( p & 1 )
            q = 2;
        else
            q = 1;
        for(;q<p;q+=2)
        {
            unsigned int a,b,c;
            int l;
            if( gcd(p,q) != 1 )
                continue;
            a = p*p - q*q;
            b = 2 * p * q;
            c = p*p + q*q;
            if( c > n/2 || (b+c) > n )
                break;
            if( (a+b+c) > n )
                continue;

            l = (a>b)?(a-b):(b-a);
            if( c % l == 0 )
            {
                printf("%d %d %d = %d  l=%d (%d,%d) Total:%d\n",a,b,c,a+b+c,l,p,q,n/(a+b+c));
                s += n/(a+b+c);
            }
        }
    }
    printf("Total: %d\n",s);
    return 0;
}

