#include <iostream>
#include <cmath>

int ocupado[10];
int arr[10];
int d[10];
int p10[10] = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};
int sumatotal;

void recur(int pos) {
    if(pos == 10) {
        for(int i = 2; i <= 8; i++) {
            d[i] = arr[i - 1]*100 + arr[i]*10 + arr[i + 1];
        }
        if(d[2] % 2 != 0)
            return;
        if(d[3] % 3 != 0)
            return;
        if(d[4] % 5 != 0)
            return;
        if(d[5] % 7 != 0)
            return;
        if(d[6] % 11 != 0)
            return;
        if(d[7] % 13 != 0)
            return;
        if(d[8] % 17 != 0)
            return;
        for(int i = 0; i < 10; i++) {
            std::cout << arr[i];
            sumatotal += arr[i]*p10[10 - i - 1];
        }
        std::cout << std::endl;
        return;
    }
    for(int i = 9; i >= 0; i--) {
        if(!ocupado[i]) {
            arr[pos] = i;
            ocupado[i] = 1;
            recur(pos + 1);
            ocupado[i] = 0;
        }
    }
}
int main() {
    sumatotal = 0;
    for(int i = 0; i < 10; i++)
        ocupado[i] = 0;
    recur(0);
    std::cout << sumatotal << std::endl;
    return 0;
}
