#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

int mcd(int a, int b) {
    while(b != 0) {
        int c = a/b;
        int r = a - b*c;
        a = b;
        b = r;
    }
    return a;
}

int main() {
    int minnum = 2;
    int minden = 7;
    for(int i = 1; i <= 1000000; i++) {
	int den = i;
	int num = (3*den - 1)/7;
	int aa = mcd(den, num);
	den = den/aa;
	num = num/aa;
	if(num*minden > minnum*den) {
		std::cout << num << "/" << den << " > " << minnum << "/" << minden << std::endl;
	    minnum = num;
	    minden = den;
	}
    }
    std::cout << minnum << std::endl;
	return 0;
}
