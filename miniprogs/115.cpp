#include <iostream>
#include <cmath>

using namespace std;

unsigned long long C[1000000];

int main() {
    for(int i = 0; i < 50; i++)
        C[i] = 1;
    unsigned long long presuma = 0;
    int pos = 50;
    while(1) {
        if(C[pos - 1] > 1000000)
            break;
        C[pos] = presuma + C[pos - 1] + 1;
        presuma += C[pos - 50];
        pos++;
    }
    cout << (pos - 1) << endl;    
	return 0;
}
