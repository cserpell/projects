#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

int isPrime(int n) {
    for(int i = 3; i <= sqrt(n); i++)
        if(n%i == 0)
            return 0;
    return 1;
}

int main() {
    int primos[79000];
    int maximo = 0;
    int pr = 1;
    
    primos[0] = 2;
    for(int i = 3; i < 1000000; i += 2) {
        if(isPrime(i)) {
            primos[pr] = i;
            pr++;
        }
    }
    cout << pr << endl;
    return 0;
    int tabla[79000][79000];
/*    tabla = new int *[pr];
    for(int i = 0; i < pr; i++)
        tabla[i] = new int[pr];*/
    for(int i = 0; i < pr; i++) {
        tabla[i][i] = primos[i];
        for(int j = 1; j < pr; j++) {
            tabla[i][j] = tabla[i][j - 1] + primos[j];
            for(int k = 0; k < pr; k++) {
                if(tabla[i][j] == primos[k])
                    maximo = tabla[i][j];
                if(tabla[i][j] < primos[k])
                    break;
            }
        }
    }
    std::cout << maximo << endl;
	return 0;
}
