#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

int isPrime(int i) {
        for(int j = 3; j <= sqrt(i); j++) {
            if(i%j == 0)
                return 0;
        }
    return 1;
}

int main() {
    int cual = 2;
    for(int i = 3; i <= 1000000; i += 2) {
        if(isPrime(i)) {
            int nuevo = cual*i;
            if(nuevo > 1000000)
                break;
            cual = nuevo;
        }
    }
    std::cout << cual << endl;
	return 0;
}
