#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned int> enteros;

unsigned int largodivide(unsigned int numero) {
    unsigned int repeticiones = 1;
    unsigned int resto = 1;
    enteros lista;
    while(resto != 0) {
        while(resto < numero) {
   //         cout << "probando " << resto << endl;
            if(lista.find(resto) != lista.end()) {
                return 0;
            }
            lista.insert(resto);
            resto = resto*10 + 1;
            repeticiones++;
        }
        resto = resto % numero;
     //   cout << "llego " << resto << endl;
    }
    return repeticiones;
}

int main() {
    unsigned int i;
    for(i = 1000001; i < 10000000; i += 2) {
        if((i % 5) != 0) {
            unsigned int largo = largodivide(i);
            cout << i << " -> " << largo << endl;
            if(largo > 1000000)
                break;
        }
    }
    cout << i << endl;
	return 0;
}
