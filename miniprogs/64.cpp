#include <iostream>
#include <cmath>
#include <cstring>
#include <string>
#include <map>
#include <utility>

using namespace std;

int mcd(int a, int b) {
  if(b == 1)
    return 1;
  if(a == b)
    return a;
  int c = a % b;
  if(c == 0)
    return b;
  return mcd(b, c);
}

class puntito {
 public:
  int f;
  int n;
  int p;
  puntito(int ff, int nn, int pp) {
    f = ff;
    n = nn;
    p = pp;
  }
  int operator<(const puntito &otro) const {
    if(f != otro.f)
      return f - otro.f;
    if(n != otro.n)
      return n - otro.n;
    return 0;
  }
  bool operator==(puntito &otro) {
    return f == otro.f && n == otro.n;
  }
};

struct strCmp {
  bool operator()( const char* s1, const char* s2 ) const {
    return strcmp( s1, s2 ) < 0;
  }
};

//map<const char *, int, strCmp> todos;
map<puntito, int> todos;

int total;

int siguiente(int dentro, int fuera, int num, double sqr, int paso) {
  // Recibe una fraccion 1/(sqrt(dentro) - fuera)
//  if(paso > 10)
  //  return -1;
 // int casi = sqrt(dentro);
  double nnum = sqr + fuera;
  int nden = dentro - fuera*fuera;
  int mmcd = mcd(nden, num);
  nden = nden/mmcd;
  if(num != mmcd) {
    cerr << "ERROR !!! " << num << " - " << nden << endl;
    return -1;
  }
 // num = num/mmcd;
  int nuevo = nnum/nden;
//  cout << nuevo <<  "   -- " << fuera << " " << num << endl;
  //char *ccc = new char[40];
  //sprintf(ccc, "%d...%d...%d%c", fuera, num, paso, 0);
  const puntito p(fuera, num, paso);
//  map<const char *, int, strCmp>::iterator it = todos.find(ccc);
  map<puntito, int>::iterator it = todos.find(p);
  if(it == todos.end()) {
//    todos.insert(ccc, paso);
    todos.insert(make_pair(p, paso));
    siguiente(dentro, nden*nuevo - fuera, nden, sqr, paso + 1);
  } else {
    // Ya estaba, hay un ciclo
    int per = (paso - (*it).first.p);
    cout << "Periodo : " << paso << " - " << (*it).first.p << " = " << per << endl;
    if(per % 2)
      total++;
    return nuevo;
  }
  return nuevo;
}

int main() {
    //int N = 13;
    total = 0;
    for(int N = 2; N <= 10000; N++) {
      int SN = (int)sqrt((double)N);
      if(SN*SN == N)
        continue;
      cout << N << " ";
      todos.clear();
      siguiente(N, SN, 1, sqrt((double)N), 1);
    }
    cout << "TOTAL : " << total << endl;
    return 0;
}
