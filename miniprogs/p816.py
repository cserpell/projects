"""Problem 816."""
import bisect
import math

S0 = 290797
Q = 50515093
KF = lambda r: r[0]


def bin_search(coll, min_p, max_p, num):
    if min_p > max_p:
        return max_p
    mid_p = min_p + ((max_p - min_p) // 2)
    val = coll[mid_p][0]
    if num < val:
        return bin_search(coll, mid_p + 1, max_p, num)
    if num > val:
        return bin_search(coll, min_p, mid_p, num)
    return mid_p


def add_one(last_x, last_y, sorted_x_points, min_dist):
    new_x = (last_y * last_y) % Q
    new_y = (new_x * new_x) % Q
    # print(f'New ({new_x}, {new_y})')
    min_x = bisect.bisect_left(sorted_x_points, new_x - min_dist, key=KF)
    # print(min_x)
    max_x = bisect.bisect_right(sorted_x_points, new_x + min_dist, key=KF)
    # print(max_x)
    for pos in range(min_x, max_x + 1):
        if pos >= 0 and pos < len(sorted_x_points):
            ddd = math.sqrt((new_x - sorted_x_points[pos][0]) ** 2 +
                            (new_y - sorted_x_points[pos][1]) ** 2)
            if ddd < min_dist:
                min_dist = ddd
                print(f'New min dist {min_dist}')
    bisect.insort_right(sorted_x_points, (new_x, new_y), key=KF)
    return new_x, new_y, min_dist


def main(max_n):
    """Main execution point."""
    n_x = S0
    n_y = (S0 * S0) % Q
    sorted_x_points = [(n_x, n_y)]
    # print(f'New ({n_x}, {n_y})')
    min_dist = Q + 1
    for n in range(max_n - 1):
        if n % 100000 == 0:
            print(n)
        n_x, n_y, min_dist = add_one(n_x, n_y, sorted_x_points, min_dist)
        # print(f'Current min dist {min_dist}')
    print(min_dist)


if __name__ == '__main__':
    main(2000000)

