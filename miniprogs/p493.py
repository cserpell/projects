import random


def test_one():
  remain = [10, 10, 10, 10, 10, 10, 10]
  total = 70
  #tests = 0
  #colors = 0
  seen_colors = set()
  while total > 50:
    one = random.randint(0, 6)
    if not remain[one]:
      # No more balls of this color
      continue
    remain[one] -= 1
    seen_colors.add(one)
    #tests += 1
    total -= 1
  return len(seen_colors)


def tries(iters):
  #expected = 0.
  tries = 0
  global_exp = 0
  while iters > tries:
    tries += 1
    #one_try = test_one()
    #expected = (expected * (tries - 1) + one_try) / tries
    global_exp += test_one()
    if tries % 100000 == 0:
      print 'After %s iters, expected is %s' % (tries, global_exp * 1.0 / tries)
  print 'After %s iters, expected is %s' % (tries, global_exp * 1.0 / tries)


def matrix(global_mat, n, p):
  if p > 20 or p <= 0:
    return 0
  if global_mat[n][p] != -1:
    return global_mat[n][p]
  if n == 1:
    if p <= 10:
      global_mat[n][p] = 1
      return 1
    global_mat[n][p] = 0
    return 0
  if p < n:
    global_mat[n][p] = 0
    return 0
  new_s = 0
  for i in range(10):
    new_s += matrix(global_mat, n - 1, p - i - 1)
  global_mat[n][p] = new_s
  return new_s


def direct():
  global_mat = [
    [-1] * 21,
    [-1] * 21,
    [-1] * 21,
    [-1] * 21,
    [-1] * 21,
    [-1] * 21,
    [-1] * 21,
    [-1] * 21,
  ]
  sum_s = 0
  total = 0
  for n in range(7):
    mm = matrix(global_mat, n + 1, 20)
    print '%s %s' % (n + 1, mm)
    sum_s += mm * (n + 1)
    total += mm
  print 'Final: %s' % (float(sum_s) / total)


def main():
  tries(1000000000L)

if __name__ == '__main__':
  main()

