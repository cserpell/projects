#include <iostream>
#include <set>
#include <cmath>

typedef std::set< unsigned int > conjunto;

int main() {
    conjunto pentas;
    for(unsigned int i = 1; i < 10000; i++)
        pentas.insert((i*(3*i - 1))/2);
    for(conjunto::const_iterator it = pentas.begin(); it != pentas.end(); it++) {
        conjunto::const_iterator it2 = it;
        it2++;
        for(; it2 != pentas.end(); it2++) {
            conjunto::const_iterator enc = pentas.find((*it) + (*it2));
            if(enc == pentas.end())
                continue;
            enc = pentas.find((*it2) - (*it));
            if(enc != pentas.end()) {
                std::cout << ((*it2) - (*it)) << std::endl;
                return 0;
            }
        }
        std::cout << (*it) << std::endl;
    }
    return 0;
}
