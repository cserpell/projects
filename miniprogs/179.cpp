#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

unsigned long long count_divisors(unsigned long long num) {
  unsigned long long ndivs = 1;
  enteros::iterator it;
  for(it = primos.begin(); it != primos.end(); it++) {
    unsigned long long p = *it;
    if(p > num)
      break;
    unsigned long long este = 1;
    while(num % p == 0) {
      num = num/p;
      este++;
    }
    ndivs *= este;
  }
  return ndivs;
}

unsigned long long big_array[10000002];

void llenar_con(unsigned long long p, unsigned long long hasta) {
  unsigned long long pactual = 2;
  unsigned long long pp = p;
  while(pp <= hasta) {
    for(unsigned long long k = pp; k <= hasta; k += pp) {
      big_array[k] = (big_array[k]/(pactual - 1))*pactual;
    }
    pp = pp*p;
    pactual = pactual + 1;
  }
}

int main(int argc, char *argv[]) {
    unsigned long long i;
    unsigned long long Hasta = 10000000LL;
    if(argc > 1)
      Hasta = atoi(argv[1]);
    for(i = 3; i <= Hasta; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    cout << "Fin llenado de primos" << endl;
    for(unsigned long long aa = 1; aa <= Hasta; aa++) {
      big_array[aa] = 1;
    }
    
    for(enteros::iterator it = primos.begin(); it != primos.end(); it++) {
      llenar_con(*it, Hasta);
    }
    
    unsigned long long total = 0;
    unsigned long long last_divs = 1;
    for(unsigned long long aa = 2; aa <= Hasta; aa++) {
      unsigned long long nuevos_divs = big_array[aa];
      cout << aa << ": " << nuevos_divs << endl;
      if(nuevos_divs == last_divs)
        total++;
      last_divs = nuevos_divs;
    }
    cout << "Total = " << total << endl;
/*    unsigned long long total = 0;
    unsigned long long last_divs = 1;
    for(i = 2; i <= 10000000LL; i++) {
      unsigned long long nuevos_divs = count_divisors(i);
      if(nuevos_divs == last_divs)
        total++;
      last_divs = nuevos_divs;
    }
    cout << "Total = " << total << endl;*/
	return 0;
}
