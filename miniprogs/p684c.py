MOD = 1000000007


def big_s(last_pos, many):
    last_base = pow(10, last_pos, MOD)
    # t_sum = 0
    """
    base = 1
    for pos in range(min(last_pos, 13)):
        t_sum += (base * (9 * (many + 9 * (last_pos - (pos + 1))) + 5 * 9)) % MOD
        # print(t_sum)
        base *= 10
        base %= MOD
    """
    # t_sum = (many + 6) * (last_base - 1) - 9 * last_pos % MOD
    # t_sum = (many + 9 * last_pos - 4) * (last_base - 1) - (10 - last_pos * last_base + (last_pos - 1) * last_base * 10) % MOD
    # t_sum += (last_base * int(many * (many + 1) / 2)) % MOD
    # print(t_sum)
    first = int((3 * many + 12 + many * many) / 2) % MOD
    t_sum = (first * last_base - 9 * last_pos - many - 6) % MOD
    return t_sum


def fib_k_r(f1, f2):
    k1, r1 = f1
    k2, r2 = f2
    nk = k1 + k2
    nr = r1 + r2
    if nr >= 9:
        nr -= 9
        nk += 1
    return nk, nr


def main():
    f1 = (0, 0)
    f2 = (0, 1)
    # print('Check: s(10) = {}'.format(f_s(10)))
    print('Check: S(0) = {}'.format(big_s(0, 0)))
    print('Check: S(1) = {}'.format(big_s(0, 1)))
    print('Check: S(2) = {}'.format(big_s(0, 2)))
    print('Check: S(3) = {}'.format(big_s(0, 3)))
    print('Check: S(5) = {}'.format(big_s(0, 5)))
    print('Check: S(8) = {}'.format(big_s(0, 8)))
    print('Check: S(9) = {}'.format(big_s(1, 0)))
    print('Check: S(10) = {}'.format(big_s(1, 1)))
    print('Check: S(11) = {}'.format(big_s(1, 2)))
    print('Check: S(20) = {}'.format(big_s(2, 2)))
    tot_sum = 0
    for i in range(2, 91):
        ff = fib_k_r(f1, f2)
        print('current fib {}: {}'.format(i, ff))
        tot_sum += big_s(*ff) % MOD
        f1 = f2
        f2 = ff
        print('sum so far: {}'.format(tot_sum))
    print(tot_sum % MOD)


if __name__ == '__main__':
  main()

