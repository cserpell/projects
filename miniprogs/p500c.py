max_lim = 1000000
primes = [(2, 1)]
print('2')
i = 3
while i < max_lim:
    is_prime = True
    for j in primes:
        if j[0] * j[0] > i:
            break
        if (i % j[0]) == 0:
            is_prime = False
            break
    if is_prime:
        print(i)
        primes.append((i, 1))
    i = i + 2


fff = lambda x: x[0]


def main():
    choices = primes
    log_divisors = 0
    current = 1
    while log_divisors < 500502:
        next_num = min(choices, key=fff)
        choices.remove(next_num)
        vvv = next_num[0]
        nums = 1  # next_num[1]
        choices.append((vvv * vvv, nums * 2))
        log_divisors += nums
        current = (vvv * current) % 500500507
        print('Divisors {}: {}'.format(log_divisors, current))


if __name__ == '__main__':
  main()

