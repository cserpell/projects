import sys

NPOINTS = 20

def get_area (points, areas, i, j, k) :
#  if areas.has_key( (i, j, k) ) :
#    return areas[ (i, j, k) ]
  a = area2(points[i], points[j], points[k])
#  areas[(i, j, k)] = a
#  areas[(j, k, i)] = a
#  areas[(k, i, j)] = a
#  areas[(i, k, j)] = -a
#  areas[(k, j, i)] = -a
#  areas[(j, i, k)] = -a
  return a

if len(sys.argv) > 1 :
  NPOINTS = int(sys.argv[1])

def area2 (p1, p2, p3) :
  ar = p1[0] * p2[1] - p2[0] * p1[1]
  ar = ar + p2[0] * p3[1] - p3[0] * p2[1]
  ar = ar + p3[0] * p1[1] - p1[0] * p3[1]
  return ar

s0 = 290797
s1 = (s0 * s0) % 50515093
s0 = (s1 * s1) % 50515093

i = 1
points = []
while i <= NPOINTS :
  t0 = ( s1 % 2000 ) - 1000
  t1 = ( s0 % 2000 ) - 1000
  points.append( ( t0 , t1 ) )
  s1 = (s0 * s0) % 50515093
  s0 = (s1 * s1) % 50515093
  i = i + 1

# Here the list of points is ready
for p in points :
  print str(p)

# Compute all areas at once
areas = {}

# Now, a list of admisible triangles!
def add_triangle (points, triangles, i, j, k) :
  a = min(i, j)
  a = min(a, k)
  c = max(i, j)
  c = max(c, k)
  b = i + j + k - a - c
  if not triangles.has_key( (a, b, c) ) :
    triangles[ (a, b, c) ] = 1
  return

triangles = {}

added_points = []

# Create all triangles
i = 0
while i < NPOINTS :
  j = i + 1
  print j
  while j < NPOINTS :
    k = j + 1
    while k < NPOINTS :
      triangles[ (i, j, k) ] = 1
#      add_triangle(points, triangles, i, j, k)
      k = k + 1
    j = j + 1
  i = i + 1

# Delete inadmissible triangles
i = 0
while i < NPOINTS :
  print i
  for t in triangles.keys() :
    a1 = get_area ( points, areas, i, t[0], t[1] )
    a2 = get_area ( points, areas, i, t[1], t[2] )
    a3 = get_area ( points, areas, i, t[2], t[0] )
    if ( a1 > 0 and a2 > 0 and a3 > 0 ) or ( a1 < 0 and a2 < 0 and a3 < 0 ) :
      # inside !
      triangles.pop(t)
  i = i + 1
  # All inadmissible triangles have been deleted at this point

for t in triangles.keys() :
  print str(points[t[0]]) + " - " + str(points[t[1]]) + " - " + str(points[t[2]]) + " - " + str(get_area(points , areas, t[0], t[1], t[2] ) )

print "Admissible triangles: " + str(len(triangles))
