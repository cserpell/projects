import sys
import random

def completeTab (Tabl) :
  if Tabl[0] == [0,0,0,0,0] :
    return True
  return False

def moveAnt (pos, Tabl) : # pos = [x,y,status] current position of ant
  x = pos[0]              # status = 1 if it has seed
  y = pos[1]
  s = pos[2]
  if y == 0 and x == 0 :
    r = random.randint(1, 2)
    if r == 1 :
      nx = x + 1
      ny = y
    else :
      nx = x
      ny = y + 1
  elif y == 4 and x == 4 :
    r = random.randint(1, 2)
    if r == 1 :
      nx = x - 1
      ny = y
    else :
      nx = x
      ny = y - 1
  elif y == 0 and x == 4 :
    r = random.randint(1, 2)
    if r == 1 :
      nx = x - 1
      ny = y
    else :
      nx = x
      ny = y + 1
  elif y == 4 and x == 0 :
    r = random.randint(1, 2)
    if r == 1 :
      nx = x + 1
      ny = y
    else :
      nx = x
      ny = y - 1
  elif y == 4 :
    r = random.randint(1, 3)
    if r == 1 :
      nx = x - 1
      ny = y
    elif r == 2 :
      nx = x + 1
      ny = y
    else :
      nx = x
      ny = y - 1
  elif x == 4 :
    r = random.randint(1, 3)
    if r == 1 :
      nx = x - 1
      ny = y
    elif r == 2 :
      nx = x
      ny = y + 1
    else :
      nx = x
      ny = y - 1
  elif y == 0 :
    r = random.randint(1, 3)
    if r == 1 :
      nx = x - 1
      ny = y
    elif r == 2 :
      nx = x + 1
      ny = y
    else :
      nx = x
      ny = y + 1
  elif x == 0 :
    r = random.randint(1, 3)
    if r == 1 :
      nx = x
      ny = y + 1
    elif r == 2 :
      nx = x + 1
      ny = y
    else :
      nx = x
      ny = y - 1
  else :
    r = random.randint(1, 4)
    if r == 1 :
      nx = x - 1
      ny = y
    elif r == 2 :
      nx = x + 1
      ny = y
    elif r == 3 :
      nx = x
      ny = y - 1
    else :
      nx = x
      ny = y + 1
  if s == 0 and Tabl[ny][nx] == 1 :
    ns = 1
    Tabl[ny][nx] = 0
  elif s == 1 and Tabl[ny][nx] == -1 :
    ns = 0
    Tabl[ny][nx] = 0
  else :
    ns = s
  pos[0] = nx
  pos[1] = ny
  pos[2] = ns
  return

iters = 100
if len(sys.argv) > 1 :
  iters = int(sys.argv[1])

totalSteps = 0
i = 0
while i < iters :
  Tabl = [[-1,-1,-1,-1,-1],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[1,1,1,1,1]]
  currentPos = [2, 2, 0]
  j = 0
  while True:
  #  print Tabl
    if completeTab(Tabl) :
    #  print "It ended at " + str(j) + " steps"
      totalSteps = totalSteps + j
      break
    moveAnt(currentPos, Tabl)
    j = j + 1
  i = i + 1
  if ( i % 1000 ) == 0 :
    print "After " + str(i) + " iterations : " + str(totalSteps * 10000000 // i)
print "=" * 60
print "After " + str(iters) + " iterations : " + str(totalSteps * 10000000 // iters)
