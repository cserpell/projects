#include <iostream>
#include <cmath>

int isPrime(int *tabla, unsigned int n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    for(unsigned long long i = 3; i <= hasta; i += 2) {
        if(tabla[i] && (n % i) == 0) {
            tabla[n] = 0;
            return 0;
        }
    }
    tabla[n] = 1;
    return 1;
}

int main() {
    int *tabla;

    tabla = new int[500000000];
    for(unsigned long long i = 0; i < 500000000; i ++) {
        tabla[i] = 0;
    }
    for(unsigned long long i = 3; i < 500000000; i += 2) {
        isPrime(tabla, i);
    }
    tabla[2] = 1;
    int total = 0;
    unsigned long long num = 1;
    for(int j = 2; j < (int)sqrt(500000000); j += 2) {
        num += j;
        if(isPrime(tabla, num))
            total++;
        num += j;
        if(isPrime(tabla, num))
            total++;
        num += j;
        if(isPrime(tabla, num))
            total++;
        num += j;
        if(isPrime(tabla, num))
            total++;
    	std::cout << j << " : " << ((100*total)/(2*j + 1)) << std::endl;
	}
	return 0;
}
