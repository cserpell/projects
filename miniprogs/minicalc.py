import sys

if len(sys.argv) < 2 :
  print "Please provide the number!"
  quit()

num = int(sys.argv[1])

lo = 1
ma = num

while lo < ma :
  p = (ma - lo) // 2
  n = lo + p
  if n*n < num :
    lo = n + 1
  else :
    ma = n

print "lo = " + str(lo)
print "ma = " + str(ma)
