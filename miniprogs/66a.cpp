#include <iostream>
#include <set>
#include <cmath>

using namespace std;

typedef std::set<unsigned long long> enteros;

void llenarTabla(int d, enteros &tabla) {
    tabla.clear();
    tabla.insert(1);
    for(int i = 2; i < d; i++) {
        if(((i*i) % d) == 1)
            tabla.insert(i);
    }
}

int main() {
    unsigned long long maxx = 0;
    int which = 0;
    enteros lista;
    for(int D = 2; D <= 1000; D++) {
        unsigned long long p = sqrt(D);
        if(p*p == D)
            continue;
        llenarTabla(D, lista);
        for(unsigned long long k = 0; k < 100000000; k++) {
            double q = D*k*(double)k;
            int found = 0;
            enteros::iterator it = lista.begin();
            if(k == 0)
                it++;
            for( ; it != lista.end(); it++) {
          //      cout << "testing " << (k*D + (*it)) << endl;
                int a = (*it)*(*it) - 1;
                if((a % D) != 0)
                    continue;
                double t = q + 2*k*(*it) + a/D;
                unsigned long long y = sqrt(t);
                if(fabs((y*(double)y) - t) < 0.00001) {
                    unsigned long long x = k*D + (*it);
                    if(x > maxx) {
                        which = D;
                        maxx = x;
                    }
                    std::cout << D << ": " << x << " , " << y << "\t" << which << ": " << maxx << std::endl;
                    found = 1;
                    break;
                }
            }
            if(found)
                break;
        }
    }
    std::cout << which << std::endl;
    return 0;
}
