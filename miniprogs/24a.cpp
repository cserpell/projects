#include <iostream>
#include <sstream>
#include <cmath>

int *digpos;
int *ocupado;
int iter;

int recur(int prof) {
    if(prof == 10) {
        iter++;
        if(iter == 1000000) {
            for(int i = 0; i < 10; i++)
                std::cout << digpos[i];
            std::cout << std::endl;
            return 1;
        }
        return 0;
    }
    for(int i = 0; i < 10; i++) {
        if(!ocupado[i]) {
            digpos[prof] = i;
            ocupado[i] = 1;
            if(recur(prof + 1))
                return 1;
            ocupado[i] = 0;
        }
    }
    return 0;
}

int main() {
    ocupado = new int[10];
    digpos = new int[10];
    for(int i = 0; i < 10; i++)
        ocupado[i] = 0;
    iter = 0;
    recur(0);
	return 0;
}
