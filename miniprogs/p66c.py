import math

import all_primes


# Limit 1000000L: x = 24248647 dx = 688
# Limit 2000000L: x = 51999603 dx = 698
# Limit 2500000L: x = 67606199 dx = 781
# Limit 10000000L: x = 263091151 dx = 742
# Limit 15000000L: x = 446526729 dx = 965
# Limit 20000000L: x = 541601801 dx = 861
# Limit 100000000L: x = 2095256249 dx = 955
# Limit 150000000L: x = 2989136930 dx = 771
# Limit 300000000L: x = 6681448801 dx = 931

class P66(object):
  """Problem 66."""
  
  def start_useful_primes(self):
    return [prime for prime in all_primes.PRIMES if prime <= self._max_n]

  def start_completed_set(self):
    completed = {}
    n = 1
    while n * n <= self._max_n:
      completed[n * n] = -1
      n += 1
    return completed
  
  def __init__(self, max_n=1000):
    self._max_n = max_n
    self._max_x = 0
    self._max_dx = 0
    self._start_y = 1
    self._completed = self.start_completed_set()
    self._primes = self.start_useful_primes()

  def inject_previous_status(self, remaining, previous_limit):
    """Starts with previous results."""
    self._completed = dict((number, -1) for number in range(2, self._max_n + 1)
                           if number in self._completed or
                           number not in remaining)
    self._start_y = previous_limit

  def factor(self, number):
    """Get list of primes diviing number."""
    primes = {}
    for prime in self._primes:
      if prime > number:
        break
      while number % prime == 0:
        if prime not in primes:
          primes[prime] = 0
        primes[prime] += 1
        number = number / prime
    return sorted([(prime, times) for prime, times in primes.iteritems()])

  def recursive_factors(self, cand_x, so_far, primes, pos):
    if pos >= len(primes):
      # print 'It is also a solution for d = %s' % so_far
      self._completed[so_far] = cand_x
      return
    prime = primes[pos][0]
    my_d = so_far
    for times in range(primes[pos][1] + 1):
      self.recursive_factors(cand_x, my_d, primes, pos + 1)
      my_d = my_d * prime * prime
      if my_d > self._max_n:
        break

  def process_sol(self, number, cand_x, cand_y):
    """Processes a solution."""
    # print 'Found sol for d = %s : x = %s y = %s' % (number, cand_x, cand_y)
    if self._max_x < cand_x:
      self._max_x = cand_x
      self._max_dx = number
    # Fill all numbers that are formed by d * y * y
    self.recursive_factors(cand_x, number, [], 0)
    # factors = self.factor(cand_y)
    # self.recursive_factors(cand_x, number, factors, 0)
    
  def iter_number(self, number):
    """Iterates for one number of D."""
    print 'Iterating on d: %s max x: %s max dx: %s' % (
      number, self._max_x, self._max_dx)
    cand_y = self._start_y
    while cand_y <= 300000000L:
      sq_x = cand_y * cand_y * number + 1
      cand_x = int(math.sqrt(sq_x))
      if cand_x * cand_x == sq_x:
        # This x works
        self.process_sol(number, cand_x, cand_y)
        return
      cand_y += 1
    print 'Max iteration reached for %s !' % number

  def main(self):
    self.inject_previous_status([149, 157, 181, 193, 199, 211, 214, 241, 271, 277, 281, 298, 301, 309, 313, 317, 331, 334, 337, 353, 358, 367, 379, 382, 394, 397, 409, 412, 421, 433, 436, 449, 454, 457, 461, 463, 477, 478, 481, 487, 489, 491, 493, 501, 508, 509, 517, 521, 523, 526, 538, 541, 547, 553, 554, 556, 565, 569, 571, 581, 586, 589, 593, 596, 597, 599, 601, 604, 607, 610, 613, 614, 617, 619, 622, 628, 631, 634, 637, 641, 643, 649, 652, 653, 661, 669, 673, 679, 681, 685, 686, 691, 694, 709, 716, 718, 719, 721, 724, 739, 746, 749, 751, 753, 757, 764, 766, 769, 772, 773, 778, 787, 789, 796, 797, 801, 809, 811, 814, 821, 823, 826, 829, 835, 838, 844, 849, 853, 856, 857, 859, 862, 863, 865, 869, 871, 877, 881, 883, 886, 889, 907, 911, 913, 919, 921, 922, 926, 929, 937, 941, 946, 947, 949, 953, 956, 958, 964, 967, 970, 971, 972, 974, 977, 981, 988, 989, 991], 300000000L)
    d = 1
    while d < self._max_n:
      d += 1
      if d in self._completed:
        continue
      self.iter_number(d)
    remaining = [number for number in range(2, self._max_n + 1)
                 if number not in self._completed and
                 int(math.sqrt(number)) * int(math.sqrt(number)) != number]
    print remaining
    print self._max_x


if __name__ == '__main__':
  P66().main()

