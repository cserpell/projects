"""Problem 103."""
import collections
import copy


def ZERO():
  return 0


class P103(object):
  """Problem 103."""

  def __init__(self, last_number=7):
    self._last_number = last_number
    self._min_sets_min = 1000000000
    self._min_sets_set = None
    self._partial_sums = None
    self._number = 0
    self._so_far = None

  def make_number(self, number):
    self._number = number
    self._min_sets_min = 1000000000
    self._min_sets_set = []
    self._so_far = [0] * number
    self._partial_sums = collections.defaultdict(ZERO)
    self.recursive_test(0, 0, 0)

  def check_condition(self, pos, new_item):
    if pos == 0:
      return True
    if new_item <= self._so_far[pos - 1]:
      print 'Trying %s cannot be because A n = %s so_far = %s pos = %s' % (new_item, self._number, self._so_far, pos)
      return False
    last = pos / 2 - 1 if pos % 2 == 0 else (pos - 1) / 2 - 1
    for item in range(0, last + 1):
      if new_item >= self._partial_sums[(0, item + 1)] - self._partial_sums[(pos - item, pos - 1)]:
        print 'Trying %s cannot be because B item = %s n = %s so_far = %s pos = %s' % (new_item, item, self._number, self._so_far, pos)
        return False
    return True

  def update_partial_sums(self, pos, next_item):
    self._partial_sums[(pos, pos)] = next_item
    for item in range(0, pos):
      self._partial_sums[(item, pos)] = self._partial_sums[(item, pos - 1)] + next_item

  def recursive_test(self, sum_so_far, last_so_far, pos):
    if pos >= self._number:
      if sum_so_far < self._min_sets_min:
        self._min_sets_min = sum_so_far
        self._min_sets_set = copy.copy(self._so_far)
        print 'Best so far: %s %s' % (sum_so_far, self._so_far)
      return
    trying = last_so_far + 1
    max_try = self._partial_sums[(0, 1)] - 1 if pos > 1 else self._number * 10
    while trying <= max_try:
      next_sum = sum_so_far + trying
      if next_sum >= self._min_sets_min:
        print '%s cannot be because sum is too big' % trying
        break
      self._so_far[pos] = trying
      if self.check_condition(pos, trying):
        self.update_partial_sums(pos, trying)
        self.recursive_test(next_sum, trying, pos + 1)
      trying += 1

  def main(self):
    for number in range(1, self._last_number):
      print 'Starting length %s' % number
      self.make_number(number)


if __name__ == '__main__':
  P103(5).main()

