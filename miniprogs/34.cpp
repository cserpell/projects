#include <iostream>
#include <sstream>
#include <cmath>

int isPalin10(unsigned int n) {
    int d1, d2, d3, d4, d5, d6;
    
    d1 = n/100000;
    d2 = (n % 100000)/10000;
    d3 = (n % 10000)/1000;
    d4 = (n % 1000)/100;
    d5 = (n % 100)/10;
    d6 = n % 10;
    //std::cout << d1 << d2 << d3 << d4 << d5 << d6 << std::endl;
    if(d1 == 0) {
        if(d2 == 0) {
            if(d3 == 0) {
                if(d4 == 0) {
                    if(d5 == 0) {
                        std::cout << n << std::endl;
                        return 1;
                    }
                    if(d6 == d5) {
                        std::cout << n << std::endl;
                        return 1;
                    }
                }
                if(d4 == d6) {
                    std::cout << n << std::endl;
                    return 1;
                }
            }
            if(d3 == d6 && d4 == d5) {
                std::cout << n << std::endl;
                return 1;
            }
        }
        if(d2 == d6 && d3 == d5) {
            std::cout << n << std::endl;
            return 1;
        }
    }
    if(d1 == d6 && d2 == d5 && d3 == d4) {
        std::cout << n << std::endl;
        return 1;
    }
    return 0;
}

int isFact(unsigned int *tabla, unsigned int n) {
    int dm1, d0, d1, d2, d3, d4, d5, d6;
    
    dm1 = n/10000000;
    d0 = (n % 10000000)/1000000;
    d1 = (n % 1000000)/100000;
    d2 = (n % 100000)/10000;
    d3 = (n % 10000)/1000;
    d4 = (n % 1000)/100;
    d5 = (n % 100)/10;
    d6 = n % 10;
    if(dm1 == 0) {
    if(d0 == 0) {
    if(d1 == 0) {
        if(d2 == 0) {
            if(d3 == 0) {
                if(d4 == 0) {
                    if(tabla[d6] + tabla[d5] == n) {
                        std::cout << n << std::endl;
                        return 1;
                    }
                    return 0;
                }
                if(tabla[d6] + tabla[d5] + tabla[d4] == n) {
                    std::cout << n << std::endl;
                    return 1;
                }
                return 0;
            }
            if(tabla[d6] + tabla[d5] + tabla[d4] + tabla[d3] == n) {
                std::cout << n << std::endl;
                return 1;
            }
            return 0;
        }
        if(tabla[d6] + tabla[d5] + tabla[d4] + tabla[d3] + tabla[d2] == n) {
            std::cout << n << std::endl;
            return 1;
        }
        return 0;
    }
    if(tabla[d6] + tabla[d5] + tabla[d4] + tabla[d3] + tabla[d2] + tabla[d1] == n) {
        std::cout << n << std::endl;
        return 1;
    }
    return 0;
    }
    if(tabla[d6] + tabla[d5] + tabla[d4] + tabla[d3] + tabla[d2] + tabla[d1] + tabla[d0] == n) {
        std::cout << n << std::endl;
        return 1;
    }
    return 0;
    }
    if(tabla[d6] + tabla[d5] + tabla[d4] + tabla[d3] + tabla[d2] + tabla[d1] + tabla[d0] + tabla[dm1] == n) {
        std::cout << n << std::endl;
        return 1;
    }
    return 0;
}

int main() {
    unsigned int *tabla;
    
    tabla = new unsigned int[10];
    unsigned int val = 1;
    for(int i = 0; i < 10; i++) {
        tabla[i] = val;
        val *= (i + 1);
    }
    for(int i = 0; i < 10; i++) {
        std::cout << tabla[i] << std::endl;
    }
    unsigned int cant = 0;
    for(unsigned int i = 10; i < 100000000; i++) {
        if(isFact(tabla, i)) {
            std::cout << i << " sirve!!" << std::endl;
            cant += i;
        }
    }
    std::cout << (cant) << std::endl;
	return 0;
}
