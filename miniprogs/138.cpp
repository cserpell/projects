#include <iostream>
#include <cmath>
#include <set>

using namespace std;

int verify(unsigned long long a, unsigned long long b, unsigned long long c) {
    return (fabs(a*(double)a + b*(double)b - c*(double)c) < 0.0001);
}

int main() {
    unsigned long long suma = 0;
    for(unsigned long long l = 1; l < 2000000000; l++) {
        double pr = 5*l*(double)l - 1;
        unsigned long long r = sqrt(pr);
        if(fabs(r*(double)r - pr) > 0.0001)
            continue;
        double b1 = (-4 + 2*(double)r)/5.;
        double b2 = (4 + 2*(double)r)/5.;
//        if(r > 2 && b1 % 5 == 0) {
        if(fabs(((unsigned long long)b1) - b1) < 0.0001) {
//            unsigned long long res = b1/5;
            unsigned long long res = b1;
        //    if(verify(res/2, res + 1, l))
                cout << "b = " << res << ", h = " << (res + 1) << " L1 = " << l << endl;
/*            if(verify(res/2, res - 1, l))
                cout << "b = " << res << ", h = " << (res - 1) << " L1 = " << l << endl;
  */          suma += l;
        }
//        if(b2 % 5 == 0) {
        if(fabs(((unsigned long long)b2) - b2) < 0.0001) {
//            unsigned long long res = b2/5;
            unsigned long long res = b2;
   /*         if(verify(res/2, res + 1, l))
                cout << "b = " << res << ", h = " << (res + 1) << " L2 = " << l << endl;
     */  //     if(verify(res/2, res - 1, l))
                cout << "b = " << res << ", h = " << (res - 1) << " L2 = " << l << endl;
            suma += l;
        }
    }
    cout << suma << endl;
    return 0;
}
