#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

int isPrime(int *tabla, unsigned int n) {
    unsigned int hasta = ((int)sqrt(n)) + 1;
    for(unsigned int i = 3; i <= hasta; i += 2) {
        if(tabla[i] && (n % i) == 0) {
            tabla[n] = 0;
            return 0;
        }
    }
    tabla[n] = 1;
    return 1;
}

int main() {
    int *tabla;
    
    tabla = new int[100000000];
    /*for(unsigned int i = 0; i < 100000000; i ++) {
        tabla[i] = 0;
    }
    for(unsigned int i = 3; i < 100000000; i += 2) {
        isPrime(tabla, i);
    }*/
    std::ifstream salida;
    salida.open("primos");
    for(unsigned int i = 0; i < 100000000; i++) {
        int a;
        salida >> a;
        salida >> tabla[a];
    }
    salida.close();
//    std::cout << "salida grabada!" << std::endl;
    //return 0;
    unsigned int maxcant = 0;
    int cuala = -1;
    int cualb = -1;
    for(int i = -999; i < 1000; i++) {
        for(int j = -999; j < 1000; j++) {
            unsigned int n = 0;
            while(tabla[n*n + i*n + j]) {
                n++;
            }
            if(n > maxcant) {
                maxcant = n;
                cuala = i;
                cualb = j;
            }
        }
    }
    std::cout << maxcant << ": " << (cuala*cualb) << std::endl;
	return 0;
}
