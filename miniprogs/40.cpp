#include <iostream>
#include <sstream>
#include <cmath>

int main() {
    unsigned int *tabla;
    
    tabla = new unsigned int[2000000];
    unsigned int val = 1;
    int pot = 10;
    int len = 1;
    for(int i = 0; i < 1200000;) {
        for(int j = len - 1; j >= 0; j--) {
            int d = (val % ((int)pow(10, j + 1)))/((int)pow(10, j));
            tabla[i] = d;
            i++;
        }
        val++;
        if(val >= pot) {
            len++;
            pot *= 10;
        }
    }
    for(int i = 0; i < 10000; i++) {
        std::cout << tabla[i];
    }
    std::cout << std::endl;
    std::cout << "1: " << tabla[0] << std::endl;
    std::cout << "10: " << tabla[9] << std::endl;
    std::cout << "100: " << tabla[99] << std::endl;
    std::cout << "1000: " << tabla[999] << std::endl;
    std::cout << "10000: " << tabla[9999] << std::endl;
    std::cout << "100000: " << tabla[99999] << std::endl;
    std::cout << "1000000: " << tabla[999999] << std::endl;
    std::cout << "Res: " << (tabla[0]*tabla[9]*tabla[99]*tabla[999]*tabla[9999]*tabla[99999]*tabla[999999]) << std::endl;
    for(int i = 0; i < 20; i++)
        std::cout << (i + 1) << ": " << tabla[i] << std::endl;
	return 0;
}
