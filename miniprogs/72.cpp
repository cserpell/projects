#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int mcd(int a, int b) {
    int r = b;
    int d = a;
    while(r != 0) {
        int p = d%r;
        d = r;
        r = p;
    }
    return d;
}

/*int distintos(int val) {
    int res = 0;
    for(int i = 1; i < val; i++) {
        if(mcd(val, i) == 1)
            res++;
    }
    return res;
}*/

int distintos(enteros & primos, int val) {
    double res = val;
    int hasta = (val/2) + 1;
    for(enteros::iterator it = primos.begin(); it != primos.end(); it++) {
        if(*it > hasta)
            break;
        if((val % (*it)) == 0) {
            res *= (1.0 - 1.0/(*it));
        }
    }
    return (int)res;
}

int main() {
    enteros primos;
    for(int i = 3; i < 1000000; i += 2)
        isPrime(primos, i);
    primos.insert(2);
    unsigned long long suma = 0;
    for(int i = 2; i <= 1000000; i++) {
        if(primos.find(i) != primos.end()) {
            suma += i - 1;
            cout << "Hasta " << i << ": " << suma << endl;
            continue;
        }
        suma += distintos(primos, i);
        cout << "Hasta " << i << ": " << suma << endl;
    }
	return 0;
}
