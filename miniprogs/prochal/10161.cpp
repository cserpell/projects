#include <iostream>
#include <cmath>

using namespace std;

int main() {
    while(1) {
        unsigned long long a;
        cin >> a;
        if(a == 0)
            break;
        a = a - 1;
        unsigned long long piso = (unsigned long long)sqrt(a);
        unsigned long long cpiso = piso*piso;
        unsigned long long dpiso = a - cpiso;
        if(piso % 2 == 0) {
            if(dpiso <= piso)
                cout << (piso + 1) << " " << (a - cpiso + 1) << endl;
            else
                cout << (2*piso + 1 - dpiso) << " " << (piso + 1) << endl;
        } else {
            if(dpiso <= piso)
                cout << (a - cpiso + 1) << " " << (piso + 1) << endl;
            else
                cout << (piso + 1) << " " << (2*piso + 1 - dpiso) << endl;
        }
    }
    return 0;
}
