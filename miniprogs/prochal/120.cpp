#include <iostream>
#include <string>

using namespace std;

int diam[30];
int canttotal;

void sort(int hasta) {
  /*  cout << " a ordenar ";
    for(int i = 0; i <= hasta; i++)
        cout << " " << diam[i];
    cout << endl;
   */ if(hasta == 0) {
        cout << "0" << endl;
        return;
    }
    int maximo = diam[0];
    int cualmax = 0;
    for(int i = 1; i <= hasta; i++) {
        if(diam[i] > maximo) {
            maximo = diam[i];
            cualmax = i;
        }
    }
    if(cualmax == hasta) { // ordenado hasta ese punto
        sort(hasta - 1);
        return;
    }
    int darvuelta;
    if(cualmax != 0) { // llevarlo para arriba
        darvuelta = canttotal - cualmax;
        cout << darvuelta << " ";
        for(int i = 0; i < (cualmax + 1)/2; i++) {
            int aux = diam[i];
            diam[i] = diam[cualmax - i];
            diam[cualmax - i] = aux;
        }
    }
    darvuelta = canttotal - hasta;
    cout << darvuelta << " ";
    for(int i = 0; i < (hasta + 1)/2; i++) {
        int aux = diam[i];
        diam[i] = diam[hasta - i];
        diam[hasta - i] = aux;
    }
    sort(hasta - 1);
}

int main() {
    while(1) {
        canttotal = 0;
        while(cin.peek() != '\n' && !cin.eof()) {
            cin >> diam[canttotal];
            if(canttotal != 0)
                cout << " ";
            cout << diam[canttotal];
            canttotal++;
        }
        cin.get();
        cout << endl;
        if(canttotal == 0)
            break;
        sort(canttotal - 1);
    }
    return 0;
}
