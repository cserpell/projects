#include <iostream>
#include <string>
#include <cmath>
#include <list>

using namespace std;

int mem[1000];
int reg[10];

int readAction() {
    int instrucciones = 0;
    int pointerpos = 0;
    int action;
    int a, b;
    int aux;
    while(1) {
    aux = mem[pointerpos];
    pointerpos = (pointerpos + 1) % 1000;
    action = aux / 100;
    a = (aux / 10) % 10;
    b = aux % 10;
    instrucciones++;
  //  cout << "ejecutando " << pointerpos << " -> " << aux << endl;
    switch(action) {
      case 1:
        return instrucciones;
      case 2:
        reg[a] = b;
        break;
      case 3:
        reg[a] = (reg[a] + b) % 1000;
        break;
      case 4:
        reg[a] = (reg[a] * b) % 1000;
        break;
      case 5:
        reg[a] = reg[b];
        break;
      case 6:
        reg[a] = (reg[a] + reg[b]) % 1000;
        break;
      case 7:
        reg[a] = (reg[a] * reg[b]) % 1000;
        break;
      case 8:
        reg[a] = mem[reg[b]];
        break;
      case 9:
        mem[reg[b]] = reg[a];
        break;
      case 0:
        if(reg[b] != 0)
            pointerpos = reg[a];
        break;
    }
    }
}

int main() {
    int n = 0;
    cin >> n;
    while(n > 0) {
        for(int i = 0; i < 1000; i++)
            mem[i] = 0;
        for(int i = 0; i < 10; i++)
            reg[i] = 0;
        int aux;
        int pos = 0;
        while(1) {
            cin >> aux;
            mem[pos] = aux;
            pos++;
            cin.get();
            if(cin.peek() == '\n' || cin.eof())
                break;
        }
        cout << readAction() << endl;
        n--;
        if(n != 0)
            cout << endl;
    }
    return 0;
}
