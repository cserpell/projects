#include <iostream>
#include <string>
#include <cmath>
#include <list>

using namespace std;

char ** picture;
int msize;
int nsize;

class par {
  public:
    int x;
    int y;
    par(int xx, int yy) {
        x = xx;
        y = yy;
    }
};

void readAction() {
    char action;
    int a, b, a1, a2, b1, b2;
    int aux;
    char c, orig;
    string str;
    list<par *> q;
    while(1) {
    cin >> action;
    switch(action) {
      case 'I':
        cin >> nsize;
        cin >> msize;
        picture = new char *[msize];
        for(int j = 0; j < msize; j++) {
            picture[j] = new char[nsize];
            for(int i = 0; i < nsize; i++)
                picture[j][i] = 'O';
        }
        break;
      case 'C':
        for(int j = 0; j < msize; j++)
            for(int i = 0; i < nsize; i++)
                picture[j][i] = 'O';
        break;
      case 'L':
        cin >> a;
        cin >> b;
        cin >> c;
        picture[b - 1][a - 1] = c;
        break;
      case 'V':
        cin >> a;
        cin >> b1;
        cin >> b2;
        cin >> c;
        if(b2 < b1) {
            aux = b2;
            b2 = b1;
            b1 = aux;
        }
        for(int i = b1; i <= b2; i++)
            picture[i - 1][a - 1] = c;
        break;
      case 'H':
        cin >> a1;
        cin >> a2;
        cin >> b;
        cin >> c;
        if(a2 < a1) {
            aux = a2;
            a2 = a1;
            a1 = aux;
        }
        for(int i = a1; i <= a2; i++)
            picture[b - 1][i - 1] = c;
        break;
      case 'K':
        cin >> a1;
        cin >> b1;
        cin >> a2;
        cin >> b2;
        cin >> c;
        for(int i = a1; i <= a2; i++)
            for(int j = b1; j <= b2; j++)
                picture[j - 1][i - 1] = c;
        break;
      case 'F':
        cin >> a;
        cin >> b;
        cin >> c;
        orig = picture[b - 1][a - 1];
        if(orig == c)
            break;
        q.clear();
        q.push_back(new par(a, b));
        while(!q.empty()) {
            par * k = q.front();
            q.pop_front();
           // cout << "llenando " << k->x << " " << k->y << endl;
            if(picture[k->y - 1][k->x - 1] == orig) {
                picture[k->y - 1][k->x - 1] = c;
                if(k->x < nsize)
                    q.push_back(new par(k->x + 1, k->y));
                if(k->y < msize)
                    q.push_back(new par(k->x, k->y + 1));
                if(k->x > 1)
                    q.push_back(new par(k->x - 1, k->y));
                if(k->y > 1)
                    q.push_back(new par(k->x, k->y - 1));
            }
        }
        break;
      case 'S':
        cin >> str;
        cout << str << endl;
        for(int j = 1; j <= msize; j++) {
            for(int i = 1; i <= nsize; i++)
                cout << picture[j - 1][i - 1];
            cout << endl;
        }
        break;
      case 'X':
        return;
      default:
        while(cin.peek() != '\n')
            cin >> str;
        break;
    }
    }
}

int main() {
    readAction();
    return 0;
}
