#include <iostream>
#include <string>
#include <cmath>
#include <list>

using namespace std;

class contestant {
  public:
    int time;
    int solvedproblems;
    int number;
    int tries[9];
    contestant(int n) {
        number = n;
        solvedproblems = 0;
        time = 0;
        for(int i = 0; i < 9; i++)
            tries[i] = 0;
    }
    bool operator<(contestant &otro) {
        if(solvedproblems > otro.solvedproblems)
            return true;
        if(solvedproblems < otro.solvedproblems)
            return false;
        if(time < otro.time)
            return true;
        if(time > otro.time)
            return false;
        if(number < otro.number)
            return true;
        return false;
    }
};

list<contestant> conjunto;
contestant * todos[100];
int ocupado[100];

int main() {
    int n;
    int ncontestant, nproblem, ntime;
    char res;
    cin >> n;
    cin.get();
    while(n > 0) {
        for(int i = 0; i < 100; i++)
            ocupado[i] = 0;
        while(1) {
            cin.get();
            if(cin.peek() == '\n' || cin.eof())
                break;
            cin >> ncontestant;
            ncontestant--;
            cin >> nproblem;
            nproblem--;
            cin >> ntime;
            cin >> res;
            if(!ocupado[ncontestant]) {
                todos[ncontestant] = new contestant(ncontestant);
                ocupado[ncontestant] = 1;
            }
            if(res == 'I') {
                if(todos[ncontestant]->tries[nproblem] != -1)
                    todos[ncontestant]->tries[nproblem]++;
            } else if(res == 'C') {
                if(todos[ncontestant]->tries[nproblem] != -1) {
                    todos[ncontestant]->time += 20*todos[ncontestant]->tries[nproblem] + ntime;
//                  cout << " added " << (20*todos[ncontestant]->tries[nproblem] + ntime) << endl;
                    todos[ncontestant]->tries[nproblem] = -1;
                    todos[ncontestant]->solvedproblems++;
                }
            }
        }
        conjunto.clear();
        for(int i = 0; i < 100; i++)
            if(ocupado[i])
                conjunto.push_front(*(todos[i]));
        conjunto.sort();
        list<contestant>::iterator it = conjunto.begin();
        while(it != conjunto.end()) {
            cout << ((*it).number + 1);
            cout << " " << (*it).solvedproblems;
            cout << " " << (*it).time << endl;
            it++;
        }
        if(n > 1)
            cout << endl;
        n--;
    }
    return 0;
}
