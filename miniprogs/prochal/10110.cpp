#include <iostream>
#include <cmath>

using namespace std;

int main() {
    while(1) {
        unsigned long long a;
        cin >> a;
        if(a == 0)
            break;
        if(a == 1) {
            cout << "yes" << endl;
            continue;
        }
        unsigned long long b = sqrt(a);
        if(b*b == a) {
            cout << "yes" << endl;
            continue;
        }
        cout << "no" << endl;
    }
    return 0;
}
