#include <iostream>
#include <string>

using namespace std;

int pos[500];

int cantidad;

int main() {
    int n;
    cin >> n;
    while(n > 0) {
        cin >> cantidad;
      //  suma = 0;
        int mmin = 30000;
        int mmax = 0;
        for(int i = 0; i < cantidad; i++) {
            cin >> pos[i];
           // suma += pos[i];
            if(pos[i] < mmin)
                mmin = pos[i];
            if(pos[i] > mmax)
                mmax = pos[i];
        }
        int ssuma = 500*30000;
        for(int prom = mmin; prom <= mmax; prom++) {
            int suma1 = 0;
            for(int i = 0; i < cantidad; i++) {
                if(prom > pos[i])
                    suma1 += prom - pos[i];
                else
                    suma1 += pos[i] - prom;
            }
            if(suma1 < ssuma)
                ssuma = suma1;
        }
        cout << ssuma << endl;
        n--;
    }
    return 0;
}
