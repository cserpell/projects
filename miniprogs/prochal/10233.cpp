#include <iostream>
#include <cmath>

#define SQ3 1.732050808

using namespace std;

int main() {
    while(!cin.eof()) {
        unsigned long long a, b;
        cin >> a;
        if(cin.eof())
            break;
        cin >> b;
        unsigned long long pisoa, pisob, dbasea, dbaseb, basea, baseb;
        pisoa = (unsigned long long)sqrt(a);
        basea = pisoa*pisoa;
        dbasea = a - basea;
        pisob = (unsigned long long)sqrt(b);
        baseb = pisob*pisob;
        dbaseb = b - baseb;
        long long pxa, pxb, pya, pyb;
        pxa = dbasea - pisoa; // todo multiplicado por 1/2
        pxb = dbaseb - pisob;
        if(dbasea % 2 == 0)
            pya = 3*pisoa + 2; // todo multiplicado por SQ3/6
        else
            pya = 3*pisoa + 1;
        if(dbaseb % 2 == 0)
            pyb = 3*pisob + 2; // todo multiplicado por SQ3/6
        else
            pyb = 3*pisob + 1;
      //  cout << "(" << pxa << "," << pya << ") , (" << pxb << "," << pyb << ")" << endl;
        double dx = (pxa - pxb)/2.;
        double dy = (pya - pyb)*SQ3/6.;
        double d = sqrt(dx*dx + dy*dy);
        unsigned long long q = (unsigned long long)d;
        unsigned long long r = (unsigned long long)((d - q)*10000);
        if(r % 10 >= 5)
            r += 10;
        r /= 10;
  //      double ud = q + r/1000.;
        cout << q << ".";
        if(r < 100)
            cout << "0";
        if(r < 10)
            cout << "0";
        cout << r << endl;
//        cout << ud << endl;
    }
    return 0;
}
