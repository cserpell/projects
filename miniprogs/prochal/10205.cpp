#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int cad[100][53];
int cartas[53];
int ncartas[53];

int main() {
    int n;
    int diasremov;
    int ncad;
    cin >> n;
    while(n > 0) {
        cin >> ncad;
        for(int i = 0; i < ncad; i++) {
            for(int j = 1; j <= 52; j++) {
                cin >> cad[i][j];
            }
        }
        for(int i = 1; i <= 52; i++)
            cartas[i] = i;
        cin.get();
        while(cin.peek() != '\n' && !cin.eof()) {
            int b;
            cin >> b;
            b = b - 1;
            for(int i = 1; i <= 52; i++) {
                //cout << cartas[i];
                ncartas[cad[b][i]] = cartas[i];
               // cout << " pasa a " << cartas[i] << endl;
            }
            for(int i = 1; i <= 52; i++) {
                cartas[i] = ncartas[i];
            }
            cin.get();
        }
        for(int i = 1; i <= 52; i++) {
            int col = (cartas[i] - 1)/13;
            int num = (cartas[i] - 1)%13;
//            cout << cartas[i] << endl;
  //          continue;
            if(num < 9)
                cout << (num + 2);
            else {
                if(num == 9)
                    cout << "Jack";
                else if(num == 10)
                    cout << "Queen";
                else if(num == 11)
                    cout << "King";
                else if(num == 12)
                    cout << "Ace";
            }
            cout << " of ";
            if(col == 0)
                cout << "Clubs";
            else if(col == 1)
                cout << "Diamonds";
            else if(col == 2)
                cout << "Hearts";
            else if(col == 3)
                cout << "Spades";
            cout << endl;
        }
        if(n > 1)
            cout << endl;
        n--;
    }
    return 0;
}
