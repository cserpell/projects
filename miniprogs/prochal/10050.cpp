#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int cad[100];

int main() {
    int n;
    int N;
    int diasremov;
    int ncad;
    cin >> n;
    while(n > 0) {
        diasremov = 0;
        cin >> N;
        cin >> ncad;
        for(int i = 0; i < ncad; i++)
            cin >> cad[i];
        for(int i = 1; i <= N; i++) {
            int r = i % 7;
            if(r == 6 || r == 0)
                continue;
            for(int p = 0; p < ncad; p++)
                if((i % cad[p]) == 0) {
                    diasremov++;
                    break;
                }
        }
        cout << diasremov << endl;
        n--;
    }
    return 0;
}
