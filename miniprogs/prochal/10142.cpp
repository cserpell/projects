#include <iostream>
#include <string>
#include <cmath>
#include <list>

using namespace std;

int votocand[20];
int voto[1000][20];

char cand[20][85];

int nvotos;
int ncands;

void eliminarCandidato(int cual) {
    for(int i = 0; i < nvotos; i++) {
        for(int j = 0; j < ncands; j++) {
            if(voto[i][j] == cual) {
                voto[i][j] = -1;
                break;
            }
        }
    }
}

void todors() {
    int quedan = ncands;
    for(int i = 0; i < ncands; i++) {
        votocand[i] = 0;
    }
    while(1) {
 //   cout << "pasada ..." << endl;
    for(int i = 0; i < ncands; i++) {
        if(votocand[i] != -1)
            votocand[i] = 0;
    }
    for(int i = 0; i < nvotos; i++) {
        for(int j = 0; j < ncands; j++) {
            if(voto[i][j] != -1) {
                votocand[voto[i][j] - 1]++;
                break;
            }
        }
    }
    int minimo = 1001;
    int maximo = 0;
    int cualmax;
    for(int i = 0; i < ncands; i++) {
        if(votocand[i] != -1 && votocand[i] < minimo)
            minimo = votocand[i];
        if(votocand[i] > maximo) {
            maximo = votocand[i];
            cualmax = i;
        }
    }
    if(maximo == minimo || quedan == 1) {
        for(int i = 0; i < ncands; i++) {
            if(votocand[i] != -1)
                cout << cand[i] << endl;
        }
        return;
    }
    if((nvotos % 2) == 0) {
        if(maximo > nvotos/2) {
            cout << cand[cualmax] << endl;
            return;
        }
    } else  {
        if(maximo > nvotos/2) {
            cout << cand[cualmax] << endl;
            return;
        }
    }
 //   cout << minimo << endl;
    for(int i = 0; i < ncands; i++) {
        if(votocand[i] == minimo) {
            eliminarCandidato(i);
            votocand[i] = -1;
            quedan--;
        }
    }
    }
}

int main() {
    int n = 0;
    int aux;
    cin >> n;
    while(n > 0) {
        cin >> ncands;
        cin.get();
        for(int i = 0; i < ncands; i++) {
            cin.getline(cand[i], 82);
        }
        nvotos = 0;
        while(1) {
            for(int i = 0; i < ncands; i++) {
                cin >> aux;
                voto[nvotos][i] = aux;
            }
            nvotos++;
            cin.get();
            if(cin.peek() == '\n' || cin.eof())
                break;
        }
        todors();
        n--;
        if(n != 0)
            cout << endl;
    }
    return 0;
}
