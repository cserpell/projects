#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int mardif[3001];

int main() {
    int n = 0;
    int aux;
    cin >> n;
    while(!cin.eof() && n > 0) {
        long long last;
        cin >> last;
        if(n == 1) {
            cout << "Jolly" << endl;
            cin >> n;
            continue;
        }
        for(int i = 1; i < n; i++)
            mardif[i] = 0;
        int maa = 0;
        for(int i = 0; i < n - 1; i++) {
            long long b;
            cin >> b;
            long long dif = b - last;
            if(dif < 0)
                dif = -dif;
            last = b;
            if(dif >= n || dif == 0 || mardif[dif])
                maa = 1;
            else
                mardif[dif] = 1;
        }
        if(maa)
            cout << "Not jolly" << endl;
        else 
            cout << "Jolly" << endl;
        cin >> n;
    }
    return 0;
}
