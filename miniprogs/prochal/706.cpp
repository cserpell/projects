#include <iostream>
#include <string>
#include <cmath>

using namespace std;

void printEspacio() {
    cout << " ";
}

void printUp(int num, int lar) {
    cout << " ";
    char centro = ' ';
    if(num == 0 || num == 2 || num == 3 || num == 5 || num == 6 || num == 7 || num == 8 || num == 9)
        centro = '-';
    for(int i = 0; i < lar; i++)
        cout << centro;
    cout << " ";
}

void printCenter(int num, int lar) {
    cout << " ";
    char centro = ' ';
    if(num == 2 || num == 3 || num == 4 || num == 5 || num == 6 || num == 8|| num == 9)
        centro = '-';
    for(int i = 0; i < lar; i++)
        cout << centro;
    cout << " ";
}

void printDown(int num, int lar) {
    cout << " ";
    char centro = ' ';
    if(num == 0 || num == 2 || num == 3 || num == 5 || num == 6 || num == 8|| num == 9)
        centro = '-';
    for(int i = 0; i < lar; i++)
        cout << centro;
    cout << " ";
}

void printMUp(int num, int lar) {
    char l1 = ' ';
    char l2 = ' ';
    if(num == 0 || num == 1 || num == 2 || num == 3 || num == 4 || num == 7 || num == 8 || num == 9)
        l2 = '|';
    if(num == 0 || num == 4 || num == 5 || num == 6 || num == 8 || num == 9)
        l1 = '|';
    cout << l1;
    for(int i = 0; i < lar; i++)
        cout << " ";
    cout << l2;
}

void printMDown(int num, int lar) {
    char l1 = ' ';
    char l2 = ' ';
    if(num == 0 || num == 1 || num == 3 || num == 4 || num == 5 || num == 6 || num == 7 || num == 8 || num == 9)
        l2 = '|';
    if(num == 0 || num == 2 || num == 6 || num == 8)
        l1 = '|';
    cout << l1;
    for(int i = 0; i < lar; i++)
        cout << " ";
    cout << l2;
}

int main() {
    unsigned long long num;
    unsigned long long p[10] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000LL, 1000000000LL};
    int stri[10];
    int lar;
    int r = 0;
    while(1) {
    cin >> lar;
    cin >> num;
    if(lar == 0 && num == 0)
        break;
    int rl = 1;
    if(num != 0)
        rl = ((int)(log((double)num)/log(10.0))) + 1;
    for(int i = rl - 1; i >= 0; i--) {
        stri[rl - i - 1] = (num % p[i + 1])/p[i];
    }
    for(int i = 0; i < rl; i++) {
        if(i != 0)
            printEspacio();
        printUp(stri[i], lar);
    }
    cout << endl;
    for(int j = 0; j < lar; j++) {
    for(int i = 0; i < rl; i++) {
        if(i != 0)
            printEspacio();
        printMUp(stri[i], lar);
    }
    cout << endl;
    }
    for(int i = 0; i < rl; i++) {
        if(i != 0)
            printEspacio();
        printCenter(stri[i], lar);
    }
    cout << endl;
    for(int j = 0; j < lar; j++) {
    for(int i = 0; i < rl; i++) {
        if(i != 0)
            printEspacio();
        printMDown(stri[i], lar);
    }
    cout << endl;
    }
    for(int i = 0; i < rl; i++) {
        if(i != 0)
            printEspacio();
        printDown(stri[i], lar);
    }
    cout << endl;
    cout << endl;
    }
    return 0;
}
