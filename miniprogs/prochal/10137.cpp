#include <iostream>
#include <string>

using namespace std;

int main() {
    unsigned long long *tab;
    unsigned long long val;
    int a;
    while(1) {
    cin >> a;
    if(cin.eof())
        break;
    if(a == 0)
        break;
    tab = new unsigned long long[a];
    double d;
    val = 0;
    for(int i = 0; i < a; i++) {
        cin >> d;
        tab[i] = (unsigned long long)(d*100);
        val += tab[i];
    }
    double prom = ((double)val)/a;
    unsigned long long promsup = (prom + 1.);
    unsigned long long prominf = prom;
    int extrapenys = val % a;
    int countextrapenys = 0;
  //  cout << promsup << " .. ";
    //cout << prominf << endl;
    unsigned long long given = 0;
    unsigned long long received = 0;
    unsigned long long difs = 0;
    if(((double)prominf) == prom) {
        for(int i = 0; i < a; i++) {
            if(tab[i] > prom) {
                unsigned long long pdif = tab[i] - prominf;
                difs += pdif;
                tab[i] -= pdif;
            }/* else {
                unsigned long long pdif = prominf - tab[i];
                difs += pdif;
                tab[i] += pdif;
            }*/
        }
    } else {
        for(int i = 0; i < a; i++) {
            if(countextrapenys < extrapenys && tab[i] >= promsup) {
                unsigned long long pdif = tab[i] - promsup;
                difs += pdif;
                countextrapenys++;
                continue;
            }
            if(countextrapenys >= extrapenys && tab[i] > prominf) {
                unsigned long long pdif = tab[i] - prominf;
                difs += pdif;
                continue;
            }
        }
    }
    /*if(given > received)
        difs = given;
    else
        difs = received;*/
    // difs = (given + received)/2;
    int md = ((unsigned long long)difs) % 100;
    cout << "$" << (((unsigned long long)difs)/100) << ".";
    if(md < 10)
        cout << "0";
    cout << md << endl;
    }
    return 0;
}
