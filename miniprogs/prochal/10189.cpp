#include <iostream>
#include <string>

using namespace std;

int main() {
    int **tab;
    string str;
    int a, b;
    int r = 0;
    while(1) {
    r++;
    cin >> a;
    if(cin.eof())
        break;
    cin >> b;
    if(a == 0)
        break;
    if(r != 1)
        cout << endl;
    tab = new int *[b];
    for(int i = 0; i < b; i++) {
        tab[i] = new int[a];
        for(int j = 0; j < a; j++) {
            tab[i][j] = 0;
        }
    }
    for(int i = 0; i < a; i++) {
        cin >> str;
        for(int j = 0; j < b; j++) {
            if(str.at(j) == '*') {
                tab[j][i] = -1;
                if(j > 0 && tab[j - 1][i] != -1)
                    tab[j - 1][i]++;
                if(j > 0 && i > 0 && tab[j - 1][i - 1] != -1)
                    tab[j - 1][i - 1]++;
                if(i > 0 && tab[j][i - 1] != -1)
                    tab[j][i - 1]++;
                if(i < a - 1)
                    tab[j][i + 1]++;
                if(j < b - 1 && i < a - 1)
                    tab[j + 1][i + 1]++;
                if(j < b - 1)
                    tab[j + 1][i]++;
                if(j < b - 1 && i > 0 && tab[j + 1][i - 1] != -1)
                    tab[j + 1][i - 1]++;
                if(i < a - 1 && j > 0)
                    tab[j - 1][i + 1]++;
            }
        }
    }
    cout << "Field #" << r << ":" << endl;
    for(int i = 0; i < a; i++) {
        for(int j = 0; j < b; j++) {
            if(tab[j][i] == -1)
                cout << "*";
            else
                cout << tab[j][i];
        }
        cout << endl;
    }
    }
    return 0;
}
