#include <iostream>
#include <cmath>

using namespace std;

int main() {
//cout << M_PI << endl;
    double raiz3 = sqrt(3.);
    double pi = 3.1415926535897932384626433832795;//3.141592653;//M_PI;
    double a1 = (pi + 3. - 3.*raiz3)/3.;
    double a2 = (pi - 12. + 6.*raiz3)/3.;
    double a3 = (-2.*pi + 12. - 3.*raiz3)/3.;
    while(!cin.eof()) {
        double ent;
        cin >> ent;
        if(cin.eof())
            break;
        double q1 = (ent*ent*a1);
        double q2 = (ent*ent*a2);
        double q3 = (ent*ent*a3);
        unsigned long long r1 = (unsigned long long)(q1*10000.);
        unsigned long long r2 = (unsigned long long)(q2*10000.);
        unsigned long long r3 = (unsigned long long)(q3*10000.);
        if(r1 % 10 >= 5)
            r1 += 10;
        r1 /= 10;
        if(r2 % 10 >= 5)
            r2 += 10;
        r2 /= 10;
        if(r3 % 10 >= 5)
            r3 += 10;
        r3 /= 10;
        unsigned long long p1 = r1/1000;
        unsigned long long p2 = r2/1000;
        unsigned long long p3 = r3/1000;
        r1 -= p1*1000;
        r2 -= p2*1000;
        r3 -= p3*1000;
        cout << p1;
        cout << ".";
        if(r1 < 100)
            cout << "0";
        if(r1 < 10)
            cout << "0";
        cout << r1;
        cout << " ";
        cout << p2;
        cout << ".";
        if(r2 < 100)
            cout << "0";
        if(r2 < 10)
            cout << "0";
        cout << r2;
        cout << " ";
        cout << p3;
        cout << ".";
        if(r3 < 100)
            cout << "0";
        if(r3 < 10)
            cout << "0";
        cout << r3;
        cout << endl;
    }
    return 0;
}
