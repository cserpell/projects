#include <iostream>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

void mostrarnum(double n) {
    printf("%.3f", n);
    return;
    unsigned long long r1, p1;
    r1 = (unsigned long long)(n*10000.);
    if(r1 % 10 >= 5)
        r1 += 10;
    r1 /= 10;
    p1 = r1/1000;
    r1 -= p1*1000;
    cout << p1;
    cout << ".";
    if(r1 < 100)
        cout << "0";
    if(r1 < 10)
        cout << "0";
    cout << r1;
}

int main() {
//cout << M_PI << endl;
    char str1[100];
    char str2[100];
    while(!cin.eof()) {
        int holes;
        cin >> holes;
        if(cin.eof())
            break;
        double g1, g2, d1, d2, h1, h2;
        cin >> g1;
        cin >> g2;
        cin >> d1;
        cin >> d2;
        int encontrada = 0;
        for(int i = 0; i < holes; i++) {
            cin >> str1;
            cin >> str2;
            h1 = atof(str1);
            h2 = atof(str2);
            if(!encontrada) {
                double t1 = 4.*((g1 - h1)*(g1 - h1) + (g2 - h2)*(g2 - h2));
                double t2 = (d1 - h1)*(d1 - h1) + (d2 - h2)*(d2 - h2);
                if(t1 <= t2) {
                    encontrada = 1;
                    cout << "The gopher can escape through the hole at (";
                    cout << str1;
//                    mostrarnum(h1);
                    cout << ",";
                    cout << str2;
//                    mostrarnum(h2);
                    cout << ")." << endl;
                }
            }
        }
        if(!encontrada)
            cout << "The gopher cannot escape." << endl;
    }
    return 0;
}
