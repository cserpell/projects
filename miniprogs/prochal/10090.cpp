#include <iostream>
#include <string>

using namespace std;

unsigned long long cantidadtotal;
unsigned long long n1, n2;
unsigned long long c1, c2;
unsigned long long m1, m2;
double cp1, cp2;

unsigned long long mcd(unsigned long long a, unsigned long long b) {
    unsigned long long a1, a2, aux;
    a1 = a;
    a2 = b;
    while(a2 != 0) {
        aux = a1 % a2;
        a1 = a2;
        a2 = aux;
    }
    return a1;
}

int main() {
    while(1) {
        cin >> cantidadtotal;
        if(cantidadtotal == 0)
            break;
        cin >> c1;
        cin >> n1;
        cin >> c2;
        cin >> n2;
        unsigned long long mm = mcd(n1, n2);
        if(cantidadtotal % mm != 0) {
            cout << "failed" << endl;
            continue;
        }
        cp1 = ((double)n1)/c1;
        cp2 = ((double)n2)/c2;
        unsigned long long calculo;
        if(cp1 > cp2) {
            m1 = cantidadtotal/n1;
            m2 = 0;
            calculo = m1*n1;
            while(calculo != cantidadtotal) {
                while(calculo < cantidadtotal) {
                    m2++;
                    calculo += n2;
                }
                m1--;
                calculo -= n1;
                if(m1 < 0) break;
            }
            if(m1 >= 0) {
                cout << m1 << " " << m2 << endl;
                continue;
            }
        }
        m2 = cantidadtotal/n2;
        m1 = 0;
        calculo = m2*n2;
        while(calculo != cantidadtotal) {
            while(calculo < cantidadtotal) {
                m1++;
                calculo += n1;
            }
            m2--;
            calculo -= n2;
            if(m2 < 0)
                break;
        }
        if(m2 >= 0) {
            cout << m1 << " " << m2 << endl;
            continue;
        }
        cout << "failed" << endl;
    }
    return 0;
}
