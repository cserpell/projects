#include <iostream>
#include <cmath>
#include <queue>

using namespace std;

class par {
  public:
    char pw;
    char ph;
    char color; // 0 a 4
    char dir;   // 0 a 3
    par(char p1, char p2, char t1, char t2, int n) {
        pw = p1;
        ph = p2;
        color = t1;
        dir = t2;
        numero = n;
    }
    int numero;
};

char tab[25][25][5][4];
int W;
int H;

void llenar(char pxini, char pyini, char pxfin, char pyfin) {
    queue<par *> cola;
    cola.push(new par(pxini, pyini, 0, 0, 1));
    while(!cola.empty()) {
        par *pp = cola.front();
        cola.pop();
        if(tab[pp->pw][pp->ph][pp->color][pp->dir] != 0) { // ya se llego ahi o bloque
            delete pp;
            continue;
        }
        if(pp->pw == pxfin && pp->ph == pyfin && pp->color == 0) {
            cout << "minimum time = " << (pp->numero - 1) << " sec" << endl;
            delete pp;
            while(!cola.empty()) {
                pp = cola.front();
                cola.pop();
                delete pp;
            }
            return;
        }
        tab[pp->pw][pp->ph][pp->color][pp->dir] = pp->numero;
        if(pp->dir == 0 && pp->ph - 1 >= 0 && tab[pp->pw][pp->ph - 1][(pp->color + 1)%5][pp->dir] == 0)
            cola.push(new par(pp->pw, pp->ph - 1, (pp->color + 1)%5, pp->dir, pp->numero + 1));
        if(pp->dir == 1 && pp->pw + 1 < W && tab[pp->pw + 1][pp->ph][(pp->color + 1)%5][pp->dir] == 0)
            cola.push(new par(pp->pw + 1, pp->ph, (pp->color + 1)%5, pp->dir, pp->numero + 1));
        if(pp->dir == 2 && pp->ph + 1 < H && tab[pp->pw][pp->ph + 1][(pp->color + 1)%5][pp->dir] == 0)
            cola.push(new par(pp->pw, pp->ph + 1, (pp->color + 1)%5, pp->dir, pp->numero + 1));
        if(pp->dir == 3 && pp->pw - 1 >= 0 && tab[pp->pw - 1][pp->ph][(pp->color + 1)%5][pp->dir] == 0)
            cola.push(new par(pp->pw - 1, pp->ph, (pp->color + 1)%5, pp->dir, pp->numero + 1));
        if(tab[pp->pw][pp->ph][pp->color][(pp->dir + 1)%4] == 0)
            cola.push(new par(pp->pw, pp->ph, pp->color, (pp->dir + 1)%4, pp->numero + 1));
        if(tab[pp->pw][pp->ph][pp->color][(pp->dir + 3)%4] == 0)
            cola.push(new par(pp->pw, pp->ph, pp->color, (pp->dir + 3)%4, pp->numero + 1));
        delete pp;
    }
    cout << "destination not reachable" << endl;
}

int main() {
    int n = 1;
    char linea[27];
    char pxini, pyini, pxfin, pyfin;
    while(1) {
        cin >> H;
        cin >> W;
        if(W == 0 && H == 0)
            break;
        for(char i = 0; i < H; i++) {
            cin >> linea;
            for(char j = 0; j < W; j++) {
                for(char k = 0; k < 5; k++)
                    for(char l = 0; l < 4; l++) {
                        if(linea[j] == '#')
                            tab[j][i][k][l] = -1;
                        else
                            tab[j][i][k][l] = 0;
                    }
                if(linea[j] == 'S') {
                    pxini = j;
                    pyini = i;
                }
                if(linea[j] == 'T') {
                    pxfin = j;
                    pyfin = i;
                }
            }
        }
        if(n != 1)
            cout << endl;
        cout << "Case #" << n << endl;
        llenar(pxini, pyini, pxfin, pyfin);
        n++;
    }
    return 0;
}
