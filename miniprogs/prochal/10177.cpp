#include <iostream>
#include <cmath>

#define SQ3 1.732050808

using namespace std;

unsigned long long base[101];
unsigned long long s2[101];
unsigned long long r2[101];
unsigned long long s3[101];
unsigned long long r3[101];
unsigned long long s4[101];
unsigned long long r4[101];

int main() {
    base[0] = 0;
    s2[0] = 0;
    r2[0] = 0;
    s3[0] = 0;
    r3[0] = 0;
    s4[0] = 0;
    r4[0] = 0;
    base[1] = 1;
    s2[1] = 1;
    r2[1] = 0;
    s3[1] = 1;
    r3[1] = 0;
    s4[1] = 1;
    r4[1] = 0;
    for(unsigned long long i = 2; i<= 100; i++) {
        base[i] = base[i - 1] + i;
        s2[i] = s2[i - 1] + i*i;
        s3[i] = s3[i - 1] + i*i*i;
        s4[i] = s4[i - 1] + i*i*i*i;
        r2[i] = base[i]*base[i] - s2[i];
        r3[i] = base[i]*base[i]*base[i] - s3[i];
        r4[i] = base[i]*base[i]*base[i]*base[i] - s4[i];
    }
    while(!cin.eof()) {
        unsigned long long a;
        cin >> a;
        if(cin.eof())
            break;
        cout << s2[a] << " ";
        cout << r2[a] << " ";
        cout << s3[a] << " ";
        cout << r3[a] << " ";
        cout << s4[a] << " ";
        cout << r4[a] << endl;
    }
    return 0;
}
