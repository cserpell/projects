#include <iostream>
#include <string>
#include <list>

using namespace std;

list<int> pos;

int cantidad;

int main() {
    int n;
    cin >> n;
    while(n > 0) {
        pos.clear();
        cin >> cantidad;
        for(int i = 0; i < cantidad; i++) {
            int a;
            cin >> a;
            pos.push_front(a);
        }
        pos.sort();
        list<int>::iterator it2 = pos.begin();
        int pp = 0;
        while(pp < cantidad/2) {
            pp++;
            it2++;
        }
        int prom = *it2;
        int suma1 = 0;
        for(list<int>::iterator it = pos.begin(); it != pos.end(); it++) {
            if(prom > (*it))
                suma1 += prom - (*it);
            else
                suma1 += (*it) - prom;
        }
        cout << suma1 << endl;
        n--;
    }
    return 0;
}
