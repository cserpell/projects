#include <iostream>

using namespace std;

unsigned long long calcularlargo(int g) {
    unsigned long long ret = 0;
    unsigned long long val = g;
    while(val != 1) {
        if(val % 2) {
            val *= 3;
            val++;
        } else
            val /= 2;
        ret++;
    }
    return ret + 1;
}

int main() {
    while(1) {
    unsigned long long a, b;
    cin >> a;
    if(cin.eof())
        break;
    cin >> b;
    unsigned long long ax = a;
    unsigned long long bx = b;
    unsigned long long aux = a;
    if(b < a) {
        ax = b;
        bx = aux;
    }
    unsigned long long cmax = 0;
    unsigned long long nmax = 0;
    for(int i = ax; i <= bx; i++) {
        unsigned long long lenn = calcularlargo(i);
        if(lenn > nmax) {
            nmax = lenn;
            cmax = i;
        }
    }
    cout << a << " " << b << " " << nmax << endl;
    }
    return 0;
}
