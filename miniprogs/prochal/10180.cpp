#include <iostream>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

void mostrarnum(double n) {
    printf("%.3f", n);
    return;
    unsigned long long r1, p1;
    r1 = (unsigned long long)(n*10000.);
    if(r1 % 10 >= 5)
        r1 += 10;
    r1 /= 10;
    p1 = r1/1000;
    r1 -= p1*1000;
    cout << p1;
    cout << ".";
    if(r1 < 100)
        cout << "0";
    if(r1 < 10)
        cout << "0";
    cout << r1;
}

int main() {
//cout << M_PI << endl;
    double pi = 3.1415926535897932384626433832795;
    int n;
    cin >> n;
    while(n > 0) {
        double p1x, p1y, p2x, p2y, R;
        cin >> p1x;
        cin >> p1y;
        cin >> p2x;
        cin >> p2y;
        cin >> R;
        double d1 = p1x*p1x + p1y*p1y;
        double d2 = p2x*p2x + p2y*p2y;
        double a1 = acos(R/sqrt(d1));
        double a2 = acos(R/sqrt(d2));
        double dif = (p1x - p2x)*(p1x - p2x) + (p1y - p2y)*(p1y - p2y);
        double a3 = acos((d1 + d2 - dif)/(2.*sqrt(d1*d2)));
        if(a3 < a1 + a2) {
            mostrarnum(sqrt(dif));
        } else {
            a3 -= a1 + a2;
            mostrarnum(sqrt(d1 - R*R) + sqrt(d2 - R*R) + a3*R);
        }
        cout << endl;
        n--;
    }
    return 0;
}
