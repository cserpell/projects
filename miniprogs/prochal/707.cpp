#include <iostream>
#include <cmath>
#include <queue>

using namespace std;

class par {
  public:
    char pw;
    char ph;
    char tm;
    par(char p1, char p2, char t1) {
        pw = p1;
        ph = p2;
        tm = t1;
    }
};

char tab[100][100][100];
int total[100];
char lseen[100][3];
int W;
int H;
int T;

void vaciar() {
    for(char i = 0; i < W; i++)
        for(char j = 0; j < H; j++)
            for(char k = 0; k < T; k++)
                if(tab[i][j][k] > 0)
                    tab[i][j][k] == 0;
    for(char k = 0; k < T; k++)
        total[k] = 0;
}

void seguirlinea() {
    queue<par *> cola;
    
}

void llenar() {
    queue<par *> cola;
    for(char i = 0; i < W; i++)
        for(char j = 0; j < H; j++)
            if(tab[i][j][0] == 0)
                cola.push(new par(i, j, 0));
    while(!cola.empty()) {
        par *pp = cola.front();
        cola.pop();
        if(tab[pp->pw][pp->ph][pp->tm] != 0)
            continue;
        tab[pp->pw][pp->ph][pp->tm] = pp->tm + 1;
        total[pp->tm]++;
        lseen[pp->tm][0] = pp->pw;
        lseen[pp->tm][1] = pp->ph;
        if(pp->tm == T - 1)
            continue;
        if(tab[pp->pw][pp->ph][pp->tm + 1] == 0)
            cola.push(new par(pp->pw, pp->ph, pp->tm + 1));
        if(pp->pw + 1 < W && tab[pp->pw + 1][pp->ph][pp->tm + 1] == 0)
            cola.push(new par(pp->pw + 1, pp->ph, pp->tm + 1));
        if(pp->ph + 1 < H && tab[pp->pw][pp->ph + 1][pp->tm + 1] == 0)
            cola.push(new par(pp->pw, pp->ph + 1, pp->tm + 1));
        if(pp->pw - 1 >= 0 && tab[pp->pw - 1][pp->ph][pp->tm + 1] == 0)
            cola.push(new par(pp->pw - 1, pp->ph, pp->tm + 1));
        if(pp->ph - 1 >= 0 && tab[pp->pw][pp->ph - 1][pp->tm + 1] == 0)
            cola.push(new par(pp->pw, pp->ph - 1, pp->tm + 1));
        delete pp;
    }
    if(total[T - 1] == 0) {
        cout << "The robber has escaped." << endl;
        return;
    }
    for(char i = 0; i < T; i++)
        lseen[i][2] = 0;
    for(char i = 0; i < T; i++)
        if(total[i] == 1 && lseen[i][2] == 0) {
            lseen[i][2] = 1;
            char px = lseen[i][0];
            char py = lseen[i][1];
            char pz = i;
            while(pz > 0) {
                char npz = pz - 1;
                char npy;
                char npx;
                char cc = 0;
                if(tab[px][py][npz] == tab[px][py][pz] - 1) {
                    npx = px;
                    npy = py;
                    cc++;
                }
                if(px + 1 < W && tab[px + 1][py][npz] == tab[px][py][pz] - 1) {
                    npx = px + 1;
                    npy = py;
                    cc++;
                }
                if(py + 1 < H && tab[px][py + 1][npz] == tab[px][py][pz] - 1) {
                    npx = px;
                    npy = py + 1;
                    cc++;
                }
                if(px - 1 >= 0 && tab[px - 1][py][npz] == tab[px][py][pz] - 1) {
                    npx = px - 1;
                    npy = py;
                    cc++;
                }
                if(py - 1 >= 0 && tab[px][py - 1][npz] == tab[px][py][pz] - 1) {
                    npx = px;
                    npy = py - 1;
                    cc++;
                }
                if(cc != 1)
                    break;
                lseen[npz][0] = npx;
                lseen[npz][1] = npy;
                lseen[npz][2] = 1;
                px = npx;
                py = npy;
                pz = npz;
            }
            px = lseen[i][0];
            py = lseen[i][1];
            pz = i;
            while(pz < T - 1) {
                char npz = pz + 1;
                char npy;
                char npx;
                char cc = 0;
                if(tab[px][py][npz] == tab[px][py][pz] - 1) {
                    npx = px;
                    npy = py;
                    cc++;
                }
                if(px + 1 < W && tab[px + 1][py][npz] == tab[px][py][pz] - 1) {
                    npx = px + 1;
                    npy = py;
                    cc++;
                }
                if(py + 1 < H && tab[px][py + 1][npz] == tab[px][py][pz] - 1) {
                    npx = px;
                    npy = py + 1;
                    cc++;
                }
                if(px - 1 >= 0 && tab[px - 1][py][npz] == tab[px][py][pz] - 1) {
                    npx = px - 1;
                    npy = py;
                    cc++;
                }
                if(py - 1 >= 0 && tab[px][py - 1][npz] == tab[px][py][pz] - 1) {
                    npx = px;
                    npy = py - 1;
                    cc++;
                }
                if(cc != 1)
                    break;
                lseen[npz][0] = npx;
                lseen[npz][1] = npy;
                lseen[npz][2] = 1;
                px = npx;
                py = npy;
                pz = npz;
            }
        }
    char enc = 0;
    for(char i = 0; i < T; i++)
        if(lseen[i][2] == 1) {
//        if(total[i] == 1) {
            cout << "Time step " << (i + 1) << ": The robber has been at "
             << (lseen[i][0] + 1) << "," << (lseen[i][1] + 1) << "." << endl;
            enc = 1;
        }
    if(enc)
        return;
    cout << "Nothing known." << endl;
}

int main() {
    int n = 1;
    while(1) {
        cin >> W;
        cin >> H;
        cin >> T;
        if(W == 0 && H == 0 && T == 0)
            break;
        for(char i = 0; i < W; i++)
            for(char j = 0; j < H; j++)
                for(char k = 0; k < T; k++)
                    tab[i][j][k] = 0;
        for(char k = 0; k < T; k++)
            total[k] = 0;
        int rects;
        cin >> rects;
        int tiempo, top, left, bottom, right;
        for(int p = 0; p < rects; p++) {
            cin >> tiempo;
            cin >> left;
            cin >> top;
            cin >> right;
            cin >> bottom;
            char tt = tiempo - 1;
            for(char i = left - 1; i < right; i++)
                for(char j = top - 1; j < bottom; j++)
                    tab[i][j][tt] = -1;
        }
        cout << "Robbery #" << n << ":" << endl;
        llenar();
        cout << endl;
        n++;
    }
    return 0;
}
