#include <iostream>
#include <cmath>
#include <set>

#define MM  8000000

using namespace std;

unsigned long long total[MM];

unsigned long long calcular(unsigned long long num) {
  //  cout << "calculando " << cuantas << ", " << limite << endl;
 // unsigned long long multtotal = 1;
//  while(num == 0) {
    if((num % 2) != 0) {
/*      num = (num - 1)/2;
      continue;*/
      return total[(num - 1)/2];
    }
    if((num % 4) == 0) {
/*      num = num/2;
      continue;*/
      unsigned long long part = total[num/2];
      int cant = 0;
      while((num % 2) == 0) {
        num = num/2;
        cant++;
      }
      num = num - 1;
      while(cant != 1) {
        cant--;
        num = num*2 + 1;
      }
      return part + total[num];
    }
    // Tengo un numero que termina en 010 o 110
    if((num % 8) == 2) {
/*      multtotal = multtotal*2;
      num = (num - 2)/4;
      continue;*/
      return 2*total[(num - 2)/4];
    }
    // Solo queda el caso 110
    if((num % 8) != 6) {
      cerr << " ERROR !!! " << num << endl;
      return -1;
    }
    return 2*total[(num - 2)/4] + total[(num - 6)/4];
//  }
}

int main() {
  total[0] = 1;
  total[1] = 1;
  double buscando = ((double)123456789)/987654321;
  for(unsigned long long i = 0; i < MM; i++) {
    total[i] = calcular(i);
    cout << i << ": " << total[i] << endl;
    if((((double)total[i])/total[i-1]) == buscando) {
      cout << "El de arriba es !!!!" << endl;
    }
  }
   cout << "FIN" << endl;
	return 0;
}
