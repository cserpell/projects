import java.util.*;
import java.io.*;
import java.math.*;

class c111 {
    static public BigInteger[] sumaD;
    static public int[] maxD;
    static public int[] actualD;
    static public void main(String[] args) throws IOException {
        int CDIGS = 10;
        sumaD = new BigInteger[10];
        maxD = new int[10];
        actualD = new int[10];
        for(int i = 0; i < 10; i++) {
            sumaD[i] = BigInteger.valueOf(0);
            maxD[i] = 0;
        }
        BigInteger B1 = BigInteger.valueOf(1);
        BigInteger B2 = BigInteger.valueOf(2);
        BigInteger BT = new BigInteger("9999999999");
        BigInteger a = new BigInteger("999999999");
        while(true) {
            a = a.add(B2);
            if(a.compareTo(BT) >= 0)
                break;
            if(!a.isProbablePrime(10000))
                continue;
            for(int j = 0; j < 10; j++) {
                actualD[j] = 0;
            }
            String b = a.toString();
            for(int j = 0; j < CDIGS; j++) {
                actualD[b.charAt(j) - '0']++;
            }
            for(int i = 0; i < 10; i++) {
                if(maxD[i] == actualD[i]) {
                    sumaD[i] = sumaD[i].add(a);
                } else if(maxD[i] < actualD[i]) {
                    System.out.println(a + " cambia " + i + " a " + actualD[i]);
                    maxD[i] = actualD[i];
                    sumaD[i] = a;
                }
            }
        }
        BigInteger sumaTot = BigInteger.valueOf(0);
        for(int i = 0; i < 10; i++)
            sumaTot = sumaTot.add(sumaD[i]);
        System.out.println(sumaTot);
    }
}
