#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

unsigned long long total_fact (unsigned long long n, double logn, unsigned long long num) {
            unsigned long long max_log = (unsigned long long)(logn/log((double)num));
            unsigned long long total = 0;
            unsigned long long pot = num;
            for(unsigned long long l = 1; l <= max_log; l++) {
                total += n/pot; // Aproxima abajo
                pot = pot*num;
            }
            return num*total;
}

enteros yavistos;

bool squarefree(unsigned long long num) {
  enteros::iterator it = primos.begin();
  while(it != primos.end()) {
    if(*it > num)
      return true;
    if(num % ((*it)*(*it)) == 0)
      return false;
    it++;
  }
  return true;
}

int main() {
    unsigned long long i;
    for(i = 3; i < 55; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    unsigned long long comb[55];
    comb[0] = 1;
    for(int i = 1; i < 55; i++) {
      comb[i] = 0;
    }
    cout << "Fin calculo de primos" << endl;
    //int lev = 0;
    unsigned long long tot = 0;
    for(int i = 1; i < 51; i++) {
      for(int j = i; j >= 1; j--) {
        comb[j] = comb[j] + comb[j - 1];
        if(yavistos.find(comb[j]) != yavistos.end())
          continue;
        yavistos.insert(comb[j]);
        if(squarefree(comb[j])) {
       //   cout << comb[j] << " es square free" << endl;
          tot += comb[j];
        }
      }
    }
    cout << tot << endl;
    return 0;
}

int main231() {
    unsigned long long i;
    for(i = 3; i < 20000000; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    cout << "Fin calculo de primos" << endl;
    unsigned long long total_den1 = 0;
    unsigned long long total_den2 = 0;
    unsigned long long total_num = 0;
    unsigned long long n = 20000000;
    unsigned long long m = 15000000;
    unsigned long long nm = n - m;
    double logn = log((double)n);
    double logm = log((double)m);
    double lognm = log((double)nm);
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        unsigned long long num = *it;
        if(num <= n)
            total_num += total_fact(n, logn, num);
        if(num <= m)
            total_den1 += total_fact(m, logm, num);
        if(num <= nm)
            total_den2 += total_fact(nm, lognm, num);
    }
    unsigned long long res_final = total_num - total_den1 - total_den2;
    cout << "Suma = " << res_final << endl;
	return 0;
}
