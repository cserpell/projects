#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

int dsq(int n) {
    int maxd = 0;
    int diez = 10;
    while(n/diez > 0) {
        maxd++;
        diez *= 10;
    }
    int resto = n;
    int nuevo = 0;
    for(int i = 0; i <= maxd; i++) {
        diez /= 10;
        int dig = resto/diez;
        nuevo += dig*dig;
        resto -= dig*diez;
    }
    return nuevo;
}

int main() {
    int *tabla;
    
    tabla = new int[10000000];
    for(int i = 0; i < 10000000; i++)
        tabla[i] = 0;
    int contador = 0;
    for(int i = 1; i < 10000000; i++) {
//        std::cout << "empezando con " << i << std::endl;
        int num = i;
        while(1) {
    //        std::cout << num << " ";
            if(num == 1)
                break;
            if(num == 89) {
                tabla[i] = 1;
                break;
            }
            if(num < i) {
                if(tabla[num]) {
                    tabla[i] = 1;
                    break;
                }
                break;
            }
            num = dsq(num);
        }
  //      std::cout << std::endl;
        if(tabla[i])
            contador++;
    }
    std::cout << contador << std::endl;
	return 0;
}
