#include <iostream>
#include <sstream>
#include <cmath>

const int TOPE = 30000;

int cuantos[TOPE];

int main() {
    for(int i = 0; i < TOPE; i++) {
        cuantos[i] = 0;
    }
    for(int i = 1; i < 10000; i++) {
        for(int j = 1; j <= i; j++) {
            for(int k = 1; k <= j; k++) {
                int c2 = 0;
                int cper = 4*(i + j + k);
                int cini = 2*(i*j + i*k + j*k);
                int cfin = 0;
                int nuevo;
                while(1) {
                    nuevo = cini + c2*cper + cfin*8;
                //    std::cout << i << " " << j << " " << k
                 //    << "  - en " << (c2 + 1) << "-> " << nuevo << std::endl;
                    if(nuevo >= TOPE)
                        break;
                    cuantos[nuevo]++;
                    cfin += c2;
                    c2++;
                }
            }
        }
    }
    for(int i = 0; i < TOPE; i++) {
        if(cuantos[i] >= 1000)
            std::cout << "**** ";
        std::cout << i << ": " << cuantos[i] << std::endl;
    }
	return 0;
}
