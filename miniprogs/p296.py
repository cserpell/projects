# Tried 1105805788 and didn't work


def gcd(a, b):
  if a < b:
    return gcd(b, a)
  while a % b != 0:
    rem = a % b
    a = b
    b = rem
  return b


class P296(object):
  """Problem 296."""

  def __init__(self, stop_n=1000):
    self._stop_n = stop_n
    self._total = 0

  def try_ab(self, a, b):
    gg = gcd(a, b)
    if gg == 1:
      # Not possible
      return
    base = b / gg + a / gg
    limit_c = min(a + b, self._stop_n - a - b)
    if b % base == 0:
      c = b
    else:
      c = (int(b / base) + 1) * base
#    c = base
#    while c < b:
#      c += base
#    while c < limit_c:
#      y = (c * a) / (base * gg)
#      print 'Next triangle: a = %s b = %s c = %s y = %s Per = %s' % (a, b, c, y, a + b + c)
#      self._total += 1
#      c += base
    cand = limit_c - c
    if cand > 0:
      self._total += cand / base

  def try_all(self):
    for a in xrange(1, self._stop_n / 3 + 1):
      print 'Starting a = %s' % a
      for b in xrange(a, (self._stop_n - a) / 2 + 1):
        self.try_ab(a, b)

  def main(self):
    self.try_all()
    print 'For %s: %s' % (self._stop_n, self._total)


if __name__ == '__main__':
  P296().main()

