def testNextPos(SeenSeqs, CurrentSeq, FinalSum, CurrentNum) :
  L = 5
  megaL = 2 ** L
  if len(CurrentSeq) == megaL : # Full sequence, all has been seen!!
   # print "Starting textNextPos method... with " + str(len(CurrentSeq))
   # print "CurrentSeq = " + str(CurrentSeq)
   # print "SeenSeqs = " + str(SeenSeqs)
    # Now check border subsequences
    erasableSeqs = set()
    for a in range(1, L) :
      pos = megaL - L + a
      currStep = 0
      for b in range(0, L) :
        currStep = currStep * 2 + CurrentSeq[(pos + b) % megaL]
      if currStep in SeenSeqs :
        print "currStep " + str(currStep) + " discarded"
        for x in erasableSeqs :
          SeenSeqs.remove(x)
        return FinalSum # Not good
      SeenSeqs.add(currStep)
      erasableSeqs.add(currStep)
    print "Found one good sequence!"
    print CurrentSeq
    print CurrentNum
    FinalSum = FinalSum + CurrentNum
    for x in erasableSeqs :
      SeenSeqs.remove(x)
    return FinalSum
  CurrentNum = CurrentNum * 2
  proxStep = CurrentNum % megaL
  if not (proxStep in SeenSeqs) :
    SeenSeqs.add(proxStep)
    CurrentSeq.append(0)
    FinalSum = testNextPos(SeenSeqs, CurrentSeq, FinalSum, CurrentNum)
    CurrentSeq.pop() # Removes the last 0
    SeenSeqs.remove(proxStep)
  CurrentNum = CurrentNum + 1
  proxStep = proxStep + 1
  if not (proxStep in SeenSeqs) :
    SeenSeqs.add(proxStep)
    CurrentSeq.append(1)
    FinalSum = testNextPos(SeenSeqs, CurrentSeq, FinalSum, CurrentNum)
    CurrentSeq.pop() # Removes the last 1
    SeenSeqs.remove(proxStep)
  CurrentNum = (CurrentNum - 1) / 2
  return FinalSum

L = 5
SeenSeqs = set()
FinalSum = 0
CurrentSeq = []
for a in range(0, L) :
  CurrentSeq.append(0)
CurrentSeq.append(1)
SeenSeqs.add(0)
SeenSeqs.add(1)
CurrentNum = 1
FinalSum = testNextPos(SeenSeqs, CurrentSeq, FinalSum, CurrentNum)
print FinalSum

