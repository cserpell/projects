#include <iostream>

using namespace std;

int main() {
    int sumar = 0;
    for(int a = 3; a <= 1000; a++) {
        int rmax = 0;
        for(int n = 1; n < a*a; n++) {
            int cand = ((2*n*a) % (a*a));
            if(cand > rmax)
                rmax = cand;
        }
        sumar += rmax;
    }
    cout << sumar << endl;
    return 0;
}
