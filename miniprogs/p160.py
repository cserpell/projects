

def mm(a, ex):
  if not ex:
    return 1L
  if ex == 1L:
    return a
  if ex % 2L == 0L:
    ee2 = ex / 2L
    return mm(a, ee2) * mm(a, ee2)
  ee2 = ex / 2L
  return mm(a, ee2) * mm(a, ee2) * a


class P160(object):
  """Problem 160."""

  def __init__(self, stop_n=1000000000000):
    self._stop_n = stop_n
    self._number_2 = 0L
    self._number_5 = 0L
    self._current_last_5 = 1L
    self._last_n = 1L

  def get_next_old(self):
    next = self._last_n + 1L
    print 'iterating for %s' % next
    total_2 = 0
    current_pot_2 = 2L
    while current_pot_2 <= next:
      total_2 += next / current_pot_2
      current_pot_2 *= 2L
    print 'total2: %s' % total_2
    total_5 = 0
    current_pot_5 = 5L
    while current_pot_5 <= next:
      total_5 += next / current_pot_5
      current_pot_5 *= 5L
    print 'total5: %s' % total_5
    iter_n = next / mm(2L, total_2) / mm(5L, total_5)
    self._number_2 += total_2
    self._number_5 += total_5
    self._current_last_5 = (self._current_last_5 * iter_n) % 100000L
    self._last_n = next

  def get_next(self):
    next = self._last_n + 1L
    total_2 = 0
    number = next
    while number % 2L == 0:
      total_2 += 1L
      number /= 2L
    total_5 = 0
    while number % 5L == 0:
      total_5 += 1L
      number /= 5L
    self._number_2 += total_2
    self._number_5 += total_5
    self._current_last_5 = (self._current_last_5 * number) % 100000L
    self._last_n = next

  def whole(self):
    while self._last_n < self._stop_n:
      if self._last_n % 1000000 == 0:
        print '%s' % self._last_n
      self.get_next()
    while self._number_2 > self._number_5:
      self._current_last_5 = (self._current_last_5 * 2L) % 100000L
      self._number_2 -= 1
    while self._number_5 > self._number_2:
      self._current_last_5 = (self._current_last_5 * 5L) % 100000L
      self._number_5 -= 1

  def main(self):
    self.whole()
    print 'For %s: %s' % (self._stop_n, self._current_last_5)


if __name__ == '__main__':
  P160().main()

