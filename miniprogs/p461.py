import math


class A:
  BEST = [0, 0, 0, 0]
  BEST_SUM = 3.


def print_best_so_far():
  print 'Best so far: [%s] = %s' % (','.join(str(a) for a in A.BEST),
                                    A.BEST_SUM)


def test(current, n, pos, previous, so_far):
  if pos >= 4:
    final_sum = abs(so_far - 4 - math.pi)
    if final_sum < A.BEST_SUM:
      A.BEST_SUM = final_sum
      for i in range(4):
        A.BEST[i] = current[i]
      # print_best_so_far()
    return
  remain = math.pi + 4 - (3 - pos) * math.exp(float(previous) / n) - so_far
  if remain <= 0:
    final_sum = abs(remain)
    if final_sum < A.BEST_SUM:
      A.BEST_SUM = final_sum
      for i in range(pos):
        A.BEST[i] = current[i]
      for i in range(pos, 4):
        A.BEST[i] = previous
      # print_best_so_far()
    return
  max_test = int(n * math.log(remain)) + 1
  min_test = previous
  if pos == 3:
    min_test = max(0, max_test - 2)
  for t in range(previous, max_test + 1):
    current[pos] = t
    test(current, n, pos + 1, t, so_far + math.exp(float(t) / n))


def test_2(current, n, pos, previous, so_far):
  if pos > 4:
    final_sum = abs(so_far - 4 - math.pi)
    if final_sum < A.BEST_SUM:
      A.BEST_SUM = final_sum
      for i in range(4):
        A.BEST[i] = current[i]
      print_best_so_far()
    return
  t = previous
  if pos == 4:
    t = max(previous, int(n * math.log(4 + math.pi - so_far - A.BEST_SUM)))
  two = math.log(4 - pos + 1)
  while t <= n * (math.log(4 + math.pi + A.BEST_SUM - so_far) - two):
    current[pos - 1] = t
    test_2(current, n, pos + 1, t, so_far + math.exp(float(t) / n))
    t += 1


def reset(best_prev):
  for i in range(4):
    A.BEST[i] = 0
  A.BEST_SUM = best_prev


def try_n(n, best_prev):
  reset(best_prev)
  test_2([0, 0, 0, 0], n, 1, 0, 0.)
  final = 0
  for i in range(4):
    final += A.BEST[i] * A.BEST[i]
  print 'For n = %s: %s - [%s] = %s' % (
      n, final, ','.join(str(a) for a in A.BEST), A.BEST_SUM)


if __name__ == '__main__':
  try_n(2, A.BEST_SUM)
  try_n(4, A.BEST_SUM)
  try_n(8, A.BEST_SUM)
  try_n(40, A.BEST_SUM)
  try_n(200, A.BEST_SUM)
  try_n(1000, A.BEST_SUM)
  try_n(2000, A.BEST_SUM)
  try_n(10000, A.BEST_SUM)

