#include <iostream>
#include <cmath>
#include <set>
#include <string>
#include <list>

using namespace std;

typedef set<unsigned int> enteros;

class carta {
 public:
  int numero;
  int pinta;
  carta(string &s) {
    char a = s.c_str()[0];
    char b = s.c_str()[1];
    numero = -1;
    pinta = -1;
    if(a == '2') numero = 2;
    if(a == '3') numero = 3;
    if(a == '4') numero = 4;
    if(a == '5') numero = 5;
    if(a == '6') numero = 6;
    if(a == '7') numero = 7;
    if(a == '8') numero = 8;
    if(a == '9') numero = 9;
    if(a == 'T') numero = 10;
    if(a == 'J') numero = 11;
    if(a == 'Q') numero = 12;
    if(a == 'K') numero = 13;
    if(a == 'A') numero = 14;

    if(b == 'S') pinta = 1;
    if(b == 'D') pinta = 2;
    if(b == 'H') pinta = 3;
    if(b == 'C') pinta = 4;
  }
};

class mano {
 public:
  carta **cartas;
  int count[15];
  mano(string &s1, string &s2, string &s3, string &s4, string &s5) {//, int lado) {
    cartas = new carta *[5];
   // int pos = lado*15;
    for(int i = 0; i < 15; i++)
      count[i] = 0;
    cartas[0] = new carta(s1);
    count[cartas[0]->numero]++;
    cartas[1] = new carta(s2);
    count[cartas[1]->numero]++;
    cartas[2] = new carta(s3);
    count[cartas[2]->numero]++;
    cartas[3] = new carta(s4);
    count[cartas[3]->numero]++;
    cartas[4] = new carta(s5);
    count[cartas[4]->numero]++;
  }
  int tipo;
  void verTodo() {
    this->tipo = 0;
    if(this->onePair())
      this->tipo = 1;
    if(this->twoPairs())
      this->tipo = 2;
    if(this->threeOfKind())
      this->tipo = 3;
    if(this->straight())
      this->tipo = 4;
    if(this->flush())
      this->tipo = 5;
    if(this->fullHouse())
      this->tipo = 6;
    if(this->fourOfKind())
      this->tipo = 7;
    if(this->straightFlush())
      this->tipo = 8;
    if(this->royalFlush())
      this->tipo = 9;
  }
  bool onePair() {
    for(int i = 0; i < 15; i++) {
      if(count[i] == 2)
        return true;
    }
    return false;
  }
  bool twoPairs() {
    bool one = false;
    for(int i = 0; i < 15; i++) {
      if(count[i] == 2) {
        if(one) {
          return true;
        } else {
          one = true;
        }
      }
    }
    return false;
  }
  bool threeOfKind() {
    for(int i = 0; i < 15; i++) {
      if(count[i] == 3)
        return true;
    }
    return false;
  }
  bool straight() {
    bool enc = false;
    int total = 0;
    for(int i = 0; i < 15; i++) {
      if(count[i] == 1) {
        if(enc) {
          total++;
          if(total == 5)
            return true;
        } else {
          enc = true;
          total = 1;
        }
      } else {
        enc = false;
        total = 0;
      }
    }
    return false;
  }
  bool flush() {
    int pe = cartas[0]->pinta;
    for(int i = 1; i < 5; i++) {
      if(cartas[i]->pinta != pe)
        return false;
    }
    return true;
  }
  bool fullHouse() {
    return (this->onePair() && this->threeOfKind());
  }
  bool fourOfKind() {
    for(int i = 0; i < 15; i++) {
      if(count[i] == 4)
        return true;
    }
    return false;
  }
  bool straightFlush() {
    return (this->straight() && this->flush());
  }
  bool royalFlush() {
    if(!this->straightFlush())
      return false;
    if(count[10] != 1)
      return false;
    if(count[14] != 1)
      return false;
    return true;
  }
  int comparar(mano &otra) {
    if(otra.tipo > this->tipo)
      return -1;
    if(otra.tipo < this->tipo)
      return 1;
    if(this->tipo == 0) {
      for(int i = 14; i > 0; i--) {
        if(this->count[i] > otra.count[i])
          return 1;
        if(this->count[i] < otra.count[i])
          return -1;
      }
      cout << "ERROR, no era tirival" << endl;
      return 0;
    }
    if(this->tipo == 1) {
      for(int i = 0; i < 15; i++) {
        if(this->count[i] == 2 && otra.count[i] != 2)
          return -1;
        if(this->count[i] != 2 && otra.count[i] == 2)
          return 1;
        if(this->count[i] == 2 && otra.count[i] == 2) {
          for(int j = 14; j > 0; j--) {
            if(i == j)
              continue;
            if(this->count[j] > otra.count[j])
              return 1;
            if(this->count[j] < otra.count[j])
              return -1;
          }
          cout << "ERROR, pares iguales, pero resto iguales" << endl;
          return 0;
        }
      }
      cout << "ERROR, falto caso?" << endl;
      return 0;
    }
    if(this->tipo == 2) {
      bool one = false;
      int cualone = -1;
      for(int i = 14; i > 0; i--) {
        if(this->count[i] == 2 && otra.count[i] != 2)
          return 1;
        if(this->count[i] != 2 && otra.count[i] == 2)
          return -1;
        if(this->count[i] == 2 && otra.count[i] == 2) {
          if(!one) {
            one = true;
            cualone = i;
            continue;
          }
          for(int j = 14; j > 0; j--) {
            if(i == j)
              continue;
            if(cualone == j)
              continue;
            if(this->count[j] > otra.count[j])
              return 1;
            if(this->count[j] < otra.count[j])
              return -1;
          }
          cout << "ERROR, 2 pares iguales, pero resto iguales" << endl;
          return 0;
        }
      }
      cout << "ERROR, falto caso 2?" << endl;
      return 0;
    }
    if(this->tipo == 3) {
      for(int i = 0; i < 15; i++) {
        if(this->count[i] == 3 && otra.count[i] != 3)
          return -1;
        if(this->count[i] != 3 && otra.count[i] == 3)
          return 1;
        if(this->count[i] == 3 && otra.count[i] == 3) {
          for(int j = 14; j > 0; j--) {
            if(i == j)
              continue;
            if(this->count[j] > otra.count[j])
              return 1;
            if(this->count[j] < otra.count[j])
              return -1;
          }
          cout << "ERROR, trios iguales, pero resto iguales" << endl;
          return 0;
        }
      }
      cout << "ERROR, falto caso 3?" << endl;
      return 0;
    }
    if(this->tipo == 4) { //escala
      for(int i = 14; i > 0; i--) {
        if(this->count[i] > otra.count[i])
          return 1;
        if(this->count[i] < otra.count[i])
          return -1;
      }
      cout << "ERROR, no era tirival en escala" << endl;
      return 0;
    }
    if(this->tipo == 5) {
      for(int i = 14; i > 0; i--) {
        if(this->count[i] > otra.count[i])
          return 1;
        if(this->count[i] < otra.count[i])
          return -1;
      }
      cout << "ERROR, no era tirival en color" << endl;
      return 0;
    }
    if(this->tipo == 6) { // Full
      for(int i = 0; i < 15; i++) {
        if(this->count[i] == 3 && otra.count[i] != 3)
          return -1;
        if(this->count[i] != 3 && otra.count[i] == 3)
          return 1;
        if(this->count[i] == 3 && otra.count[i] == 3) {
          for(int j = 14; j > 0; j--) {
            if(i == j)
              continue;
            if(this->count[j] > otra.count[j])
              return 1;
            if(this->count[j] < otra.count[j])
              return -1;
          }
          cout << "ERROR, trio iguales, pero full iguales" << endl;
          return 0;
        }
      }
    }
    if(this->tipo == 7) { // Poker (4)
      for(int i = 0; i < 15; i++) {
        if(this->count[i] == 4 && otra.count[i] != 4)
          return -1;
        if(this->count[i] != 4 && otra.count[i] == 4)
          return 1;
        if(this->count[i] == 4 && otra.count[i] == 4) {
          for(int j = 14; j > 0; j--) {
            if(i == j)
              continue;
            if(this->count[j] > otra.count[j])
              return 1;
            if(this->count[j] < otra.count[j])
              return -1;
          }
          cout << "ERROR, poker iguales, pero resto iguales" << endl;
          return 0;
        }
      }
    }
    if(this->tipo == 8) { // escala color
      for(int i = 14; i > 0; i--) {
        if(this->count[i] > otra.count[i])
          return 1;
        if(this->count[i] < otra.count[i])
          return -1;
      }
      cout << "ERROR, no era tirival en escala color" << endl;
      return 0;
    }
   cout << "ERROR, Caso escala real color, jamas deberian chocar aqui" << endl;
   return 0;
  }
};

int main() {
  string s1, s2, s3, s4, s5, t1, t2, t3, t4, t5;
  int nlin = 0;
  int numg = 0;
  while(!cin.eof()) {
    cin >> s1;
    if(cin.eof())
      break;
    cin >> s2;
    cin >> s3;
    cin >> s4;
    cin >> s5;
    cin >> t1;
    cin >> t2;
    cin >> t3;
    cin >> t4;
    cin >> t5;
    nlin++;
    mano m1(s1, s2, s3, s4, s5);
    mano m2(t1, t2, t3, t4, t5);
    m1.verTodo();
    m2.verTodo();
    int res = m1.comparar(m2);
    cout << m1.tipo << ":";
    cout << m1.cartas[0]->numero << "," << m1.cartas[0]->pinta << " ";
    cout << m1.cartas[1]->numero << "," << m1.cartas[1]->pinta << " ";
    cout << m1.cartas[2]->numero << "," << m1.cartas[2]->pinta << " ";
    cout << m1.cartas[3]->numero << "," << m1.cartas[3]->pinta << " ";
    cout << m1.cartas[4]->numero << "," << m1.cartas[4]->pinta << "  ";
    cout << m2.tipo << ":";
    cout << m2.cartas[0]->numero << "," << m2.cartas[0]->pinta << " ";
    cout << m2.cartas[1]->numero << "," << m2.cartas[1]->pinta << " ";
    cout << m2.cartas[2]->numero << "," << m2.cartas[2]->pinta << " ";
    cout << m2.cartas[3]->numero << "," << m2.cartas[3]->pinta << " ";
    cout << m2.cartas[4]->numero << "," << m2.cartas[4]->pinta << " ";
    if(res > 0) {
      cout << "Gana 1" << endl;
      numg++;
    } else if(res < 0) {
      cout << "Gana 2" << endl;
    } else {
      cout << "ERROOOOOOOOOOR en linea " << nlin << endl;
    }
    cout << "TOTAL : " << numg << endl;
  }
	return 0;
}
