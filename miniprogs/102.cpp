#include <iostream>
#include <cstdlib>

using namespace std;

int ar2(int px1, int py1, int px2, int py2, int px3, int py3) {
    return (px2 - px1)*(py3 - py1) - (px3 - px1)*(py2 - py1);
}

int main() {
    int count = 0;
    for(int i = 0; i < 1000; i++) {
        string s;
        cin >> s;
        int p1 = s.find(",", 0);
        int p2 = s.find(",", p1 + 1);
        int p3 = s.find(",", p2 + 1);
        int p4 = s.find(",", p3 + 1);
        int p5 = s.find(",", p4 + 1);
        int px1 = atoi(s.substr(0, p1).c_str());
        int py1 = atoi(s.substr(p1 + 1, p2).c_str());
        int px2 = atoi(s.substr(p2 + 1, p3).c_str());
        int py2 = atoi(s.substr(p3 + 1, p4).c_str());
        int px3 = atoi(s.substr(p4 + 1, p5).c_str());
        int py3 = atoi(s.substr(p5 + 1).c_str());
        int a21 = ar2(px1, py1, px2, py2, 0, 0);
        int a22 = ar2(px2, py2, px3, py3, 0, 0);
        int a23 = ar2(px3, py3, px1, py1, 0, 0);
        int a2g = ar2(px1, py1, px2, py2, px3, py3);
        if(a2g > 0 && a21 > 0 && a22 > 0 && a23 > 0)
            count++;
        else if(a2g < 0 && a21 < 0 && a22 < 0 && a23 < 0)
            count++;
    }
    cout << count << endl;
    return 0;
}
