
MOD = 1000000000


class Error(Exception):
  """Bla."""


SOME_RES2 = {}


def f(n):
  if n in SOME_RES2:
    return SOME_RES2[n]
  while n > 3:
    m4 = n % 4
    if m4 == 0:
      n = n / 4
    elif m4 == 1:
      c = (2 * f((n - 1) / 2 + 1) - f((n - 1) / 4)) % MOD
      SOME_RES2[n] = c
      return c
    elif m4 == 2:
      n = n / 2
    elif m4 == 3:
      c = (3 * f((n - 3) / 2 + 1) - 2 * f((n - 3) / 4)) % MOD
      SOME_RES2[n] = c
      return c
  if n == 1:
    return 1
  if n == 2:
    return 1
  if n == 3:
    return 3
  return 0


SOME_RES = {}


def s(n):
  if n < 4:
    if n == 1:
      return 1
    if n == 2:
      return 2
    if n == 3:
      return 5
    return 0
  if n in SOME_RES:
    return SOME_RES[n]
  norig = n
  m4 = n % 4
  tot = 0
  if m4 == 0:
    tot = f(n)
    n -= 1
  elif m4 == 1:
    tot = (f(n) + f(n - 1)) % MOD
    n -= 2
  elif m4 == 2:
    tot = (f(n) + f(n - 1) + f(n - 2)) % MOD
    n -= 3
  c = (tot + 6 * s((n - 3) / 2 + 1) - 8 * s((n - 3) / 4) - 1) % MOD
  SOME_RES[norig] = c
  return c


def main():
  print s(8)
  print s(100)
  print s(3**37)


if __name__ == '__main__':
  main()

