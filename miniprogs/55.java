import java.util.*;
import java.io.*;
import java.math.*;

class c55 {
    static public BigInteger retPalin(BigInteger B) {
        String a = B.toString();
        String g = "";
        for(int i = 0; i < a.length(); i++) {
            g = g.concat("" + a.charAt(a.length() - i - 1));
        }
        return new BigInteger(g);
    }
    static public boolean isPalin(BigInteger B) {
        String a = B.toString();
        for(int i = 0; i < a.length()/2; i++) {
            if(a.charAt(i) != a.charAt(a.length() - i - 1))
                return false;
        }
        return true;
    }
    static public void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int cant = 0;
        for(int i = 10; i < 10000; i++) {
            BigInteger nuevo = new BigInteger("" + i);
            boolean es = false;
            for(int j = 1; j <= 50; j++) {
                BigInteger pal = retPalin(nuevo);
            //    System.out.print(nuevo);
                nuevo = pal.add(nuevo);
          //      System.out.println(" + " + pal + "->" + nuevo);
                if(isPalin(nuevo)) {
                    es = true;
                    System.out.println(i + " tomo " + j + " iters");
                    break;
                }
            }
            if(!es) {
                cant++;
            }
        }
        System.out.println(cant);
    }
}
