#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long canttotal;

void rec(int *st, int *ocupado, int pos, int maxlargo) {
    if(pos == maxlargo) {
        int sc = 0;
        for(int i = 1; i < pos; i++)
            if(st[i - 1] < st[i])
                sc++;
        if(sc == 1)
            canttotal++;
        return;
    }
    for(int i = 0; i < 26; i++)
        if(!ocupado[i]) {
            st[pos] = i;
            ocupado[i] = 1;
            rec(st, ocupado, pos + 1, maxlargo);
            ocupado[i] = 0;
        }
}

int main() {
    unsigned long long lastint = 325;
    unsigned long long maxint = 325;
    int donde = 2;
    unsigned long long t = 0;
    for(int i = 2; i <= 26; i++) {
        unsigned long long ni = i - 1;
        if(i > 2)
            lastint = (lastint*(26 - ni)*(2*t + ni))/(t*(ni + 1));
        else
            lastint = 13*25;
        if(lastint > maxint) {
            maxint = lastint;
            donde = i;
        }
        cout << i << ": " << lastint << endl;
        t = 2*t + ni;
    }
    cout << "Max(" << donde << "): " << maxint << endl;
    
    int *st = new int[6];
    int *ocupado = new int[26];
    for(int i = 0; i < 26; i++)
        ocupado[i] = 0;
    for(int i = 1; i < 6; i++) {
        canttotal = 0;
        rec(st, ocupado, 0, i);
        cout << i << " -> " << canttotal << endl;
    }
    return 0;
}
