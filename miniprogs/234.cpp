#include <iostream>
#include <cmath>
#include <set>
#include <cstdlib>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

unsigned long long sumar(unsigned long long MN) {
  unsigned long long suma = 0;
  enteros::iterator it = primos.begin();
  unsigned long long last_prim = *it;
  it++;
  while((it != primos.end()) && (last_prim*last_prim < MN)) {
    unsigned long long new_prim = *it;
    unsigned long long test = last_prim*last_prim + last_prim;
    while(test < new_prim*new_prim && test <= MN) {
      if((test % new_prim) != 0) {
        suma += test;
    //    cout << test << endl;
      }
      test += last_prim;
    }
    test = new_prim*new_prim - new_prim;
    if(test > MN)
      test = MN;
    while((test % new_prim) != 0)
      test--;
    while(test > last_prim*last_prim) {
      if((test % last_prim) != 0) {
        suma += test;
    //    cout << test << endl;
      }
      test -= new_prim;
    }
    last_prim = new_prim;
    it++;
  }
  return suma; 
}

int main(int argc, char *argv[]) {
    unsigned long long i;
    for(i = 3; i <  1100000LL; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    unsigned long long n = 999966663333LL;
    if(argc > 1)
      n = atoi(argv[1]);
    unsigned long long tot = sumar(n);
    cout << "Suma = " << tot << endl;
	return 0;
}
