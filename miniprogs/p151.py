"""Problem 151."""
import random

MAX_SIZE = 5
MAX_STEPS = (2 ** (MAX_SIZE - 1)) - 2

EXPECTED = {
    (5,): 0
}


def get_sample():
    sheets = list(range(2, MAX_SIZE + 1))
    n_sheets = MAX_SIZE - 1
    num_ones = 0
    for reps in range(MAX_STEPS):
        if n_sheets == 1:
            num_ones += 1
            sheet = sheets.pop()
        else:
            sheet = sheets.pop(random.randint(0, n_sheets - 1))
        n_sheets -= 1
        if sheet != MAX_SIZE:
            for num in range(sheet + 1, MAX_SIZE + 1):
                sheets.append(num)
            n_sheets += MAX_SIZE - sheet
        # print(sheets)
    # print(num_ones)
    return num_ones


def expected(current):
    current = sorted(current)
    ttuple = tuple(current)
    if ttuple not in EXPECTED:
        print(ttuple)
        num = 0.0
        ltuple = len(ttuple)
        if ltuple == 1:
            print(f'+1')
            num += 1.0
        nnext = 0.0
        for ind in range(0, ltuple):
            print(f'A{current}')
            val = current.pop(ind)
            for val_add in range(val + 1, MAX_SIZE + 1):
                current.insert(ind, val_add)
            nnext += expected(current)
            for val_add in range(val + 1, MAX_SIZE + 1):
                current.pop(ind)
            current.insert(ind, val)
            print(f'B{current}')
        print(f'+{nnext} / {ltuple}')
        num += nnext / ltuple
        print(f'expected( {ttuple} ) = {num}')
        EXPECTED[ttuple] = num
    return EXPECTED[ttuple]


def main():
    """Main execution point."""
    print(expected([4]))
    print('***************')
    print(expected([3]))
    print('***************')
    print(expected([2]))
    print('***************')
    print(expected([1]))
    return
    max_n = 1000000
    estim = 0
    for n in range(max_n):
        if n % 100000 == 0:
            print(n)
        estim += get_sample()
    estim /= max_n
    print(estim)


if __name__ == '__main__':
    main()
