#include <iostream>
#include <sstream>

int main() {
    int triang = 1;
    int num = 2;
    while(1) {
        int divisores = 0;
        for(int i = 1; i <= triang; i++) {
            if(triang % i == 0)
                divisores++;
        }
        std::cout << triang << " tiene " << divisores << std::endl;
        if(divisores > 500)
            break;
        triang += num;
        num++;
    }
    std::cout << triang << std::endl;
	return 0;
}
