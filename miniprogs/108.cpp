#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<int> enteros;

int isPrime(enteros & primos, int n) {
    int hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}    

int main() {
    enteros primos;
    for(int j = 3; j < 100000; j += 2) {
        isPrime(primos, j);
    }
    primos.insert(2);
    
    unsigned long long val = 1;
    unsigned long long lval = 1;
/*    unsigned long long cant = 1;
    enteros::iterator it = primos.begin();
    while(cant < 100) {
        lval = val;
        val *= *it;
        cant *= 2;
        it++;
    }
    cout << val << " -> " << cant << endl;
*/
lval = 1;
val = 10000000;

    unsigned long long minval;
    unsigned long long cual;
    for(unsigned long long i = lval; i < val; i++) {
        unsigned long long count = 0;
        for(unsigned long long b = 1; b <= i; b++) {
            unsigned long long x = i + b;
            unsigned long long y = (i*x)/b;
            if(i*(x + y) == x*y) {
    //            cout << i << " -> x: " << x << " y: " << y << endl;
                count++;
            }
        }
        if(count > 1000) {
            minval = count;
            cual = i;
            break;
        }
        cout << i << " " << count << endl;// << (2*count - 1) << endl;
    }
    cout << cual << " -> " << minval << endl;
	return 0;
}
