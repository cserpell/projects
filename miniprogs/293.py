import sys
import random
import math

def completePrimes (primes, finish, primeshash) :
  primes.append(2)
  primeshash[2] = 1
  i = 3
  while i < finish :
    ei = i // 2 + 1
    isp = True
    for a in primes :
      if a > ei :
        break
      if i % a == 0 :
        isp = False
        break
    if isp :
     # print "Adding " + str(i)
      primes.append(i)
      primeshash[i] = 1
    i = i + 2
    if i % 1000000 == 1 :
      print i

fin = 100
if len(sys.argv) > 1 :
  fin = int(sys.argv[1])

print "Computing primes..."
primes = []
primeshash = {}
completePrimes(primes, 3*fin, primeshash)
print "End of computing primes"

admis = []

p2 = 2
while p2 < fin :
  admis.append(p2)
  p2 = p2 * 2

lastprime = 2

for newprime in primes :
  if newprime == 2 :
    continue
  nadmis = []
  for a in admis :
    if a % lastprime == 0 :
      j = a*newprime
      while j < fin :
        nadmis.append(j)
        j = j * newprime
  lastprime = newprime
  admis += nadmis

print "Total admisible numbers: " + str(len(admis))
admis.sort()
totalsum = 0
for n in admis :
  m = 3
  while not primeshash.has_key(n + m) :
    m = m + 2
  print "For " + str(n) + "  m = " + str(m)
  totalsum = totalsum + m
print "TOTAL : " + str(totalsum)
