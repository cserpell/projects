#include <iostream>
#include <sstream>
#include <cmath>

int isPalin10(unsigned int n) {
    int d1, d2, d3, d4, d5, d6;
    
    d1 = n/100000;
    d2 = (n % 100000)/10000;
    d3 = (n % 10000)/1000;
    d4 = (n % 1000)/100;
    d5 = (n % 100)/10;
    d6 = n % 10;
    //std::cout << d1 << d2 << d3 << d4 << d5 << d6 << std::endl;
    if(d1 == 0) {
        if(d2 == 0) {
            if(d3 == 0) {
                if(d4 == 0) {
                    if(d5 == 0) {
                        std::cout << n << std::endl;
                        return 1;
                    }
                    if(d6 == d5) {
                        std::cout << n << std::endl;
                        return 1;
                    }
                }
                if(d4 == d6) {
                    std::cout << n << std::endl;
                    return 1;
                }
            }
            if(d3 == d6 && d4 == d5) {
                std::cout << n << std::endl;
                return 1;
            }
        }
        if(d2 == d6 && d3 == d5) {
            std::cout << n << std::endl;
            return 1;
        }
    }
    if(d1 == d6 && d2 == d5 && d3 == d4) {
        std::cout << n << std::endl;
        return 1;
    }
    return 0;
}

int isPalin2(unsigned int *pote, unsigned int n) {
    unsigned int *d;
    
    d = new unsigned int[20];
    for(int i = 0; i < 20; i++) {
//        std::cout << pote[i] << std::endl;
        d[i] = (n & pote[i]) >> i;
    }
    int largo = 19;
    for(; largo >= 0; largo--) {
        if(d[largo])
            break;
    }
    largo++;
    for(int i = 0; i < largo/2; i++) {
        if(d[i] != d[largo - i - 1]) {
            delete d;
            return 0;
        }
    }
    delete d;
    for(int i = 0; i < 20; i++) {
        std::cout << d[19 - i];
    }
    std::cout << std::endl;
    return 1;
}

int main() {
    unsigned int *tabla;
    
    tabla = new unsigned int[20];
    unsigned int val = 1;
    for(int i = 0; i < 20; i++) {
        tabla[i] = val;
        val *= 2;
    }
    for(int i = 0; i < 20; i++) {
        std::cout << tabla[i] << std::endl;
    }
    unsigned int cant = 0;
    for(unsigned int i = 1; i < 1000000; i++) {
        if(isPalin10(i) && isPalin2(tabla, i)) {
            std::cout << i << " sirve!!" << std::endl;
            cant += i;
        }
    }
    std::cout << (cant) << std::endl;
	return 0;
}
