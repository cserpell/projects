class SortedTree(object):
    def __init__(self, val=None):
        self.val = val
        self.child1 = None
        self.child2 = None

    def insert(self, val):
        if self.val is None:
            self.val = val
            return
        if val < self.val:
            if self.child1 is None:
                self.child1 = SortedTree(val)
            else:
                self.child1.insert(val)
        else:
            if self.child2 is None:
                self.child2 = SortedTree(val)
            else:
                self.child2.insert(val)

    def get_min(self):
        if self.val is None:
            return None
        if self.child1 is not None:
            return self.child1.get_min()
        return self.val

    def iterate(self):
        vals = []
        if self.child1 is not None:
            vals.extend(self.child1.iterate())
        if val is not None:
            vals.append(val)
        if self.child2 is not None:
            vals.extend(self.child2.iterate())
        return vals

    def remove(self, val):
        if self.val == val:
            if self.child2 is not None:
                self.val = self.child2.val
                if self.child2.child1 is not None:
                    if self.child1 is None:
                        self.child1 = self.child2.child1
                    else:
                        for value in self.child2.child1.iterate():
                            self.child1.insert(value)
                self.child2 = self.child2.child2
            elif self.child1 is not None:
                self.val = self.child1.val
                self.child1 = self.child1.child1
                self.child2 = self.child1.child2
            else:
                self.val = None
            return val
        elif val < self.val:
            if self.child1 is None:
                return None
            return self.child1.remove(val)
        # val > self.val
        if self.child2 is None:
            return None
        return self.child2.remove(val)

