#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<int> enteros;

enteros p[6];

int encontrado[6];

int func(int pos, int i) {
    switch(pos) {
      case 0:
        return (i*(i + 1))/2;
      case 1:
        return i*i;
      case 2:
        return (i*(3*i - 1))/2;
      case 3:
        return i*(2*i - 1);
      case 4:
        return (i*(5*i - 3))/2;
      case 5:
        return i*(3*i - 2);
      default:
        ;
    }
    return -1;
}

int rec(int pos, int cola, int last, int first) {
    if(pos == 6 && (first/100) == cola)
        return 1;
    for(int i = 0; i < 6; i++) {
       if(encontrado[i])
            continue;
       //int i = pos;
        encontrado[i] = 1;
        for(enteros::iterator it = p[i].begin(); it != p[i].end(); it++) {
            int val = func(i, *it);
            if(val >= 1000) {
                if(pos == 0 || (val/100) == cola) {
                    if(pos == 0)
                        first = val;
                    if(rec(pos + 1, val%100, val, first)) {
                        cout << (*it) << " -> " << val << endl;
                        return 1;
                    }
                }
            }
        }
        encontrado[i] = 0;
    }
    return 0;
}

int main() {
    for(int j = 0; j < 6; j++) {
    for(int i = 1; i < 10000; i++) {
        int val = func(j, i);
        if(val >= 10000)
            break;
        p[j].insert(i);
    }
    }
    enteros::iterator it;
    for(int i = 0; i < 6; i++)
        for(it = p[i].begin(); it != p[i].end(); it++)
            if(func(i, *it) >= 1000)
                cout << "p" << (i + 3) << ": " << func(i, *it) << endl;
    for(int i = 0; i < 6; i++)
        encontrado[i] = 0;
    rec(0, -1, -1, -1);
	return 0;
}
