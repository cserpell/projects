"""Problem 103."""
import copy


def recurrent_check(len_so_far, sum_so_far, list_so_far, all_sums_so_far, min_sums_len, max_sums_len, number):
    if len_so_far == number:
        print(f'Length {number} list {list_so_far} with sum {sum_so_far}')
        return True
    trying = 1 if len_so_far == 0 else list_so_far[-1] + 1
    max_sums_2 = 40 if len_so_far <= 2 else max_sums_len[2] - 1
    print(max_sums_2)
    while trying < max_sums_2:
        new_all_sums_so_far = copy.deepcopy(all_sums_so_far)
        # for length in range(len_so_far, 0, -1):
        valid = True
        for length in range(len_so_far + 1):
            for key, val in all_sums_so_far[length].items():
                print(key)
                print(list(key) + [trying])
                print(val)
                new_subset = tuple(list(key) + [trying])
                new_sum = val + trying
                new_all_sums_so_far[length + 1][new_subset] = new_sum
                for check_length in range(length + 2, len_so_far + 1):
                    if new_sum >= min_sums_len[check_length]:
                        print(f'New sum {new_subset} is bigger than minimum of length {check_length} ({min_sums_len[check_length]})')
                        valid = False
                        break
                if not valid:
                    break
            if not valid:
                break
        if valid:
            # Check that all subsets have different sums
            checked = set()
            for length in range(len_so_far + 2):
                for key, val in new_all_sums_so_far[length].items():
                    if val in checked:
                        valid = False
                        break
                    checked.add(val)
                if valid == False:
                    break
            if valid:
                print(f'Valid: {list_so_far + [trying]} sum: {sum_so_far + trying}')
                print(f'Complete dict: {new_all_sums_so_far}')
                new_min_sums_len = {length: min(new_all_sums_so_far[length].values()) if length <= len_so_far + 1 else 100000 for length in range(number + 1)}
                new_max_sums_len = {length: max(new_all_sums_so_far[length].values()) if length <= len_so_far + 1 else 100 for length in range(3)}
                print(f'Complete mins: {new_min_sums_len}')
                print(f'Complete maxs: {new_max_sums_len}')
                res = recurrent_check(
                        len_so_far + 1,
                        sum_so_far + trying,
                        list_so_far + [trying],
                        new_all_sums_so_far,
                        new_min_sums_len,
                        new_max_sums_len,
                        number)
                if res:
                    return True
        trying += 1
    return False


def main(number):
    print(f'Starting length {number}')
    def_dict = {length: {} for length in range(number + 1)}
    def_dict_maxs = {length: 1000000 for length in range(number + 1)}
    def_dict_mins = {length: 0 for length in range(number + 1)}
    def_dict[0][tuple()] = 0
    recurrent_check(0, 0, [], def_dict, def_dict_mins, def_dict_maxs, number)


if __name__ == '__main__':
    main(7)

