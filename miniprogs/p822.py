"""Problem 822."""
import bisect

MOD = 1234567891


def mega_pow(base, exp2, dict_res):
    if exp2 in dict_res:
        return dict_res[exp2]
    if exp2 == 1:
        dict_res[exp2] = pow(base, 2, MOD)
    else:
        dict_res[exp2] = pow(mega_pow(base, exp2 - 1, dict_res), 2, MOD)
    return dict_res[exp2]


def iter_one(sorted_list, nnn, initial_part=True):
    first = sorted_list.pop(0)  # First element
    if not initial_part:
        squared = pow(first, 2, MOD)
        sorted_list.append(squared)
    else:
        squared = first * first
        # Get position of last element < squared
        ind = bisect.bisect_left(sorted_list, squared)
        bisect.insort_right(sorted_list, squared)
        if ind == nnn - 2:
            initial_part = False
    return initial_part


def main(nnn, mmm, print_res=True):
    sorted_list = list(range(2, nnn + 1))
    initial_part = True
    first_part = True
    curr_m = 0
    if print_res:
        print(f'm: {curr_m} list: {sorted_list}')
    while curr_m < mmm:
        if curr_m % 1000 == 0:
            print(f'm = {curr_m} ...')
        curr_m += 1
        initial_part = iter_one(sorted_list, nnn, initial_part=initial_part)
        if print_res:
            print(f'm: {curr_m} list: {sorted_list}')
        if not initial_part and first_part:
            # Finished first part!
            kkk = (mmm - curr_m) // (nnn - 1)
            mmm = mmm - curr_m - kkk * (nnn - 1)
            print(f'New M = {mmm} K = {kkk}')
            if kkk > 0:
                sorted_list = [pow(item, pow(2, kkk, MOD - 1), MOD) for item in sorted_list]
                # sorted_list = [mega_pow(item, kkk, {}) for item in sorted_list]
            if print_res:
                print(f'list: {sorted_list}')
            curr_m = 0
            first_part = False
    the_sum = 0
    for item in sorted_list:
        the_sum = (the_sum + item % MOD) % MOD
    print(the_sum)


if __name__ == '__main__':
    main(5, 3)
    main(10, 100)
    main(10000, 10 ** 16, print_res=False)

