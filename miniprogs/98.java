import java.util.*;
import java.io.*;
import java.math.*;
import java.lang.*;

class wordy {
  public char[] l;
  public String pal;
  public ArrayList<wordy> lista;
  public wordy(String w) {
    l = new char[w.length()];
    for(int i = 0; i < w.length(); i++) {
      l[i] = w.charAt(i);
    }
    lista = new ArrayList<wordy>();
    pal = w;
  }
  public void fillList(ArrayList<wordy> complete) {
    for(int i = 0; i < complete.size(); i++) {
      wordy ww = complete.get(i);
      int tusada = 0;
      boolean[] usada = new boolean[l.length];
      if(pal.equals(ww.pal))
        continue;
      if(ww.l.length != l.length)
        continue;
      for(int j = 0; j < l.length; j++) {
        char busca = ww.l[j];
        for(int k = 0; k < l.length; k++) {
          if(usada[k])
            continue;
          if(busca == l[k]) {
            usada[k] = true;
            tusada++;
            break;
          }
        }
      }
      if(tusada == l.length) {
        // Son anagramas !!!
        lista.add(ww);
      }
    }
  }
}

class c98 {
    static public void probar(wordy a, wordy b, int llenas, int[] v) {
      if(llenas == a.l.length) {
        int val1 = 0;
        int val2 = 0;
        boolean[] usa = new boolean[a.l.length];
        for(int i = 0; i < a.l.length; i++) {
          val1 += v[i]*Math.pow(10, a.l.length - i - 1);
          for(int k = 0; k < b.l.length; k++) {
            if((!usa[k]) && (b.l[k] == a.l[i])) {
              val2 += v[i]*Math.pow(10, b.l.length - k - 1);
              usa[k] = true;
            }
          }
        }
        int sq1 = (int)Math.sqrt((double)val1);
        int sq2 = (int)Math.sqrt((double)val2);
        if(sq1*sq1 == val1 && sq2*sq2 == val2) {
          // Candidatooooo!!
          if(val1 > val2) {
            System.out.print(val1 + "\t(" + val2 + ")\t");
          } else 
            System.out.print(val2 + "\t(" + val1 + ")\t");
          for(int i = 0; i < a.l.length; i++) {
            System.out.print(a.l[i] + "" + v[i]);
          }
          System.out.println("");
        }
        return;
      }
        boolean enc = false;
        for(int j = 0; j < llenas; j++) {
          if(a.l[j] == a.l[llenas]) {
            v[llenas] = v[j];
            probar(a, b, llenas + 1, v);
            enc = true;
            break;
          }
        }
        if(!enc) {
          for(int n = 0; n <= 9; n++) {
            if((n == 0) && (llenas == 0))
              continue;
            boolean cambian = false;
            for(int j = 0; j < llenas; j++) {
              if(v[j] == n) {
                cambian = true;
                break;
              }
            }
            if(cambian)
              continue;
            v[llenas] = n;
            probar(a, b, llenas + 1, v);
          }
        }

    }
    static public void main(String[] args) {
        ArrayList<wordy> complete = new ArrayList<wordy>();
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()) {
          String palabra = sc.next();
          complete.add(new wordy(palabra));
        }
        for(int i = 0; i < complete.size(); i++) {
          complete.get(i).fillList(complete);
        }

        for(int i = 0; i < complete.size(); i++) {
          wordy w = complete.get(i);
//          System.out.print(w.pal);
          for(int j = 0; j < w.lista.size(); j++) {
            System.out.println(w.pal + "\t" + w.lista.get(j).pal);
            probar(w, w.lista.get(j), 0, new int[w.l.length]);
          }
//          System.out.println("");
        }
   //     System.out.println(suma);
    }
}
