#include <iostream>

int main() {
	for(int i = 1; i < 1000; i++)
		for(int j = 1; j < 1000; j++)
			for(int k = 1; k < 1000; k++) {
				if(i*i + j*j != k*k)
					continue;
				if(i + j + k != 1000)
					continue;
				std::cout << (i*j*k) << std::endl;
			}
	return 0;
}

