"""Problem 800."""
import all_primes
import math


def main(max_n_e, max_n_l):
    """Main."""
    lmn = max_n_e * math.log(max_n_l)
    primes = [p for p in all_primes.PRIMES if math.log(p) <= lmn]
    nums = 0
    for p1 in range(len(primes)):
        pp1 = primes[p1]
        lp1 = math.log(pp1)
        for p2 in range(p1 + 1, len(primes)):
            pp2 = primes[p2]
            if pp2 > lmn / lp1 - pp1:
                break
            lp2 = math.log(pp2)
            if pp1 * lp2 + pp2 * lp1 <= lmn:
                print(f'Primes {pp1} and {pp2} match')
                nums += 1
    print(nums)


if __name__ == '__main__':
    main(800800, 800800)
