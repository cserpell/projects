"""Problem 813."""
MOD = 1000000007


def mult_cands(cands1, cands2):
    cands = set()
    for pos in cands2:
        new_cands = [cand + pos for cand in cands1]
        for cand in new_cands:
            if cand in cands:
                cands.remove(cand)
            else:
                cands.add(cand)
    return sorted(list(cands))


def pow3(cands):
    rcands = mult_cands(cands, cands)
    rcands = mult_cands(rcands, cands)
    return rcands


def square(cands):
    return mult_cands(cands, cands)


def get_value(cands):
    res = 0
    for cand in cands:
        res = (res + pow(2, cand, MOD)) % MOD
    return res


def main():
    """Main execution point."""
    cands = [0, 1, 3]
    print(get_value(square(cands)))
    for _ in range(52):
        cands = square(cands)
        print(cands)
    # cands = [1, 0]
    for _ in range(8):
        cands = pow3(cands)
        print(cands)
    res = 0
    # base = 2 ** 52
    for cand in cands:
        res = (res + pow(2, cand, MOD)) % MOD
    print(res)
    print(get_value(cands))


if __name__ == '__main__':
    main()

