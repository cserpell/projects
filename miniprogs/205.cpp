#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long cantmanerasC[37][7];
unsigned long long cantmanerasP[37][10];

unsigned long long calcular(unsigned long long cuantas, unsigned long long nd, unsigned long long nm, bool C) {
    if(nd <= 0)
      return 0;
    if(nd == 1) {
      if(cuantas >= 1 && cuantas <= nm)
        return 1;
      return 0;
    }
    if(cuantas < nd)
      return 0;
    unsigned long long res = 0;
    for(unsigned long long i = 1; i <= nm && i < cuantas; i++) {
      //cout << "agregando " << i << " = [" << (cuantas - i) << "][" << (nd - 1) << "]" << endl;
      if(C)
        res = res + cantmanerasC[cuantas - i][nd - 1];
      else
        res = res + cantmanerasP[cuantas - i][nd - 1];
    }
    return res;
}

int main() {
    for(unsigned long long j = 1; j <= 6; j++) {
      for(unsigned long long i = 0; i <= 36; i++) {
        unsigned long long res = calcular(i, j, 6, true);
        cout << i << " , " << j << " -> " << res << endl;
        cantmanerasC[i][j] = res;
      }
    }
    for(unsigned long long j = 1; j <= 9; j++) {
      for(unsigned long long i = 0; i <= 36; i++) {
        unsigned long long res = calcular(i, j, 4, false);
        cout << i << " , " << j << " -> " << res << endl;
        cantmanerasP[i][j] = res;
      }
    }
    
    unsigned long long tot_ganadas = 0;
    
    for(unsigned long long j = 6; j <= 36; j++) {
      unsigned long long tot_aux = 0;
      for(unsigned long long i = j + 1; i <= 36; i++) {
        tot_aux += cantmanerasP[i][9];
      }
      tot_ganadas += tot_aux*cantmanerasC[j][6];
    }
    cout << tot_ganadas << endl;
    cout << (((unsigned long long)6)*6*6*6*6*6 * 4*4*4*4*4*4*4*4*4) << endl;
   cout << "FIN" << endl;
	return 0;
}
