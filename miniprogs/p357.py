import math

print 'Importing primes'
import all_primes
print 'Ready'


class P357(object):

  def __init__(self):
    print 'Initializing primes'
    self._primes_d = set(prime for prime in all_primes.PRIMES)
    print 'Ready'

  def factors(self, num):
    if num == 1:
      return {}
    res = num
    so_far = {}
    for prime in all_primes.PRIMES:
      if prime * prime > res:
        break
      total_p = 0
      while res % prime == 0:
        res = res / prime
        total_p += 1
      if total_p:
        so_far[prime] = total_p
    if res > 1:
      if res not in so_far:
        so_far[res] = 0
      so_far[res] += 1
    return so_far

  def rec_try(self, num, sq_num, so_far, p_list, div, pos):
    if pos >= len(p_list):
      # print 'Trying divisor %s of %s' % (div, num)
      return bool(div + num / div in self._primes_d)
    for t_one in range(so_far[p_list[pos]] + 1):
      res = self.rec_try(num, sq_num, so_far, p_list, div, pos + 1)
      if not res:
        return False
      div = div * p_list[pos]
      if div > sq_num:
        break
    return True

  def try_n(self, num):
    if num + 1 not in self._primes_d:
      return False
    facts = self.factors(num)
    # print 'Factors of %s: %s' % (num, facts)
    sq_num = int(math.sqrt(num))
    return self.rec_try(num, sq_num, facts, facts.keys(), 1, 0)

  def main(self, maax=100000000):
    total = 0
    for prime in all_primes.PRIMES:
      if prime > maax:
        break
      res = self.try_n(prime - 1)
      if res:
        print 'Number %s works' % (prime - 1)
        total += prime - 1
    print 'Total = %s' % total


if __name__ == '__main__':
  p = P357().main()

