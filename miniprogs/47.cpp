#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<int> enteros;

int isPrime(enteros & primos, int n) {
    int hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}    

int distintos(enteros & primos, int val) {
    int queda = val;
    int distintos = 0;
    enteros::iterator it = primos.begin();
    while(queda > 1) {
//        cout << "entrando while1" << endl;
        while((queda % (*it)) != 0) {
            it++;
        }
        distintos++;
//        cout << "entrando while2" << endl;
        while((queda % (*it)) == 0) {
            queda /= (*it);
        }
    }
    cout << distintos << " <- " << val << endl;
    return distintos;
}

int main() {
    enteros primos;
    for(int j = 3; j < 250000; j += 2) {
        isPrime(primos, j);
    }
    primos.insert(2);
    
    for(int i = 1; i <= 200000; i++) {
        if(distintos(primos, i) == 4) {
            if(distintos(primos, i + 1) == 4 &&
            distintos(primos, i + 2) == 4 &&
            distintos(primos, i + 3) == 4) {
                cout << i << endl;
                break;
            }
        }
    }
	return 0;
}
