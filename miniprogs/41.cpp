#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

int *ocupado;
int *numero;
int maxa;
int maximo;

void recur(int nivel) {
    if(nivel == maxa) {
        int valor = 0;
        for(int i = 0; i < maxa; i++)
            valor += ((int)pow(10., (double)i))*numero[i];
        if(valor % 2 == 0)
            return;
        int hasta = (int)sqrt(valor);
        for(int i = 3; i <= hasta; i += 2)
            if(valor % i == 0)
                return;
        if(valor > maximo)
            maximo = valor;
        std::cout << valor << std::endl;
        return;
    }
    for(int i = 1; i <= maxa; i++) {
        if(!ocupado[i]) {
            ocupado[i] = 1;
            numero[nivel] = i;
            recur(nivel + 1);
            ocupado[i] = 0;
        }
    }
}

int main() {
    ocupado = new int[10];
    numero = new int[10];
    maximo = 0;
    for(int n = 4; n <= 9; n++) {
        for(int i = 1; i <= n; i++)
            ocupado[i] = 0;
        maxa = n;
        recur(0);
    }
    std::cout << "Maximo: " << maximo << std::endl;
	return 0;
}
