"""Problem 346."""

class P346(object):
  """Problem 346."""

  def __init__(self, max_pot):
    self._last_number = 1
    self._sum = 1
    self._covered = set()
    self._max_num = 10 ** max_pot

  def iterate(self):
    number = self._last_number + 1
    pot = number * number
    new_num = number + 1
    while True:
      new_num += pot
      if new_num >= self._max_num:
        break
      if new_num not in self._covered:
        self._sum += new_num
        self._covered.add(new_num)
        # print 'Adding ' + str(new_num)
      pot *= number
    self._last_number = number        

  def print_last(self):
    #print 'last number: ' + str(self._last_number)
    #print 'assigned: ' + str(self._two)
    print 'sum: ' + str(self._sum)

  def run(self):
    while self._last_number < self._max_num:
      self.iterate()
      if self._last_number % 100000 == 0:
        self.print_last()
    print 'Sum: ' + str(self._sum)


if __name__ == '__main__':
  P346(12).run()

