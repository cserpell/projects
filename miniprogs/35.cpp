#include <iostream>
#include <sstream>
#include <cmath>

int isPrime(int *tabla, int n) {
    int hasta = ((int)sqrt(n)) + 1;
    for(int i = 3; i <= hasta; i += 2) {
        if(tabla[i] && (n % i) == 0) {
            tabla[n] = 0;
            return 0;
        }
    }
    tabla[n] = 1;
    return 1;
}

int isCircular(int *tabla, int n) {
    if(!tabla[n])
        return 0;
    int d1, d2, d3, d4, d5, d6;
    
    d1 = n/100000;
    d2 = (n % 100000)/10000;
    d3 = (n % 10000)/1000;
    d4 = (n % 1000)/100;
    d5 = (n % 100)/10;
    d6 = n % 10;
    if(d1 == 0) {
        if(d2 == 0) {
            if(d3 == 0) {
                if(d4 == 0) {
                    if(d5 == 0) {
                        return 1;
                    }
                    if(!tabla[10*d6 + d5])
                        return 0;
                    return 1;
                }
                if(!tabla[100*d5 + 10*d6 + d4])
                    return 0;
                if(!tabla[100*d6 + 10*d4 + d5])
                    return 0;
                return 1;
            }
            if(!tabla[1000*d4 + 100*d5 + 10*d6 + d3])
                return 0;
            if(!tabla[1000*d5 + 100*d6 + 10*d3 + d4])
                return 0;
            if(!tabla[1000*d6 + 100*d3 + 10*d4 + d5])
                return 0;
            return 1;
        }
        if(!tabla[10000*d3 + 1000*d4 + 100*d5 + 10*d6 + d2])
            return 0;
        if(!tabla[10000*d4 + 1000*d5 + 100*d6 + 10*d2 + d3])
            return 0;
        if(!tabla[10000*d5 + 1000*d6 + 100*d2 + 10*d3 + d4])
            return 0;
        if(!tabla[10000*d6 + 1000*d2 + 100*d3 + 10*d4 + d5])
            return 0;
        return 1;
    }
    if(!tabla[100000*d2 + 10000*d3 + 1000*d4 + 100*d5 + 10*d6 + d1])
        return 0;
    if(!tabla[100000*d3 + 10000*d4 + 1000*d5 + 100*d6 + 10*d1 + d2])
        return 0;
    if(!tabla[100000*d4 + 10000*d5 + 1000*d6 + 100*d1 + 10*d2 + d3])
        return 0;
    if(!tabla[100000*d5 + 10000*d6 + 1000*d1 + 100*d2 + 10*d3 + d4])
        return 0;
    if(!tabla[100000*d6 + 10000*d1 + 1000*d2 + 100*d3 + 10*d4 + d5])
        return 0;
    return 1;
}

int main() {
    int *tabla;
    
    tabla = new int[1000000];
    for(int i = 3; i < 1000000; i += 2) {
        int r = isPrime(tabla, i);
//        std::cout << i << " es " << r << std::endl;
    }
    int cant = 0;
    for(int i = 3; i < 1000000; i += 2)
        cant += isCircular(tabla, i);
    std::cout << (cant + 1) << std::endl;
	return 0;
}
