#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long cantulen;

int dig[10];

unsigned long long pot[11];

void cant(int pos, int max) {
    if(pos == max) {
        unsigned long long numsuma = 0;
        for(int i = 0; i < max; i++) {
            numsuma += pot[i]*dig[i];
            numsuma += pot[max - i - 1]*dig[i];
        }
        int d;
        for(int i = 1; i <= max; i++) {
            d = (numsuma % pot[i])/pot[i - 1];
            if((d % 2) == 0)
                return;
        }
        cantulen++;
/*        for(int i = 0; i < max; i++)
            cout << dig[i];
        cout << " -> " << numsuma << endl;
  */      return;
    }
    int ini = 0;
    if(pos == 0 || pos == max - 1)
        ini = 1;
    for(int i = ini; i < 10; i++) {
        dig[pos] = i;
        cant(pos + 1, max);
    }
}

int main() {
    pot[0] = 1;
    for(int i = 1; i < 11; i++)
        pot[i] = pot[i - 1]*10;
    unsigned long long sumat = 0;
    for(int i = 1; i < 10; i++) {
        cantulen = 0;
        cant(0, i);
        cout << " * " << i << " : " << cantulen << endl;
        sumat += cantulen;
        cout << " ** suma total = " << sumat << endl;
    }
    return 0;
}
