#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;
enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

unsigned long long le1[6];
unsigned long long le2[6];
unsigned long long le3[6];

void calcular_sig_nivel(unsigned long long niveldel2) {
    for(int i = 0; i < 6; i++) {
        le1[i] = le2[i];
        le2[i] = le3[i];
    }
    le3[0] = le3[5] + niveldel2 + 1;
    for(int i = 1; i < 6; i++)
        le3[i] = le3[i - 1] + niveldel2 + 2;
}

int calc_dif_e(unsigned long long val, int easoc, unsigned long long nivel) {
    unsigned long long dif;
    int cant = 0;
    dif = val - le1[easoc];
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    dif = le3[easoc] - val;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    dif = (le3[easoc] + 1) - val;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    if(easoc != 0)
        dif = (le3[easoc] - 1) - val;
    else
        dif = (le3[5] + nivel) - val;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    if(easoc == 0) {
        dif = (le2[5] + nivel - 1) - val;
//        std::cout << dif << " ";
        if(primos.find(dif) != primos.end())
            cant++;
    }
    return cant;
}

int calc_dif_no_e(unsigned long long val, int easoc, unsigned long long nivel) {
    unsigned long long dif;
    unsigned long long dif2 = val - le2[easoc];
    int cant = 0;
    dif = val - (le1[easoc] + dif2 - 1);
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    if(nivel > 2)
        dif = val - (le1[easoc] + dif2);
    else
        dif = val - le1[(easoc + 1)%6];
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    dif = (le3[easoc] + dif2) - val;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    dif = (le3[easoc] + dif2 + 1) - val;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    if(easoc == 5 && dif2 == nivel - 1) {
        dif = val - le2[0];
//        std::cout << dif << " ";
        if(primos.find(dif) != primos.end())
            cant++;
    }
    return cant;
}

int calc2_dif_e(unsigned long long val, int easoc, unsigned long long nivel) {
    unsigned long long dif;
    int cant = 0;
    dif = 6*nivel - 6 + easoc;
  //  std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    if(easoc == 0) {
        dif = 6*nivel - 1;
//        std::cout << dif << " ";
        if(primos.find(dif) != primos.end())
            cant++;
    }
    dif = 6*nivel + easoc;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    dif = 6*nivel + easoc + 1;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    if(easoc != 0)
        dif = 6*nivel + easoc - 1;
    else
        dif = 12*nivel + 5;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
//    std::cout << std::endl;
    return cant;
}

int calc2_dif_no_e(unsigned long long val, int easoc, unsigned long long nivel) {
    unsigned long long dif;
    unsigned long long dif2 = val /*- le2[easoc]*/;
    int cant = 0;
    if(easoc == 5 && dif2 == nivel - 1) {
        dif = 6*nivel - 1;
//        std::cout << dif << " ";
        if(primos.find(dif) != primos.end())
            cant++;
    }
    dif = 6*nivel - 5 + easoc;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    dif = 6*nivel + easoc;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    dif = 6*nivel + easoc + 1;
//    std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
    if(easoc == 5 && dif2 == nivel - 1)
        dif = 12*nivel - 7;
    else
        dif = 6*nivel - 6 + easoc;
 //   std::cout << dif << " ";
    if(primos.find(dif) != primos.end())
        cant++;
//    std::cout << std::endl;
    return cant;
}

int main() {
    for(unsigned long long i = 3; i < 1000000LL; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    for(int i = 0; i < 6; i++)
        le1[i] = i + 2;
    for(int i = 0; i < 6; i++)
        le2[i] = 8 + 2*i;
    for(int i = 0; i < 6; i++)
        le3[i] = 20 + 3*i;
    int res;
    int cant = 2;
    unsigned long long niv = 2;
/*    while(cant < 2000) {
        for(int j = 0; j < 6; j++) {
            res = calc2_dif_e(le2[j], j, niv);
            if(res == 3) {
                cant++;
                std::cout << cant << " = " << le2[j] << " : " << res << std::endl;
            }
            for(int corr = 1; corr < niv; corr++) {
                res = calc2_dif_no_e(le2[j] + corr, j, niv);
                if(res == 3) {
                    cant++;
                    std::cout << cant << " = " << (le2[j] + corr) << " : " << res << std::endl;
                }
            }
        }
        calcular_sig_nivel(niv);
        niv++;
    }*/
    while(cant < 2005) {
        res = calc2_dif_e(0, 0, niv);
        if(res == 3) {
            cant++;
            std::cout << cant << " = " << (3*niv*(niv - 1) + 2) << " : " << res << std::endl;
        }
        res = calc2_dif_no_e(niv - 1, 5, niv);
        if(res == 3) {
            cant++;
            std::cout << cant << " = " << (3*niv*(niv + 1) + 1) << " : " << res << std::endl;
        }
        niv++;
    }
    return 0;
}
