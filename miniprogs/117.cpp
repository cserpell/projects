#include <iostream>
#include <cmath>

using namespace std;

#define SIZE  55
#define SIZE1 56

unsigned long long pascal[SIZE][SIZE1];

void llenar() {
    for(int i = 0; i < SIZE; i++)
        for(int j = 0; j < SIZE1; j++)
            pascal[i][j] = 0;
    pascal[0][0] = 1;
    for(int i = 1; i < SIZE; i++) {
        for(int j = 0; j <= i; j++) {
            if(j == 0) {
                pascal[i][j] = 1;
                continue;
            }
            pascal[i][j] = pascal[i - 1][j] + pascal[i - 1][j - 1];
        }
    }
}

int main() {
    llenar();
    unsigned long long tamtotal = 50;
    unsigned long long negros;
    unsigned long long canttotal = 0;
    unsigned long long rojos = 0;
    unsigned long long verdes = 0;
    unsigned long long azules = 0;
    unsigned long long num;
    while(rojos*2 <= tamtotal) {
        verdes = 0;
        while(verdes*3 + rojos*2 <= tamtotal) {
            azules = 0;
            while(azules*4 + verdes*3 + rojos*2 <= tamtotal) {
                negros = tamtotal - 2*rojos - 3*verdes - 4*azules;
                num = pascal[negros + rojos][rojos];
                num *= pascal[negros + rojos + verdes][verdes];
                num *= pascal[negros + rojos + verdes + azules][azules];
                canttotal += num;
                cout << "rojos " << rojos << ", verdes " << verdes << ", azules " << azules << " -> " << num << endl;
                azules++;
            }
            verdes++;
        }
        rojos++;
    }
    cout << canttotal << endl;    
	return 0;
}
