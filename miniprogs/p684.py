

def f_s(nnn):
    min_len = int(nnn / 9)
    the_pow = pow(10, min_len)
    first_dig = nnn % 9
    return the_pow * (first_dig + 1) - 1


def next_fib(f1, f2):
    return f1 + f2


def main():
    f1 = 1
    f2 = 1
    print('Check: {}'.format(sum(f_s(nnn) for nnn in range(1, 21))))
    the_bigs = [1]
    for i in range(3, 91):
        ff = next_fib(f1, f2)
        print('current fib {}: {}'.format(i, ff))
        the_bigs.append(sum(f_s(nnn) % 1000000007 for nnn in range(f2 + 1, ff + 1)) % 1000000007)
        print('thebigs: {}'.format(the_bigs[-1]))
        f1 = f2
        f2 = ff
    big_s = sum((90 - i + 1) * the_bigs[i - 2] for i in range(2, 91))
    print(big_s)


if __name__ == '__main__':
  main()

