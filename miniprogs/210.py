import math, string, sys
from math import *

radio = int(sys.argv[1])

def total_puntos(radi):
  valor = 2*radi*radi + 2*radi + 1
  return valor

def restados(radi):
  valor = 3*radi*radi/2
  return valor

def banda(radi):
  valor = radi*radi/2 + 5*radi/4 + 1
  return valor

def otros_lineas(radi):
  valor = 3*radi/4
  return valor

def en_circulo(radi):
  rprima = radi/8.0
  
  radit = rprima*sqrt(2.0)
  final = floor( radit + rprima)
  x = ceil(-radit + rprima)
  total = 0
  
  while x <= final:
    raiz = sqrt(  rprima*rprima*2  -  (x - rprima)*(x - rprima)  )
    finalyantesfloor = raiz + rprima
    finaly = floor(finalyantesfloor)
    if finaly == finalyantesfloor:
      finaly = finaly - 1
    inicioyantesfloor = -raiz + rprima
    inicioy = ceil(inicioyantesfloor)
    if inicioy == inicioyantesfloor:
      inicioy = inicioy + 1
    resta = finaly - inicioy + 1
#    print 'Para x ', x, ' desde ', inicioy, ' hasta ', finaly, ' = ', resta
    total = total + resta
    x = x + 1
  
  linea = radi/4 - 1
  total = total - linea
  return total

def por_bordes(x, rprima):
  rr = 2*rprima*rprima - x*x
  difer = sqrt( 1.0*rr )
  finaly = floor(difer)
  if finaly == difer:
    finaly = finaly - 1
  if finaly < 0:
    finaly = 0
  return 2*int(finaly) + 1

def circulo_por_bordes(radi):
  rprima = radi/8
  tope = floor (rprima*sqrt(2.0))
  x = 1
  suma = 0
  while x <= tope:
    suma = suma + por_bordes(x, rprima)
    #print str(x) + " -> " + str(por_bordes(x, rprima))
    x = x + 1
  suma = 2*suma + por_bordes(0, rprima)
  linea = radi/4 - 1
  total = suma
  total = suma - linea
  return total

resultado = total_puntos(radio) - banda(radio) - otros_lineas(radio)
print "Calculado por partes: " + str(resultado)
resultado2 = restados(radio)
print "Calculado por formula: " + str(resultado2)
#resultado3 = en_circulo(radio)
#print "Calculado en circulo: " + str(resultado3)
resultado4 = circulo_por_bordes(radio)
print "Calculado en circulo por bordes: " + str(resultado4)
print "Total: " + str(resultado + resultado4)
