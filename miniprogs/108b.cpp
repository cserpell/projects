#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int dist[10000000];

unsigned long long distintos(enteros & primos, unsigned long long val) {
    if(val > 1) {
        if(dist[val] != 0)
            return dist[val];
        unsigned long long int queda = val;
        enteros::iterator it = primos.begin();
        int cant = 0;
        while((queda % (*it)) != 0) {
            it++;
        }
        while((queda % (*it)) == 0) {
            queda /= (*it);
            cant++;
        }
        int a = distintos(primos, queda);
/*        if(a == cant + 1)
            dist[val] = a*(cant + 1) + a + 1;
        else*/ if(a > cant + 1 || queda == 1)
            dist[val] = a*(cant + 1) + a - 1;
        else
            dist[val] = a*(cant + 1) + cant;
        return dist[val];
    }
    return 1;
}
/*
unsigned long long distintos(enteros & primos, unsigned long long val) {
    unsigned long long int queda = val;
    enteros::iterator it = primos.begin();
    if(queda > 1) {
        cant = 0;
        while((queda % (*it)) != 0) {
            it++;
        }
        while((queda % (*it)) == 0) {
            queda /= (*it);
            cant++;
        }
        return distintos(primos, queda)*(cant + 1);
    }
    return 1;
}
*/
int main() {
/*    enteros primos;
    for(unsigned long long j = 3; j < 1000000; j += 2) {
        isPrime(primos, j);
    }
    primos.insert(2);
    
    for(int i = 0; i < 10000000; i++) {
        dist[i] = 0;
    }*/
    unsigned long long val = 1;
    unsigned long long lval = 1;
/*    unsigned long long cant = 1;
    enteros::iterator it = primos.begin();
    while(cant < 100) {
        lval = val;
        val *= *it;
        cant *= 2;
        it++;
    }
    cout << val << " -> " << cant << endl;
*/
lval = 1;
val = 1000000;

    unsigned long long culmax[10000];
    unsigned long long valmax[10000];
    culmax[0] = 2;
    valmax[0] = 2;
    int cuantos = 1;
    unsigned long long minval = 0;
    unsigned long long cual;
    while(1) {
        for(int j = 0; j < cuantos; j++) {
            unsigned long long i = culmax[cuantos - 1] + culmax[j];
            unsigned long long count;
        //count = distintos(primos, i);
            count = 0;
            for(unsigned long long b = 1; b <= i; b++) {
                unsigned long long x = i + b;
                unsigned long long y = (i*x)/b;
                if(i*(x + y) == x*y) {
    //            cout << i << " -> x: " << x << " y: " << y << endl;
                    count++;
                }
            }
            if(count > minval) {
                minval = count;
                valmax[cuantos] = count;
                culmax[cuantos] = i;
                cout << i << " " << count << endl;// << (2*count - 1) << endl;
                break;
            }
        }
        cuantos++;
//        cout << i << " " << count << endl;// << (2*count - 1) << endl;
    }
    cout << culmax[cuantos - 1] << " -> " << minval << endl;
	return 0;
}
