#include <iostream>
#include <cmath>
#include <set>

#define MM 10000000

using namespace std;

typedef set<int> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((unsigned long long)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int *tabla;

int phi(int n) {
/*    unsigned long long val = n;
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > val)
            break;
        if((val % (*it)) == 0) {
            val /= (*it);
            val *= ((*it) - 1);
        }
    }
    return val;*/
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((n % (*it)) == 0) {
            int val = n/(*it);
            if((val % (*it)) == 0) {
                tabla[n] = tabla[val]*(*it);
                return tabla[n];
            }
            tabla[n] = tabla[val]*((*it) - 1);
            return tabla[n];
        }
    }
    primos.insert(n);
    tabla[n] = n - 1;
    return tabla[n];
}

int dig1[10];
int dig2[10];

int pot[8] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000LL};

int esPermut(int n1, int n2) {
    int largo1 = ((int)log10(n1)) + 1;
    int largo2 = ((int)log10(n2)) + 1;
    if(largo1 != largo2)
        return 0;
    for(int i = 0; i < 10; i++) {
        dig1[i] = 0;
        dig2[i] = 0;
    }
    for(int i = 0; i < largo1; i++) {
        dig1[(n1/pot[i]) % 10]++;
        dig2[(n2/pot[i]) % 10]++;
    }
    for(int i = 0; i < 10; i++)
        if(dig1[i] != dig2[i])
            return 0;
    return 1;
}

int main() {
    tabla = new int[10000000];
    if(tabla == NULL) {
        cout << "No pudo..." << endl;
        return 0;
    }
    tabla[1] = 1;
    int i;
    double mmin = 2.;
    int cual = -1;
    for(i = 2; i < 10000000LL; i++) {
        int p = phi(i);
   //     cout << i << endl;
        double vv = ((double)i)/p;
        if(vv < mmin) {
        //    cout << "Posible minimo " << i << endl;
            if(esPermut(i, p)) {
                cout << vv << " " << i << " " << p << endl;
                mmin = vv;
                cual = i;
            }
        }
    }
    cout << "El minimo: " << mmin << endl;
	return 0;
}
