import java.util.*;
import java.io.*;
import java.math.*;
import java.lang.*;

class c17 {

static public void main(String[] args) {
  int totlen = 0;
  for(int i = 1; i <= 1000; i++) {
    String aux = Num2Word(i, false);
    totlen += aux.length();
    System.out.println(aux);
  }
  System.out.println(totlen);
}
/*
Num2Word, ouputs written number in human language format
ARGUMENTS:
-----------
Nbr: num, the number to be worded. This number is converted into an integer.
Crd: bol, flags whether output is cardinal or ordinal number. Cardinal is used when describing the order of items, like "first" or "second" place.
*/
public static String Num2Word(int num2, boolean fmt) {
//_ arguments
	if (num2 == 0) return "zero"; // if number given is 0, return and exit
	String num = (new Integer(num2)).toString(); // round value
//_ locals
	// word numbers
	String[][] wnums = new String[21][10];
	wnums[0][0] = "hundred";
	wnums[0][1] ="thousand";
	wnums[0][2] ="million";
	wnums[0][3] ="billion";
	wnums[0][4] ="trillion";
	wnums[0][5] ="zillion";
	
	wnums[1][0] = "one";
	wnums[1][1] ="first";
	wnums[1][2] ="ten";
	wnums[1][3] ="";
	wnums[1][4] ="th";
	
	wnums[2][0] ="two";
	wnums[2][1] ="second";
	wnums[2][2] ="twen";
	wnums[2][3] ="";
	wnums[2][4] ="";
	
	wnums[3][0] ="three";
	wnums[3][1] ="third";
	wnums[3][2] ="thir";
	wnums[3][3] ="";
	wnums[3][4] ="";
	
	wnums[4][0] ="four";
	wnums[4][1] ="fourth";
	wnums[4][2] ="for";
	wnums[4][3] ="";
	wnums[4][4] ="";
	
	wnums[5][0] ="five";
	wnums[5][1] ="fifth";
	wnums[5][2] ="fif";
	wnums[5][3] ="";
	wnums[5][4] ="";
	
	wnums[6][0] ="six";
	wnums[6][1] ="sixth";
	wnums[6][2] ="";
	wnums[6][3] ="";
	wnums[6][4] ="";
	
	wnums[7][0] =	"seven";
	wnums[7][1] ="seventh";
	wnums[7][2] ="";
	wnums[7][3] ="";
	wnums[7][4] ="";
	
	wnums[8][0] ="eight";
	wnums[8][1] ="eighth";
	wnums[8][2] ="eigh";
	wnums[8][3] ="";
	wnums[8][4] ="";

	wnums[9][0] ="nine";
	wnums[9][1] ="ninth";
	wnums[9][2] ="";
	wnums[9][3] ="";
	wnums[9][4] ="";
	
	wnums[10][0] ="ten";
	
	wnums[11][0] ="eleven";
	
	wnums[12][0] ="twelve";
	wnums[12][1] ="twelfth";
	
	wnums[13][0] ="thirteen";
	wnums[14][0] ="fourteen";
	wnums[15][0] ="fifteen";
	wnums[16][0] ="sixteen";
	wnums[17][0] ="seventeen";
	wnums[18][0] ="eighteen";
	wnums[19][0] ="nineteen";
	// digits outside triplets
	int dot = ((num.length() % 3) != 0) ? num.length() % 3 : 3;
	// number of triplets in number
	int sets = ((num.length() % 3) != 0) ? (num.length() + 3)/3 : (num.length()/3);
	// result string, word as number
	String rslt = "";
//_ convert every three digits
	for (int i = 0; i < sets; i++) { // for each set of triplets
		// capture set of numbers up to three digits
		String subt = num.substring((i == 0) ? 0 : dot + (i - 1) * 3,(i == 0) ? dot : (dot + i * 3));
		if (!subt.equals("")) { // if value of set is not 0...
			int hdec = (subt.length() > 2) ? Integer.parseInt(subt.substring(0,1)) : 0; // get hundreds digit
			int ddec = Integer.parseInt(subt.substring(Math.max(subt.length() - 2,0),subt.length())); // get double digits
			int odec = Integer.parseInt(subt.substring(subt.length() - 1)); // get one"s digit
			// hundreds digit
			if (hdec != 0) rslt += "" + wnums[hdec][0] + "hundred" + ((fmt && ddec == 0) ? "th" : "");
			// add double digit
			if (ddec < 20 && 9 < ddec) { // if less than 20...
				// add "and" for last set without a tens digits
				if ((0 < hdec || 1 < sets) && i + 1 == sets && 0 < ddec)// && ddec < 10)
				  rslt += "and";
				// add double digit word
				rslt += "" + ((fmt) ? ((!((wnums[ddec][1]).equals(""))) ? wnums[ddec][1] : wnums[ddec][0] + "th") : wnums[ddec][0]);
			} else { // otherwise, add words for each digit...
				// add "and" for last set without a tens digits
				if ((0 < hdec || 1 < sets) && i + 1 == sets && 0 < ddec)// && ddec < 10)
				  rslt += "and";
				// tens digit
				if (19 < ddec)
				  rslt += wnums[ddec/10][(!((wnums[ddec/10][2]).equals(""))) ? 2 : 0] + ((i + 1 == sets && odec == 0 && fmt) ? "tieth" : "ty") + ((0 < odec) ? "" : "");
				// one"s digit
				if (0 < odec) rslt += wnums[odec][(i + 1 == sets && fmt) ? 1 : 0];
			}
			// add place for set
			if (i + 1 < sets) rslt += "" + wnums[0][sets - i - 1] + ""; // if not last set, add place
		} else if (i + 1 == sets && fmt) { // otherwise, if this set is zero, is the last set of the loop, and the format (fmt) is cardinal
			rslt += "th"; // add cardinal "th"
		}
	}
//_ return result
	return rslt;
}

}
