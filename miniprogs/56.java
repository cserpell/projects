import java.util.*;
import java.io.*;
import java.math.*;

class c56 {
    static public void main(String[] args) {
        int cont = 0;
        HashMap H = new HashMap();
        for(int i = 2; i <= 100; i++) {
            for(int j = 2; j <= 100; j++) {
                String st = BigInteger.valueOf(i).pow(j).toString();
                if(!H.containsKey(st)) {
                    H.put(st, ""+cont);
                    cont++;
                }
            }
        }
        System.out.println(cont);
    }
}
