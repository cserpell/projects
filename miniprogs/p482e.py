
MAX_PER = 10000

PRIMES_SO_FAR = [2, 3]
PRIMES_SET = set(PRIMES_SO_FAR)
MAX_PRIME = 3

def _internal_is_prime(number):
  stop = int(number ** 0.5)
  for prime in PRIMES_SO_FAR:
    if prime > stop:
      break
    if number % prime == 0:
      return False
  return True


def max_prime(number):
  """ Return maximum prime that divides number, or 0 if it is prime."""
  while PRIMES_SO_FAR[-1] < number:
     next = PRIMES_SO_FAR[-1] + 2
     while True:
       if _internal_is_prime(next):
         PRIMES_SO_FAR.append(next)
         PRIMES_SET.add(next)
         break
       next += 2
  if number in PRIMES_SET:
    return 0
  stop = int(number ** 0.5)
  max_p = 0
  for prime in PRIMES_SO_FAR:
    if prime > stop:
      break
    if number % prime == 0:
      max_p = prime
      number = number / prime
      stop = int(number ** 0.5)
  return max_p


def test_sqrt(number):
  sqrt = number ** 0.5
  isqrt = int(sqrt)
  if isqrt == sqrt:
    return isqrt
  return 0


def main():
  tot = 0
  
  for p in range(3, MAX_PER + 1):
    print 'Testing perimeter %s' % p
    max_p = max_prime(p)
    if not max_p:
      continue
    for a in range(1, int(p / 3) + 1):
      for b in range(max(a, int((p - 2 * a + 1) / 2), max_p - a),
                     int((p - a) / 2) + 1):
        up = 2 * (a + b) * a * b
        if up % p:
          continue
        # Candidate
        c = p - a - b
        q = 2 * a * b - up / p
        s1 = test_sqrt(a * c - q)
        if not s1:
          continue
        s2 = test_sqrt(a * b - q)
        if not s2:
          continue
        s3 = test_sqrt(b * c - q)
        if not s3:
          continue
        print '%s %s %s -- %s %s %s' % (a, b, c, s1, s2, s3)
        tot += a + b + c + s1 + s2 + s3
  print tot


if __name__ == '__main__':
  main()

