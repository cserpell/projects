#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

enteros todos;
int token[4];
int usado[4];
int op[3];
int listatoks[4];

const int ridiculo = -10000000;

double realop(double a, double b, int o) {
            switch(o) {
                case 1:
                    return a + b;
                    break;
                case 2:
                    return a * b;
                    break;
                case 3:
                  //  if(a >= b)
                        return a - b;
                    //else
                        return ridiculo;
                    break;
                case 4:
               //     if(b != 0 && (a % b) == 0)
                    if(b != 0)
                        return a / b;
                    else
                        return ridiculo;
                    break;
                case 5:
                //    if(b >= a)
                        return b - a;
                  //  else
                        return ridiculo;
                    break;
                case 6:
                 //   if(a != 0 && (b % a) == 0)
                    if(a != 0)
                        return b / a;
                    else
                        return ridiculo;
                    break;
            }    
}

void recur(int pos) {
    if(pos == 3) {
        for(int i = 0; i < 4; i++) {
            if(!usado[i]) {
                token[3] = listatoks[i];
                break;
            }
        }
    /*    int res = token[3];
        for(int i = 2; i >= 0; i--) {
            int v2 = token[i];
            res = realop(v2, res, op[i]);
            if(res == ridiculo)
                break;
         //   cout << res << " - " << op[i] << " - " << v2 << endl;
        }
        if(res > 0) {
      //      cout << res << endl;
            todos.insert(res);
        }
        int res1 = realop(token[0], token[1], op[0]);
        if(res1 == ridiculo)
            return;
        int res2 = realop(token[2], token[3], op[2]);
        if(res2 == ridiculo)
            return;
        res = realop(res1, res2, op[1]);
        if(res > 0) {
      //      cout << res << endl;
            todos.insert(res);
        }
        res = realop(res2, res1, op[1]);
        if(res > 0) {
      //      cout << res << endl;
            todos.insert(res);
        }

        res1 = realop(token[1], token[2], op[1]);
        if(res1 == ridiculo)
            return;
        res2 = realop(token[0], res1, op[0]);
        if(res2 == ridiculo)
            return;
        res = realop(res2, token[3], op[2]);
        if(res > 0) {
      //      cout << res << endl;
            todos.insert(res);
        }
        res2 = realop(res1, token[3], op[2]);
        if(res2 == ridiculo)
            return;
        res = realop(token[0], res2, op[0]);
        if(res > 0) {
      //      cout << res << endl;
            todos.insert(res);
        }*/
        double r1, r2, r3;
        r1 = realop(token[0], token[1], op[0]);
        r2 = realop(r1, token[2], op[1]);
        r3 = realop(r2, token[3], op[2]);
        if(r1 != ridiculo && r2 != ridiculo && r3 != ridiculo && r3 > 0 && ((int)r3 == r3))
            todos.insert((int)r3);
        r1 = realop(token[1], token[2], op[1]);
        r2 = realop(token[0], r1, op[0]);
        r3 = realop(r2, token[3], op[2]);
        if(r1 != ridiculo && r2 != ridiculo && r3 != ridiculo && r3 > 0 && ((int)r3 == r3))
            todos.insert((int)r3);
        r1 = realop(token[0], token[1], op[0]);
        r2 = realop(token[2], token[3], op[2]);
        r3 = realop(r1, r2, op[1]);
        if(r1 != ridiculo && r2 != ridiculo && r3 != ridiculo && r3 > 0 && ((int)r3 == r3))
            todos.insert((int)r3);
        r1 = realop(token[1], token[2], op[1]);
        r2 = realop(r1, token[3], op[2]);
        r3 = realop(token[0], r2, op[0]);
        if(r1 != ridiculo && r2 != ridiculo && r3 != ridiculo && r3 > 0 && ((int)r3 == r3))
            todos.insert((int)r3);
        r1 = realop(token[2], token[3], op[2]);
        r2 = realop(token[1], r1, op[1]);
        r3 = realop(token[0], r2, op[0]);
        if(r1 != ridiculo && r2 != ridiculo && r3 != ridiculo && r3 > 0 && ((int)r3 == r3))
            todos.insert((int)r3);
        return;
    }
    for(int i = 0; i < 4; i++) {
        if(usado[i])
            continue;
        usado[i] = 1;
        token[pos] = listatoks[i];
        for(int j = 1; j <= 4; j++) {
            op[pos] = j;
            recur(pos + 1);
        }
        usado[i] = 0;
    }
}

int main() {
    int mmax = 0;
    int cuali = -1;
    int cualj = -1;
    int cualk = -1;
    int cuall = -1;
    for(int i = 0; i <= 9; i++)
    for(int j = i + 1; j <= 9; j++)
    for(int k = j + 1; k <= 9; k++)
    for(int l = k + 1; l <= 9; l++) {
  /*  int i = 1;
    int j = 2;
    int k = 3;
    int l = 4;
    */    todos.clear();
        listatoks[0] = i;
        listatoks[1] = j;
        listatoks[2] = k;
        listatoks[3] = l;
        usado[0] = 0;
        usado[1] = 0;
        usado[2] = 0;
        usado[3] = 0;
        recur(0);
        enteros::iterator it = todos.begin();
        int a = 1;
        while(it != todos.end() && (*it) == a) {
            a++;
            it++;
        }
        if(a - 1 > mmax) {
            mmax = a - 1;
            cuali = i;
            cualj = j;
            cualk = k;
            cuall = l;
        }
        cout << i << " " << j << " " << k << " " << l << " -> " << (a - 1) << endl;
    }
    cout << "*** " << cuali << " " << cualj << " " << cualk << " "
                 << cuall << " -> " << mmax << endl;
    return 0;
}
