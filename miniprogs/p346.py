"""Problem 346."""

class P346(object):
  """Problem 346."""

  def __init__(self, max_pot):
    self._last_number = 1
    self._one = set([1])
    self._two = set([1])
    self._sum = 1
    self._max_num = 10 ** max_pot

  def iterate(self):
    number = self._last_number + 1
    pot = number
    new_num = 1
    while True:
      new_num += pot
      if new_num >= self._max_num:
        break
      if new_num in self._one and new_num not in self._two:
        self._two.add(new_num)
        self._sum += new_num
        print 'Sum: %s New number: %s with number %s' % (self._sum, new_num, number) 
      self._one.add(new_num)
      pot *= number
    self._last_number = number        

  def print_last(self):
    pass
    #print 'last number: ' + str(self._last_number)
    #print 'assigned: ' + str(self._two)
    #print 'sum: ' + str(self._sum)

  def run(self):
    while self._last_number < self._max_num:
      self.iterate()
      self.print_last()
    print 'Sum: ' + str(self._sum)


if __name__ == '__main__':
  P346(3).run()

