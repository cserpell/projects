"""Problem 373."""
import math

# import all_primes
#Starting R = 6137 so far: 66768077
#Starting R = 6138 so far: 66774214
#Starting R = 6139 so far: 66774214

def int_test(number):
  aux = int(number)
  return bool(aux == number)


def get_triangle_rad(side_a, side_b, side_c):
  below = ((side_a + side_b + side_c) * (side_a + side_b - side_c) *
           (side_a - side_b + side_c) * (-side_a + side_b + side_c))
  sq_test = math.sqrt(below)
  isq = int(sq_test)
  if isq != sq_test:
    # It is not square
    return -1
  mult = side_a * side_b * side_c
  if mult < isq or mult % isq != 0:
    return -1
  value = mult / isq
  return value


class P373(object):

  def __init__(self, max_n=100):
    self._max_n = max_n

  def main(self):
    total = 0
    max_side = 2 * self._max_n
    for side_a in range(1, max_side + 1):
      for side_b in range(side_a, max_side + 1):
        for side_c in range(side_b, min(side_a + side_b, max_side + 1)):
          trirad = get_triangle_rad(side_a, side_b, side_c)
          if trirad == -1 or trirad > self._max_n:
            continue
          total += trirad
          print 'Found triangle: (%s, %s, %s) -- %s' % (
            side_a, side_b, side_c, trirad)
          #if side_a != side_b and side_b != side_c:
          #  total += trirad
          #  print 'Found triangle: (%s, %s, %s) -- %s. Total so far: %s' % (
          #    side_a, side_c, side_b, trirad, total)
    print 'End: %s' % total


class P373b(object):

  def __init__(self, max_n=100):
    self._max_n = max_n
    # self._primes = [p for p in all_primes.PRIMES if p <= self._max_side]

  def primes_list(self, number):
    return_list = set()
    for prime in self._primes:
      if prime > number:
        break
      while number % prime == 0:
        number = number / prime
        return_list.add(prime)
    return list(return_list)

  def main(self):
    total = 0
    for rad in range(1, self._max_n + 1):
      print 'Starting R = %s so far: %s' % (rad, total)
      limit_a = int(math.sqrt(3) * rad) + 1
      rad2 = 2 * rad
      for side_a in range(2, limit_a, 2):
        limit_b = int(math.sqrt(rad2 * rad + rad * math.sqrt(rad2 * rad2 - side_a * side_a))) + 1
        for side_b in range(side_a, limit_b, 2):
          xx = (rad2 - side_a) * (rad2 + side_a) * (rad2 + side_b) * (rad2 - side_b)
          sqx = int(math.sqrt(xx))
          if sqx * sqx != xx:
            continue
          right1 = side_a * side_b * (sqx - side_a * side_b)
          if right1 % (rad2 * rad) != 0:
            continue
          try1 = side_a * side_a + side_b * side_b + right1 / (rad2 * rad)
          sq1 = int(math.sqrt(try1))
          if sq1 * sq1 != try1:
            continue
          # sq1 is c
          total += rad
#          print 'Found triangle: (%s, %s, %s) -- %s' % (
#            side_a, side_b, sq1, rad)
    print 'End: %s' % total


if __name__ == '__main__':
  p = P373b(max_n=10000000)
  p.main()

