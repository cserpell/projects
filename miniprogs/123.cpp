#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int main() {
    enteros primos;

    for(unsigned long long i = 3; i < 5000000; i += 2) {
        isPrime(primos, i);
    }
    primos.insert(2);
    enteros::iterator it = primos.begin();
    unsigned long long n = 1;
    while(it != primos.end()) {
        if(n & 1) {
            double resto = (2*((double)(*it))*n);
            double den = ((double)(*it))*(*it);
            while(resto > den)
                resto -= den;
            cout << n << " -> " << (*it) << " : " << resto << endl;
            if(resto > 10000000000.) {
                cout << n << endl;
                break;
            }
        }
        n++;
        it++;
    }
	return 0;
}
