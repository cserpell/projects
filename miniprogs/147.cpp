#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long cantidad_total(unsigned long long n, unsigned long long m) {
    unsigned long long ret = 0;
    for(unsigned long long i = 0; i < n; i++) {
        for(unsigned long long j = 0; j < m; j++) {
            unsigned long long val = 1;
            long long dif = i - j;
            unsigned long long men = j;
            unsigned long long may = i;
            if(dif < 0) {
                dif = -dif;
                may = j;
                men = i;
            }
            if(dif == 1) {
                if((men % 2) == 0) {
                    if(men == 0)
                        val = 2;
                    else
                        val = 4;
                } else
                    val = 3;
            }
            if(dif == 0) {
                if((men % 2) == 0) {
                    if(men == 0)
                        val = 1;
                    else
                        val = 6*men;
                } else {
                    if(men == 1)
                        val = 6;
                    else
                        val = 6*men;
                }
            }
        //    std::cout << "De " << (i + 1) << "x" << (j + 1) << " = " << val << std::endl;
            ret += (n - i)*(m - j)*val;
        }
    }
    return ret;
}

int main() {
    std::cout << "1,1 : " << cantidad_total(1, 1) << std::endl;
    std::cout << "2,1 : " << cantidad_total(2, 1) << std::endl;
    std::cout << "3,1 : " << cantidad_total(3, 1) << std::endl;
    std::cout << "1,2 : " << cantidad_total(1, 2) << std::endl;
    std::cout << "2,2 : " << cantidad_total(2, 2) << std::endl;
    std::cout << "3,2 : " << cantidad_total(3, 2) << std::endl;
    std::cout << "47,43 : " << cantidad_total(47, 43) << std::endl;
    unsigned long long suma = 0;
    for(int i = 1; i <= 47; i++) {
        for(int j = 1; j <= 43; j++) {
            suma += cantidad_total(i, j);
        }
    }
    std::cout << suma << std::endl;
    return 0;
}
