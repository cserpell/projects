#include <iostream>
#include <cmath>

using namespace std;

int main() {
    unsigned long long *rep = new unsigned long long[20];
    unsigned long long base = 5;
    unsigned long long *func = new unsigned long long[base];
    unsigned long long *sumas = new unsigned long long[base];
    unsigned long long *pot = new unsigned long long[20];
    
    for(int i = 0; i < base; i++) {
        func[i] = 0;
        sumas[i] = 0;
    }
    pot[0] = 1;
    for(int i = 1; i < 20; i++) {
        rep[i] = 0;
        pot[i] = pot[i - 1]*base;
    }
    for(unsigned long long i = 1; i < 100000000; i++) {
        unsigned long long resto = i;
        unsigned long long hasta = (log(resto)/log(base)) + 1;
        for(int j = 1; j < 20; j++) {
            unsigned long long h = resto % pot[j];
            rep[j - 1] = h/pot[j - 1];
            resto -= h;
        }
        for(unsigned long long j = 0; j <= hasta; j++)
            func[rep[j]]++;
        for(unsigned long long j = 1; j < base; j++) {
            if(func[j] == i) {
                cout << i << " [" << j << "]: ";
                for(int k = hasta; k >= 0; k--)
                    cout << rep[k];
                cout << "       " << func[1] << " - " << func[2] << endl;
                sumas[j] += i;
            }
        }
    }
    cout << "SUMAS: " << endl;
    unsigned long long sumat = 0;
    for(unsigned long long i = 1; i< base; i++) {
        cout << i << " : " << sumas[i] << endl;
        sumat += sumas[i];
    }
    cout << sumat << endl;
    return 0;
}
