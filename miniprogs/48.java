import java.util.*;
import java.io.*;
import java.math.*;

class c48 {
    static public BigInteger Suma(BigInteger a, BigInteger b) {
        BigInteger aux = a.add(b);
        String c = aux.toString();
        if(c.length() > 10)
            return new BigInteger(c.substring(c.length() - 10));
        return aux;
    }
    static public BigInteger Pot(BigInteger n, int pot) {
        if(pot == 1)
            return n;
        BigInteger aux;
        String a;
        if(pot % 2 == 0) {
            aux = Pot(n, pot/2);
            aux = aux.multiply(aux);
            a = aux.toString();
            if(a.length() > 10)
                return new BigInteger(a.substring(a.length() - 10));
            return aux;
        }
        aux = Pot(n, pot/2);
        aux = aux.multiply(aux).multiply(n);
        a = aux.toString();
        if(a.length() > 10)
            return new BigInteger(a.substring(a.length() - 10));
        return aux;
    }
    static public void main(String[] args) {
        BigInteger suma = new BigInteger("" + 0);
        BigInteger fact;
        for(int i = 1; i <= 1000; i++) {
            fact = Pot(new BigInteger("" + i), i);
            suma = Suma(suma, fact);
        }
        System.out.println(suma.toString());
    }
}
