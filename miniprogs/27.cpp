#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

int isPrime(int *tabla, unsigned int n) {
    unsigned int hasta = ((int)sqrt(n)) + 1;
    for(unsigned int i = 3; i <= hasta; i += 2) {
        if(tabla[i] && (n % i) == 0) {
            tabla[n] = 0;
            return 0;
        }
    }
    tabla[n] = 1;
    return 1;
}

int main() {
    int *tabla;
    
    tabla = new int[10000000];
    for(unsigned int i = 0; i < 10000000; i ++) {
        tabla[i] = 0;
    }
    for(unsigned int i = 3; i < 10000000; i += 2) {
        isPrime(tabla, i);
    }
    tabla[2] = 1;
//    std::ofstream salida;
//    salida.open("primos");
//    for(unsigned int i = 0; i < 1000000; i++) {
  //      salida << i << " " << tabla[i] << std::endl;
   // }
//    salida.close();
 //   std::cout << "salida grabada!" << std::endl;
    //return 0;
    unsigned int maxcant = 0;
    int cuala = -1;
    int cualb = -1;
    for(int i = -999; i < 1000; i++) {
        for(int j = 2; j < 1000; j++) {
            int n = 0;
            int next = j;
            while(next > 0 && next < 10000000 && tabla[next]) {
                n++;
                next = n*n + i*n + j;
            }
            if(n > maxcant) {
                maxcant = n;
                cuala = i;
                cualb = j;
            }
        }
    }
    std::cout << maxcant << ": " << (cuala*cualb) << std::endl;
	return 0;
}
