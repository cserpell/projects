#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int usados[10];
int pos[10];
int cualpos[9];

int canttotal;

enteros nuevalista;

void recurdiv(int donde) {
    if(donde == 9) {
        // Found one!!
        canttotal++;
        for(enteros::iterator it = nuevalista.begin(); it != nuevalista.end(); it++) {
            cout << (*it) << " ";
        }
        cout << endl;
        return;
    }
    unsigned long long val = 0;
    for(int i = donde + 1; i <= 9; i++) {
        val *= 10;
        val += cualpos[i - 1];
        if(primos.find(val) != primos.end()) {
            nuevalista.insert(val);
            recurdiv(i);
            nuevalista.erase(val);
        }
    }
}

void recur(int ppos) {
    if(ppos == 9) {
        nuevalista.clear();
        recurdiv(0);
        return;
    }
    for(int i = 1; i <= 9; i++) {
        if(usados[i])
            continue;
        usados[i] = 1;
        pos[i] = ppos;
        cualpos[ppos] = i;
        recur(ppos + 1);
        usados[i] = 0;
    }
}

int main() {
    cout << "Buscando primos" << endl;
    for(unsigned long long i = 3; i < 100000000; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    cout << "Iniciando recursion" << endl;
    for(int i = 0; i < 10; i++)
        usados[i] = 0;
    canttotal = 0;
    recur(0);
    cout << canttotal << endl;
    return 0;
}
