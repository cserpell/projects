#include <iostream>
#include <cmath>
#include <set>

#define MM 1000

using namespace std;

int cantmaneras[MM][MM];

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

void calcular(int para, int hasta) {
    cout << "calculando " << para << ", " << hasta << endl;
    enteros::iterator it = primos.begin();
    int cantidadmaneras = 0;
    while((*it) <= hasta) {
        int dif = para - (*it);
        cantidadmaneras += cantmaneras[dif][*it];
        it++;
    }
    cantmaneras[para][hasta] = cantidadmaneras;
}

int main() {
    unsigned long long i;
    for(i = 3; i < 10000; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    cout << "Primos calculados" << endl;
    for(int i = 0; i < MM; i++) {
        cantmaneras[1][i] = 0;
        cantmaneras[0][i] = 1;
    }
    cout << "Base llenada" << endl;
    for(int i = 2; i < MM; i++) {
        for(int j = 0; j <= i; j++)
            calcular(i, j);
        for(int j = i + 1; j < MM; j++)
            cantmaneras[i][j] = cantmaneras[i][i];
        cout << i << ": " << cantmaneras[i][i] << endl;
        if(cantmaneras[i][i] > 5000)
            break;
    }
    cout << "FIN" << endl;
	return 0;
}
