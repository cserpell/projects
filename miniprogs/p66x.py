# import all_primes


def is_square(n):
  sqd = int(n ** 0.5)
  return bool(sqd * sqd == n)


def x(d):
  if is_square(d):
    return 0L
  cand = d
  next_rep = 10000L
  while True:
    if cand > next_rep:
#      print 'tested up to %s' % next_rep
      next_rep += 10000L
      if next_rep > 2000000000L:
        # Assuming high
        print 'Assuming high!'
        return 2000000000L
    cand1 = (cand - 1L)
    if is_square((cand1 * cand1 - 1L) / d):
      return cand1
    cand2 = (cand + 1L)
    if is_square((cand2 * cand2 - 1L) / d):
      return cand2
    cand += d


def x2(d):
  if is_square(d):
    return 0L
  base_cand = 1
  next_rep = 100000L
  while True:
    cand = base_cand * base_cand * d + 1
    if is_square(cand):
      return int(cand ** 0.5)
    base_cand += 1
    if base_cand > next_rep:
#      print 'tested up to %s' % next_rep
      next_rep += 100000L
      if next_rep > 100000000L:
        # Assuming high
        print 'Assuming high!'
        return 100000000L

def all_ds(n):
  d = 2
  maximum_x = 2L
  maximum_d = 2L
  while d <= n:
    xx = x2(d)
#    xxx = x(d)
#    if xx != xxx:
#      print 'WTF? %s %s %s' % (d, xx, xxx)
    print 'D = %s  x = %s' % (d, xx)
    if xx > maximum_x:
      maximum_x = xx
      maximum_d = d
    d += 1L
  print 'maximum d %s' % maximum_d


def main():
  all_ds(1000)


if __name__ == '__main__':
  main()

