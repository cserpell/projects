import java.util.*;
import java.lang.*;
import java.io.*;
import java.math.*;

class c128 {

static public int centro; // = 4000; // PAR!!!

static public BigInteger[][] tabla; //[2*centro][2*centro];

static public BigInteger getLeftUp(int px, int py) {
    if((px % 2) != 0) {
        return tabla[px - 1][py];
    } else
        return tabla[px - 1][py - 1];
}
static public BigInteger getLeftDown(int px, int py) {
    if((px % 2) != 0) {
        return tabla[px - 1][py + 1];
    } else
        return tabla[px - 1][py];
}
static public BigInteger getUp(int px, int py) {
    return tabla[px][py - 1];
}
static public BigInteger getDown(int px, int py) {
    return tabla[px][py + 1];
}
static public BigInteger getRightUp(int px, int py) {
    if((px % 2) != 0) {
        return tabla[px + 1][py];
    } else
        return tabla[px + 1][py - 1];
}
static public BigInteger getRightDown(int px, int py) {
    if((px % 2) != 0) {
        return tabla[px + 1][py + 1];
    } else
        return tabla[px + 1][py];
}

static public boolean siNo(BigInteger i1, BigInteger i2) {
    return (i1.subtract(i2).abs().isProbablePrime(5000));
}
static public int contadas;
static public void calcDif(int px, int py) {
    int ndifs = 0;
    if(siNo(tabla[px][py], getRightUp(px, py)))
        ndifs++;
    if(siNo(tabla[px][py], getRightDown(px, py)))
        ndifs++;
    if(siNo(tabla[px][py], getLeftUp(px, py)))
        ndifs++;
    if(siNo(tabla[px][py], getLeftDown(px, py)))
        ndifs++;
    if(siNo(tabla[px][py], getUp(px, py)))
        ndifs++;
    if(siNo(tabla[px][py], getDown(px, py)))
        ndifs++;
    if(ndifs == 3) {
        contadas++;
        System.out.println(contadas + " -> " + tabla[px][py]);
    }
}

static public BigInteger BI1;

static public void llenarTabla() {
    BI1 = BigInteger.valueOf(1);
    tabla[centro][centro] = BI1;
    BigInteger nactual = BI1;
    for(int i = 1; i < centro - 200; i++) {
        for(int j = 0; j < i; j++) {
            nactual = nactual.add(BI1);
            tabla[centro - j][centro - i + j/2] = new BigInteger(nactual.toString());
        }
        for(int j = 0; j < i; j++) {
            nactual = nactual.add(BI1);
            tabla[centro - i][centro - (i + 1)/2 + j] = new BigInteger(nactual.toString());
        }
        for(int j = 0; j < i; j++) {
            nactual = nactual.add(BI1);
            tabla[centro - i + j][centro + (i + j)/2] = new BigInteger(nactual.toString());
        }
        for(int j = 0; j < i; j++) {
            nactual = nactual.add(BI1);
            tabla[centro + j][centro + i - (j + 1)/2] = new BigInteger(nactual.toString());
        }
        for(int j = 0; j < i; j++) {
            nactual = nactual.add(BI1);
            tabla[centro + i][centro + i/2 - j] = new BigInteger(nactual.toString());
        }
        for(int j = 0; j < i; j++) {
            nactual = nactual.add(BI1);
            tabla[centro + i - j][centro - (i + j + 1)/2] = new BigInteger(nactual.toString());
        }
    }
 /*   for(int i = 0; i < 2*centro; i++) {
        for(int j = 0; j < 2*centro; j++) {
            cout << tabla[j][i] << " ";
        }
        cout << endl;
    }*/
}

static public void contarTabla() {
    contadas = 1;
    for(int i = 1; i < centro - 201; i++) {
        for(int j = 0; j < i; j++) {
            calcDif(centro - j,centro - i + j/2);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro - i,centro - (i + 1)/2 + j);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro - i + j,centro + (i + j)/2);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro + j,centro + i - (j + 1)/2);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro + i,centro + i/2 - j);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro + i - j,centro - (i + j + 1)/2);
        }
    }
}

static public void main(String[] args) {
    centro = 6000;
    tabla = new BigInteger[2*centro][2*centro];
    llenarTabla();
    contarTabla();
}
}
