"""Problem 105."""


class P105(object):
  """Problem 105."""

  def __init__(self, file_name='p105_sets.txt'):
    self._file_name = file_name
    self._sum = 0
    self._current_set = None

  def try_length_internal(self, numbers, length, minimum, selected, pos):
    # print 'Trying length %s pos %s (len = %s)' % (length, pos, len(numbers))
    if length == 0:
      # Ended filling stuff
      current_sum = sum(selected)
      if current_sum in self._current_set or current_sum <= minimum:
        # print 'It is not. pos = %s length = %s minimum = %s current_sum = %s selected = %s set = %s' % (pos, length, minimum, current_sum, selected, self._current_set)
        return False
      # print 'It is so far. pos = %s length = %s minimum = %s current_sum = %s selected = %s set = %s' % (pos, length, minimum, current_sum, selected, self._current_set)
      self._current_set.add(current_sum)
      return True  # No problem so far
    if pos + length > len(numbers):
      # print 'Cannot fit %s starting from pos %s (len = %s)' % (length, pos, len(numbers))
      return True  # No problem so far
    # Try selecting it
    selected.append(numbers[pos])
    myt = self.try_length_internal(numbers, length - 1, minimum, selected, pos + 1)
    if not myt:
      return False
    selected.pop()
    myt = self.try_length_internal(numbers, length, minimum, selected, pos + 1)
    if not myt:
      return False
    # print 'Everything ok at length %s pos %s (len = %s)' % (length, pos, len(numbers))
    return True

  def try_length(self, numbers, length, minimum):
    self._current_set = set()
    return self.try_length_internal(numbers, length, minimum, [], 0)
  
  def process_one(self, numbers):
    if not numbers:
      print 'Empty set'
      return
    minimum = 0
    for length in range(1, len(numbers) + 1):
      result = self.try_length(numbers, length, minimum)
      if not result:
        print 'Set %s is not special' % numbers
        return
      minimum = max(self._current_set)
      # print 'Max sum so far length %s is %s' % (length, minimum)
    the_sum = sum(numbers)
    print 'Set %s is special S = %s' % (numbers, the_sum)
    self._sum += the_sum

  def process_file(self):
    with open(self._file_name) as my_file:
      for line in my_file:
        numbers = line.split(',')
        self.process_one(sorted(int(num) for num in numbers))

  def main(self):
    self.process_file()
    print 'Final sum: %s' % self._sum

if __name__ == '__main__':
  P105().main()

