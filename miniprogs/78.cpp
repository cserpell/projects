#include <iostream>
#include <cmath>
#include <set>

#define MM  10000
#define MM1 10002

using namespace std;

unsigned long long cantmaneras[MM1][MM1];

void calcular(unsigned long long cuantas, unsigned long long limite) {
  //  cout << "calculando " << cuantas << ", " << limite << endl;
    unsigned long long cantidadmaneras = 0;
    unsigned long long posactual = cuantas - 1;
    while(posactual > cuantas - limite) {
        cantidadmaneras += cantmaneras[posactual][cuantas - posactual];
        posactual--;
//        cout << posactual << endl;
    }
//    cout << ".. sgte" << endl;
    cantmaneras[cuantas][limite] = cantidadmaneras + 1;
}

int main() {
    for(unsigned long long i = 1; i <= MM; i++) {
        cantmaneras[1][i] = 1;
        cantmaneras[i][1] = 1;
    }
    for(unsigned long long i = 2; i <= MM; i++) {
        for(unsigned long long j = 2; j < i; j++) {
            cantmaneras[i][j] = cantmaneras[i][j-1] + cantmaneras[i-j][j];
        }
        cantmaneras[i][i] = cantmaneras[i][i-1] + 1;
        for(unsigned long long j = i + 1; j <= MM; j++)
            cantmaneras[i][j] = cantmaneras[i][i];
        cout << i << ": " << cantmaneras[i][i] << endl;
        if(cantmaneras[i][i] % 1000000LL == 0)
            break;
    }
    cout << "FIN" << endl;
	return 0;
}
