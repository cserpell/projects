// Version sin hash

// Una lista enlazada tendria el mismo orden de tiempo de ejecucion

import java.util.*;
import java.io.*;
import java.math.*;

class c29v2 {
    static public void main(String[] args) {
        int cont = 0;
        BigInteger[] bigarray = new BigInteger[10000];
        for(int i = 2; i <= 100; i++) {
            for(int j = 2; j <= 100; j++) {
                BigInteger st = BigInteger.valueOf(i).pow(j);
                boolean found = false;
                for(int k = 0; k < cont; k++) {
                    if(bigarray[k].compareTo(st) == 0) {
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    bigarray[cont] = st;
                    cont++;
                }
            }
        }
        System.out.println(cont);
    }
}
