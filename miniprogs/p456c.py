import math


def modpot(a, b, mod):
  if b == 0:
    return 1
  if b == 1:
    return a % mod
  met = b / 2
  m2 = modpot(a, met, mod)
  if 2 * met == b:
    return (m2 * m2) % mod
  return (m2 * m2 * a) % mod


class Point(object):
  def __init__(self, x, y):
    self.x = x
    self.y = y
    self.a = math.atan2(y, x)


class P456(object):
  def __init__(self):
    self._last_n = 0
    self._c = [0L]
    self._points = []

  def bin_search(self, ang):
    start = 0
    end = len(self._points) - 1
    while end > start:
      mid = (end + start) / 2
      if self._points[mid].a > ang:
        end = mid
      else:
        start = mid + 1
    if start < len(self._points) and self._points[start].a <= ang:
        return start + 1
    return start

  def get_new_point(self):
    new_p_x = modpot(1248, self._last_n + 1, 32323) - 16161
    new_p_y = modpot(8421, self._last_n + 1, 30103) - 15051
    if new_p_x == 0 and new_p_y == 0:
      print 'ERROR!!!!'
    return Point(new_p_x, new_p_y)

  def add_point(self, point):
    new_pos = self.bin_search(point.a)
    self._points = self._points[:new_pos] + [point] + self._points[new_pos:]
    return new_pos

  def increment_n(self):
    np = self.get_new_point()
    pos = self.add_point(np)
    print 'Adding %s in pos %s: %s %s -- %s' % (self._last_n, pos, np.x, np.y, np.a)
    self._last_n += 1
    opposite_ang = np.a + math.pi
    if opposite_ang > math.pi:
      opposite_ang -= 2 * math.pi
    oangpos = self.bin_search(opposite_ang) % self._last_n
#    print oangpos
    tot = 0L
    left = (pos + 1) % self._last_n
    right = oangpos
    while left != oangpos:
#      print 'Trying triangles from %s to %s  (%s -- %s)' % (left, right, self._points[left].a, self._points[right].a)
      dif = self._points[right].a - self._points[left].a
      if dif < 0:
        dif += 2 * math.pi
      if dif >= math.pi:
        tot += (right - oangpos) % self._last_n
#        print 'Increasing %s' % ((right - oangpos) % self._last_n)
        oleft = left
        left = (left + 1) % self._last_n
      else:
        right = (right + 1) % self._last_n
    self._c.append(tot + self._c[self._last_n - 1])
    print 'C[%s] = %s' % (self._last_n, self._c[self._last_n])

  def c_n(self, n):
    while self._last_n < n:
      self.increment_n()

def side(p1, p2):  # , p3):
  #a = (p1[0] * p3[1] + p2[0] * p1[1] + p3[0] * p2[1] -
  #     p1[0] * p2[1] - p2[0] * p3[1] - p3[0] * p1[1])
  a = (p2[0] * p1[1] - p1[0] * p2[1])
  if a > 0:
    return 1
  if a < 0:
    return -1
  return 0  # In line


def test_has_origin(p1, p2, p3):
  s1 = side(p1, p2)  # , (0, 0))
  if s1 == 0:
    return False
  s2 = side(p2, p3)  # , (0, 0))
  if s2 == 0:
    return False
  if s1 != s2:
    return False
  s3 = side(p3, p1)  # , (0, 0))
  if s3 == 0:
    return False
  if s1 != s3:
    return False
  return True



def main():
  a = P456()
  for n in [8, 600, 40000]:
    a.c_n(n)
#    print '%s %s' % (n, '\n'.join('%s %s %s' % (p.x, p.y, p.a) for p in a._points))
    # print '%s' % a._p


if __name__ == '__main__':
  main()
