import all_primes

MODULO = 1000000009


def calc_factorial_as_primes2(n):
  res = {}
  for one in all_primes.PRIMES:
    if one > n:
      return res
    next = one
    this_prime = 0
    while next <= n:
      current = n / next
      this_prime += current
      next = next * one
    res[one] = this_prime
  return res


COMPUTED = {}

def potmod(a, b, m):
  if b == 0:
    return 1
  if b == 1:
    return a
  c = b / 2
  d = potmod(a, c, m)
  res = (d * d) % m
  if 2 * c != b:
    res = (res * a) % m
  return res


def s2(factors):  # as a list
  tot = 1
  for i in xrange(len(factors)):
#    print 'prime %s - exp %s' % (factors[i][0], factors[i][1])
    ntot = (potmod(factors[i][0], 2 * factors[i][1], MODULO) + 1) % MODULO
    tot = (tot * ntot) % MODULO
  return tot


def main():
  print 'factorial2(2): %s' % calc_factorial_as_primes2(2)
  print 'factorial2(3): %s' % calc_factorial_as_primes2(3)
  print 'factorial2(4): %s' % calc_factorial_as_primes2(4)
  print 'factorial2(5): %s' % calc_factorial_as_primes2(5)
  print 'factorial2(6): %s' % calc_factorial_as_primes2(6)
  print 'factorial2(7): %s' % calc_factorial_as_primes2(7)
  print 'factorial2(8): %s' % calc_factorial_as_primes2(8)
  print 'factorial2(9): %s' % calc_factorial_as_primes2(9)
  print 'factorial2(10): %s' % calc_factorial_as_primes2(10)
  print 'computing factorial of 100000000'
  fff = calc_factorial_as_primes2(100000000)
  print 'computing s...'
  print s2([(a, b) for (a, b) in calc_factorial_as_primes2(4).iteritems()])
  print s2([(a, b) for (a, b) in calc_factorial_as_primes2(5).iteritems()])
  print s2([(a, b) for (a, b) in fff.iteritems()])


if __name__ == '__main__':
  main()

