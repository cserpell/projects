#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

int mcd(int a, int b) {
    while(b != 0) {
        int c = a/b;
        int r = a - b*c;
        a = b;
        b = r;
    }
    return a;
}

int main() {
    int contador = 0;
    for(int i = 4; i <= 10000; i++) {
        int desde, hasta;
        desde = i/3 + 1;
        if(i % 2 == 0)
            hasta = i/2 - 1;
        else
            hasta = i/2;
        for(int j = desde; j <= hasta; j++) {
            if(mcd(j, i) == 1)
                contador++;
        }
    }
    std::cout << contador << std::endl;
	return 0;
}
