import java.util.*;
import java.io.*;
import java.math.*;
import java.lang.*;

class c138 {
    static BigInteger BI2;
    static BigInteger BI1;
    static BigInteger BI16;
    static BigInteger raiz(BigInteger orig) {
        BigInteger ret = orig.divide(BI2);
        BigInteger limsup = orig;
        BigInteger liminf = BI1;
        while(true) {
            BigInteger res = ret.multiply(ret);
            int rr = res.compareTo(orig);
            if(rr == 0)
                return ret;
            if(rr > 0) {
                limsup = ret;
                ret = liminf.add(ret).divide(BI2);
            } else {
                liminf = ret;
                ret = limsup.add(ret).divide(BI2);
            }
        }
    }
    static public void main(String[] args) throws IOException {
        BI2 = BigInteger.valueOf(2);
        BI1 = BigInteger.valueOf(1);
        BI16 = BigInteger.valueOf(16);
        BigInteger lastlargo2 = BigInteger.valueOf(0);
        BigInteger lastlargo1 = BigInteger.valueOf(16);
        BigInteger lastc = BigInteger.valueOf(17);
        BigInteger suma = BigInteger.valueOf(17);
        int type = 1;
        for(int i = 0; i < 11; i++) {
            BigInteger nuevo = lastc.multiply(BI16).add(lastlargo2);
            lastlargo2 = lastlargo1;
            lastlargo1 = nuevo;
            BigInteger nuevo2 = nuevo.divide(BI2);
            BigInteger nuevoadd = nuevo.add(BigInteger.valueOf(type));
            lastc =
                 raiz(nuevo2.multiply(nuevo2).add(nuevoadd.multiply(nuevoadd)));
            suma = suma.add(lastc);
            System.out.println(nuevoadd + " . " + nuevo + " = " + lastc + " .. " + suma);
            type *= -1;
        }
    }
}
