#include <iostream>
#include <cmath>
#include <set>

using namespace std;

unsigned long long tablaX[1001];
unsigned long long tablaY[1001];
int contados;
unsigned long long maxmult = 0;
int mmult;

set<int> notomar;

int main() {
    contados = 0;
    for(int i = 0; i < 1001; i++) {
        tablaX[i] = 0;
        tablaY[i] = 0;
    }
    for(int i = 1; i < 500; i++) {
        if(i*i > 1000)
            break;
        notomar.insert(i*i);
    }
    int hasta = 1000 - notomar.size();
    unsigned long long y = 0;
    while(1) {
        y++;
        unsigned long long den = y*y;
        unsigned long long num;
        unsigned long long q;
        int mult = (maxmult/den);
        if(mult == 0)
            mult = 1;
        while(mult <= 1000) {
            if(notomar.find(mult) != notomar.end()) {
                mult++;
                continue;
            }
            num = mult*den;
            num++;
            q = (unsigned long long)sqrt(num);
            if(q*q == num) {
                if(tablaX[mult] == 0 || tablaX[mult] > q) {
                    if(tablaX[mult] == 0) {
                        contados++;
                    }
                    tablaX[mult] = q;
                    if(q > maxmult) {
                        maxmult = q;
                        mmult = mult;
                        cout << y << " -> " << maxmult << " (" << mmult << ")" << endl;
                    }
                    if(contados == hasta) {
                        break;
                    }
                }
            }
            mult++;
        }
        if(contados == hasta) {
            break;
        }
    }
    std::cout << "TERMINO: " << mmult << " -> " << maxmult << endl;
    for(int i = 1; i <= 1000; i++) {
        if(notomar.find(i) != notomar.end())
            continue;
        
    }
    return 0;
}
