import java.util.*;
import java.io.*;
import java.math.*;
import java.lang.*;

class o1 {
  public int n1;
  public int n2;
  public int n3;
  public o1(int l1, int l2, int l3) {
    n1 = l1;
    n2 = l2;
    n3 = l3;
  }
  public int compareTo(o1 otro) {
    if(otro.n1 == n1) {
      if(otro.n2 == n2)
        return n3 - otro.n3;
      return n2 - otro.n2;
    }
    return n1 - otro.n1;
  }
}

class c225 {
    public BigInteger[] Fibs;
    public int nfibs;
    //public boolean[][][] arr;
    public c225(int n, int ni) {
 //     Fibs = new BigInteger[n + 1];
   //   Fibs[0] = BigInteger.valueOf(1);
   //   Fibs[1] = BigInteger.valueOf(1);
   //   Fibs[2] = BigInteger.valueOf(1);
   //   for(int i = 3; i <= n; i++)
    //    Fibs[i] = Fibs[i-1].add(Fibs[i-2]).add(Fibs[i-3]);
      nfibs = n;
//      arr = new boolean[ni][ni][ni];
    }
    
    public boolean test_num(int num) {
  //    for(int i = 1; i < num; i++)
  //    for(int j = 1; j < num; j++)
   //   for(int k = 1; k < num; k++)
   //     arr[i][j][k] = false;
   //   arr[1][1][1] = true;
//      HashSet<o1> hs = new HashSet<o1>();
  //    hs.add(new o1(1, 1, 1));
      HashSet<String> hs = new HashSet<String>();
      hs.add("1_1_1");
    //  BigInteger bnum = BigInteger.valueOf(num);
      int l1 = 1;
      int l2 = 1;
      int l3 = 1;
  //    BigInteger bl1 = BigInteger.ONE;
  //    BigInteger bl2 = BigInteger.ONE;
  //    BigInteger bl3 = BigInteger.ONE;
      for(int i = 4; i <= nfibs; ) { //i++) {
//        BigInteger nfib = bl1.add(bl2).add(bl3).mod(bnum);
        int nint = (l1 +  l2 + l3)%num;
//        int nint = Fibs[i].mod(bnum).intValue();
   //     int nint = nfib.intValue();
        if(nint == 0)
          return false;
        l1 = l2;
        l2 = l3;
        l3 = nint;
   //     bl1 = bl2;
   //     bl2 = bl3;
   //     bl3 = nfib;
//        o1 no1 = new o1(l1, l2, l3);
      String no1 = "" + l1 + "_" + l2 + "_" + l3;
//        if(arr[l1][l2][l3]) {
        if(hs.contains(no1)) {
          // Se repite !!
          return true;
        }
//        arr[l1][l2][l3] = true;
        hs.add(no1);
      }
      System.err.println("Faltan fibs para " + num + "!!!");
      System.exit(0);
      return true;
    }
    
    static public void main(String[] args) throws IOException {
            int totalF = 100000;
        if(args.length > 0)
          totalF = Integer.parseInt(args[0]);
        int totalI = 10000;
        if(args.length > 1)
          totalI = Integer.parseInt(args[1]);
        c225 cc = new c225(totalF, totalI);
        int total = 116;
        for(int i = 1911; i < totalI; i += 2) {
         if(cc.test_num(i)) {
           total++;
           System.out.println(total + ": " + i);
           if(total == 124)
             break;
         }
       }
       //System.out.println(total);
    }
}
