#include <iostream>
#include <cmath>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

int sidivide(unsigned long long numero, unsigned long long largo) {
    unsigned long long repeticiones = 1;
    unsigned long long resto = 1;
    enteros lista;
    while(resto != 0) {
        if(repeticiones > largo)
            return 0;
        while(resto < numero) {
   //         cout << "probando " << resto << endl;
            if(lista.find(resto) != lista.end()) {
                return 0;
            }
            lista.insert(resto);
            resto = resto*10 + 1;
            repeticiones++;
        }
        resto = resto % numero;
     //   cout << "llego " << resto << endl;
    }
    if((largo % repeticiones) != 0)
        return 0;
    return 1;
}

int main() {
    enteros primos;
    unsigned long long i;
    for(i = 3; i < 200000; i += 2) {
        isPrime(primos, i);
    }
    int cant = 1;
    enteros encontrados;
    unsigned long long num = 1;
//    for(int j = 1; j < 10; j++) {
    int j = 9;
    num = 1000000000;
//        num *= 10;
        for(enteros::iterator it = primos.find(76801); it != primos.end(); it++) {
            if(cant > 40)
                break;
            if(/*encontrados.find(*it) == encontrados.end() &&*/ sidivide(*it, num)) {
//                encontrados.insert(*it);
                cout << (*it) << " divide a R(10^" << j << ") -- divisor " << cant << endl;
                cant++;
            }
        }
  //  }
	return 0;
}
