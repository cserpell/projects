#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

double recalc(int base, int exp) {
    return exp*log(base);
}

int main() {
    double *tabla;
    int t1[1000];
    int t2[1000];
    
    tabla = new double[1000];
    int cual = -1;
    double max = 0;
    for(int i = 0; i < 1000; i ++) {
        string str;
        std::cin >> str;
        int a = str.find(',', 0);
        t1[i] = atoi(str.substr(0, a).c_str());
        t2[i] = atoi(str.substr(a + 1).c_str());
        tabla[i] = recalc(t1[i], t2[i]);
        if(tabla[i] > max) {
            max = tabla[i];
            cual = i;
        }
    }
    std::cout << t1[cual] << " , " << t2[cual] << " -> " << cual << std::endl;
	return 0;
}
