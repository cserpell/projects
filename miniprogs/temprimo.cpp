#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;

int isPrime(enteros & primos, unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

enteros listausados;

int verificar(unsigned long long p1, int l1, unsigned long long p2, int l2, enteros & primos) {
    unsigned long long val = p1*pot[l2] + p2;
    if(primos.find(val) != primos.end())
        return 1;
    return 0;
}

void recur(enteros & primos, int largo) {
    if(largo == 5) {
        enteros::iterator it = primos.begin();
        unsigned long long p1 = *it;
        int largo1 = (log(p1)/log(10)) + 1;
        it++;
        unsigned long long p2 = *it;
        int largo2 = (log(p1)/log(10)) + 1;
        it++;
        unsigned long long p3 = *it;
        int largo3 = (log(p1)/log(10)) + 1;
        it++;
        unsigned long long p4 = *it;
        int largo4 = (log(p1)/log(10)) + 1;
        it++;
        unsigned long long p5 = *it;
        int largo5 = (log(p1)/log(10)) + 1;
if(!        verificar(p1, largo1, p2, largo2, primos) ) return;
if(!        verificar(p2, largo2, p1, largo1, primos) ) return;
if(!        verificar(p1, largo1, p3, largo3, primos) ) return;
if(!        verificar(p3, largo3, p1, largo1, primos) ) return;;
if(!        verificar(p1, largo1, p4, largo4, primos) ) return;
if(!        verificar(p4, largo4, p1, largo1, primos) ) return;
if(!        verificar(p1, largo1, p5, largo5, primos) ) return;
if(!        verificar(p5, largo5, p1, largo1, primos) ) return;
if(!        verificar(p2, largo2, p3, largo3, primos) ) return;
if(!        verificar(p3, largo3, p2, largo2, primos) ) return;
if(!        verificar(p2, largo2, p4, largo4, primos) ) return;
if(!        verificar(p4, largo4, p2, largo2, primos) ) return;
if(!        verificar(p2, largo2, p5, largo5, primos) ) return;
if(!        verificar(p5, largo5, p2, largo2, primos) ) return;
if(!        verificar(p3, largo3, p4, largo4, primos) ) return;
if(!        verificar(p4, largo4, p3, largo3, primos) ) return;
if(!        verificar(p3, largo3, p5, largo5, primos) ) return;
if(!        verificar(p5, largo5, p3, largo3, primos) ) return;
if(!        verificar(p4, largo4, p5, largo5, primos) ) return;
if(!        verificar(p5, largo5, p4, largo4, primos) ) return;
        cout << p1 << " " << p2 << " " << p3 << " " << p4 << " " << p5 << " = " << (p1+p2+p3+p4+p5) << endl;
        return;
    }
}

int main() {
    enteros primos;

    for(unsigned long long i = 3; i < 100000; i += 2) {
        isPrime(primos, i);
    }
    primos.insert(2);
    for(enteros::iterator it = primos.begin(); it != primos.end(); it++) {
        cout << (*it) << endl;
    }
    return 0;
}
