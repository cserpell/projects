"""Problem 106."""
EMPTY = 0
B = 1
C = 2


class P106(object):
  """Problem 106."""

  def __init__(self, last_number=7):
    self._last_number = last_number
    self._number = 0
    self._equal_checks = 0

  def test_parity(self, sequence):
    parity = 0
    for pos in range(self._number):
      if sequence[pos] == B:
        parity += 1
      elif sequence[pos] == C:
        parity -= 1
        if parity < 0:
          return False
    return True

  def build_sequence(self, sequence, pos, num_b, num_c, sum_b, sum_c):
    if pos >= self._number:
      # End sequence
      if num_b > num_c and sum_b <= sum_c:
        return False
      elif num_b < num_c and sum_c <= sum_b:
        return False
      elif num_b == num_c:
        if not self.test_parity(sequence):
          self._equal_checks += 1
        if sum_b == sum_c:
          return False
      return True
    sequence[pos] = B
    self.build_sequence(sequence, pos + 1, num_b + 1, num_c, sum_b + 1, sum_c)
    if num_b > 0:
      sequence[pos] = C
      self.build_sequence(sequence, pos + 1, num_b, num_c + 1, sum_b + 1, sum_c + 1)
    sequence[pos] = EMPTY
    self.build_sequence(sequence, pos + 1, num_b, num_c, sum_b, sum_c)

  def make_number(self, number):
    self._number = number
    self._equal_checks = 0
    sequence = [EMPTY] * number
    self.build_sequence(sequence, 0, 0, 0, 0, 0)
    print 'Equals: %s' % self._equal_checks

  def main(self):
    for number in range(1, self._last_number):
      print 'Starting length %s' % number
      self.make_number(number)


if __name__ == '__main__':
  P106(13).main()

