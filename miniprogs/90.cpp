#include <iostream>
#include <cmath>
#include <set>

using namespace std;

int tabla[2][10];
int counta;

void recur(int cual, int q, int prof) {
    if(prof == 6) {
        recur(cual + 1, 0, 0);
    }
    if(cual == 2) {
        if((!tabla[0][0] || !tabla[1][1]) && (!tabla[1][0] || !tabla[0][1]))
            return;
        if((!tabla[0][0] || !tabla[1][4]) && (!tabla[1][0] || !tabla[0][4]))
            return;
        if((!tabla[0][0] || !tabla[1][9]) && (!tabla[1][0] || !tabla[0][9]) &&
           (!tabla[0][0] || !tabla[1][6]) && (!tabla[1][0] || !tabla[0][6]))
            return;
        if((!tabla[0][1] || !tabla[1][6]) && (!tabla[1][1] || !tabla[0][6]) &&
           (!tabla[0][1] || !tabla[1][9]) && (!tabla[1][1] || !tabla[0][9]))
            return;
        if((!tabla[0][2] || !tabla[1][5]) && (!tabla[1][2] || !tabla[0][5]))
            return;
        if((!tabla[0][3] || !tabla[1][6]) && (!tabla[1][3] || !tabla[0][6]) &&
           (!tabla[0][3] || !tabla[1][9]) && (!tabla[1][3] || !tabla[0][9]))
            return;
        if((!tabla[0][4] || !tabla[1][9]) && (!tabla[1][4] || !tabla[0][9]) &&
           (!tabla[0][4] || !tabla[1][6]) && (!tabla[1][4] || !tabla[0][6]))
            return;
        if((!tabla[0][6] || !tabla[1][4]) && (!tabla[1][6] || !tabla[0][4]) &&
           (!tabla[0][9] || !tabla[1][4]) && (!tabla[1][9] || !tabla[0][4]))
            return;
        if((!tabla[0][8] || !tabla[1][1]) && (!tabla[1][8] || !tabla[0][1]))
            return;
        cout << "{";
        for(int i = 0; i < 10; i++) 
            if(tabla[0][i])
                cout << i << ",";
        cout << "},{";
        for(int i = 0; i < 10; i++) 
            if(tabla[1][i])
                cout << i << ",";
        cout << "}" << endl;
        counta++;
        return;
    }
    for(int i = q; i < 10; i++) {
        if(!tabla[cual][i]) {
            tabla[cual][i] = 1;
            recur(cual, i + 1, prof + 1);
            tabla[cual][i] = 0;
        }
    }
}

int main() {
    for(int i = 0; i < 10; i++) {
        tabla[0][i] = 0;
        tabla[1][i] = 0;
    }
    counta = 0;
    recur(0, 0, 0);
    cout << counta << endl;
    return 0;
}
