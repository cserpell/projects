"""Problem 347."""

def check_prime(number):
  if number == 2:
    return True
  if number % 2 == 0:
    return False
  i = 3
  while i * i <= number:
    if number % i == 0:
      return False
    i += 2
  return True


class P347(object):
  """Problem 347."""

  def __init__(self, max_pot):
    self._primes = []
    self._sum = 0
    self._max_num = 10 ** max_pot

  def fill_primes(self):
    print 'Filling primes...'
    self._primes.append(2)
    number = 3
    while 2 * number <= self._max_num:
      if check_prime(number):
        self._primes.append(number)
        print 'New prime: ' + str(number)
      number += 2

  def loop(self):
    for pos1 in range(len(self._primes)):
      if self._primes[pos1] * self._primes[pos1] > self._max_num:
        break
      print 'Starting prime ' + str(self._primes[pos1])
      for pos2 in range(pos1 + 1, len(self._primes)):
        current_num = self._primes[pos1]
        max_so_far = 0
        while current_num <= self._max_num:
          inner_num = current_num * self._primes[pos2]
          while inner_num <= self._max_num:
            if inner_num > max_so_far:
              max_so_far = inner_num
            inner_num *= self._primes[pos2]
          current_num *= self._primes[pos1]
        # print 'Primes %s and %s -> %s' % (self._primes[pos1], self._primes[pos2], max_so_far)
        self._sum += max_so_far

  def run(self):
    self.fill_primes()
    self.loop()
    print 'Sum: ' + str(self._sum)


if __name__ == '__main__':
  P347(7).run()

