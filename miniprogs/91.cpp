#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int NUM = 50;
    int NUM1 = NUM + 1;
    int NUM2 = NUM1*NUM1;
    int count = 0;
    for(int i = 1; i < NUM2; i++) {
        for(int j = i + 1; j < NUM2; j++) {
            int px1 = i % NUM1;
            int py1 = i / NUM1;
            int px2 = j % NUM1;
            int py2 = j / NUM1;
            int dx = px2 - px1;
            int dy = py2 - py1;
            int a = px1*px1 + py1*py1;
            int b = px2*px2 + py2*py2;
            int c = dx*dx + dy*dy;
            if(a + b == c || a + c == b || b + c == a) {
                cout << "(" << px1 << "," << py1 << ") " <<
                        "(" << px2 << "," << py2 << ")" << endl;
                count++;
            }
        }
    }
    cout << count << endl;
    return 0;
}
