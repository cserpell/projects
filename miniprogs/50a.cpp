#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

unsigned long long primos[79000];

int isPrime(int n, int pr) {
    int max = (int)sqrt(n);
    for(int i = 0; i < pr; i++) {
        if(n%primos[i] == 0)
            return 0;
        if(primos[i] > max)
            return 1;
    }
    return 1;
}

int main() {
    int numeros[1000000];
    unsigned long long maximo = 0;
    int pr = 1;
    int nm = 0;
    
    for(int i = 0; i < 1000000; i++)
        numeros[i] = 0;
    primos[0] = 2;
    numeros[2] = 1;
    for(unsigned long long i = 3; i < 1000000; i += 2) {
        if(isPrime(i, pr)) {
            primos[pr] = i;
            pr++;
            numeros[i] = 1;
        }
    }
    cout << pr << endl;
 //   return 0;
    unsigned long long tabla[79000];
    tabla[0] = primos[0];
    for(int i = 1; i < pr; i++) {
        tabla[i] = tabla[i - 1] + primos[i];
        cout << tabla[i] << endl;
    }
    for(int i = 0; i < pr; i++) {
        cout << "Desde " << i << endl;
        for(int j = i; j < pr; j++) {
            /*if(tabla[j] < 0) {
                cout << "ERROR!" << endl;
                return 0;
            }*/
            if(tabla[j] < 1000000 && numeros[tabla[j]] && (j - i) > nm) {
                maximo = tabla[j];
                nm = j - i;
            }
            tabla[j] -= primos[i];
        }
    }
    std::cout << maximo << endl;
	return 0;
}
