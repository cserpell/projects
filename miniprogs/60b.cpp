#include <iostream>
#include <cmath>
#include <set>
#include <list>

#define MAXPRIM 100000000

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

unsigned long long intpot(unsigned long long base, unsigned long long expon) {
  if(expon == 1)
    return base;
  if(expon == 0)
    return 1;
  if((expon % 2) == 0) {
    unsigned long long res = intpot(base, expon/2);
    return res*res;
  }
  unsigned long long res = intpot(base, expon/2);
  return res*res*base;
}

void llenar_sig(set<set<unsigned long long> *> *anterior, set<set<unsigned long long> *> *sgte) {
  enteros::iterator it;
  enteros::iterator it2;
  set<set<unsigned long long> *>::iterator itset;
    for(it = primos.begin(); it != primos.end(); it++) {
      unsigned long long num = *it;
      if(num*num > MAXPRIM)
        break;
      unsigned long long numlen = ((unsigned long long)log10((double)num)) + 1;
      cout << "Probando primo " << num << endl;
      for(itset = anterior->begin(); itset != anterior->end(); itset++) {
        // *itset es un set<unsigned long long> *
        set<unsigned long long> * thisset = (*itset);
        bool candidate = true;
        set<unsigned long long> * newset = new set<unsigned long long>();
        for(it2 = thisset->begin(); it2 != thisset->end(); it2++) {
          unsigned long long primo = *it2;
      if(primo*primo > MAXPRIM)
        break;
          unsigned long long primolen = ((unsigned long long)log10((double)primo)) + 1;
          unsigned long long supernum1 = primo*intpot(10, numlen) + num;
          if(supernum1 > MAXPRIM)
            candidate = false;
          if(primos.find(supernum1) == primos.end()) {
            candidate = false;
            break;
          }
          unsigned long long supernum2 = primo + num*intpot(10, primolen);
          if(supernum2 > MAXPRIM)
            candidate = false;
          if(primos.find(supernum2) == primos.end()) {
            candidate = false;
            break;
          }
          newset->insert(primo);
        }
        if(candidate) {
          // Forma un nuevo conjunto !
          newset->insert(num);
          sgte->insert(newset);
        } else {
          delete newset;
        }
      }
      
      
    }
  
}

void llenar_segundo(set<set<unsigned long long> *> *sgte) {
  enteros::iterator it;
  enteros::iterator itset;
    for(it = primos.begin(); it != primos.end(); it++) {
      unsigned long long num = *it;
      if(num*num > MAXPRIM)
        break;
      unsigned long long numlen = ((unsigned long long)log10((double)num)) + 1;
      for(itset = primos.begin(); itset != primos.end(); itset++) {
          bool candidate = true;
          unsigned long long primo = *itset;
      if(primo*primo > MAXPRIM)
        break;
          unsigned long long primolen = ((unsigned long long)(log10((double)primo))) + 1;
          unsigned long long supernum1 = primo*intpot(10, numlen) + num;
          if(primos.find(supernum1) == primos.end()) {
            candidate = false;
          }
          if(supernum1 > MAXPRIM)
            candidate = false;
     //     cout << supernum1;
          unsigned long long supernum2 = primo + num*intpot(10, primolen);
          if(supernum2 > MAXPRIM)
            candidate = false;
          if(primos.find(supernum2) == primos.end()) {
            candidate = false;
          }
    //      cout << " " << supernum2 << endl;
        if(candidate) {
          set<unsigned long long> * newset = new set<unsigned long long>();
          // Forma un nuevo conjunto !
          newset->insert(num);
          newset->insert(primo);
          sgte->insert(newset);
          cout << "Nuevo largo 2 : " << num << " , " << primo << endl;
        } 
      }
      
      
    }
  
}

int main() {
    unsigned long long i;
    for(i = 3; i <= MAXPRIM; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    cout << "Fin calculo de primos" << endl;
    set<set<unsigned long long> *> *len2 = new set<set<unsigned long long> *>();
    set<set<unsigned long long> *> *len3 = new set<set<unsigned long long> *>();
    set<set<unsigned long long> *> *len4 = new set<set<unsigned long long> *>();
    set<set<unsigned long long> *> *len5 = new set<set<unsigned long long> *>();

    llenar_segundo(len2);
    llenar_sig(len2, len3);
  set<set<unsigned long long> *>::iterator itset;
    for(itset = len2->begin(); itset != len2->end(); itset++) {
      set<unsigned long long> * cjto = *itset;
      delete cjto;
    }
    delete len2;
    
    llenar_sig(len3, len4);
    llenar_sig(len4, len5);
  enteros::iterator it2;
//  set<set<unsigned long long> *>::iterator itset;    
    for(itset = len5->begin(); itset != len5->end(); itset++) {
        // *itset es un set<unsigned long long> *
        unsigned long long suma = 0;
        set<unsigned long long> * thisset = *itset;
        for(it2 = thisset->begin(); it2 != thisset->end(); it2++) {
          cout << " , " << (*it2);
          suma += (*it2);
        }
        cout << endl;
        cout << "Suma = " << suma << endl;
     }
	return 0;
}
