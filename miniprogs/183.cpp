#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <list>

using namespace std;

typedef set<unsigned long long> enteros;

enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

unsigned long long count_divisors(unsigned long long num) {
  unsigned long long ndivs = 1;
  enteros::iterator it;
  for(it = primos.begin(); it != primos.end(); it++) {
    unsigned long long p = *it;
    if(p > num)
      break;
    unsigned long long este = 1;
    while(num % p == 0) {
      num = num/p;
      este++;
    }
    ndivs *= este;
  }
  return ndivs;
}

unsigned long long mcd(unsigned long long a, unsigned long long b) {
  if(b == 1)
    return 1;
  if(a == b)
    return b;
  unsigned long long res = a%b;
  if(res == 0)
    return b;
  return mcd(b, res);
}

int main(int argc, char *argv[]) {
    unsigned long long i;
    unsigned long long Hasta = 10000LL;
    if(argc > 1)
      Hasta = atoi(argv[1]);
    for(i = 3; i <= Hasta; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    cout << "Fin llenado de primos" << endl;
    double ee = exp(1.);
    long long sumatotal = 0;
    for(unsigned long long aa = 5; aa <= Hasta; aa++) {
      unsigned long long pos1 = aa/ee;
      unsigned long long poss = pos1;
      double val1 = pos1*log(((double)aa)/pos1);
      unsigned long long pos2 = pos1 + 1;
      double val2 = pos2*log(((double)aa)/pos2);
//      double val2 = pow((((double)aa)/pos2), pos2);
      if(val1 < val2) {
        poss = pos2;
        val1 = val2;
      }
      pos2 = pos1 - 1;
      val2 = pos2*log(((double)aa)/pos2);
//      val2 = pow((((double)aa)/pos2), pos2);
      if(val1 < val2) {
        poss = pos2;
        val1 = val2;
      }
      pos2 = pos1 + 2;
      val2 = pos2*log(((double)aa)/pos2);
//      val2 = pow((((double)aa)/pos2), pos2);
      if(val1 < val2) {
        poss = pos2;
        val1 = val2;
      }
      pos2 = pos1 - 2;
      val2 = pos2*log(((double)aa)/pos2);
//      val2 = pow((((double)aa)/pos2), pos2);
      if(val1 < val2) {
        poss = pos2;
        val1 = val2;
      }
      // Aqui pos1 vale el mejor k
      unsigned long long mm = mcd(aa, poss);
      unsigned long long kk = poss/mm;
      long long valfinal = -((long long)aa);
      for(enteros::iterator it = primos.begin(); it != primos.end(); it++) {
        if(((*it) == 2) || ((*it) == 5))
          continue;
        if((*it) > kk)
          break;
        if((kk % (*it)) == 0) {
          // Hay un divisor, es infinito
          valfinal = -valfinal;
          break;
        }
      }
      cout << aa << " -> " << poss << " => " << valfinal << "   " << kk << " (" << mm << ")" << endl;
      sumatotal += valfinal;
    }
    
   cout << "Total = " << sumatotal << endl;
	return 0;
}
