
def init_primes(up_to):
  primes = [2L]
  t = 3L
  while t < up_to:
    i = 0
    is_prime = True
    limit = long(t ** 0.5)
    while i < len(primes) and primes[i] <= limit:
      if t % primes[i] == 0:
        is_prime = False
        break
      i += 1
    if is_prime:
      primes.append(t)
    t += 2L
  return primes


class A:
  BEST = [False] * 42
  BEST_DIFF_2 = 10000000.
  BEST_DIFF = 100000000000000L
  PRIMES = []


def test(current, pos, so_far):
  if pos >= 42:
    izq = 1L
    der = 1L
    for i in range(42):
      if current[i]:
        izq *= A.PRIMES[i]
      else:
        der *= A.PRIMES[i]
    new_dif = der - izq
    if new_dif < 0L:
      return
    if new_dif < A.BEST_DIFF:
      A.BEST_DIFF = new_dif
      for i in range(42):
        A.BEST[i] = current[i]
      print 'New best %s' % A.BEST_DIFF
    return
  current[pos] = False
  test(current, pos + 1, so_far)
  if A.PRIMES[pos] * A.PRIMES[pos] * so_far < so_far + A.BEST_DIFF:
    current[pos] = True
    test(current, pos + 1, so_far * A.PRIMES[pos])


def test_2(current, pos, left, right):
  if pos >= len(current):
    upd = float(right) / left
    if upd < 1.:
      print 'Warning'
      return
    if upd < A.BEST_DIFF_2:
      A.BEST_DIFF_2 = upd
      for i in range(len(current)):
        A.BEST[i] = current[i]
      print 'New best %s -- %s' % (A.BEST_DIFF_2, left % 10000000000000000L)
    return
  current[pos] = False
  test_2(current, pos + 1, left, right)
  if A.PRIMES[pos] * A.PRIMES[pos] * left < right:
    current[pos] = True
    test_2(current, pos + 1, left * A.PRIMES[pos], right / A.PRIMES[pos])


def reset(primes):
  for i in range(42):
    A.BEST[i] = False
  A.BEST_DIFF_2 = 100000000.
  A.PRIMES = primes


def try_n(give_list):
  reset(give_list)
  left = 1L
  right = 1L
  for i in range(len(give_list)):
    right *= A.PRIMES[i]
  test_2([False] * len(give_list), 0, left, right)
  final = 1L
  for i in range(len(give_list)):
    if A.BEST[i]:
      final = (final * A.PRIMES[i]) % 10000000000000000L
  print '%s - %s' % (final, A.BEST_DIFF_2)


if __name__ == '__main__':
  try_n([2, 2, 3])
  try_n([2, 3, 11, 47])
  try_n(init_primes(190))

