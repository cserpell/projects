
MAX_PER = 10000

SQUARES_SO_FAR = [(1, 1)]

def test_sqrt(number):
  sqrt = number ** 0.5
  isqrt = int(sqrt)
  if isqrt == sqrt:
    return isqrt
  return 0


def main():
  max_l = MAX_PER / 2
  maxr = 0
  maxl = 0
  tot = 0
  for a in range(1, max_l):
    print 'Testing starting with %s -- so far %s' % (a, tot)
    for b in range(a, int((MAX_PER - a) / 2) + 1):
      max_c1 = a + b - 1
      max_c2 = MAX_PER - a - b
      max_c = min(max_c1, max_c2)
      for c in range(b, max_c + 1):
        up_q = 2 * a * b * c
        down_q = a + b + c
        if up_q % down_q:
          continue
        # Candidate
        q = up_q / down_q
        s1 = test_sqrt(a * c - q)
        if not s1:
          continue
        s2 = test_sqrt(a * b - q)
        if not s2:
          continue
        s3 = test_sqrt(b * c - q)
        if not s3:
          continue
        print '%s %s %s -- %s %s %s' % (a, b, c, s1, s2, s3)
        tot += a + b + c + s1 + s2 + s3
  print tot


if __name__ == '__main__':
  main()

