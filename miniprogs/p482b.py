
MAX_PER = 10000
DO_NOT_TEST = set()  # set of tuples (a, b, c) already tested


def test_sqrt(number):
  sqrt = number ** 0.5
  if int(sqrt) == sqrt:
    return sqrt
  return 0


def discard_from(p, a, b, c):
  k = 2
  while k * p <= MAX_PER:
    DO_NOT_TEST.add((k * a, k * b, k * c))
    k += 1


def test_triangle(a, b, c):
  p = a + b + c
  right = 2 * a * b * c
  if right % p:
    # Division is not integer, discard
    return 0
  right = right / p
  s1 = test_sqrt(a * c - right)
  if not s1:
    # Discard it and all its bigger triangles
    discard_from(p, a, b, c)
    return 0
  s2 = test_sqrt(b * a - right)
  if not s2:
    # Discard it and all its bigger triangles
    discard_from(p, a, b, c)
    return 0
  s3 = test_sqrt(c * b - right)
  if not s3:
    # Discard it and all its bigger triangles
    discard_from(p, a, b, c)
    return 0
  base = s1 + s2 + s3 + p
  tot = 0
  # Discard all bigger, adding their value
  k = 1
  while k * p <= MAX_PER:
    DO_NOT_TEST.add((k * a, k * b, k * c))
    print '%s %s %s - %s %s %s - %s' % (k * a, k * b, k * c,
                                        k * s1, k * s2, k * s3, k * base)
    tot += k * base
    k += 1
  return tot


def main():
  max_l = MAX_PER / 2
  tot = 0
  for a in range(1, max_l):
    print 'Testing starting with %s -- so far %s' % (a, tot)
    for b in range(a, max_l):
      if a + b > MAX_PER:
        break
      for c in range(b, min(a + b, MAX_PER - a - b + 1)):
        if (a, b, c) in DO_NOT_TEST:
          continue
        tot += test_triangle(a, b, c)
        #if a != b or b != c or a != c:
        #  tot += one_test
        #if a != b and b != c and a != c:
        #  tot += one_test
  print tot


if __name__ == '__main__':
  main()

