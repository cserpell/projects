import java.util.*;
import java.io.*;
import java.math.*;

class c22 {
    static public void main(String[] args) throws IOException {
        String[] lista = new String[6000];
        String linea;
        StringTokenizer t;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        BigInteger suma = new BigInteger("" + 0);
        linea = in.readLine();
        t = new StringTokenizer(linea, ",");
        int i = 0;
        while(t.hasMoreTokens()) {
            lista[i] = t.nextToken();
            i++;
        }
        for(int j = 0; j < i; j++) {
            System.out.println(lista[j]);
        }
        Arrays.sort(lista, 0, i);
        for(int j = 0; j < i; j++) {
            int val = 0;
            for(int k = 1; k < lista[j].length() - 1; k++)
                val += ((int)lista[j].charAt(k) - (int)'A') + 1;
            suma = suma.add(new BigInteger("" + (val*(j + 1))));
        }
        System.out.println(suma);
    }
}
