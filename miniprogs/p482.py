

def test_side(p, a, b, c):
#  print '%s %s %s' % (a, b, c)
  up_num = a * c * (a - b + c)
  if up_num % p:
    return 0
  new_num = up_num / p
  sqrt = new_num ** 0.5
  if int(sqrt) == sqrt:
#    print sqrt
    return sqrt
  return 0


def test_triangle(a, b, c):
  p = a + b + c
  s1 = test_side(p, a, b, c)
  if not s1:
    return 0
  s2 = test_side(p, b, c, a)
  if not s2:
    return 0
  s3 = test_side(p, c, a, b)
  if not s3:
    return 0
  return s1 + s2 + s3 + p


def main():
  limit = 10000000
  max_l = limit / 2
  tot = 0
  for a in xrange(1, max_l):
    print 'Testing starting with %s -- so far %s' % (a, tot)
    for b in xrange(a, max_l):
      if a + b > limit:
        break
      for c in xrange(b, a + b):
        if a + b + c > limit:
          break
        one_test = test_triangle(a, b, c)
        tot += one_test
        #if a != b or b != c or a != c:
        #  tot += one_test
        #if a != b and b != c and a != c:
        #  tot += one_test
  print tot


if __name__ == '__main__':
  main()

