#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int tabla[50000000];
    for(int i = 0; i < 50000000; i++)
        tabla[i] = 0;
    for(unsigned long long z = 1; z < 50000000; z++) {
        unsigned long long d;
        unsigned long long comp = 3 - (z % 3);
        unsigned long long n;
        if(comp == 3) {
            d = z/3;
            n = 0;
        } else {
            d = z/3 + 1;
            n = ((4*z + comp)*comp)/3;
        }
        for(; d < 1000000; d++) {
            //unsigned long long n = z*(2*d - z) + 3*d*d;
            if(n >= 50000000)
                break;
            tabla[(int)n]++;
            cout << z << " " << d << "  -> " << n << endl;
            n += 2*z + 6*d + 3;
        }
    }
    int count = 0;
    for(int i = 0; i < 50000000; i++) {
        if(tabla[i] == 1) {
            count++;
//            cout << "* ";
        }
  //      cout << tabla[i] << " " << i << endl;
    }
    cout << count << endl;
    return 0;
}
