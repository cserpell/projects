import java.util.*;
import java.io.*;
import java.math.*;

class c104 {
    static public BigInteger BI0;
    static public BigInteger BI1;
    static public BigInteger BI2;
    static public BigInteger BI5;
    static public BigInteger combin(int n, int k) {
        BigInteger res = BI1;
        for(int p = n; p > n - k; p--)
            res = res.multiply(BigInteger.valueOf(p));
        for(int p = 2; p <= k; p++)
            res = res.divide(BigInteger.valueOf(p));
        return res;
    }
    static public BigInteger calculate(int n) {
        BigInteger res = BI0;
        for(int p = 1; p <= n; p += 2) {
            res = res.add(combin(n, p).multiply(BI5.pow((p - 1)/2)));
        }
        res = res.divide(BI2.pow(n - 1));
        return res;
    }
    static public boolean isPandigLast(String a, int lena) {
        if(lena < 9)
            return false;
        boolean[] ocupado = new boolean[10];
        int hasta = lena - 9;
        int p;
        for(int i = lena - 1; i >= hasta; i--) {
            p = a.charAt(i) - '0';
            if(p == 0)
                return false;
            if(ocupado[p])
                return false;
            ocupado[p] = true;
        }
        return true;        
    }
    static public boolean isPandigBegin(String a, int lena) {
        if(lena < 9)
            return false;
        boolean[] ocupado = new boolean[10];
        int p;
        for(int i = 0; i < 9; i++) {
            p = a.charAt(i) - '0';
            if(p == 0)
                return false;
            if(ocupado[p])
                return false;
            ocupado[p] = true;
        }
        return true;
    }
    static public void main(String[] args) throws IOException {
        BI0 = BigInteger.valueOf(0);
        BI1 = BigInteger.valueOf(1);
        BI2 = BigInteger.valueOf(2);
        BI5 = BigInteger.valueOf(5);
        int encual = 2;
        BigInteger F1 = BigInteger.valueOf(1);
        BigInteger F2 = BigInteger.valueOf(1);
        BigInteger OF1 = BigInteger.valueOf(1);
        BigInteger OF2 = BigInteger.valueOf(1);
        int lastlen = 1;
        while(true) {
            String a = F2.toString();
            int lena = a.length();
            if(isPandigLast(a, lena)) {
             //   BigInteger RealF2 = calculate(encual);
                String aa = OF2.toString();
                System.out.println(aa);
                if(isPandigBegin(aa, aa.length())) {
                    System.out.println("FINAL " + encual + " -> " + aa);
                    break;
                }
                System.out.println(encual + " -> " + a);                
            }
            if(lena > 9) {
                F2 = new BigInteger(a.substring(lena - 9));
            }
/*                if(lastlen > 9) {
                    F2 = new BigInteger(a.substring(lena - 9));
                }*/
            BigInteger aux = F1.add(F2);
            F1 = F2;
            F2 = aux;
            aux = OF1.add(OF2);
            OF1 = OF2;
            OF2 = aux;
            lastlen = lena;
            encual++;
        }
        System.out.println(F2);
        System.out.println(encual);
    }
}
