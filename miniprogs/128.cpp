#include <iostream>
#include <cmath>
#include <set>

using namespace std;

typedef set<unsigned long long> enteros;
enteros primos;

int isPrime(unsigned long long n) {
    unsigned long long hasta = ((int)sqrt(n)) + 1;
    
    enteros::iterator it;
    for(it = primos.begin(); it != primos.end(); it++) {
        if((*it) > hasta)
            break;
        if(n % (*it) == 0)
            return 0;
    }
    primos.insert(n);
    return 1;
}

const int centro = 6000; // PAR!!!
unsigned long long tabla[2*centro][2*centro];

unsigned long long getLeftUp(int px, int py) {
    if(px % 2) {
        return tabla[px - 1][py];
    } else
        return tabla[px - 1][py - 1];
}
unsigned long long getLeftDown(int px, int py) {
    if(px % 2) {
        return tabla[px - 1][py + 1];
    } else
        return tabla[px - 1][py];
}
unsigned long long getUp(int px, int py) {
    return tabla[px][py - 1];
}
unsigned long long getDown(int px, int py) {
    return tabla[px][py + 1];
}
unsigned long long getRightUp(int px, int py) {
    if(px % 2) {
        return tabla[px + 1][py];
    } else
        return tabla[px + 1][py - 1];
}
unsigned long long getRightDown(int px, int py) {
    if(px % 2) {
        return tabla[px + 1][py + 1];
    } else
        return tabla[px + 1][py];
}
unsigned long long valAbs(unsigned long long i1, unsigned long long i2) {
    if(i1 > i2)
        return i1 - i2;
    return i2 - i1;
}
int siNo(unsigned long long i1, unsigned long long i2) {
    unsigned long long dif = valAbs(i1, i2);
    if(primos.find(dif) != primos.end())
        return 1;
    return 0;
}
int contadas;
void calcDif(int px, int py) {
    int ndifs = 0;
    ndifs += siNo(tabla[px][py], getRightUp(px, py));
    ndifs += siNo(tabla[px][py], getRightDown(px, py));
    ndifs += siNo(tabla[px][py], getLeftUp(px, py));
    ndifs += siNo(tabla[px][py], getLeftDown(px, py));
    ndifs += siNo(tabla[px][py], getUp(px, py));
    ndifs += siNo(tabla[px][py], getDown(px, py));
    if(ndifs == 3) {
        contadas++;
        std::cout << contadas << " -> " << tabla[px][py] << std::endl;
    }
}

void llenarTabla() {
    tabla[centro][centro] = 1;
    unsigned long long nactual = 1;
    for(int i = 1; i < 5800; i++) {
        for(int j = 0; j < i; j++) {
            nactual++;
            tabla[centro - j][centro - i + j/2] = nactual;
        }
        for(int j = 0; j < i; j++) {
            nactual++;
            tabla[centro - i][centro - (i + 1)/2 + j] = nactual;
        }
        for(int j = 0; j < i; j++) {
            nactual++;
            tabla[centro - i + j][centro + (i + j)/2] = nactual;
        }
        for(int j = 0; j < i; j++) {
            nactual++;
            tabla[centro + j][centro + i - (j + 1)/2] = nactual;
        }
        for(int j = 0; j < i; j++) {
            nactual++;
            tabla[centro + i][centro + i/2 - j] = nactual;
        }
        for(int j = 0; j < i; j++) {
            nactual++;
            tabla[centro + i - j][centro - (i + j + 1)/2] = nactual;
        }
    }
 /*   for(int i = 0; i < 2*centro; i++) {
        for(int j = 0; j < 2*centro; j++) {
            cout << tabla[j][i] << " ";
        }
        cout << endl;
    }*/
}

void contarTabla() {
    contadas = 1;
    for(int i = 1; i < 5799; i++) {
        for(int j = 0; j < i; j++) {
            calcDif(centro - j,centro - i + j/2);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro - i,centro - (i + 1)/2 + j);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro - i + j,centro + (i + j)/2);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro + j,centro + i - (j + 1)/2);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro + i,centro + i/2 - j);
        }
        for(int j = 0; j < i; j++) {
            calcDif(centro + i - j,centro - (i + j + 1)/2);
        }
    }
}

int main() {
    for(unsigned long long i = 3; i < 80000000LL; i += 2) {
        isPrime(i);
    }
    primos.insert(2);
    llenarTabla();
    contarTabla();
	return 0;
}
