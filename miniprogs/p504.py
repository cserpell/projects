"""Problem 504."""
GCDS = {}


def gcd(a, b):
    ttuple = (a, b)
    if ttuple not in GCDS:
        if a < b:
            val = gcd(b, a)
        else:
            r = a % b
            if r == 0:
                val = b
            else:
                val = gcd(b, r)
        GCDS[ttuple] = val
    return GCDS[ttuple]


def main(m):
    """Main execution point."""
    squares = set([4 * x * x for x in range(2 * m)])
    tot = 0
    for a in range(1, m + 1):
        for b in range(1, m + 1):
            for c in range(1, m + 1):
                for d in range(1, m + 1):
                    l1 = 2 * a * b - 2 * gcd(a, b) + 1
                    l2 = 2 * a * c - 2 * gcd(a, c) + 1
                    l3 = 2 * b * d - 2 * gcd(b, d) + 1
                    l4 = 2 * d * c - 2 * gcd(d, c) + 1
                    points = l1 + l2 + l3 + l4
                    if points in squares:
                        print(f'Valid {a} {b} {c} {d} with {points // 4}')
                        tot += 1
    print(tot)


if __name__ == '__main__':
    main(100)

