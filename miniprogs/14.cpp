#include <iostream>

int main() {
    int maximo = 0;
    unsigned int valactual;
    int largo;
    int cmax = 1;
    for(unsigned int i = 2; i < 1000000; i++) {
        valactual = i;
        largo = 1;
        while(valactual != 1) {
            largo++;
            if(valactual % 2 == 0) {
                valactual /= 2;
            } else {
                valactual = 3*valactual + 1;
            }
        }
        std::cout << i << std::endl;
        if(largo > maximo){
            maximo = largo;
            cmax = i;
        }
    }
	std::cout << cmax << std::endl;
}

