import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

class Coneccion {
    private Connection conn;
    private final String base = "DBLP";
    private final String user = "cristian";
    private final String pass = "basededatos";
    public Coneccion() throws Exception {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conn = DriverManager.getConnection("jdbc:mysql:///" + base, user, pass);
        if(conn == null || conn.isClosed()) {
            System.out.println("Error al abrir conexion a base de datos");
            System.exit(0);
        }
    }
    public Statement createStatement() throws Exception {
        return conn.createStatement();
    }
}
