import java.sql.ResultSet;
import java.sql.Statement;
import java.lang.*;
import java.util.*;

class Conferencia {
    public String codigo;
    public int anoMenor;
    public int anoMayor;
    public int numeroAutores;
    public double[] puntajeRonda;
    public int numeroPublicaciones;
    public double valorInicial;
    public double puntajeCiteseer;
    public String titulo;
//    private final int MAXRONDA = 30;
    public Conferencia(String pid, int npubs, int menor, int mayor, int nautores, double citeseer, String tit) {
        codigo = new String(pid);
        numeroPublicaciones = npubs;
        numeroAutores = nautores;
        valorInicial = 0;
        anoMenor = menor;
        anoMayor = mayor;
        puntajeCiteseer = citeseer;
        titulo = new String(tit);
//        puntajeRonda = new double[MAXRONDA];
        puntajeRonda = new double[2];
    }
}

public class LectorConferencias {
    private Hashtable identificador;
    private Conferencia[] lista;
    private int cantConf;
    private double promedioCiteseer;
    private final int MAXCONF = 3000;
    public LectorConferencias(int ainicio, int afin) throws Exception {
        Coneccion conn = new Coneccion();
        lista = new Conferencia[MAXCONF];
        identificador = new Hashtable(MAXCONF);
        Statement stmt = conn.createStatement();
        Escapador es = new Escapador();
        ResultSet lis = stmt.executeQuery("SELECT PID, CITESEER, TITLE FROM PROCEEDING");
        cantConf = 0;
        int cantConCiteseer = 0;
        while(lis.next()) {
            String pid = ((String)lis.getObject("PID")).toLowerCase();
            if(identificador.get(pid) == null) {
                stmt = conn.createStatement();
                String escapa = es.escapar(pid);
                ResultSet res = stmt.executeQuery(
                       "SELECT MIN(YEAR) AS DESDE, MAX(YEAR) AS HASTA," +
                       " COUNT(*) AS CANT FROM PUB_PROCEEDING WHERE PID = '" +
                          escapa + "' AND YEAR >= '" + ainicio +
                          "' AND YEAR <= '" + afin + "'");
                res.next();
                int npubs = ((Long)res.getObject("CANT")).intValue();
                if(npubs == 0)
                    continue;
                int nmenor = ((Integer)res.getObject("DESDE")).intValue();
                int nmayor = ((Integer)res.getObject("HASTA")).intValue();
                if(nmenor > nmayor) {
                    System.out.println(pid + " .. ERROR ANOS " + es.escapar(pid));
                    System.exit(0);
                }
                String tit = (String)lis.getObject("TITLE");
                double cs = ((Double)lis.getObject("CITESEER")).doubleValue();
                res = stmt.executeQuery("SELECT DISTINCT NAME " +
                          "FROM `AUTHOR_PROCEEDING` WHERE " +
                          "PID = '" + escapa + "' AND YEAR >= '" + ainicio +
                          "' AND YEAR <= '" + afin + "'");
                res.last();
                int nautores = res.getRow();
                lista[cantConf] = new Conferencia(pid, npubs, nmenor, nmayor, nautores, cs, tit);
                identificador.put(pid, new Integer(cantConf));
                if(cs != 0) {
                    cantConCiteseer++;
                    promedioCiteseer += cs;
                }
                cantConf++;
                if(cantConf % 500 == 0)
                    System.out.println("Agregadas " + cantConf + " conferencias");
            }
        }
        if(cantConCiteseer != 0)
            promedioCiteseer /= cantConCiteseer;
    }
    public Conferencia conferencia(int cual) {
        return lista[cual];
    }
    public Conferencia conferencia(String cual) {
        return lista[((Integer)identificador.get(cual.toLowerCase())).intValue()];
    }
    public int cantidadConferencias() {
        return cantConf;
    }
    public void iniciarRonda(int ronda) {
        for(int i = 0; i < cantConf; i++)
            lista[i].puntajeRonda[ronda] = 0;
    }
    public void normalizarRonda(int ronda) {
        double sumatot = 0;
        for(int i = 0; i < cantConf; i++)
            sumatot += lista[i].puntajeRonda[ronda];
        if(sumatot == 0)
            return;
        for(int i = 0; i < cantConf; i++)
            lista[i].puntajeRonda[ronda] /= sumatot;
    }
    public void imprimir(int desdeRonda, int hastaRonda) {
        for(int i = 0; i < cantConf; i++) {
            System.out.print("\"");
            System.out.print(lista[i].codigo);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(Double.toString(lista[i].puntajeCiteseer).replace('.', ','));
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(lista[i].numeroPublicaciones);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(lista[i].numeroAutores);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(lista[i].anoMenor);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(lista[i].anoMayor);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(Double.toString(lista[i].valorInicial).replace('.', ','));
            System.out.print("\"");
            System.out.print(";");
            for(int j = desdeRonda; j <= hastaRonda; j++) {
                System.out.print("\"");
                System.out.print(Double.toString(lista[i].puntajeRonda[j]).replace('.', ','));
                System.out.print("\"");
                if(j < hastaRonda)
                    System.out.print(";");
            }
            System.out.println("");
        }
    }
    public double maxError(int ronda) {
        double merror = 0;
        for(int i = 0; i < cantConf; i++) {
            double nerror = lista[i].puntajeRonda[ronda] - lista[i].puntajeRonda[ronda - 1];
            if(nerror < 0)
                nerror = -nerror;
            if(nerror > merror)
                merror = nerror;
        }
        return merror;
    }
    public void pasarRonda() {
        for(int i = 0; i < cantConf; i++)
            lista[i].puntajeRonda[0] = lista[i].puntajeRonda[1];
    }
    public double factorImpactoCiteseer(String pid) {
        return lista[((Integer)identificador.get(pid.toLowerCase())).intValue()].puntajeCiteseer;
    }
    public double factorCiteseerPromedio() {
        return promedioCiteseer;
    }
    public void llenarValorInicial() {
        for(int i = 0; i < cantConf; i++)
            lista[i].valorInicial = lista[i].puntajeRonda[1];
    }
    
    public void grabarBD() throws Exception {
        // Este es un metodo muy ad-hoc para llenar una tabla
        // Resumen en la BD
        Coneccion conn = new Coneccion();
        Statement stmt = conn.createStatement();
        Escapador es = new Escapador();
        for(int i = 0; i < cantConf; i++) {
            Conferencia a = lista[i];
/*            String query = "INSERT INTO RESUMEN_PROCEEDING (" +
             "PID, TITLE, NUM_AUTHORS, NUM_PUBS, MINYEAR, MAXYEAR, CITESEER, I_ISI_ALL, R_ALL) " +
             " VALUES " +
             "('" + a.codigo + "', " +
             "'" + es.escapar(a.titulo) + "', " +
             "'" + a.numeroAutores + "', " +
             "'" + a.numeroPublicaciones + "', " +
             "'" + a.anoMenor + "', " +
             "'" + a.anoMayor + "', " +
             "'" + a.puntajeCiteseer + "', " +
             "'" + a.valorInicial + "', " +
             "'" + a.puntajeRonda[1] + "')";
*/
/*            String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "I_ISI_2001 = '" + a.valorInicial + "', " +
             "R_2001 = '" +  a.puntajeRonda[1] + "' " +
             "WHERE PID = '" + a.codigo + "'";
*/
    /*        String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "I_ISI_0102 = '" + a.valorInicial + "', " +
             "R_0102 = '" +  a.puntajeRonda[1] + "' " +
             "WHERE PID = '" + a.codigo + "'";
/*
            String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "I_CJ_ALL = '" + a.valorInicial + "' " +
             "WHERE PID = '" + a.codigo + "'";
*/
/*            String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "I_CJ_2001 = '" + a.valorInicial + "' " +
             "WHERE PID = '" + a.codigo + "'";
*/
/*            String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "I_CJ_0102 = '" + a.valorInicial + "' " +
             "WHERE PID = '" + a.codigo + "'";
*//*
            String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "I_CP_ALL = '" + a.valorInicial + "' " +
             "WHERE PID = '" + a.codigo + "'";
/*
            String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "I_CP_2001 = '" + a.valorInicial + "' " +
             "WHERE PID = '" + a.codigo + "'";
*/
/*            String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "I_CP_0102 = '" + a.valorInicial + "' " +
             "WHERE PID = '" + a.codigo + "'";*/

 /*           String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "NI_ISI_ALL = '" + a.valorInicial + "', " +
             "NUM_PUBS = '" + a.numeroPublicaciones + "', " +
             "NR_ALL = '" +  a.puntajeRonda[1] + "' " +
             "WHERE PID = '" + a.codigo + "'";
*/    /*        String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "NI_ISI_2001 = '" + a.valorInicial + "', " +
             "NR_2001 = '" +  a.puntajeRonda[1] + "' " +
             "WHERE PID = '" + a.codigo + "'";
*/            String query = "UPDATE RESUMEN_PROCEEDING SET " +
             "NI_ISI_0102 = '" + a.valorInicial + "', " +
             "NR_0102 = '" +  a.puntajeRonda[1] + "' " +
             "WHERE PID = '" + a.codigo + "'";

            stmt.executeUpdate(query);
            
            if(i % 1000 == 0)
                System.out.println("Impresos " + i);
        }
    }
}
