import java.sql.ResultSet;
import java.sql.Statement;
import java.lang.*;
import java.util.*;

class Autor implements Comparable {
    // Clase que guarde en memoria todos los datos de un autor
    public String nombre;
    public double[] puntajeRonda;
    public int numeroPublicacionesConferencias;
    public int numeroPublicacionesRevistas;
    public int numeroConferencias;
    public int numeroRevistas;
    public double valorInicialISI;
    public double valorInicialCiteseerConferencias;
    public double valorInicialCiteseerRevistas;
//    private final int MAXRONDA = 30;
    public Autor(String name) {
        nombre = new String(name);
        numeroPublicacionesConferencias = 0;
        numeroPublicacionesRevistas = 0;
        numeroConferencias = 0;
        numeroRevistas = 0;
        valorInicialISI = 0;
        valorInicialCiteseerConferencias = 0;
        valorInicialCiteseerRevistas = 0;
//        puntajeRonda = new double[MAXRONDA];
        puntajeRonda = new double[2];
    }
    public int compareTo(Object otra) {
        Autor otro = (Autor)otra;
        double dif = otro.puntajeRonda[1] - puntajeRonda[1];
        if(dif < 0)
            return -1;
        if(dif > 0)
            return 1;
        return 0;
    }
}

public class LectorAutores {
    private Hashtable identificador;
    private int cantAutores;
    private Autor[] lista;
    private final int MAXAUTORES = 600000;
    public LectorAutores(int ainicio, int afin) throws Exception {
        Coneccion conn = new Coneccion();
        lista = new Autor[MAXAUTORES];
        identificador = new Hashtable(MAXAUTORES);
     //   Escapador es = new Escapador();
        Statement stmt = conn.createStatement();
//        ResultSet lis = stmt.executeQuery("SELECT NAME, COUNT(PID) AS NPUBS FROM AUTHOR_PROCEEDING WHERE YEAR >= '" + ainicio + "' AND YEAR <= '" + afin + "' GROUP BY NAME");
        ResultSet lis = stmt.executeQuery("SELECT NAME FROM AUTHOR");
        cantAutores = 0;
        while(lis.next()) {
            String name = ((String)lis.getObject("NAME")).toLowerCase();
    //        int npubs = (Integer)lis.getObject("NPUBS");
     //       System.out.println(name + " -> " + npubs);
            if(identificador.get(name) == null) {
           /*     stmt = conn.createStatement();
                ResultSet res = stmt.executeQuery(
                         "SELECT COUNT(*) AS CANT FROM AUTHOR_PROCEEDING WHERE NAME = '" +
                             es.escapar(name) + "' AND YEAR >= '" + ainicio +
                                "' AND YEAR <= '" + afin + "'");
                res.next();           
                */
                lista[cantAutores] = new Autor(name);
                identificador.put(name, new Integer(cantAutores));
                cantAutores++;
                if(cantAutores % 2000 == 0)
                    System.out.println("Agregados " + cantAutores + " autores");
            }
        }
    }
    public void aumentarNumeroPublicacionesConferencias(String name, int valor) {
        int cual = ((Integer)identificador.get(name.toLowerCase())).intValue();
        lista[cual].numeroPublicacionesConferencias += valor;
        lista[cual].numeroConferencias++;
    }
    public void aumentarNumeroPublicacionesRevistas(String name, int valor) {
        int cual = ((Integer)identificador.get(name.toLowerCase())).intValue();
        lista[cual].numeroPublicacionesRevistas += valor;
        lista[cual].numeroRevistas++;
    }
    public void vaciarNumeroPublicaciones() {
        for(int i = 0; i < cantAutores; i++) {
            lista[i].numeroPublicacionesConferencias = 0;
            lista[i].numeroPublicacionesRevistas = 0;
            lista[i].numeroConferencias = 0;
            lista[i].numeroRevistas = 0;
        }
    }
    public Autor autor(int cual) {
        return lista[cual];
    }
    public Autor autor(String cual) {
        return lista[((Integer)identificador.get(cual.toLowerCase())).intValue()];
    }
    public int cantidadAutores() {
        return cantAutores;
    }
    public void iniciarValores(boolean isi, boolean citeseerC, boolean citeseerR) {
        for(int i = 0; i < cantAutores; i++) {
            lista[i].puntajeRonda[0] = 0;
            if(isi)
                lista[i].puntajeRonda[0] += lista[i].valorInicialISI;
            if(citeseerC)
                lista[i].puntajeRonda[0] += lista[i].valorInicialCiteseerConferencias;
            if(citeseerR)
                lista[i].puntajeRonda[0] += lista[i].valorInicialCiteseerRevistas;
        }
        normalizarRonda(0);
    }
    public void iniciarRonda(int ronda) {
        for(int i = 0; i < cantAutores; i++)
            lista[i].puntajeRonda[ronda] = 0;
    }
    public void normalizarRonda(int ronda) {
        double sumatot = 0;
        for(int i = 0; i < cantAutores; i++)
            sumatot += lista[i].puntajeRonda[ronda];
        if(sumatot == 0)
            return;
        for(int i = 0; i < cantAutores; i++)
            lista[i].puntajeRonda[ronda] /= sumatot;
    }
    public void imprimir(int desdeRonda, int hastaRonda, int hastaAutor) {
        if(hastaAutor >= 0)
            Arrays.sort(lista, 0, cantAutores);
        else
            hastaAutor = cantAutores;
        if(hastaAutor > cantAutores)
            hastaAutor = cantAutores;
        for(int i = 0; i < hastaAutor; i++) {
            System.out.print("\"");
            System.out.print(lista[i].nombre);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(lista[i].numeroConferencias);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(lista[i].numeroPublicacionesConferencias);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(lista[i].numeroRevistas);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(lista[i].numeroPublicacionesRevistas);
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(Double.toString(lista[i].valorInicialISI).replace('.', ','));
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(Double.toString(lista[i].valorInicialCiteseerConferencias).replace('.', ','));
            System.out.print("\"");
            System.out.print(";");
            System.out.print("\"");
            System.out.print(Double.toString(lista[i].valorInicialCiteseerRevistas).replace('.', ','));
            System.out.print("\"");
            System.out.print(";");
            for(int j = desdeRonda; j <= hastaRonda; j++) {
                System.out.print("\"");
                System.out.print(Double.toString(lista[i].puntajeRonda[j]).replace('.', ','));
                System.out.print("\"");
                if(j < hastaRonda)
                    System.out.print(";");
            }
            System.out.println("");
        }
    }
    public double maxError(int ronda) {
        double merror = 0;
        for(int i = 0; i < cantAutores; i++) {
            double nerror = lista[i].puntajeRonda[ronda] - lista[i].puntajeRonda[ronda - 1];
            if(nerror < 0)
                nerror = -nerror;
            if(nerror > merror)
                merror = nerror;
        }
        return merror;
    }
    public void pasarRonda() {
        for(int i = 0; i < cantAutores; i++)
            lista[i].puntajeRonda[0] = lista[i].puntajeRonda[1];
    }
    
    public void grabarBD() throws Exception {
        // Este es un metodo muy ad-hoc para llenar una tabla
        // Resumen en la BD
        Coneccion conn = new Coneccion();
        Statement stmt = conn.createStatement();
        Escapador es = new Escapador();
        for(int i = 0; i < cantAutores; i++) {
            Autor a = lista[i];
/*            String query = "INSERT INTO RESUMEN_AUTHOR (" +
             "NAME, N_PROC, N_PUBPROC, N_JOURNAL, N_PUBJOURNAL" +
             ", I_ISI_ALL, I_CJ_ALL, I_CP_ALL, R_ALL)" +
             " VALUES " +
             "('" + es.escapar(a.nombre) + "', " +
             "'" + a.numeroConferencias + "', " +
             "'" + a.numeroPublicacionesConferencias + "', " +
             "'" + a.numeroRevistas + "', " +
             "'" + a.numeroPublicacionesRevistas + "', " +
             "'" + a.valorInicialISI + "', " +
             "'" + a.valorInicialCiteseerRevistas + "', " +
             "'" + a.valorInicialCiteseerConferencias + "', " +
             "'" + a.puntajeRonda[1] + "')";
*/
/*            String query = "UPDATE RESUMEN_AUTHOR SET " +
             "I_ISI_2001 = '" + a.valorInicialISI + "', " +
             "I_CJ_2001 = '" + a.valorInicialCiteseerRevistas + "', " +
             "I_CP_2001 = '" + a.valorInicialCiteseerConferencias + "', " +
             "R_2001 = '" +  a.puntajeRonda[1] + "' " +
             "WHERE NAME = '" + es.escapar(a.nombre) + "'";
*/
/*            String query = "UPDATE RESUMEN_AUTHOR SET " +
             "NI_ISI_ALL = '" + a.valorInicialISI + "', " +
             "NR_ALL = '" +  a.puntajeRonda[1] + "' " +
             "WHERE NAME = '" + es.escapar(a.nombre) + "'";
*//*            String query = "UPDATE RESUMEN_AUTHOR SET " +
             "NI_ISI_2001 = '" + a.valorInicialISI + "', " +
             "NR_2001 = '" +  a.puntajeRonda[1] + "' " +
             "WHERE NAME = '" + es.escapar(a.nombre) + "'";*/
            String query = "UPDATE RESUMEN_AUTHOR SET " +
             "NI_ISI_0102 = '" + a.valorInicialISI + "', " +
             "NR_0102 = '" +  a.puntajeRonda[1] + "' " +
             "WHERE NAME = '" + es.escapar(a.nombre) + "'";

            stmt.executeUpdate(query);
            
            if(i % 1000 == 0)
                System.out.println("Impresos " + i);
        }
    }
}
