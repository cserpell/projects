import java.sql.ResultSet;
import java.sql.Statement;
import java.lang.*;
import java.util.*;

class Par {
    public String nombre;
    public String ident;
//    public int i_nombre;
  //  public int i_ide;
    public int cantidad;
    public Par(String name, String ide) {
        nombre = new String(name);
        ident = new String(ide);
/*        i_nombre = n1;
        i_ide = n2;*/
        cantidad = 1;
    }
    public int comparar(Par otro) {
        int sc1 = nombre.compareTo(otro.nombre);
        if(sc1 == 0) {
            int sc2 = ident.compareTo(otro.ident);
            return sc2;
        }
        return sc1;
    }
    public int comparar(String nname, String nident) {
        int sc1 = nombre.compareTo(nname);
        if(sc1 == 0) {
            int sc2 = ident.compareTo(nident);
            return sc2;
        }
        return sc1;
    }
}

public class LectorRelacion {
    private Par[] lista;
    private int cantRels;
    private String identif;
    private int leyendo;
    private int encontrar(int desde, int hasta, String nname, String nident) {
        if(hasta < desde)
            return -1;
        int piv = (hasta - desde)/2 + desde;
        int r1 = lista[piv].comparar(nname, nident);
        if(r1 == 0)
            return piv;
        if(r1 > 0)
            return encontrar(desde, piv - 1, nname, nident);
        return encontrar(piv + 1, hasta, nname, nident);
    }
    public LectorRelacion(String tabla, String ident, int ainicio, int afin) throws Exception {
        Coneccion conn = new Coneccion();
        lista = new Par[1600000];
        Statement stmt = conn.createStatement();
        ResultSet Elementos = stmt.executeQuery("SELECT NAME, " + ident +
                 /*   ", COUNT(" + ident + ") AS CANT" +*/ " FROM " + tabla + 
                    " WHERE YEAR >= '" + ainicio + "' AND YEAR <= '" + afin + "'"
                     );//+ " ORDER BY NAME, " + ident);
                    //+ " GROUP BY NAME, " + ident + "");
        identif = new String(ident);
        cantRels = 0;
        while(Elementos.next()) {
            String nname = (String)Elementos.getObject("NAME");
            String nident = (String)Elementos.getObject(ident);
            int pos = encontrar(0, cantRels - 1, nname, nident);
            if(pos == -1) {
                lista[cantRels] = new Par(nname, nident);
                cantRels++;
                if(cantRels % 10000 == 0)
                    System.out.println("Agregadas " + cantRels + " relaciones");
            } else
                lista[pos].cantidad++;
        }
        inicio();
    }
    public boolean siguiente() {
        leyendo++;
        if(leyendo >= cantRels) {
            leyendo = -1;
            return false;
        }
        return true;
    }
    public void inicio() {
        leyendo = -1;
    }
    public String nombre() {
        return lista[leyendo].nombre;
    }
    public String identificador() {
        return lista[leyendo].ident;
    }
    public int peso() {
        return lista[leyendo].cantidad;
    }
}
