import java.sql.ResultSet;
import java.sql.Statement;
import java.lang.*;
import java.util.*;

public class LectorRevistas {
    private Hashtable impactFactorISI;
    private Hashtable impactFactorCiteseer;
    private Hashtable Cats;
    public double[] impactoPromedioISIcat;
    public String[] cualesCat = { "ARTIFICIAL INTELLIGENCE",
                                "THEORY & METHODS",
                                "SOFTWARE ENGINEERING",
                                "HARDWARE & ARCHITECTURE",
                                "INTERDISCIPLINARY APPLICATIONS",
                                "INFORMATION SYSTEMS",
                                "ENGINEERING, ELECTRICAL & ELECTRONIC",
                                "CYBERNETICS",
                                "TELECOMMUNICATIONS"
                              };
    // 0 = ARTIFICIAL INTELLIGENCE
    // 1 = THEORY & METHODS
    // 2 = SOFTWARE ENGINEERING
    // 3 = HARDWARE & ARCHITECTURE
    // 4 = INTERDISCIPLINARY APPLICATIONS
    // 5 = INFORMATION SYSTEMS
    // 6 = ENGINEERING, ELECTRICAL & ELECTRONIC
    // 7 = CYBERNETICS
    // 8 = TELECOMMUNICATIONS
    private double impactoPromedioISI;
    private double impactoPromedioCiteseer;
    private Hashtable numberPubs;
    public LectorRevistas(int ainicio, int afin) throws Exception {
        Coneccion conn = new Coneccion();
        impactFactorISI = new Hashtable(580);
        impactFactorCiteseer = new Hashtable(580);
        numberPubs = new Hashtable(580);
        Cats = new Hashtable(580);
        impactoPromedioISIcat = new double[cualesCat.length];
        int[] cantImpactoPromedioISIcat = new int[cualesCat.length];
        Escapador es = new Escapador();
        Statement stmt = conn.createStatement();
        ResultSet lista = stmt.executeQuery("SELECT JID, FACTOR, CITESEER, CATS FROM JOURNAL");
        int n = 0;
        int conimpactoISI = 0;
        int conimpactoCiteseer = 0;
        impactoPromedioISI = 0;
        impactoPromedioCiteseer = 0;
        for(int kk = 0; kk < cualesCat.length; kk++) {
            cantImpactoPromedioISIcat[kk] = 0;
            impactoPromedioISIcat[kk] = 0;
        }
        while(lista.next()) {
            double nfactorISI = ((Double)lista.getObject("FACTOR")).doubleValue();
            double nfactorCiteseer = ((Double)lista.getObject("CITESEER")).doubleValue();
            String jid = (String)lista.getObject("JID");
            String ccat = (String)lista.getObject("CATS");
            stmt = conn.createStatement();
            ResultSet res = stmt.executeQuery(
                     "SELECT COUNT(*) AS CANT FROM AUTHOR_JOURNAL WHERE JID = '" +
                     es.escapar(jid) + "' AND YEAR >= '" + ainicio +
                     "' AND YEAR <= '" + afin + "'");
            res.next();
            int npubs = ((Long)res.getObject("CANT")).intValue();
            impactFactorISI.put(jid, new Double(nfactorISI));
            impactFactorCiteseer.put(jid, new Double(nfactorCiteseer));
            Cats.put(jid, new String(ccat));
            numberPubs.put(jid, new Integer(npubs));
            
            for(int kk = 0; kk < cualesCat.length; kk++)
                if(ccat.indexOf(cualesCat[kk]) >= 0) {
                    cantImpactoPromedioISIcat[kk]++;
                    impactoPromedioISIcat[kk] += nfactorISI;
                }
            
            if(nfactorISI != 0) {
                impactoPromedioISI += nfactorISI;
                conimpactoISI++;
            }
            if(nfactorCiteseer != 0) {
                impactoPromedioCiteseer += nfactorCiteseer;
                conimpactoCiteseer++;
            }
            n++;
            if(n % 100 == 0)
                System.out.println("Agregadas " + n + " revistas");
        }
        for(int kk = 0; kk < cualesCat.length; kk++)
            if(cantImpactoPromedioISIcat[kk] != 0)
                impactoPromedioISIcat[kk] /= cantImpactoPromedioISIcat[kk];
        if(conimpactoISI != 0)
            impactoPromedioISI /= conimpactoISI;
        if(conimpactoCiteseer != 0)
            impactoPromedioCiteseer /= conimpactoCiteseer;
    }
    public double factorImpactoISI(String jid) {
        return ((Double)impactFactorISI.get(jid)).doubleValue();
    }
    public double factorImpactoCiteseer(String jid) {
        return ((Double)impactFactorCiteseer.get(jid)).doubleValue();
    }
    public int numeroPublicaciones(String jid) {
        return ((Integer)numberPubs.get(jid)).intValue();
    }
    public double factorISIPromedio() {
        return impactoPromedioISI;
    }
    public double factorCiteseerPromedio() {
        return impactoPromedioCiteseer;
    }
    public boolean esCategoria(String jid, String cual) {
        return (((String)Cats.get(jid)).indexOf(cual) >= 0);
    }
}
