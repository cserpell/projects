import java.util.*;
import java.lang.*;

public class AsignadorPuntajesInicialesConCategorias {
    public AsignadorPuntajesInicialesConCategorias(LectorRevistas lj, LectorAutores la, LectorConferencias lc, LectorRelacion lrC, int ainicio, int afin, double constanteNo) throws Exception {
        LectorRelacion lr = new LectorRelacion("AUTHOR_JOURNAL", "JID", ainicio, afin);
        // La cantidad de publicaciones de cada autor es llenada a mano
        la.vaciarNumeroPublicaciones();
        while(lr.siguiente())
            la.aumentarNumeroPublicacionesRevistas(lr.nombre(), lr.peso());
        lrC.inicio();
        while(lrC.siguiente())
            la.aumentarNumeroPublicacionesConferencias(lrC.nombre(), lrC.peso());
        // Recorre la relacion autor - revista llenando puntajes iniciales
        lr.inicio();
        while(lr.siguiente()) {
            String aut = lr.nombre();
            String jid = lr.identificador();
            int pesorel = lr.peso();
          //  System.out.println(aut + " <-> " + jid + " -- " + pesorel);
            double impactI = 0;
            int cantidadCategorias = 0;
            
            for(int kk = 0; kk < lj.cualesCat.length; kk++) {
                if(lj.esCategoria(jid, lj.cualesCat[kk])) {
                    impactI += lj.factorImpactoISI(jid)/lj.impactoPromedioISIcat[kk];
                    cantidadCategorias++;
                }
            }
            if(cantidadCategorias == 0)
                impactI = lj.factorImpactoISI(jid); // Impacto de la revista
            else
                impactI /= cantidadCategorias;
            
            double impactC = lj.factorImpactoCiteseer(jid); // Impacto de la revista
            // El promedio es ponderado por una constante parametro
            if(impactI == 0)
                impactI = lj.factorISIPromedio()*constanteNo;
            if(impactC == 0)
                impactC = lj.factorCiteseerPromedio()*constanteNo;
  //          int num = lj.numeroPublicaciones(jid); // Cantidad de articulos en la revista
            Autor cualautor = la.autor(aut);
//            int numaut = cualautor.numeroPublicacionesRevistas; // Cant. de articulos del autor
            cualautor.valorInicialISI = cualautor.valorInicialISI + pesorel*impactI;
            cualautor.valorInicialCiteseerRevistas = cualautor.valorInicialCiteseerRevistas + pesorel*impactC;
        }
        // Lo mismo con las conferencias y su puntaje Citeseer
        lrC.inicio();
        while(lrC.siguiente()) {
            String aut = lrC.nombre();
            String pid = lrC.identificador();
            int pesorel = lrC.peso();
          //  System.out.println(aut + " <-> " + jid + " -- " + pesorel);
            double impactC = lc.factorImpactoCiteseer(pid); // Impacto de la revista
            if(impactC == 0)
                impactC = lc.factorCiteseerPromedio()*constanteNo;
  //          int num = lj.numeroPublicaciones(jid); // Cantidad de articulos en la revista
            Autor cualautor = la.autor(aut);
//            int numaut = cualautor.numeroPublicacionesRevistas; // Cant. de articulos del autor
            cualautor.valorInicialCiteseerConferencias = cualautor.valorInicialCiteseerConferencias + pesorel*impactC;
        }
    }
}
