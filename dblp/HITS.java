import java.io.*;
import java.util.*;
import java.lang.*;

public class HITS {
    public static void main(String args[]) throws Exception {
        int ainicio = 0;
        int afin = 9999;
        int niters = 20;
        double constante = 1.;
        boolean conISI = false;
        boolean conCC = false;
        boolean conCR = false;
        if(args.length > 0) {
            String indd = args[0];
            if(indd.indexOf("I") != -1)
                conISI = true;
            if(indd.indexOf("C") != -1)
                conCC = true;
            if(indd.indexOf("R") != -1)
                conCR = true;
        }
        if(!conISI && !conCC && !conCR) {
            System.out.println("USO : HITS (ICR) [n_iters=20] [ponderacion_prom=1] [ano_inicio] [ano_fin]");
            System.out.println("I = Puntajes ISI");
            System.out.println("C = Puntajes CiteSeer Conferencias");
            System.out.println("R = Puntajes CiteSeer Revistas");
            System.exit(0);
        }
        if(args.length > 1)
            niters = Integer.parseInt(args[1]);
        if(args.length > 2)
            constante = Double.parseDouble(args[2]);
        if(args.length > 3)
            ainicio = Integer.parseInt(args[3]);
        if(args.length > 4)
            afin = Integer.parseInt(args[4]);
        // Aqui empieza todo
        System.out.println("Leyendo revistas...");
        LectorRevistas lj = new LectorRevistas(ainicio, afin);
        System.out.println("Leyendo autores...");
        LectorAutores la = new LectorAutores(ainicio, afin);
        System.out.println("Leyendo conferencias...");
        LectorConferencias lc = new LectorConferencias(ainicio, afin);
        System.out.println("Leyendo relacion autor - conferencia...");
        LectorRelacion lr = new LectorRelacion("AUTHOR_PROCEEDING", "PID", ainicio, afin);
        System.out.println("Asignando puntajes iniciales a autores...");
//        AsignadorPuntajesIniciales api = new AsignadorPuntajesIniciales(lj, la, lc, lr, ainicio, afin, constante);
        AsignadorPuntajesInicialesConCategorias api = new AsignadorPuntajesInicialesConCategorias(lj, la, lc, lr, ainicio, afin, constante);
        System.out.println("Leyendo relacion publicacion - conferencia...");
        LectorPublicacion lp = new LectorPublicacion(la, "P", ainicio, afin);
        la.iniciarValores(conISI, conCC, conCR); // Transforma el valor inicial en el puntaje ronda 0
        for(int i = 0; i < niters; ) {
            System.out.println("Iterando conferencias " + i + "...");
            iterarConferenciasViaPub(la, lc, lp, 1);
            if(i > 0)
                System.out.println("Max error = " + lc.maxError(1));
            else
                lc.llenarValorInicial();
            lc.pasarRonda();
//            i++;
            System.out.println("Iterando autores " + i + "...");
            iterarAutores(la, lc, lr, 1);
            if(i > 0)
                System.out.println("Max error = " + la.maxError(1));
            la.pasarRonda();
            i++;
        }
        // Aqui se pueden imprimir a la pantalla o grabar en la b.d. los resultados
        System.out.println("Imprimiendo autores...");
//        la.imprimir(1, 1, 2000);
        System.out.println("Imprimiendo conferencias...");
//        lc.imprimir(1, 1);
        
        la.grabarBD();
        lc.grabarBD();
    }
    public static void iterarAutores(LectorAutores la, LectorConferencias lc, LectorRelacion lr, int paso) {
        la.iniciarRonda(paso);
        lr.inicio();
        while(lr.siguiente()) {
            int peso = lr.peso();
            Autor cualautor = la.autor(lr.nombre());
            Conferencia cualconf = lc.conferencia(lr.identificador());
    //        int numautor = cualautor.numeroPublicacionesConferencias;
    //        int numconf = cualconf.numeroPublicaciones;
            cualautor.puntajeRonda[paso] += peso*cualconf.puntajeRonda[paso - 1];
        }
        la.normalizarRonda(paso);
    }
    public static void iterarConferencias(LectorAutores la, LectorConferencias lc, LectorRelacion lr, int paso) {
        lc.iniciarRonda(paso);
        lr.inicio();
        while(lr.siguiente()) {
            int peso = lr.peso();
        //    System.out.println(lr.nombre() + " <-> " + lr.identificador());
            Autor cualautor = la.autor(lr.nombre());
            Conferencia cualconf = lc.conferencia(lr.identificador());
     //       int numautor = cualautor.numeroPublicacionesConferencias;
            int numconf = cualconf.numeroPublicaciones;
            cualconf.puntajeRonda[paso] += peso*cualautor.puntajeRonda[paso - 1]/numconf;
        }
        lc.normalizarRonda(paso);
    }
    public static void iterarConferenciasViaPub(LectorAutores la, LectorConferencias lc, LectorPublicacion lp, int paso) {
        lc.iniciarRonda(paso);
        lp.inicio();
        while(lp.siguiente()) {
         //   int peso = 1; //lp.peso();  EL PESO ERA CUANTAS VECES ESTA REL.
        //    System.out.println(lr.nombre() + " <-> " + lr.identificador());
            Autor cualautor = lp.autor(paso - 1);
         /*   if(cualautor == null) {
                System.out.println("Error ??" + lp.publicacion().cantAutores);
                System.exit(0);
            }*/
            Conferencia cualconf = lc.conferencia(lp.identificador());
        //    int numautor = cualautor.numeroPublicacionesConferencias;
            int numconf = cualconf.numeroPublicaciones;
            cualconf.puntajeRonda[paso] += /*peso**/cualautor.puntajeRonda[paso - 1]/numconf;
        }
        lc.normalizarRonda(paso);
    }
/*    public static double formulaAct(double p1, int n1, double p2, int n2, int peso) {
        return p1 + p2*peso;
    }*/
}
