import java.io.FileReader;

import org.xml.sax.XMLReader;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.helpers.DefaultHandler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class parseDBLP extends DefaultHandler {
    String beingProc;
    boolean getThis;
    boolean insideOne;
    String waitingFor;
    String recentRead;
    
    int year;
    String booktitle;
    String journal;   // Equivalente a booktitle para revistas
    String title;
    String[] author;
    int authorLen;
    boolean isJournal;
    String key;
    
    Connection conn;
    
    public static void main(String args[]) throws Exception {
	    XMLReader xr = XMLReaderFactory.createXMLReader();
    	parseDBLP handler = new parseDBLP();
    	xr.setContentHandler(handler);
    	xr.setErrorHandler(handler);
    	for (int i = 0; i < args.length; i++) {
    	    FileReader r = new FileReader(args[i]);
    	    xr.parse(new InputSource(r));
    	}
    }
    public parseDBLP() throws Exception {
    	super();
        conn = null;
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conn = DriverManager.getConnection("jdbc:mysql:///DBLP", "cristian", "basededatos");
        if(conn.isClosed()) {
            System.out.println("Can't connect to database");
            System.exit(0);
        }
        System.out.println("Connected to MySQL");
        author = new String[40];
    }
    public void startDocument() {
        System.out.println("Start document");
    }
    public void endDocument() {
        System.out.println("End document");
    }
    public void startElement(String uri, String name, String qName, Attributes atts) {
	    if(!insideOne) {
            beingProc = qName;
            getThis = false;
            insideOne = true;
            waitingFor = qName;
            authorLen = 0;
            isJournal = false;
            if(beingProc.equals("inproceedings")) {
                // Articulo en conferencia
                getThis = true;
                key = atts.getValue("key");
            } else if(beingProc.equals("incollection")) {
                // Articulo en libro o serie
                getThis = true;
                key = atts.getValue("key");
            } else if(beingProc.equals("article")) {
                // Articulo en revista
                getThis = true;
                key = atts.getValue("key");
            } else if(beingProc.equals("proceedings")) {
                // Intancia de conferencia
                getThis = true;
                key = atts.getValue("key");
            }
/*            for (int i = 0; i < atts.getLength(); i++) {
                System.out.println(atts.getQName(i) + " = " + atts.getValue(i));
            }*/
        }
    }
    public void endElement(String uri, String name, String qName) {
        if(qName.equals(waitingFor)) {
            insideOne = false;
            if(getThis) {
                // AQUI HACER TODO EL PROCESO PARA LA B.D.

                int ind;
                if(!isJournal) {
                    ind = booktitle.indexOf("(");
                    if(ind != -1)
                        booktitle = booktitle.substring(0, ind);
                } else
                    booktitle = journal;
                for(int i = 0; i < authorLen; i++) {
            // Verificar si existia en la base de datos
            // Si no, agregar
            // Agregar la relacion autor - conferencia o revista
            // o aumentar la participacion en ella
                    
                }
            }
            return;
        }
        if(!getThis)
            return;
        if(qName.equals("author")) {
            author[authorLen] = new String(recentRead);
            authorLen++;
        } else if(qName.equals("booktitle")) {
            booktitle = new String(recentRead);
        } else if(qName.equals("journal")) {
            isJournal = true;
            journal = new String(recentRead);
        } else if(qName.equals("year")) {
            year = Integer.parseInt(recentRead);
        }
    }

    public void characters(char ch[], int start, int length) {
        if(!getThis)
            return;
        recentRead = new String();
        for(int i = start; i < start + length; i++) {
            recentRead = recentRead + ch[i];
        }
/*	System.out.print("Characters:    \"");
	for (int i = start; i < start + length; i++) {
	    switch (ch[i]) {
	    case '\\':
		System.out.print("\\\\");
		break;
	    case '"':
		System.out.print("\\\"");
		break;
	    case '\n':
		System.out.print("\\n");
		break;
	    case '\r':
		System.out.print("\\r");
		break;
	    case '\t':
		System.out.print("\\t");
		break;
	    default:
		System.out.print(ch[i]);
		break;
	    }
	}
	System.out.print("\"\n");*/
    }
}
