public class Escapador {
    public Escapador() {
    }
    public String escapar(String st) {
        String resp = "";
        for(int i = 0; i < st.length(); i++) {
            char c = st.charAt(i);
            if(c == '\'')
                resp += "\\";
            resp += st.charAt(i);
        }
        return resp;
    }
}
