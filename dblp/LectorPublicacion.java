import java.sql.ResultSet;
import java.sql.Statement;
import java.lang.*;
import java.util.*;

class Pub {
    public LinkedList autores;
//    public int cantAutores;
    public String ident;
//    public int i_nombre;
  //  public int i_ide;
    public Pub(String ide) {
        ident = new String(ide);
        autores = new LinkedList();
//        cantAutores = 0;
    }
    public void agregarAutor(Autor cual) {
        autores.add(cual);
//        cantAutores++;
    }
    public Autor maxAutor(int ronda) {
        if(autores.size() == 0)
            return null;
        Autor res = (Autor)autores.getFirst();
        double max = res.puntajeRonda[ronda];
        int tama = autores.size();
        for(int i = 1; i < tama; i++) {
            Autor cual = (Autor)autores.get(i);
            if(cual.puntajeRonda[ronda] > max) {
                res = cual;
                max = cual.puntajeRonda[ronda];
            }
        }
        return res;
    }
}

public class LectorPublicacion {
    private Pub[] lista;
    private int cantPubs;
    private String identif;
    private int leyendo;
    public LectorPublicacion(LectorAutores la, String tabla, int ainicio, int afin) throws Exception {
        Coneccion conn = new Coneccion();
        lista = new Pub[600000];
        Statement stmt = conn.createStatement();
        /* ESTE GRAN COMENTARIO ES LA MANERA ANTIGUA
           DE HACERLO. AHORA SE CREO UNA TABLA ESPECIAL
           EN LA BASE DE DATOS PARA HACER SOLO UNA CONSULTA RAPIDA 
           *******************************************************
        String tabla1 = "PUB_JOURNAL";
        String ident = "JID";
        String tabla2 = "AUTHOR_PJ";
        if(tabla.compareTo("P") == 0) {
            tabla1 = "PUB_PROCEEDING";
            tabla2 = "AUTHOR_PP";
            ident = "PID";
        }
        ResultSet Elementos = stmt.executeQuery("SELECT PUBID, " + ident +
                    " FROM " + tabla1 + 
                    " WHERE YEAR >= '" + ainicio + "' AND YEAR <= '" + afin + "'"
                     );
        identif = new String(ident);
        cantPubs = 0;
        while(Elementos.next()) {
            int pubid = ((Long)Elementos.getObject("PUBID")).intValue();
            String nident = (String)Elementos.getObject(ident);
            lista[cantPubs] = new Pub(nident);
//            System.out.println("Creada nueva publicacion");
            stmt = conn.createStatement();
            ResultSet El2 = stmt.executeQuery("SELECT NAME FROM " + tabla2 + 
                    " WHERE PUBID = '" + pubid + "'");
//            System.out.println("Realizada consulta");
            while(El2.next()) {
                String nname = (String)El2.getObject("NAME");
                lista[cantPubs].agregarAutor(la.autor(nname));
//                System.out.println("Nombre agregado");
            }
//            System.out.println("Finalizado!");
            cantPubs++;
            if(cantPubs % 10000 == 0)
                System.out.println("Agregadas " + cantPubs + " relaciones");
        }*/

        String tabla1 = "PUB_JOURNAL";
        String ident = "JID";
        String tabla2 = "AUTHOR_PJ";
        if(tabla.compareTo("P") == 0) {
            tabla1 = "PUB_PROCEEDING";
            tabla2 = "AUTHOR_PP";
            ident = "PID";
        }
        ResultSet Elementos = stmt.executeQuery("SELECT NAME, PUBID, " + ident +
                    " FROM PUBLICACION " + 
                    " WHERE YEAR >= '" + ainicio + "' AND YEAR <= '" + afin + "'"
                    // + " ORDER BY PUBID"
                     );
        identif = new String(ident);
        cantPubs = 0;
        int lpubid = -1;
        while(Elementos.next()) {
            // PARA QUE ESTO FUNCIONE, LA TABLA ESTA ORDENADA POR PUBID
            // O BIEN EN LA CONSULTA AGREGAR ORDER BY PUBID
            int npubid = ((Long)Elementos.getObject("PUBID")).intValue();
            String nident = (String)Elementos.getObject(ident);
            String nname = (String)Elementos.getObject("NAME");
//            System.out.print(lpubid + " ");
            if(npubid != lpubid) {
                lista[cantPubs] = new Pub(nident);
                cantPubs++;
                lpubid = npubid;
                if(cantPubs % 10000 == 0)
                    System.out.println("Agregadas " + cantPubs + " pubs");
            }
//            System.out.print(npubid + " " + nname + " - " + nident + "....");
            lista[cantPubs - 1].agregarAutor(la.autor(nname));
        }

        inicio();
        
        for(int i = 0; i < cantPubs; i++) {
            if(lista[i].autores.size() <= 0) {
                System.out.println("Hubo algun error");
                System.exit(0);
            }
        }
    }
    public boolean siguiente() {
        leyendo++;
        if(leyendo >= cantPubs) {
            leyendo = -1;
            return false;
        }
        return true;
    }
    public void inicio() {
        leyendo = -1;
    }
    public Pub publicacion() {
        return lista[leyendo];
    }
    public Autor autor(int ronda) {
        return lista[leyendo].maxAutor(ronda);
    }
    public String identificador() {
        return lista[leyendo].ident;
    }
}
