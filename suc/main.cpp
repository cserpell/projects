#include <iostream>
#include <vector>
#include <string>

using namespace std;

// CLASE GENERAL: SUCESION (template)
template<class C> class Sucesion : public vector<C>
{
public:
    Sucesion() : vector<C>() { }
    Sucesion(int n) : vector<C>(n+1) { } //�Por qu� no puedo poner vector<C>(n+1)?
    C Suma();
    C Suma(int n);
};

template<class C> C Sucesion<C>::Suma()
{
    C sum = 0;
    int n = size();
    for(int i = 0; i < n; i++)
        sum = sum + at(i);
    return sum;
}
template<class C> C Sucesion<C>::Suma(int n)
{
    C sum = 0;
    if(n >= (int)size())
        n = size();    
    for(int i = 0; i < n; i++)
        sum = sum + at(i);
    return sum;
}

// CLASE HIJA: MISUCESION (enteros)
class Misucesion : Sucesion<int>
{
    int maxim;          // Ya que size() cuenta el X_0 hasta el X_n da n+1
    int Tipo_Suma();
    bool Probar_A_Fuerza_Bruta(iterator pos);
    bool Mejor_Prueba(iterator pos, int saem);
public:
    Misucesion() : Sucesion<int>() { }
    Misucesion(int n) : Sucesion<int>(n) { maxim = n; }
    bool Correcta_Apuntadas();
    bool Correcta_Suma();
    bool Probar_A_Fuerza_Bruta();
    bool Mejor_Prueba();
//    int Suma();
    void Strpaprobar();
};
/*
int Misucesion::Suma()
{
    int sum;
    int n = size();    
    for(int i = 0; i < n; i++)
        sum = sum + at(i);
    return sum;
}*/

bool Misucesion::Correcta_Suma()
{
    if(Suma() == size())
        return true;
    return false;
}
int Misucesion::Tipo_Suma()
{
    int sum;
    sum = Suma();
    if(sum == (int)size())
        return 1;
    if(sum < (int)size())
        return 0;
    return -1;    
}
// Llama a la otra
bool Misucesion::Probar_A_Fuerza_Bruta()
{
    if(empty())
        return false;
    // Lo voy a empezar con s�lo ceros
    clear();
    for(int i = 0; i < maxim + 1; i++)
        push_back(0);
    return Probar_A_Fuerza_Bruta(begin());
}
// Por ahora retorna si es posible encontrar una que cumpla la condicion
bool Misucesion::Probar_A_Fuerza_Bruta(iterator pos)
{
//    cout << "Probando\n";
    if(pos != end()) // No es el �ltimo
    {
        for(int i = 0; i < maxim; i++) // Bastante mula mi algoritmo
        {
            erase(pos);
            pos = insert(pos, i);
            if(Tipo_Suma() >= 0)
            {
                if(Probar_A_Fuerza_Bruta(pos + 1))
                    return true;
            } else
            {
                erase(pos);
                insert(pos, 0);
                return false;
            }
        }
        erase(pos);
        insert(pos, 0);
        return false;
    }
    for(int i = 0; i < maxim; i++) // Bastante mula mi algoritmo
    {
        pop_back();
        push_back(i);
        int p = Tipo_Suma();
        if(p > 0)
        {
            if(Correcta_Apuntadas())
                return true;
        } else if(p < 0)
        {
            pop_back();
            push_back(0);
            return false;
        }
    }
    pop_back();
    push_back(0);
    return false;
}
// Llama a la otra
bool Misucesion::Mejor_Prueba()
{
    if(empty())
        return false;
    // Lo voy a empezar con s�lo ceros
    clear();
    for(int i = 0; i < maxim + 1; i++)
        push_back(0);
    return Mejor_Prueba(begin(), 0);
}
// Ahora haremos un mejor algoritmo, probando solo las que se pueden segun su suma
// saem guarda la suma hasta ese momento, as� no tengo que calculara en cada paso
bool Misucesion::Mejor_Prueba(iterator pos, int saem)
{
    if(pos != end()) // No es el �ltimo
    {
        for(int i = 0; i < maxim;)
        {
            if(Mejor_Prueba(pos + 1, saem))  // Asumo que habia un 0
                return true;
            if(saem >= (int)size())  // Ninguno m�s puede ser
            {
                erase(pos);
                insert(pos, 0);  // Lo dejo como 0
                return false;
            }
            saem++;
            erase(pos);
            pos = insert(pos, ++i);  // Siguiente a probar (�es ++i o i++?)
        }
        erase(pos);
        insert(pos, 0);     // No servia, lo dejo como 0
        return false;
    }
    // Es el �ltimo
    int p = size() - saem; // Supongo que todos han partido con 0
/*    if(p < 0)
        return false;*/    // Supongo que lo corrijo antes de llamarla!
    if(p != 0)
    {
        pop_back();  // Saco y agrego el �ltimo con el valor que falta
        push_back(p);
        if(Correcta_Apuntadas()) // Pruebo (la suma ya debiera estar bien)
            return true;
        pop_back();   // Lo vuelvo a 0
        push_back(0);
        return false;
    }
    if(Correcta_Apuntadas())  // El 0 me sirve?
        return true;
    return false;   // Ya est� con 0, da igual
}
bool Misucesion::Correcta_Apuntadas()
{
    for(int i = 0; i < (int)size(); i++)
        if(count(begin(), end(), i) != at(i))
            return false;
    return true;
}
void Misucesion::Strpaprobar()
{
    for(int i = 0; i < (int)size(); i++)
        cout << at(i) << ' ';
    cout << '\n';
}

int main(int narg, char *args[])
{
    if(narg != 2)
        return 0;
    Misucesion suc(atoi(args[1]));
    cout << (suc.Probar_A_Fuerza_Bruta()?"Se puede. suc=":"No se puede. suc=");
    suc.Strpaprobar();    
    return 0;
}
int main2(int narg, char *args[])
{
    if(narg != 2)
        return 0;
    Misucesion suc(atoi(args[1]));
    cout << (suc.Mejor_Prueba()?"Se puede. suc=":"No se puede. suc=");
    suc.Strpaprobar();    
    return 0;
}
/*
int main()
{
    while(true)
    {
        int ss;
        cout << "Ingresa el tamano.\n";
        cin >> ss;
        if(ss == -1)
            break;
        Misucesion suc(ss);
        cout << "Se puede: " << (suc.Probar_A_Fuerza_Bruta()?"si.\n":"no.\n");
        cout << "Correcta: " << ((suc.Correcta_Apuntadas()&&suc.Correcta_Suma())?"si.\n":"no.\n");
        cout << "NSe puede: " << (suc.Mejor_Prueba()?"si.\n":"no.\n");
        cout << "Correcta: " << ((suc.Correcta_Apuntadas()&&suc.Correcta_Suma())?"si.\n":"no.\n");
        suc.Strpaprobar();
    }
    return 0;
}
*/
