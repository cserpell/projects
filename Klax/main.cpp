//---------------------------------------------------------------------------
#include <vcl.h>
#include <string.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
  bmp = new Graphics::TBitmap();
  bmp->PixelFormat = pf32bit;
  comenzado = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::InicioClick(TObject *Sender)
{
  bmp->Width = PaintBox1->Width;
  bmp->Height = PaintBox1->Height;
  juego = new Juego(5, 6);
  escy = bmp->Height/juego->AlturaTotal();
  escx = bmp->Width/maxx;
  UpDown1->Position = 1000;
  Timer1->Interval = 1000;
  comenzado = true;
}
//---------------------------------------------------------------------------
void TForm1::Dibujar()
{
  int aa = juego->AlturaTotal();

  Label1->Caption = IntToStr(juego->Verticales());
  Label2->Caption = IntToStr(juego->Horizontales());
  Label3->Caption = IntToStr(juego->Diagonales());
  Label4->Caption = IntToStr(juego->Puntaje());
  Label9->Caption = IntToStr(juego->Errores());
  Label12->Caption = IntToStr(UpDown1->Position);
  Memo1->Clear();
  for(int y = 0; y < aa; y++)
  {
    string ll = juego->MostrarLinea(y);
    string p = "";                 // El memo

    for(int i = 0; i < maxx; i++)
    {
      p += ll[i];
      p += ll[i];
    }
    Memo1->Lines->Append(p.c_str());
    for(int i = 0; i < escy; i++)    // El paintbox
    {
      int *pb = (int *)bmp->ScanLine[y*escy + i];

      for(int j = 0; j < maxx; j++)
        for(int m = 0; m < escx; m++)
          pb[j*escx + m] = ll[j]*0x002222;
    }
  }
  PaintBox1Paint(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormKeyPress(TObject *Sender, char &Key)
{
  if(comenzado)
  {
    if(!juego->Jugar(Key))
      comenzado = false;
    Dibujar();
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
  if(comenzado)
  {
    if(!juego->Jugar('l'))
      comenzado = false;
    Dibujar();
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TerminoClick(TObject *Sender)
{
  comenzado = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBox1Paint(TObject *Sender)
{
  PaintBox1->Canvas->Draw(0, 0, bmp);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::UpDown1Click(TObject *Sender, TUDBtnType Button)
{
  Timer1->Interval = UpDown1->Position;
}
//---------------------------------------------------------------------------

