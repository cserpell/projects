//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Clases.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

string tablero::Bajar(string nuevalinea)
{
  int len = nuevalinea.length();

  if(len > maxx)
    return "";
  if(len < maxx)
    for(int i = len; i < maxx; i++)
      nuevalinea += " ";

  string res = linea[0];

  for(int i = 0; i < maxy - 1; i++)
    linea[i] = linea[i + 1];
  linea[maxy - 1] = nuevalinea;
  return res;
}
//---------------------------------------------------------------------------
string tablero::Bajar()
{
  string res = linea[0];

  for(int i = 0; i < maxy - 1; i++)
    linea[i] = linea[i + 1];
  for(int i = 0; i < maxx; i++)
    linea[maxy - 1][i] = ' ';
  return res;
}
//---------------------------------------------------------------------------
string tablero::Linea(int lin)
{
  if(lin < 0 ||lin >= maxy)
    return "";
  return linea[maxy - lin - 1];
}
//---------------------------------------------------------------------------
void tablero::LlegadaAbajo(char a, int posx, int alt)
{
  if(a == ' ')
    return;
  if(alt > maxy)
    alt = maxy;
  if(alt < 1)
    alt = 1;
  if(posx >= maxx)
    posx = maxx - 1;
  if(posx < 0)
    posx = 0;
  while(alt <= maxy && linea[alt - 1][posx] != ' ')
    alt++;
  if(alt > maxy)       // Raro ser�a esto!
    return;
  linea[alt - 1][posx] = a;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
bool sopor::AgregarFicha(char a)
{
  return fichas.AgregarFicha(a);
}
//---------------------------------------------------------------------------
string sopor::Linea(int lin)
{
  string res = "";
  int pr;

  if(lin < 0 || lin >= alturamax)
    return res;
  pr = alturamax - lin - 1;
  for(int i = 0; i < maxx; i++)
  {
    if(i == posx && pr < fichas.Altura())
      res += fichas.Linea()[pr];
    else
      res += " ";
  }
  return res;
}
//---------------------------------------------------------------------------
bool sopor::MoverIzquierda()
{
  if(posx == 0)
    return false;
  posx--;
  return true;
}
//---------------------------------------------------------------------------
bool sopor::MoverDerecha()
{
  if(posx == mx - 1)
    return false;
  posx++;
  return true;
}
//---------------------------------------------------------------------------
char sopor::SoltarFicha()
{
  return fichas.SoltarFicha();
}
//---------------------------------------------------------------------------
bool sopor::DefinirAncho(int nmx)
{
  if(nmx > maxx)
    return false;
  mx = nmx;
  return true;
}
//---------------------------------------------------------------------------
int sopor::Posicion()
{
  return posx;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
bool caja::AgregarFicha(char a, int px)
{
  if(px < 0 || px >= mx)
    return false;
  return fichas[px].AgregarFicha(a);
}
//---------------------------------------------------------------------------
string caja::Linea(int lin)
{
  if(lin < 0 || lin >= alturamax)
    return "";

  string res = "";
  int pr = alturamax - lin - 1;

  for(int i = 0; i < maxx; i++)
  {
    if(pr < fichas[i].Altura())
      res += fichas[i].Linea()[pr];
    else
      res += " ";
  }
  return res;
}
//---------------------------------------------------------------------------
int caja::DetectarLinea() // Retorna el tipo que hubo (para el puntaje)
{
  // El formato ser� 0 + 0x000001 por cada vertical +
  //                     0x000100 por cada horizontal +
  //                     0x010000 por cada diagonal
  int res = 0;
  string Lin[maxx];
  string Llin[maxx];
  int alt = 0;

  for(int i = 0; i < mx; i++)
  {
    Llin[i] = vacio;
    Lin[i] = fichas[i].Linea();
  }
  for(int i = 0; i < mx; i++)
    for(int j = 0; j < alturamax; j++)
    {
      char p = Lin[i][j];

      if(p != ' ')
      {
        if(i < mxxx)
        {
          if(p == Lin[i + 1][j] && p == Lin[i + 2][j])
          {
            Llin[i][j] = '*';
            Llin[i + 1][j] = '*';            // Horizontales
            Llin[i + 2][j] = '*';
            res += 0x000100;
          }
          if(j < myyy && p == Lin[i + 1][j + 1] && p == Lin[i + 2][j + 2])
          {
            Llin[i][j] = '*';
            Llin[i + 1][j + 1] = '*';      // Diagonales
            Llin[i + 2][j + 2] = '*';
            res += 0x010000;
          }
          if(j > 1 && p == Lin[i + 1][j - 1] && p == Lin[i + 2][j - 2])
          {
            Llin[i][j] = '*';
            Llin[i + 1][j - 1] = '*';
            Llin[i + 2][j - 2] = '*';    // Las otras diagonales
            res += 0x010000;
          }
        }
        if(j < myyy && p == Lin[i][j + 1] && p == Lin[i][j + 2])
        {
          Llin[i][j] = '*';
          Llin[i][j + 1] = '*';  // Verticales
          Llin[i][j + 2] = '*';
          res += 0x000100;
        }
      }
    }
  for(int i = 0; i < mx; i++)
  {
    for(int j = 0; j < alturamax; j++)
      if(Llin[i][j] == '*')
        fichas[i].BorrarFichaPos(j);
    fichas[i].Reordenar();
    alt += fichas[i].Altura();
  }
  if(alt >= mx*alturamax)  // Est� lleno
    return -1;
  return res;
}
//---------------------------------------------------------------------------
bool caja::DefinirAncho(int nmx)
{
  if(nmx > maxx)
    return false;
  mx = nmx;
  mxxx = mx - 2;
  return true;
}
//---------------------------------------------------------------------------
void caja::LimpiarCaja()
{
  for(int i = 0; i < maxx; i++)
    fichas[i].LimpiarTorre();
}

