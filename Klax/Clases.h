//---------------------------------------------------------------------------
#ifndef ClasesH
#define ClasesH
//---------------------------------------------------------------------------
#include "torre.h"

const int maxx = 5;
const int maxy = 40;

// Cada char representa el color de la ficha: ' ' = no hay nada
class tablero
{
 private:
  string linea[maxy];
 public:
  string Bajar(string nuevalinea);
  string Bajar();
  string Linea(int lin);
  void LlegadaAbajo(char a, int posx, int alt);
  tablero()
  {
    for(int i = 0; i < maxy; i++)
    {
      linea[i] = "";
      for(int j = 0; j < maxx; j++)
        linea[i] += " ";
    }
  }
};
//---------------------------------------------------------------------------
class sopor
{
 private:
  int mx;
  torre fichas;
  int posx;
 public:
  string Linea(int lin);
  bool AgregarFicha(char a);
  bool MoverIzquierda();
  bool MoverDerecha();
  char SoltarFicha();
  bool DefinirAncho(int nmx);
  int Posicion();
  sopor()
  {
    posx = 0;
    fichas.LimpiarTorre();
  }
};
//---------------------------------------------------------------------------
class caja
{
 private:
  int mx;
  torre fichas[maxx];
  int mxxx;
  int myyy;
  string vacio;
 public:
  bool AgregarFicha(char a, int px);
  int DetectarLinea(); // Retorna el tipo que hubo (para el puntaje)
  string Linea(int lin);
  bool DefinirAncho(int nmx);
  void LimpiarCaja();
  caja()
  {
    mx = 0;
    LimpiarCaja();
    myyy = alturamax - 2;
    for(int i = 0; i < alturamax; i++)
      vacio += " ";
  }
};
#endif
