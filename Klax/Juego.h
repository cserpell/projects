//---------------------------------------------------------------------------
#ifndef JuegoH
#define JuegoH
//---------------------------------------------------------------------------
#include "Clases.h"

class Juego
{
 private:
  int ancho;    // Ancho
  int colores;  // Cantidad de colores en juego!
  int alttotal;
  int horiz;
  int vert;
  int diag;
  int odiag;
  int ohoriz;
  int overt;
  int opunt;
  int oerro;
  int punt;
  int erro;
  tablero tab;
  sopor sop;
  caja caj;
  string GenerarNuevaLinea(int len);
 public:
  int Verticales();
  int Horizontales();
  int Diagonales();
  int Puntaje();
  int Errores();
  bool Jugar(char ev);
  bool Evento(char ev);
  int AlturaTotal();
  string MostrarLinea(int numlinea);
  int DetectarLinea();
  Juego(int p, int q)
  {
    alttotal = maxy + 2*alturamax + 3;
    ancho = p;
    colores = q;
    horiz = 0;
    vert = 0;
    diag = 0;
    punt = 0;
    erro = 0;
    odiag = 0;           // Objetivos!
    ohoriz = 0;
    overt = 0;
    opunt = 0;
    oerro = 0;
    sop.DefinirAncho(ancho);
    sop.MoverDerecha();
    sop.MoverDerecha();
    caj.DefinirAncho(ancho);
  }
};
#endif
 