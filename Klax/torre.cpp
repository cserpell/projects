//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "torre.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

bool torre::DefinirAlturaMax(int namx)
{
  if(namx > alturamax)
    return false;
  alturamaxactual = namx;
  return true;
}
//---------------------------------------------------------------------------
void torre::LimpiarTorre()
{
  altura = 0;
}
//---------------------------------------------------------------------------
bool torre::AgregarFicha(char a)
{
  if(a == ' ')
    return true;
  if(altura == alturamaxactual)
    return false;
  ficha[altura] = a;
  altura++;
  return true;
}
//---------------------------------------------------------------------------
void torre::BorrarFichaPos(int pos)
{
  if(pos < 0 || pos >= altura)
    return;
  ficha[pos] = ' ';    // Nota: Siempre llamar a Reordenar despu�s!
}
//---------------------------------------------------------------------------
void torre::Reordenar()
{
  int i = 0;

  while(i < altura)
  {
    if(ficha[i] == ' ')
    {
      for(int j = i; j < altura - 1; j++)
        ficha[j] = ficha[j + 1];
      altura--;
    } else
      i++;
  }
}
//---------------------------------------------------------------------------
char torre::SoltarFicha()
{
  if(altura == 0)
    return ' ';
  altura--;
  return ficha[altura];
}
//---------------------------------------------------------------------------
string torre::Linea() // Retorna la torre como un string
{
  string str = "";
  int i = 0;

  while(i < altura)
    str += ficha[i++];
  while(i++ < alturamax)
    str += " ";
  return str;
}
//---------------------------------------------------------------------------
int torre::Altura()
{
  return altura;
}
//---------------------------------------------------------------------------
