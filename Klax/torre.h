//---------------------------------------------------------------------------
#ifndef torreH
#define torreH
//---------------------------------------------------------------------------
#include <fstream.h>

const int alturamax = 5;

class torre
{
 private:
  int altura;
  int alturamaxactual;
  char ficha[alturamax];
 public:
  bool AgregarFicha(char a);
  char SoltarFicha();
  void BorrarFichaPos(int pos);
  void Reordenar();
  int Altura();
  string Linea(); // Retorna la torre como un string
  bool DefinirAlturaMax(int namx);
  void LimpiarTorre();
  torre()
  {
    alturamaxactual = alturamax;
    LimpiarTorre();
  }
};
#endif
