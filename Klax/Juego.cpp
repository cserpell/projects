//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>
#pragma hdrstop

#include "Juego.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

string Juego::GenerarNuevaLinea(int len)
{
  int p = random(len + colores*4);
  string res = "";

  for(int i = 0; i < len; i++)
    res += " ";
  if(p >= len)
    return res;
  res[p] = (char)(random(colores) + (int)'a');
  return res;
}
//---------------------------------------------------------------------------
int Juego::AlturaTotal()
{
  return alttotal;
}
//---------------------------------------------------------------------------
int Juego::Verticales()
{
  return vert;
}
//---------------------------------------------------------------------------
int Juego::Horizontales()
{
  return horiz;
}
//---------------------------------------------------------------------------
int Juego::Diagonales()
{
  return diag;
}
//---------------------------------------------------------------------------
int Juego::Puntaje()
{
  return punt;
}
//---------------------------------------------------------------------------
int Juego::Errores()
{
  return erro;
}
//---------------------------------------------------------------------------
string Juego::MostrarLinea(int numlinea)
{
  if(numlinea < 0 || numlinea >= alttotal)
    return "";
  if(numlinea < maxy)
    return tab.Linea(numlinea);
  if(numlinea == maxy)
  {
    string res = "";

    for(int i = 0; i < maxx; i++)
      res += "-";
    return res;
  }
  numlinea -= maxy + 1;
  if(numlinea < alturamax)
    return sop.Linea(numlinea);
  numlinea -= alturamax;
  if(numlinea == 0)
  {
    string res = "";

    for(int i = 0; i < maxx; i++)
    {
      if(i == sop.Posicion())
        res += "^";
      else
        res += " ";
    }
    return res;
  }
  if(numlinea == 1)
  {
    string res = "";

    for(int i = 0; i < maxx; i++)
      res += "-";
    return res;
  }
  numlinea -= 2;
  return caj.Linea(numlinea);
}
//---------------------------------------------------------------------------
int Juego::DetectarLinea()
{
  return caj.DetectarLinea();
}
//---------------------------------------------------------------------------
bool Juego::Evento(char ev) // Retorna false cuando se pierde
{
  char a;
  string n;

  switch(ev)
  {
   case 'l': // Loop, bajar el tablero y llenar soporte
   case 'j': // Abajo, acelera la bajada
    n = tab.Bajar(GenerarNuevaLinea(ancho));
    if(!sop.AgregarFicha(n.c_str()[sop.Posicion()]))
      return false;
    for(int i = 0; i < ancho; i++)
      if(sop.Posicion() != i && n.c_str()[i] != ' ')
        return false;
    break;
   case 'h': // Izquierda, soporte
    sop.MoverIzquierda();
    break;
   case 'k': // Derecha, soporte
    sop.MoverDerecha();
    break;
   case 'u': // Arriba, devolver pieza al tablero desde soporte
    a = sop.SoltarFicha();
    tab.LlegadaAbajo(a, sop.Posicion(), ancho);
    break;
   case 'f': // Bot�n, baja pieza del soporte a la caja
    a = sop.SoltarFicha();
    if(!caj.AgregarFicha(a, sop.Posicion()))
      sop.AgregarFicha(a);
    break;
   default:  // Nada
    break;
  }
  return true;
}
//---------------------------------------------------------------------------
bool Juego::Jugar(char ev)
{
  if(!Evento(ev))
  {
    erro++;
    if(oerro != 0 && erro >= oerro)
      return false;
    return true;
  }
  if(ev == 'f')
    while(true)
    {
      int p = DetectarLinea();

      if(p == 0)
        return true;
      if(p == -1)
        return false;
      int v = p%0x000100;
      vert += v;
      punt += v*50;
      int h = (p/0x000100)%0x0100;
      horiz += h;
      punt += h*1000;
      int d = p/0x010000;
      horiz += d;
      punt += d*5000;
      if((ohoriz != 0 && horiz >= ohoriz)
         || (odiag != 0 && diag >= odiag)
         || (overt != 0 && vert >= overt)
         || (opunt != 0 && punt >= opunt))
        return false;
    }
  return true; 
}
//---------------------------------------------------------------------------

