//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TPaintBox *PaintBox;
        TEdit *Largo;
        TButton *Comienzo;
        TCheckBox *sencos;
        TEdit *a1;
        TEdit *a2;
        TEdit *f1;
        TEdit *f2;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *Resol;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TEdit *zoom;
        TLabel *Label6;
        void __fastcall ComienzoClick(TObject *Sender);
        void __fastcall PaintBoxPaint(TObject *Sender);
private:	// User declarations
        Graphics::TBitmap *bmp;
        void Plot(int x, int y);
        void GraficarPolar(double amp1, double frec1, double amp2, double frec2);
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
