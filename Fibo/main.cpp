//---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
    bmp = new Graphics::TBitmap();
    bmp->PixelFormat = pf32bit;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ComienzoClick(TObject *Sender)
{
    if(sencos->Checked)
    {
        GraficarPolar(StrToFloat(a1->Text), StrToFloat(f1->Text),
                                  StrToFloat(a2->Text), StrToFloat(f2->Text));
        return;
    }
    double zzz = StrToFloat(zoom->Text);
    int iter = StrToInt(Largo->Text);
    int rad0 = 1, rad1 = 1;
    int dir = 0;   // Direcci�n del pr�ximo cuadrado
    int xm, ym;
    bmp->Width = PaintBox->Width;
    bmp->Height = PaintBox->Height;
    xm = bmp->Width/(2*zzz);
    ym = bmp->Height/(2*zzz);
    bmp->Canvas->Brush->Style = bsSolid;
    bmp->Canvas->Brush->Color = clBlack;
    bmp->Canvas->Pen->Color = clBlack;
    bmp->Canvas->Rectangle(0, 0, bmp->Width, bmp->Height);
    bmp->Canvas->Pen->Color = clRed;
    bmp->Canvas->Brush->Style = bsClear;
    for(int i = 0; i < iter; i++)
    {
        int a1, a2, a3, a4, a5, a6, a7, a8;
        a1 = xm - rad1;  // Bordes del supuesto c�rculo
        a2 = ym - rad1;
        a3 = xm + rad1;
        a4 = ym + rad1;
        if(dir == 0)
        {
            a5 = xm;
            a6 = ym + rad1;
            a7 = xm + rad1;
            a8 = ym;
            xm -= rad0;
        }
        else if(dir == 1)
        {
            a5 = xm + rad1;
            a6 = ym;
            a7 = xm;
            a8 = ym - rad1;
            ym += rad0;
        }
        else if(dir == 2)
        {
            a5 = xm;
            a6 = ym - rad1;
            a7 = xm - rad1;
            a8 = ym;
            xm += rad0;
        }
        else if(dir == 3)
        {
            a5 = xm - rad1;
            a6 = ym;
            a7 = xm;
            a8 = ym + rad1;
            ym -= rad0;
        }
        bmp->Canvas->Arc(zzz*a1, zzz*a2, zzz*a3, zzz*a4, zzz*a5, zzz*a6, zzz*a7, zzz*a8);
        dir++;
        if(dir == 4)
            dir = 0;
        int trad = rad1;
        rad1 += rad0;
        rad0 = trad;
    }
    PaintBoxPaint(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBoxPaint(TObject *Sender)
{
    PaintBox->Canvas->Draw(0, 0, bmp);
}
//---------------------------------------------------------------------------
void TForm1::Plot(int x, int y)
{
    if(x < 0 || x >= bmp->Width || y < 0 || y >= bmp->Height)
        return;
    int *ptr = (int *)bmp->ScanLine[y];
        ptr[x] = clBlue;
}
//---------------------------------------------------------------------------
void TForm1::GraficarPolar(double amp1, double frec1, double amp2, double frec2)
{
    double zzz = StrToFloat(zoom->Text);
    int iter = StrToInt(Largo->Text);
    int maxi = StrToInt(Resol->Text);
    int rad0 = 1, rad1 = 1;
    int dir = 0;   // Direcci�n del pr�ximo cuadrado
    int xm, ym;
    bmp->Width = PaintBox->Width;
    bmp->Height = PaintBox->Height;
    xm = bmp->Width/(2*zzz);
    ym = bmp->Height/(2*zzz);
    bmp->Canvas->Brush->Style = bsSolid;
    bmp->Canvas->Brush->Color = clBlack;
    bmp->Canvas->Pen->Color = clBlack;
    bmp->Canvas->Rectangle(0, 0, bmp->Width, bmp->Height);
    bmp->Canvas->Pen->Color = clRed;
    bmp->Canvas->Brush->Style = bsClear;
    for(int i = 0; i < iter; i++)
    {
        if(dir == 0)
        {
            for(int j = 0; j < maxi; j++)  // Graficamos el circulo
                Plot(zzz*( (rad1 + amp1*sin(frec1*M_PI_2*j/maxi) + amp2*sin(frec2*M_PI_2*j/maxi))*sin(M_PI_2*j/maxi) + xm),
                     zzz*( (rad1 + amp1*sin(frec1*M_PI_2*j/maxi) + amp2*sin(frec2*M_PI_2*j/maxi))*cos(M_PI_2*j/maxi) + ym));
            xm -= rad0;
        }
        else if(dir == 1)
        {
            for(int j = 0; j < maxi; j++)  // Graficamos el circulo
                Plot(zzz*( (rad1 + amp1*sin(frec1*M_PI_2*j/maxi) + amp2*sin(frec2*M_PI_2*j/maxi))*cos(M_PI_2*j/maxi) + xm),
                     zzz*(-(rad1 + amp1*sin(frec1*M_PI_2*j/maxi) + amp2*sin(frec2*M_PI_2*j/maxi))*sin(M_PI_2*j/maxi) + ym));
            ym += rad0;
        }
        else if(dir == 2)
        {
            for(int j = 0; j < maxi; j++)  // Graficamos el circulo
                Plot(zzz*(-(rad1 + amp1*sin(frec1*M_PI_2*j/maxi) + amp2*sin(frec2*M_PI_2*j/maxi))*sin(M_PI_2*j/maxi) + xm),
                     zzz*(-(rad1 + amp1*sin(frec1*M_PI_2*j/maxi) + amp2*sin(frec2*M_PI_2*j/maxi))*cos(M_PI_2*j/maxi) + ym));
            xm += rad0;
        }
        else if(dir == 3)
        {
            for(int j = 0; j < maxi; j++)  // Graficamos el circulo
                Plot(zzz*(-(rad1 + amp1*sin(frec1*M_PI_2*j/maxi) + amp2*sin(frec2*M_PI_2*j/maxi))*cos(M_PI_2*j/maxi) + xm),
                     zzz*( (rad1 + amp1*sin(frec1*M_PI_2*j/maxi) + amp2*sin(frec2*M_PI_2*j/maxi))*sin(M_PI_2*j/maxi) + ym));
            ym -= rad0;
        }
        dir++;
        if(dir == 4)
            dir = 0;
        int trad = rad1;
        rad1 += rad0;
        rad0 = trad;
    }
    PaintBoxPaint(this);
}
//---------------------------------------------------------------------------
