/* hd_dd.i */

#ifdef ROTACION
#define Radio      0.5 
#define DosMu      1.0
#define K          2.0
#define UnoMasK    3.0 
#define KSobreMR   4.0 
#define Inercia    (Radio*Radio/K)
#define SIGNO(x)  ((x>0)?1.0:-1.0)
#define lambda_c    mu_kCDD;
#define lambda_e    mu_sCDD;
#endif

/* Cambia velocidades de disco1 y disco2 en una colision CDD */

void CambioVelocCDD(int disco1,int disco2)
{
#ifdef ROTACION
    double Vn,Vt,nx,ny,tx,ty,ux,uy,Qn,Qt,Qx,Qy,Vct;
    double Omega,aux,raux;
    double V2,p_n,p_t,r_n,r_t,vg;
    int    desliza;
    static long int ce=0;
    static double sumqn=0.0,sumqt=0.0;
    double q_n,q_t,aux1,aux2;

    VectorNormal(disco1,disco2,Diametro,&nx,&ny);

    tx = ny;
    ty = -nx;

    Omega = Radio*VelWz[disco1]+Radio*VelWz[disco2];
    Vn = (VelX[disco2]-VelX[disco1])*nx + (VelY[disco2]-VelY[disco1])*ny;
    Vt = (VelX[disco2]-VelX[disco1])*tx + (VelY[disco2]-VelY[disco1])*ty;
    Vct=Vt+Omega; 

#ifdef ARENA

#ifdef COTA_V2_CDD
    V2 = Vn*Vn;
    if (V2 > cota_v2_CDD) p_n=PnCDD; else p_n=1.0;
#else
    p_n=PnCDD;
#endif

#ifdef COTA_V2_CDD
    V2 = Vct*Vct;
    if (V2 > cota_v2_CDD) p_t=PtCDD; else p_t=1.0;
#else
    p_t=PtCDD;
#endif


    Qn = DosMu*p_n*Vn;
    Qt = DosMu*p_t*Vct/UnoMasK;

    if (fabs(Qt/Qn)>lambda_e) Qt = -SIGNO(Vct)*lambda_c*Qn;

#else /* not ARENA */

    Qn = DosMu*Vn;
    Qt = DosMu*Vct/UnoMasK;

#endif

    Qx = Qn*nx+Qt*tx;
    Qy = Qn*ny+Qt*ty;

    VelX[disco1] += Qx;
    VelY[disco1] += Qy;
    VelX[disco2] -= Qx;
    VelY[disco2] -= Qy;
    VelWz[disco1] -= KSobreMR*Qt;
    VelWz[disco2] -= KSobreMR*Qt;

    IntercambioVel = Qn;

#undef K
#undef Radio
#undef DosMu
#undef UnoMasK
#undef KSobreMR
#undef Inercia
#undef SIGNO

#else /* not ROTACION */

    static double Qn,nx,ny,Qx,Qy;
    double V2,p_n;

    VectorNormal(disco1,disco2,Diametro,&nx,&ny);

    Qn = (VelX[disco2]-VelX[disco1])*nx+(VelY[disco2]-VelY[disco1])*ny;

#ifdef ARENA
#ifdef COTA_V2_CDD
    V2 = Qn*Qn;
    if (V2 > cota_v2_CDD) p_n=PnCDD; else p_n=1.0;
#else
    p_n=PnCDD;
#endif
    Qn *= p_n;
#endif

    Qx = Qn*nx;
    Qy = Qn*ny;

    VelX[disco1] += Qx;
    VelY[disco1] += Qy;
    VelX[disco2] -= Qx;
    VelY[disco2] -= Qy;

    IntercambioVel = Qn;

#endif 

} /* CambioVelocCDD() */

