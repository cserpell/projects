/*   Compilacion  sistema granular con un solo tipo de particulas
     gcc -o go  file.c  -lplot -lm

	aqui el choque con la base es t'ermico
*/

#define GRAFICO   // puede colocarse undef y no dibuja

#include<stdio.h>
#include<math.h>
#include<stdlib.h>

#ifdef GRAFICO
#include<plot.h>
#endif

#define	   pi			M_PI
#define    N 			50  // numero de particulas
#define    Ll                   N    //largo de listas por partic
#define    largo		400000

#define	   rn			0.9  //coefs de restitucion
#define	   rt			0.9  // "
#define    kappa		0.2   // coef de roce

#define    Lx			30.0 //ancho caja
#define	   Ly		        30.0 //alto caja
#define    r1                   0.5  //radio base particulas 
#define    M1			(r1*r1) //masa de particulas
#define	   g			1.0   // gravedad
#define    semilla		1007
#define    nombre               "Alt_10n.dat"


//Si cada es 1 el prog avanza muy lento
#define    cada			(2*N)    //fotos cada cuanto

#define    infinite		(1e+40)
#define    epsi			0.04    //holgura, es +o- arbitrario
#define    chico		0.001         //0.008

double X[N],Y[N],Vx[N],Vy[N],
       tt[N],     // tt[k] tiempo minimo en lista de eventos de k
       T[N],      // tiempo en que se atendi'o a k
       Ta[N][Ll], //tiempo predicho choque i j
       w[N],	  // veloc angular
       A[N];	  // angulo
int    W[N],      // pared con que chocar'a k (0=L, 1=D, 2=R, 3=T)
       B[2*N+1],  // CBT
       C[N],      // # de choques duros de k
       S[N][Ll],  //socios
       K[N][Ll],  //kind (tipo de choque: 1 ==> DD,  0 ==> DW)
       cc[N][Ll], //c (ver articulo de 1993)
       G[N];      //valor de e,  0 <= e <= Ll
char   col[N][6], name[20];       

int    dens[30];
       
double reloj,reloj0,tnext,Vs,tF,IMr,tanFi,Eold,Tb,U,hG,hOld;
double sumat,sumar,sumaV,Et,Er,EV,Etot;
int    s,nF,knext,det,pl_handle,tot;
short  ojo;
FILE   *arch;

void NextEvent();
void EventosNuevos(int);
double distancia(int,int);


double distancia(i1,i2)
{
   double dis,x1,x2,y1,y2,dt1,dt2,dX,dY;
   dt1 = reloj - T[i1];
   x1  = X[i1] + Vx[i1]*dt1;
   y1  = Y[i1] + Vy[i1]*dt1 - 0.5*g*dt1*dt1;
   dt2 = reloj - T[i2];
   x2  = X[i2] + Vx[i2]*dt2;
   y2  = Y[i2] + Vy[i2]*dt2 - 0.5*g*dt2*dt2;
   dX  = x2-x1;
   dY  = y2-y1;
   return( sqrt(dX*dX+dY*dY) -2.0*r1 );
}



void Control()
{ int k1,k2;
  double dis;
  for(k1=0; k1<N-1; k1++)
  { for(k2=k1+1; k2<N; k2++)
    { dis = distancia(k1,k2);
      if(dis<-0.0001) 
      { printf(" Distancia [ %d ; %d ]=%f  !!!!!!!!!\n",k1,k2,dis);
        exit(0);
      }
    }
  }
}




#ifdef GRAFICO
void Dibuja()
{ int k;
  double t,x,y,alf;
  pl_erase();  
  for(k=0; k<N; k++)    
  {  t = tF - T[k];
     x = X[k] + Vx[k]*t;
     y = Y[k] + Vy[k]*t - 0.5*g*t*t;
     pl_fillcolorname(col[k]);
     pl_fcircle(x,y,0.95*r1);     
     alf = A[k] + w[k]*t;
     x = x  + 0.5*r1*cos(alf);
     y = y  + 0.5*r1*sin(alf);
     pl_fillcolorname("black");  
     pl_fcircle(x,y,0.3*r1);
  }
 // usleep(1000);
}   
#endif

void Mueve(int k)		//18
{ double t,hS;
  t     =   reloj - T[k];
  X[k]  =   X[k]  + Vx[k]*t;
  Y[k]  =   Y[k]  + Vy[k]*t - 0.5*g*t*t;
  A[k]  =   A[k]  + w[k]*t;
  Vy[k] =   Vy[k] - g*t;
  T[k]  =   reloj;
   hS   =  0.0;
}

void ChoqueDD(int k1, int k2) //regla de choque discos k1 y k2 //17
{ double tx,ty,v21x,v21y,eSe,vn,v21t,
         vt,qn,qt,qx,qy,Efin,nx,ny,nQ,ma1,ma2,mu,avt;
  //--geometria--
  nx    = X[k1]-X[k2];
  ny    = Y[k1]-Y[k2];
  nQ    = sqrt(nx*nx+ny*ny);
  nx    = nx/nQ;
  ny    = ny/nQ;
  tx	= ny;
  ty	= -nx;
  //--cinematica--
  v21x	= Vx[k2] - Vx[k1];
  v21y	= Vy[k2] - Vy[k1];
  eSe	= r1*w[k1] + r1*w[k2];
  vn	= v21x*nx + v21y*ny;
  v21t	= v21x*tx + v21y*ty;
  vt	= v21t - eSe;
  qn	= 0.5*(1+rn)*M1*vn;
  //CAMBIA SEGUN TIPO DE CHOQUE: <<<< ojo
  avt   = fabs(vt);
  if( avt < tanFi*fabs(vn))
       qt = 0.5*(1+rt)*M1*vt/3.0;
  else qt = avt*kappa*qn/vt; 
  qx	= qn*nx + qt*tx;
  qy	= qn*ny + qt*ty;
  //--resultados--
  Vx[k1]= Vx[k1] + qx/M1;
  Vy[k1]= Vy[k1] + qy/M1;
  Vx[k2]= Vx[k2] - qx/M1;
  Vy[k2]= Vy[k2] - qy/M1;
  w[k1] = w[k1]  + IMr*qt;  
  w[k2] = w[k2]  + IMr*qt;
}  


// Comienzo del cuesco.  Esto es solo para entendidos @@@@@@@@@@@@@@@@@@@@@
void LimpiaLista(int k)		//2
{ int e;
  for(e=0; e<Ll; e++)
   Ta[k][e] = infinite;
}
void AgregaEvento(int k,int Socio,int Tipo,int Ce,double Te)	//3
{ int e;
  e=0;
  while( Ta[k][e]!=infinite  && e<Ll)  e++;
  if(e>=Ll) 
  { printf("Lista llena!! e = %d (se debe usar un Ll + grande\n",e);
    exit(0);
  }
  S[k][e] = Socio;// de la lista
  K[k][e] = Tipo;
  cc[k][e]= Ce;
  Ta[k][e]= Te;
}
void Ganador(int k)
{ int e,j,ka;
  double tau;
  tau = infinite;
  for(j=0; j<Ll; j++)
  { if(Ta[k][j]<tau) 
    {  tau = Ta[k][j];
       e   = j;
       ka  = K[k][j];
    }
  }
  G[k]   = e;       // el ganador de la lista k es el 'e-simo evento 
  tt[k]  = Ta[k][e];// de la lista
}  
//__Comienza_CBT____Comienza_CBT____Comienza_CBT____Comienza_CBT____Comienza_CBT____
void UpGradeCBT(int kk)	//6
{ int f, ll, rr, kl, kr;
  f = kk + N - 1;
  do
  { f   = (f-1)/2;
    ll  = 2*f+1;
    kl  = B[ll];
    rr  = 2*f+2;
    kr  = B[rr];
    if( tt[kl] < tt[kr])  B[f] = B[ll];
    else                  B[f] = B[rr];
  }while(f!=0);
}
void IniciaCBT()		//8  ok
{ int f, r;
  for(r=N-1;  r<2*N-1;  r++)
  B[r] = r-N+1;   /*da a cada hoja lo que tendr'a siempre */
  /*=====================================================*/
  for(r=2*N-2;  r!=0;  r=r-2)
  { f = (r-1)/2;
    if( tt[B[r]] < tt[B[r-1]])  B[f] = B[r];
    else                        B[f] = B[r-1];
  }
}  
//fin_arbol_binario___fin_arbol_binario___fin_arbol_binario___fin_arbol_binario___
//fin del cuesco @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

void ChoqueW()  //choques con paredes
{ int svt,ri;
  double vx,vy,ww,vn,vt,dt;
  vx = Vx[knext];
  vy = Vy[knext];
  ww = w[knext];           //zzu
  switch(W[knext])
  {  case 1:{ vt  =  -vx -r1*ww;  //SUELO
              vn  =  -vy;
              ri  =  (int) reloj;
              dt  =  fabs(reloj-ri);
              if( fabs(vt/vn) < tanFi)
              {  Vy[knext] = sqrt(-Tb*log(drand48()));  //-rn*vy;	
                 Vx[knext] = ( (2-rt)*vx - (1+rt)*r1*ww)/3.0;
                 w[knext]  = ((1.0-2*rt)*ww - 2.0*(1+rt)*vx/r1)/3.0;
              }
              else
              {  svt = (int)(vt/fabs(vt));
                 Vy[knext] = sqrt(-Tb*log(drand48()));  //-rn*vy;	
                 Vx[knext] = vx - (1+rn)*kappa*vy*svt;
                 w[knext]  = ww - 2.0*(1+rn)*kappa*svt*vy/r1;// -
              }
              break;
            }
     case 3:{  vt = vx - r1*ww;  //TECHO
               vn = vy;
              if( fabs(vt/vn) < tanFi)
              {  Vy[knext] = -rn*vy;
                 Vx[knext] = ((2-rt)*vx + (1+rt)*r1*ww)/3.0;
                 w[knext]  = ((1-2*rt)*ww + 2*(1+rt)*vx/r1)/3.0;
              }
              else
              {  svt = (int) ( vt/fabs(vt));
                 Vy[knext] = -rn*vy;
                 Vx[knext] = vx - (1+rn)*kappa*svt*vy;
                 w[knext]  = ww + 2*(1+rn)*kappa*svt*vy/r1; //+
              }
              break;
            }

     case 2:{  vt = -vy - r1*ww; // DERECHA
               vn =  vx;
              if( fabs(vt/vn) < tanFi)
              {  Vy[knext] = ((2-rt)*vy-(1+rt)*r1*ww)/3.0;
                 Vx[knext] = -rn*vx;
                 w[knext]  = ((1-2*rt)*ww-2*(1+rt)*vy/r1)/3.0;
              }
              else
              {  svt = (int) (vt/fabs(vt));
                 Vy[knext] =  vy + (1+rn)*kappa*svt*vx;
                 Vx[knext] = -rn*vx;
                 w[knext]  = ww + 2*(1+rn)*kappa*svt*vx/r1;
              }
              break;
            }
     case 0:{  vt =  vy - r1*ww; // IZQUIERDA
               vn = -vx;
              if( fabs(vt/vn) < tanFi)
              {  Vy[knext] = ((2-rt)*vy+(1+rt)*r1*ww)/3.0;
                 Vx[knext] = -rn*vx;
                 w[knext]  = ((1-2*rt)*ww +2*(1+rt)*vy/r1)/3.0;
              }
              else
              {  svt = (int)(vt/fabs(vt));
                 Vy[knext] =  vy + (1+rn)*kappa*svt*vx;
                 Vx[knext] = -rn*vx;
                 w[knext]  = ww - 2*(1+rn)*kappa*svt*vx/r1;
              }
              break;
            }
  }
}
void NextEvent()		// para entendidos
{  int ca,jsoc,kind,vale,e;
   double dis,vy,ener,Ea,dE;
   do
   {  knext   =  B[0];
      vale    =  1;
      e       =  G[knext];
      tnext   =  Ta[knext][e];
      ca      =  cc[knext][e];
      jsoc    =  S[knext][e];  //necesario
      kind    =  K[knext][e];
      if(kind && ca!=C[jsoc])
      { vale = 0;
          Ta[knext][e] = infinite;
          Ganador(knext);
          UpGradeCBT(knext);
      }
   }while(vale==0);
   
   //printf(" tipo de choque = %d  (%d  %d) \n",kind,knext,jsoc);
   
#ifdef GRAFICO
   while(tF<tnext)
   {   Dibuja();
       tF += chico;
   }
#endif   
   
   reloj = tnext;
   if(kind)
   {  Mueve(knext);
      Mueve(jsoc);

     ChoqueDD(knext,jsoc); //aplica los C[a]++
     
     C[knext]++;
     C[jsoc]++;
     LimpiaLista(knext);
     EventosNuevos(knext);
     LimpiaLista(jsoc);
     EventosNuevos(jsoc);
     Ganador(knext);
     Ganador(jsoc);
     UpGradeCBT(knext);
     UpGradeCBT(jsoc);
   }
   else   //choque con pared
   { Mueve(knext);
     ChoqueW();
     LimpiaLista(knext);
     EventosNuevos(knext);  
     Ganador(knext);
     UpGradeCBT(knext);
   }
}


void XVinic() 	//muy bien probado	//11
{ int i,j,k,cc,na,nb,rho;
  double aa,r,h0,dist;
  reloj = 0.0;
  Vs    = 0.0;
  j     = 0;
  IMr   = 2.0/(M1*r1);
  tanFi = 3.0*(1.0+rn)*kappa/(1.0+rt);
  Eold  = 1e+20;
  //printf("  tan(phi) = %f\n",tanFi);
  r     = r1 + epsi;
  na    = (int)(Lx/(2.0*r));
  nb    = (int)((Lx/(2.0*r)) -1.0 );
  j     = 0;
  i     = 0;
  X[0]  = 0.5*Lx;
  Y[0]  = r1+epsi;     //<<<<<<<<<<<<<<<<<
  i++;
  j++;
  i=0;

  h0 = 2.0*r1 + epsi; //sqrt(3)*j*r + r1;
  r   = r1 + epsi;
  na  = (int)(Lx/(2.0*r));
  nb  = (int)((Lx/(2.0*r)) -1.0 );
  j=0;
  k=1;
  do
  { i=0;
    while(i<na && k<N)
    { X[k] = r + 2.0*r*i;
      Y[k] = epsi+(h0 + (sqrt(3)*j+1)*r);     //<<<<<<<<<<<<<<<<<
      if(Y[k]>Ly-r1)
      {  printf("La P%d no cabe!\n",k);
         //Cierre();
         exit(0);
      }
      i++;
      k++;
    }
    j++;
    i=0;
    while(i<nb && k<N)
    { X[k] = 2.0*r + 2.0*r*i;
      Y[k] = epsi+(h0 + (sqrt(3)*j+1)*r);   //<<<<<<<<<<<<<<<<<<<<<
      if(Y[k]>Ly-r1)
      {  printf("La P%d no cabe!\n",k);
         //Cierre();
         exit(0);
      }
     i++;
     k++;
    }
    j++;
  }while(k<N);
  printf(">   S A L I   con k=%d\n",k);
  //sortea velocidades  
  for(k=0; k<N; k++)       //fila eVeloc
  { Vx[k]  = sqrt(Tb)* (0.5 - drand48());
    Vy[k]  = sqrt(Tb)* (0.6 - drand48());
    T[k]   =  0.0;
    w[k]   = 0.5-drand48();
    if(Y[k]<0.2*Ly)  sprintf(col[k],"%s","green");
    if(Y[k]>0.2*Ly && Y[k]<0.4*Ly)  sprintf(col[k],"%s","green");
    if(Y[k]>0.3*Ly)  sprintf(col[k],"%s","blue");
  }
}  

double TchoqueDD(int k1, int j)  //tiempo choque discos k1 y j
{ double b,b2,Del,Rad,R2,x,y,gx,gy,g2,dtk,dtj;	//14
  dtk  = reloj - T[k1];
  dtj  = reloj - T[j];
  x   = (X[k1] + Vx[k1]*dtk)  - (X[j] + Vx[j]*dtj);
  y   = (Y[k1] + Vy[k1]*dtk - 0.5*dtk*dtk*g) - (Y[j] + Vy[j]*dtj - 0.5*dtj*dtj*g);
  gx  = Vx[k1] - Vx[j];
  gy  = (Vy[k1] - g*dtk) - (Vy[j] - g*dtj);
  b   = -x*gx  - y*gy;
  b2  = b*b;
  g2  = gx*gx + gy*gy;

  Rad = 2.0*r1;

  R2  = Rad*Rad;
  Del = b2-g2*(x*x+y*y-R2);
  if(b>0.0 && Del>0)
  {  return reloj + (b-sqrt(Del))/g2;
  }  
  else return infinite;
} 

//Ver SueloOsc.tex
double TchBase(int k)
{ double y,vy,vy2,v1,y2,v2,tp,t1,g2,au,ut;
  y   = Y[k];
  vy  = Vy[k];
  vy2 = vy*vy;
  ut  = 2.0*g*(r1-y);
    t1 = reloj + (vy + sqrt(vy2 -ut))/g ;
    v1 = vy - g*(t1-reloj);
    ut = v1-Vs;
    tp = (ut + sqrt(ut*ut  ))/g;
    return t1 + tp;   //este es el tiempo absoluto
}   

double TchoqueDW(int k) //tiempo paChocar contra pared	//15
{ int i,j,r,s,u;           //se revis'o que da res sensatos
  double x,y,vx,vy,VX,VY,t0,t1,t2,t_out;

    x   = X[k];
    y   = Y[k];
    vx  = Vx[k];
    vy  = Vy[k];
    VX  = vx/fabs(vx);
    VY  = vy/fabs(vy);
    r   = (1 + ((int) VX))/2;
    s   = (1 + ((int) VY))/2;
    u   = r + 2*s;
    switch(u) 
    { case 0 : { t1 = reloj - (x-r1)/vx;                     // con w_izq
                 t2 = TchBase(k); 
                 if(t1<t2) {t_out=t1; W[k] = 0; det=0;}
                 else      {t_out=t2; W[k] = 1; det=1;}
                 break;
               }
      case 1 : { t1 = reloj+ (Lx-r1-x)/vx;                    // con w_der
                 t2 = TchBase(k);  
                 if(t1<t2) {t_out=t1;  W[k] = 2; det=2;}
                 else      {t_out=t2;  W[k] = 1; det=3;}
                 break;
               }
      case 2 : { if(vy*vy>2.0*g*(Ly-r1-y))
                 { t1 = reloj -(x-r1)/vx;                     // con w_der
                   t2 = reloj+(vy-sqrt(vy*vy-2*g*(Ly-r1-y)))/g;// con w_top **
                   if(t1<t2) {t_out=t1;  W[k] = 0; det=4;}
                   else      {t_out=t2;  W[k] = 3; det=5;}
                 }
                 else
                 { t1 = reloj-(x-r1)/vx;	                 // con w_izq
                   t2 = TchBase(k); 
                   if(t1<t2) {t_out=t1; W[k] = 0; det=6;}
                   else      {t_out=t2; W[k] = 1; det=7;}
                 }
                 break;
               }
      case 3 : { if(vy*vy>2.0*g*(Ly-r1-y))         
                 { t1 = reloj+(Lx-r1-x)/vx;                    // con w_der
                   t2 = reloj+(vy-sqrt(vy*vy-2*g*(Ly-r1-y)))/g;// con w_top **
                   if(t1<t2) {t_out=t1;  W[k] = 2; det=8;}
                   else      {t_out=t2;  W[k] = 3; det=9;}
                 }
                 else
                 { t1 = reloj+(Lx-r1-x)/vx;                  // con w_der
                   t2 = TchBase(k); 
                   if(t1<t2) {t_out=t1;  W[k] = 2; det=10;}
                   else      {t_out=t2;  W[k] = 1; det=11;}
                 }
               }
    }      
    return t_out;             
} 

void EventosNuevos(int k1)	//16
{ int i,j,c,aa;
  double tj;
  for(j=0; j<N; j++)
  {  if(j!=k1) 
     {   tj = TchoqueDD(k1,j);
         c  = C[j];
         if(tj<0.1*infinite) 
         {  if(tj<reloj) 
            {  printf("Vienen problemas!!!!! (%d  %d)\n",k1,j); 
               ojo = 1;
            }
             //tj=reloj-(1e-8)*drand48();
            AgregaEvento(k1,j,1,c,tj);
         }
     }
  }
  tj = TchoqueDW(k1);
  if(tj<reloj) 
  {  printf("[%d  W%d](gulp)  se vienen problemas!!\n",k1,W[k1]);
     ojo = 1;
   }
  AgregaEvento(k1,W[k1],0,C[k1],tj); //1er 0=>W, 2do 0=>C[pared]
}

//+++++++++++++++++++++++Rutinas_fuera_del_cuesco+++++++++++++++++++++++

#ifdef GRAFICO
Tablero()
{  pl_parampl ("USE_DOUBLE_BUFFERING", "yes");
   pl_parampl ("BITMAPSIZE", "400x400");
   pl_handle = pl_newpl ("X", stdin, stdout, stderr);
   pl_selectpl (pl_handle);
   if (pl_openpl () < 0)     
   {   fprintf (stderr, "Couldn't open Plotter\n");
       return 1;
   }
   pl_fspace (0.0,0.0,Lx,Ly);
   pl_flinewidth (0.01);
   pl_filltype(1); 
}
Cierre()
{ int n;
  printf("poner entero:");
  //scanf("%ld",&n);
  for(s=0; s<2; s++) Dibuja;

  pl_closepl();
  printf("hicimos closepl \n");
  pl_selectpl(0);
  printf("hicimos selepl\n");
  pl_deletepl(pl_handle);
  printf("hicimos deletepl\n");
  exit(0);
}
#endif

void Inic()		//21
{ int i,u;
  double t_check;
  XVinic();
  //LeeXV();
  t_check = infinite;
  for(i=0; i<N; i++)
  { C[i] = 0;
    LimpiaLista(i);   //
    EventosNuevos(i);
    Ganador(i);
    if(tt[i]<t_check) 
    {  t_check = tt[i];
       u       = i;
    }
  }
  IniciaCBT(); 
  printf("Seg'un mi sondeo el protagonista es %d, segun CBT es %d\n",u,B[0]);
}

void Termaliza()
{ int i,cuenta,ri,num;
  double sumaEt,sumaEr,sumaEV,sumaE,sumaH;
  s      =  0;
  nF     =  0;
  num    =  0;
  hOld   =  20.0;
  sumaE  =  0.0;
  sumaEt =  0.0;
  sumaEr =  0.0;
  sumaEV =  0.0;
  
  sumaH  =  0.0;  
  cuenta = 0;
  printf("Termalizando con T = %f\n",Tb);
  do
  { NextEvent();
    if(s%cada==0) cuenta++;
    s++;
  }while(s<1000000);   
}


// ___________________________________main__________________________________________
main()
{ int i,j,cuenta,ka;
  double aa,sumaE,sumaH;


   ojo    = 0;
   tF     = 0.0;
   Tb     = 65.0;
   U      = 2*pi/Lx;
   reloj0 = 0.0;
   
   srand48(111);
         
   tanFi = 3.0*(1.0+rn)*kappa/(1.0+rt);

   printf("  >>>>>>>>>>>>>>>>  g = %g \n",g);   
   Inic();

#ifdef GRAFICO
   Tablero();
   Dibuja();
#endif   

   printf("-------- fin inicializacion --------:\n");
   printf("N=%d  Lx=%f Ly=%f r1=%f\n",N,Lx,Ly,r1);

     cuenta = 0;
   
   ka = 0;
   tot =1;
   
   Termaliza();


  if(ojo) printf("     Hubo problemas\n");
  else    printf("     No hubo problemas\n");

  //printf("ingrese numero entero:"); scanf("%ld",&s);
  printf("\n\n                           -- F I N --\n\n\n");

#ifdef GRAFICO
  Cierre();
#endif
}    
