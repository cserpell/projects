/* g_obj_so_down.i */

/* 
 * Predice colision de disco con objeto SUELO_OSCILANTE_DOWN, que genera dos tipos 
 * de subobjetos PVO_SO_DOWN pared virtual , y OBJ_SO_DOWN objeto suelo oscilante 
 */

#ifdef HD
#define gRadio(disco) Radio
#endif

#define T             Periodo
#define ALFA          (gy*GAMMA)
#define BIMINIMO(a,b) ((a<b)?a:b)
#define TRIMINIMO(a,b,c) ((a<b)?BIMINIMO(a,c):BIMINIMO(b,c))

void TmpoParedVirtualSueloOscilanteDOWN(int disco,double posicion, double velocidad, double *ptchoque)
{
    double raiz;
    raiz=velocidad*velocidad-2*gy*(posicion-S_amplitud_DOWN);
    *ptchoque=(-velocidad+sqrt(raiz))/gy;
}

int TmpoSueloOscilanteDOWN(int disco,double posicion,double velocidad,double *ptchoque)
{
    double phiaux;
    double tiempo1,tiempo2,tiempo3,tiempoA;

    phiaux=(TmpoSim+FASE_INICIAL_SUELO_OSCILANTE_DOWN)-T*(int)((TmpoSim+FASE_INICIAL_SUELO_OSCILANTE_DOWN)/T);

    if(posicion>PosicionSueloOscilanteDOWN(TmpoSim))
    {
        if(velocidad>=VelocidadSueloOscilanteDOWN(TmpoSim))
        {
            *ptchoque=0.0;
            return(1);
        }
        else posicion=PosicionSueloOscilanteDOWN(TmpoSim);
    }
    tiempo1=Tmpo123_DOWN(disco,phiaux,posicion,velocidad,S_amplitud_DOWN,0,+GAMMA,-1);
    tiempo2=Tmpo123_DOWN(disco,phiaux,posicion,velocidad,LargoY-BASE_SUELO_DOWN,T/2,-GAMMA,-1);
    tiempo3=Tmpo123_DOWN(disco,phiaux,posicion,velocidad,S_amplitud_DOWN,T,+GAMMA,-1);
    *ptchoque=TRIMINIMO(tiempo1,tiempo2,tiempo3);

#ifdef PARED_VIRTUAL_SUELO_OSCILANTE_DOWN
    if(velocidad<0.0) {
        tiempoA=TmpoA_DOWN(disco,posicion,velocidad);
        if (tiempoA<*ptchoque) return(0); else return(1);
    }
#endif
    return(1);

} /* TmpoSueloOscilanteDOWN() */

double TmpoA_DOWN(int disco,double posicion, double velocidad)
{
    double raiz,ta;
    raiz=velocidad*velocidad-2*gy*(posicion-S_amplitud_DOWN);

    if(raiz>0.0)
    {
        ta=(-velocidad+sqrt(raiz))/gy;
        if(ta>0.0) return(ta);
        else return(INFINITO);
    }
    else return(INFINITO);
}

/****************************************************************************
   to: phiaux (TmpoSim modulo el periodo)
   yo: posicion de la particula en TmpoSim [ y=yo+voy*(t-to)+g/2*(t-to)^2 ]
   voy: velocidad inicial particula
   To: desfasaje de la parabola. Valores: (0,T/2,T)
   signoGAMMA: signo*GAMMA
   signoRAIZ: que signo de la raiz escoger
   So: posicion suelo oscilante en instante To [ ys=So+signo Gamma g (t-To)^2 ].
       Valores: (S_amplitud_DOWN=LargoY-2A, LargoY,LargoY-2A)
*****************************************************************************/  
#define SEGUN_ROSA ((aux2+signoGAMMA*(To-to)+signoRAIZ*sqrt(raiz))/(signoGAMMA-1))

double Tmpo123_DOWN(int disco,double to,double yo,double voy,double So,double To,double signoGAMMA,double signoRAIZ)
{
    double aux1,aux2,t,raiz;

    aux1 = to-To;
    aux2 = (voy/gy);

    raiz = signoGAMMA*aux1*aux1+aux2*aux2
           -(2.0/gy)*((signoGAMMA-1)*So+signoGAMMA*(aux1*voy-yo)+yo);

    if(raiz>0) { t = SEGUN_ROSA; } else return(INFINITO);

    if ( (t>=0.0) && (t+to>=To-T/4) && (t+to<=To+T/4) && (t+to<=T) ) return(t);
    else return(INFINITO);
}

#undef SEGUN_ROSA

#undef BIMINIMO
#undef TRIMINIMO
#undef ALFA
#undef T

