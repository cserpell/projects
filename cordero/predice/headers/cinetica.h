/* cinetica.h */

#ifdef _cinetica_
#define ext
#else
#define ext extern
#endif

/* Posiciones actuales de los discos */
#ifdef GRAVEDADX
#define ActualPosX(disco) (PosX[disco]+(TmpoSim-TDisco[disco])*(VelX[disco]+medio_gx*(TmpoSim-TDisco[disco])))
#else
#define ActualPosX(disco) (PosX[disco]+VelX[disco]*(TmpoSim-TDisco[disco]))
#endif

#if defined(GRAVEDAD) || defined(GRAVEDADY)
#define ActualPosY(disco) (PosY[disco]+(TmpoSim-TDisco[disco])*(VelY[disco]+medio_gy*(TmpoSim-TDisco[disco])))
#else
#define ActualPosY(disco) (PosY[disco]+VelY[disco]*(TmpoSim-TDisco[disco]))
#endif 

/* Velocidades actuales de los discos. */
#if defined(GRAVEDADX)
#define ActualVelX(disco) (VelX[disco]+gx*(TmpoSim-TDisco[disco]))
#else
#define ActualVelX(disco) (VelX[disco])
#endif

#if defined(GRAVEDAD) || defined(GRAVEDADY)
#define ActualVelY(disco) (VelY[disco]+gy*(TmpoSim-TDisco[disco]))
#else
#define ActualVelY(disco) (VelY[disco])
#endif

#ifdef ROTACION
#define ActualVelWz(disco) (VelWz[disco])
#define ActualPosR(disco) (PosR[disco]+VelWz[disco]*(TmpoSim-TDisco[disco]))
#endif

extern void Evolucion(int disco);

ext double PosicionSueloOscilanteDOWN(double tiempo);
ext double VelocidadSueloOscilanteDOWN(double tiempo);
ext double PosicionSueloOscilanteUP(double tiempo);
ext double VelocidadSueloOscilanteUP(double tiempo);
ext double PosicionParedMovilHorizontal(double tiempo);
ext double VelocidadParedMovilHorizontal(double tiempo);
ext double PosicionParedMovilVertical(double tiempo);
ext double VelocidadParedMovilVertical(double tiempo);

#undef ext

