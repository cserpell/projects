/* predice.h */

#ifdef _predice_
#define ext
#else
#define ext extern
#endif

extern int TmpoColision(double *tiempo,int disco1, int disco2, int *evento );
extern int TmpoColisionImagen(double *tiempo, int disco1, int disco2, int *evento, int Corrx, int Corry );
extern int TmpoColision2(double *tiempo,int disco1,int disco2,int *evento,int Corrx,int Corry );
extern int TmpoColisionDadoEvento(double *tiempo,int disco1,int disco2,int *evento );
extern void TmpoPrd(double *tiempo,int disco,int *pared,int *evento,int celda);
extern void TmpoPrd1(double *tiempo,int disco,int *pared,int *evento,int celda);
extern int TmpoObjeto(double *tiempo,int disco,int *pared,int *evento,int celda);
extern int TmpoObjetoDadoTipo(double *tiempo,int disco,int *pared,int *evento,int tipo);

extern int TmpoSueloOscilanteDOWN(int disco,double posicion,double velocidad,double *ptchoque);
extern void TmpoParedSueloOscilanteDOWN(int disco,double posicion,double velocidad,double *ptchoque);
extern double TmpoA_DOWN(int disco,double posicion,double velocidad);
extern double Tmpo123_DOWN(int disco,double to,double yo,double voy,double So,double To,double signoGAMMA,double signoRAIZ);

extern int TmpoSueloOscilanteUP(int disco,double posicion,double velocidad,double *ptchoque);
extern void TmpoParedSueloOscilanteUP(int disco,double posicion,double velocidad,double *ptchoque);
extern double TmpoA_UP(int disco,double posicion,double velocidad);
extern double Tmpo123_UP(int disco,double to,double yo,double voy,double So,double To,double signoGAMMA,double signoRAIZ);

#undef ext


