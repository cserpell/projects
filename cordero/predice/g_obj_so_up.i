/* g_obj_so_up.i */

/* 
 * Predice colision de disco con objeto SUELO_OSCILANTE_UP, que genera dos tipos
 * de subobjetos PVO_SO_UP pared virtual , y OBJ_SO_UP objeto suelo oscilante 
 */

#ifdef HD
#define gRadio(disco) Radio
#endif

#define T             Periodo
#define ALFA          (gy*GAMMA)
#define BIMINIMO(a,b) ((a<b)?a:b)
#define TRIMINIMO(a,b,c) ((a<b)?BIMINIMO(a,c):BIMINIMO(b,c))

void TmpoParedVirtualSueloOscilanteUP(int disco,double posicion, double velocidad, double *ptchoque)
{
    double raiz;
    raiz=velocidad*velocidad-2*gy*(posicion-S_amplitud_UP);
    if ((raiz>0)  && (velocidad <0)) {     /* OJO. Interesa solo cuando entra */
        *ptchoque=(-velocidad-sqrt(raiz))/gy;
    } else *ptchoque=INFINITO; 
}

int TmpoSueloOscilanteUP(int disco,double posicion,double velocidad,double *ptchoque)
{
    double phiaux;
    double tiempo1,tiempo2,tiempo3,tiempoA;

    phiaux=(TmpoSim+FASE_INICIAL_SUELO_OSCILANTE_UP)-T*(int)((TmpoSim+FASE_INICIAL_SUELO_OSCILANTE_UP)/T);

    if(posicion<PosicionSueloOscilanteUP(TmpoSim))
    {
        if(velocidad<=VelocidadSueloOscilanteUP(TmpoSim))
        {
            *ptchoque=0.0;
            return(1);
        }
        else posicion=PosicionSueloOscilanteUP(TmpoSim);
    }
    tiempo1=Tmpo123_UP(disco,phiaux,posicion,velocidad,BASE_SUELO_UP+2*Amplitud,0,-GAMMA,1);
    tiempo2=Tmpo123_UP(disco,phiaux,posicion,velocidad,BASE_SUELO_UP,T/2,+GAMMA,1);
    tiempo3=Tmpo123_UP(disco,phiaux,posicion,velocidad,BASE_SUELO_UP+2*Amplitud,T,-GAMMA,1);
    *ptchoque = TRIMINIMO(tiempo1,tiempo2,tiempo3);

    return(1);

} /* TmpoSueloOscilanteUP() */


/****************************************************************************
   to: phiaux (TmpoSim modulo el periodo)
   yo: posicion de la particula en TmpoSim [ y=yo+voy*(t-to)+g/2*(t-to)^2 ]
   voy: velocidad inicial particula
   To: desfasaje de la parabola. Valores: (0,T/2,T)
   signoGAMMA: signo*GAMMA
   signoRAIZ: que signo de la raiz escoger
   So: posicion suelo oscilante en instante To [ ys=So+signo Gamma g (t-To)^2 ].
       Valores: (S_amplitud=2A,0,2A)
*****************************************************************************/  
#define SEGUN_ROSA ((aux2+signoGAMMA*(To-to)+signoRAIZ*sqrt(raiz))/(signoGAMMA-1))

double Tmpo123_UP(int disco,double to,double yo,double voy,double So,double To,double signoGAMMA,double signoRAIZ)
{
    double aux1,aux2,t,raiz;

    aux1 = to-To;
    aux2 = (voy/gy);

    raiz = signoGAMMA*aux1*aux1+aux2*aux2
           -(2.0/gy)*((signoGAMMA-1)*So+signoGAMMA*(aux1*voy-yo)+yo);

    if(raiz>0) { t = SEGUN_ROSA; } else return(INFINITO);

    if ( (t>=0.0) && (t+to>=To-T/4) && (t+to<=To+T/4) && (t+to<=T) ) return(t);
    else { return(INFINITO); }
}

#undef SEGUN_ROSA

#undef BIMINIMO
#undef TRIMINIMO
#undef ALFA
#undef T

