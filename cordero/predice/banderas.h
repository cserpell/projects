/* banderas de compilacion */
#define VERSION 1.18

/* tipo de particula: definir solo una */
#define HD
#undef  HDBM
#undef  SWD
#undef  SWDBM

/* HD or HDBM con disipacion */
#undef  ARENA  /* Reglas de colision disipativas */
#undef  RANDOM_REST_COEF
#undef  GRANULAR_BINARIO
#undef  IHS_REALISTA_NORMAL      /* working OK */
#undef  IHS_REALISTA_TANGENCIAL  /* working OK */
#undef  DEBUG_IHS_REALISTA       /* t, qn(t), qt(t) al stdout  */
#undef  TEST_COLAPSO             /* t, ncol al stdout  */
#define COTA_V2_CDD              /* cota para hacer colisiones elasticas */
#define COTA_V2_CDP              /* cota para hacer colisiones elasticas */

#define ROTACION
#undef  DEBUG_ROTACION
#undef  DEBUG_ROTACION_PARED

/* obstaculo */
#undef  CON_OBJETO
#undef  SUELO_OSCILANTE_DOWN
#undef  SUELO_OSCILANTE_UP
#undef  OBJETO_PARED_DURA
#undef  OBJETO_LINEA_VERTICAL
#undef  OBJETO_LINEA_HORIZONTAL

#ifdef  SUELO_OSCILANTE_DOWN
#define PARED_VIRTUAL_SUELO_OSCILANTE_DOWN
#define BASE_SUELO_DOWN 0.0 
#define FASE_INICIAL_SUELO_OSCILANTE_DOWN (0.25*Periodo)
#endif

#ifdef  SUELO_OSCILANTE_UP
#define PARED_VIRTUAL_SUELO_OSCILANTE_UP
#define BASE_SUELO_UP 4.0 
#define FASE_INICIAL_SUELO_OSCILANTE_UP (0.75*Periodo)
/* #define DELTA_H  1.0 *//* <--- no esta listo todavia */
#endif

/* formato de data */
#ifdef  HD
#define DATA_HD
#endif

#ifdef  HDBM 
#define DATA_HDBM
#endif

#ifdef  SWDBM
#undef  DATA_SWDBM
#endif

/* campo externo */
#undef  GRAVEDAD
#undef  GRAVEDADX
#undef  GRAVEDADY
#undef  GRAVEDAD_MODULO_ANGULO

/* condiciones de borde */

/* si hay o no periodicidad y en que direccion */
#undef  CBPHorz
#undef  CBPVert

/* tipo de condicion de borde (se usa: modelo/choques/general_dp.i) */
#define GENERAL_BC

#undef  PARED_UP_DISIPATIVA_ROTACION
#undef  PARED_DOWN_DISIPATIVA_ROTACION
#define PARED_LEFT_DISIPATIVA_ROTACION
#define PARED_RIGHT_DISIPATIVA_ROTACION

#undef  PARED_UP_ELASTICA
#undef  PARED_DOWN_ELASTICA
#undef  PARED_LEFT_ELASTICA
#undef  PARED_RIGHT_ELASTICA

#undef  PARED_UP_REFLEXION_ELASTICA
#undef  PARED_DOWN_REFLEXION_ELASTICA
#undef  PARED_LEFT_REFLEXION_ELASTICA
#undef  PARED_RIGHT_REFLEXION_ELASTICA

#undef  PARED_UP_REFLEXION_TERMICA
#undef  PARED_DOWN_REFLEXION_TERMICA
#undef  PARED_LEFT_REFLEXION_TERMICA
#undef  PARED_RIGHT_REFLEXION_TERMICA

#undef  PARED_UP_ANTIREFLEXION_TERMICA
#undef  PARED_DOWN_ANTIREFLEXION_TERMICA
#undef  PARED_LEFT_ANTIREFLEXION_TERMICA
#undef  PARED_RIGHT_ANTIREFLEXION_TERMICA

#undef  PARED_UP_TERMICA_SIN_MEMORIA
#undef  PARED_DOWN_TERMICA_SIN_MEMORIA
#undef  PARED_LEFT_TERMICA_SIN_MEMORIA
#undef  PARED_RIGHT_TERMICA_SIN_MEMORIA

#undef  PARED_UP_ALTAFRECUENCIA
#undef  PARED_DOWN_ALTAFRECUENCIA

#undef  PARED_UP_COMPLETE_THERMAL_PLUS_SHEAR
#undef  PARED_DOWN_COMPLETE_THERMAL_PLUS_SHEAR

#undef  PARED_UP_NORMAL_THERMAL_PLUS_SHEAR
#undef  PARED_DOWN_NORMAL_THERMAL_PLUS_SHEAR

#undef  PARED_UP_COMPLETE_THERMAL_PLUS_SHEAR_OSCILLATION
#undef  PARED_DOWN_COMPLETE_THERMAL_PLUS_SHEAR_OSCILLATION

#undef  PARED_UP_ALTAFRECUENCIA_SHEAR_OSCILLATION
#undef  PARED_DOWN_ALTAFRECUENCIA_SHEAR_OSCILLATION

#define PARED_DOWN_DIENTESIERRA_ALTAFRECUENCIA
#define PARED_UP_DIENTESIERRA_ALTAFRECUENCIA

/* condiciones iniciales */

/*posiciones */
#undef  HOMOGENEO_POSICIONES
#define PACKED_POSICIONES
#undef  TRIANGULAR_POSICIONES
#undef  GOTA_POSICIONES
#undef  GOTA_VAPOR

/* velocidad */
#undef  HOMOGENEO_TEMPERATURA
#undef  GRADIENTE_TEMPERATURA
#undef  VORTICIDAD
#define KFIJO_VCM_NULA

/* animacion */
#undef  ANIMACION
#undef  ANI_XY
#undef  ANI_XY_LIGADO
#undef  ANI_XY_TIPO_CRISTAL

/* grafico DJGPP */
#define GR_DJGPP
/* 975x725 875x416 640x480 etc. */
#define GR_ANCHO  700 /* 640 975 1018 975 */
#define GR_ALTO   480  /* 480 */
#define GR_PROFUNDIDAD 32

#if defined(SUELO_OSCILANTE_DOWN) || defined(SUELO_OSCILANTE_UP)  
#define DRAW_VIRTUAL_BOUNDARY
#endif

#define DRAW_LINEA_HORIZONTAL  
#define DRAW_LINEA_VERTICAL  
#define DIBUJA_CELDAS_SIMULADOR
#define DIBUJA_CELDAS_MEDICION

#define CAMBIA_COLOR_VERTICAL
#define NUMERO_NIVELES_COLOR  10 
#undef  NUMERACION
#define INGLES
#define INCREMENTO_AMPLITUD   1.1
#define DECREMENTO_AMPLITUD   0.9
#define INCREMENTO_LINEA_HORIZONTAL  1.01
#define DECREMENTO_LINEA_HORIZONTAL  0.99
#define INCREMENTO_LINEA_VERTICAL  1.01
#define DECREMENTO_LINEA_VERTICAL  0.99
#define INCREMENTO_FRECUENCIA 1.1
#define DECREMENTO_FRECUENCIA 0.9
#define INCREMENTO_GRAVEDAD   1.1
#define DECREMENTO_GRAVEDAD   0.9
#define INCREMENTO_ANGULO_GRAVEDAD    1.01
#define DECREMENTO_ANGULO_GRAVEDAD    0.99
#define INCREMENTO_MASA       1.1
#define DECREMENTO_MASA       0.9
#define INCREMENTO_DISIPACION 1.1
#define DECREMENTO_DISIPACION 0.9
#define INCREMENTO_FRICCION   1.1
#define DECREMENTO_FRICCION   0.9
#define INCREMENTO_GAMMA      1.1
#define DECREMENTO_GAMMA      0.9
#define INCREMENTO_ZETA       1.1
#define DECREMENTO_ZETA       0.9

/* Mediciones */
#undef  MIDE_CLUSTERS  	/* Banderas especificas en clusters.h */
#undef  MIDE_NESS	/* Banderas especificas en mideness.h */
#undef  AUTOCORR_VEL  /* Autocorrelacion de vels en regimenes no homogeneos */
#undef  MIDE_RING  /* Detecta y mide distintos tipos de diagramas Ring */
#undef  PERTURBATIVO_Q
#undef  CORR_VELS_CONTACTO /* Sin hacer el NESS */
#undef  MIDE_CORRPARES

#undef  DISTRVEL
#undef  DISTRVEL_PARED /* Distribucion de velocidades incidentes y reflejadas en las paredes */

#undef  MEDGLOBAL

#undef  HISTORIA_INTRUSO  /* Mide la posicion y velocidad del intruso en segregacion */

#undef  MIDE_CONVECCION   /* Mide observables asociados a la conveccion */
#define MIDE_CM		  /* Mide CM sistema */
#define VELOC_CM          /* Mide Velocidad CM cuando CM esta definido */
#undef  VELOC_INTRUDER    /* Mide solo la velocidad del intruso */
#define CADA_N_CICLOS 1   /* Guarda Config y Data cada X ciclos */
#undef  STOP_SIMULA       /* detiene simulacion cuando intruso esta arriba */
#define FOTO_SIMPLE       /* Guarda fotos solo con las posiciones, sino, tb las velocidades*/
#undef  FOTO_CON_ROTACION /* Guarda foto con velocidad rotacion si correspnde */
#undef  USR_FOTO_UNIX_STDOUT

/* Mediciones de campos en celdas */
#define MIDE_CELDAS
#undef  MIDE_CELDAS_TIEMPO
#undef  VERBOSE

/******* TEST A FRECUENCIA NULA ********
#undef  TEST_CELDAS_TIEMPO 
****************************************/

/* Debugging */
#define DEBUG_ADMC
#undef  DEBUG_CI
#undef  DEBUG_PREDICCION

#define DENSITY_REF 0.4
#undef  OTRA_REGLA_PREDICCION
#define ERROR_TMPO_SIM 1.0e-05
#define PARCHE_COLAPSO_ESQUINA
#define ERROR_DIAMETRO 1.0e-05
#undef  SIGUE_NO_MAS /* no hacer caso de diametros y tiempos negativos */
#undef  CLUSTERUBB

#define TIEMPOTESTB  0           
#define TIEMPOTESTA  INFINITO
#define DISCOPINTADO 0
#define PARTNERPINTADO 0
#define MENSAJE(disco,accion)  if((TmpoSim>=TIEMPOTESTA)&&(TmpoSim<TIEMPOTESTB)&&(disco==DISCOPINTADO)) accion

