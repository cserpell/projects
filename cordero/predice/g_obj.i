/* g_obj.i */

/*
 * Predice colision de disco con objetos.
 * OBJETO SUELO_OSCILANTE_DOWN:
 *       genera dos tipos de subobjetos PVO_SO_DOWN (pared virtual) y
 *       OBJ_SO_DOWN (objeto suelo oscilante). Las predicciones de los
 *       tiempos correspondientes al SUELO_OSCILANTE_DOWN estan en g_obj_so_down.i,
 * OBJETO SUELO_OSCILANTE_UP:
 *       genera dos tipos de subobjetos PVO_SO_UP (pared virtual) y
 *       OBJ_SO_UP (objeto suelo oscilante). Las predicciones de los
 *       tiempos correspondientes al SUELO_OSCILANTE_UP estan en g_obj_so_up.i,
 */

#define S_amplitud_DOWN (LargoY-2.0*Amplitud-BASE_SUELO_DOWN)
#define S_amplitud_UP (2.0*Amplitud+BASE_SUELO_UP)

#ifdef SUELO_OSCILANTE_DOWN
#include "g_obj_so_down.i"
#endif
#ifdef SUELO_OSCILANTE_UP
#include "g_obj_so_up.i"
#endif

int TmpoObjeto(double *tiempo,int disco,int *pared,int *evento,int celda)
{
    double Yaux,Vaux;
    double tcolisi,taux;
    int obj;

    *tiempo=INFINITO;
    tcolisi=INFINITO;
    taux=INFINITO;

#ifdef SUELO_OSCILANTE_DOWN
    Yaux=ActualPosY(disco)+gRadio(disco);
    Vaux=ActualVelY(disco);

#ifdef PARED_VIRTUAL_SUELO_OSCILANTE_DOWN
    if(Yaux<S_amplitud_DOWN)
    {
        TmpoParedVirtualSueloOscilanteDOWN(disco,Yaux,Vaux,&taux);
        *tiempo=taux+TmpoSim;
        *evento=EventoCDO;
        *pared=PVO_SO_DOWN;
    }
    else
#endif
    {
        if(TmpoSueloOscilanteDOWN(disco,Yaux,Vaux,&taux))
        {
            *tiempo=taux+TmpoSim;
            *evento=EventoCDO;
            *pared=OBJ_SO_DOWN;
        }
#ifdef PARED_VIRTUAL_SUELO_OSCILANTE_DOWN
        else
        {
            TmpoParedVirtualSueloOscilanteDOWN(disco,Yaux,Vaux,&taux);
            *tiempo=taux+TmpoSim;
            *evento=EventoCDO;
            *pared=PVO_SO_DOWN;
        }
#endif
    }
    tcolisi=taux;
#endif

#ifdef SUELO_OSCILANTE_UP
    Yaux=ActualPosY(disco)-gRadio(disco);
    Vaux=ActualVelY(disco);

#ifdef PARED_VIRTUAL_SUELO_OSCILANTE_UP
    if(Yaux>S_amplitud_UP)
    {
        TmpoParedVirtualSueloOscilanteUP(disco,Yaux,Vaux,&taux);
        if (taux < tcolisi) {
            *tiempo=taux+TmpoSim;
            *evento=EventoCDO;
            *pared=PVO_SO_UP;
            tcolisi=taux;
        }
    }
    else
#endif
    {
        if(TmpoSueloOscilanteUP(disco,Yaux,Vaux,&taux))
        {
            if (taux < tcolisi) {
                *tiempo=taux+TmpoSim;
                *evento=EventoCDO;
                *pared=OBJ_SO_UP;
                tcolisi=taux;
            }
        }
/* NO SE REQUIERE 
#ifdef PARED_VIRTUAL_SUELO_OSCILANTE_UP
        else
        {
            TmpoParedVirtualSueloOscilanteUP(disco,Yaux,Vaux,&taux);
            if (taux < tcolisi) {
                *tiempo=taux+TmpoSim;
                *evento=EventoCDO;
                *pared=PVO_SO_UP;
                tcolisi=taux;
            }
        }
#endif
*/
    }
#endif

    if (*tiempo <INFINITO) return(1); else return(0);

} /* TmpoObjeto() */

int TmpoObjetoDadoTipo(double *tiempo,int disco,int *pared,int *evento,int tipo)
{
    double Yaux,Vaux;
    double tcolisi,taux;
    int obj;

    *tiempo=INFINITO;

    switch(tipo)
    {
#ifdef SUELO_OSCILANTE_DOWN

        case PVO_SO_DOWN:

            Yaux=ActualPosY(disco)+gRadio(disco);
            Vaux=ActualVelY(disco);
            if(TmpoSueloOscilanteDOWN(disco,Yaux,Vaux,&tcolisi))
            {
                *tiempo=tcolisi+TmpoSim;
                *evento=EventoCDO;
                *pared=OBJ_SO_DOWN;
            }
            return(1);
            break;

        case OBJ_SO_DOWN:

            Yaux=ActualPosY(disco)+gRadio(disco);
            Vaux=ActualVelY(disco);
            if(TmpoSueloOscilanteDOWN(disco,Yaux,Vaux,&tcolisi))
            {
                *tiempo=tcolisi+TmpoSim;
                *evento=EventoCDO;
                *pared=OBJ_SO_DOWN;
                return(1);
            }
#ifdef PARED_VIRTUAL_SUELO_OSCILANTE_DOWN
            else
            {
                TmpoParedVirtualSueloOscilanteDOWN(disco,Yaux,Vaux,&tcolisi);
                *tiempo=tcolisi+TmpoSim;
                *evento=EventoCDO;
                *pared=PVO_SO_DOWN;
                return(1);
            }
#endif
            break;
#endif

#ifdef SUELO_OSCILANTE_UP

        case PVO_SO_UP:

            Yaux=ActualPosY(disco)-gRadio(disco);
            Vaux=ActualVelY(disco);
            if(TmpoSueloOscilanteUP(disco,Yaux,Vaux,&tcolisi))
            {
                *tiempo=tcolisi+TmpoSim;
                *evento=EventoCDO;
                *pared=OBJ_SO_UP;
            }
            return(1);
            break;

        case OBJ_SO_UP:

            Yaux=ActualPosY(disco)-gRadio(disco);
            Vaux=ActualVelY(disco);
            if(TmpoSueloOscilanteUP(disco,Yaux,Vaux,&tcolisi))
            {
                *tiempo=tcolisi+TmpoSim;
                *evento=EventoCDO;
                *pared=OBJ_SO_UP;
                return(1);
            }
#ifdef PARED_VIRTUAL_SUELO_OSCILANTE_UP
            else
            {
                TmpoParedVirtualSueloOscilanteUP(disco,Yaux,Vaux,&tcolisi);
                *tiempo=tcolisi+TmpoSim;
                *evento=EventoCDO;
                *pared=PVO_SO_UP;
                return(1);
            }
#endif
            break;
#endif
   }

} /* TmpoObjetoDadoEvento() */

