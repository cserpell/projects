/* s_oscila.i */

/* Dos funciones que dan  la posicion del suelo y la velocidad  del suelo. */
/* en un tiempo */

#define T Periodo 

double PosicionSueloOscilanteDOWN(double tiempo)
{
#ifdef SUELO_OSCILANTE_DOWN
    static double phiaux, Yaux;
    phiaux=tiempo+FASE_INICIAL_SUELO_OSCILANTE_DOWN-T*(int)((tiempo+FASE_INICIAL_SUELO_OSCILANTE_DOWN)/T);

    if(phiaux<0.25*T)
    {
        Yaux=LargoY-2.0*Amplitud-BASE_SUELO_DOWN+gy*GAMMA/2.0*phiaux*phiaux;
        return(Yaux);
    }
    else if(phiaux<0.75*T)
    {
        Yaux=LargoY-BASE_SUELO_DOWN-gy*GAMMA/2.0*(phiaux-T/2.0)*(phiaux-T/2.0);
        return(Yaux);
    }
    else
    {
        Yaux=LargoY-2.0*Amplitud-BASE_SUELO_DOWN+gy*GAMMA/2.0*(phiaux-T)*(phiaux-T);
        return(Yaux);
    }
#endif
} /* PosicionSueloOscilanteDOWN() */

double VelocidadSueloOscilanteDOWN(double tiempo)
{
#ifdef SUELO_OSCILANTE_DOWN
    static double phiaux, Vaux;
    phiaux=tiempo+FASE_INICIAL_SUELO_OSCILANTE_DOWN-T*(int)((tiempo+FASE_INICIAL_SUELO_OSCILANTE_DOWN)/T);

    if(phiaux<0.25*T)
    {
        Vaux=gy*GAMMA*phiaux;
        return(Vaux);
    }
    else if(phiaux<0.75*T)
    {
        Vaux=-gy*GAMMA*(phiaux-T/2.0);
        return(Vaux);
    }
    else
    {
        Vaux=gy*GAMMA*(phiaux-T);
        return(Vaux);
    }
#endif
} /* VelocidadSueloOscilanteDOWN() */


double PosicionSueloOscilanteUP(double tiempo)
{
#ifdef SUELO_OSCILANTE_UP
    static double phiaux, Yaux;
    phiaux=tiempo+FASE_INICIAL_SUELO_OSCILANTE_UP-T*(int)((tiempo+FASE_INICIAL_SUELO_OSCILANTE_UP)/T);

    if(phiaux<0.25*T)
    {
        Yaux=BASE_SUELO_UP+2*Amplitud-gy*GAMMA/2.0*phiaux*phiaux;
        return(Yaux);
    }
    else if(phiaux<0.75*T)
    {
        Yaux=BASE_SUELO_UP+gy*GAMMA/2.0*(phiaux-T/2.0)*(phiaux-T/2.0);
        return(Yaux);
    }
    else
    {
        Yaux=BASE_SUELO_UP+2*Amplitud-gy*GAMMA/2.0*(phiaux-T)*(phiaux-T);
        return(Yaux);
    }
#endif
} /* PosicionSueloOscilanteUP() */

double VelocidadSueloOscilanteUP(double tiempo)
{
#ifdef SUELO_OSCILANTE_UP
    static double phiaux, Vaux;
    phiaux=tiempo+FASE_INICIAL_SUELO_OSCILANTE_UP-T*(int)((tiempo+FASE_INICIAL_SUELO_OSCILANTE_UP)/T);

    if(phiaux<0.25*T)
    {
        Vaux=-gy*GAMMA*phiaux;
        return(Vaux);
    }
    else if(phiaux<0.75*T)
    {
        Vaux=gy*GAMMA*(phiaux-T/2.0);
        return(Vaux);
    }
    else
    {
        Vaux=-gy*GAMMA*(phiaux-T);
        return(Vaux);
    }
#endif
} /* VelocidadSueloOscilanteUP() */

#undef T
