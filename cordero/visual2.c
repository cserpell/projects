/* Cristian Serpell 2005 - 2006 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <plot.h>

int N;
double Lx, Ly, Lz;

double TPiso, TTecho; // Periodos de cada uno.. mas tarde son iguales
double AmpTecho, CteTecho, AmpPiso, CtePiso; // Cte = 16*Amp/T^2
double reloj, tF, CteReloj;
int    knext, cantmovreloj;

#define TPant   400   // Tamaño de la pantalla en pixeles

double CamX, CamY, CamZ, CamVx, CamVy, CamVz;    /* pos camara y donde apunta */
double CamYx, CamYy, CamYz, CamXx, CamXy, CamXz; /* v. unit. de la pantalla */
double CamD; /* distancia del observador a la pantalla */
double p_medio;
int pl_handle;
int p_tamano;

FILE *camfile;
int sicamfile;
double nextcamevent;

int IniciarPantalla(int tamano)
{
    char s[20];
    
    sprintf(s, "%dx%d", tamano, tamano);
    pl_parampl("USE_DOUBLE_BUFFERING", "yes");
    pl_parampl("BITMAPSIZE", s);
    pl_handle = pl_newpl("X", stdin, stdout, stderr);
    pl_selectpl(pl_handle);
    if(pl_openpl() < 0)     
    {
        fprintf(stderr, "No se pudo abrir ventana\n");
        return 1;
    }
    pl_fspace(0.0, 0.0, tamano, tamano);
    pl_flinewidth(0.01);
    pl_filltype(0); /* 0 = transparente, 1 = lleno */
    p_tamano = tamano;
    p_medio = tamano/2.;
    return 0;
}

void IniciarCamara(double cx, double cy, double cz, double vx, double vy, double vz, double d)
{
    double r;
    
    CamX = cx;
    CamY = cy;
    CamZ = cz;
    r = sqrt(vx*vx + vy*vy + vz*vz);
    CamVx = vx/r;
    CamVy = vy/r;
    CamVz = vz/r;
    CamD = d;
    CamYx = -CamVy*CamVx;
    CamYy = 1. - CamVy*CamVy;
    CamYz = -CamVy*CamVz;
    r = sqrt(CamYx*CamYx + CamYy*CamYy + CamYz*CamYz);
    CamYx = CamYx/r;
    CamYy = CamYy/r;
    CamYz = CamYz/r;
    CamXx = -CamVz/r;
    CamXy = 0.;
    CamXz = CamVx/r;
}

void CalcProyR(double *px, double *py, double *r, double x, double y, double z)
{
    double den;  // Lo hace sin restar pos de camara

    den = CamD/(x*CamVx + y*CamVy + z*CamVz);
    *px = den*(x*CamXx + y*CamXy + z*CamXz) + p_medio;
    *py = den*(x*CamYx + y*CamYy + z*CamYz) + p_medio;
    *r = CamD/sqrt(x*x + y*y + z*z);
}

void CalcProy(double *px, double *py, double x, double y, double z)
{
    double den;
    
    x -= CamX;
    y -= CamY;
    z -= CamZ;
    den = CamD/(x*CamVx + y*CamVy + z*CamVz);
    *px = den*(x*CamXx + y*CamXy + z*CamXz) + p_medio;
    *py = den*(x*CamYx + y*CamYy + z*CamYz) + p_medio;
}

void DibujarEjes(double PP, double PT)
{
    double px[8], py[8];

    CalcProy(&px[0], &py[0], 0., PT, 0.);
    CalcProy(&px[1], &py[1], 0., PP, 0.);
    CalcProy(&px[2], &py[2], Lx, PT, 0.);
    CalcProy(&px[3], &py[3], Lx, PP, 0.);
    CalcProy(&px[4], &py[4], Lx, PT, Lz);
    CalcProy(&px[5], &py[5], Lx, PP, Lz);
    CalcProy(&px[6], &py[6], 0., PT, Lz);
    CalcProy(&px[7], &py[7], 0., PP, Lz);
    pl_fline(px[0], py[0], px[1], py[1]);
    pl_fline(px[0], py[0], px[2], py[2]);
    pl_fline(px[0], py[0], px[6], py[6]);
    pl_fline(px[3], py[3], px[1], py[1]);
    pl_fline(px[3], py[3], px[2], py[2]);
    pl_fline(px[3], py[3], px[5], py[5]);
    pl_fline(px[4], py[4], px[2], py[2]);
    pl_fline(px[4], py[4], px[5], py[5]);
    pl_fline(px[4], py[4], px[6], py[6]);
    pl_fline(px[7], py[7], px[1], py[1]);
    pl_fline(px[7], py[7], px[6], py[6]);
    pl_fline(px[7], py[7], px[5], py[5]);
}

void DibujarPantalla()
{ 
    int k;
    double t, x, y, z, px, py, r, pp, pt, xa, ya, za;
    
    pl_erase();
    pt = Techo(tF);
    pp = Piso(tF);
    DibujarEjes(pp, pt);
    for(k = 0; k < N; k++)
    {
        t = tF - Tpo[k];
        x = PosX[k] + VelX[k]*t - CamX;
        y = PosY[k] + (VelY[k] - g2*t)*t - CamY;
        z = PosZ[k] + VelZ[k]*t - CamZ;
        CalcProyR(&px, &py, &r, x, y, z);
        r *= Radio[k];
        pl_fillcolorname(Col[k]);
        pl_fcircle(px, py, r);
        CalcRot(&xa, &ya, &za, k, t);
        x += xa*Radio[k];
        y += ya*Radio[k];
        z += za*Radio[k];
        CalcProyR(&px, &py, &r, x, y, z);
        r *= 0.3*Radio[k];
        pl_fillcolorname("black");  
        pl_fcircle(px, py, r);
    }
}

void CerrarPantalla()
{
    pl_closepl();
    pl_selectpl(0);
    pl_deletepl(pl_handle);
}

void IniciarArchCam(char *nom)
{
    camfile = fopen(nom, "r");
    if(camfile == NULL)
    {
        fprintf(stderr, "No se pudo abrir archivo de camara\n");
        sicamfile = 0;
        return; /* error */
    }
    sicamfile = 1;
}

double LeerNum()
{
    char linea[256];
    int largo = 0;
    int c;
    
    while(1)
    {
        c = fgetc(camfile);
        if(c == EOF || largo == 256)
            return infinite;
        if(c == '\t' || c == '\n' || c == ' ')
        {
            linea[largo] = 0;
            break;
        }
        linea[largo] = c;
        largo++;
    }
    return atof(linea);
}

void LeerSiguienteEvCam()
{
    double cx, cy, cz, vx, vy, vz, d;
    
    cx = LeerNum();
    cy = LeerNum();
    cz = LeerNum();
    vx = LeerNum();
    vy = LeerNum();
    vz = LeerNum();
    d = LeerNum();
    nextcamevent = LeerNum() - cantmovreloj*CteReloj;
    IniciarCamara(cx, cy, cz, vx, vy, vz, d);
}

FILE *arch;

int IniciarArchivo(char *nombrearchivo)
{
    arch = fopen(nombrearchivo, "r");
    if(arch == NULL)
    {
        fprintf(stderr, "No se pudo abrir archivo de entrada\n");
        return 1; // error
    }
    fprintf(arch, "Parametros:\n");
    fprintf(arch, "N = %d\tLx = %f\tLy = %f\tLz = %f\tr1 = %f\tVARRADIO = %f\n", N, Lx, Ly, Lz, r1, VARRADIO);
    fprintf(arch, "rn = %f\trt = %f\tkappa = %f\tg = %f\tCOTAELAS = %f\n", rn, rt, kappa, g, COTAELAST);
    fprintf(arch, "A = %f\tT = %f\n", A, T);
    return 0;
}

void GrabarTodo()
{ 
    int k;
    double t, x, y, z, xa, ya, za;
    
    fprintf(arch, "techo = %f, piso = %f\n", Techo(tF), Piso(tF));
    for(k = 0; k < N; k++)
    {
        t = tF - Tpo[k];
        x = PosX[k] + VelX[k]*t;
        y = PosY[k] + (VelY[k] - g2*t)*t;
        z = PosZ[k] + VelZ[k]*t;
        CalcRot(&xa, &ya, &za, k, t);
        fprintf(arch, "%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", k, tF, x, y, z, xa, ya, za, Radio[k]);
    }
}

void CerrarArchivo()
{
    fclose(arch);
}

void NextEvent()		// para entendidos
{
    int jsoc, vale, e, kind, k, l;
    double tnext;
    
    do
    {
        tnext = TpoE[knext][e];
        jsoc  = Socio[knext][e];  //necesario
        kind  = Kind[knext][e];
        if(kind && CC[knext][e] != Choques[jsoc])
        {
            vale = 0;
            TpoE[knext][e] = infinite;
            Ganador(knext);
            UpGradeCBT(knext);
        }
    } while(vale == 0);
    
    while(tF < tnext)
    {
        if(sicamfile && tF > nextcamevent)
            LeerSiguienteEvCam();
        DibujarPantalla();
        GrabarTodo();
        tF += DELTAPANT;
    }
   
    reloj = tnext;
    if(reloj > CteReloj)
    {
        for(k = 0; k < N; k++)
        {
            Tpo[k] -= CteReloj;
            TpoSE[k] -= CteReloj;
            for(l = 0; l < Ll; l++)
                TpoE[k][l] -= CteReloj;
        }
        reloj -= CteReloj;
        tF -= CteReloj;
        if(sicamfile)
            nextcamevent -= CteReloj;
        cantmovreloj++;
    }
}

void Termaliza()
{
    int s;
    
    s = 0;
    printf("Termalizando con T = %f\n", Tb);
    do
    {
        if((!sicamfile) && s%cada == 0)
        {
            double c = 4.*(s%(N*N))*Lx/(N*N);
            
            IniciarCamara(2.*Lx - c, Ly, -3.*Lz, c - 1.5*Lx, -0.5*Ly, 3.5*Lz, 28.*Lz);
        }
        s++;
    } while(s < 20000000);   /* LOOP PRINCIPAL */
}

int main(int argc, char *argv[])
{
    Tb     = 65.0;
    srand48(111);
    Inic();

    sicamfile = 0;
    IniciarPantalla(TPant);
    if(argc > 1)
        IniciarArchCam(argv[1]);
    if(sicamfile)
        LeerSiguienteEvCam();
    else
        IniciarCamara(Lx, Ly/2, -Lz/2, -0.5, 0, 1, 8*Lz);
    DibujarPantalla();
    IniciarArchivo("out.txt");
    GrabarTodo();

    printf("-------- fin inicializacion --------:\n");
    printf("N = %d\tLx = %f\tLy = %f\tLz = %f\nr1 = %f\tVARRADIO = %f\n", N, Lx, Ly, Lz, r1, VARRADIO);
    printf("rn = %f\trt = %f\tkappa = %f\ng = %f\tCOTAELAS = %f\n", rn, rt, kappa, g, COTAELAST);
    printf("A = %f\tT = %f\n", A, T);
    
    Termaliza();

    printf("\n\n                           -- F I N --\n\n\n");

    CerrarPantalla();
    CerrarArchivo();
    return 0;
}

