/* Por ahora el choque con la base es termico
    comentarios Cristian Serpell 2005 - 2006
   22 / 11  Comienzo nueva version
    Decidi dejar los arreglos en vez de crear una estructura 'Particula'
    para que siga la misma linea, ademas que el codigo de Risso usa
    esa manera de guardar los datos tambien.
    Cambio el nombre de las variables a nombres mas entendibles.
    Elimino variables no usadas y ordeno un poco el codigo para
    luego hacer modificaciones para 3d y dejarlo mas estructurado
    al estilo de Risso. Hay dudas de si dejar los arreglos para
    pos, vel, etc como 3 arreglos o crear vector 3d.
    Se podria reemplazar 0.5*g por otra cte.
    Asi como Kind de un choque se ve por el valor del
    'siguiente choque' en la lista, no se puede
    hacer lo mismo con el tiempo del siguiente choque?
    (esta guardado en la lista de eventos)
    Otra duda. Distintos radios => Hacerlo con distintas masas? 
   23 / 11  Luego de conversar con Patricio para la
    rotacion es necesario guardar angulo segun x y angulo segun y
    para definir posicion angular de la particula, y
    la vel angular como vector 3d con magnitud = norma.
   26 / 11 Una idea: La camara ira moviendose segun como diga
    un archivo de configuracion leido al principio, o que
    se leera segun corresponda. La configuracion de
    cuantas particulas y algunas constantes tambien deberia
    leerse de un archivo. Duda, como es el vector tang. al choque en 3d?
    Por que 2*mu = 0.5*masa ?
    Arregle harto la regla de choque entre 2 part. a 3d. Hay un par
    de vars nuevas para el techo y suelo.
    Se prueba cambiar el calculo del prox choque contra paredes,
    pero hay problemas, particulas salen por lados!
    Se arregló poniendo un epsilon al querer t > 0, t > epsilon.
    De todas maneras hay problemas aun, al ponerle posicion Z inicial != 0
    algunas no chocan entre si.. y algunas pasan las murallas!
   27 / 11 Achicando el epsilon ya no cruzan las murallas pero
    hay una que no choca con las demas. Encontre el error, solo
    la part. 0 tenia z != 0.
   28 / 11 Agrego la parte visual, muchas cosas por arreglar.
    Aun no funciona pero esta escrito todo. Arregle la regla
    de choque segun vector tangencial corregido.
    Programe la regla de choque contra las murallas
    en caso general (habria que mejorar eso). El
    problema es que si se deja tal cual se empiezan a ir
    algunas particulas. Para solucionar esto se borra el 0.5*
    delante del calculo de los qt, qn (ver mas abajo).
    Luego las particulas adquieren energia al chocar con las
    murallas (ademas que se puso la normal mas grande hacia
    arriba en el piso para dar mas energia, como antes, no
    esta 'termalizada')
   30 / 11 Hay un problema menos trivial para arreglar
    que es dibujar primero a las que estan atras, sino
    hay errores, algunas atras q debian estar adelante.
   01 / 12 En la regla de choque contra los muros se
    agrega el movimiento del techo y piso.
    Falta agregar este movimiento en el tiempo del prox.
    choque. Particulas transparentes para que no haya
    unas delante de otras. Al agregar el mov. del techo
    y piso en la regla de choque se caen muchas! Ni
    siquiera disminuyendo el EPSILONCHOQUE al maximo.
   02 / 12 Puse la prueba de vt = -|v x n|
    Ahora voy a programar la escritura a disco.
    Esta funciona correctamente. Tuve que dejar el mismo
    Delta para la pantalla y el archivo para que se pueda
    hacer de la manera en que se hacia aumentandso tF.
    Los archivos generados son enormes!
    Corregí un signo malo en DPiso (quizas eso haga q se caigan menos)
    Igual se salen algunas hacia los lados!
    Ahora programe el tiempo de choque con el piso segun Risso o Rosa,
    pero no funciona, hay que arreglarlo.
   03 / 12 No entendi bien la regla del documento de Risso, pero
    lo que tiene Rosa es exactamente lo que me da a mano. Obviamente
    SI esta la diferencia del signo y del factor afuera, es raro.
    De todas maneras aun no funciona.
   06 / 12 Programado visual.c con rutinas visuales que muestran lo
    grabado en un archivo.
   07 / 12 Dudas resueltas con Patricio: mu = m1*m2/(m1 + m2),
    el calculo de cuando entra a la region de choque ES importante,
    tambien hay que tener ojo con las que no salen de esa region.
   08 / 12 Luego de corregir mucho el codigo de esta parte, elimine lo
    antiguo de Risso, parti de cero, haciendo calculos a mano adhoc para
    mis constantes. Por fin funciona (se caen pocas). Hay 5 casos considerados.
    Creo que deberian ser todos. Hay que ver por que se salen, pero tambien
    se salen a los lados, asi que puede ser el mismo problema. Hay que
    hacer el choque con el techo tambien ahora. Programe el choque con el
    techo. No funciona.
   09 / 12 Arreglando el choque con el techo. Ahora retorna Tpo + porx en
    calculo de choque contra paredes, por si se llama para otra que no sea
    la recien actualizada. Arreglado choque con el techo. Funciona.
    Aun se pasan algunas particulas en las paredes, incluso por los lados.
    Cambiando el EPSILONCHOQUE se salen menos (lo disminui)
   10 / 12 Idea: Al chocar con las murallas poner la posicion de la particula
    a la fuerza donde deberia ir en vez de iterar con el tiempo.
    Igual se caen (abajo al menos).
    Para evitar caidas agrego cte que si el tiempo es mas grande, rebaja
    todos los tiempos (reescala)
   11 / 12 Puse un arreglo forzado de la pos cuando las pelotas se caen.
    Es feo pero voy a medir cuantas veces se corrige. No funciona bien.
    Ademas, no es la idea.
   13 / 12 Haciendo pruebas se ve que la que sale hacia abajo no tenia
    tiempo de proximo choque luego de chocar la ultima vez.
    Se agrego una raiz en el choque con el piso (en realidad varias
    pensando en arreglar, pero solo una tiene sentido y no estaba
    considerada antes). Funciona identico a como antes.
    Evitando el epsilon de choque con las murallas, se pone a 0.
    (Asi se caian hartas). Pero entonces se verifica que la velocidad
    en el momento de choque sea valida para un choque. Con esto se comporta
    igual que con epsilon de choque! Es bueno. Hace la comparacion al
    calcular el tiempo pues despues seria como tener eventos invalidos.
    Ahora la vel, igual que la pos del techo y piso tienen el tiempo
    como parametro. Una solucion es dejar un epsilon para las comparaciones
    t > 0 en el calculo del choque, pues antes apenas tenia uno retornaba,
    pero puede pasar que abajo no le gustara por la cond de las velocidades.
    El problema es cuando una se queda 'pegada'. Ahora el epsilon de arriba
    puede ser 0, pues la comparacion de si las veloc. estan bien la
    hago en el metodo donde veo el choque con el piso. Como soluciono eso?
    De todas maneras, la que sale asi 'vuelve a entrar'. Borre la raiz
    agregada, no parece cambiar, habria que hacer una prueba mejor de eso.
    Puede chocar?
   14 / 12 Se agrego comparacion de velocidades al techo.
   15 / 12 Se tiene que probar C > g, para que adquiera energia y se
    despeguen las particulas en y = 0. Se supone que asi deberian
    caerse menos. Luego de probar se que ve raices de todas las que se
    estan calculando son tomadas por el programa, se descartan las que
    nunca fueron tomadas. Ahora al parecer no se caen particulas con
    C > g/2. Defino una manera de poner al azar las particulas al inicio.
    Ahora, al quitar el mov forzado despues del choque a las part. parecen
    no salirse de la caja. Al poner un tamagno muy pequegno de Ly donde
    no alcanzan a chocar verticalmente, derrepente salen!
    Ojo con variar TTecho y TPiso por separado pues hay que arreglar
    la cte del reloj para que lo pueda soportar.
   16 / 12 Borre definitivamente el EPSILONCHOQUE, calculo un solo tiempo
    segun x y otro segun z para prox choque con murallas, en vez de uno
    para cada lado. Se agregan constantes rn1 y rt13.
   19 / 12 Ordene el codigo con defines y undefs para probar cosas.
    Arregle varias constantes y calculos demas en los choques entre ellas
    y con paredes.
   20 / 12 A mano y con Maple hice varios calculos de como rota la particula
    y llegue al resultado, verificado que cumple con las condiciones.
    Programe el metodo mueve con esta mejora. Tambien la parte visual.
    El modulo de Ang empieza a variar distinto de uno. Puse correccion.
    Lo programe tambien con |Wel| = 1, pero no funciona.
   22 / 12 Luego de hablar con Patricio, voy a agregar cota donde el choque
    es elastico. Ahora esta comentado todo lo de rotaciones. Con ese arreglo
    se caen muchisimo menos, pero se pone mas lento al subir (hay muchos choques).
    Lamentablemente cae algo asi como una en un millon. Recalcule la regla
    de rotacion, estaba mal un supuesto.
    Mis supuestos son: d1·d0 = d0·w - (d0·w)*alpha + alpha
                       d1·w  = d0·w
                       d1·d1 = 1          donde alpha = cos(|w|t)
   23 / 11 Arreglo rotacion para |w| = 1 y hago que el angulo entre W y d0
    sea siempre menor o igual a 90º. El choque con las murallas ahora es
    ad-hoc para cada una. Deje comentado el en general (serviria para
    murallas inclinadas incluso!)
   25 / 11 Arregle pifia de undef de GRAFICO. Ojo: problemas si A = 0 !
    Voy a programar el choque con el calculo de la raiz segun
    Numerical Recipes in C.
   26 / 11 Modifique el choque entre ellas ya que hay una donde siempre
    reloj = Tpo[k]. Se simplifica el calculo. Gran cambio. La forma de
    revisar era mala, era en momentos cualquiera. Ahora reviso antes de
    cada choque y despues de cada uno. Se descubrieron varios errores
    asi. La revision tiene un epsilon de error. Pero dado que cuando
    luego de eso se pasa el 'traslape' o lo que sea significa que es por
    el momento de choque que estan traslapadas, pero es un epsilon y no
    afecta al calculo del proximo choque. Con todas las nuevas tecnicas,
    usando A = 0.5 aguanta hasta T = 1 sin caerse nada, ya con menos
    se nota que es mejor con NUMERICAL, pero igual se cae a veces.
    En esta version no se puede sacar la comparacion de las velocidades
    del prox. choque.
   03 / 01 Agregada posibilidad de tener coeficientes distintos para
    choques con paredes o entre ellas. Ademas las ctes rn1, rt13, etc,
    ahora son variables globales, para permitir manejo de los coefs.
    mas facilmente. Agregado tambien el calculo de choque arriba mas preciso.
    La unica manera de no reportar traslapes es disminuir la cota que anuncia.
    De todas maneras no parece que haya alguna que se vaya de verdad lejos.
   04 / 01 Al agregar que entre las particulas el choque tambien sea elastico
    ya parecen no salir!! El caso g = 0, al igual que A = 0, no funcionan.
    Fue programada la gravedad 0. Funciona bien, hay que prender el #define GRAV0 */


#define GRAFICO   // puede colocarse undef y no dibuja
#undef ARCHIVO   // puede colocarse undef y no graba

#undef DATOSENTRE   // Mostrar datos sobre choques entre particulas
#undef DATOSARRIBA  // Mostrar datos sobre choques con el techo
#undef DATOSABAJO  // Mostrar datos sobre choques con el piso

//#define MOSTRARENERGIA  // Mostrar energia del sistema luego de cada choque
#undef MOVFORZVERTICAL  // Forzar posicion al chocar con techo o piso
#undef MOVFORZRESTO  // Forzar posicion al chocar con el resto
#undef REVISIONMALAS  // Revisar paso a paso si hay traslapes o particulas afuera

#define NUMERICAL

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#ifdef GRAFICO
#include <plot.h>
#endif

#define N           313   // numero de particulas
#define Ll          N    // largo de listas por partic

#define	rn          0.9  // coef de restitucion normal     ENTRE PARTICULAS
#define	rt          0.9  // coef de restitucion tangencial ENTRE PARTICULAS
#define kappa       0.1  // coef de roce                   ENTRE PARTICULAS
#define	RN          0.9 // coef de restitucion normal     CON MURO
#define	RT          0.9 // coef de restitucion tangencial CON MURO
#define KAP         0.1  // coef de roce                   CON MURO
#define COTAELAST   0.00005  // Si vn < cota entonces choque elastico RN = 1
#define A           0.02 // Amplitud techo piso (para distintas modificar XVinic)
#define T           1.//2.82842712474619009760   // Periodo del techo y piso (seg)
#define Lx          30. // Dimensiones de la caja
#define	Ly          1.4
#define	Lz          10.0
#define r1          0.5   // radio base particulas 
#define MASA(r)     (r*r) // masa de particulas
#undef GRAV0
#define	g           0.001 // gravedad
#define VARRADIO    0.1
#define infinite    1e40
#define DELTAPANT   0.05  // A cada cuanto mostrar pantalla / archivo (seg)

#define MAXPRUEBAS  20000000 // Cuantas pruebas hace de pos. antes de rendirse al partir
#define EPSREVISION 1e-10  // En la revision, cuanto permite de traslape
#ifdef REVISIONMALAS
int ultimoPiso;
int cantverif;
#endif

double PosX[N], PosY[N], PosZ[N];
double VelX[N], VelY[N], VelZ[N];
double TpoSE[N];    // Tiempo siguiente evento en la lista (el minimo)
int    SE[N];       // Cual es el siguiente evento
double Tpo[N];      // Tiempo ultima atencion (posicion temporal)
double TpoE[N][Ll]; // Tiempo predicho para cada evento de la lista
double WelX[N], WelY[N], WelZ[N]; // veloc angular
double AngX[N], AngY[N], AngZ[N], Rot[N]; // pos angular (solo dos son necesarias) y rot.
int    Wall[N];      // Siguiente pared con que chocara (0=L, 1=D, 2=R, 3=T)
int    Choques[N];   // # de choques duros de cada particula
int    Socio[N][Ll]; // Socio de choque en cada evento
int    Kind[N][Ll];  // kind (tipo de choque: 1 ==> DD,  0 ==> DW)
int    CC[N][Ll];    // cantidad de choques esperado para el evento
char   Col[N][6];    // color
double Radio[N];     // radio de cada particula

int B[2*N + 1];      // CBT .. es un arbol lleno

int contadorpiso, arreglospiso;

double TPiso, TTecho; // Periodos de cada uno.. mas tarde son iguales
double AmpTecho, CteTecho, AmpPiso, CtePiso; // Cte = 16*Amp/T^2
double TTecho2, TTecho14, TTecho34, TPiso2, TPiso14, TPiso34; // ctes varias
double g2CtePiso, g2CtePisom, g2CteTecho, g2CteTechom; // (g2 + CtePiso),(g2 - CtePiso)
double reloj, tF, tanFi, TANfI, Tb, CteReloj, rn1, RN1, rt13, RT13, g2;
int    knext, cantmovreloj;
int tpo1, tpo2, tpo3, tpo4, tpo5, tpo6, tpo7, tpo8; // vars aux para medir que choques habia, se pueden borrar

double sq(double d)
{
    return d*d;
}

double DTecho(double tpo) /* Velocidad del techo */
{
    double phiaux;
    
    phiaux = tpo - TTecho*floor(tpo/TTecho);
    if(phiaux < TTecho14)
        return -2.*CteTecho*phiaux;
    else if(phiaux < TTecho34)
        return CteTecho*(2.*phiaux - TTecho);
    return -2.*CteTecho*(phiaux - TTecho);
}

double DPiso(double tpo)
{
    double phiaux;
    
    phiaux = tpo - TPiso*floor(tpo/TPiso);
    if(phiaux < TPiso14)
        return -2.*CtePiso*phiaux;
    else if(phiaux < TPiso34)
        return CtePiso*(2.*phiaux - TPiso);
    return -2.*CtePiso*(phiaux - TPiso);
}

double Techo(double tpo) /* Pos. del techo, con tpo para calculos */
{
    double phiaux, p;

//    phiaux = fmod(tpo, TTecho);  // Es mas lento !! ?
    phiaux = tpo - TTecho*floor(tpo/TTecho);
    if(phiaux < TTecho14)
        return Ly + AmpTecho - CteTecho*phiaux*phiaux;
    else if(phiaux < TTecho34)
    {
        p = phiaux - TTecho2;
        return Ly - AmpTecho + CteTecho*p*p;
    }
    p = phiaux - TTecho;
    return Ly + AmpTecho - CteTecho*p*p;
}

double Piso(double tpo)
{
    double phiaux, p;
    
    phiaux = tpo - TPiso*floor(tpo/TPiso);
    if(phiaux < TPiso14)
        return AmpPiso - CtePiso*phiaux*phiaux;
    else if(phiaux < TPiso34)
    {
        p = phiaux - TPiso2;
        return -AmpPiso + CtePiso*p*p;
    }
    p = phiaux - TPiso;
    return AmpPiso - CtePiso*p*p;
}

void CalcRot(double *ax, double *ay, double *az, int k, double t)
{
    double alpha, d0w, mw, wx, wy, wz, d0xwx, d0xwy, d0xwz, den, c1, c2, c3;
    double signo;

    mw = sqrt(WelX[k]*WelX[k] + WelY[k]*WelY[k] + WelZ[k]*WelZ[k]);
    wx = WelX[k]/mw;
    wy = WelY[k]/mw;
    wz = WelZ[k]/mw;
    d0w = AngX[k]*wx + AngY[k]*wy + AngZ[k]*wz;  // d0w = beta
    if(d0w < 0.)
    {
        signo = -1.;
        wx = -wx;
        wy = -wy;
        wz = -wz;
        d0w = -d0w;
    } else
        signo = 1.;
    den = 1. + d0w;
    alpha = cos(mw*t); // Supongo |d0| = |Ang| = 1
    d0xwx = AngY[k]*wz - AngZ[k]*wy;
    d0xwy = AngZ[k]*wx - AngX[k]*wz;
    d0xwz = AngX[k]*wy - AngY[k]*wx;
    c1 = (d0w + alpha)/den;
    c2 = d0w*(1. - alpha)/den;
    c3 = signo*sqrt((1. - alpha + 2.*d0w)*(1. - alpha))/den;
    *ax = AngX[k]*c1 + wx*c2 + d0xwx*c3;
    *ay = AngY[k]*c1 + wy*c2 + d0xwy*c3;
    *az = AngZ[k]*c1 + wz*c2 + d0xwz*c3;
}

#ifdef REVISIONMALAS
void Verificar(double tpo)
{
    double t, x, y, z, pp, pt, t1, xa, ya, za;
    int k, l;
    
    for(k = 0; k < N; k++)
    {
        t = tpo - Tpo[k];
        x = PosX[k] + VelX[k]*t;
        y = PosY[k] + (VelY[k] - g2*t)*t;
        z = PosZ[k] + VelZ[k]*t;
        pp = Piso(tpo);
        pt = Techo(tpo);

        if(x < Radio[k] - EPSREVISION || x > Lx - Radio[k] + EPSREVISION)
            printf("La %d esta afuera de la caja segun x!!\n", k);
        if(z < Radio[k] - EPSREVISION || z > Lz - Radio[k] + EPSREVISION)
            printf("La %d esta afuera de la caja segun z!!\n", k);
        if(y < pp + Radio[k] - EPSREVISION)
        {
            printf("La %d esta afuera de la caja hacia abajo (y)!!\n", k);
            printf("su py = %f, vy = %f, piso = %f %d / %d\n", y, VelY[k] - g2*t, Piso(tpo), arreglospiso, contadorpiso);
            exit(1);
            arreglospiso++;
            //PosY[k] = Piso(tF) + Radio[k];
            //Tpo[k] = tF;
        }
        if(y > pt - Radio[k] + EPSREVISION)
        {
            printf("La %d esta afuera de la caja hacia arriba (y)!!\n", k);
            printf("su py = %f, vy = %f, techo = %f\n", y, VelY[k] - g2*t, Techo(tpo));
            exit(1);
        }
        for(l = k + 1; l < N; l++) // Comprobacion de traslape .. NO HA HABIDO EN MUUUCHO RATO =)
        {
            t1 = tpo - Tpo[l];
            xa = PosX[l] + VelX[l]*t1 - x;
            ya = PosY[l] + (VelY[l] - g2*t1)*t1 - y;
            za = PosZ[l] + VelZ[l]*t1 - z;
            if(xa*xa + ya*ya + za*za < sq(Radio[k] + Radio[l]) - EPSREVISION)
                printf("Las %d (%f,%f,%f) y %d (%f,%f,%f) estan traslapadas %g!!\n", k,
                    x, y, z, l, xa, ya, za, sqrt(xa*xa + ya*ya + za*za) - Radio[k] - Radio[l]);
        }
    }
}
#endif

// PARTE VISUAL
#ifdef GRAFICO

#define cada    (N)   // mov. de la camara a cada cuantos eventos
#define TPant   400   // Tamaño de la pantalla en pixeles

double CamX, CamY, CamZ, CamVx, CamVy, CamVz;    /* pos camara y donde apunta */
double CamYx, CamYy, CamYz, CamXx, CamXy, CamXz; /* v. unit. de la pantalla */
double CamD; /* distancia del observador a la pantalla */
double p_medio;
int pl_handle;
int p_tamano;

FILE *camfile;
int sicamfile;
double nextcamevent;

int IniciarPantalla(int tamano)
{
    char s[20];
    
    sprintf(s, "%dx%d", tamano, tamano);
    pl_parampl("USE_DOUBLE_BUFFERING", "yes");
    pl_parampl("BITMAPSIZE", s);
    pl_handle = pl_newpl("X", stdin, stdout, stderr);
    pl_selectpl(pl_handle);
    if(pl_openpl() < 0)     
    {
        fprintf(stderr, "No se pudo abrir ventana\n");
        return 1;
    }
    pl_fspace(0.0, 0.0, tamano, tamano);
    pl_flinewidth(0.01);
    pl_filltype(0); /* 0 = transparente, 1 = lleno */
    p_tamano = tamano;
    p_medio = tamano/2.;
    return 0;
}

void IniciarCamara(double cx, double cy, double cz, double vx, double vy, double vz, double d)
{
    double r;
    
    CamX = cx;
    CamY = cy;
    CamZ = cz;
    r = sqrt(vx*vx + vy*vy + vz*vz);
    CamVx = vx/r;
    CamVy = vy/r;
    CamVz = vz/r;
    CamD = d;
    CamYx = -CamVy*CamVx;
    CamYy = 1. - CamVy*CamVy;
    CamYz = -CamVy*CamVz;
    r = sqrt(CamYx*CamYx + CamYy*CamYy + CamYz*CamYz);
    CamYx = CamYx/r;
    CamYy = CamYy/r;
    CamYz = CamYz/r;
    CamXx = -CamVz/r;
    CamXy = 0.;
    CamXz = CamVx/r;
}

void CalcProyR(double *px, double *py, double *r, double x, double y, double z)
{
    double den;  // Lo hace sin restar pos de camara

    den = CamD/(x*CamVx + y*CamVy + z*CamVz);
    *px = den*(x*CamXx + y*CamXy + z*CamXz) + p_medio;
    *py = den*(x*CamYx + y*CamYy + z*CamYz) + p_medio;
    *r = CamD/sqrt(x*x + y*y + z*z);
}

void CalcProy(double *px, double *py, double x, double y, double z)
{
    double den;
    
    x -= CamX;
    y -= CamY;
    z -= CamZ;
    den = CamD/(x*CamVx + y*CamVy + z*CamVz);
    *px = den*(x*CamXx + y*CamXy + z*CamXz) + p_medio;
    *py = den*(x*CamYx + y*CamYy + z*CamYz) + p_medio;
}

void DibujarEjes(double PP, double PT)
{
    double px[8], py[8];

    CalcProy(&px[0], &py[0], 0., PT, 0.);
    CalcProy(&px[1], &py[1], 0., PP, 0.);
    CalcProy(&px[2], &py[2], Lx, PT, 0.);
    CalcProy(&px[3], &py[3], Lx, PP, 0.);
    CalcProy(&px[4], &py[4], Lx, PT, Lz);
    CalcProy(&px[5], &py[5], Lx, PP, Lz);
    CalcProy(&px[6], &py[6], 0., PT, Lz);
    CalcProy(&px[7], &py[7], 0., PP, Lz);
    pl_fline(px[0], py[0], px[1], py[1]);
    pl_fline(px[0], py[0], px[2], py[2]);
    pl_fline(px[0], py[0], px[6], py[6]);
    pl_fline(px[3], py[3], px[1], py[1]);
    pl_fline(px[3], py[3], px[2], py[2]);
    pl_fline(px[3], py[3], px[5], py[5]);
    pl_fline(px[4], py[4], px[2], py[2]);
    pl_fline(px[4], py[4], px[5], py[5]);
    pl_fline(px[4], py[4], px[6], py[6]);
    pl_fline(px[7], py[7], px[1], py[1]);
    pl_fline(px[7], py[7], px[6], py[6]);
    pl_fline(px[7], py[7], px[5], py[5]);
}

void DibujarPantalla()
{ 
    int k;
    double t, x, y, z, px, py, r, pp, pt, xa, ya, za;
    
    pl_erase();
    pt = Techo(tF);
    pp = Piso(tF);
    DibujarEjes(pp, pt);
    for(k = 0; k < N; k++)
    {
        t = tF - Tpo[k];
        x = PosX[k] + VelX[k]*t - CamX;
        y = PosY[k] + (VelY[k] - g2*t)*t - CamY;
        z = PosZ[k] + VelZ[k]*t - CamZ;
        CalcProyR(&px, &py, &r, x, y, z);
        r *= Radio[k];
        pl_fillcolorname(Col[k]);
        pl_fcircle(px, py, r);
//        CalcRot(&xa, &ya, &za, k, t);
        xa = 0.;
        ya = 0.;
        za = 0.;
      //  printf("Ang a dibujar: x = %f y = %f z = %f  mod = %f\n", xa, ya, za, xa*xa + ya*ya + za*za);
        x += xa*Radio[k];
        y += ya*Radio[k];
        z += za*Radio[k];
        CalcProyR(&px, &py, &r, x, y, z);
        r *= 0.3*Radio[k];
        pl_fillcolorname("black");  
        pl_fcircle(px, py, r);
    }
}

void CerrarPantalla()
{
    pl_closepl();
    pl_selectpl(0);
    pl_deletepl(pl_handle);
}

void IniciarArchCam(char *nom)
{
    camfile = fopen(nom, "r");
    if(camfile == NULL)
    {
        fprintf(stderr, "No se pudo abrir archivo de camara\n");
        sicamfile = 0;
        return; /* error */
    }
    sicamfile = 1;
}

double LeerNum()
{
    char linea[256];
    int largo = 0;
    int c;
    
    while(1)
    {
        c = fgetc(camfile);
        if(c == EOF || largo == 256)
            return infinite;
        if(c == '\t' || c == '\n' || c == ' ')
        {
            linea[largo] = 0;
            break;
        }
        linea[largo] = c;
        largo++;
    }
    return atof(linea);
}

void LeerSiguienteEvCam()
{
    double cx, cy, cz, vx, vy, vz, d;
    
    cx = LeerNum();
    cy = LeerNum();
    cz = LeerNum();
    vx = LeerNum();
    vy = LeerNum();
    vz = LeerNum();
    d = LeerNum();
    nextcamevent = LeerNum() - cantmovreloj*CteReloj;
    IniciarCamara(cx, cy, cz, vx, vy, vz, d);
}
#endif
// FIN PARTE VISUAL

// PARTE ARCHIVO
#ifdef ARCHIVO

FILE *arch;

int IniciarArchivo(char *nombrearchivo)
{
    arch = fopen(nombrearchivo, "w");
    if(arch == NULL)
    {
        fprintf(stderr, "No se pudo abrir archivo de salida\n");
        return 1; // error
    }
    fprintf(arch, "Parametros:\n");
    fprintf(arch, "N = %d\tLx = %f\tLy = %f\tLz = %f\tr1 = %f\tVARRADIO = %f\n", N, Lx, Ly, Lz, r1, VARRADIO);
    fprintf(arch, "rn = %f\trt = %f\tkappa = %f\tg = %f\tCOTAELAS = %f\n", rn, rt, kappa, g, COTAELAST);
    fprintf(arch, "A = %f\tT = %f\n", A, T);
    return 0;
}

void GrabarTodo()
{ 
    int k;
    double t, x, y, z, xa, ya, za;
    
    fprintf(arch, "techo = %f, piso = %f\n", Techo(tF), Piso(tF));
    for(k = 0; k < N; k++)
    {
        t = tF - Tpo[k];
        x = PosX[k] + VelX[k]*t;
        y = PosY[k] + (VelY[k] - g2*t)*t;
        z = PosZ[k] + VelZ[k]*t;
        CalcRot(&xa, &ya, &za, k, t);
        fprintf(arch, "%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", k, tF, x, y, z, xa, ya, za, Radio[k]);
    }
}

void CerrarArchivo()
{
    fclose(arch);
}
#endif
// FIN PARTE ARCHIVO

#ifdef MOSTRARENERGIA
double EnergiaCinetica()
{
    int i;
    double suma = 0.;
    
    for(i = 0; i < N; i++)
    {
        double m = 0.5*MASA(Radio[i]);
        
        suma += m*sq(VelX[i]) + m*sq(VelY[i]) + m*sq(VelZ[i]);
    }
    return suma;
}

double EnergiaPotencial()
{
    int i;
    double suma = 0.;
    
    for(i = 0; i < N; i++)
        suma += MASA(Radio[i])*g*PosY[i];
    return suma;
}
#endif

void Mueve(int k)
{
    double t, mo, ax, ay, az;
    
    t       = reloj - Tpo[k];
    PosX[k] = PosX[k] + VelX[k]*t;
    PosY[k] = PosY[k] + (VelY[k] - g2*t)*t;
    PosZ[k] = PosZ[k] + VelZ[k]*t;
    VelY[k] = VelY[k] - g*t;
    CalcRot(&ax, &ay, &az, k, t);
    AngX[k] = ax;
    AngY[k] = ax;
    AngZ[k] = ax;
    mo = sqrt(AngX[k]*AngX[k] + AngY[k]*AngY[k] + AngZ[k]*AngZ[k]);
    AngX[k] /= mo;  // Hay que hacer la correccion..
    AngY[k] /= mo;
    AngZ[k] /= mo;
   /* mo = sqrt(AngX[k]*AngX[k] + AngY[k]*AngY[k] + AngZ[k]*AngZ[k]);
    printf("Nueva Ang: x = %f y = %f z = %f  mod = %f\n", AngX[k], AngY[k], AngZ[k], mo);*/
    Tpo[k]  = reloj;
}

void ChoqueDD(int k1, int k2) //regla de choque discos k1 y k2 //17
{
    double tx, ty, tz, v21x, v21y, v21z, w21x, w21y, w21z, crx, cry, crz, vn,
           qn, qt, qx, qy, qz, nx, ny, nz, nQ, ax, ay, az, m1, m2, mu;
    
    nQ = Radio[k1] + Radio[k2]; //  nQ = sqrt(nx*nx + ny*ny + nz*nz) = R1 + R2
    nx = (PosX[k1] - PosX[k2])/nQ; /* n vector unitario normal al pto de choque */
    ny = (PosY[k1] - PosY[k2])/nQ;
    nz = (PosZ[k1] - PosZ[k2])/nQ;
    v21x = VelX[k2] - VelX[k1];
    v21y = VelY[k2] - VelY[k1];
    v21z = VelZ[k2] - VelZ[k1];
    w21x = Radio[k1]*WelX[k1] + Radio[k2]*WelX[k2];
    w21y = Radio[k1]*WelY[k1] + Radio[k2]*WelY[k2];
    w21z = Radio[k1]*WelZ[k1] + Radio[k2]*WelZ[k2];
    ax = v21x + w21y*nz - w21z*ny; /*  + w21 cruz n */
    ay = v21y + w21z*nx - w21x*nz;
    az = v21z + w21x*ny - w21y*nx;
    crx = ay*nz - az*ny;
    cry = az*nx - ax*nz;
    crz = ax*ny - ay*nx;
    nQ = sqrt(crx*crx + cry*cry + crz*crz);
    crx = crx/nQ;
    cry = cry/nQ;
    crz = crz/nQ;
    tx = cry*nz - crz*ny;    /* t vector tangencial al pto de choque */
    ty = crz*nx - crx*nz;
    tz = crx*ny - cry*nx;
    vn = v21x*nx + v21y*ny + v21z*nz; // vt = ax*tx + ay*ty + az*tz = -nQ
#ifdef DATOSENTRE
    printf("vn = %f\np(%d,%d)=(%f,%f,%f;%f,%f,%f)\nv(%d,%d)=(%f,%f,%f;%f,%f,%f)\n",
          vn, k1, k2, PosX[k1], PosY[k1], PosZ[k1], PosX[k2], PosY[k2], PosZ[k2],
           
   k1, k2, VelX[k1], VelY[k1], VelZ[k1], VelX[k2], VelY[k2], VelZ[k2]);
#endif
    m1 = MASA(Radio[k1]);
    m2 = MASA(Radio[k2]);
    mu = m1*m2/(m1 + m2);
 //   qn = rn1*vn*mu;
    if(vn > COTAELAST)
        qn = rn1*vn*mu;
    else
        qn = 2.*vn*mu;   // en este caso es elastico, rn = 1
    if(/*avt*/nQ < tanFi*fabs(vn))
        qt = rt13*mu*nQ;
    else
        qt = /*avt*/kappa*qn;
    qx = qn*nx - qt*tx;
    qy = qn*ny - qt*ty;
    qz = qn*nz - qt*tz;
    VelX[k1] += qx/m1; /* masa 1 */  // Resultados
    VelY[k1] += qy/m1;
    VelZ[k1] += qz/m1;
    VelX[k2] -= qx/m2; /* masa 2 !! */
    VelY[k2] -= qy/m2;
    VelZ[k2] -= qz/m2;
    nQ = 2.*qt;
    crx = nQ*(ny*tz - nz*ty); /* 2 * qt * (n cruz t) */
    cry = nQ*(nz*tx - nx*tz);
    crz = nQ*(nx*ty - ny*tx);
    nQ = m1*Radio[k1];
    WelX[k1] += crx/nQ;
    WelY[k1] += cry/nQ;
    WelZ[k1] += crz/nQ;
    nQ = m2*Radio[k2];
    WelX[k2] += crx/nQ;
    WelY[k2] += cry/nQ;
    WelZ[k2] += crz/nQ;
#ifdef DATOSENTRE
    printf("Nuevas v(%d,%d)=(%f,%f,%f;%f,%f,%f)\n",
               k1, k2, PosX[k1], PosY[k1], PosZ[k1], PosX[k2], PosY[k2], PosZ[k2],
               k1, k2, VelX[k1], VelY[k1], VelZ[k1], VelX[k2], VelY[k2], VelZ[k2]);
#endif
}

void LimpiaLista(int k)
{
    int e;
    
    for(e = 0; e < Ll; e++)
        TpoE[k][e] = infinite;
}

void AgregaEvento(int k, int Soc, int Tipo, int Ce, double Te)
{
    int e;
    
    e = 0;
    while(TpoE[k][e] != infinite && e < Ll)
        e++;
    if(e >= Ll) // Esta comparacion se debe sacar cuando funcione completamente
    {
        printf("Lista llena!! e = %d (se debe usar un Ll mas grande)\n", e);
        exit(0);
    }
    Socio[k][e] = Soc; // de la lista
    Kind[k][e]  = Tipo;
    CC[k][e]    = Ce;
    TpoE[k][e]  = Te;
}

void Ganador(int k)
{
    int e, j, ka;
    double tau;
    
    tau = infinite;
    e = -1;
    for(j = 0; j < Ll; j++)
        if(TpoE[k][j] < tau) 
        {  
            tau = TpoE[k][j];
            e = j;
            ka = Kind[k][j];
        }
    SE[k] = e;             // el ganador de la lista k es el 'e-simo evento 
    TpoSE[k] = TpoE[k][e]; // de la lista
}  

void UpGradeCBT(int kk)	//6
{ 
    int f, ll, rr;
    
    f = kk + N - 1;
    do
    {
        f  = (f - 1)/2;
        ll = 2*f + 1;
        rr = ll + 1; /* 2*f + 2 */
        if(TpoSE[B[ll]] < TpoSE[B[rr]])
            B[f] = B[ll];
        else
            B[f] = B[rr];
    } while(f != 0);
}

void IniciaCBT()
{ 
    int f, r;
    
    for(r = N - 1; r < 2*N - 1; r++)
        B[r] = r - N + 1;   /* da a cada hoja lo que tendra siempre */
    f = N - 1;
    for(r = 2*N - 2; r > 0; r -= 2)
    {
        f--;  /* f= (r - 1)/2 */
        if(TpoSE[B[r]] < TpoSE[B[r - 1]])
            B[f] = B[r];
        else
            B[f] = B[r - 1];
    }
}  

void ChoqueW()  //choques con paredes
{
    double tx, ty, tz/*, v21x, v21y, v21z, w21x, w21y, w21z*/, crx, cry, crz, vn,
           qn, qt, nQ/*, nx, ny, nz*/;
    
    switch(Wall[knext]) // OJO ahora hay mov forzado
    {
        case 0:  // Izquierda
#ifdef MOVFORZRESTO
            PosX[knext] = Radio[knext];
#endif
            cry = VelZ[knext] + Radio[knext]*WelY[knext];
            crz = VelY[knext] - Radio[knext]*WelZ[knext];
            vn  = -VelX[knext];
            nQ  = sqrt(cry*cry + crz*crz);
            ty  = crz/nQ;
            tz  = cry/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            VelX[knext] += qn;
            VelY[knext] -= qt*ty;
            VelZ[knext] -= qt*tz;
            nQ  = 2.*qt/Radio[knext];
            WelY[knext] -= nQ*tz;
            WelZ[knext] += nQ*ty;
            break;
        case 1:  // Piso
            contadorpiso++;
#ifdef DATOSABAJO
            printf("vy = %f, py = %f, piso = %f, r = %f   %d: %d\n",
                    VelY[knext], PosY[knext], Piso(reloj), Radio[knext], knext, contadorpiso);
#endif
#ifdef MOVFORZVERTICAL
            PosY[knext] = Piso(reloj) + Radio[knext];
#endif
            crx = VelZ[knext] - Radio[knext]*WelX[knext];
            crz = VelX[knext] + Radio[knext]*WelZ[knext];
            vn  = DPiso(reloj) - VelY[knext];
            nQ  = sqrt(crx*crx + crz*crz);
            tx  = crz/nQ;    // t vector tangencial al pto de choque
            tz  = crx/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            VelX[knext] -= qt*tx;
            VelY[knext] += qn;
            VelZ[knext] -= qt*tz;
            nQ  = 2.*qt/Radio[knext];
            WelX[knext] += nQ*tz;
            WelZ[knext] -= nQ*tx;
            break;
        case 2:  // Derecha
#ifdef MOVFORZRESTO
            PosX[knext] = Lx - Radio[knext];
#endif
            cry = VelZ[knext] - Radio[knext]*WelY[knext];
            crz = VelY[knext] + Radio[knext]*WelZ[knext];
            vn  = VelX[knext];
            nQ  = sqrt(cry*cry + crz*crz);
            ty  = crz/nQ;
            tz  = cry/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            VelX[knext] -= qn;
            VelY[knext] -= qt*ty;
            VelZ[knext] -= qt*tz;
            nQ  = 2.*qt/Radio[knext];
            WelY[knext] += nQ*tz;
            WelZ[knext] -= nQ*ty;
            break;
        case 3:  // Techo
#ifdef DATOSARRIBA
            printf("vy = %f, py = %f, techo = %f, r = %f   \n",
                    VelY[knext], PosY[knext], Techo(reloj), Radio[knext]);
#endif
#ifdef MOVFORZVERTICAL
            PosY[knext] = Techo(reloj) - Radio[knext];
#endif
            crx = VelZ[knext] + Radio[knext]*WelX[knext];
            crz = VelX[knext] - Radio[knext]*WelZ[knext];
            vn  = VelY[knext] - DTecho(reloj);
            nQ  = sqrt(crx*crx + crz*crz);
            tx  = crz/nQ;    // t vector tangencial al pto de choque
            tz  = crx/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            VelX[knext] -= qt*tx;
            VelY[knext] -= qn;
            VelZ[knext] -= qt*tz;
            nQ  = 2.*qt/Radio[knext];
            WelX[knext] -= nQ*tz;
            WelZ[knext] += nQ*tx;
            break;
        case 4:  // Delante
#ifdef MOVFORZRESTO
            PosZ[knext] = Radio[knext];
#endif
            crx = VelY[knext] + Radio[knext]*WelX[knext];
            cry = VelX[knext] - Radio[knext]*WelY[knext];
            vn  = -VelZ[knext];
            nQ  = sqrt(crx*crx + cry*cry);
            tx  = cry/nQ;    // t vector tangencial al pto de choque
            ty  = crx/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            VelX[knext] -= qt*tx;
            VelY[knext] -= qt*ty;
            VelZ[knext] += qn;
            nQ  = 2.*qt/Radio[knext];
            WelX[knext] -= nQ*ty;
            WelY[knext] += nQ*tx;
            break;
        default://case 5:  // Atras
#ifdef MOVFORZRESTO
            PosZ[knext] = Lz - Radio[knext];
#endif
            crx = VelY[knext] - Radio[knext]*WelX[knext];
            cry = VelX[knext] + Radio[knext]*WelY[knext];
            vn  = VelZ[knext];
            nQ  = sqrt(crx*crx + cry*cry);
            tx  = cry/nQ;    // t vector tangencial al pto de choque
            ty  = crx/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            VelX[knext] -= qt*tx;
            VelY[knext] -= qt*ty;
            VelZ[knext] -= qn;
            nQ  = 2.*qt/Radio[knext];
            WelX[knext] += nQ*ty;
            WelY[knext] -= nQ*tx;
            break;
    }
/*    v21x = -VelX[knext];
    v21y = -VelY[knext];
    v21z = -VelZ[knext];
    w21x = Radio[knext]*WelX[knext];
    w21y = Radio[knext]*WelY[knext];
    w21z = Radio[knext]*WelZ[knext];
    AQUI DECLARAR n VECTOR NORMAL Y AGREGAR VEL DE LA MURALLA A v21
    ax   = v21x + w21y*nz - w21z*ny; //  + w21 cruz n
    ay   = v21y + w21z*nx - w21x*nz;
    az   = v21z + w21x*ny - w21y*nx;
    crx  = ay*nz - az*ny;
    cry  = az*nx - ax*nz;
    crz  = ax*ny - ay*nx;
    nQ   = sqrt(crx*crx + cry*cry + crz*crz);
    crx  = crx/nQ;
    cry  = cry/nQ;
    crz  = crz/nQ;
    tx   = cry*nz - crz*ny;    // t vector tangencial al pto de choque
    ty   = crz*nx - crx*nz;
    tz   = crx*ny - cry*nx;
    vn   = v21x*nx + v21y*ny + v21z*nz; // vt = ax*tx + ay*ty + az*tz = -nQ
    if(vn > COTAELAST)
        qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
    else
        qn = 2.*vn;   // en este caso es elastico, rn = 1
    if(nQ < TANfI*fabs(vn)) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
        qt = RT13*nQ;
    else
        qt = KAP*qn;
    VelX[knext] += qn*nx - qt*tx; // Resultados
    VelY[knext] += qn*ny - qt*ty;
    VelZ[knext] += qn*nz - qt*tz;
    nQ  = 2.*qt/Radio[knext];
    crx = nQ*(ny*tz - nz*ty); // 2 * qt * (n cruz t)
    cry = nQ*(nz*tx - nx*tz);
    crz = nQ*(nx*ty - ny*tx);
    WelX[knext] += crx;
    WelY[knext] += cry;
    WelZ[knext] += crz;*/
#ifdef DATOSABAJO
    if(Wall[knext] == 1)
        printf("Nueva vy = %f, VPiso = %f reloj = %f  %d\n", VelY[knext], DPiso(reloj), reloj, knext);
#endif
#ifdef DATOSARRIBA
    if(Wall[knext] == 3)
        printf("Nueva vy = %f, VTecho = %f reloj = %f  %d\n", VelY[knext], DTecho(reloj), reloj, knext);
#endif
}

double TchoqueDD(int k, int j)  // tiempo choque discos k y j (Tpo[k] > Tpo[j])
{ // Esta es la que gasta mas tiempo de todas!!
    double b, Del, Rad, R2, x, y, z, gx, gy, gz, gg, qq,/* dtk,*/ dtj;

    Rad = Radio[k] + Radio[j];
    R2  = Rad*Rad;
//    dtk = reloj - Tpo[k];
    dtj = Tpo[k] - Tpo[j];
    x   = PosX[k] - PosX[j] - VelX[j]*dtj;
    y   = PosY[k] - PosY[j] - (VelY[j] - g2*dtj)*dtj;
    z   = PosZ[k] - PosZ[j] - VelZ[j]*dtj;
    gx  = VelX[k] - VelX[j];
    gy  = VelY[k] - VelY[j] + g*dtj;
    gz  = VelZ[k] - VelZ[j];
    b   = -x*gx - y*gy - z*gz;
    qq  = x*x + y*y + z*z - R2;
    gg  = gx*gx + gy*gy + gz*gz;  // gg es usado solo al definir Del
    Del = b*b - gg*qq;
    if(b > 0. && Del > 0.) // b > 0 => gg != 0)
    {
#if 0
//        if( (k == 1 && j == 7) || (k == 7 && j == 1) )
        {
            double t = qq/(b+sqrt(Del));
//            double t = (b - sqrt(Del))/gg;
            double tj = t + reloj - Tpo[j];
            double tk = t + reloj - Tpo[k];
            printf("tnext = %f\n",reloj+t);
            printf("pnj=(%f,%f,%f) pnk=(%f,%f,%f) d=(%f,%f,%f) r=(%f,%f) |d|=%f ed=%g\n",
                   PosX[j] + VelX[j]*tj,
                   PosY[j] + VelY[j]*tj - g2 * tj * tj,
                   PosZ[j] + VelZ[j]*tj,
                   
                   PosX[k] + VelX[k]*tk,
                   PosY[k] + VelY[k]*tk - g2 * tk * tk,
                   PosZ[k] + VelZ[k]*tk,
                   
                   PosX[j] + VelX[j]*tj - (PosX[k] + VelX[k]*tk),
                   PosY[j] + VelY[j]*tj - g2 * tj * tj - (PosY[k] + VelY[k]*tk - g2 * tk * tk),
                   PosZ[j] + VelZ[j]*tj - (PosZ[k] + VelZ[k]*tk),
                   
                   Radio[j], Radio[k],
                   sqrt(
                   sq(PosX[j] + VelX[j]*tj - (PosX[k] + VelX[k]*tk)) +
                   sq(PosY[j] + VelY[j]*tj - g2 * tj * tj - (PosY[k] + VelY[k]*tk - g2 * tk * tk)) +
                   sq(PosZ[j] + VelZ[j]*tj - (PosZ[k] + VelZ[k]*tk)) ),
                   
                   sq(PosX[j] + VelX[j]*tj - (PosX[k] + VelX[k]*tk)) +
                   sq(PosY[j] + VelY[j]*tj - g2 * tj * tj - (PosY[k] + VelY[k]*tk - g2 * tk * tk)) +
                   sq(PosZ[j] + VelZ[j]*tj - (PosZ[k] + VelZ[k]*tk)) - sq(Radio[j] + Radio[k])
                   
                   );
        } 
#endif
//        return Tpo[k] + (b - sqrt(Del))/gg;
#ifdef DATOSENTRE
        printf("choque entre %d y %d en %f\n", k, j, Tpo[k] + qq/(b + sqrt(Del)));
#endif
        return Tpo[k] + qq/(b + sqrt(Del));
    }
    return infinite;
} 

void Raiz(double *rr1, double *rr2, double Cte, double phip, double v0, double Am, double R, double y0)
{
    double a = Cte + g2;  // Esto ya esta definido como una cte
    double b = 2*Cte*phip - v0;
    double c = Am + Cte*phip*phip + R - y0;
    double q, raiz, t1, t2;
    
    raiz = b*b - 4.*a*c;
    if(raiz < 0.)
    {
        *rr1 = -1;
        *rr2 = -1;
        return;
    }
    raiz = sqrt(raiz);
    if(b >= 0.)
        q = -0.5*(b + raiz);
    else
        q = -0.5*(b - raiz);
    t1 = q/a;
    t2 = c/q;
    if(t2 < t1)
    {
        *rr1 = t2;
        *rr2 = t1;
    } else
    {
        *rr1 = t1;
        *rr2 = t2;
    }
}

int EntradaArriba(double *tt2, double *v0y, double *y0, double PY, double R, double VY)
{
    double tt1;

#ifdef GRAV0
    if(VY != 0.)
    {
        tt1 = (Ly - AmpTecho - R - PY)/VY;
        if(VY < 0.)
        {
            if(tt1 < 0.)
                return 1;
            *tt2 = 0.;
            *y0 = PY;
        } else
        {
            if(tt1 < 0.)
            {
                *tt2 = 0.;
                *y0 = PY;
            } else
            {
                *tt2 = tt1;
                *y0  = Ly - AmpTecho - R;
            }
        }
    } else
    {
        if(PY < Ly - AmpTecho - R)
            return 1;
        *tt2 = 0.;
        *y0 = PY;
    }
    *v0y = VY;
#else
    double raiz = VY*VY - 2*g*(Ly - PY - R - AmpTecho);
    
    if(raiz < 0.) // Nunca entra en la region de choque
        return 1;
    raiz = sqrt(raiz);
    tt1  = (VY - raiz)/g;
    if(tt1 < 0.)
    {
        tt1 = (VY + raiz)/g;
        if(tt1 < 0.)
            return 1; // Nunca entra, viene de ahi
        *v0y = VY;
        *tt2 = 0.;
        *y0  = PY;
    } else
    {
        *y0  = Ly - AmpTecho - R;
        *v0y = raiz;
        *tt2 = tt1;
    }
#endif
    return 0;
}
int EntradaAbajo(double *tt2, double *v0y, double *y0, double PY, double R, double VY)
{
    double tt1;

#ifdef GRAV0
    if(VY != 0.)
    {
        tt1 = (AmpPiso + R - PY)/VY;
        if(VY > 0.)
        {
            if(tt1 < 0.)
                return 1;
            *tt2 = 0.;
            *y0 = PY;
        } else
        {
            if(tt1 < 0.)
            {
                *tt2 = 0.;
                *y0 = PY;
            } else
            {
                *tt2 = tt1;
                *y0  = AmpPiso + R;
            }
        }
    } else
    {
        if(PY > AmpPiso + R)
            return 1;
        *tt2 = 0.;
        *y0 = PY;
    }
    *v0y = VY;
#else
    double raiz;
    
    raiz = VY*VY + 2*g*(PY - R - AmpPiso);
    if(raiz < 0.) // Se esta en la region de choque y no saldra!!
    {
        *v0y = VY;
        *tt2 = 0.;
        *y0  = PY;
    } else
    {
        raiz = sqrt(raiz);
        tt1  = (VY + raiz)/g;
        if(tt1 < 0.) // Se esta en la region de choque y no saldra!!
        {
            *v0y = VY;
            *tt2 = 0.;
            *y0  = PY;
        } else
        {
            *v0y = -raiz;
            *tt2 = tt1;
            *y0  = AmpPiso + R;
            if(PY < AmpPiso + R) // ojo aqui podria chocar antes!
                return 2;
        }
    }
#endif
    return 0;
}

#ifdef NUMERICAL
double TArriba(int k)
{
    double y0, v0y, phiaux, tt2, phip;
    double t1, t2;
    int r;

    r = EntradaArriba(&tt2, &v0y, &y0, PosY[k], Radio[k], VelY[k]);
    if(r == 1)
        return infinite;
    phiaux = tt2 + Tpo[k] - TTecho*((int)((tt2 + Tpo[k])/TTecho));
    if(phiaux < TTecho2)
        phiaux += TTecho2;
    else
        phiaux -= TTecho2;
    phip   = phiaux;
    Raiz(&t1, &t2, CteTecho, phip, v0y, -AmpTecho, Ly - Radio[k], y0);
    if(t1 > 0. && t1 + phiaux < TTecho14
           && VelY[k] - g*(t1 + tt2) >= DTecho(Tpo[k] + t1 + tt2))
        return t1 + tt2;
    if(t2 > 0. && t2 + phiaux < TTecho14
           && VelY[k] - g*(t2 + tt2) >= DTecho(Tpo[k] + t2 + tt2))
        return t2 + tt2;
    phip   = phiaux - TTecho2;
    Raiz(&t1, &t2, -CteTecho, phip, v0y, AmpTecho, Ly - Radio[k], y0);
    if(t1 > 0. && t1 + phiaux >= TTecho14 && t1 + phiaux < TTecho34
           && VelY[k] - g*(t1 + tt2) >= DTecho(Tpo[k] + t1 + tt2))
        return t1 + tt2;
    if(t2 > 0. && t2 + phiaux >= TTecho14 && t2 + phiaux < TTecho34
           && VelY[k] - g*(t2 + tt2) >= DTecho(Tpo[k] + t2 + tt2))
        return t2 + tt2;
    phip  = phiaux - TTecho;
    Raiz(&t1, &t2, CteTecho, phip, v0y, -AmpTecho, Ly - Radio[k], y0);
    if(t1 > 0. && t1 + phiaux >= TTecho34 && t1 + phiaux < TTecho
           && VelY[k] - g*(t1 + tt2) >= DTecho(Tpo[k] + t1 + tt2))
        return t1 + tt2;
    if(t2 > 0. && t2 + phiaux >= TTecho34 && t2 + phiaux < TTecho
           && VelY[k] - g*(t2 + tt2) >= DTecho(Tpo[k] + t2 + tt2))
        return t2 + tt2;
    return infinite;
}
#else
double TArriba(int k)
{
    double y0, v0y, phiaux, tt2, phip, v0y2;
    double delta, t;
    int r;

    r = EntradaArriba(&tt2, &v0y, &y0, PosY[k], Radio[k], VelY[k]);
    if(r == 1)
        return infinite;
    v0y2   = v0y*v0y;
    phiaux = tt2 + Tpo[k] - TTecho*((int)((tt2 + Tpo[k])/TTecho));
    if(phiaux < TTecho2)
        phiaux += TTecho2;
    else
        phiaux -= TTecho2;
    phip  = phiaux;
    delta = v0y2 - 4.*CteTecho*phip*(g2*phip + v0y)
                        - 4.*g2CteTecho*(Ly - Radio[k] - AmpTecho - y0);
    if(delta >= 0.)
    {
        t = 0.5*(v0y - 2.*CteTecho*phip - sqrt(delta))/g2CteTecho;
        if(t > 0. && t + phiaux < TTecho14 &&
             VelY[k] - g*(t + tt2) >= DTecho(Tpo[k] + t + tt2))
        {
          //  tpo1++;
            return t + tt2;
        }
    }
    phip  = phiaux - TTecho2;
    delta = v0y2 + 4.*CteTecho*phip*(g2*phip + v0y)
                        - 4.*g2CteTechom*(Ly - Radio[k] + AmpTecho - y0);
    if(delta >= 0.)
    {
        t = 0.5*(v0y + 2.*CteTecho*phip - sqrt(delta))/g2CteTechom;
        if(t > 0. && t + phiaux >= TTecho14 && t + phiaux < TTecho34 &&
             VelY[k] - g*(t + tt2) >= DTecho(Tpo[k] + t + tt2))
        {
           // tpo2++;
            return t + tt2;
        }
        // solo deberia llegar aqui cuando no puede pasar hacia arriba
       /* t += sqrt(delta)/g2CteTechom;
        if(t > 0. && t + phiaux >= TTecho14 && t + phiaux < TTecho34 &&
             VelY[k] - g*(t + tt2) >= DTecho(Tpo[k] + t + tt2))
        {
        //    tpo3++;
            return t + tt2;
        }*/
    }
    phip  = phiaux - TTecho;
    delta = v0y2 - 4.*CteTecho*phip*(g2*phip + v0y)
                        - 4.*g2CteTecho*(Ly - Radio[k] - AmpTecho - y0);
    if(delta >= 0.)
    {
        t = 0.5*(v0y - 2.*CteTecho*phip - sqrt(delta))/g2CteTecho;
        if(t > 0. && t + phiaux >= TTecho34 && t + phiaux < TTecho &&
              VelY[k] - g*(t + tt2) >= DTecho(Tpo[k] + t + tt2))
        {
      //      tpo4++;
            return t + tt2;
        }
    }
    return infinite;
}
#endif

#ifdef NUMERICAL
double TAbajo(int k)
{
    double y0, v0y, phiaux, tt2, phip;
    double t1, t2, t;
    int r;

    r = EntradaAbajo(&tt2, &v0y, &y0, PosY[k], Radio[k], VelY[k]);
    if(r == 1)
        return infinite;
    if(r == 2)
    {
        phiaux = Tpo[k] - TPiso*((int)(Tpo[k]/TPiso));
        phip   = phiaux - TPiso2;
        Raiz(&t1, &t2, CtePiso, phip, VelY[k], -AmpPiso, Radio[k], PosY[k]);
        if(t1 > 0. && t1 + phiaux >= TPiso14 && t1 + phiaux < TPiso34
              && VelY[k] - g*t1 <= DPiso(Tpo[k] + t1))
             return t1;
        if(t2 > 0. && t2 + phiaux >= TPiso14 && t2 + phiaux < TPiso34
              && VelY[k] - g*t2 <= DPiso(Tpo[k] + t2))
             return t2;
        phip  = phiaux - TPiso;
        Raiz(&t1, &t2, -CtePiso, phip, VelY[k], AmpPiso, Radio[k], PosY[k]);
        if(t1 > 0. && t1 + phiaux >= TPiso34 && t1 + phiaux < TPiso
              && VelY[k] - g*t1 <= DPiso(Tpo[k] + t1))
             return t1;
        if(t2 > 0. && t2 + phiaux >= TPiso34 && t2 + phiaux < TPiso
              && VelY[k] - g*t2 <= DPiso(Tpo[k] + t2))
             return t2;
    }
    phiaux = tt2 + Tpo[k] - TPiso*((int)((tt2 + Tpo[k])/TPiso));
    phip   = phiaux;
    Raiz(&t1, &t2, -CtePiso, phip, v0y, AmpPiso, Radio[k], y0);
    if(t1 > 0. && t1 + phiaux < TPiso14
           && VelY[k] - g*(t1 + tt2) <= DPiso(Tpo[k] + t1 + tt2))
        return t1 + tt2;
    if(t2 > 0. && t2 + phiaux < TPiso14
           && VelY[k] - g*(t2 + tt2) <= DPiso(Tpo[k] + t2 + tt2))
        return t2 + tt2;
    phip   = phiaux - TPiso2;
    Raiz(&t1, &t2, CtePiso, phip, v0y, -AmpPiso, Radio[k], y0);
    if(t1 > 0. && t1 + phiaux >= TPiso14 && t1 + phiaux < TPiso34 &&
            VelY[k] - g*(t1 + tt2) <= DPiso(Tpo[k] + t1 + tt2))
        return t1 + tt2;
    if(t2 > 0. && t2 + phiaux >= TPiso14 && t2 + phiaux < TPiso34
           && VelY[k] - g*(t2 + tt2) <= DPiso(Tpo[k] + t2 + tt2))
        return t2 + tt2;
    phip  = phiaux - TPiso;
    Raiz(&t1, &t2, -CtePiso, phip, v0y, AmpPiso, Radio[k], y0);
    if(t1 > 0. && t1 + phiaux >= TPiso34 && t1 + phiaux < TPiso
           && VelY[k] - g*(t1 + tt2) <= DPiso(Tpo[k] + t1 + tt2))
        return t1 + tt2;
    if(t2 > 0. && t2 + phiaux >= TPiso34 && t2 + phiaux < TPiso
           && VelY[k] - g*(t2 + tt2) <= DPiso(Tpo[k] + t2 + tt2))
        return t2 + tt2;
    t = (VelY[k] + sqrt(VelY[k]*VelY[k] + 2*g*(PosY[k] - Radio[k] + AmpPiso)))/g;
    if(t > 0.)
        return t;  // Este es el tiempo si se pasa!
    return infinite; // NUNCA DEBERIA LLEGAR A ESTOS TIEMPOS!!!!!
}
#else
double TAbajo(int k)
{
    double y0, v0y, phiaux, tt2, phip, v0y2;
    double delta, t;
    int r;

    r = EntradaAbajo(&tt2, &v0y, &y0, PosY[k], Radio[k], VelY[k]);
    if(r == 1)
        return infinite;
    if(r == 2)
    {
        phiaux = Tpo[k] - TPiso*((int)(Tpo[k]/TPiso));
        phip   = phiaux - TPiso2;
        v0y2   = VelY[k]*VelY[k];
        delta  = v0y2 - 4.*CtePiso*phip*(g2*phip + VelY[k])
                      - 4.*g2CtePiso*(Radio[k] - AmpPiso - PosY[k]);
        if(delta >= 0.)
        {
            t = 0.5*(VelY[k] - 2.*CtePiso*phip + sqrt(delta))/g2CtePiso;
            if(t > 0. && t + phiaux >= TPiso14 && t + phiaux < TPiso34)
//                    && VelY[k] - g*t <= DPiso(Tpo[k] + t))
            {
                //tpo1++;
                return t;
            }
        }
        phip  = phiaux - TPiso;
        delta = v0y2 + 4.*CtePiso*phip*(g2*phip + VelY[k])
                     - 4.*g2CtePisom*(Radio[k] + AmpPiso - PosY[k]);
        if(delta >= 0.)
        {
            t = 0.5*(VelY[k] + 2.*CtePiso*phip + sqrt(delta))/g2CtePisom;
            if(t > 0. && t + phiaux >= TPiso34 && t + phiaux < TPiso)
//                   && VelY[k] - g*t <= DPiso(Tpo[k] + t))
                 return t;
       /*     double t1, t2;
  
            t1 = 0.5*(VelY[k] + 2.*CtePiso*phip - sqrt(delta))/g2CtePisom; // OJO PROBANDO creo q esta bien
            if(!(t1 > 0. && t1 + phiaux >= TPiso34 && t1 + phiaux < TPiso &&
                 VelY[k] - g*t1 <= DPiso(Tpo[k] + t1)))
                t1 = infinite;
            t2 = 0.5*(VelY[k] + 2.*CtePiso*phip + sqrt(delta))/g2CtePisom;
            if(!(t2 > 0. && t2 + phiaux >= TPiso34 && t2 + phiaux < TPiso &&
                   VelY[k] - g*t2 <= DPiso(Tpo[k] + t2)))
                t2 = infinite;
            if(t1 < t2)
            {
                tpo2++;
                return t1;
            } else if(t2 < infinite)
            {
                tpo3++;
                return t2;
            }*/
        }
    }
    v0y2   = v0y*v0y;
    phiaux = tt2 + Tpo[k] - TPiso*((int)((tt2 + Tpo[k])/TPiso));
    phip   = phiaux;
    delta  = v0y2 + 4.*CtePiso*phip*(g2*phip + v0y)
                        - 4.*g2CtePisom*(Radio[k] + AmpPiso - y0);
    if(delta >= 0.)
    {
        t = 0.5*(v0y + 2.*CtePiso*phip + sqrt(delta))/g2CtePisom;
        if(t > 0. && t + phiaux < TPiso14)
//            && VelY[k] - g*(t + tt2) <= DPiso(Tpo[k] + t + tt2))
            return t + tt2;
       /* double t1, t2;
        
        raiz = sqrt(delta);
        t1 = 0.5*(v0y + 2.*CtePiso*phip - raiz)/g2CtePisom; // OJO PROBANDO creo q esta bien
        if(!(t1 > 0. && t1 + phiaux < TPiso14 &&
             VelY[k] - g*(t1 + tt2) <= DPiso(Tpo[k] + t1 + tt2)))
            t1 = infinite;
        t2 = 0.5*(v0y + 2.*CtePiso*phip + raiz)/g2CtePisom;
        if(!(t2 > 0. && t2 + phiaux < TPiso14 &&
             VelY[k] - g*(t2 + tt2) <= DPiso(Tpo[k] + t2 + tt2)))
            t2 = infinite;
        if(t1 < t2)
        {
            tpo4++;
            return t1 + tt2;
        } else if(t2 < infinite)
        {
            tpo5++;
            return t2 + tt2;
        }*/
    }
    phip  = phiaux - TPiso2;
    delta = v0y2 - 4.*CtePiso*phip*(g2*phip + v0y)
                        - 4.*g2CtePiso*(Radio[k] - AmpPiso - y0);
    if(delta >= 0.)
    {
        t = 0.5*(v0y - 2.*CtePiso*phip + sqrt(delta))/g2CtePiso;
        if(t > 0. && t + phiaux >= TPiso14 && t + phiaux < TPiso34)
//            && VelY[k] - g*(t + tt2) <= DPiso(Tpo[k] + t + tt2))
        {
//            tpo6++;
            return t + tt2;
        }
    }
    phip  = phiaux - TPiso;
    delta = v0y2 + 4.*CtePiso*phip*(g2*phip + v0y)
                        - 4.*g2CtePisom*(Radio[k] + AmpPiso - y0);
    if(delta >= 0.)
    {
        t = 0.5*(v0y + 2.*CtePiso*phip + sqrt(delta))/g2CtePisom;
        if(t > 0. && t + phiaux >= TPiso34 && t + phiaux < TPiso)
//            && VelY[k] - g*(t + tt2) <= DPiso(Tpo[k] + t + tt2))
            return t + tt2;
       /* double t1, t2;
        
        raiz = sqrt(delta);
        t1 = 0.5*(v0y + 2.*CtePiso*phip - raiz)/g2CtePisom; // Solo sirve si no sale!
        if(!(t1 > 0. && t1 + phiaux >= TPiso34 && t1 + phiaux < TPiso &&
             VelY[k] - g*(t1 + tt2) <= DPiso(Tpo[k] + t1 + tt2)))
            t1 = infinite;
        t2 = 0.5*(v0y + 2.*CtePiso*phip + raiz)/g2CtePisom;
        if(!(t2 > 0. && t2 + phiaux >= TPiso34 && t2 + phiaux < TPiso &&
             VelY[k] - g*(t2 + tt2) <= DPiso(Tpo[k] + t2 + tt2)))
            t2 = infinite;
        if(t1 < t2)
        {
            tpo7++;
            return t1 + tt2;
        } else if(t2 < infinite)
        {
            tpo8++;
            return t2 + tt2;
        }*/
    }
    t = (VelY[k] + sqrt(VelY[k]*VelY[k] + 2*g*(PosY[k] - Radio[k] + AmpPiso)))/g;
    if(t > 0.)
        return t;  // Este es el tiempo si se pasa!
    return infinite; // NUNCA DEBERIA LLEGAR A ESTOS TIEMPOS!!!!!
}
#endif

double TchoqueDW(int k) //tiempo pa chocar contra pared
{
    double ty0, ty1, mint, tx, tz;
    int w, wx, wz;
    
    if(VelX[k] != 0.)
    {
        if(VelX[k] > 0.)
        {
            tx = (Lx - PosX[k] - Radio[k])/VelX[k];
            wx = 2;
        } else
        {
            tx = (Radio[k] - PosX[k])/VelX[k];
            wx = 0;
        }
    } else
        tx = infinite;
    ty0 = TAbajo(k);
    //printf("t1 = %d t2 = %d t3 = %d t4 = %d t5 = %d t6 = %d t7 = %d t8 = %d\n", tpo1, tpo2, tpo3, tpo4, tpo5, tpo6, tpo7, tpo8);
    ty1 = TArriba(k);
    //printf("t1 = %d t2 = %d t3 = %d t4 = %d\n", tpo1, tpo2, tpo3, tpo4);
    if(VelZ[k] != 0.)
    {
        if(VelZ[k] > 0.)
        {
            tz = (Lz - PosZ[k] - Radio[k])/VelZ[k];
            wz = 5;
        } else
        {
            tz = (Radio[k] - PosZ[k])/VelZ[k];
            wz = 4;
        }
    } else
        tz = infinite;
    mint = infinite;
    w = -1;
    if(tx < mint)  // Ahora siempre son > 0 los tiempos ya calculados
    {
        mint = tx;
        w = wx;
    }
    if(ty0 < mint)
    {
        mint = ty0;
        w = 1;
    }
    if(ty1 < mint)
    {
        mint = ty1;
        w = 3;
    }
    if(tz < mint)
    {
        mint = tz;
        w = wz;
    }
    Wall[k] = w;
#ifdef DATOSABAJO
    if(w == 1)
        printf("choque abajo de %d en %f\n", k, reloj + mint);
#endif
#ifdef DATOSARRIBA
    if(w == 3)
        printf("choque arriba de %d en %f\n", k, reloj + mint);
#endif
    return Tpo[k] + mint;
} 

void EventosNuevos(int k)
{
    int j, c;
    double tj;
    
    for(j = 0; j < k; j++)
    {
        tj = TchoqueDD(k, j);
        c  = Choques[j];
        if(tj < infinite)
            AgregaEvento(k, j, 1, c, tj);
    }
    j++;
    for(; j < N; j++)
    {
        tj = TchoqueDD(k, j);
        c  = Choques[j];
        if(tj < infinite)
            AgregaEvento(k, j, 1, c, tj);
    }
    tj = TchoqueDW(k); // siempre hay choque con alguna pared (la de abajo)
    AgregaEvento(k, Wall[k], 0, Choques[k], tj); //1er 0=>W, 2do 0=>C[pared]    
}

void NextEvent()		// para entendidos
{
    int jsoc, vale, e, kind, k, l;
    double tnext;
#ifdef MOSTRARENERGIA
    double ek, eu;
#endif
    
    do
    {
        knext = B[0];
        vale  = 1;
        e     = SE[knext];
        tnext = TpoE[knext][e];
        jsoc  = Socio[knext][e];  //necesario
        kind  = Kind[knext][e];
//        printf(" t->%f ", tnext);
        if(kind && CC[knext][e] != Choques[jsoc])
        {
            vale = 0;
            TpoE[knext][e] = infinite;
            Ganador(knext);
            UpGradeCBT(knext);
        }
    } while(vale == 0);
    //printf(" tipo de choque = %d  (%d  %d) \n", kind, knext, jsoc);

#ifdef REVISIONMALAS
    Verificar(tnext);
    cantverif++;
    if(contadorpiso/10000 >= ultimoPiso)
    {
        printf(" %d / %d \r", contadorpiso, cantverif);
        fflush(stdout);
        ultimoPiso++;
    }
#endif
#ifdef MOSTRARENERGIA
    ek = EnergiaCinetica();
    eu = EnergiaPotencial();
    printf("ENERGIAS K = %f U = %f TOT = %f\n", ek, eu, ek + eu);
#endif
    
    while(tF < tnext)
    {
#ifdef GRAFICO
        if(sicamfile && tF > nextcamevent)
            LeerSiguienteEvCam();
        DibujarPantalla();
#endif   
#ifdef ARCHIVO
        GrabarTodo();
#endif   
        tF += DELTAPANT;
    }
   
    reloj = tnext;
    if(kind)
    {
        Mueve(knext);
        Mueve(jsoc);
        ChoqueDD(knext,jsoc); //aplica los Choque[a]++
        Choques[knext]++;
        Choques[jsoc]++;
        LimpiaLista(knext);
        EventosNuevos(knext);
        LimpiaLista(jsoc);
        EventosNuevos(jsoc);
        Ganador(knext);
        Ganador(jsoc);
        UpGradeCBT(knext);
        UpGradeCBT(jsoc);
    } else   //choque con pared
    {
        Mueve(knext);  // OJO DESPUES SE MUEVE FORZADO
        ChoqueW();
        LimpiaLista(knext);
        EventosNuevos(knext);  
        Ganador(knext);
        UpGradeCBT(knext);
    }
    if(reloj > CteReloj)
    {
        for(k = 0; k < N; k++)
        {
            Tpo[k] -= CteReloj;
            TpoSE[k] -= CteReloj;
            for(l = 0; l < Ll; l++)
                TpoE[k][l] -= CteReloj;
        }
        reloj -= CteReloj;
        tF -= CteReloj;
#ifdef GRAFICO
        if(sicamfile)
            nextcamevent -= CteReloj;
        cantmovreloj++;
#endif
    }
}

void Termaliza()
{
    int s;
    
    s = 0;
    printf("Termalizando con T = %f\n", Tb);
    do
    {
        NextEvent();
#ifdef GRAFICO
        if((!sicamfile) && s%cada == 0)
        {
            double c = 4.*(s%(N*N))*Lx/(N*N);
            
            IniciarCamara(2.*Lx - c, Ly, -3.*Lz, c - 1.5*Lx, -0.5*Ly, 3.5*Lz, 28.*Lz);
        }
#endif
        s++;
    } while(s < 20000000);   /* LOOP PRINCIPAL */
}

void XVinic() 	//muy bien probado
{
    int j, k, p;
    
    double na, nb;
    double r;
    double epsi = 0.001;
    
    reloj    = 0.;
    AmpTecho = A;
    AmpPiso  = A;
    TTecho   = T;  // Siempre se necesita que TTecho/TPiso sea racional!
    TPiso    = T;  // Por ahora se necesita que T sea entero
    TTecho2  = 0.5*TTecho;  // Eso se debe arreglar para poder variar
    TPiso2   = 0.5*TPiso;    // TTecho y TPiso por separado
    TTecho14 = 0.25*TTecho;
    TPiso14  = 0.25*TPiso;
    TTecho34 = 0.75*TTecho;
    TPiso34  = 0.75*TPiso;
    CteReloj = T*3.;
    cantmovreloj = 0;
    tpo1 = tpo2 = tpo3 = tpo4 = tpo5 = tpo6 = tpo7 = tpo8 = 0;
    CteTecho = 16.*AmpTecho/(TTecho*TTecho); // ojo: C != g/2 !!
    CtePiso  = 16.*AmpPiso/(TPiso*TPiso);
    if(CteTecho < g || CtePiso < g)
    {
        printf("No se ejecutara, pues no se tiene que 16*A/T^2 > g\n");
        exit(1);
    }
    g2 = 0.5*g;
    g2CtePiso   = g2 + CtePiso;
    g2CtePisom  = g2 - CtePiso;
    g2CteTecho  = g2 + CteTecho;
    g2CteTechom = g2 - CteTecho;
    rn1   = rn + 1.;
    RN1   = RN + 1.;
    rt13  = (rt + 1.)/3.;
    RT13  = (RT + 1.)/3.;
    tanFi = rn1*kappa/rt13;
    TANfI = RN1*KAP/RT13;
    contadorpiso = 0;
    arreglospiso = 0;
    k = 0;
    j = 0;
    p = 0;
    
    r = r1 + epsi;
    na = (int)(Lx/(2.*r));
    nb = (int)((Lx/(2.*r)) - 1.);
do
  { int i=0;
    while(i<na && k<N)
    { PosX[k] = r + 2.0*r*i;
      PosZ[k] = epsi+(epsi + (sqrt(3)*j+1)*r);     //<<<<<<<<<<<<<<<<<
      if(PosZ[k]>Lz-r1)
      {  printf("La P%d no cabe!\n",k);
         exit(0);
      }
      i++;
      PosY[k]  = (Ly - 2*r1)*(((double)rand())/RAND_MAX) + r1 + Piso(0.);
    Radio[k] = r1;
      k++;
    }
    j++;
    i=0;
    while(i<nb && k<N)
    { PosX[k] = 2.0*r + 2.0*r*i;
      PosZ[k] = epsi+(epsi + (sqrt(3)*j+1)*r);   
      if(PosZ[k]>Lz-r1)
      {  printf("La P%d no cabe!\n",k);
         exit(0);
      }
     i++;
     PosY[k]  = (Ly - 2*r1)*(((double)rand())/RAND_MAX) + r1 + Piso(0.);
    Radio[k] = r1;
     k++;
    }
    j++;
  }while(k<N);
  /*  while(k < N)
    {
        int l;
        
        Radio[k] = r1*(1. + VARRADIO*(0.5 - ((double)rand())/RAND_MAX));
        PosX[k]  = (Lx - 2*Radio[k])*(((double)rand())/RAND_MAX) + Radio[k];
        PosY[k]  = (Ly - 2*Radio[k])*(((double)rand())/RAND_MAX) + Radio[k] + Piso(0.);
        PosZ[k]  = (Lz - 2*Radio[k])*(((double)rand())/RAND_MAX) + Radio[k];
        //PosZ[k] = 2.;
        for(l = 0; l < k; l++)
        {
            double x, y, z, rr;
            
            x  = PosX[k] - PosX[l];
            y  = PosY[k] - PosY[l];
            z  = PosZ[k] - PosZ[l];
            rr = Radio[k] + Radio[l];
            if(x*x + y*y + z*z < rr*rr + EPSREVISION)
                break;
        }
        if(l == k)
        {
            k++;
            j = 0;
            continue;
        }
        j++;
        if(j > p)
            p = j;
        if(j == MAXPRUEBAS)
        {
            printf("La %d no cabe luego de %d pruebas\n", k + 1, j);
            printf("Aumentar MAXPRUEBAS o hacer N = %d\n", k);
            exit(1);
        }
    }*/
    printf("Numero maximo de pruebas usadas fue %d\n", p);
    for(k = 0; k < N; k++) //sortea velocidades
    {
        VelX[k] = sqrt(Tb)*(0.5 - drand48());
        VelY[k] = sqrt(Tb)*(0.6 - drand48());
        VelZ[k] = sqrt(Tb)*(0.5 - drand48());
//        VelZ[k] = 0.;
        Tpo[k]  = 0.;
        AngX[k] = 0.;
        AngY[k] = 1.;  // Apuntan hacia arriba
        AngZ[k] = 0.;
        Rot[k]  = 0.;
        WelX[k] = 0.5 - drand48();
        WelY[k] = 0.5 - drand48();
        WelZ[k] = 0.5 - drand48();
        if(PosY[k] < 0.3*Ly)
            sprintf(Col[k],"%s","green");
        if(PosY[k] > 0.3*Ly)
            sprintf(Col[k],"%s","blue");
    }
}  

void Inic()
{
    int i;

    tF     = 0.;
    XVinic();
    for(i = 0; i < N; i++)
    {
        Choques[i] = 0;
        LimpiaLista(i);
        EventosNuevos(i);
        Ganador(i);
    }
    IniciaCBT(); 
}

// ___________________________________main__________________________________________
int main(int argc, char *argv[])
{
    Tb     = 65.0;
    srand48(111);
    Inic();

#ifdef GRAFICO
    sicamfile = 0;
    IniciarPantalla(TPant);
    if(argc > 1)
        IniciarArchCam(argv[1]);
    if(sicamfile)
        LeerSiguienteEvCam();
    else
        IniciarCamara(Lx, Ly/2, -Lz/2, -0.5, 0, 1, 8*Lz);
    DibujarPantalla();
#endif
#ifdef ARCHIVO
    IniciarArchivo("out.txt");
    GrabarTodo();
#endif
#ifdef REVISIONMALAS
    ultimoPiso = 0;
    cantverif = 0;
#endif

    printf("-------- fin inicializacion --------:\n");
    printf("N = %d\tLx = %f\tLy = %f\tLz = %f\nr1 = %f\tVARRADIO = %f\n", N, Lx, Ly, Lz, r1, VARRADIO);
    printf("rn = %f\trt = %f\tkappa = %f\ng = %f\tCOTAELAS = %f\n", rn, rt, kappa, g, COTAELAST);
    printf("RN = %f\tRT = %f\tKAP = %f\n", RN, RT, KAP);
    printf("A = %f\tT = %f\n", A, T);
    
    Termaliza();

    printf("\n\n                           -- F I N --\n\n\n");

#ifdef GRAFICO
    CerrarPantalla();
#endif
#ifdef ARCHIVO
    CerrarArchivo();
#endif
    return 0;
}
