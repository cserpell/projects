//---------------------------------------------------------------------------
#ifndef PiezaH
#define PiezaH
#define MAXLUG 1024  // Suponiendo m�ximo 32x32 por mientras
//---------------------------------------------------------------------------
typedef struct Pieza
{
  bool horizontal;
  int largo;
  int pos;
} Pieza;
class Tablero
{
 private:
  int numpiezas;
  Pieza Piezas[MAXLUG];
  int marcada;
  int ysalida;
  bool ganado;
  int mx;
  int my;
  int puede[MAXLUG];
 public:
  int DisponibleLugar(int x, int y);
  int TipoLugar(int x, int y);
  int MX(void);
  int MY(void);
  int *Descrip(void);
  int MoverPieza(int x, int y, int nx, int ny);
  int PiezaEn(int x, int y);
  bool AgregarPieza(int x, int y, int largo, bool horiz, bool unica = false);
  bool Ganado(void);
  bool Definido(void);
  bool DefinirTamano(int x, int y);
  bool DefinirSalida(int y);
  void Limpiar(void);
  Pieza PiezaEnPos(int x, int y);
  Tablero()
  {
    marcada = -1;
    ysalida = -1;
    mx = 0;
    my = 0;
    ganado = false;
    numpiezas = 0;
    for(int i = 0; i < MAXLUG; i++)
      puede[i] = -1;
  }
  Tablero(Tablero *orig)
  {
    marcada = orig->marcada;
    ysalida = orig->ysalida;
    mx = orig->mx;
    my = orig->my;
    ganado = orig->ganado;
    numpiezas = orig->numpiezas;
    for(int i = 0; i < MAXLUG; i++)
      puede[i] = orig->puede[i];
    for(int i = 0; i < numpiezas; i++)
    {
      Piezas[i].pos = orig->Piezas[i].pos;
      Piezas[i].largo = orig->Piezas[i].largo;
      Piezas[i].horizontal = orig->Piezas[i].horizontal;
    }
  }
};
#endif
