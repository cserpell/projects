//---------------------------------------------------------------------------
#ifndef FormH
#define FormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "Pieza.h"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
 __published:	// IDE-managed Components
  TMemo *Memo1;
  TPaintBox *PaintBox1;
  TGroupBox *GroupBox1;
  TLabel *Label5;
  TLabel *Label2;
  TLabel *Label3;
  TEdit *Edit1;
  TEdit *Edit2;
  TEdit *Edit3;
  TEdit *Edit4;
  TButton *Mover;
  TGroupBox *GroupBox2;
  TLabel *Label8;
  TLabel *movlbl;
  TLabel *Label1;
  TLabel *Label7;
  TGroupBox *GroupBox3;
  TEdit *posx;
  TEdit *larg;
  TCheckBox *hori;
  TEdit *posy;
  TCheckBox *unic;
  TLabel *Label11;
  TLabel *Label12;
  TLabel *Label13;
  TButton *Agreg;
  TGroupBox *GroupBox4;
  TButton *Button2;
  TLabel *Label4;
  TEdit *Medida;
  TGroupBox *GroupBox5;
  TLabel *Label9;
  TEdit *Numpiez;
  TButton *MoverAzar;
  TButton *Button1;
  TLabel *Label10;
  TLabel *gana;
  TButton *Dibujar;
  TButton *PRES;
  TLabel *RESS;
  TMemo *MemoRES;
  TLabel *Label6;
        TButton *Button3;
  void __fastcall MoverAzarClick(TObject *Sender);
  void __fastcall Button1Click(TObject *Sender);
  void __fastcall DibujarClick(TObject *Sender);
  void __fastcall MoverClick(TObject *Sender);
  void __fastcall PaintBox1MouseDown(TObject *Sender, TMouseButton Button,
                                               TShiftState Shift, int X, int Y);
  void __fastcall AgregClick(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall PRESClick(TObject *Sender);
  void __fastcall unicMouseUp(TObject *Sender, TMouseButton Button,
                                               TShiftState Shift, int X, int Y);
        void __fastcall Button3Click(TObject *Sender);
 private:	// User declarations
  Tablero *Juego;
  void DibujarPiezasCanvas(void);
  void DibujarPiezas(void);
  bool escogido;
  int movidas;
 public: 	// User declarations
  __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
