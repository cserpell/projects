//---------------------------------------------------------------------------
#ifndef SolverH
#define SolverH
//---------------------------------------------------------------------------
#include "Pieza.h"
#define MAXARR 4098
#define MAXMOV 200
class Solver
{
 private:
  int /* * */ arreglopuede[MAXARR][MAXLUG]; //
  int numtabs;
  //Ahora tengo que crear un arreglo donde guardar
  int nummov;
  int AX[MAXMOV];
  int AY[MAXMOV];
  int NX[MAXMOV];
  int NY[MAXMOV];
 public:
  int ResolverTablero(Tablero *pru);
  void Inicializar();
  int ax(int mov);
  int ay(int mov);
  int nx(int mov);
  int ny(int mov);
};
#endif
