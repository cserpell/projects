//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Solver.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
// Ahora una funci�n que resuelva el puzle
// DE AQUI EN ADELANTE TODAVIA NO HAY NADA MUY BIEN
void Solver::Inicializar(void)
{
  numtabs = 0;
  nummov = 0;
}
// ODIO HACER ESTO, PERO...
// Hago obviamente una funci�n recursiva que lo resuelva
int Solver::ResolverTablero(Tablero *pru)
{
  int mx, my;
  int *p;

  if(pru->Ganado())
    return nummov;
  if(!pru->Definido())
    return 0;
  if(numtabs == MAXARR)
    return 0;
  p = pru->Descrip();
  for(int i = 0; i < numtabs; i++)
  {
    bool enc = true;

    for(int j = 0; j < MAXLUG; j++)
    {
      if(enc && arreglopuede[i][j] != p[j])
        enc = false;
      arreglopuede[numtabs][j] = p[j];
    }
    if(enc)
      return 0;
  } /*
  for(int i = 0; i < numtabs; i++)        //
    if(arreglopuede[i] == pru->puede)     //
      return false;                       //
  if(numtabs == MAXARR)                   //
    return false;                         //
  arreglopuede[numtabs] = pru->puede;     // */
  numtabs++;
  if(nummov == MAXMOV)
    return 0;
  mx = pru->MX();
  my = pru->MY();
  // Por ahora una forma charcha de hacerlo
  for(int i = 0; i < mx; i++)
    for(int j = 0; j < my; j++)
    {
      int t = pru->TipoLugar(i, j);

      if(t == 1)
        for(int nx = 0; nx < mx; nx++)
        {
          int r = pru->MoverPieza(i, j, nx, j);

          if(r != 0)
          {
            AX[nummov] = i;
            AY[nummov] = j;
            if(nx > i)
              NX[nummov] = i + r;
            else
              NX[nummov] = i - r;
            NY[nummov] = j;
            nummov++;
            if(ResolverTablero(pru) != 0)
              return nummov;
            nummov--;
            pru->MoverPieza(NX[nummov], j, i, j);
          }
        }
      else if(t == 2)   // Verticales
        for(int ny = 0; ny < my; ny++)
        {
          int r = pru->MoverPieza(i, j, i, ny);

          if(r != 0)
          {
            AX[nummov] = i;
            AY[nummov] = j;
            NX[nummov] = j;
            if(ny > j)
              NY[nummov] = j + r;
            else
              NY[nummov] = j - r;
            nummov++;
            if(ResolverTablero(pru) != 0)
              return nummov;
            nummov--;
            pru->MoverPieza(i, NY[nummov], i, j);
          }
        }
    }
  return 0;
}
int Solver::ax(int mov)
{
  if(mov < nummov && mov >= 0)
    return AX[mov];
  return -1;
}
int Solver::ay(int mov)
{
  if(mov < nummov && mov >= 0)
    return AY[mov];
  return -1;
}
int Solver::nx(int mov)
{
  if(mov < nummov && mov >= 0)
    return NX[mov];
  return -1;
}
int Solver::ny(int mov)
{
  if(mov < nummov && mov >= 0)
    return NY[mov];
  return -1;
}

