//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Pieza.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
int Tablero::DisponibleLugar(int x, int y)
{
  if(x >= mx || x < 0 || y < 0 || y >= my)
    return false;
  // Tengo un arreglo de enteros para esto...
  if(puede[mx*y + x] != -1)
    return false;
  return true;
}
// Retorna si hay una pieza horizontal (1) o vertical (2) o nada (0) en el lugar
int Tablero::TipoLugar(int x, int y)
{
  if(x >= mx || x < 0 || y < 0 || y >= my)
    return -1;     // Error de coordenadas
  if(puede[mx*y + x] == -1)
    return 0;
  if(Piezas[puede[mx*y + x]].horizontal)
    return 1;
  return 2;
}
// Simple
void Tablero::Limpiar(void)
{
  marcada = -1;  // Lo �nico que no cambio son las medidas
  ganado = false;
  numpiezas = 0;
  for(int i = 0; i < MAXLUG; i++)
    puede[i] = -1;
}
// Hago todos los casos
int Tablero::MoverPieza(int x, int y, int nx, int ny)
{
  int tomar, num = 0;

  if(x > mx || x < 0 || y < 0 || y > my)
    return 0;
  tomar = puede[mx*y + x];
  if(tomar == -1)
    return 0;
  if(Piezas[tomar].horizontal)
  {
    if(nx > x)
    {
      for(int i = x; i < nx; i++)
      {
        if(DisponibleLugar(Piezas[tomar].pos + Piezas[tomar].largo, y))
        {
          puede[mx*y + Piezas[tomar].pos + Piezas[tomar].largo] = tomar;
          puede[mx*y + Piezas[tomar].pos] = -1;
          Piezas[tomar].pos++;
          num++;
        } else if(tomar == marcada &&
                  Piezas[tomar].pos + Piezas[tomar].largo == mx && y == ysalida)
        {
          puede[mx*y + Piezas[tomar].pos] = -1;
          ganado = true;
          Piezas[tomar].pos++;
          num++;
        }
      }
      return num;
    }
    if(nx < x)
    {
      for(int i = nx; i < x; i++)
      {
        if(Piezas[tomar].pos + Piezas[tomar].largo > mx)
        {
          if(DisponibleLugar(Piezas[tomar].pos - 1, y))
          {
            puede[mx*y + Piezas[tomar].pos - 1] = tomar;
            Piezas[tomar].pos--;
            num++;
          }
        } else if(DisponibleLugar(Piezas[tomar].pos - 1, y))
        {
          puede[mx*y + Piezas[tomar].pos - 1] = tomar;
          puede[mx*y + Piezas[tomar].pos + Piezas[tomar].largo - 1] = -1;
          Piezas[tomar].pos--;
          num++;
        }
      }
      return num;
    }
  } else
  {
    if(ny > y)
    {
      for(int i = y; i < ny; i++)
        if(DisponibleLugar(x, Piezas[tomar].pos + Piezas[tomar].largo))
        {
          puede[mx*(Piezas[tomar].pos + Piezas[tomar].largo) + x] = tomar;
          puede[mx*Piezas[tomar].pos + x] = -1;
          Piezas[tomar].pos++;
          num++;
        }
      return num;
    }
    if(ny < y)
    {
      for(int i = ny; i < y; i++)
        if(DisponibleLugar(x, Piezas[tomar].pos - 1))
        {
          puede[mx*(Piezas[tomar].pos - 1) + x] = tomar;
          puede[mx*(Piezas[tomar].pos + Piezas[tomar].largo - 1) + x] = -1;
          Piezas[tomar].pos--;
          num++;
        }
      return num;
    }
  }
  return 0;
}
// Define la salida en la altura y (desde arriba)
bool Tablero::DefinirSalida(int y)
{
  if(y >= mx || y < 0)
    return false;
  ysalida = y;
  return true;
}
// La funci�n m�s importante...
bool Tablero::AgregarPieza(int x, int y, int largo, bool horiz, bool unica)
{
  int max;

  if(unica && (marcada != -1))
    return false;
  if(unica && !horiz)   // La marcada s�lo ser� horizontal
    return false;
  if(x < 0 || y < 0 || largo < 1)
    return false;
  if(horiz)
  {
    max = x + largo;
    if(max > mx)
      return false;
    for(int i = x; i < max; i++)
      if(!DisponibleLugar(i, y))
        return false;
    for(int i = x; i < max; i++)
      puede[mx*y + i] = numpiezas;
    Piezas[numpiezas].pos = x;
    Piezas[numpiezas].largo = largo;
    Piezas[numpiezas].horizontal = true;
    if(unica)
      marcada = numpiezas;
    numpiezas++;
    return true;
  }
  max = y + largo;
  if(max > my)
    return false;
  for(int i = y; i < max; i++)
    if(!DisponibleLugar(x, i))
      return false;
  for(int i = y; i < max; i++)
    puede[mx*i + x] = numpiezas;
  Piezas[numpiezas].pos = y;
  Piezas[numpiezas].largo = largo;
  Piezas[numpiezas].horizontal = false;
  if(unica)
    marcada = numpiezas;
  numpiezas++;
  return true;
}
// Funciones para no acceder directamente a las variables...
bool Tablero::Ganado(void)
{
  return ganado;
}
bool Tablero::Definido(void)
{
  if(marcada == -1 || ysalida == -1)
    return false;
  return true;
}
int Tablero::MX(void)
{
  return mx;
}
int Tablero::MY(void)
{
  return my;
}
int *Tablero::Descrip(void)
{
  return puede;
}

bool Tablero::DefinirTamano(int x, int y)
{
  if(x * y > MAXLUG)
    return false;
  mx = x;
  my = y;
  if(!DefinirSalida(ysalida))
    ysalida = -1;
  return true;
}
// Retorna qu� pieza est� en (x,y) + 1, si no hay retorna 0 si hay error, -1
int Tablero::PiezaEn(int x, int y)
{
  if(x >= mx || x < 0 || y < 0 || y >= my)
    return -1;     // Error de coordenadas
  return puede[mx*y + x] + 1;
}
Pieza Tablero::PiezaEnPos(int x, int y)
{
  int i = PiezaEn(x, y);

  if(i == -1)
  {
    Pieza ret;

    ret.pos = 0;
    ret.largo = 0;
    ret.horizontal = false;
    return ret;     // Error de coordenadas
  }
  return Piezas[i - 1];
}

