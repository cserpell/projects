//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>
#pragma hdrstop

#include "Form.h"
#include "Solver.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
  escogido = false;
  movidas = 0;
  Juego = new Tablero();
}
//---------------------------------------------------------------------------
void TForm1::DibujarPiezas(void)
{
  int m = StrToInt(Medida->Text);

  Memo1->Clear();
  for(int i = 0; i < m; i++)
  {
    AnsiString linea = "";

    for(int j = 0; j < m; j++)
    {
      linea += (char)(Juego->PiezaEn(j, i) + '0');
/*      int tipo = Juego.TipoLugar(j, i);

      if(tipo == 0)
        linea += " ";
      else if(tipo == 1)
        linea += "-";
      else
        linea += "|";*/
    }
    Memo1->Lines->Add(linea);
  }
}
//---------------------------------------------------------------------------
void TForm1::DibujarPiezasCanvas(void)
{
  int m = StrToInt(Medida->Text);
  int arx = PaintBox1->Width / m;
  int ary = PaintBox1->Height / m;

  PaintBox1->Canvas->Pen->Color = clBlack;
  PaintBox1->Canvas->Brush->Color = clBlack;
  PaintBox1->Canvas->FillRect(TRect(0, 0, PaintBox1->Width, PaintBox1->Height));
  for(int j = 0; j < m; j++)
  {
    for(int i = 0; i < m; i++)
    {
      int p = Juego->PiezaEn(i, j);

      if(p == 0)
      {
        PaintBox1->Canvas->Brush->Color = clBlack;
        PaintBox1->Canvas->FillRect(TRect(i*arx, j*ary, (i+1)*arx, (j+1)*ary));
      } else
      {
        PaintBox1->Canvas->Brush->Color = (TColor)(p*0x002222);
        PaintBox1->Canvas->FillRect(TRect(i*arx, j*ary, (i+1)*arx, (j+1)*ary));
      }
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MoverAzarClick(TObject *Sender)
{
  int x, y, nx, ny, tipo, alc;
  int m = StrToInt(Medida->Text);

  x = random(m);
  y = random(m);
  nx = random(m);
  ny = random(m);
  tipo = Juego->TipoLugar(x, y);
  if(tipo == 0)
    return;
  else if(tipo == 1)
    ny = y;
  else
    nx = x;
  Edit1->Text = IntToStr(x);
  Edit2->Text = IntToStr(y);
  Edit3->Text = IntToStr(nx);
  Edit4->Text = IntToStr(ny);
  alc = Juego->MoverPieza(x, y, nx, ny);
  Label7->Caption = IntToStr(alc);
  if(alc != 0)
  {
    DibujarPiezas();
    DibujarPiezasCanvas();
    movidas++;
    movlbl->Caption = IntToStr(movidas);
    if(Juego->Ganado())
      gana->Caption = "SI";
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  int n = 0;
  int m = StrToInt(Medida->Text);
  int np = StrToInt(Numpiez->Text);

  Juego->DefinirTamano(m, m);
  Juego->Limpiar();
  unic->Checked = true;
  unic->Enabled = true;
  do
  {
    if(Juego->AgregarPieza(random(m), random(m), random(m - 1) + 2, random(2)))
      n++;
  } while(n < np);
  DibujarPiezas();
  DibujarPiezasCanvas();
  movidas = 0;
  movlbl->Caption = IntToStr(movidas);
  gana->Caption = "NO";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::DibujarClick(TObject *Sender)
{
  DibujarPiezas();
  DibujarPiezasCanvas();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MoverClick(TObject *Sender)
{
  int alc = Juego->MoverPieza(StrToInt(Edit1->Text),
          StrToInt(Edit2->Text), StrToInt(Edit3->Text), StrToInt(Edit4->Text));

  Label7->Caption = IntToStr(alc);
  if(alc != 0)
  {
    movidas++;
    movlbl->Caption = IntToStr(movidas);
    DibujarPiezasCanvas();
    DibujarPiezas();
    if(Juego->Ganado())
      gana->Caption = "SI";
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBox1MouseDown(TObject *Sender,
                           TMouseButton Button, TShiftState Shift, int X, int Y)
{
  int m = StrToInt(Medida->Text);
  int arx = PaintBox1->Width / m;
  int ary = PaintBox1->Height / m;
  int alc;

  if(!escogido)
  {
    Edit1->Text = IntToStr(X/arx);
    Edit2->Text = IntToStr(Y/ary);
    escogido = true;
    return;
  }
  Edit3->Text = IntToStr(X/arx);
  Edit4->Text = IntToStr(Y/ary);
  escogido = false;

  alc = Juego->MoverPieza(StrToInt(Edit1->Text),
          StrToInt(Edit2->Text), StrToInt(Edit3->Text), StrToInt(Edit4->Text));
  Label7->Caption = IntToStr(alc);
  if(alc != 0)
  {
    movidas++;
    movlbl->Caption = IntToStr(movidas);
    DibujarPiezasCanvas();
    DibujarPiezas();
    if(Juego->Ganado())
      gana->Caption = "SI";
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AgregClick(TObject *Sender)
{
  if(Juego->AgregarPieza(StrToInt(posx->Text), StrToInt(posy->Text),
                            StrToInt(larg->Text), hori->Checked, unic->Checked))
    if(unic->Checked)
    {
      unic->Checked = false;
      unic->Enabled = false;
      hori->Enabled = true;
    }
  DibujarPiezas();
  DibujarPiezasCanvas();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  int m = StrToInt(Medida->Text);

  Juego->DefinirTamano(m, m);
  Juego->DefinirSalida(2);     // Cuidado con que m sea menor que 3
  Juego->Limpiar();
  unic->Checked = true;
  unic->Enabled = true;
  DibujarPiezas();
  DibujarPiezasCanvas();
  movidas = 0;
  movlbl->Caption = IntToStr(movidas);
  gana->Caption = "NO";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PRESClick(TObject *Sender)
{
  Solver Sol;
  int rr;

  Sol.Inicializar();
  rr = Sol.ResolverTablero(Juego);
  if(rr != 0)
  {
    MemoRES->Clear();
    RESS->Caption = "�SI!";
    for(int i = 0; i < rr; i++)
    {
      AnsiString linea;

      linea = "(" + IntToStr(Sol.ax(i)) + "," + IntToStr(Sol.ay(i)) + ") ("
                        + IntToStr(Sol.nx(i)) + "," + IntToStr(Sol.ny(i)) + ")";
      MemoRES->Lines->Add(linea);
    }
    return;
  }
  RESS->Caption = "�NO!";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::unicMouseUp(TObject *Sender, TMouseButton Button,
                                                TShiftState Shift, int X, int Y)
{
  if(unic->Checked)
  {
    hori->Checked = true;
    hori->Enabled = false;
    return;
  }
  hori->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
  Solver Sol;
  int rr;

  Sol.Inicializar();
  rr = Sol.ResolverTablero(Juego);
  if(rr != 0)
  {
    MemoRES->Clear();
    RESS->Caption = "�SI!";
    for(int i = 0; i < rr; i++)
    {
      AnsiString linea;

      linea = "(" + IntToStr(Sol.ax(i)) + "," + IntToStr(Sol.ay(i)) + ") ("
                        + IntToStr(Sol.nx(i)) + "," + IntToStr(Sol.ny(i)) + ")";
      MemoRES->Lines->Add(linea);
    }
    return;
  }
  RESS->Caption = "�NO!";

}
//---------------------------------------------------------------------------

