/*
    InertiaPuzzle: Little Java MIDP game.
    Copyright (C) 2006  Daniel Serpell <daniel.serpell@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package org.ds.puzzles;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.rms.RecordStore;

/**
 * This class implements all the logic and drawing of the "Inertia" puzzle game.
 * 
 * @author Daniel Serpell
 *
 */
public class InertiaCanvas extends Canvas
{
    private Inertia midlet;
    private boolean showStartScreen = false, playerLost = false, playerWin = false;
    private boolean eraseScores = false;
    private int[] tiles;
    private int playerX, playerY, numGemsGet;
    private int boardSizeX, boardSizeY;
    private int dificultyLevel, lastPlayedGame;
    private byte[][] gamesDone;
    
    // Tile values are:
    private static final int tileBack   = 0; // 'b'
    private static final int tileWall   = 1; // 'w'
    private static final int tileStop   = 2; // 's'
    private static final int tileGem    = 3; // 'g'
    private static final int tileMine   = 4; // 'm'

    // The tile images. This is needed because drawing the tiles directly is too slow.
    private Image tileImages[] = null;
    private int tileImagesSize = 0;
    
    // The games
    inertiaLevels levels;
    
    // Instead of having an image for each tile type, this function draws
    // a scalable representation of the tile.
    /**
     * This method draws a tile using only basic drawing methods. As this is slow, it's
     * intended to be drawn to an image.
     * 
     * @param g    The graphics context to draw the tile.
     * @param size The size of the tile.
     * @param type The tile type, see the tile type constants.
     */
    private void drawTileImage(Graphics g, int size, int type)
    {
        size = size - 1;
        
        // Always draw the background
        g.setColor(128,128,128);
        g.fillRect(0,0,size,size);
        g.setColor(96,96,96);
        g.drawLine(size,size,size,0);
        g.drawLine(size,size,0,size);
        
        switch( type )
        {
            case tileBack:
                break;
            case tileWall:
                g.setColor(224,224,224);
                g.drawLine(0,0,size-1,0);
                g.drawLine(0,0,0,size-1);
                g.setColor(64,64,64);
                g.drawLine(size-1,size-1,size-1,0);
                g.drawLine(size-1,size-1,0,size-1);
                g.setColor(192,192,192);
                g.fillRect(1,1,size-2,size-2);
                break;
            case tileStop:
            {
                int x0 = size / 14;
                int x1 = size / 3 - size / 14;
                int x2 = size - 1 - size / 3 + size / 14;
                int x3 = size - 1 - size / 14;
                int y0 = size / 14;
                int y1 = size / 3 - size / 14;
                int y2 = size - 1 - size / 3 + size / 14;
                int y3 = size - 1 - size / 14;
                g.setColor(0,0,0);
                g.drawLine(x0,y1,x1,y0);
                g.drawLine(x0,y2,x1,y3);
                g.drawLine(x2,y0,x3,y1);
                g.drawLine(x2,y3,x3,y2);
                if ( size > 14 )
                {
                    g.drawLine(x0,y1-1,x1-1,y0);
                    g.drawLine(x0,y2+1,x1-1,y3);
                    g.drawLine(x2+1,y0,x3,y1-1);
                    g.drawLine(x2+1,y3,x3,y2+1);
                }
                break;
            }
            case tileMine:
            {
                g.setColor(0,0,0);
                int ys = size / 2 - 1 - size  / 10;
                int ye = (size + 1) / 2 + size / 10;
                int x1 = 1;
                int x2 = (size - 1) / 2;
                int xr = size - 2;
                for(int x=x1; x<=x2;x++)
                {
                    g.drawLine(x,ys,x,ye);
                    g.drawLine(xr,ys,xr,ye);
                    xr--;
                    if( ys > 1 )
                    {
                        ys--;
                        ye++;
                    }
                }
                break;
            }
            case tileGem:
            {
                g.setColor(64,255,255);
                int ys = (size - 1) / 2;
                int ye = size / 2;
                int x1 = 1;
                int x2 = (size - 1) / 2;
                int xr = size - 2;
                for(int x=x1; x<=x2;x++)
                {
                    g.drawLine(x,ys,x,ye);
                    g.drawLine(xr,ys,xr,ye);
                    xr--;
                    if( ys > 1 )
                    {
                        ys--;
                        ye++;
                    }
                }
                break;
            }
                
        }
       
    }
    
    private void drawPlayerImg(Graphics g, int size, boolean lost)
    {
        size = size - 1;
        if( lost )
            g.setColor(255,0,0);
        else
            g.setColor(255,255,0);
        int ys = size / 2 - 1 - size  / 10;
        int ye = (size + 1) / 2 + size / 10;
        int x1 = 1;
        int x2 = (size - 1) / 2;
        int xr = size - 2;
        for(int x=x1; x<=x2;x++)
        {
            g.drawLine(x,ys,x,ye);
            g.drawLine(xr,ys,xr,ye);
            xr--;
            if( ys > 1 )
            {
                ys--;
                ye++;
            }
        }
    }
    
    private void initImages(int size)
    {
        if( tileImagesSize == size && tileImages != null )
            return;
        
        tileImagesSize = size;
        if( tileImages == null )
            tileImages = new Image[9];
        
        tileImages[0] = Image.createImage(size,size);
        drawTileImage(tileImages[0].getGraphics(),size,tileBack);
        tileImages[1] = Image.createImage(size,size);
        drawTileImage(tileImages[1].getGraphics(),size,tileWall);
        tileImages[2] = Image.createImage(size,size);
        drawTileImage(tileImages[2].getGraphics(),size,tileStop);
        tileImages[3] = Image.createImage(size,size);
        drawTileImage(tileImages[3].getGraphics(),size,tileMine);
        tileImages[4] = Image.createImage(size,size);
        drawTileImage(tileImages[4].getGraphics(),size,tileGem);
        tileImages[5] = Image.createImage(size,size);
        drawTileImage(tileImages[5].getGraphics(),size,tileBack);
        drawPlayerImg(tileImages[5].getGraphics(),size,false);
        tileImages[6] = Image.createImage(size,size);
        drawTileImage(tileImages[6].getGraphics(),size,tileStop);
        drawPlayerImg(tileImages[6].getGraphics(),size,false);
        tileImages[7] = Image.createImage(size,size);
        drawTileImage(tileImages[7].getGraphics(),size,tileBack);
        drawPlayerImg(tileImages[7].getGraphics(),size,true);
        tileImages[8] = Image.createImage(size,size);
        drawTileImage(tileImages[8].getGraphics(),size,tileStop);
        drawPlayerImg(tileImages[8].getGraphics(),size,true);
    }
    
    private void drawTile(Graphics g, int px, int py, int type)
    {
        switch( type )
        {
            case tileBack:
                g.drawImage(tileImages[0],px,py,0);
                break;
            case tileWall:
                g.drawImage(tileImages[1],px,py,0);
                break;
            case tileStop:
                g.drawImage(tileImages[2],px,py,0);
                break;
            case tileMine:
                g.drawImage(tileImages[3],px,py,0);
                break;
            case tileGem:
                g.drawImage(tileImages[4],px,py,0);
                break;
        }
    }
    
    private void drawPlayer(Graphics g, int px, int py)
    {
        int t = tiles[playerX + playerY * boardSizeX];
        if( playerLost)
        {
            if( t == tileStop )
                g.drawImage(tileImages[8],px,py,0);
            else
                g.drawImage(tileImages[7],px,py,0);
        }
        else
        {
            if( t == tileStop )
                g.drawImage(tileImages[6],px,py,0);
            else
                g.drawImage(tileImages[5],px,py,0);
        }
    }

    private void winGame()
    {
        // Win!
        playerWin = true;
        // Mark this level as completed
        gamesDone[dificultyLevel][lastPlayedGame/8] |= 1<<(lastPlayedGame&7);
        
        lastPlayedGame = lastPlayedGame + 1;
        if( lastPlayedGame >= levels.getNumGames(dificultyLevel) )
        {
            if( dificultyLevel < levels.getNumDificultyLevels() )
            {
                dificultyLevel ++;
                lastPlayedGame = 0;
            }
            else
            {
                lastPlayedGame = 0;
                dificultyLevel = 0;
            }
        }
        // Save game status
        writeGameStatus();
    }
    
    private void lostGame()
    {
        // Lost!
        playerLost = true;
        // Save game status
        writeGameStatus();
    }
    
    private void tryMove(int dirX, int dirY, boolean automove)
    {
        if( (playerX + dirX < 0) || (playerX + dirX >= boardSizeX) ||
            (playerY + dirY < 0) || (playerY + dirY >= boardSizeY) )
        {
            // Invalid move
            return;
        }
        int tn = (playerX + dirX) + (playerY + dirY) * boardSizeX;
        int next = tiles[ tn ];
        switch( next )
        {
            case tileMine:
                // Game ends!
                playerX += dirX;
                playerY += dirY;
                lostGame();
                break;
            case tileStop:
                // Stop movement, move Ok.
                playerX += dirX;
                playerY += dirY;
                break;
            case tileWall:
                // Don't move and stop movement.
                break;
            case tileGem:
                // Get's gem and continue movement.
                numGemsGet --;
                tiles[tn] = tileBack;
            case tileBack:
                // Draw's movement - NOW
                playerX += dirX;
                playerY += dirY;
                repaint();
                serviceRepaints();
                // The repaint speed is enough, so don't sleep.
                /*
                try
                {
                    Thread.sleep(50);
                }
                catch (InterruptedException e)
                {
                    // Nothing to do
                }
                */
                // Call this function again :-)
                tryMove(dirX, dirY, true);
                break;
        }
        if( numGemsGet < 1 && !playerLost && !automove)
        {
            winGame();
        }
        repaint();
    }
    
    private void quitApp()
    {
        writeGameStatus();
        midlet.quit();
    }
    
    protected void keyPressed(int keyCode)
    {
        if( (playerLost || playerWin ) && !showStartScreen )
        {
            if( keyCode == -12 || keyCode == -11 )
            {
                // Exit
                quitApp();
                return;
            }
            showStartScreen = true;
            repaint();
            return;
        }
        if( showStartScreen )
        {
            if( keyCode == -12 || keyCode == -11 || keyCode == -8 )
            {
                // Exit
                quitApp();
                return;
            }
            if( eraseScores )
            {
                if( keyCode == '5' )
                {
                    clearScores();
                    writeGameStatus();
                    eraseScores = false;
                    repaint();
                    return;
                }
                else
                {
                    eraseScores = false;
                    repaint();
                    return;
                }
            }
            // Handle key presses on start screen
            switch(keyCode)
            {
                // Key UP
                case '2':
                case -59:
                case -1:
                    dificultyLevel = (dificultyLevel+1) % levels.getNumDificultyLevels();
                    if( lastPlayedGame >= levels.getNumGames(dificultyLevel) )
                    {
                        lastPlayedGame = levels.getNumGames(dificultyLevel);
                    }
                    writeGameStatus();
                    repaint();
                    return;
                // Key DOWN
                case '8':
                case -60:
                case -2:
                    dificultyLevel = (dificultyLevel+levels.getNumDificultyLevels()-1) %
                                        levels.getNumDificultyLevels();
                    if( lastPlayedGame >= levels.getNumGames(dificultyLevel) )
                    {
                        lastPlayedGame = levels.getNumGames(dificultyLevel);
                    }
                    writeGameStatus();
                    repaint();
                    return;
                // Key LEFT
                case '4':
                case -61:
                case -3:
                    lastPlayedGame = (lastPlayedGame + levels.getNumGames(dificultyLevel) - 1) %
                    levels.getNumGames(dificultyLevel);                   
                    repaint();
                    return;
                // Key RIGHT
                case '6':
                case -62:
                case -4:
                    lastPlayedGame = (lastPlayedGame + 1) % levels.getNumGames(dificultyLevel);
                    repaint();
                    return;
                case '5':
                    break;
                case '*':
                    eraseScores = true;
                    repaint();
                    return;
                // Other Key
                default:
                    // start
                    break;
            }
            // Start Game.
            showStartScreen = false;
            newGame();
            repaint();
            return;
        }
        else
        {
            // Handle key presses in game
            switch(keyCode)
            {
                case -12:
                case -8:
                    // Exit
                    showStartScreen = true;
                    repaint();
                    return;
                // Key UP-LEFT
                case '1':
                    tryMove(-1,-1,false);
                    return;
                // Key UP
                case '2':
                    tryMove(0,-1,false);
                    return;
                // Key UP-RIGHT
                case '3':
                    tryMove(1,-1,false);
                    return;
                // Key DOWN-LEFT
                case '7':
                    tryMove(-1,1,false);
                    return;
                // Key DOWN
                case '8':
                    tryMove(0,1,false);
                    return;
                // Key DOWN-RIGHT
                case '9':
                    tryMove(1,1,false);
                    return;
                // Key LEFT
                case '4':
                case -61:
                    tryMove(-1,0,false);
                    return;
                // Key RIGHT
                case '6':
                    tryMove(1,0,false);
                    return;
                // Other Key
                default:
                    break;
            }            
            return;
        }
        
    }
    
    private void clearScores()
    {
        dificultyLevel = 0;
        lastPlayedGame = 0;
        for(int i=0;i<gamesDone.length;i++)
            for(int j=0;j<gamesDone[i].length;j++)
                gamesDone[i][j] = 0;
        
    }    

    public InertiaCanvas(Inertia midlet) throws IOException
    {
        super();
        // Store midlet
        this.midlet = midlet;
        // Init levels
        levels = new inertiaLevels();
        // Init tiles
        tiles = null;
        // Init state
        dificultyLevel = 0;
        lastPlayedGame = 0;
        // Init games done bit array.
        gamesDone = new byte[levels.getNumDificultyLevels()][];
        for(int i=0;i<gamesDone.length;i++)
            gamesDone[i] = new byte[(levels.getNumGames(i)+7)/8];
        // Init scores.
        clearScores();
        // Read game status
        readGameStatus();
        // Show start screen
        showStartScreen = true;
    }
    
    private void newGame()
    {
        tiles = levels.getNewBoard(dificultyLevel, lastPlayedGame);
        boardSizeX = levels.getBoardSizeX(dificultyLevel);
        boardSizeY = levels.getBoardSizeY(dificultyLevel);
        playerX = levels.getPlayerStartX();
        playerY = levels.getPlayerStartY();
        numGemsGet = levels.getNumGems();
        playerLost = false;
        playerWin = false;
    }

    private void drawBoldString(Graphics g, String s, int x, int y, int flags)
    {
        g.setColor(0,0,0);
        g.drawString(s, x-1, y, flags);
        g.drawString(s, x+1, y, flags);
        g.drawString(s, x, y-1, flags);
        g.drawString(s, x, y+1, flags);
        g.setColor(255,255,255);
        g.drawString(s, x, y, flags);
    }
    
    protected void paint(Graphics g)
    {
        int sizeX = getWidth();
        int sizeY = getHeight();
        
        if( showStartScreen || tiles == null )
        {
            // Draws start screen....
            Font fnt = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
            g.setColor(64,64,255);
            g.fillRect(0,0,sizeX, sizeY);
            g.setColor(255,255,255);
            g.setFont(fnt);
            
            boardSizeX = levels.getBoardSizeX(dificultyLevel);
            boardSizeY = levels.getBoardSizeY(dificultyLevel);
            int fh = fnt.getHeight() + 1;
            int x = sizeX / 2;
            g.drawString("INERTIA", x, 3, Graphics.TOP|Graphics.HCENTER);
            g.drawString("Board Size:"+boardSizeX+"x"+boardSizeY, x, fh+6, Graphics.TOP|Graphics.HCENTER);
            g.drawString("Game Number:"+(1+lastPlayedGame), x, 2*fh+6, Graphics.TOP|Graphics.HCENTER);
            if( eraseScores )
            {
                g.drawString("Clear scores?", x, 4*fh+6, Graphics.TOP|Graphics.HCENTER);
                g.drawString("Press '5' = yes", x, 5*fh+6, Graphics.TOP|Graphics.HCENTER);
            }
            else
            {
                g.drawString("Press any key", x, 5*fh+5, Graphics.TOP|Graphics.HCENTER);
            }
            
            
            
            if( 0 != (gamesDone[dificultyLevel][lastPlayedGame/8] & (1<<(lastPlayedGame&7)) ) )
                g.drawString("(completed)", x, 3*fh+6, Graphics.TOP|Graphics.HCENTER);

        }
        else
        {
            // Get's tile size
            int size = sizeX / boardSizeX;
            if( sizeY < size * boardSizeY )
                size = sizeY / boardSizeY;
            
            initImages(size);
            
            int sx = (sizeX - size * boardSizeX + 1) / 2;
            int sy = (sizeY - size * boardSizeY + 1) / 2;
            
            // Draws background
            g.setColor(96,96,96);
            g.fillRect(0,0,sizeX,sizeY);
            
            // Draw tiles
            int i=0;
            for(int y=0;y<boardSizeY;y++)
                for(int x=0;x<boardSizeX;x++)
                {
                    drawTile(g,sx+x*size,sy+y*size, tiles[i]);
                    i++;
                }
            // Draw player
            drawPlayer(g,sx+playerX*size,sy+playerY*size);
            
            if( playerLost || playerWin )
            {
                Font fnt = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
                g.setFont(fnt);
                if( playerLost )
                    drawBoldString(g,"DEAD", sizeX/2, 4, Graphics.TOP|Graphics.HCENTER);
                else if( playerWin )
                    drawBoldString(g,"COMPLETED", sizeX/2, 4, Graphics.TOP|Graphics.HCENTER);
            }

        }
    }
    
    private void readGameStatus()
    {
        try
        {
            RecordStore rs = RecordStore.openRecordStore("status",true);
            ByteArrayInputStream ba = new ByteArrayInputStream(rs.getRecord(1));
            DataInputStream dis = new DataInputStream(ba);
            dificultyLevel = dis.readShort();
            lastPlayedGame = dis.readShort();
            
            if( dificultyLevel >= levels.getNumDificultyLevels())
                dificultyLevel = 0;
            
            if( lastPlayedGame >= levels.getNumGames(dificultyLevel))
                lastPlayedGame = 0;
            
            for(int i=0;i<gamesDone.length;i++)
                for(int j=0;j<gamesDone[i].length;j++)
                    gamesDone[i][j] = dis.readByte();
        }
        catch (Exception e)
        {
            // Fill values
            clearScores();
        }
    }
    
    private void writeGameStatus()
    {
        try
        {
            RecordStore rs = RecordStore.openRecordStore("status", true);
            ByteArrayOutputStream ba = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(ba);
            dos.writeShort(dificultyLevel);
            dos.writeShort(lastPlayedGame);
            for(int i=0;i<gamesDone.length;i++)
                for(int j=0;j<gamesDone[i].length;j++)
                    dos.writeByte(gamesDone[i][j]);
            byte[] b = ba.toByteArray();
            if( rs.getNumRecords() < 1 )
                rs.addRecord(b,0,b.length); // k is the array size
            else
                rs.setRecord(1,b,0,b.length); // k is the array size
        }
        catch (Exception e)
        {
            // Write error, can't do anything.
        }
    }

}
