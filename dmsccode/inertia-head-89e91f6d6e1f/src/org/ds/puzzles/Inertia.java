/*
    InertiaPuzzle: Little Java MIDP game.
    Copyright (C) 2006  Daniel Serpell <daniel.serpell@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package org.ds.puzzles;

import java.io.IOException;

import javax.microedition.lcdui.Display;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class Inertia extends MIDlet
{
    private Display display;

    public Inertia()
    {
        display = Display.getDisplay(this);
    }

    protected void startApp() throws MIDletStateChangeException
    {
        InertiaCanvas canvas;
        try
        {
            canvas = new InertiaCanvas(this);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }
        display.setCurrent(canvas);
    }

    protected void pauseApp()
    {
    }

    protected void destroyApp(boolean unconditional)
    {
    }
    
    public void quit()
    {
        destroyApp(false);
        notifyDestroyed();
    }

}
