/*
    InertiaPuzzle: Little Java MIDP game.
    Copyright (C) 2006  Daniel Serpell <daniel.serpell@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package org.ds.puzzles;

public class inertiaLevels
{

    private static final String[] levels6x5 = {
        "sgswsmmwbSmgsmwbsbbwgggwmbwsmg",
        "wgbgsmbmgbgmwmwwsbgmSssswgwsmb",
        "wsgmwsbwmgmsbbsswbmbggwmwggSsm",
        "msbsmbwwgmgbwmmgsswbggssmbSgww",
        "bSsmsmmgwsbmgwbwgssbgwmgbmgwws",
        "mmggsbmswmmbgwswbgSmwsgswgbwbs",
        "bbsbsswgSmmwmwwswmgggbmbsggwms",
        "bwmssgwmbswbbmwmsbggmgwgsSsgwm",
        "wbgwwsmwgmmSmbsmbgswgmbsbwsggs",
        "bmwmSwwssgmgmbbwwggbsbmgsgmwss",
        "wbbmmsswmbsgbsgswSggmgwwsgmwmb",
        "mssggSmgssbswmgmgbbwgwwbwswmbm",
        "swsswmgwmwbwSsbmmsggmgsmbggbwb",
        "gbwgbsmgsgwbsmswgmsmsSwmbwgmwb",
        "mwsggbmwwmbggbssgsgbwmmsbSswwm",
        "wgmbbSgswmmswswwmgmwggmsbsgsbb",
        "smgmswsbmbwwmwbsgmgwbSgsmgbgws",
        "sgwsmwwsswgssgmmSgmwbmwgbbbmbg",
        "mwsmgmgsgbSwgbmssgwbswwwmsgmbb",
        "wmgsmwmmbgbwbgwssbwmssmgbwgSgs",
        "mmbsswwbmgsgsmbbswmggwbwgsSwgm",
        "sgwwwsmgsmbwbbmsmgmgbmwgbsSgsw",
        "bwwmgsSbwgbgswwmsgssbgsmwmbmmg",
        "Sgmsssgggwwwbsggbmsbbbwwmwmmms",
        "swbswmbwSwgmgmmsssmgmgwbbwggsb",
        "wsmwwmgSwbbsgmggsmgsbswmbmwgbs",
        "swmgwswwggwsgsswggbbbsmbSbmmmm",
        "wgmsgbsmmwwgmgwgsswbsbgmsmbSwb",
        "mmbwmSgbgmwgsmwsgswsgsbswwbmgb",
        "wmwwmmmbbgbwbssgswwgmbssmsgSgg",
        "mmbSswbsbggbsmgmwsgwggwwmbmsws",
        "Smgwbgsssbmmssggsbbgmwwwgwbmmw",
        "gmwmbwwbsbmswswgSmwsgsbmmgggsb",
        "mmbwgSgwmgsmwbsggssmswbbswbwgm",
        "wsgwmbmmbgbwmbmsmwwgsbswgssSgg",
        "wbsmwsgbggsmbgwsbwmwgwmsmSsmgb",
        "gmmwbwgmmbswssmwgbmwgbsssgbgSw",
        "mwwsbwmwgmgsbgbSmwmwmbbssgsggs",
        "bsmmwbmggwmbSbwgmwmsggswsbswsg",
        "gbwwsmmgbbmwssswgsSbwgmgswbmgm",
        "msgwsgssgbgwwmbsSwmwbbmgsgmwbm",
        "gbswsmbbssmwmgwbgwSmmsgbwgwmsg",
        "wbswbwgmsbmmSsbgwmsbsgsgmgwmwg",
        "mwbmgsSgwwmbmswmbsgwgggsmbbwss",
        "wsmgsggsbSwsswgbbbwmwwgmmbmsmg",
        "wbbsgmgbbswggwsmsSbwwmgmmwgssm",
        "mbmswbggbgmmwgssbswmwgSswmwbgs",
        "bsgsmmmmSwbswwgggswsmwsbbgmbwg",
        "gwggbbgmsmwbsSgbwmsmsmmsgwswwb",
        "mgwwsggsgbwmmgssSgwbmsmmbswwbb",
        "mbgsbggsmSsgsbswmwwggbwmwwsbmm",
        "gsmmsbgwSmwmwsgbbbggwsbswsmgwm",
        "wgwsssgbsggwSbwbsmbbmgmsmmmwwg",
        "gwmbmsbsbsmmgmwwswbsswmgbggwSg",
        "gwsggSmgmgwswbwbssbgbswmwmmmsb",
        "wgbmsbwmsbwSggsmgmggmbsmwswbsw",
        "smmwgwbbggsgwbmbmwsbsmSswmwsgg",
        "ssmswswsggwgbmgmSwbswbmwgmbbgm",
        "gwmmsswbbmggbwsswgmgsSmmwbbgsw",
        "gSsmmwsmmgwssgsggwbwmwbsbbbmwg",
        "gbwgmwwssmmgmsssgbbbwmgSsmwwgb",
        "wswbsbwmgmwbgsmsgsgmSgmbswmwbg",
        "wgmsSwmggwwsbgmbsbwmwmgssbbmgs",
        "gsgbswsswmmswbbgbwbwggsmmwSmmg",
        "mwgmbwwsbSggsggbwswwgmsmmbssbm",
        "sSwsggsbbmsmggsgswmbwgmwwbmwbm",
        "bmwwwgbsgmmbgbwmsgSsgmgswwsbsm",
        "bwgmsgbbwmsgwmsgsmgbSswgwmmwbs",
        "wsmbmSgmssmswwmsgwmwggsbwbggbb",
        "wbwssmbSsgmwggmwsbmgswwsmggmbb",
        "wsbgSgwsmggbmswgsmssmbbwgwmmwb",
        "wbwswgbbmsbmbwgsgssmgmwgmmsSgw",
        "sswsbwggmmgbgmbgmswssSmbwwwmgb",
        "wgsmmbwswbmwbwmbgmsggssggmsSbw",
        "bwbswsbmgmgsmwmSmggmbwggwbswss",
        "wwsbggsbbgbsgbwgsmSwsmmmwgmwms",
        "swgbbgwbgsmgbsmsmwgmsbwmgwSsmw",
        "bwssgwgmwbwmbmggmbsgSbgmwsswms",
        "swmmbmbgbwwsswggSgswmbgmmsswbg",
        "sgbmswbmbmwsggmbwwwgsgwmsgSmbs",
        "wbgwwgmgsssmsbbmsbbsgSmmmggwww",
        "bgggSmmgmbwbmswmwsbmgwsgsbwssw",
        "mbsbgswwmwmgmgmsgmsgSwssbwwbgb",
        "bmsmsgmwSmbwmsbsggsgwwgbbmsgww",
        "bSwbmmggbwssggwmbwsbmsgmwwmsgs",
        "mwwggsbgssmsgsgbwbmswmgwbwmSbm",
        "mbwwwswggsbwgbmmsgmmsggsSbwbsm",
        "bsbmggmmwbssgmmgwwmgSgwwssbwsb",
        "mgmsmwmsggsbbbwbgmbwsSgswmwwgs",
        "gggbmwwsgsbwwsmwbwsmmssSgbmmbg",
        "sgmwswSgwwgwsmgbgbmmbgssbmswbm",
        "Smssmwssggbwbgwwmsgwbmmswmbgbg",
        "wswmbgwwsmgbmgsmssmSggwsbgbbwm",
        "mmgbSggssgwbgsswsgwwmmwwmsbbbm",
        "mgsmgbbsSgwwmgwwbssmwmsbgmswgb",
        "msmsggwgssbwbwwwsSwbbmsmbmmggg",
        "wgmwsmbsbggsbmwwbmsgSbmwgwsgsm",
        "wbmwSmgwmmmsmggsgbswbbswssbwgg",
        "wggsbmswgbssmsSbgsgwbmwwmmgwmb",
        "msmwgwsmgbwmgsgwmgssbbbwbsSgwm"
    };

    private static final String[] levels10x8 = {
        "bgbmmwssmwgmmbggmggwmsbgwbgbwbgwsbgwbmgbmsSsbgwsssswwwmgmsgwmgwmwsmswmsbsgbbsbwm",
        "sgwmmwwsswwgssssbbbbgssgmgsbgmggsmwwmwmmgwgwbbwbmwbmgbmbssgwgmbSwbgssmmbwswbggmm",
        "wswmbbmmbbgbwwwmwsmgsmwwmgbbssbbgsggsbmsgmggmsbSwwswgwsbbgwmgggsmwmbgssbmwmmssgw",
        "ggwmgbsbwmwmsgmmbwmgssswbbmmsmwbwgmmwgbsssbgbggwggsssgswbmmswgSwbwbgswwbgmmsbwmb",
        "gmwmgwwbmmggSsbmgwbsbwwmwsmsgwbgbgsssswssgssmgsgbgwgbwswsggmbbmmbwmbmmmgwbmbbsww",
        "mgmmmmbwsgbbgwbmmbSssmsgmwgmwmsgmwsggwwbbswbgswbssgssbmbswwsssggbwwbgmwggmgmwbbw",
        "bwbswgmwbsswsmbsmmgsgsbbbSmmgwmgbsbwmmgbgggmbmgwbssgwgmwwgbwwmmsssgbgwsbmmgsswww",
        "bwsbmbbmsgsgwgwssgggbbwmsmsmmbwwwsswmbwgmwSsmgbbwwsbwggmswmwwmmmgsmmgggbbssggsbb",
        "swwwwsgmwmsggbmsbggggmwbwsmsmmwsbwsgmwmmbsbswsgwsgsbmgbmwbgsmwsgmwgbSgbsgbmbmwbb",
        "wbggsbwmgswgmbwggbwbwbbswwmswsmggsmmmmggswbwmwmsbbbbmsgsbggmmsbsSwsswwsmswmgggmb",
        "wmggswbmwwgbbsmsggmsgbwmwgbgssmsgwbmgsgwwwsbmsgwbbmbmmsmswswwmgmbwbmsbwgbSgbsgsm",
        "mwmwsbsgwssbsggmgsbmsbsswmgmmsSwbgbwmmwwsgbgsmwsmwmbbggwbwwmbwbgsgsbbmmgggwwmgsb",
        "swmmggsmbggbsbwsgsbmgmgwssmwgSmswwmsbswwbsbmswssmsggbmmggbwbbmggwwwbwmmgbwmwbbsg",
        "mswwgmsmmsssgbbsgmggggwmmgbbbwwgswwwsmssgbsgSbswmwbbswbgsbmmmsmwwmwmgbgwggbwbmbs",
        "mwbbggsggsmgbgsgbgbwwmgwgsbwgbmgbwmmmmbmSbbswsbmwsgmwsmmsmgwsswssswsbsmggbwwwbwm",
        "bbgbbgwbmwmwmgsbmmsgsmgbsswmSsbwbwbwggwssmmwggmmswmgwswwswggggswsmgmsbmbbbsbsgwm",
        "wssbwbbwsssggbssswbmggsmgmmbwwbgmmsbmswwgbgmgmwmmgmmmsggbmSwsmwbbsbbwwwgbgggssww",
        "wwbmbbsmgmwmgmwmgbwwssgmbbgbgmsmsbwswbmbmbmgsggwsmmgwsswsbSmgsbwswgsssbgwgggwmwb",
        "bmgswwsmgsbwggbmmswgmmmmgwbswbbwbmmbmswwmbbwbbgwsgswgsbmsggSsmbsgswswggsmmswgbwg",
        "bbbgwbmgSsbswbwssmsbgmgbbggbwmgmwwsbgmwmwmggmggmsgbswwsswsbmmgmswwmbsbwwsgsmgsmw",
        "wgSbwmgwmbwsgmswsmsssmbsbmbgwmbgwmgmmgssggsbbgmbgwgsgbbgbbsbgwwsmmwwmwsgsmsmwwwb",
        "gggwswbmssbgwbmwssbwbwbsbmgmmwmwgggswwmsmbwmsbwgwbmssgsbgbsgsmmmmbsSwmggsbmwwgbg",
        "msswmswwSgmwmsgsmmbgwmgmbgwwsgmwgwbbmsbbbmbmbgmgssbmgswwgwswwsbggsbwswbmgmbggssb",
        "bbsmbmswgmmbgmsmwbmsmgmsgbwsggwwbmsmgbwwbgmwsgwswsgwssgwbgmsbggwmwsbSgmwwbmbbssg",
        "ggbwbbmwssbmswbsgmbwggmgbmbwgbmswwgwbmbmwsbwssmmgwwwgwmSmmgsbggssmbmgmbssswggssw",
        "mmSwmssmbgbwwgssmwbbbbbbgmgbmmgmmsgbswsgbwbmmwwswgwswggssmbmwgwsgsbggsbwswsmgmgw",
        "bmbwwbwswmbwmmbwwswmwgmwssgggbggmmbmsgmbgmgwwmwmsgsgsbbbsSsssbggggmsbbbgwmsmswsw",
        "mgbgmwbbmmmgbmmswwwwwswswbwgbmbbwswssmwmsbsgmmbsbsgSgsmsbgggsgmwmsmgbwgwsbgggswb",
        "swmsbwbgwswbswSggmgbbwmmwmbmgmwbbbmbmwsswwgmbmggwbssgwgssggsgwmggbsmmmwbgswsssbm",
        "wwgwwbmmsbbwmbmwbSmswsmbbswbswsmggwsggswbgwgmgmwmgmbsbgsssbgwmsmsbggbmsggsgmwwmb",
        "wmbgwswmbmwbsmbmmgssbwwmbwsgswmsbgsgswbgswwmgSggbbbwbsbgsbmsmssmmggwggwgbsmwwmmg",
        "gsbgsmmssmbwwwgsgsmgmwbwwgbbgsmbmbgwwwwgwggsmbgwsmbmgbgsmbmswwmwmssbSsbmbssmwgbg",
        "wmbsmmbbsmgsmsgsgbgmgmsggwsmwbwsmgmmSbwbgwsbmsgmbwwbgmmbwgbgswssbmwwsgsbwsggwwbw",
        "wwswmgbgmgggmbwmswsggbwmbwwggbwgsbmwwmsbmssswmSsmwbbmmbbgssswgsbgbwgsgsmbmmmsbwg",
        "mgwgswmbgssswggwgsbwbwgwSwsmgggmgsgbgwmgmsmgsmswbsmwgsbmbwbbbbmbbmbwsmmwbmsswwsm",
        "smmmwsbmmbbswwmwggmgbsbwwwsgsbgmgswbbgmmggmsbsbbwgSwmggswbsgbsmbggwsmsmgswwwwsmb",
        "mgswwwggbgsswgmmmmbbgsggbwbsssbbsmsbgwSwgmwwwwmmbwsmbbbmsgsgbmmwwgmwsbggswbssgmm",
        "bmmbmbsmbgmwmsssgssswbggsgwbbmsmbmbgbwmbswwbsSwswmgmbgmgwgmgbgbwmgwmswwsswwwgsgg",
        "sgbbbswgwggmwbsmbmgsssbggwwmssmswbbwbwbmgmgwmmwwSgbmssbswbgmggsssmbmgwgwwmmbsgwm",
        "mwbgsbmggsmsbwmmwwsgwbggmgswwsmsbwsgwgswsmmwbgmsgmbbwbSmbwbmgwmbgbwmggssmsbwgssb",
        "gbwssgbswswmgmsbmbmwwwwbgbwsgmmmgwgmSbswbwsmmwwgbmsmwbsmsbmgssgwmsgbmwggbbgbssgg",
        "wbsbmbbwswgbmmwbwbgbgwsmsmgwmgggbwmmgsgsbsmmbsswwggwgbbmmsmwsmsswsgmsmggwgSswbbw",
        "mgbmbwgbwmmsggssmbbbswmwwmmbwmssmgsbsgbsbSgsmsbwgwsbmwmwmmswswgsbwwbwwggmgsgggbg",
        "gwmsmsmwbgbwswgwgSgsbmbwsmswbsswmssmgggsmsbsgggsbmmgmbsbmwmmgbggwbswwwbwmbbmwgwb",
        "mwmwbmmmbgsmgmwgssmswsmbwgmsgwbswggmbmgssbbggwgmwbmmgbsswmsgbwgwbgwwswbswbbbSsgs",
        "wggsmbwbsgswmwgsggggmbgbsssbbmwggmmsgmsmbmgbSbwwmbwsbbwwgsgsswwbmmswmswmswmbgbwm",
        "ssswmmbmmmbbmmsgbmwbsmmssgwwgsbwmgwgggbwgsssgwmwwwssbbgbgwmsgSsmgbwbbbgwgwmmwgbs",
        "wgwwswsgmwbgbmwmsbmbwsmgbbgggmssbggwwmwgsmmwgsbwmSgswmbmmbsmbwggsswssmssbbbgwbgm",
        "gbwwmgwmbbbmwmSbbssssssgbmbggmbsswsbmsmgswgwmwswggsmgwmbggggbwgsmswwmbwbmgmmbwsw",
        "ggwwswwggbwswbswgmsmsmsggwbSwgbgbgwssgwbssmbmswbsgbmmmwgbgbbbmbwgsbsmsmsmmmmgwwm",
        "bmwbmmbsbsbmsswsmswmwwgwgwbggwmSsgwggsmwgswsggmsggbmwwsgssbmbwmmmwbmgbbbsmbgbsgw",
        "gwbgswsggmmbggbsgbmgwwgmgbwswwssbbbggwwbswmssbmsbSmwgssmmsbmmbsmmggwwswbgsmwwmmb",
        "swgbwwsmmbgmwbwgbwmgggbsmsssgwgmgmbswbwgbsmswswgsbmSsbmmbwmwsswbbsgbmmsgmbmgwwgg",
        "wmsgmsbgsbgsmsmwbmsswswbwbgwwwsgbmgSmbwwgsmsmbwsgswbmbsbggmgbbwbmmsmgwbgmggswgmw",
        "Sbsbmbsmmgmsmgbsgsssbmswmwmwsmbbswgggwsbwwswmbwswsbgbbbsggwgmwgmwwmgggwsgmmbbgmw",
        "wbsmmmbgwbmmwwsbSswsmsbmgmwbbggsbsggwsbbgwswmgbggbmmbwswssmwmwgmgsmswbggsmswgwgb",
        "bswbbwswgbmwbmmgbgSmmsgmsmsgwwsbgbgwggggbwbsgmbsbssmwmwsggwmbwbsswmwmwswmggmssbm",
        "sgsbSwmmbgsgbmgwwwsgwmgwggmsmsbwmwsgbbwgbwwmgsmsggbmsmwwmssgbbmsswbwbwmbsmmgsbbg",
        "gggsmswswmsswgsgsmSbwwswwgmwmmsgsgssbbbwswwggsbsmbbmbggbmgmwbgmbmbgswmmbbwmwgmwb",
        "swmsbgmmwsgbwwmmsssmmbgwswsbmgmbggwbSwwgsgbsswmbgbwgwsgswswbmbmgbsmgmbgbwmggbswm",
        "bmsmbswwbsbwwwmssmsgmbmgmSwsbsbmbbwgmwgwggbgbbsmgsssgmbmssgmwsmmmwgbwggwwbwwgggs",
        "smbswsswgwswmbggSggmggbsmbsswwgmsswwsggmmsmmbwmgwbwbbwmgsmsgmbbmgmbbwwbsgswgbwmb",
        "ggsSwwgwmmwgmsgwsmsswwbwbmsgwgmgmbmsbbwgwmmbssgmgwmmsgbwsbbbbbmgwssbmggbmwbssswg",
        "wmwgbSwbgmmgbmmwssbbsgwbmwmsgbsgbbmwggsgmgswsmwmmwsgswwsbbsmbswsmwgwmgmbggbsbsgw",
        "bwmwwbmswggsmsmbwssggbmbwwbmbgwmbmwsmgbssbwsggwmmsgmsgsmsbwggggmwsgssbwbSbmwmgwb",
        "sggmmssswwbsgwgwSbgmsggswwgmmsmmgbmsbwwsssmgsbmbgmmgwgsbwwgsbgbwbmsbbmmbbbwwmwwg",
        "wsgsmbsmgsmbgwwssmgsgmmgbgbbmwsbmgsswbwbswsggbmwbwbgSmwwggbswbmwmmmbmggmswsbgsww",
        "mgwgbwmSbbgbsbggmbsgmsggbsbgwmssmmsmgwwsgbmsgbswmmwwwgssmgbwwmggbwmwwbswmsmssbwb",
        "wsmbgwgwwwgsssgbggmbmgmmmbswsgsmmbmsgwswmwmwmbbbsgmwmmmgssbggbSwwwssswwbsgbbggbb",
        "smbwsbgbgmbmgmmmwgswssbssmwsbgsgwbsbmwwggmwsgwgmwgmwsbwwgbbsbsmwgbSgmgmsgsmbwwmb",
        "wmsgmsmwgwbgwgssggbsgwsswmgbsgwgsmwsmSggwbgswbmggsswgwbmwbwsmbsbbwbbmbwmmmmmsmbb",
        "swbgbmmbsgssbbmggsmgmwmwwwsgwbsgwssswsmsbwbgbwgmmsggmwwwSbwbgbmswgmbggmsmsgbbmwm",
        "bSgwbgmsgmgwgwbgssbgwmbbsmswmmssbmwbmgwmggwswwgbbmsgsssggbmbwwbwgswwmmswmsmsgbmb",
        "mgwmbwswmbwggbbbwbwwSsgmgmbmbswgmbsmgbsgwssmmgbwswwmsgmmsgbsgwsbwbmmgggswsmbsgsw",
        "ggwwwwmssgbgwmbwmsbbswggmmswmsgbmssgmbsswmmmbsmsbmbwgbswwbwgbswgmgwbgSmmwssggbbg",
        "bmbbmgswmbwgsbsbmwmsgwwgmmgbgwsggbbswswwbmmwgbmmssgwwbsbgsbgwssgwbwSsmgswmmmmggs",
        "ssggmwmsgbmmsmswbbmgbggmgsgbgwwmwwgwbgsbmmsbwgsgbmgwswbSmwsmsbgwmsbmwbsgwswsbmbw",
        "mwgbbgsmgwbmmmmbsgbwsbwbwmbsgwwwwswsmggbwssSgmgbgsgsmgmbssmmwwwsbwgmsmsbsggbgwbm",
        "gbmgssbgmwwSsgbggwmmmwgmwbggsswwbgmgwwswwssmbsgbgsgwmbbmbbswwwmsssbbmmsbsgmmgwbm",
        "mmsmbwwwgmgbsgbwmwgsgggSgmbbsgwsbwmwmbgwgsggbgssswwgmbbswssbbsmmwswswmsmbbwgmmmb",
        "wwmbbggmmswwgmgsbbbwwbmgmbwmssssgwgsmmwmgbbbwwbmwbsmmswgbggggmSbmwsgssgswbwgsssm",
        "wswsbgwswmbmgSmwwswwmmsmmbwmbmgwbsbbmwmbbsggmbwgsmgwsggmgsmssggbbsbgbswmbsgwsggw",
        "wgsmgbmswbmmbssmgbwwwwggbbmbbgwbwSssmggswmmmbgmsmgwbwbssbwmwbgmmmgwgsgssgsgssbww",
        "swbbbwggsmswmmmwssbbsgwwggmwmmbmmsswswggsbsgbswgbmmsmbwmwSbgbwmsgbgsgsbbgwgmwwgm",
        "mmwmssgsswmbmwbsbgwbsgsggSwmgwsswwmwbmmssbsmmwbgwggbmgbgbsgbmwswbbgmwmwmgbgsbgsw",
        "sSsgwmbmwgmbmwmswmsmbgbwsgbbgmgbsmmgsbgwmggmssswbggswsbwgssgwwbbgbwbsmbwwswwmmmg",
        "bsswbgmgsgmbswmwbbgggmgsbmwwmswgbgwgsswbmmbswwbmsgbswmbbsswsSmwsmbgwmgmgswbwgmmg",
        "wgbbmbbmwbmswssgbmwwsbbwbbgbsmwgwgmwmswgmssmbwgwggwmgwmssggsbbgssmSssmgwmwbmgmgs",
        "mmgsbwwssgbmbsgsmgmwbmwmbmbmsgbsbwbbwmgwswwgwsSsbmbwgwbgggbbswsggwwsggsmmmgmsmws",
        "bggsmmsmgwgbmbmbsbmsgbgswwgwmwmbwmssbgmwbssswgwbbgsgSwsbmbmbmwsswwwmmgmgwgwgsgsb",
        "mswgmmbsswbmbggsmbgsggbwbgbgmbwsmgwmsmwwsmmsgSsbsmwmgbwwbgwmmgsbbwgmgwgbswwsbwss",
        "sgmmwgbgsmggbbswmgsmswsmwbSgswmsgwgbgwmggwbgwmwsmsbbgwgwmmbwwbsmbsswmmsbwbbmsgbs",
        "wmsggsbmmmsgwwwsbwwggwmsbbbbbgswsgmgswbwsmgbgmgSswmgggbmwmsbgbsmbwwsbbssmmmswmwg",
        "wmwwgmgbwwmgggmmgwbbbbssSbgsmsggmwwbgsgsgmswswggbswmbwmsgbgmmmsbsswwmmssswwmbbbb",
        "mssgwmbgssSbmbgsbmbbggwmmswsmssggsbbwgwbwgsbgmmwmmbsmgwggsbwmgwwmwswgsbgwbsmwwmb",
        "msmmggsmggwsmsmbwwwbsggmwmbmbsgbwgwmsbSbwbwgbsssmgwwwmbbsbbwgsmbsgsggmwmbssgmgww",
        "bbmwgwsbgmwwsmgbmwbmbsmmsmwsbgssgswgbgswggmmsswmbwwmbssbwggmwbwwbgswggsmbmgmbsSg",
        "wggsmwbwggwwgmmswmwsmgmmmgbmsggwsbbbgbsgsgmsbmgbwwswmbSgwsmmwswsbbgssbwgwssbbmmb",
        "mwgmmsbwmwgmbsmwmwbmssssmgbbswbwmwsggswmwbgbmSwggssggbbmsgbbgbsmwgsmmbwgbsgswwwg",
        "mmmwgwmbssbbwmbwsmmsgsgbbmwssmmbmmswgwsgsbswbgsgbgggbbsmmwwssbgbwSggggwbswwmwwgm"
    };

    private static final String[] levels12x10 = {
        "bmsmsbgbmbmwgmwwsmmgbsmbgswmmsmmgbsmgggwgwbgsswgmwsgbbwgssgmmsswgmbwwwwswgwgbwgbbsgmwmswwgsmsmbwmbgbssbmbwbsgmsSwbsbbgwg",
        "bmmswsgbsgsmmsgbsggmsswwmmssggwmbwbbwwsmbmwwbsgwwgwmswmsmbgmmgbgwmwbbbgsgsmbssgwbggssmsmgbmbggbbmwmbwsswwwbSbwggbwsmwggm",
        "gswmbbgbbbsgwgbwmwsmbswwmsmmbmSwbmgsbwswmgswsgmgbsswbbgsmgsmsmmsggsbmgwwbsgsgbwmmsbwwbsmggbswwgmbmmwsbgbswsgwgwwmmwbgggm",
        "mmgbsgbgsbggwgwwwbwsmwgbgsbmbsmgswbsmwbbgswmgwbbmsbbmmgggbmssgSwsbmsswmgswwmmwswggswmsgbwgbsbmmwbbmwsmmmswgmsswmwggbwbsg",
        "mgwwgmmwsgmsgsgmswmgmbgbbwbwbbwgsmmbmgggbmgwgssmmbssmgbbgswbbsmwwmmwsbgsssmbSbwgwmsbgmwgbmwbssgbgwsbbmsgswwwbggwmsswwswm",
        "bwwsmwgbwwwggbswbgbswbsbbmmwmgbbssgwssggbbgmgmbmwsmgmwsgsmssbbbbmwwbbbmbgwgSgbmmmgsgmgsswsgswbgmwsmswsgmgwsmwsmsmwgmmwgw",
        "mmgwswssbwswbmwbssbbmwbmswgbgwbgbwgbgssmsgwsmswsmmwbsswwbbmgbmsgmgmbmmwggsgbbsmgsmgwggbsmmSggsmwmsgwgbggmwswwmbwmwsbbbwg",
        "mmswssbwwsmwwwwbgsbssmgssgwgmwggmmmbssgwsbsggmgbswmmbmwsgwsgsbmbgbsbgbmgbwmwsbmmbbswwggswwgmbmSgbgmsmwmbgbmwwbsggmbwgwsb",
        "mwmwgmbwggwmsgmssmmbsbwswssmsssmgbbmmbbbwswbsggswmsmmbsgbbmmwbsgsbgsbmgwwgwmsgsbmbbbggwgwmgwmwwbggwSwwwgsgmsmsgmwwbbgbgs",
        "bbbsmmgmgwsmbmmmmbmggwwwbmbmbbsmsbmwsbwsggbsbmwwgsswsbswwggwswgmmgggmwsSwsggbsswbbsggwssmwsggsbbwgwgwmmgsbbmwswgwmbmbmgs",
        "wbsbggbgggmswgwmwgbwssbwbgswswmmbwmgmgmwbbSsbbbbmmgmmsbggwmwswswbmbsbwwbgswmbmgwggmmssgsssmwsmmsmsbmmwsbsmgwgwgbwgggwsbs",
        "wbgmwmwssmswmmgssbbsswsgbswbbgwgmgggbmwwggsggmmbwmmswmgsmsbsgsbbbgwmwgwsbbsbSsbmmwgmgmgssswmsgswwgbmbwbmwmggwbwwbbsmwbmg",
        "smbbmgwbgsswwssbsgmbbbgbgmmgmgmwmwgwsswmswmssbgwmmwgmwbwwsbmbbsmmmsbswmbsggSbsmgbwwbmgmgwbwgmssmsgbsbbwwgmgwssbwggwwsggg",
        "sgwsbgbbgmmmsgssbswssmbgwSgwmssgggggbgbmwbmmgsmbgmgbwgwssgwgwbmgswbmbbswmmmbbwwwbwsmbwwsssgmsswgwmgwmbmgbgbwbwmsmwwsmsmb",
        "swwbbgssmbbbmwwggwbmgswwmggbbwwmmgmsgswgmbgbgbbmgwwmmwmswmsmwgswsbgggbbgwsbsggSgsmssssgbswbmwbsmsmmbwgwmwsmmbssmbwgmbgws",
        "bSmbmmgwwwgbwgwbbsmsssbwsbgbggbmgmswmmmbmsgsgmwwwbgsssgwbbwsmssbwggmbsggbsmgwbmswgswwggggbgbsgwwbbssmwsgmwbmbmmswsmwwmmm",
        "bmsbswwwbbbbsggsggbmsmgwgbsmbwswmmbsgbgggwmwwbggsmwbsggSgbssgwmsmmgbsmgwssmmgwwssgmmbggswwmbwgwwwbgwsssswmwbmbbmmwsmbmbm",
        "swwgmSsgwsgbgssbsgwmmmmbmmsgbgsmmwssmssmbsbbmgwmbwbmwbgmswwbsgmmmwmbgssbbgwmggswwwgbbwggwwgwwmssgwgssmgwswbmbsgbgwbbbmgb",
        "ggssbwwbwbmbwmgsggmmwgbbmwwsbwwmmggbmsmmbmgsmwsmwsbwwgbgsmbsswwmwsswbwSwbgbmbgmmgbmssgssgsmmsgwsbgsgmbwbwggwmsmbbbsgwsgg",
        "wwssswsmssmbbbggwsswmbwsssgwmwbswbbswgbggmbmsgggmbwswwmbgbbbbwwwbgmwmwbgmmgbbwssgwswwmggmmgmmbsgmsbbmgSsgssmwwgmsgmmmggb",
        "mgwbbbsmsgbgbgwwwwwwggwmbgbwsgbwbsssggmssssgsgmwgwgbbmmmwgmsgswsgsmbmwbwbsmgmgwmbgbssSmwbwmswswsgmbbmsmmbsgswgbbmwbmwmmg",
        "gswggmgsmmwgggbsmbsbgbwswmgbmmsgbmsmmmwbmwgbsgbgbswmswswwmbwSbbggggbsgbmwssgsmbmwwmwbsssgwbwwmmmbbsmbmswsbmwwgwwswggbsgs",
        "wwbsbwsmmwsbbmbmgSgmsbgwssmgmgwgmwmwgbgswmgbmwbsmbbsgwmwwgwsbmbgsgsmsmsbggswsbmsmgbgmwgwgwwwbmsmsbsbgwbgggssmwbbsgwbmsmw",
        "gbwgmbbsgmmwbsmbssgsmwbwmgbsgwmwsgSswsmggwwwbgbmsggwgbmwgbwmmmsmsmgsbgwmsggwmmwsmbbbgwbgsgmsbmmbbsswbbssgwwbswsgbwgwwsmm",
        "wwwgmgbbwsgwgggsmsmsbsbmsgwgswmbbbgmbsmmssmgwgmbwwsmbgbmmmbwwgwwwwmwmgmbsmwmgswswgbswwssggsmbssmbmgbggbbbsgswwbmgSmgbbss",
        "mwwbgwbbmbsbmwbbgswbbgmsbmwgwgsmgwmgsmmbggmwbmgwwssgbswmgmsgmbbmsmwgwwsmmggggsSgbswsmwbbswmmbmgwwsmswbssbsmwggbsgswgwsbs",
        "sbwgSwgwmmwmmmwsmbwsbgsbmggmmwwbwgsmsbsssgbgggggswggwbmwsmgmmwmmbbwgsgmsbwmwgswbbsbssmbmsbbbmswgmwswbsbwgggbbmbgwwsgssmw",
        "ssbbgbwmbwsbsmbwwmgwggmmbsmsbsmgbwbbmgbswwwmSggwgwmggsmbbwwwswggbmsmgmbgbssbgmmssggbmmsggbsmmmsswwgwwggswbwgbwsmwwssmbms",
        "gggmwmbwmSbswgsbwmwwmwmssbsbmgbssmwsmmwssmgsswgsmwgwbwmwbgwwbmbsbsbwgwmbgmbgmsggssggbgwmgwswbmbmgbsbmgbwwsgsbwgbmmsggbsm",
        "mggmsmmgmmbgsmssssbwgmwbbmbbmgbbggbwgmwgbbbsswsbsmmwwsgwbwmsSwwswgssgmwbbggwsmmwwbmwbwswgwswssssgbbgsmmwgbggsgmmgmmbgwwb",
        "sgbssgbwwwbbmgbggwmbsbmmmwggsbmbgbgwmswsgmmmsgsbbsgsmgbwwwbmsmggggswwbssmsgSwbmwbbwgwgswbwmmbbgsgsswgmbwbswmwwmmwgmsmsms",
        "sswbmbwbsmbbgmsgbmbmmgwgsgbgbwmssmgswsgggmbgmswmbssbmbwmmmbwbmbswsggswwwmwwmbmbmgswswgbgwssSwggswmmbwbwwwbbswssggsgggmmg",
        "bwbbwwmwmmwgmgmmgmsgwwgsssgbmsgbwSbmswbsmgwbmsmmsggsbbwbswswmbbmsgbbmbssgwggwgmwsbmswgwsbbbwgsmmmssmwbsggbgwsgwmggwwgbsm",
        "gwmswggsbwwmsbbsgmwbsbwgmbmmgmwwwbgbbssggwmswgmbbmgbwsmwmbsbgmsbbmSmswggbsmwbmswsgwwwwmgswbssmmswgbsbwsmmgwssgmbgsggmbgg",
        "mmmbgggsmbgswwmwmssmgssbggssgswwgbbSgmbmwgbmwwwbwgsbmbbbwbmswssgbgbmggsmmmswbsmwsggwgswbmwwgggbwbwwgmmbmsbbwsgwsbsmwsmsm",
        "gggbgwbwmmmbsmmmmwwmgsgswbwgbwsbgsbwswgsmbssbwgbmwgsgsgwmsbbwgbgsmwsbmbgsbwgsmggmbswbgmmgsmSwsbwssmmwbbbggwbwgwmmwsswmsm",
        "wmgmbmwsggbggsswbwwbwwmmbmgmbsggsggwgmwgmsmwssmswbmmswssbwsbmmswmsbbbgwSswbmbgmbsswbggswwgbsmbssbwsmbbwggmswbgmmwmwggbgg",
        "gbbwmwwwgwbgbbwggbbmsmgbmbbbmwbgswmbsmsSbwmmmgbswwmsbmggswgwssgsmsmsgsmswwmmmwsgggbssgwwwsbbwmggwwggbsgbmsmsmssbmwsmwggb",
        "gbwsbsmgsgwsmmssmmsgwmmbswgggbgbgbsbwwbbbgwbsgwmsbsgbmmmgbsbbwmswwbgsbgsmwgmwsmgmmgswSwwwmwmgsbsmmmbssmwgwwgbwbwsggsgmwb",
        "wgbbgsmbmwwsssgswmwmwggsbmwmbmwbmwsbbssbgbwwgswsmwmbbbsgswssbwmmgggsmgwbwwggSbwmwmgmmgsgwmbwmssgbbgmssgbsbwgsgmmsgwmmgbb",
        "bwsmmmgwgwmwwwgbwwgsbsbwbsgswgmmmwgsmgwbwgssbwwsbgwsmbmbmbgsmmmgwbgwwssssgwwbmsbwwgmgmsbbsSmsgwsgbbssmsbggggmgmgbbmbbsmm",
        "wmgswgbwmmsbgwsbwsSwsgmwsmsmgwgbsmmsbswwbbgmbwsbgsssmmsgwmwbmsssmbsswwgwbwbmbmgwbggggmmwbmbbbgbwswggwbmmggsgwgmgbwgbsmms",
        "wwmwmbmgbbmsgwmbgssgmwswbmswgsmwgswbwgggmsmmmbwgmgsbsbgsgwwgbgswbgmmbbswmmbgwmbwbbgbswbgwsmwgssmmsssbgswSgbmmbgwwswmsgsb",
        "sswbgsgmwbwbggbwwmggwmmbbgwgmbmbgmbgbwsmgmwssbmswwwgmgggbsSggmwmswsmgbgwwbswbssssbmgggwbsbsmwggmsmwmwbmmwbmbwmbssswsssmb",
        "bsgbwssmwgsmgswgmgbsbwggbmmsmgmmgmbsmmmsmsbswsgbmbmbbwbmgwbbwgbwgwmwsbggwwSbgsmwggsbbmgwsgssbssgmwmbbggswwwbmwwmwgmswssw",
        "wmmgwmssgbwssbggmgsmmsgmwggwwswsmsbmgwwbgssbbbbsbmwSbswwbwbmsmmmsbsmsmsmbgwwsbwgmwgwmsgbggwbsmgwbgbmwsgggwbwswmgggsbbbmm",
        "gswwwsmmsggbwgmssmwbmwbmggswsbbswwmssgmmbmbggsbwwbsgggmsbbwgwbsbwmmsbwwbmbgmssgwmgmmmwgbwwbbswswgmmSgsbmgbmgssggwmsgsbbw",
        "ssgmmmgbswbwwmmsmgmsbmmswwbsbwwmwswbmmwggbggwwbbwsmsgbmwmbwbggsbwswsssmgmmwsbbsgbswbwgmsbwgggmsggmwwwbmmbsmssgggbSsbgggb",
        "bwmwsgsswbmmswmsbmmbwbwgwbbsmwsgsgbwswmgsggmwswwmbbsmmbggwwmmwbbwgSsggsgswwmgmmwbgbmssgggbgwbmbmbswbgbmbggbgwssswsmsgsmm",
        "msgmsmmggmgwsmsgbwwgsbbmwmwbgwgmmwwsbmsbwgsgggbsmwmmwswsbgsgsbgmwsbwwwwwbgbggmbsgbmswmbbswSbsgwssgmwbbmsbmswgbwggsmsmbbm",
        "mmwsbgsssgsmbwbbmmsggmsmgmgsbwgmbwwgbgwmmwsbssgbgbwwbsgmmgsbwbbggmgsggsbmwsswbSwwgsmbbbmmwwwgsggwwwmbbswgswmwmsbsmmbgmws",
        "bwswgbgmmbbgmwsbgwsmgbgwbsbmbsgwmSgwbgmggsgswgsbgswgbsmsbmmwmwmwwbssbwsmbbmgwgsmwwwwbbswgmwgsgssgwsbswsbbmsmmgsmmbggwmmm",
        "smggssbggbbsmbbmwgsgwgswsmmbwggwmgsmsmbmwwbwgsggbgbbswbmssmmswsmssbgwswmsgwmmbSmmbgbsgsgswbwgbmwgwwwgmbgwwmbbmsgsbmwbwwm",
        "bwsssmsmbmmmbmmmwmbswmbwbwgsbgbsbggwmwggwsmwwggswmggsswbmwmmsmsgmwgwgsbbbbbwwmbsbwsggwgwwssgbgsbgmswbsmmbbmgwswSggmsgsbg",
        "mmgmwmbbmwsbSbmggwggsmwmbsmswbbsbbmgwwssmbbwmmggggwwwgmmgssgwmbmgmbbwsmmwwbggswbwbssbggbsmswwsbbgwgwggmsbssswwsbmwgsmssg",
        "wwbwmsbwbsSgmgmmmsgmbbsmgsgssbbgswmwgmbgwgmwssbwwmggwmsbbbmgbbbsmmswgsgwwbwwggmswwsmmbwsbbmgmggwbmsbsmwwsgsgbmmgsgbgssww",
        "mmwwmmsssgswgbgbbbsgbssggsswbmsggmgbmssmbwssgwgbbwwmwmsbbSgbgbmmbgsmmmmwwswmgwgssbgmgswbwggwmgsssggwbwwwwbwmbgwmmmwbbmsb",
        "sgbbgbsmmsbwmwwsbwmggwgwmwmmwmbbgsbgwssgggwswgggswssbswsmmgmbsSgsssbwggwwbsgmwbgwbbbmsmbmwsbsmmbwwsmwmmsbmbsbggmwmgbwgmg",
        "mmmmgSsgwmwbsmmbgwbgwmsmsgbbswmmgwwbmgssmwmmwwsgbmbbwbgwmmbgggwsgbwwbmgsssgwgsgsbgwmsswmwbgmbsbmbgsgwbgbsmsbwsbsggwwsbsw",
        "bgsbsggwwbmwgwbsbgbmmmmgssmmbbgssmswssgmbmbwggwgbwgsgwsswbsmgwssgmgwmSwwbgmbwmbggwggwwbswbbswsmbbbmmgwwsmswbmsmswgbmmgsm",
        "wbmmgsbbwmmmbswsbwmgmgwsbmssmwbmwwsswgsmwmsbsmgbwmsbgwgsbbbmgggbbmgmgggwswwmsggmbgwgmwSsbgwbsbmsbgwgsswmgbmswgwbgwsmbsws",
        "bwgwssmbgwwmmmgwbwgSsmgbwmsgbgssmsgbgbwgsmbgggbgwgmswmbsssggsmwmwwwmbsmmsgbmbbwwwswmmsgswbwmbbsbggmsswgswsbwbbmmsmgbgbmw",
        "ggbbgmbmbwwwswwbgmmbgmgmgsSbwwbsggwmwwsmmsmsbgbgsbgbwgbsmbmssmbmmwgbbwgwwssbgsmgwwsmgwmwmgsmssbmmgbssbsmwwggmwwbsgbssgsw",
        "mmwbsmsbmwwsmbsgwbggsgbgmbbssmmwwgmgmbmgswbmmsggwbmgsggwbwmgsswmsgwwbsbsbwwmggsgmbwgmmgbwsbswbwwbmsbmsSbgbswmbgsgwmwswgs",
        "ggsgmsmsgwwsmsmwswbbsbsmwSsgssmwmmgmbsgbbbmwbwsggmgmwbgbgmggbswgsgwwmsbwsssmwwgbgwwmsmmgbmsmsbbwggmwbswwbmbswbgbmwgwmbgb",
        "wsmmgwsmgssbmmssgbmwsbgwwmggbgmbgmSwmssmswswsgggbbggwsbsmbwsswssbwssbbbbmggbmmbwwwwgswwwsmbmmgggbwbwmgmwgwsmgbmbmmbswbgg",
        "sgwbbbwbgmgbbwbwwmbggbswwssssmgbwwmbbggmmSsbwwmmsgsgwsgwwsgwswbggsmmsmbmmgsmssggsmwmwgbgswbswgbbgwswmwsmmmsgmmbmbgbmwbsg",
        "bgbgsmgwgswsbgbgwwwbsmsgmgswmmwwswgSwgmmsswmbbgssgmbgbmssbmwwgmbbwsmmgsgwsmmgswwbsbsgsmsgwmgbbbwbwsgsbwsmmwgmbmgbbgwmbwm",
        "bwwssmsggggwsgbmsssbmbmgbmmmwgsbmmswbgbmwggwbmwgwwswwssmgbbbbbbgmbbmmssgSwswbmsbbmwsswwmmggbbwgswsmsbggwmggmgmmsswgswwgw",
        "mssbbmsbssmsmmmggsmwmwswsgwmggsmsmwbwggmwbggssggbgbggsbmsmbwwbbsggbmswgssbwmbgwggswbwwwswwbwSwwgssmbmmgbmbmwmmbggwwsbmbb",
        "gmwwswbssswmgmgmsgbbgmbggbgwgmgmwbgwgbbswbgmbbsswgbmwbgmwmwswgsgSwbswgbswgsmmbwsgbgwsbmswmmwbwbssssmmmbwbsmmmmsggwwgmbss",
        "wmgwgwmbsgggbsmbgmswbbwbbwwmsgsgwmgmsmwwsgbgbssgmsgbmgsbswgbmbgsSmwmsswbgmbbwsbmsmbbmwsssbmgmwbwgsgmwswsbmgbwgmwwwgmswgm",
        "smmbsgsmbmggbwsgmssbmbsmmgwgmgwwmswgswbssgwwmmbbgbmgwswwbsbwsmbmmswgbmgsgwmbssmswgwmbswSmbggsbmbwgbssggswmbbwgwgwmwbggbw",
        "wbmbwbbsgwmwmwwgsbwbgmsgmbsgwgggmsmwmssbgwsbmmsgsmbsmgsssbgggmbmggwwwbwsggbmsswbsgmsSgmbsbgbmbwwwggwmwmmbsbssgwbmwmwmwsb",
        "gmbgbwSmwbwbwgmwswgmmmwbwbswsgwbmbbgsgbwmwbbsgsmwsbggswggmmbmsssgmwswsmbggmgsgmbgsmwwgwgbbssssbmmmsbwwbbsmgwsbgwgmmssmwg",
        "ssswsmswwwgwwmssmbwwbswsmsgswgswbgwmmbmbswSwggsgsmmbbwmwgbmbbgwsbbwsbgsmmwbbggbwmsgwmsgbgbmgmmgbmbmgggwmbwmsgbbswmgmsggs",
        "bbbmbgmmmwSmwmsmmbbswgsgbmggswbsbgwsgbswbmmgswssbwmmwggsggmwbmwwbwsbgbgmwmmwssmbgbsmssgbwssggsgsbgbwwgswwmgwgwmwgmswbmbs",
        "wwsbwwsbmmwmsgmgwgggbmsgsmwsgsmmgmgsgbwwbmbwbwgswbwmbSmbgsmmmsggsbsgbsgbmswbgbmwbsgwsbwwgbbwggsmmmwmbbssmwwgsmwmggbbssws",
        "mgmggbsmwssbbbbgwwwmbmswssgwgwgswgwbggbssmgggswbswsbwgbwmmwgmmmswssSwsbgbwmbmswmbgmgsbsbgbsmmsbmmmwsgwwwgsgmmmbmbbbwggsw",
        "gbwbgbmswmbbgsgwbwgwwwwmgmgssgmbsggbbsbwsmmgmgsgwmmsbwsmmmsgsmsssbswgggmSgwgbbsswgbmgmgmwwwsmbbgbwbwsmswmsgsbwbmmwbwmwbs",
        "mwwgggwgwbmbggsbbwsgwwmwbggwssmbwbbbgswwmwmggmssmbbwwbwwswswbmmbgwssmbbsssssmmgswmmggmmgmsbmmgwgmbbbgsbbSmsggsbswwggssmm",
        "mwwgsmbsbgsbwggbgwbwwbbmwbmgssmwswgbswwmgmbgwwsmgsbmgwbggbwsbsgbgbsmgssmmbbggwggmwmmgwssgmssmbmbmbwwwsssmbwmmsbggwmSssmw",
        "sbsmbsswmsmbwsgbmsbmgwgwgsswgssgbgwbbswmmmmwsbgmwwwmmbbSgmbmsmgswsgmbgbgggmbgsgbsgbwwwgmbwbwbsgsmsgwmswbgbmgswwgwmmwmbws",
        "bmbbsbgbgbsmbwsgwgmwgwsbwmmggbbbsbbwssgsmmmmmbmggwmwbmswgssmgswbmmwmbswwwswgssmgggmwgmgggsmsbwwbmsgwswgbSgmwwbwbsssbgsmw",
        "bmbwsswwswwmwgbgbgwsmwsgwgmsmgggwbbmwgmbbbbmsgsgmbwbmgsSwbsbbswbmmbgbsmbsgwssmmbwsmssgggbwmwwssgwmmbwmmmsmwgwswgsgsggmgb",
        "wssbgbmwsbsgsbwgmwbggmgmsgwgwwwgmsmsmbgwmmbbmwgbssgsbbmbswmgmsmbgwbmwwgbbsbgwmsmgbbmmmwsssggsgwmwswwsgmwwgbSmgssbmswgbbw",
        "wwsmgmwssSgggwwsgggwmmmmwsbbwbmbmmswbmsmgbmwbssmwggsmbssbgsmbwswwbgbbwgwbgbmbgbwmgmsgbggwbssssmsggssbgbwbmsmwwswwbmgwgmm",
        "gmbbbgggbwwbgggbgwbbgbwmbgwsbwgmwbggwmbsgbbmsmmsmgwbgswwsmsmgmgwsssbsmbsmwmsmmmmwwsSwsggwbwbbmmgmbmsssmmbwgsgsswwssgwwsw",
        "wwmmmssgggmwwbbmwmwSgggsmwbwwsmbwsgbmmsgswsbmmwggbgwsbggssmbgssgsmwwggwbbwbbsmbmwmbsgbsbsmmwbwgmbggmwsmbswsgwsbggwbsmmsb",
        "gssmwwwmmgswwwbmggbsgwwwssggmbswSbgmswsmwbgwbggsmbmsggmsbsbgbbwbmmbmwwwmmssbsmbsgsbwmbwwsbggsbmgbbgsbmggwgmmsmmwwwggmssb",
        "mmgwswmwwwmwgsggbmwggswmwgbwgmwgsbwbgmbsgbgwmggmbssbsgbbwmgsbmsmsmsmsgSbwbssbwswsbgggsgbwsbwsbwwgbmmbmgmwsmbgssbwmmswmmb",
        "mwgmgsgbgmsgsbmswmgsSgsmbssswwgwwgmsbbbbsgmmgsgmbmwggbmmgbswwmwwwwgsmwwbwbmmgsswwwsbbwwsmgsbgmsgbbswwmgwssbsbmbbmbmgmgbg",
        "wbbbbbmsgsswmmwmmwwggwmsgsbwbgwgbmbsbmgbswgwmwbgwmwsgwmgwggbssmsggggwmmsmmmbSgwwsmgbbmssmgbsmgbsgssbgbswgwbbmwwsmsbmwwss",
        "bbsgbbgsgggwbmgsgmwsbssssmmswwmsgbggmggsgmgbwsgbwmwwsgsbgbmSsbwgbwsgwsmmmwwbwgswsbmwmsggwwmmwbbgbbmsmbmmsbmgwbmmwwwsbwms",
        "sgsgbssssgbwwmmmbmmwgbbwwggswsmsmgwsbbmssgggwwbbbmwsbggwsbsgwwbbmgwbgmmsswgbwbbmbswwgbbsgmmwwggwgmswSswbmggmmmwbmsmmgssm",
        "gssmbwbwggmbswmgggbbbmmwbswmssmgwSwwmbwbwgswsgbbwsmswmwmwgggwgmbmwmssmggwbmmbswgwbwsgswsssgmbbbmbbsgggswbbmmbsgsgwmgmssm",
        "bwgwwbwswgmwmmgmwgmggsmggssmswmmssbsbgsmssbmmbgwwmgmwbbwmgbsbwssgsbmggbgwwbsSwwbmwswbgsgsgssbbswmbwswmmmbbmmbwgbbwmsgggg",
        "wwgswggbgmwbbgggsbsbsmbsmwgsmbssswgwmwsgbwwwgwswmmbmmbbsgbbmsmgmwbmbswsSwwggmgggbwwgsbgmggbmmmbgmmwsmwwssmbgbbmssmssbwws",
        "wmsmwmbmbsbwgsmsgsbwsmmssbggwbbsbggwwwggwbsmgsgwsbwsbmbbbgswwSbwssmsggwwgwgwgswsbbggwgwwmmgmswmbmssmbbmgmsbmgbbsgmgmmmwm",
        "wsbwsbmmsbwgsbgwgbsbmmbgmwwbmmsmmbgwwmgssmmsgswssmwgmsmgsgggswwbsssswwbgsmmwbmbmwwsbbbbSggbmwsbsbbwmgbmwwmgwgmbgggswgggw"
    };

    private static final String[] levels15x12 = {
        "wsgwSbsmgwmmmgwgwswmgwbmgwsmbsmgwsgwssbmgmmgggsbsswsmbswwgsswggmbmmwbmgbsgwgbwgbbmgwwsmbbmmmmmsbmsbbgwbgmwsgssbsmsswgmbbswmbmsbbsgsggswwwmmbbwbwbsswwwmwmmbbsggwgwmgbwgggbbgsgmwsbsb",
        "swbwbwggmgwsssgbmSwmwgggbgwmsbwsmbwmmwswgwggmgwgbbwsmgmssbmwwmsswmmmbmswmbmbmggmwbmgmwsbswwmwbgbmsbwgsbmwgsbmsgmwsgssmssssgbwbbgggbgsmssggwwwmwssbwwggmgbgbgmsmwbmgwbsbmbmgsbsbswbbb",
        "mbggssgwswbwgmmgmsbwswbgmssgwsgsmsbmsssbwmmbgsmgsmsmgsmgbbgwwbmgbgbgmwwswwbwmwwwsgswmsmswgbbmmsbsbgwbgwsbbmmgmswbmwmgbSswwwgwmsmbmbbmbmwgwssmwgmbbsggbswsmwggmgbbsbgbggwwwgsbbgsmmgw",
        "bgmmmmsSbmgswgmsbbbsmbmgsggsbsmgsgwwgsgbggwwswsbggbswsgwwbgwwwswmwmmsbbbmmmssmgsmwwbmbwsmswbssmwsbggwbwwbswwbmbwbwgwbbswmggbmmsmmsbgbbsmwgggbgmwggwsmsbsbswbgsggmswggwsmgmmwbmwmbmsg",
        "wbsmwwswwggsgbmmgbmgbsbwbssbsbbmsmswgmsbgwwmggswgmwgbmsmsggbgwbbbsgmsmgbbmwmwbmgsgmggbmwmmsmgwwwsbgmwmbmbggsmwwgssmbmsbgssgsmbmwsgwbsswgsgbmmwbgmsbwbswwswgwwwwwmwsbgSggsmgbbbbwsmsm",
        "sswwmmsgwbsmmmswmgssmmgwmbwbwsmggwbbmwbwsbwsgsgggssbgsswgsmswggwmbbwbbgbgsggbbggwwwsssbbgsssSwbsgggwmbgsbmbgmbwwgwmwsmmbmsgwmgswwbmgmmmmgmssmgsbggbsgwwwwmmwwwmmbwmbgbbbbsbbwsbmmsmg",
        "smbwwmwbbssbwgmwmwsmgmbssmmmgwbbwgwgssgwmmwsmgmbmsbsgbgwwmgsmgwsgsbmmswgbggsgbsmgwwgmmmmgbswwsgmwbsgsmbssbmmbwsgbmswgsSmsmwbwgsgbgwmgbwgbbsmwsggsggwbwbmsbgswbwgwwswbbsmgmwgwsbbbbmb",
        "bbbgwmbgsswssmgggmgmgbgwwbgmwssgwsgwwgsmbgwwsmggsmgwwmwmsmbwbbmmbsbmmssgSwbwbmmwmswsbmbbmwmsbwgswmgbgbbgbgmmssbgsmgwmgsmswgbwwbwsmbbbswwgmsbmwmgwmswmmswbwbwgssswggssgsmbbgmbbggsswg",
        "mmmbwmgsbmwwmmsmsmwgbwwsbsssgbwsbmbmbbsmsbwgbbmggwgwssmssgmgwsgssswmgswgbggwgwmbmsbsswwwmbgmwgmwwsbggbgbsswmbgmmbwssggswgmbgwwwmswggbbmbwggwsssbmwbmbbwbgmsmbgsmwbgmgggbmsmgmwbsSwbw",
        "mmwwgwbbggswsgbbgggsbmmwgbmgwbbbmsbgmbgwgsmmwwwsbsswmsbwsgwsmbgbmmsmsmwmmgwgsgmsgwsbggwswswwwsgbwgbggsgmwsmmsggbmbwbssmbbgbgwbbswbmmbsmbmsbbwmmwswgbgmmgmsSgbssbmsgwwbswmgsgwswwmwms",
        "gwgwsmwmsgwsggwsssswmwmbbwbgwgmbgggmwsmbmmmbsggwmwbsggwwsbbmwsbggggsswwbwmbwwwbbgwwwgsgsggbbsmwmbsmwgsgbbmmsmswmggbmbmsgSsgmbbbgmwmssgwwsggwssbgssbmsbwbwmbmwswmmbbgmssmbbmmbgmsmbws",
        "bmbmSgmgwswmgmwsswgbsgmbbsswbmmmgbbggssgmswwwgwggmgggbgsswwsgsbbmgmwgbbsgmggwmsgbwmsbwsmbbsgssmmsmmwmwswwgsbmmwbbwwmswmgmmswwswwgwmgsbbssbbggmbbbsmgmmgsgwgsbwbbmbbwbgsmwwgwwmswbbss",
        "gbmbmbbgmsmmmgwswmwmsmwggwbwsgbssmmswwssbwgbbmswgssmsmwgwsbbgswsmsmbbbsgggggsmgwbbmwwssgmwwbbsmwbbmgsgbswwsmwgbmmbgmsmwgmmswwgwggmgbgbbbgwbswswmsswggmwmbwmwbmmbggwsswSsgbbsggmwsgbb",
        "bsgggwbbsswwmwbswggwbgwwgbgsmgwsmgsbsbsgwsssgmmsgbbgmbbsggwmgmssgmmswmwswmbgbsbgwwmbbmwwmggwsmmwmbbbwbssgwmbsgmswmwssbwwbbmsswmgbmswwsswmmbwbssmsggbSgmbggwgwmmwbgbmmssgbmbmwmgwggmb",
        "mgggmwwwgbsssbmmwbSmswswmbbgmbwbmgsbmmssbmwsgbsbwwsbgwwgggsmwbssbsbmsbgwmsgmbgwsmwbmwbmbbwmmsgwsmsggsmbwbmbsgwmgswbwmbgbbwbgbsmmgsbmsggsswmbgwwmsggmwggggmbsmwbsgsgbggwmwwmsgwwwswsm",
        "sbsmmbmwwgbmggbsmmsgswmwgbbswwsmwbbmsgbwgbgbmwmbmbbmwsgsgsgswmwbbmwwswswbgmsbsmmgwwmwgmmmgsbswsmmbwmsgsssggsbgSgsbwbbwggwbgmsgsgwwgmwmgmwsgwbwgsmwgwbbmbgwsbmbswgsssbmmggwwsgbbmbmsg",
        "mswbmswswwwsbwbswgsgmggbbbwwbmgmgswwmbmsbbwmmwgwwsbsgmsggwSswbwmmgmwgmbwgggbmwsmsbmmssgwmswgmgwgsgbsswbgmbmsmsgsmgbwmswmswbsgbssbmmbsbmbmgggbswswbmmgbwbsmgmbwgwggbgmgsmbwbsgsgwwbsb",
        "sggwgbbbmwmgsbswbmgmmwmmmswgswswsggmwbsssssmwbbwbmgggmswgbsgwsssbbmgwgmwmbwsgsbgswgmwmwmmgmssbgsgswbwwbgmmmssggmbmbwmbbmswwgbgbmwwwswbwgmbmbsggmgmwmbswgbsbwbwgbsmbgswsbgbwmmssbgwSg",
        "smwbbbggwsbbbwgsbwbggbggsbmmsggwgmwwbmmwbbsssbbmgmmmmwbssgmwswssswbgwgbbgmsmgbsbggmgmsgssmsmsmssmwmgbwgwbwsgmwsmmwwwgsbgggwsbgwgbmsmbwmbsgwswbmswmsbwswswgbgbwwgsmbmmwwggSgwmswmmbbm",
        "mmmbSbgmmgsbgsgwbwbwsgmsmgbsmbwgwgsmmwmsgswswbggmgbbmwwssgswsmwbmswggbwgmggmsmbsgwwgsgswgsbmwbmbmwbmbmwsgssmmbbmswbsbbwmgbmmbgmsggwssmbgsbwbsbmgwgwwmwmwgbbmwbbssgwswswggbsswgbwmgsw",
        "wmgwwsmgsggbggswgggbwbbwgmbwbmsgbggsssbmssmwwwsmSgswmwmgwmswmmmbgwbwsssgswsbsggwbmmwgssbgwgssgmmbmmwmbwsbwbbsmsswbmmwggmgsmmbswgbsbgmwbwbmbbmsgsmsbsswmwbbbmbmwbwgwggmmmswsbbbggggww",
        "wgswwmgmsggmmgssggmbswsmmmSmgwbsmgsbmmbsmmsbgbbbwgwmwbgsgggsswwgmmswwwgsgbmbsssssgbgbmgmbbwwswwgmgggmwswgbbwwbsbssgwgmsmmwbmswgbmwssgmbwwbbwbbbwgsswsmswgsmwmsgbgbmbwgsmwbmgwmbwbbmb",
        "bsmgwbgwgswssssbbmbmsssbssmmgwwbgwmgmwbssgbbmmbwmgbwmbbwmwwwswwssmmmgmmsmmbwbgggbgsmbbgwmbsbswmmsmbSgbmgmbwgwssmwsgwwsmgbgswggsgbgbggsbmwbmsmbswsggmsbbwwwgmmsgwgbwwwbbmsgmwgwwggssg",
        "wbgmgmswwbwsmbbbmwgwgbgbwbsmssgsgsbmmbbsmmsgswwsswsmwgbwsswmsssmgbbbgmsmwsgwsbggmwgwwmgbwwwwmbgwsmbbsgbbgbwwmbgbwbmwgmgmggwwgmwggbgmmbssmwgswwbmmgsmsgmsbgggwbmbsmwwmbgsmbssSgssbsmm",
        "bssmbsbbmsgwwwswbsbbmgbggwgwgwbwmgwmswgwmmwsswgwmbmwgwwggswwgbgwmmbbsgsmgmmsggswgbmbssgsmwwmbmbwbmmsmmsmwwsmgbssgswswggggmbbbbbbbbggwwbwsgSmmgsmwbmmgbswsssmgbsgmmsmbwsswbbsmgbmgswg",
        "sbgsmbsmwbwbbmmgmgsgsgsswgwgwmsmbwgbgggwssgsbsggwwbgmsmgmmgswsSwwmmbbwgmmwgmsmwwbbsssggwsbgggmmsmsbmbmwmbmswbwbgbwwmsbswgmwsmmgbwmbwwbgbbmwbsswwgsmbgbwwmwgbgmswgbmsggbbbgsswmwsbssm",
        "sbbbmmgbgwmggwmbsmwwwsbbssgwgbwggggbbwgsgsbmwwsmbswbmgwgbmbbbsmmgmsmmsbwsmmwbmbbwmssmwsmggwbgbwswwbbbsmswmggSbggwwmggswsbgswmswmwgmswsssgwgbsmssbgmbgbsgggwwgsbmmgswwbsgmmswmmwswbmm",
        "bwbmbwgsgmwsggmgmmmggSbwwsbsbgmgmbbsmswmgsgsmwgsmbgwwbwmwwgmgmbbmwbbmsgsbssgbmmsbwmbwgswsgswgmmgggswwggggbwgsgmwsmsgssmbmmwsbwbmbmssbwmgbbwswssbgswgbsswmsbbmbssgwwgbmmwbwmwgbwswbwm",
        "mbgwgbwgwbgmmswsmsmgwmsgsmbwwbgggbbgwwbswmmbsgssmgbgbmwwwswmwbgmwbgbssbggmwgbsgsgwbwsgmbmwmwssgmwmwsbggbwsgwggsbwwsbwbmmsmwgbbgssbmwsmbssbbmmssmsbggsgwmmwmwbmmbwssgSbsbbmsmwggswmmg",
        "msbbssgbwbbsbbsssgwssgmbswwswwssbsmsgwmwwwsgsgbgmwmgmbwmgsgwbmgsswmwswbmbsbbbmsmwbmgsbwgswmgmgmwmbmgbswgbggswbbbmwsmgSmbsggsgmmmgwgmmbbbgwmsbwggggwwmwsbmsmswbwgmgbwgwwgwmmmgbwbmssg",
        "bbssbwsbmmsgswsmwwggbbmbssmsggmmgmswmwgwbwwgbbmgwgmgsgbmmwswwwbgssbgmgsmgmmgsbmsbbsmbwmbbgmgmmmsmwgbbwsgswbssgsbsgmsgbwmbswmbgwwssmgbbwwbgswmmmbgwbwmgbbgsgbwsSwsgwsmmgwmwwwbgsgswwg",
        "smbwwwwwgssswwwbsbwbmgsbbswmwwbgswmsbgbbmmbswmbmmbmmsgswwbgsbwmgbmsbgsbgggswggwbmwmwssmggwwgsbgmbggswmsgwwwsgmsmwsbsmmggsswbmgmmmbswbmwSgmmbssgwgwmggbwgbgbsssgmmmmssbwgbmbbmsbbgggw",
        "wmsgwbswbbmwwwsswsmssbbbbgmbwmbggswwswmmbgswbmmbmmbswssmmgmbsmsgsbbgmswgwmbwssbgsSgwggbwgwwmbbbbswgwsbbsbmmbgmmgmwbwgmsswmgswsgmggwbmmwssmgsmgggmgwmmwgsgsbswgbmwggggbbswgwwbmssbwmg",
        "wggmgwwsbmwgmsbmbwsswbbsmsggsmmggwggmmmgmwssbwgsbmgwggsmgwgsbssmwbssgmbsmsbgwbbmssbbmmwsbwwsgwmsbwbwmbwbgmbwmmwswgsbmwgwmbwsmmgggwsmmwwgwmsbwmssswwsSsgmbgbbggmbgwbsbbbmgbgwmbssbwgg",
        "gswgssmgbmswwwmgbswgmggmbbmsbwswmggggwwmmbbsbwggwgbmwbwsbgmmmswsgbwsgmbwggbgmgwwswbmsbsbgwmmbgwwswsmgsbbwwbgwwbgbmbgsmmsmbsmmgmbbgswsmbbmbmmmswwssSmbgsbwwbgwsgsggsbwsmsggssmmsbwwsm",
        "smmgwwwsmsggwmwsgmgwswwswsggmwwsgsmwmgsgmmwgbgmmwsbsbmbsbwswbwssbbggbwbswsswbwSgwbgmmmggbmmwmbggbbmbsbwwwmsbwssgmbmbmwgsgsmwsbgmmmmgsmwmssmbbgsgwsgbggbbgwbggsmsbbmgbsmsmgsbwgwwbbwb",
        "wgmwsgggmbmsmmsmwbwgbbwSswmgwwmssbmsgssbgwmsswgwswggwwgmmbsbmwmgbbgmwmsbbmsmwbmwbmswggbsbbwsbwmwgswggbmsmgwssbgwbswmggbmbbsgsmsbbwsgsgwbwssswgbgbsgbmswbbggmmgmmgbgwmwbmsgsbgwsmwmmw",
        "wbsmmsmwwmgmwgwbgmssmbbwmwgsgsmwbgsmmswbbgswwbmmwbgbbwmmsgbwbbssmbswgwgswgmbsssbmgbwsbmgmmsswmbmswwgbmgbwswbswmmgbggwsssbbwmsmwbgbsggggwwmbgwgwbwbmgmmgwgswbSbsgggbgsmsgssmmmgsgbsww",
        "wwmsmgssbmmbmsmbmwmsgsswswmbgssgbggmbmgmmbmgswmbssgwswgmbwbbbwsssSwbbmwbsgwbmswgmmwgsbbwggmsbwbmmbwmwwmbwwwsgssggwggbmbsggbbbsggwbwsbmgmmgsmwwbswwwgbbmgwmgggwgmbgmgwsssbmbsgwgswsms",
        "mmbwbgsbsgwgmwwmgbmbwmmmgmggwwggbbmmwmbwmwSbmgwgbsssgmmwwwgmwbwgswbssgwmsgwsmwssgsgsssmgsgwggwssbmmwsswsgbbbbmsmgbgmgbgbsbbwbsmmsgwwssmswbsbsssbmbmbswbmmwsmgbgwwwsgmbwbggmbbwgwbmgg",
        "swswgbgwsmmbswbwmwbgsssbgmmwgwwgssbbssbmgwggmwbgsgsmwmmmbswwgsbbgsbgwmbmbbmwbsmbwbbbmsbgwgwSssmmgwmmgmmwgsbgwswbgwwsggswbmwmbggssggmwgggwwbmmswmbgbbgwwgssgswssgbmbbbmmbgmssmwssmmmw",
        "wswbmwsbwmmmmgswwgswwmwmsbwsgwgsgmmmsbggbgbmwswwbbbmwmgsbgmbwssmmwwsgbgwwssbgbswggbsgmmmswmsssbbbbmsgsbgmbwswgmgmgbswgswbgwmggmmbsmwsggssbwbwwbmsmgggbmsSbwmbbmsmgmbsggsggbwwwbwbmgs",
        "sbgswbssggbssgsmgbwsggmbbsgsmwmbbSwswsbwmwgwsmmwgwmbwmmbmgwmwgsbsbwgsmbwwgmmwssgwwbgbsgbggggbwgwsmwmgbbwmsgsbbgswgsbbwmbmssgmmsgwwgbswbsmmsbsmwmsgwgmbbswwmbmsbwbggwbmsmbmgwgwmmmgsm",
        "mwwbmsgsmgmgbmwwbwbwwsmsbwgsbsssmbmbsggmbsmwwwbwbswbmwbgwgbmbmbsbbswgswwgggbmgwbmwwggsmgsbmwmgbmbwgbmmsggbswsmmmbggbswgsmgbbmssbSwwmgbssbsmsswwwmsgggbmbbsmgmwssgsmwggwsgmmswmgwswgg",
        "mwbwbwwwsggssmbsgwwwbwswgsbmSbmgbsgsmsmsmbggsswsswmwmgwsbssgbmbbwmbmsbwgwbmggswgwmwmmbgbggbbggssbbsbsmbbgsbmbsmsgsgwsbmsgwsggmgmbsggmmbwgbwwbwgbmsggwmmmwmmwmwgggmwsssmbmbwwmwwmwgsb",
        "gsbmbswbswgmsmsgbsmgwgsgbwmbwsbwgsmwgbgssgmgbwmssmsbsmwsbgwmwsssggwmmwwbgmbmmbwwwmswbsbmSmbmwgwbbsmwgmmbbbmsmsggggbsmbmwbwwsgwgsswbgggmbwgsmwgsmswbsmwsmsmwbgwwbwmbbmbbgbwgsgmggggws",
        "mbwmgmwwwswwgmgwgbsbmgsmgsbbwgwwbgbggwsgmgsmbbgsgwmswwwwmbgsbbmgmbbbmmggbwbsmbsgmmsmsgssswbmwwsmwgbmgmggsbmbwbmmsmgwmgsgwsbmgwmwgsbwsmswbmwgswwmssbmwbsssgbsmbmwsbwgbbwsbsmSbsgsggwg",
        "msbsbgsbmwbmwgwgwsmbmbbmwmwgswmsbgbgmmgssgswswgbmmbssmmbbswbssswmsbmwgmsswswgwgwsgswwgbmmbsmbmwggbmbsgbbmgsmsmwgggwbbggswswsmmbmwsgwgmgggwmsgsbgggsbbwsmgbwwwwwbsgmbbwwmbbmmsSwbmwgg",
        "wwSmmmbsmbbwmwwsgsmsmbbwwmgsggmsmgbwmmmgwsswmmmswssgmbgmgswwswmwgbsggbwgswbsmsggbggbwbgwswwgmggbssgmbsgbgssgsgmmmgwmmbgsssbwbgwwgbbbswmgbgsmwmsbbbbbwssmmwsbwbmmgbwgbwsbwgbmgwmwbssw",
        "ggwsbsgggswmbmbmbbgsgmwwmggwwbwbggswbbmsmwssmswsmsmgbwwmwwsbgwgmbssgwmmmswwbmmsgbbggmwgggsswssmwwwsbbmsbwmgsmsggsgmmmsbwsssmbmmbbbmggssswSbmmwmbgbgbwgwgmwmwgsgbwggbmbbbswbswmgswbwb",
        "bbbbswbgbmsgwwwSgbgbbsmmmswwgmsgwgwgwswwgsgssgbbbwwgmgwbsggsbmwmmmwgbsbmmwggmbswgswbmswmggswbwwgwswbgswgsmmmggmsbwgbmwsmmbbsmmsmbgmsgwgbgwbsbgbwmmmsgsmsggbwsbbswmbssssmwwmgbswsmmmb",
        "smmbgwssSmwsswbgwwsggswbswgwmbssgmmmmbgswmgswgbmbsmwggmgmbwbwgsggwbgwmwssgsbwsbsbssbwwbsmggmmbgsbggmbbmgsgmbmmbggsbswwsbsmbwmbbwsmmwgsbbgbgwbwmmmgwmmbwbwmswwbsswmwggwbgmswmgmssggwb",
        "bmmbmbmsmbswgsssmgbsmgmsmwgwwsswggbwbwwgsswbwbmwbbgmmswmmgswmwwbwbbmbmsmsbbsmbmbwbswsgbsbmsssmbmgggsggwwbgwbmbwwbwSggswmgbgmmgmwmmmwgwbmswsgggggswbwwmgsswggwssbsggswbgmwsbsgmgbgbms",
        "Sbbmbsmmwgbmsgggsmgmmwmgmwwwbwsswssmggwbwwbbgbwbggbsggwwswsgsgbwmsmwgbmsbsbmssgsgbmbwgsggmssmbbwgwmssmgwsgbsbwgbmbwwmgwwgwwgbmsgmbgmbsbwbmbmgbwmwswbwssmsgwsmsmmsssmbwwgbmggmbgbmwms",
        "bgbgwsSmbbbggswwgggsmbwgsgsgwssbwbmmwbmwwbbmsbgbmswgwsbmmbsbsmbbmbmmgbgggwmgsbssbssgwwgsswwgmgsgwssmbbmmbwswgmggsmbgsmwsbggwmbmgmwmsmmwwbwggswsmmbsmmwbggwsmwgwsswsgbssbbgwwwmmmbwmw",
        "mbbbsbsgsggmbsmbbgbmgswsbwgswmbgsmbwwwgbmbbwmwsswbgmmbgbwwgwmswswbswbsmbmgwsgwmgwbbwgggwsmmbwsbmgmbssgmwgwssmgsmmbwwgsmmbsgwgsmbwggwsgwsbwgmgsggswmbbbwswSwsgggbmbmwgsmgmmmsmswbsmms",
        "bmwwwwwsgwwgswsbwmbbssgmggmmsgmgmmsgmmbsmmwwmbbsgbwwwbbmwwgwwbgwsbgbwwmSsggsggbggsbwmmgbmmsbwmsbgbgssmsswsswsgbmgwmwbbgwgwmgwswbbgmgmbbsssgbbgbbsswmsbmmbsmmgsbsgsmgmgbwsgwbsmmgswwm",
        "smmmwgggbwswgwsmbsbgwbbwbwgbgmbgwmbbwbgswbwwwwsgwwgbgbgwgssmsmwmgmbbgssssgbsbmbswwsmsgwgsmggggsbsmmSwwmswgwgbbmmmwbssmbmbsmgsmwgbmbgmgwswgwsgbmsgmgbsmmsbwmmsmwmbwbsgbbmwsmssmwbwggs",
        "bbwgwgbggmggsgbgsgSmwssswmbmwsssssbwgmgmsmbwsssgmmwbwmswwggwgwbmbbbwsmsmsgwgbbwgbbgbmwmmssgwggbggmgsgbbbbwwbwmwgsmswwsbwgbbmmwmsbsgbmmmgwssgsmbmwmgbbswssbsgbwswbmgmmgwwwmmwsmgbmmws",
        "bgssgbgwswmggswwgmwbmgmmggwgbssbgwsbwmmwsggswmmmssbgmswmgmbmbmgwgwmssgbswmsbwmmsbmmgmwggwbbwmsmwmwwgsbbggbbwmsgsbbsbssbwsgswbswmswbssbsgsSbgmwgbwmgbwmmbwbgmbwmgggmgsbwgswswmmsbwbwb",
        "bmSswwsmbbbsgsmbmgsgwgwbmmgbswwwbmswmbssgbmgwbmwswwbggsmbmsbwwswsgmbsbgmswbbmwgmgbwwmwsggbsgswbgswswgggbwbmswggbmgmmmggsmmwsbgggmsggwmgwbmmmbswsmbmwgbwsggsswsbbbssmmsgmwbwmsmbbgwsw",
        "bbmbmswbgbwsmwbsbwbbmwbgggwbggbgbwwmgmgmsmbwwsmwsbwwsgmwgbbgswswmsmmssssgggbsgbgmmSbmmgmmbgswgsssmmmswsmbssggmgsmbbwmgwsbwwwwwmbbwmbmgmwswbmswmswgsgssgwbggmwbgbmsggsgmwgbwwgssmsbbw",
        "wsssssmgmmwmgmmwwsggwgwbgsgbggwmbmwSswsmbbsgssgbmmwgmwwwmgsgsssswwwbwbsmbbgwsgggbmwmbmsgmbgggwbgmbwgsmgmmbbggwmbwmbbwbssggbwwgbwmbwsbmbbbsswbgbwmgsmbssbsmmmbwsssgwmwggbsmwsmbgmmwsw",
        "sbmwmbgswsbmbwmswsbsmwsgbggggswssswmssmsgwgsgggswbssmsmbbgwwmbbwmbsmgwmbbmmsbmmwwswwggbbgbswbwsmSwgsgmwmgwbbmgggmwggsbgmbmmgwwwggbsmwsggwgwbwmwgswgmbsgsmswwbbbbmmwmmssgsbmsmgmbbbbw",
        "gmgmwgbwsbswwsssggsmsggbmwgbwgmwbmssmggswgsbsgssbsmgmsbgbmmwwsbwwswbmwwswbbwbgsmmgbbmggmwbbbwswgbwmssgbbgmgsgsbmbbmwwSwmsgsmgmbmbwgggwbwmmgwmswgwgsmbmmwsgmswbwswbmbsbmgwsmgmwmbgssb",
        "mmgwmmbwwwwwssgsbggmwmmwwmbbwsmbSgbbsbgsbsbsgbmmswmbbssmgmsbbbsmgsgssgbbgwssgmgsmbwmbbwwgmswbsgmbmmsgggswswwsbwmwggwggmmmwbwgbggwmmmbsmgbwmsmwwmmsbmbwbbgwwwwgwbgsbgsggwgsssssgbmswg",
        "mswmmsmgssbbbwbbbbbbmmswgsgwwbbgswmbwbwgbsgwbbmmbbsmgwgwgwmbwgmmsmbgwswwsggssgbgbmgwwbwsbmwgbsbwwggmsgsbbsswmwswbwbbmsgswsggsmsggmgbmsmbmgmgSwgswmsmmsmsgswmgsgmbmgsgmbwwwsgmsmmgwww",
        "wwwmmmsbmmmbbgggwbmbmgmmmwwgmgmsgswbgsbbbgwbwbbbmwbmgwsmwsgsgggbbsmbswbmbwbbmmgsggwswgsmbsgwbwwmbgmsbgwSgbsgmbsmbbwwmsbgmwssswsgmgssgswmwbwsswwmbgggsbwwsmmmgsmssgsgswwgsbgmwsbgwmsw",
        "bbbswswsmgwwmgbssbwbwmgsbmwsgbmbssssmggwgsmbbmsssbsmgsswgmSgbmgwwgsggwsmbswgggmbwmwwmggwbbgsswbggwwmbbwgbmgmswwggsgmwbmsmbbmggwswgwmmmmwgwbwbwwbbsbbwmwwbmmsgmbmsgmsmbssmgmbwsbmgssg",
        "sgbbwmwmgsgbwsbmssgwmwgsggbbmmggmssgwgsgwbbgsmgmgsmswsswwgmSbgmgbwsmmgsswwbwbsgmmwsgwwbbwbssgsbmwwssgmwbbgmwmbbmgwgbwmwmbbbmwmssmmbgswwwsgsmswsmwbggwwbbbmmgsbgmbgssgwsbmgwwmbgmsmbb",
        "mgbmwbwgmmmsbmgbgmssmgwsmgbwsmbbgbwbsmssbbwsmgbssswgsbbmswbmmmmggbbwgwwbmsgbbmwbsbswwmmggggwwwggsgssgswwmbsmswsgmbggmbmmSgbssgbwbgmmbsgswwwbsmgbgbgwmgmsbsmmssmgwbwswwwsbwwgwwwgsgmw",
        "swbsbswbwbwsgsmgmsmwwgbwmgwwwbbbmswmbgmgggwsmmmbssmgmwswggbmwwwgwwmmgssbbgwmgbbsbgsgbssssmmmsmgwwsmmmgmgggssgswmbbbmsmgmwsbwswgmgwgwbbbwbgbmswwmgbbgbsSwbmmwbswsgmwgbsggbmbwssmssgbg",
        "msbsmwwwmwwbggbbbmbgwgwbwgbwwmgmbgsbmmbgwmmbmsbgsswgswmmmmmgsmgwmmmggwwbmbmwggswsbsbbsgbwbbgmssgwmggmssgbgbsgbmswmsgwwbgbssbsbsbbwsbmswmmgbswwssgwmssmssggmgggwbgsmbwmwwgwswbwgsswSm",
        "ggsbmwmbwbwgggbwmmsggbwmwgsssSwmsbbssmswmsbwwmwbgbbbsgsmgbgbbwmwmgsswmwbbgsmmbmmbsmwmbggwwwbwgmbggbmmgggmmmwsgwswbmgwsswwsmgwwmbmgbmsgbgbsgwgbsswbwsgbsbbswmmbwmgbsgmgmsgssswswssgmw",
        "ggwbsmgbbgwwsmwbmmgbbssmgbsbsmmswmbgbsssgmsmwwsmwmgmbbmbwSwwmgwbswgwgsbssbgswbmgggwbbgwmwbmmgsggmgwbmwwbwsmgwsbgsmgmwmbsgmsmgbgsgbbwgsgwsmwsbsmmsbwsbwmswgsbwswgsmwbwwmgsgmsmgmbgbwb",
        "sswwgmbsbbbgbbsmsmgwwwssbssgmwbmgbbmggwgssgmswgbmwbwmwwswgbmggbgwgbmmwsmgwsgbmwgssbgswmSgwbwmgmbmbmbswmwggmbwsmbsmmmbswbgbssswsmbswwmbbwgsmgsggwsmssswbbmbsbwgmgwggwwbsgwmbwsgmggmmm",
        "bwsmbgbmbgmmgmbgbgwgmmgbsbmswsgsmssmwmgwsmgmmswbbsgmbgbwwsgswmbwwwgbwmswwssmbmbbggbgwmgsbmgmswswbSswgbbbwwmgsgsmwmbmwbmggbwsbbmgggwsbggwmbwwssgssmbmbsswmgbmssmwgwbwswgsmwwgwsgmbssg",
        "mbbgsmwsgmsmmbgsggswgbbwbwsggsgwgmwmbswmwwsbgwsswmgmbbbgmgmsgsmsgsmbgbggggswgmswgwbwwsbbbmmbswwbbwwsgwwbwmmmmmmbgsgwmwssgsssbbsmbbmwmmbwbsmggbmmbwgsbwwgsgsgwswbggbwssbsmmSgwbmmwmsw",
        "smmwbbwsswSmbmmbgsgbwwswsbgmmmmwwggwwbmwbsgsmswgmmssbmgsbswgbmggbmssmbmgbgbwwwwbgbwwwwbmbwbgggbmwmggssmwwsgssmwbgsbwmggbwwsbsmgwwgbsswgsbgsbwbbgmmmgsmbwsmbgbgmsmwgswsbgsmsgmssgbmgm",
        "swsssmmggsssswgswbmbwsgsmwbgSwmbgsmbbgsmwwwbwgswbwwsgbbwwwbgswsmgbwbmmswgmmbbwmmgbmmsbgmmbmwgwmsgmbbssmbsmbgwgswgmsgwwmwbgwwgbmbsmgggmggbwwsgbggwggbssbgssbmmmmswgmsswwmgmbwbgsmbbsb",
        "mssswmwbsmbwggbwwsbgsmbbswwmbbmbgwgbgsswwwggsbgmggwwbwmwbbswswwsmgsggmgsgmsmgmgbgmbmSmgmmwwbbbbgbggwbssmwmmmbmsmgsgwwwsbmswgwsgsgmwbsgssmgswggsswmmmmsbbmbgbwmwsgmswsmgwbbwbswgbbsbm",
        "mbgbmwmmgmmwwsgwsbwmbsbmmswggmsggmmwbsbwwwsbgbggbgswbmmswmssbbwgmmwbwmwbwsbssgsbsmmmbgbsmmbwwssswbwbggssmbgmmgmswwgbbssbmggsgssmgmgmgbgggwwmbsSwbbwgsmggsgbswgswgsgswgwbwbsmwwwwbmmb",
        "bmbbbwgmsgwsmswmwsbswmsbwsmgwmmsmmsswwwmgbwwmbmgwswgbwbSmmgswgwswmggwbwbmgbmmgggsgbmbbsgssswgmbsmsmmbgsgbsbmwbswbwgbmwgwsggwmwgmgbgbgmswmsmwwbggbssssbbsmggssgbswbmmgbbbsggmwmswwwbg",
        "mwmgmswwgbbsmssmggwwbwwwgwsswgwgmswmbbwbggsmmsbmgggbsmbwwmgwssswgswmmbsmggbgbwwbbmmmwwmmbmswgmsmgmbwbbwsbggbgmmsssggbgmgswwmgmmgswsbbsswSgwbgwssbsbwbsmmsbbmbbggbbmgmwsmswgwwggbssbs",
        "bmgwwswmwbwsmbmwbgbgmgbwwmbgwgwmsmsmmwgsgsbwSmgmsmgmwwgbwbbggwsbgswbbwbwgsgwgmmmbbsmwgwwsgmmgbmbgwbbgwwmsgsbsbsmmsbgwbgbbsmgsggswmswwggsbwgbbmsmsbssgsbssmgmbmmsgmwsmmwgsssswbgwmbws",
        "bwbsmwswwmmbmwmsgmmgggwgmwsgsmmsmmmswgsmsbgsbgbsbsmmsgswgmmswsmsbwgwgggwwwgswbwgbmbwmwwsbwggwsgbsmsbbmsbbsbsbwwmmwbwwggmgbsbbgmwmmwgwsbbswsbbwbbmsmsmSbsggmgmwwgsgmwgggbbmwsbssbgggb",
        "wwgmwbgsbsssmssggmgwwsmmbbgggbwsbsbmwbwssmsbbsmwwmwwbmsmmbwsswwgwsgswbgmmsmgmwwgbggmbbwbgswsbsswmgsbmgggwmsggmbmbbgwwbmmgbggmswSsbwsmmsbwggbwsgmbbsmgmmbwsgbgbwmssgwbsmsgwwgwbgmbmmw",
        "wmsgbmmmbwmmbwgssbbgswwgmsmbwgssbgsbgbmsswbswmmgmsbmwsgwsmgmbmwwgwggbbsgwwwsmgwwgggmgsgbgwbbbmbswssmmwbsgwggsgmssmgsgmwgmmwbbwggbmbsbSmmbbswgbgmwsswsgwmgbwwbwwssmsmgbwswbbwgbmbmssm",
        "bgwbwmSmwsmmwswbmmmbgbbmwsgwwwbsmbmmsbsmmbgbbsgmsbgwggsbgbmmbwmbgwgwbmbwbbmwggggwsbgwmgsbsssswmsgwmmsmbwwmwgmbgwsgswbswbswssmsgbbwswggswbbsggmbbmsmwwgggmggsmbbswsggmmgswggssmmwwwss",
        "gwgbsbgbbgsbmmbsmsgmwsswggwmwsssbggmmwbwmgsgbsmmbbwgwbgwggsgmmmwwsswbbsmmbgmbggsgmsswwwwwggsgwmbbmgmbwwbwwmsbmsbbgssbgswmmsmbgmmbwSsgmmwswwgwbmbbbmsbgbmmgwbmwwggswgsgssbmsbwswssmwg",
        "bswgswwssbmbmmsgwsbsmgbsgswbggwgwbmgbgbgswwwbgmbbwmmsbwbwgsmwswbmmwgggmssgbmgmbmswmgmggSmwswgmgbmwmwggwgmsbbbmbwwbssbwgbssgswgbwwssgbgbgbbswwgbgsmwgsmmmsmmmwmwwssbwbwsmsmbbsmmgmssg",
        "wbswmwmgbwbbwmwbgmgwbwwswwwgwggwbmwmbgssbsgggmsgswbmSmsmbsmmwsgssbgsgmwmwsgsgmbssggsgsswgsgsbmbbbbswwgmbmwggsbwsmmbmswgbwsgwgbwbsbmbbwgwbmggwmmsbmsmbbmgsgsmmwbbmmgmgwswwmgwssmmbbsg",
        "ggwsmgwgbmsbmgsswsgswwbgsssbssgmwmsmwwmmgmgwwggSsgsbgwsmsbwmmbmmmgbwswsgbbwbwbbswgmmmbgbgsmmmbssgbwsbwggsbbbbmssggbmbsbbmssmbgsmwmwmgwwwgwwswgbmbgggsgbmgmwmwwwmgbsbwsbgwbmwmwbwgsms",
        "wgsbmswbgssggbsbgmgwgbsbgwmmwswbSmsmggmbbbmbgbgmbbswswsmbgwwwsmsbwmbwgwsmswsbsmmbmgmgbbmgmmwgsmmmwsmwwgmssmmbgbmwbwgsmswswbbwsggwbbsmbbgggwwgmggsbgwbsggwgssmmbswssmbwggbwmswmgwssww",
        "smsbbsgbwbbsggsbwmmswgmmmgwbwmwmgswswggggssgsgsmbmwsbwbwmmggbbgwgwmmwggwgsgbmwswgsbbmbbggmgwbswbsmmSwswmbsmbswmwssmwwwgwsswbmgbgmswgssgmssbsmmwsbgswbmwsmmgmmgwgbmwmgbgwgsbbmsbbwbbb",
        "bmbmsgwwsgswmbggbsgbssbgmmwmgwbgmbwgmsgwmsswsbgwwsmbmmmmmgsmbwbwmsggmwmbbmbmgsgbgbbwmmmswbbgbbmbgmwsggsmmswbbwgswbSwwgbbgswbbwmssgwwwmswmgwbmsmsggmgbwgsbsgbgwssmwgwsggsbwsswgswwssm",
        "wwSbmgwsgmwmbbmbwssssgmsbbbsgwbmgwmsgsgswwgggbwbgwbbmmswwbswbwmmbswsgsgmmssbgwmgwbswwggwbgmgbswmbbbsbmsbwwsmmbsmbsggwsbbmsmwbmmgwgswmbwswwgwssgmwmsgmgbbggmsmggwgbgmmgswgssmbgwbmmsm",
        "wbwwswgmbbsbmssgmgmwsgggbwmsbgmgwggsbgbwwsmgbwswwsbsbwwsgbgsmgwmsbwbbmSsmmggswmsgmbmbwmbbmwwgwmssbmbwbsggmgwbmmsgwgwgbsssbbbsgggggwsmmsggmmbsmsmswwgsswwbmmgssbbwmwgswmmmwbmbsgbwwmb",
        "mmwsswsgmwgsmbbwwwwwggmsssgmmwmbmswswbmssmgwbwbbwwgbswmssbsgbssmggbsSmmbgmgmsbgmsbmsmgssbwsgggsmwmbbmbgbwbggmssswgwgmgbmgbwsmbwsbgggmbgmggbsswgwwbsbbbgswgwmmwmmwwsbbwbbwgggsmbwwmmw",
        "gwwsswgmgsmmmmsmggbwmmsbbwmbgbwwggmsswwwmbwbbggmggbmgsmgwbssbsmsbwbwsbgggwwwwswmgbgbmbsbwbbgmbgmgbmwsbsswsgsgswswgwbwbssmmsmwggbgmmbmswmgwmmgsgbsmwbsggmswmsbbggwbbgSsmwbmbssmwwmwss"
    };

    private static final String[] levels20x16 = {
        "bwwwsbgsmmgmsgsbwwmwmwmmbmgmmwmsmwwwbwgbbwggwbbssggmgbmwmgbgswwsmgbwssgwsbbggwbswbggwgswswgswsgwwwbbgmbbwwmgwmbwmwwwwbsgsbwwgbgmggbgssgsbsmmmwgmsssmwswbmmbmbmsbsgggswbsmsgwmmmmggsbgmbwgbggsswmggbbbwsssswsgwssgsbbsssbggssbbbgssgmbwbmggwbsgbmwwmbmbsbmsmsmsmmbgwwmwsmmsbggmwbsbwmbbsmmgwwsbwbwssmsbgggmwmgwbmbgmmmgmgSwgbbsmm",
        "mgsswbgbwsmwwgmwmwwbwssmsmmgsbmwgwwggwgmwgbbbbmgwsmwssgswggbwmgwwbmsgssgmswmgbsgwmgsbmwsmmsgwsmwmgwbgssbwwbbbbwggsmwwmsbwsbswmmsmmbgsbsssbmwgmwmbmsgmmsbgwmgwmbwmgggbsgswbbwsggsmswwsggSssbsbbsmswmbwgmmbmwbggbgsgbmgsswwbwmsbwmbswbsmmbsmmbbgbbgbgssmmsgbwwwbswswsmgwggbmmbmmgbsbwwbmmwswgbwbsgbgmmmwwgbggggwgbmbsbsgsbbmmgwggs",
        "smmsmmmbwbsgmgwmbmwbbbgmmbgbgbwbbgbgwgwswbmsmgwssmmmwmmsmggsgswgbmwgmbsbswgmbsssbwmggggwmbwsgswsmsgmggsggsswwbwwmsmmmmwsswmwggswmwbsgbbmbswmsbgsgwsbbwgwmmbbbmbswgsbmwbwmmsmmbwbmbbbgmswwsgsmssmsssmbbgsbwbgbbggbsbgwmwwmwmwmgsmbbwsgsswmgsSgswmswwbsmsmbbssgwgggbwwsbbgwggwbmwgwggwgggsgmsmbgwmgwwsgwbwbgmswgwmgbswwbbgsssmmwbb",
        "gmmswwsbbsmwgwssgmgmgsmbmsgbgswwbbmwsmbbbwbgsgsmsmmbmsbwsbsgbswbmgggggsmbwbgbsbsgbbbwmmmwwmmsswbwswmmwbggsgggmbwmmmbsggwwwgbbswbmwmmwmmggbwgmwggbsgwsmwbmsgwgwgwwwgsbSwwbsgbswmbwsbwgmbmwbwmgmswgsggsmwbgwmbgsmbgbmgbmsbsmbbgwgwgmwmwgwsbgmmbswmssgsbbsmbssssbmmmmbwgssggssmsmbwwgwbssgbwbwbmgwwgsbgmgswwbwwgmsbsbggwsswgmsmssmm",
        "bgsbbmmssbwmgbwggwgwbsmsswmswmgbwmbsbswbgmgbsbgbmmwgswmwmwsbwbwmwbsbmsbwbbsmgwbgwwgbmwmgbmsbgwmgsmswbsmswswgmgbwmmsbsgwgbwmwswwmsmwbbmbmwgmwmmwsmgwmmsbggwbwswmggmgwmmgmmgbbsbbssmbbggsmmggwmggmggmsgsSgmbswwggwsswbmbsssmsmmbgbwbggbsbsgsbmgmbwgbbsswsgmwgbgwbwmgsswsgmmmmgwwswwswgmwbgbsgwwggbbwbsgbbsmgwwsmgwsswsbbggmsssssgb",
        "sSbsmbsmsbgbsgwsmmgwmwwgwbgbsbswmmbmmsmgmsbbbbwsbbmwsgwmsmmbgsbsmsmbmgswwgwsgmwbmswgmwmbwggmsbgmsbgswswsswbggsssmmwbwwbgsgwsbwgsgsgwgmwgsswwgmmbsgmwwwswswssgmbggsbbmgggmbbswwmsmbbbwmgmmswbbbwsmwgwwbgwgwgggsmgwwgmmbswgggssmbsmbwbbmbbgwbswmsgmssbwswwmgmssgwbbwwmmgbmsgbgsmbgwwbmmbggmbsbbwgbmsgwwmmbbmsgmwgbwgggsgwswmbbgsmm",
        "gsgbgmwwbbsmmwsbsmmwgsbsggwbgmbmswwsgbsbsgsmwmggsbbbwsbwgmwmwsmmmswbwswbgswsmwbsbmgsgswbwmgwbbsgmmbbggbgbgwgsSmwgmbbwbwmwmbgwwssswbwsmgmwgsgwwwbmgbmwmbsmgmbbswmmwwgggmmbsggwsmbbsmbbbmsgwsmwsgwwmsgwbwwmwgwbsbmgbsbsggwmmbgsgwggbmwsswbwsbggbbbmssmwmmswmgggmgmbgbbbbssgwmsbbgwgsmwssmmwbmsssggsmmggwwwsmwmgmssmbmsmswbbsgggsgw",
        "bgggbgmwwmwswgbmsmsmmggsbmmwsbmsmwgmsmmswssbmgbbbgmgbgwsbbmbmwsbmgwbsbwgmbgbwggwwgwswsSgsmgwgmmssgsgwsbbggsgbmbgsmssgwswgssgwmmwmmwbgwsmwsmgggmmbgbgwsmsmgsgswswmwwbmmsbsgwgswwbgwmswmssbwbbmbwgbmmbbgmwwgwbbbggbgmswmmmmmgbsbbwmsmbmwbsgggssbswwbmgbbgbwwsgbwgbmgbsmgbbbwggswbwwwswsmsmwwsbbwmsmggwmmwwwsbmbsmsmbgbwssgbwwsgsss",
        "gmmgmmbsmbmbsbbbgwgmgbmggbgmwgbwbbsmsbbswgwbbbmwmgmggbggmmmsgmggbbbbmswggmbggggssswswwssgwggsgmmwmsgbbsmgbgmmbswbswsswwssmwsgbbssmwmsmbmmgwgwwbmgwmwsmmmbmmwbwwswbbwsbbmgwbgwmbwgwwgwsssswmmbmsbwbgssbmswggwgsmbsbwwsbswmssgmsgbsmmsmmgwssmmwswgwmssgwwbmggsbwmwmssbbwggswgwbggsswbwbwbmssmggbwsbwbwmmwbwmbgsbmwbgmwggssggwSwsbg",
        "gwgmbsbbmsbwgwswgggwmswbwggwbbmggmgbsmmmsmmgsmmsmbswbgmgmswsmswwswswggbswgbwgsmmmwwmmgsgbmbbmsgwmbsgmggmmsbsbgwbsgggmwgmmbmgmgbwwbggsmbgwgsmwbgsgwwbssswwmbmwsmmggbggwbsmmgwwsgggbgmsbgwwbbgwbwmsgwmmmsgsswmwbgwbbbmmsbmbwmswwwbbswbmwmmwwmsgswgsswmsbgsswsgsbbbwgsmgmwwmbgsbssbmbSgmmwwggmwmsbssbbssbwbmbbsbbssswsbwsbgwswwgbgb",
        "mwbmgsbwmwswbgmswbwbgwbgbsmbwbssgbsbwsmmwsswbwbgsbwgssmmswmwbsmsggsbmwwgbgswggmmwgbmbsgbmgwmmssggsggmswgmwwbgbmgbwsswggbssbwwmbwbwmssbgwmbSswgggmbwwgggwgbbswbswbbbmbmbbwbmbmwbsbmsgwwsbmsmwgswwgggwmbwswsgmmbsmsgwmgsgmggwgmbgmbwwssssbssmmggbgmgsmwgbbswbgbsmmwsgmsgmwwsgbbmmwwwggmmsggbgmsbgmgsmwmmbwswbmbsmmmsswsmwsgsbbmwgm",
        "sggbssmswwmswsbsgwgmgsgwswsbsbmmmgsmsggggmsbbmbwwgswmwsbmwmggwwgmmbsgwbgsmmgsssswwmsggsbbwsgbwwggwbsmssgsggwwmbbwwsbmbsbwswmgwsmbbbbsssbmbgmwmsgbwwgbmmbgwgmmbbbgmbbwgmggbwmsgwmgbggsswmwsgsbgmmmmmbwggsgswmwgbgwwgmmmmmbmswmwswggsgsmbbbmmwwgsswbmgwwbgwsgsswgmbmmbbwwbmbswsbbmgbgbwwgssSbwswbbmbsgsssswmbwgmgmmwwbsmbgmbmbwbgw",
        "mwwbbmwbmmggswmmggsgmgwsgbgggbwmbwwmbgswgmwmwggwwmwsmswssmbmwssbgbggwsmgggsmsbsggmmmgbsbwsbwsbsmbwbgbbbggwbgswsgwgmswmmbsbwgssgsbgbbsgsswwwwbssbbwbgwgsgmbbmgswmgwsgsggssswwgmbwwgwbgsgmgmsssbgwmgbsbmsgmggmgmswmsggmbsbswsgmsSswmmssmwwwbmwbsmbmsmbbbsbbwbbbbmmmsbwgwwmgwmbmswmbssmmggmmwmgbbmwsmwgbbsmbgwwmmwsbwgwbbsmmwbgswww",
        "wswwbgbwbggmbmbsbgsggmwbmbgsbgwmsmswgmmswbmwsmsmgmwwmbsbwsbbsssggwwmbgwmsggwbmssgwmbgwbwbswsmssggssmmbbssmsssmsbwgbggsgggwwmwbswgbmbmwmsgwwbbgsbsswmmgmbmggbmbmmbmwsbbsgwssbmssggbbwwbbswgwbmgbsmgggmbgbwsgwbmswmmssbmwsbgwssmgmbgwgmwgggmbgmmmmwgwmmwbsbsmswwwbsssmSmwggbbmbwggwggsbwsgbwggmsgsgwwwwmwbgmmmssgbwwwswbmbbmwgmwbs",
        "mwmmwbgsbgwbsbggmwbwssswsgsmgmgmgsbbmbgbggbbwgbmswggggggbbswbmwssggwgsssgswwwbbsmmbmwbbsmwbsbgmbmbbsbmbbggmgbggmbmbmwsmwsmbwbbmbwmbsbwSmbwwmsbmgsmmbwswgsssssswmbmbsgbmmgsgbsbwsmgbwbwwmbggswmwgwbwgmsggwmbgssggwbmgwwbbswmmsbgbsmmwsmmsgwgmmsbmwmswwwgsbssgbbbgmsggbwgswgmgswmmwggsgggmwssgwsmmbmgwsgswwwwmmwwwsmmwgwwswsswbsmm",
        "bmbsmmsmggggbwbsgwswbmsgsgmwgsmmsbbswgwsmwsmgbmsswgmsbsgwgwsmsgsgggwbmsgwswmmwbwsbmmgmwgggmbwgssmsgsmmsmsmbswbswwwsmgswwsgssmsmwgbsgbbmgwmbbbbgsbmgwggSgwgwmwmbmgsbgbgbbmgswwswmgwmgwsgmwwmbwbbmswgbsmgbgbgmwgssgmmbwsmbgsmsgmbwbsmbwwwbwwwbwbswbgmbgsbbsssggwsggwsswsggwwmwwbbwbmbbswbwwbmbmmmmbgmmmbbsswgbbbbmswbwmsgggwmbsmmg",
        "sbmwsgwbmbwgmbsmggswwmbgssgbgbgmbmwggbmwmwgwwwbwwbmmmmssgswbgwbbsssbbwbsbmwsswgwssmmSmgwssgsgmbwbwgswwmsbmbbbwbggwgbmggmwmgsbmmsbswwbwswbgmswbmwmbggwwssmgmmbbbmsmwbgsmgbgsbsbsmmsmsbmmwbmggbbmsmsgbsmgbswmwsgwgwmbmswgwwssgbwbsmgwwswbmgmggswwswmbbssgggwwwgwssggsggwmbbgggmmggwmgsmwwbsggmgmgssbgssbmmmmswsbwbmbmgbsgsbwmwbbsw",
        "bbmmgbssmgmmggbswmwmmbgmgbgsbsmgmmsmbbsmswbsgbwgmmswssgbmgsbwsgsbmgsgbgbwwgbmbssswwbbwmsswmmswwssbggwbmmbbmmbmwmwmsmgmmmsggbmwwmbgwwgmwgmbbwwsbbgbbwbmgssmmgssgmwgbswgmgbbsgmwsbggsswmbwgsgwmmwwwsSsgggsmwmgsbggggsbwbmsgwwwssmggwbssbggwmswbwgmwbbgmbgbbsggbbmgswsbwmwbsmwmmswwwssmsmgbswswwgmwgwgmwmwbwbswsgmbggbwbswgswsswbbb",
        "swgmbwbwssgbsgmmgwgmwgwsbmwwggmgsbbsbgmsmgbsbwsmwbbgwgswbsbsmsmbwbwbbgbmmgwgsgbwwgwsmgsmgsmmgwgsgmbsbwmwmgmbwgmwsmbwsmsmgssggggbswmsbbbmmbmbssggmsbmbmbbswggswgSwbssmwgmwsbmmmwsgmwmswsmbbbsmwbmbssgbbwmwmgwsmmmbsmsmgbsgwbmggggwbbwwgsbsmgwmwbbgmsgsbswwgwbwbsmsbswgggmwswgwswbggwwbwwsmgwmbsmmggwsswggmgbmssmssswwwwgbbwmgbmbb",
        "bbwmgbggggsggggbmbbwswsgsbmmwgwbwmbwwwmwgmbgwsmmswsgwmbsswbwgsssswmwwmmgbsgsggsmbbmbsmbmbsswsggggwmgwsssmbbbwgbmgwmbmwgbwmgwbwgmmggssggbmmmwwwggsgsbwwwwwggsbbgwsbmbmwsmsbmsswwmgwssmgssmgmsmwswmwmbbsmwbgwggsmmmsbwmssgbgsbbwssbbmbbggwmmgmwwmbmbmwsbbmwbmsbbbmmwsmwssbbbmbbbgmmsbgsgsgmsbgsgwmbbgmwwwwbsmsgwgwsggwbmSsgwwssggs",
        "gwggwmsmbswmgwwbbbwwsgmswsbmgmwmsggwgwwsgbsmswsgswgbsbssbsgggmbgSgsgmmgswsgwgbbbgbmwsgmmbgwswbsbbsswgsmmswwwgbwbgwmbsgsgmwmswbbmgwmswssmswbgwsbbbwbbgbssbgmmgwwswgmmmsgwsbggmwgsmswbgmmmsmmgwmgmwmswgsmwbmbmmsgwsggwwgmbwwmwmmbbbbssmbwmsgbbwmbbwwggbbggbbmsmmwwgsmggbgsbmbbbmswbgswwmbsgmgbbbmsmbsswsmbsbwwsmmggbsgwmsmwmwwssgg",
        "smwsbwggmsggwgwbwssbsbmgsmgbSsbwbwwggswsbsggmgbsgwmgggbbbswmgmmwgwgwgwwmgwsbwmgmwsmgbswmbsbwmwggmmbsmmbmbwbwsgwsbsgbwmmbgwwwbswbbmgswmggwmsssgswggsbwsbbbgsswmmgsgsmbmmgwswmwsmgmwwsmbbwmbbbbgswmgbgwmwsswbbwmwgsbwggggbbbmbsbsbsggsbgsmmsbmgmmmsmswsbbgsmmmmgmmmswbsgmwmwsmgsbsgwwssmbwsmwmmggbbbbwmssmswsbggwbwmwgsbwmwgwbggbs",
        "bmsmbwwbwbbsbgbsmsbsmmbwsmmgmbgggwgwsbmssgbbbsgbwgwswswwmbmbgmbwmssmbssmwmmbgmbbggwwsswwggsggmwbgbgmwsmswmmbbwbmgmsgwbsggbwmggwbmgwwbswmmwmbswmmbsgbwgmsgswwbggmsgbwsbgbgbgsmmmbbmgssgmsswgbsgmwsswbbbwbmwwwwgbgwwgwmmwmbgwswsgsgbmsmsgwwssbgbbmbggwmwSmgssbswssgmwwwssmggggsgsgsmggmbmwbwwsbgmbwmwmsmsgbmmwmbsbbwwsbmmgmswsgssg",
        "gmmbsbgbwbgmsbgwbsbbgbmwsgmwgssbsggmbmwsbsgmwssgbsbmbsbwwggwsmwbbssswwmwgmbssbmswmbswmwwmwwswsbmmwgwgswwsbmgggbgmbgmsmbggwbbwwmsgmswmbsmgbswwbggSgggggmwmbmswsbbwwswmbbwggwbmmmbssmmgmsmwsgwwsgsggbgwmwbsgmsbmssgwbbbwwmmwmgsswgmmswgssmbbmgsmgsssswbmssgwmwggmbbwsgwbwmwgsbgbwmgwsbgbswbmmmgmmssmbwbbgmgmgbbggbwmbgwmbmsgwwsswg",
        "mmgbsgwsbmsgwsgbwsggswwbsswwmsssgmmgwsgmsgmgmbggwsbwwmbssgbwbbbmwgmbswsbmmmmgsgsbbwsgmwswmmwwbbwmbgwswwbsbswwsswmswgsgSmgwbmsmbwbmggbmssswbwwbmswwsbwgmgmgbmgwgssgmbsmmbmbbgwsgsmmgbmgwgmsbbmwmsgwbgwmbmswwgsmbmwswbbsgwwmbgbbgbgwsmmmsmsmsmmgswgswwmgswbbbwbbssswgwbggsbwmmsmbgwgwgbmsggggswsmwbgbmggbgbwbgmwmssgsgggbmbbwmwbbm",
        "gbbwwmbmbwwsmbwbwbmwSggmbwgsgwwbswwgmsgbggsssbwmbswwswbmbbbmsggwmwgsmwbgsmbswgbmsbsgssgssmgmswmwmbgsmggbmmbwmmmbwmmmssbswwsswsswggbmbmbwwgbggwmgmgssswsmsbbgssmsssgwswmmmbwswmgmbggsmsggmsmbwssgbwgwwbbgwbgmssgwwbbswmgbmwswsmbsgwwbsgbmwggmwwgwmsgsswbbwgggmwgbwgmwbgmgmmbwmmbbmwswbmgbmsbbsbgbbbbmwgbsgggsswmmwgmbgsmmsmgbgwgs",
        "bbbbmwbgmgssbwbbbwwwgsgsgbbsbmssbgmbwsmbmbbbgwbmbgbwsggsgwswsggswwsbmmgsmmwwmwmswmwbswsmssggwbbmwgwmgggmsmssgmbswmmwsgbbwbbbsmbwwsggwgwwbsgsmwbsmgwwwmbsbssgbmwgbmgsswmwbmbmsmssbgbmmwgbgwwwwmmgggwwmbbbbsswmSgswmsgsmgmwmbwmwbbmbswbwgwggggmwggwggmmgsswwgsbwsggmbgmwwmsggswssgbmmswsgbmsswgmmbsgbsgsbsgbbmmgsssbmsgwbgwmmwmmmm",
        "bmbbswbbbbbmwwbbggbbmmbmgmbswwmwwgsswgbswwmbmmbwwbmmsbgmbgmsgmsbgmmggbbmgbgwmgsssgmbbsbwsssmbgwmmswmsggwbgsswgwgbsbssggsmbmbmbbwsmmwmsmggwbsbsssbsgbbwbggbwwgwwssggsmgwmsmsbmwwbwmwwwswswbwbbmmswmsbmbmmgsmwwgsgbsgmbmwwgwbwswmwsswbgwgsgswgsgmmwwsbgwmmsbsgmbgmmsssmbswsmsggssswggSwggwsgmggbgwbgmmbwgwsgwgwwgsmmsbgbgmmbgwgmwm",
        "gwmmmbsbbmswwwsssmbsggbmgbgbgmgbgmbgssmwsbsbbwgsgswbswbssmbmbgbswswmwgswsmsgbwmwmbwbwwbgsbmsbmmsbwbwggmsgwgwmmmmbgbggmgmmwmgbwgsbwsgbwsswgswbsbswgggswsmwbmwgSswmgsbmgbwggmwmsgsmmgmgwmbgsmgbbmbssmwmbbgbmwwssmbggmmgbbgwmbswwwgsmgwwwswbmwsmgbwwbbmwmwgssswwsgsgbmmwsmssbmsgsgmmbgsmgbwbwmsmwbgbgbwswgsggbbgggwwsmgmwwwmssbbbms",
        "gsbmgwwmmmbswwmmmmgbgbwmswmgmmmmwwgswwbgbgmmbbmbsbwbswbsgsmwbswmgbbswmswmsbggwbgwsmbsmwbsgbwmbwwgwmgsbwsbgsmbsggbssgwggbmsmmmbgsssbsbwwmssbswgwbmbgmmsbgwbgsSwgwwbmsmbgbswwswggmmmgsswmbmbswgmgsgbgmbmbgsmbbbbwsbgsmsmsggggggwbwssmbswgswsgggwgwgbwwbggmmmwbwwbwgsbwbwwggswsbwmsmmmmggwsbgswwmgwmsmsgwsmsgsbwbmwmssssbbmwgbsbggm",
        "ssgwgmbbssmswbmsmwgswmbsgmmwbgwwwgswmwbbgwmwwsggwwsbwbgbmssbbbbbmgggsgggsgwwssmmbmgmwgbswgwmmwsbmwmsmwgssmggsmbbwwsmbsgwsgwsbgmggmbmwggwsgswmgswgggbSmmbssmsgwmsmgbmwswbwbmmsmmwwmmbsssbgwbgmbwsbswwbmwgbmssmmbsbswbmbsswbwgggsgsmggwbsgwbgbwsmbbwwbmmgwgssmbgwgsmwsbggbbbgwbbwwwbbmgsgsgmmmmwsmsmgwmbmwgbwbgmmbwgsbbbwsmmsgbgss",
        "mmswbsmgsmwsbbsgsgsmmbsmmbwsswsgwggsmsmgmbsmbbbwbwwsgswwwgswssmwmwgbmsbmgbwwbgmbmbsggmmgmmbwwwgmgmwwgsggbsssgsmbbwbwSgmswgsmsmbmgsggggmbswwmgwgsggwbswbbsgswsgmsswmbggsbbwggbwbsmwgmggssgbmmmbbmmwbgmbbggmmmbwwgswgbwwwwwwmmbbbwgsmmggbmwbbbgmmmwmsbswwsmggbswbssbbsmssgmbgwwwgmmwsssgmwbbsmsmwgbsggwgmbwwbwwbsswggsmbwbsgbwbmbs",
        "sswbwswssbssSgbwsgmbmsbgmgwbmgggmsgsgmmwmmsbsgmsbmwgbsmbbswwmmssbwgwwbbsgsmbbgmwwmwwgggwbggwggwmsmwswmbbsswwbgbmggbsbmssgmmmmmsgwggswsbwgbmmggwmmbggbwwwwgwbbsggswgmbssswwgmssgbggbbbbsggwwssgggsmswsbbgbbbswmbsbwsmgmbwbggsmwsggbwbgwsmgbgmwsssmssmmwsmbgmbggbmmmwwmgmwmbswwbwbbwwgsgwbsgwwwswbsmmswwbwwmbbmmmbmbsgsmgbwmmbsmmm",
        "mwgsgwgmbmsgmmswsmgwswwwsbbwwmbbwswggmbswbbmbgwmbbswggmgsggwwsmmwgsssgwgsgbwgsgmmgmbbbmmbbgwmbwgsbgwbmssswwbbwmgsgwmmwwmwsgsmbmgsgwgbwbbgwbsmgsgmwmbggwmgbmwwmwwbmbwwmbwmsgwsgsssbsmbwggwsmswwgbmmsbbbgwsbmbwmsbmbmgbgbmggbwmgwgwsgsmbwgsbmsbmwsgmwgbbwgsbbsmbmwsmwmsswsswbgmsgmmbmgssmsggssgbgggsmsmsssbbbswbmgwSgmwwwsgbmsbbms",
        "mmwsbggbgwgbsmsbwmgwmwsbmmbbwgwssgbwwmggbbmbbmbbbwsswmbmswgmbwmmmmbmbgsggmwbwswmwbwmbwbwsggwsbgssmsgggbwbsbgbbgssswwmgwggsgsmbmwgmgggmgmswwmmmgwwmmmwssgswgbsgsmmwbmbswmsgswwmbmbmmbmssmbmsmswgsssmwwgwbgmswggmwswswwgwgwwgbbmsmsssmsmssmgsbbsbbgbggggwbggswwggsswwbwswmgssbbbsbswbwbbbgwwgwmgbmbsSmggwgmgwsbmbbgswsmgbmgbsbsmwm",
        "wwbmwmmbgsmmsbsbgbbbssmbwmbgggssbbgmssgssbgbgmsbgbmmmbmbbmgwswwgwbmmwgbmsmwbgmmwssmsgwgsmmmwwmsmsSsmwmbsgsbmsbggsmsgmgmmgbbbmmgwbsmwbmmmsbsmmgbgwwssbwwwwbmmsgmswwbsggwsgbgwggssbgmgwmmwbgsbmgsgwswmmbmbmwwssmbmggbgswbbmwswgbwggbmbssbggwsgwsgsswggwbwmsgggwswbmwwgwsmbmbmwbssmggbsbwgswswwbwwwbggbsbbwgwsggbswswbwwwmwbsmgwgsg",
        "gmwmbgmgmsmbbmwggmsgbmmwgwswgwbwbsbbbwggwbggmbssbmbsmwbwsswgsgswbsmbmwwbwmsbmwbgmwbgwwwmswsmgbsbwmsbwswmsgbwmgbgggmbbmwwsmbgwwmmwsbmsmmssswbgbswsgbwwmbwwbmSgsgmmgwgswgsmsmbwmmbwmbggsbmbwgsmbbwmssmwwgmssgmgbsbmgmsgbgbggmwgbwwggssggsgwssmwsggwwbbbgbbggmsmwmwssmssmsbmmwmbbgsbgwsmbsmbsswgggbgwsggwgwmwbbgwbmmgswbmsgwmsssbss"
   };
    private static final int[] gameSizeX    = {  6, 10, 12, 15, 20 };
    private static final int[] gameSizeY    = {  5, 8, 10, 12, 16 };
    private static final String[][] gameTable = {  levels6x5, levels10x8, levels12x10, levels15x12, levels20x16 };
    private static final int numDificultyLevels = gameTable.length;
    private int[][] gameBoards;
    private java.util.Random rnd = new java.util.Random();
    
    public inertiaLevels()
    {
        // Pre-initialize boards to avoid garbage collection.
        gameBoards = new int[numDificultyLevels][];
        for(int i=0;i<numDificultyLevels;i++)
        {
            gameBoards[i] = new int[gameSizeX[i]*gameSizeY[i]];
        }
    }
    
    public int getNumDificultyLevels()
    {
        return numDificultyLevels;
    }
    
    public int getNumGames(int dificultyLevel)
    {
        if( dificultyLevel < 0 || dificultyLevel >= numDificultyLevels )
            return 0;
        return gameTable[dificultyLevel].length;
    }

    // Tile values are:
    public static final int tileBack   = 0; // 'b'
    public static final int tileWall   = 1; // 'w'
    public static final int tileStop   = 2; // 's'
    public static final int tileGem    = 3; // 'g'
    public static final int tileMine   = 4; // 'm'
    
    private int playerStartX = 0, playerStartY = 0, numGems = 0;

    public int[] getNewBoard(int dificultyLevel, int gameNumber)
    {
        if( dificultyLevel < 0 || dificultyLevel >= numDificultyLevels )
            return null;
        if( gameNumber < 0 || gameNumber >= gameTable[dificultyLevel].length )
            return null;
        String s = gameTable[dificultyLevel][gameNumber]; 
        if( s.length() != gameBoards[dificultyLevel].length )
            return null; // Internal error - should not happen
        
        // Prepare new game
        numGems = 0;
        for(int i=0;i<s.length();i++)
        {
            switch( s.charAt(i) )
            {
                case 'b':
                    gameBoards[dificultyLevel][i] = tileBack;
                    break;
                case 'w':
                    gameBoards[dificultyLevel][i] = tileWall;
                    break;
                case 's':
                    gameBoards[dificultyLevel][i] = tileStop;
                    break;
                case 'g':
                    gameBoards[dificultyLevel][i] = tileGem;
                    numGems ++;
                    break;
                case 'm':
                    gameBoards[dificultyLevel][i] = tileMine;
                    break;
                case 'S':
                    gameBoards[dificultyLevel][i] = tileStop;
                    playerStartX = i % gameSizeX[dificultyLevel];
                    playerStartY = i / gameSizeX[dificultyLevel];
                    break;
            }
        }
        return gameBoards[dificultyLevel];
    }

    public int getPlayerStartX()
    {
        return playerStartX;
    }
    
    public int getPlayerStartY()
    {
        return playerStartY;
    }
    
    public int getNumGems()
    {
        return numGems;
    }
    public int getBoardSizeX(int dificultyLevel)
    {
        return gameSizeX[dificultyLevel];
    }
    
    public int getBoardSizeY(int dificultyLevel)
    {
        return gameSizeY[dificultyLevel];
    }
    
    // Returns a random board.
    public int[] getNewBoard(int dificultyLevel)
    {
        int n = getNumGames(dificultyLevel);
        if( n < 1 )
            return null;
        int i = rnd.nextInt(), j = 2147483647/n;
        if( i < 0 )
            i = -i;
        return getNewBoard(dificultyLevel,i/j);
    }
    
}
