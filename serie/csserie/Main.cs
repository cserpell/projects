// CRISTIÁN SERPELL 2004
using System;

// Nodo para la lista enlazada de la serie
class Nodo
{
    public string valor;
    public Nodo siguiente;
    public Nodo(string nuevo_valor, Nodo siguiente_nodo)
    {
        valor = nuevo_valor;
        siguiente = siguiente_nodo;
    }
}

class Serie
{
    private Nodo primero;
    private Nodo ultimo;
    private int calculados;
    public Serie(string nueva_semilla)
    {
        primero = new Nodo(nueva_semilla, null);
        ultimo = primero;
        calculados = 1;
    }
    public void calcular_mas_terminos(int cantidad)
    {   // Si cantidad <= 0 no hace nada
        for(int i = 0; i < cantidad; i++)
        {
            Nodo nuevo = new Nodo(numero_descriptor(ultimo.valor), null);
            ultimo.siguiente = nuevo;
            ultimo = nuevo;
            calculados++;
        }
    }
    public int comparar_delante(int termino1, int termino2)
    {
    	Nodo n1 = primero;
        Nodo n2 = primero;
        int tmax = Math.Max(termino1, termino2);
        int num = 0;
        int largomin = 0;

        calcular_mas_terminos(tmax - calculados + 1);
		for(int i = 0; i < tmax; i++)
        {
            if(termino1 > i)
            	n1 = n1.siguiente;
            if(termino2 > i)
            	n2 = n2.siguiente;
        }
        largomin = (tmax == termino1)?n2.valor.Length:n1.valor.Length;
        for(int j = 0; j < largomin; j++)
        {
        	if(n1.valor[j] != n2.valor[j])
        		break;
        	num++;
        }
        return num;
    }
    public int comparar_detras(int termino1, int termino2)
    {
    	Nodo n1 = primero;
        Nodo n2 = primero;
        int tmax = Math.Max(termino1, termino2);
        int num = 0;
        int largomin = 0;

        calcular_mas_terminos(tmax - calculados + 1);
		for(int i = 0; i < tmax; i++)
        {
            if(termino1 > i)
            	n1 = n1.siguiente;
            if(termino2 > i)
            	n2 = n2.siguiente;
        }
        largomin = (tmax == termino1)?n2.valor.Length:n1.valor.Length;
        for(int j = 1; j <= largomin; j++)
        {
        	if(n1.valor[n1.valor.Length - j] != n2.valor[n2.valor.Length - j])
        		break;
        	num++;
        }
        return num;
    }
    public string termino_especifico(int numero)
    {
        Nodo lectura = primero;
        // Recordar que no calcula si ya están calculados
        calcular_mas_terminos(numero - calculados + 1);
        for(int i = 0; i < numero; i++)
            lectura = lectura.siguiente;
        return lectura.valor;
    }
    public static string numero_descriptor(string numero)
    {
        string descriptor = "";
        int largo = numero.Length;
                                                                                 
        for(int i = 0; i < largo; )
        {
            char c = numero[i];
            int k = 1;
                                                                                 
            i++;
            if(c < '0' || c > '9')
                return "El String de parámetro no contiene un número";
            while(i < largo && numero[i] == c)
            {
                k++;
                i++;
            }
            descriptor += k;
            descriptor += c;
        }
        return descriptor;
    }
    public static void Main(string[] args)
    {
        Serie s = new Serie(args[0]);
        int j = Int32.Parse(args[1]);
                                                                                 
        for(int i = 0; i <= j; i++)
        {
            string a = s.termino_especifico(i);
                                                                                 
            Console.WriteLine("Término " + i);
            Console.WriteLine(a);
        }
        for(int i = 0; i <= j; i++)
        {
            string a = s.termino_especifico(i);
                                                                                 
            Console.WriteLine("Largo término " + i + ": " + (a.Length/2));
        }
        for(int i = 0; i <= j - 3; i++)
        {
        	int comp = s.comparar_delante(i, i + 3);
        	
        	Console.WriteLine("Delante término " + i + " y término " + (i + 3) + " = " + comp);
        }
        for(int i = 0; i <= j - 2; i++)
        {
        	int comp = s.comparar_detras(i, i + 2);
        	
        	Console.WriteLine("Detrás término " + i + " y término " + (i + 2) + " = " + comp);
        }
    }
}