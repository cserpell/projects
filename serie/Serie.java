// CRISTIÁN SERPELL 2004

public class Serie {


// Nodo para la lista enlazada de la serie
public class Nodo
{
    public String valor;
    public Nodo siguiente;
    public Nodo(String nuevo_valor, Nodo siguiente_nodo)
    {
        valor = nuevo_valor;
        siguiente = siguiente_nodo;
    }
}

    private Nodo primero;
    private Nodo ultimo;
    private int calculados;
    public Serie(String nueva_semilla)
    {
        primero = new Nodo(nueva_semilla, null);
        ultimo = primero;
        calculados = 1;
    }
    public void calcular_mas_terminos(int cantidad)
    {   // Si cantidad <= 0 no hace nada
        for(int i = 0; i < cantidad; i++)
        {
            Nodo nuevo = new Nodo(numero_descriptor(ultimo.valor), null);
            ultimo.siguiente = nuevo;
            ultimo = nuevo;
            calculados++;
        }
    }
    public String termino_especifico(int numero)
    {
        Nodo lectura = primero;
        // Recordar que no calcula si ya están calculados
        calcular_mas_terminos(numero - calculados + 1);
        for(int i = 0; i < numero; i++)
            lectura = lectura.siguiente;
        return lectura.valor;
    }
    static public String numero_descriptor(String numero)
    {
        String descriptor = "";
        int largo = numero.length();
                                                                                 
        for(int i = 0; i < largo; )
        {
            char c = numero.charAt(i);
            int k = 1;
                                                                                 
            i++;
            if(c < '0' || c > '9')
                return "El String de parámetro no contiene un número";
            while(i < largo && numero.charAt(i) == c)
            {
                k++;
                i++;
            }
            descriptor += k;
            descriptor += c;
        }
        return descriptor;
    }
    static public void main(String[] args)
    {
        Serie s = new Serie(args[0]);
        int j = Integer.parseInt(args[1]);
                                                                                 
        for(int i = 0; i <= j; i++)
        {
            String a = s.termino_especifico(i);
                                                                                 
            System.out.println(a);
            System.out.println("Término " + i + ": " + (a.length()/2));
        }
    }
}
