#include <gtkmm.h>
#include "bola.h"

using namespace std;

class DA : public Gtk::DrawingArea
{
  public:
	DA(int xsize, int ysize, Control *origen) {
        Cp = origen;
		set_size_request(xsize, ysize);
	}
	bool on_expose_event(GdkEventExpose *) {
		Glib::RefPtr<Gdk::Window> win = get_window();
  		Glib::RefPtr<Gdk::GC> gc = get_style()->get_black_gc();
		for(int i = 0; i < Cp->numero_bolas(); i++)
			win->draw_arc(gc, true, (int)(Cp->Bolas[i]->Pos->x*200),
                                    (int)(Cp->Bolas[i]->Pos->y*200),
                      (int)(2*Cp->Bolas[i]->radio), (int)(2*Cp->Bolas[i]->radio), 0, 23040);
  		return true;
	}
  private:
	Control *Cp;
};

class P3d : public Gtk::Window
{
  public:
    P3d(/*double ntes, double np, double elas*/) : m_button("Recalcular"), labeltotal("0") {
        Cp = new Control(0.01, 1, 0.7, 0.7, 0.001);
        muestra = new DA(200, 200, Cp);
        set_border_width(10);
        m_button.signal_clicked().connect(sigc::mem_fun(*this, &P3d::on_button_clicked));
		add(m_Box);
		m_Box.pack_start(labeltotal);
        m_Box.pack_start(m_button);
		m_Box.add(*muestra);
		show_all_children();
		Glib::signal_idle().connect(sigc::mem_fun(*this, &P3d::Loop), Glib::PRIORITY_DEFAULT_IDLE);
    }
	~P3d() {
	}
  protected:
	Control *Cp;
    bool Loop() {
		return true;
	}
    virtual void on_button_clicked() {
		Cp->iterar_movimiento();
    }
	Gtk::VBox m_Box;
    Gtk::Button m_button;
    Gtk::Label labeltotal;
	DA *muestra;
};

int main (int argc, char *argv[])
{
    Gtk::Main kit(argc, argv);

    P3d ventana;

    Gtk::Main::run(ventana);
    return 0;
}
