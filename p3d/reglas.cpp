#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <plot.h>

#define MASA(r)     (r*r)
#define infinite    1e40

class Evento
{
  public:
    double TpoE;
    int Socio;
    int Kind;
    int CC;
};

class Particula
{
  public:
    double PosX;
    double PosY;
    double PosZ;
    double VelX;
    double VelY;
    double VelZ;
    double TpoSE;
    int SE;
    double Tpo;
    Evento *Ev;
    double WelX;
    double WelY;
    double WelZ;
    double AngX;
    double AngY;
    double AngZ;
    double Rot;
    int Wall;
    int Choques;
    char Col[6];
    double Radio;
    int cada;
    int Ll;
};

class Visual
{
    int TPant;
    double CamX, CamY, CamZ, CamVx, CamVy, CamVz;
    double CamYx, CamYy, CamYz, CamXx, CamXy, CamXz;
    double CamD;
    double p_medio;
    int pl_handle;
    int p_tamano;
};

class Control
{
  private:
    double rt13;
    double rn1;
    double RN1;
    double RT13;
    double tanFi;
    double TANfI;
  public:
    Visual *Vis;
    Particula *Part;
    double DELTAPANT;
    double COTAELAST;
    int N;
    double rn;
    double rt;
    double kappa;
    double RN;
    double RT;
    double KAP;
    double Lx;
    double Ly;
    double Lz;
    int *B;
    double reloj;
    double tF;
    double CteReloj;
    int knext;
};

double sq(double d)
{
    return d*d;
}

void Control::CalcRot(double *ax, double *ay, double *az, int k, double t)
{
    double alpha, d0w, mw, wx, wy, wz, d0xwx, d0xwy, d0xwz, den, c1, c2, c3;
    double signo;

    mw = sqrt(Part[k]->WelX*Part[k]->WelX + Part[k]->WelY*Part[k]->WelY + Part[k]->WelZ*Part[k]->WelZ);
    wx = WelX[k]/mw;
    wy = WelY[k]/mw;
    wz = WelZ[k]/mw;
    d0w = Part[k]->AngX*wx + Part[k]->AngY*wy + Part[k]->AngZ*wz;  // d0w = beta
    if(d0w < 0.)
    {
        signo = -1.;
        wx = -wx;
        wy = -wy;
        wz = -wz;
        d0w = -d0w;
    } else
        signo = 1.;
    den = 1. + d0w;
    alpha = cos(mw*t); // Supongo |d0| = |Ang| = 1
    d0xwx = Part[k]->AngY*wz - Part[k]->AngZ*wy;
    d0xwy = Part[k]->AngZ*wx - Part[k]->AngX*wz;
    d0xwz = Part[k]->AngX*wy - Part[k]->AngY*wx;
    c1 = (d0w + alpha)/den;
    c2 = d0w*(1. - alpha)/den;
    c3 = signo*sqrt((1. - alpha + 2.*d0w)*(1. - alpha))/den;
    *ax = Part[k]->AngX*c1 + wx*c2 + d0xwx*c3;
    *ay = Part[k]->AngY*c1 + wy*c2 + d0xwy*c3;
    *az = Part[k]->AngZ*c1 + wz*c2 + d0xwz*c3;
}

// PARTE VISUAL
int Visual::IniciarPantalla(int tamano)
{
    char s[20];
    
    sprintf(s, "%dx%d", tamano, tamano);
    pl_parampl("USE_DOUBLE_BUFFERING", "yes");
    pl_parampl("BITMAPSIZE", s);
    pl_handle = pl_newpl("X", stdin, stdout, stderr);
    pl_selectpl(pl_handle);
    if(pl_openpl() < 0)     
    {
        fprintf(stderr, "No se pudo abrir ventana\n");
        return 1;
    }
    pl_fspace(0.0, 0.0, tamano, tamano);
    pl_flinewidth(0.01);
    pl_filltype(0); /* 0 = transparente, 1 = lleno */
    p_tamano = tamano;
    p_medio = tamano/2.;
    return 0;
}

void Visual::IniciarCamara(double cx, double cy, double cz, double vx, double vy, double vz, double d)
{
    double r;
    
    CamX = cx;
    CamY = cy;
    CamZ = cz;
    r = sqrt(vx*vx + vy*vy + vz*vz);
    CamVx = vx/r;
    CamVy = vy/r;
    CamVz = vz/r;
    CamD = d;
    CamYx = -CamVy*CamVx;
    CamYy = 1. - CamVy*CamVy;
    CamYz = -CamVy*CamVz;
    r = sqrt(CamYx*CamYx + CamYy*CamYy + CamYz*CamYz);
    CamYx = CamYx/r;
    CamYy = CamYy/r;
    CamYz = CamYz/r;
    CamXx = -CamVz/r;
    CamXy = 0.;
    CamXz = CamVx/r;
}

void Visual::CalcProyR(double *px, double *py, double *r, double x, double y, double z)
{
    double den;  // Lo hace sin restar pos de camara

    den = CamD/(x*CamVx + y*CamVy + z*CamVz);
    *px = den*(x*CamXx + y*CamXy + z*CamXz) + p_medio;
    *py = den*(x*CamYx + y*CamYy + z*CamYz) + p_medio;
    *r = CamD/sqrt(x*x + y*y + z*z);
}

void Visual::CalcProy(double *px, double *py, double x, double y, double z)
{
    double den;
    
    x -= CamX;
    y -= CamY;
    z -= CamZ;
    den = CamD/(x*CamVx + y*CamVy + z*CamVz);
    *px = den*(x*CamXx + y*CamXy + z*CamXz) + p_medio;
    *py = den*(x*CamYx + y*CamYy + z*CamYz) + p_medio;
}

void Visual::DibujarEjes(double Lx, double Ly, double Lz)
{
    double px[8], py[8];

    CalcProy(&px[0], &py[0], 0., Ly, 0.);
    CalcProy(&px[1], &py[1], 0., 0., 0.);
    CalcProy(&px[2], &py[2], Lx, Ly, 0.);
    CalcProy(&px[3], &py[3], Lx, 0., 0.);
    CalcProy(&px[4], &py[4], Lx, Ly, Lz);
    CalcProy(&px[5], &py[5], Lx, 0., Lz);
    CalcProy(&px[6], &py[6], 0., Ly, Lz);
    CalcProy(&px[7], &py[7], 0., 0., Lz);
    pl_fline(px[0], py[0], px[1], py[1]);
    pl_fline(px[0], py[0], px[2], py[2]);
    pl_fline(px[0], py[0], px[6], py[6]);
    pl_fline(px[3], py[3], px[1], py[1]);
    pl_fline(px[3], py[3], px[2], py[2]);
    pl_fline(px[3], py[3], px[5], py[5]);
    pl_fline(px[4], py[4], px[2], py[2]);
    pl_fline(px[4], py[4], px[5], py[5]);
    pl_fline(px[4], py[4], px[6], py[6]);
    pl_fline(px[7], py[7], px[1], py[1]);
    pl_fline(px[7], py[7], px[6], py[6]);
    pl_fline(px[7], py[7], px[5], py[5]);
}

void Visual::DibujarPantalla(Control *Co)
{ 
    int k;
    double t, x, y, z, px, py, r, pp, pt, xa, ya, za;
    
    pl_erase();
    DibujarEjes(Co->Lx, Co->Ly, Co->Lz);
    for(k = 0; k < Co->N; k++)
    {
        t = Co->tF - Co->Part[k]->Tpo;
        x = Co->Part[k]->PosX + Co->Part[k]->VelX*t - CamX;
        y = Co->Part[k]->PosY + Co->Part[k]->VelY*t - CamY;
        z = Co->Part[k]->PosZ + Co->Part[k]->VelZ*t - CamZ;
        CalcProyR(&px, &py, &r, x, y, z);
        r *= Co->Part[k]->Radio;
        pl_fillcolorname(Col[k]);
        pl_fcircle(px, py, r);
//        CalcRot(&xa, &ya, &za, k, t);
        xa = 0.;
        ya = 0.;
        za = 0.;
        x += xa*Co->Part[k]->Radio;
        y += ya*Co->Part[k]->Radio;
        z += za*Co->Part[k]->Radio;
        CalcProyR(&px, &py, &r, x, y, z);
        r *= 0.3*Co->Part[k]->Radio;
        pl_fillcolorname("black");  
        pl_fcircle(px, py, r);
    }
}

void Visual::CerrarPantalla()
{
    pl_closepl();
    pl_selectpl(0);
    pl_deletepl(pl_handle);
}
// FIN PARTE VISUAL

void Particula::Mueve(double reloj)
{
    double t, mo, ax, ay, az;
    
    t       = reloj - Tpo;
    PosX = PosX + VelX*t;
    PosY = PosY + VelY*t;
    PosZ = PosZ + VelZ*t;
    CalcRot(&ax, &ay, &az, k, t);
    AngX = ax;
    AngY = ax;
    AngZ = ax;
    mo = sqrt(AngX*AngX + AngY*AngY + AngZ*AngZ);
    AngX /= mo;  // Hay que hacer la correccion..
    AngY /= mo;
    AngZ /= mo;
   /* mo = sqrt(AngX*AngX + AngY*AngY + AngZ*AngZ);
    printf("Nueva Ang: x = %f y = %f z = %f  mod = %f\n", AngX, AngY, AngZ, mo);*/
    Tpo  = reloj;
}

void Control::ChoqueDD(int k1, int k2) //regla de choque discos k1 y k2 //17
{
    double tx, ty, tz, v21x, v21y, v21z, w21x, w21y, w21z, crx, cry, crz, vn,
           qn, qt, qx, qy, qz, nx, ny, nz, nQ, ax, ay, az, m1, m2, mu;
    
    nQ = Part[k1]->Radio + Part[k2]->Radio; //  nQ = sqrt(nx*nx + ny*ny + nz*nz) = R1 + R2
    nx = (Part[k1]->PosX - Part[k2]->PosX)/nQ; /* n vector unitario normal al pto de choque */
    ny = (Part[k1]->PosY - Part[k2]->PosY)/nQ;
    nz = (Part[k1]->PosZ - Part[k2]->PosZ)/nQ;
    v21x = VelX[k2] - Part[k1]->VelX;
    v21y = VelY[k2] - Part[k1]->VelY;
    v21z = VelZ[k2] - Part[k1]->VelZ;
    w21x = Part[k1]->Radio*Part[k1]->WelX + Radio[k2]*WelX[k2];
    w21y = Part[k1]->Radio*Part[k1]->WelY + Radio[k2]*WelY[k2];
    w21z = Part[k1]->Radio*Part[k1]->WelZ + Radio[k2]*WelZ[k2];
    ax = v21x + w21y*nz - w21z*ny; /*  + w21 cruz n */
    ay = v21y + w21z*nx - w21x*nz;
    az = v21z + w21x*ny - w21y*nx;
    crx = ay*nz - az*ny;
    cry = az*nx - ax*nz;
    crz = ax*ny - ay*nx;
    nQ = sqrt(crx*crx + cry*cry + crz*crz);
    crx = crx/nQ;
    cry = cry/nQ;
    crz = crz/nQ;
    tx = cry*nz - crz*ny;    /* t vector tangencial al pto de choque */
    ty = crz*nx - crx*nz;
    tz = crx*ny - cry*nx;
    vn = v21x*nx + v21y*ny + v21z*nz; // vt = ax*tx + ay*ty + az*tz = -nQ
    m1 = MASA(Part[k1]->Radio);
    m2 = MASA(Part[k2]->Radio);
    mu = m1*m2/(m1 + m2);
 //   qn = rn1*vn*mu;
    if(vn > COTAELAST)
        qn = rn1*vn*mu;
    else
        qn = 2.*vn*mu;   // en este caso es elastico, rn = 1
    if(/*avt*/nQ < tanFi*fabs(vn))
        qt = rt13*mu*nQ;
    else
        qt = /*avt*/kappa*qn;
    qx = qn*nx - qt*tx;
    qy = qn*ny - qt*ty;
    qz = qn*nz - qt*tz;
    Part[k1]->VelX += qx/m1; /* masa 1 */  // Resultados
    Part[k1]->VelY += qy/m1;
    Part[k1]->VelZ += qz/m1;
    Part[k2]->VelX -= qx/m2; /* masa 2 !! */
    Part[k2]->VelY -= qy/m2;
    Part[k2]->VelZ -= qz/m2;
    nQ = 2.*qt;
    crx = nQ*(ny*tz - nz*ty); /* 2 * qt * (n cruz t) */
    cry = nQ*(nz*tx - nx*tz);
    crz = nQ*(nx*ty - ny*tx);
    nQ = m1*Part[k1]->Radio;
    Part[k1]->WelX += crx/nQ;
    Part[k1]->WelY += cry/nQ;
    Part[k1]->WelZ += crz/nQ;
    nQ = m2*Part[k2]->Radio;
    Part[k2]->WelX += crx/nQ;
    Part[k2]->WelY += cry/nQ;
    Part[k2]->WelZ += crz/nQ;
}

void Particula::LimpiaLista()
{
    int e;
    
    for(e = 0; e < Ll; e++)
        Ev[e]->TpoE = infinite;
}

void Particula::AgregaEvento(int Soc, int Tipo, int Ce, double Te)
{
    int e;
    
    e = 0;
    while(Ev[e]->TpoE != infinite && e < Ll)
        e++;
    if(e >= Ll) // Esta comparacion se debe sacar cuando funcione completamente
    {
        printf("Lista llena!! e = %d (se debe usar un Ll mas grande)\n", e);
        exit(0);
    }
    Ev[e]->Socio = Soc; // de la lista
    Ev[e]->Kind  = Tipo;
    Ev[e]->CC    = Ce;
    Ev[e]->TpoE  = Te;
}

void Particula::Ganador()
{
    int e, j, ka;
    double tau;
    
    tau = infinite;
    e = -1;
    for(j = 0; j < Ll; j++)
        if(Ev[j]->TpoE < tau) 
        {  
            tau = Ev[j]->TpoE;
            e = j;
            ka = Ev[j]->Kind;
        }
    SE = e;             // el ganador de la lista k es el 'e-simo evento 
    TpoSE = Ev[e]->TpoE; // de la lista
}  

void Control::UpGradeCBT(int kk)  //6
{ 
    int f, ll, rr;
    
    f = kk + N - 1;
    do
    {
        f  = (f - 1)/2;
        ll = 2*f + 1;
        rr = ll + 1; /* 2*f + 2 */
        if(Part[B[ll]]->TpoSE < Part[B[rr]]->TpoSE)
            B[f] = B[ll];
        else
            B[f] = B[rr];
    } while(f != 0);
}

void Control::IniciaCBT()
{ 
    int f, r;
    
    for(r = N - 1; r < 2*N - 1; r++)
        B[r] = r - N + 1;   /* da a cada hoja lo que tendra siempre */
    f = N - 1;
    for(r = 2*N - 2; r > 0; r -= 2)
    {
        f--;  /* f= (r - 1)/2 */
        if(Part[B[r]]->TpoSE < Part[B[r - 1]]->TpoSE)
            B[f] = B[r];
        else
            B[f] = B[r - 1];
    }
}  

void Control::ChoqueW()  //choques con paredes
{
    double tx, ty, tz/*, v21x, v21y, v21z, w21x, w21y, w21z*/, crx, cry, crz, vn,
           qn, qt, nQ/*, nx, ny, nz*/;
    
    switch(Wall[knext]) // OJO ahora hay mov forzado
    {
        case 0:  // Izquierda
            cry = Part[knext]->VelZ + Part[knext]->Radio*Part[knext]->WelY;
            crz = Part[knext]->VelY - Part[knext]->Radio*Part[knext]->WelZ;
            vn  = -Part[knext]->VelX;
            nQ  = sqrt(cry*cry + crz*crz);
            ty  = crz/nQ;
            tz  = cry/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            Part[knext]->VelX += qn;
            Part[knext]->VelY -= qt*ty;
            Part[knext]->VelZ -= qt*tz;
            nQ  = 2.*qt/Part[knext]->Radio;
            Part[knext]->WelY -= nQ*tz;
            Part[knext]->WelZ += nQ*ty;
            break;
        case 1:  // Piso
            crx = Part[knext]->VelZ - Part[knext]->Radio*Part[knext]->WelX;
            crz = Part[knext]->VelX + Part[knext]->Radio*Part[knext]->WelZ;
            vn  = -Part[knext]->VelY;
            nQ  = sqrt(crx*crx + crz*crz);
            tx  = crz/nQ;    // t vector tangencial al pto de choque
            tz  = crx/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            Part[knext]->VelX -= qt*tx;
            Part[knext]->VelY += qn;
            Part[knext]->VelZ -= qt*tz;
            nQ  = 2.*qt/Part[knext]->Radio;
            Part[knext]->WelX += nQ*tz;
            Part[knext]->WelZ -= nQ*tx;
            break;
        case 2:  // Derecha
            cry = Part[knext]->VelZ - Part[knext]->Radio*Part[knext]->WelY;
            crz = Part[knext]->VelY + Part[knext]->Radio*Part[knext]->WelZ;
            vn  = Part[knext]->VelX;
            nQ  = sqrt(cry*cry + crz*crz);
            ty  = crz/nQ;
            tz  = cry/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            Part[knext]->VelX -= qn;
            Part[knext]->VelY -= qt*ty;
            Part[knext]->VelZ -= qt*tz;
            nQ  = 2.*qt/Part[knext]->Radio;
            Part[knext]->WelY += nQ*tz;
            Part[knext]->WelZ -= nQ*ty;
            break;
        case 3:  // Techo
            crx = Part[knext]->VelZ + Part[knext]->Radio*Part[knext]->WelX;
            crz = Part[knext]->VelX - Part[knext]->Radio*Part[knext]->WelZ;
            vn  = Part[knext]->VelY;
            nQ  = sqrt(crx*crx + crz*crz);
            tx  = crz/nQ;    // t vector tangencial al pto de choque
            tz  = crx/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            Part[knext]->VelX -= qt*tx;
            Part[knext]->VelY -= qn;
            Part[knext]->VelZ -= qt*tz;
            nQ  = 2.*qt/Part[knext]->Radio;
            Part[knext]->WelX -= nQ*tz;
            Part[knext]->WelZ += nQ*tx;
            break;
        case 4:  // Delante
            crx = Part[knext]->VelY + Part[knext]->Radio*Part[knext]->WelX;
            cry = Part[knext]->VelX - Part[knext]->Radio*Part[knext]->WelY;
            vn  = -Part[knext]->VelZ;
            nQ  = sqrt(crx*crx + cry*cry);
            tx  = cry/nQ;    // t vector tangencial al pto de choque
            ty  = crx/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            Part[knext]->VelX -= qt*tx;
            Part[knext]->VelY -= qt*ty;
            Part[knext]->VelZ += qn;
            nQ  = 2.*qt/Part[knext]->Radio;
            Part[knext]->WelX -= nQ*ty;
            Part[knext]->WelY += nQ*tx;
            break;
        default://case 5:  // Atras
            crx = Part[knext]->VelY - Part[knext]->Radio*Part[knext]->WelX;
            cry = Part[knext]->VelX + Part[knext]->Radio*Part[knext]->WelY;
            vn  = Part[knext]->VelZ;
            nQ  = sqrt(crx*crx + cry*cry);
            tx  = cry/nQ;    // t vector tangencial al pto de choque
            ty  = crx/nQ;
            if(vn > COTAELAST)
                qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
            else
                qn = 2.*vn;   // en este caso es elastico, rn = 1
            if(nQ < TANfI*vn) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
                qt = RT13*nQ;
            else
                qt = KAP*qn;
            Part[knext]->VelX -= qt*tx;
            Part[knext]->VelY -= qt*ty;
            Part[knext]->VelZ -= qn;
            nQ  = 2.*qt/Part[knext]->Radio;
            Part[knext]->WelX += nQ*ty;
            Part[knext]->WelY -= nQ*tx;
            break;
    }
/*    v21x = -Part[knext]->VelX;
    v21y = -Part[knext]->VelY;
    v21z = -Part[knext]->VelZ;
    w21x = Part[knext]->Radio*Part[knext]->WelX;
    w21y = Part[knext]->Radio*Part[knext]->WelY;
    w21z = Part[knext]->Radio*Part[knext]->WelZ;
    AQUI DECLARAR n VECTOR NORMAL Y AGREGAR VEL DE LA MURALLA A v21
    ax   = v21x + w21y*nz - w21z*ny; //  + w21 cruz n
    ay   = v21y + w21z*nx - w21x*nz;
    az   = v21z + w21x*ny - w21y*nx;
    crx  = ay*nz - az*ny;
    cry  = az*nx - ax*nz;
    crz  = ax*ny - ay*nx;
    nQ   = sqrt(crx*crx + cry*cry + crz*crz);
    crx  = crx/nQ;
    cry  = cry/nQ;
    crz  = crz/nQ;
    tx   = cry*nz - crz*ny;    // t vector tangencial al pto de choque
    ty   = crz*nx - crx*nz;
    tz   = crx*ny - cry*nx;
    vn   = v21x*nx + v21y*ny + v21z*nz; // vt = ax*tx + ay*ty + az*tz = -nQ
    if(vn > COTAELAST)
        qn = RN1*vn;  // qn y qt representan qn/mu y qt/mu .. (mu = MASA(Radio[knext]))
    else
        qn = 2.*vn;   // en este caso es elastico, rn = 1
    if(nQ < TANfI*fabs(vn)) // nQ >= 0 => avt = fabs(vt) = -vt = nQ
        qt = RT13*nQ;
    else
        qt = KAP*qn;
    Part[knext]->VelX += qn*nx - qt*tx; // Resultados
    Part[knext]->VelY += qn*ny - qt*ty;
    Part[knext]->VelZ += qn*nz - qt*tz;
    nQ  = 2.*qt/Radio[knext];
    crx = nQ*(ny*tz - nz*ty); // 2 * qt * (n cruz t)
    cry = nQ*(nz*tx - nx*tz);
    crz = nQ*(nx*ty - ny*tx);
    Part[knext]->WelX += crx;
    Part[knext]->WelY += cry;
    Part[knext]->WelZ += crz;*/
}

double Control::TchoqueDD(int k, int j)  // tiempo choque discos k y j (Tpo[k] > Tpo[j])
{ // Esta es la que gasta mas tiempo de todas!!
    double b, Del, Rad, R2, x, y, z, gx, gy, gz, gg, qq,/* dtk,*/ dtj;

    Rad = Part[k]->Radio + Part[j]->Radio;
    R2  = Rad*Rad;
//    dtk = reloj - Tpo[k];
    dtj = Part[k]->Tpo - Part[j]->Tpo;
    x   = Part[k]->PosX - Part[j]->PosX - Part[j]->VelX*dtj;
    y   = Part[k]->PosY - Part[j]->PosY - Part[j]->VelY*dtj;
    z   = Part[k]->PosZ - Part[j]->PosZ - Part[j]->VelZ*dtj;
    gx  = Part[k]->VelX - Part[j]->VelX;
    gy  = Part[k]->VelY - Part[j]->VelY;
    gz  = Part[k]->VelZ - Part[j]->VelZ;
    b   = -x*gx - y*gy - z*gz;
    qq  = x*x + y*y + z*z - R2;
    gg  = gx*gx + gy*gy + gz*gz;  // gg es usado solo al definir Del
    Del = b*b - gg*qq;
    if(b > 0. && Del > 0.) // b > 0 => gg != 0)
    {
        return Part[k]->Tpo + qq/(b + sqrt(Del));
    }
    return infinite;
} 

double Control::TchoqueDW(int k) //tiempo pa chocar contra pared
{
    double ty, mint, tx, tz;
    int w, wx, wz, wy;
    
    if(Part[k]->VelX != 0.)
    {
        if(Part[k]->VelX > 0.)
        {
            tx = (Lx - Part[k]->PosX - Part[k]->Radio)/Part[k]->VelX;
            wx = 2;
        } else
        {
            tx = (Part[k]->Radio - Part[k]->PosX)/Part[k]->VelX;
            wx = 0;
        }
    } else
        tx = infinite;
    if(Part[k]->VelY != 0.)
    {
        if(Part[k]->VelY > 0.)
        {
            ty = (Ly - Part[k]->PosY - Part[k]->Radio)/Part[k]->VelY;
            wy = 3;
        } else
        {
            ty = (Part[k]->Radio - Part[k]->PosY)/Part[k]->VelY;
            wy = 1;
        }
    } else
        ty = infinite;
    if(Part[k]->VelZ != 0.)
    {
        if(Part[k]->VelZ > 0.)
        {
            tz = (Lz - Part[k]->PosZ - Part[k]->Radio)/Part[k]->VelZ;
            wz = 5;
        } else
        {
            tz = (Part[k]->Radio - Part[k]->PosZ)/Part[k]->VelZ;
            wz = 4;
        }
    } else
        tz = infinite;
    mint = infinite;
    w = -1;
    if(tx < mint)  // Ahora siempre son > 0 los tiempos ya calculados
    {
        mint = tx;
        w = wx;
    }
    if(ty < mint)
    {
        mint = ty;
        w = wy;
    }
    if(tz < mint)
    {
        mint = tz;
        w = wz;
    }
    Part[k]->Wall = w;
    return Part[k]->Tpo + mint;
} 

void Control::EventosNuevos(int k)
{
    int j, c;
    double tj;
    
    for(j = 0; j < k; j++)
    {
        tj = TchoqueDD(k, j);
        c  = Part[j]->Choques;
        if(tj < infinite)
            Part[k]->AgregaEvento(j, 1, c, tj);
    }
    j++;
    for(; j < N; j++)
    {
        tj = TchoqueDD(k, j);
        c  = Part[j]->Choques;
        if(tj < infinite)
            Part[k]->AgregaEvento(j, 1, c, tj);
    }
    tj = TchoqueDW(k); // siempre hay choque con alguna pared (la de abajo)
    Part[k]->AgregaEvento(Part[k]->Wall, 0, Part[k]->Choques, tj); //1er 0=>W, 2do 0=>C[pared]    
}

void Control::NextEvent()		// para entendidos
{
    int jsoc, vale, e, kind, k, l;
    double tnext;
    
    do
    {
        knext = B[0];
        vale  = 1;
        e     = Part[knext]->SE;
        tnext = Part[knext]->Ev[e]->TpoE;
        jsoc  = Part[knext]->Ev[e]->Socio;  //necesario
        kind  = Part[knext]->Ev[e]->Kind;
        if(kind && Part[knext]->Ev[e]->CC != Part[jsoc]->Choques)
        {
            vale = 0;
            Part[knext]->Ev[e]->TpoE = infinite;
            Part[knext]->Ganador();
            UpGradeCBT(knext);
        }
    } while(vale == 0);
    
    while(tF < tnext)
    {
        Vis->DibujarPantalla();
        tF += DELTAPANT;
    }
    reloj = tnext;
    if(kind)
    {
        Part[knext]->Mueve();
        Part[jsoc]->Mueve();
        ChoqueDD(knext, jsoc); //aplica los Choque[a]++
        Part[knext]->Choques++;
        Part[jsoc]->Choques++;
        Part[knext]->LimpiaLista();
        EventosNuevos(knext);
        Part[jsoc]->LimpiaLista();
        EventosNuevos(jsoc);
        Part[knext]->Ganador();
        Part[jsoc]->Ganador();
        UpGradeCBT(knext);
        UpGradeCBT(jsoc);
    } else   //choque con pared
    {
        Part[knext]->Mueve();  // OJO DESPUES SE MUEVE FORZADO
        ChoqueW();
        Part[knext]->LimpiaLista();
        EventosNuevos(knext);  
        Part[knext]->Ganador();
        UpGradeCBT(knext);
    }
    if(reloj > CteReloj)
    {
        for(k = 0; k < N; k++)
        {
            Part[k]->Tpo -= CteReloj;
            Part[k]->TpoSE -= CteReloj;
            for(l = 0; l < Ll; l++)
                Part[k]->Ev[l]->TpoE -= CteReloj;
        }
        reloj -= CteReloj;
        tF -= CteReloj;
    }
}

void Control::Ciclo()
{
    int s;
    
    s = 0;
    do
    {
        NextEvent();
        if(s%cada == 0)
        {
            double c = 4.*(s%(N*N))*Lx/(N*N);
            
            Vis->IniciarCamara(2.*Lx - c, Ly, -3.*Lz, c - 1.5*Lx, -0.5*Ly, 3.5*Lz, 28.*Lz);
        }
        s++;
    } while(s < 20000000);   /* LOOP PRINCIPAL */
}

void Control::Control()
{
    int j, k, p;
    
    double na, nb;
    double r, r1;
    double epsi = 0.001;
    
    reloj    = 0.;
    CteReloj = 3.;
    rn    = 0.4;
    RN    = 0.9;
    rt    = 0.4;
    RT    = 0.9;
    kappa = 0.1;
    KAP   = 0.01;
    rn1   = rn + 1.;
    RN1   = RN + 1.;
    rt13  = (rt + 1.)/3.;
    RT13  = (RT + 1.)/3.;
    tanFi = rn1*kappa/rt13;
    TANfI = RN1*KAP/RT13;
    k = 0;
    j = 0;
    p = 0;
    r1 = 0.5;
    r = r1 + epsi;
    Lx = 30;
    Ly = 1.8;
    Lz = 10;
    na = (int)(Lx/(2.*r));
    nb = (int)((Lx/(2.*r)) - 1.);
do
  { int i=0;
    while(i<na && k<N)
    { Part[k]->PosX = r + 2.0*r*i;
      Part[k]->PosZ = epsi+(epsi + (sqrt(3)*j+1)*r);     //<<<<<<<<<<<<<<<<<
      if(Part[k]->PosZ>Lz-r1)
      {  printf("La P%d no cabe!\n",k);
         exit(0);
      }
      i++;
      Part[k]->PosY  = (Ly - 2*r1)*(((double)rand())/RAND_MAX) + r1;
      Part[k]->Radio = r1;
      k++;
    }
    j++;
    i=0;
    while(i<nb && k<N)
    { Part[k]->PosX = 2.0*r + 2.0*r*i;
      Part[k]->PosZ = epsi+(epsi + (sqrt(3)*j+1)*r);   
      if(Part[k]->PosZ>Lz-r1)
      {  printf("La P%d no cabe!\n",k);
         exit(0);
      }
     i++;
     Part[k]->PosY  = (Ly - 2*r1)*(((double)rand())/RAND_MAX) + r1;
     Part[k]->Radio = r1;
     k++;
    }
    j++;
  }while(k<N);
  /*  while(k < N)
    {
        int l;
        
        Part[k]->Radio = r1*(1. + VARRADIO*(0.5 - ((double)rand())/RAND_MAX));
        Part[k]->PosX  = (Lx - 2*Radio[k])*(((double)rand())/RAND_MAX) + Radio[k];
        Part[k]->PosY  = (Ly - 2*Radio[k])*(((double)rand())/RAND_MAX) + Radio[k];
        Part[k]->PosZ  = (Lz - 2*Radio[k])*(((double)rand())/RAND_MAX) + Radio[k];
        //Part[k]->PosZ = 2.;
        for(l = 0; l < k; l++)
        {
            double x, y, z, rr;
            
            x  = Part[k]->PosX - Part[l]->PosX;
            y  = Part[k]->PosY - Part[l]->PosY;
            z  = Part[k]->PosZ - Part[l]->PosZ;
            rr = Part[k]->Radio + Part[l]->Radio;
            if(x*x + y*y + z*z < rr*rr + EPSREVISION)
                break;
        }
        if(l == k)
        {
            k++;
            j = 0;
            continue;
        }
        j++;
        if(j > p)
            p = j;
        if(j == MAXPRUEBAS)
        {
            printf("La %d no cabe luego de %d pruebas\n", k + 1, j);
            printf("Aumentar MAXPRUEBAS o hacer N = %d\n", k);
            exit(1);
        }
    }*/
    printf("Numero maximo de pruebas usadas fue %d\n", p);
    for(k = 0; k < N; k++) //sortea velocidades
    {
        Part[k]->VelX = sqrt(Tb)*(0.5 - drand48());
        Part[k]->VelY = sqrt(Tb)*(0.6 - drand48());
        Part[k]->VelZ = sqrt(Tb)*(0.5 - drand48());
//        VelZ[k] = 0.;
        Part[k]->Tpo  = 0.;
        Part[k]->AngX = 0.;
        Part[k]->AngY = 1.;  // Apuntan hacia arriba
        Part[k]->AngZ = 0.;
        Part[k]->Rot  = 0.;
        Part[k]->WelX = 0.5 - drand48();
        Part[k]->WelY = 0.5 - drand48();
        Part[k]->WelZ = 0.5 - drand48();
        if(Part[k]->PosY < 0.3*Ly)
            sprintf(Part[k]->Col,"%s","green");
        if(Part[k]->PosY > 0.3*Ly)
            sprintf(Part[k]->Col,"%s","blue");
    }
}  

void Inic()
{
    int i;

    tF     = 0.;
    XVinic();
    for(i = 0; i < N; i++)
    {
        Part[i]->Choques = 0;
        Part[i]->LimpiaLista();
        EventosNuevos(i);
        Part[i]->Ganador();
    }
    IniciaCBT(); 
}

// ___________________________________main__________________________________________
int main(int argc, char *argv[])
{
    Tb     = 65.0;
    srand48(111);
    Inic();

    Vis->IniciarPantalla(TPant);
    Vis->IniciarCamara(Lx, Ly/2, -Lz/2, -0.5, 0, 1, 8*Lz);
    Vis->DibujarPantalla();

    printf("-------- fin inicializacion --------:\n");
    printf("N = %d\tLx = %f\tLy = %f\tLz = %f\nr1 = %f\tVARRADIO = %f\n", N, Lx, Ly, Lz, r1, VARRADIO);
    printf("rn = %f\trt = %f\tkappa = %f\ng = %f\tCOTAELAS = %f\n", rn, rt, kappa, g, COTAELAST);
    printf("RN = %f\tRT = %f\tKAP = %f\n", RN, RT, KAP);
    printf("A = %f\tT = %f\n", A, T);
    
    Termaliza();

    printf("\n\n                           -- F I N --\n\n\n");

    Vis->CerrarPantalla();
    return 0;
}
