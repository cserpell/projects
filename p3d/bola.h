#define NBOL (10)

using namespace std;

class Vector3d
{
  public:
    double x;
    double y;
    double z;
    Vector3d() {
        x = 0;
        y = 0;
        z = 0;
    }
    Vector3d(double xx, double yy, double zz) {
        x = xx;
        y = yy;
        z = zz;
    }
    static double Dot(Vector3d *A, Vector3d *B) {
        return A->x*B->x + A->y*B->y + A->z*B->z;
    }
    static Vector3d *Mult(double a, Vector3d *A) {
        Vector3d *Ret = new Vector3d();
        
        Ret->x = a*A->x;
        Ret->y = a*A->y;
        Ret->z = a*A->z;
        return Ret;
    }
    static Vector3d *Sum(Vector3d *A, Vector3d *B) {
        Vector3d *Ret = new Vector3d();
        
        Ret->x = A->x + B->x;
        Ret->y = A->y + B->y;
        Ret->z = A->z + B->z;
        return Ret;
    }
    static Vector3d *Cross(Vector3d *A, Vector3d *B) {
        Vector3d *Ret = new Vector3d();
        
        Ret->x = A->y*B->z - A->z*B->y;
        Ret->y = A->z*B->x - A->x*B->z;
        Ret->z = A->x*B->y - A->y*B->x;
        return Ret;
    }
    void Reset() {
        x = 0;
        y = 0;
        z = 0;
    }
    double Norm2() {
        return x*x + y*y + z*z;
    }
    Vector3d *Sphere();
};

class Bola
{
  public:
    Vector3d *Polo;  // Indica hacia donde esta un polo (visual)
    double gpolo; // Grado de rotacion del polo (visual) en rads

    double masa;  // Constante que representa la masa
    double radio;
    Vector3d *Pos; // Posicion fisica
    Vector3d *Vel; // Velocidad fisica
    Vector3d *Velantigua;
    Vector3d *Rot; // Vector por el que se está rotando
    Bola(double radi, double mass, Vector3d *Npos) {
        Polo = new Vector3d();
        Pos = Npos;
        Vel = new Vector3d();
        Velantigua = new Vector3d();
        Rot = new Vector3d();
        masa = mass;
        radio = radi;
    }
    bool en_movimiento();
    void iterar_movimiento(double coefroce, double coefrot, double paso);
};

class Muro
{
  public:
    Vector3d *Normal; // Normal del plano
    Vector3d *Punto;  // Un punto del plano
    Muro(Vector3d *Norm, Vector3d *Point) {
        Normal = Norm;
        Punto = Point;
    }
};

class Control
{
  public:
    Bola *Bolas[NBOL]; // No quiero hacer una lista enlazada porque me tinca más lento
    Muro *Muros[6];
    double coefroce;  // Roce por el movimiento contra el aire
    double coefrot;
    double paso;
    Control(double radio, double masa, double croce, double crot, double pa) {
        Muros[0] = new Muro(new Vector3d(1., 0., 0.), new Vector3d(0., 0., 0.));
        Muros[1] = new Muro(new Vector3d(0., 1., 0.), new Vector3d(0., 0., 0.));
        Muros[2] = new Muro(new Vector3d(0., 0., 1.), new Vector3d(0., 0., 0.));
        Muros[3] = new Muro(new Vector3d(-1., 0., 0.), new Vector3d(1., 1., 1.));
        Muros[4] = new Muro(new Vector3d(0., -1., 0.), new Vector3d(1., 1., 1.));
        Muros[5] = new Muro(new Vector3d(0., 0., -1.), new Vector3d(1., 1., 1.));
        for(int i = 0; i < NBOL; i++) /* OJO: radio < 1/(3*NBOL) */
            Bolas[i] = new Bola(radio, masa, new Vector3d(0.5, 0.5, 3*radio*i));
        coefroce = croce;
        coefrot = crot;
        paso = pa;
    }
    void iterar_movimiento() {
        for(int i = 0; i < NBOL; i++)
            Bolas[i]->iterar_movimiento(coefroce, coefrot, paso);
    }
    int numero_bolas() {
        return NBOL;
    }
};
