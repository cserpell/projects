#include <cmath>
#include <stdio.h>

#include "bola.h"

//#define NBOL (10)

double epsilon = 0.0001;

using namespace std;

Vector3d *Vector3d::Sphere() {
    Vector3d *Ret = new Vector3d();
        
    Ret->z = atan2(y, x);  /* phi */
    if(cos(Ret->z))
        Ret->y = atan2(x, z*cos(Ret->z)); /* theta */
    else
        Ret->y = atan2(y, z*sin(Ret->z)); /* theta */
    if(cos(Ret->y))
        Ret->x = z/cos(Ret->y); /* r */
    else if(cos(Ret->z))
        Ret->x = x/(sin(Ret->y)*cos(Ret->z)); /* r */
    else
        Ret->x = y/(sin(Ret->y)*sin(Ret->z)); /* r */
    return Ret;
}

bool Bola::en_movimiento() {
    if((Vel->Norm2() != 0) || (Rot->Norm2() != 0))
        return true;
    return false;
}

void Bola::iterar_movimiento(double coefroce, double coefrot,
                    /* Estas variables podrian ser locales */ double paso) {
                          // Todo mayor que 0, paso siempre igual!
    // Movimiento:
    Vector3d *VelNueva = Vector3d::Sum(Vel, Vector3d::Mult(paso/masa, (Vector3d::Sum(Vector3d::Mult(-coefroce, Vel), Vector3d::Cross(Vel, Rot)))));
    Pos = Vector3d::Sum(Pos, Vector3d::Mult(-paso/2, (Vector3d::Sum(Vector3d::Mult(-3, Vel), Velantigua))));
        
    // Rotacion:   (la norma del vector rot indica el angulo en rads)
    double nr = sqrt(Rot->Norm2());
    if(nr != 0)
    {
        double ca = 1 - cos(nr);
        double sa = sin(nr);
    
        /*double r11, r12, r13, r21, r22, r23, r31, r32, r33;
        double x, y, z;
        x = Rot->x;
        y = Rot->y;
        z = Rot->z;
        r11 = 1 - ca*(z*z + y*y);
        r12 = -sa*z + ca*(x*y);
        r13 = sa*y + ca*(x*z);
        r21 = sa*z + ca*(x*y);
        r22 = 1 - ca*(z*z + x*x);
        r23 = -sa*x + ca*(y*z);
        r31 = -sa*y + ca*(x*z);
        r32 = sa*x + ca*(y*z);
        r33 = 1 - ca*(y*y + x*x);*/
    
        Rot = Vector3d::Mult(1/nr, Rot);
        Polo = Vector3d::Sum(Vector3d::Sum(Polo, Vector3d::Mult(sa, Vector3d::Cross(Rot, Polo))), Vector3d::Mult(ca, Vector3d::Cross(Rot, Vector3d::Cross(Rot, Polo))));
        Rot = Vector3d::Mult(nr, Rot);
    }
        
    Velantigua = Vel;
    Vel = VelNueva;
    if(Vel->Norm2() < epsilon)
        Vel->Reset();
    Rot = Vector3d::Mult(coefrot, Rot);
    if(Rot->Norm2() < epsilon)
        Rot->Reset();
}

/*
int main(int argc, char *argv[])
{
    Bola bolaprueba;
    
    bolaprueba.Polo->x = 1;
    bolaprueba.masa = 1;
    bolaprueba.Vel->x = 1;
    bolaprueba.Vel->z = 1;
//    bolaprueba.Rot->z = 1;
    bolaprueba.Rot->y = -1;

    while(bolaprueba.en_movimiento())
    {
        printf("Pos(x,y,z) = (%f, %f, %f)\n",
                             bolaprueba.Pos->x, bolaprueba.Pos->y, bolaprueba.Pos->z);
        Vector3d *Np = bolaprueba.Polo->Sphere();
        printf("Polo(r, theta, phi) = (%f, %f, %f)\n", Np->x, Np->y, Np->z);
        printf("Velocidad = %f\n", sqrt(bolaprueba.Vel->Norm2()));
        bolaprueba.iterar_movimiento(0.5, 0.8, 0.1);
    }
    return 0;
}
*/
