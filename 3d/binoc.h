//---------------------------------------------------------------------------
#ifndef binocH
#define binocH
//---------------------------------------------------------------------------
#define X (300)
#define Y (300)
#define MAXTABLA 256
class Binocular
{
 private:
  int distojos;  // Distancia entre los ojos en pixeles
  int doc;       // distojos / 2
  int medx;      // X/2
  int medy;      // Y/2
  int distpant;  // Distancia a la pantalla, en pixeles
  int distfond;  // Profundidad m�xima
  int ImIzq[X][Y];   // Enteros que guardan la profundidad de cada punto
  int ImDer[X][Y];
  int ColIzq[X][Y];  // Enteros que guardan el color de cada punto
  int ColDer[X][Y];  // Luego podr�a trabajar directamente con los bmps
  int TablaColores[MAXTABLA];
  Graphics::TBitmap *bmpder;
  Graphics::TBitmap *bmpizq;
 public:
  void AgregarBmp(Graphics::TBitmap *orig, int px, int py, int prof);
  void AgregarPlano(bool SN[X][Y], int profundidad);
  void AgregarPlano(bool SN[X][Y], int profundidad, int color);
  void AgregarCuadrado(int x1, int y1, int x2, int y2, int profundidad);
  void AgregarCuadrado(int x1, int y1, int x2, int y2, int profundidad,
                                                                     int color);
  void AgregarPunto(int x, int y, int profundidad);
  void AgregarPunto(int x, int y, int profundidad, int color);
  void UnirImagenes(bool ran /*esto es provisorio*/);
  void LlenarFondo(int color = 0);
  void LlenarTabla(void);
  void Inicializar(void);
  void DefinirDistojos(int nueva); // Separaci�n entre los ojos
  void DefinirDistpant(int nueva); // Distancia observador - pantalla
  void DefinirDistfond(int nueva); // Profundidad m�xima
  int MX(void);
  int MY(void);
  int MaxProf(void);
  Graphics::TBitmap *BMPder();
  Graphics::TBitmap *BMPizq();
  Binocular()
  {
    bmpder = new Graphics::TBitmap;
    bmpizq = new Graphics::TBitmap;
  }
};
#endif
