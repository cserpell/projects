#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

class Matriz3x3
{
    double lin[3][3];      // [x][y]
public:
//    double operator [][] (int i, int j) { return lin[i - 1][j - 1]; }
    double EnPos(int x, int y)
    {
        if(x < 1 || x > 3 || y < 1 || y > 3)
            return 0;
        return lin[x - 1][y - 1];
    }
    Matriz3x3(double c1, double c2, double c3, double c4, double c5, double c6,
                                                double c7, double c8, double c9)
    {
        lin[0][0] = c1;
        lin[1][0] = c2;
        lin[2][0] = c3;
        lin[0][1] = c4;
        lin[1][1] = c5;
        lin[2][1] = c6;
        lin[0][2] = c7;
        lin[1][2] = c8;
        lin[2][2] = c9;
    }
    Matriz3x3()
    {
        lin[0][0] = 0;
        lin[1][0] = 0;
        lin[2][0] = 0;
        lin[0][1] = 0;
        lin[1][1] = 0;
        lin[2][1] = 0;
        lin[0][2] = 0;
        lin[1][2] = 0;
        lin[2][2] = 0;
    }
    void Definir_Pos(int x, int y, double val)
    {
        if(x < 1 || x > 3 || y < 1 || y > 3)
            return;
        lin[x - 1][y - 1] = val;
    }
};

class Coord   // ES UN VECTOR DE 3 COORDENADAS, deje de lado la idea de template
{
    double vec[3];
    bool tipo;    // tipo= 1:cartesianas 2:cilindricas 3:esfericas
public:
    Coord()
    {
        vec[0] = 0;
        vec[1] = 0;
        vec[2] = 0;
        tipo = 1;
    }
    Coord(double x, double y, double z, int t = 1)
    {
        tipo = t;
        vec[0] = x;
        vec[1] = y;
        vec[2] = z;
    }
/*    double& operator [] (int i) { return at(i); }
    const double& operator [] (int i) const { return at(i); }*/
    void A_Cilindricas(); // Asumo (radio, angulo, altura)
    void A_Esfericas();   // Asumo (radio, angulo desde eje vert., angulo rot.)
    void A_Cartesianas();
    void Aplicar_Matriz(Matriz3x3 A);
    void Igual_A(Coord b);
    void Sumar(Coord b);
    double x() { return vec[0]; }
    double y() { return vec[1]; }
    double z() { return vec[2]; }
};   // Recomiendo trabajar siempre como cartesianas y aplicar matrices de tran.

void Coord::Igual_A(Coord b)
{
    tipo = b.tipo;
    vec[0] = b.vec[0];
    vec[1] = b.vec[1];
    vec[2] = b.vec[2];
}
void Coord::Sumar(Coord b)
{
    if(tipo == b.tipo)
    {
        vec[0] += b.vec[0];
        vec[1] += b.vec[1];
        vec[2] += b.vec[2];
    }
}

void Coord::A_Cilindricas()
{
    if(tipo == 2)
        return;
}
void Coord::A_Esfericas()
{
    if(tipo == 3)
        return;
    double agr = vec[2];
    double ang = vec[1];
    double rad = vec[0];
    if(tipo == 2)
    {
        vec[0] = sqrt(rad*rad + agr*agr);  // vec[2] no se modifica!
        if(agr != 0)
            vec[1] = atan(rad/agr);
        else
            vec[1] = M_PI_2;
        tipo = 3;
        return;
    }
    vec[0] = sqrt(rad*rad + agr*agr + ang*ang);
    vec[1] = acos(ang/vec[0]);
    if(rad != 0)
        vec[2] = atan(agr/rad);
    else
        vec[2] = M_PI_2;
    tipo = 3;
}
void Coord::A_Cartesianas()
{
    if(tipo == 1)
        return;
    double agr = vec[2];
    double ang = vec[1];
    double rad = vec[0];
    if(tipo == 2)
    {
        vec[0] = rad*cos(ang);
        vec[1] = rad*sin(ang);   // vec[2] no se modifica!
        tipo = 1;
        return;
    }
    vec[0] = rad*sin(ang)*cos(agr);
    vec[1] = rad*cos(ang);
    vec[2] = rad*sin(ang)*sin(agr);
    tipo = 1;
}
void Coord::Aplicar_Matriz(Matriz3x3 A)
{
    double c = vec[2];
    double b = vec[1];
    double a = vec[0];
    vec[0] = A.EnPos(1,1)*a + A.EnPos(2,1)*b + A.EnPos(3,1)*c;
    vec[1] = A.EnPos(1,2)*a + A.EnPos(2,2)*b + A.EnPos(3,2)*c;
    vec[2] = A.EnPos(1,3)*a + A.EnPos(2,3)*b + A.EnPos(3,3)*c;
}

// (x,y) es la posici�n del punto en la pantalla (el pixel)
// Hay un cierto vector que me indica 
class PlanoCorte
{
    Coord *Centro;
    Coord *Normal;
public:
    void Definir_Centro(double x, double y, double z);
    void Definir_Normal(double angteta, double angphi);
    Coord Posicion_Real(Coord Plana);
};

void PlanoCorte::Definir_Centro(double x, double y, double z)
{
    Centro = new Coord(x, y, z);
}
void PlanoCorte::Definir_Normal(double angteta, double angphi)
{
    Normal = new Coord(0, angteta, angphi, 3);  // esf�ricas!
}

Coord PlanoCorte::Posicion_Real(Coord Plana)
{
    Coord res;
    // res = centro + rotarseguntetadelanormal(rotarsegunphidelanormal(plana))
    res.Igual_A(Plana);
    cout << "Plana = (" << res.x() << ", " << res.y() << ", " << res.z() << ")\n";
    res.A_Esfericas();
    cout << "Esfericas = (" << res.x() << ", " << res.y() << ", " << res.z() << ")\n";
    res.Sumar(*Normal);
    cout << "+ Normal = (" << res.x() << ", " << res.y() << ", " << res.z() << ")\n";
    res.A_Cartesianas();
    cout << "Cartesianas = (" << res.x() << ", " << res.y() << ", " << res.z() << ")\n";
    res.Sumar(*Centro);
    cout << "+ Centro = (" << res.x() << ", " << res.y() << ", " << res.z() << ")\n";
    return res;
}

int colordelpunto(unsigned int x, unsigned int y)
{
}

int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        cout << "Uso: proy x y\n";
        return 0;
    }
    PlanoCorte A;
    Coord apro(atof(argv[1]), atof(argv[2]), 0);
    A.Definir_Centro(1,0,0);
    A.Definir_Normal(0, 0);
    apro = A.Posicion_Real(apro);
    cout << "(" << apro.x() << ", " << apro.y() << ", " << apro.z() << ")\n";
    return 0;
}
