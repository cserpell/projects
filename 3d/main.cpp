//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::HacerClick(TObject *Sender)
{
  pbIZQ->Width = Binoc.MX();
  pbDER->Width = Binoc.MX();
  pbIZQ->Height = Binoc.MY();
  pbDER->Height = Binoc.MY();
  //ESTODEBERIAIRSUPUESTAMENTE  pbDER->Left = pbIZQ->Left + distojos;
  pbDER->Left = pbIZQ->Left + pbIZQ->Width - 60;
  Binoc.UnirImagenes(crand->Checked);
  pbIZQPaint(this);
  pbDERPaint(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AzarClick(TObject *Sender)
{
  Binoc.LlenarFondo();
  for(int i = 0; i < 3000; i++)
    Binoc.AgregarPunto(random(Binoc.MX()), random(Binoc.MY()),
                               random(Binoc.MaxProf())/3 + Binoc.MaxProf()*2/3);
/*  for(int i = 0; i < X; i++)
    for(int j = 0; j < Y; j++)
      P[i][j] = false;
  for(int i = 0; i < 1400; i++)
    P[random(Binoc.MX())][random(Binoc.MY())] = true;
  Binoc.AgregarPlano(P, 700);
  for(int i = 0; i < X; i++)
    for(int j = 0; j < Y; j++)
      P[i][j] = false;
  for(int i = 0; i < 1600; i++)
    P[random(Binoc.MX())][random(Binoc.MY())] = true;
  Binoc.AgregarPlano(P, 850);*/
}
//---------------------------------------------------------------------------
void __fastcall TForm1::InitClick(TObject *Sender)
{
  Binoc.Inicializar();
  Binoc.DefinirDistojos(200);
  Binoc.DefinirDistpant(780);
  Binoc.DefinirDistfond(1026);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::pbIZQPaint(TObject *Sender)
{
  pbIZQ->Canvas->Draw(0, 0, Binoc.BMPizq());
}
//---------------------------------------------------------------------------
void __fastcall TForm1::pbDERPaint(TObject *Sender)
{
  pbDER->Canvas->Draw(0, 0, Binoc.BMPder());
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PruClick(TObject *Sender)
{
  bool P[X][Y];

  Binoc.LlenarFondo();
  Binoc.AgregarCuadrado(100, 0, 120, Binoc.MY(), 450);
  Binoc.AgregarCuadrado(58, 100, 143, 200, 700);
  for(int i = 0; i < X; i++)
    for(int j = 0; j < Y; j++)
      P[i][j] = false;
  for(int i = 0; i < Binoc.MY() - 9; i++)
    {
      P[i][i] = true;
      P[i + 1][i] = true;
      P[i + 2][i] = true;
      P[i + 3][i] = true;
      P[i + 4][i] = true;
      P[i + 5][i] = true;
      P[i + 6][i] = true;
      P[i + 7][i] = true;
      P[i + 8][i] = true;
      P[i + 9][i] = true;
    }
  Binoc.AgregarPlano(P, 550);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::OtraPruClick(TObject *Sender)
{
  Binoc.LlenarFondo();
  for(int i = 0; i < Binoc.MX(); i++)
  {
    int n = 700;

    for(int j = 0; j < Binoc.MY(); j++)
    {
      int p = random(4);

      if(p == 0)
        n--;
      else if(p == 1)
        n++;
      else if(p == 2)
        n -= 2;
      else
        n += 2;
      if(n == 0)
        n++;
      if(n > Binoc.MaxProf())
        n = Binoc.MaxProf();
      Binoc.AgregarPunto(i, j, n);
    }
  }
}
//---------------------------------------------------------------------------

