//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "binoc.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
void Binocular::AgregarBmp(Graphics::TBitmap *orig, int px, int py, int prof)
{
  int mmy = py + orig->Height;
  int mmx = px + orig->Width;

  // S�lo lo que est� antes del fondo
  if(prof > distfond)
    return;
  if(py < 0)
    py = 0;
  if(mmy > Y)
    mmy = Y;
  if(mmx > X)
    mmx = X;
  for(int j = py; j < mmy; j++)
  {
    int ny = distpant*(j - medy)/(distpant + prof) + medy;

    if(ny < Y && ny >= 0)
    {
      int *sy = (int *)orig->ScanLine[j - py];

      for(int i = px; i < mmx; i++)
      {
        int nx;

        // IZQ
        nx = distpant*(i + doc - medx)/(distpant + prof) + medx;
        if(nx < X && nx >= 0 && ImIzq[nx][ny] > prof)
        {
          ImIzq[nx][ny] = prof;
          ColIzq[nx][ny] = sy[nx - px];
        }
        // DER
        nx = distpant*(i - doc - medx)/(distpant + prof) + medx;
        if(nx < X && nx >= 0 && ImDer[nx][ny] > prof)
        {
          ImDer[nx][ny] = prof;
          ColDer[nx][ny] = sy[nx - px];
        }
      }
    }
  }

}
//---------------------------------------------------------------------------
void Binocular::AgregarPlano(bool SN[X][Y], int profundidad)
{
  // S�lo lo que est� antes del fondo
  if(profundidad > distfond)
    return;
  for(int j = 0; j < Y; j++)
  {
    int ny = distpant*(j - medy)/(distpant + profundidad) + medy;

    if(ny < Y && ny >= 0)
      for(int i = 0; i < X; i++)
        if(SN[i][j])
        {
          int nx;

          // IZQ
          nx = distpant*(i + doc - medx)/(distpant + profundidad) + medx;
          if(nx < X && nx >= 0 && ImIzq[nx][ny] > profundidad)
          {
            ImIzq[nx][ny] = profundidad;
            ColIzq[nx][ny] = profundidad;  // Deber�a dejarlo negro...
          }
          // DER
          nx = distpant*(i - doc - medx)/(distpant + profundidad) + medx;
          if(nx < X && nx >= 0 && ImDer[nx][ny] > profundidad)
          {
            ImDer[nx][ny] = profundidad;
            ColDer[nx][ny] = profundidad;  // Lo mismo...
          }
        }
  }
}
//---------------------------------------------------------------------------
void Binocular::AgregarPlano(bool SN[X][Y], int profundidad, int color)
{
  // S�lo lo que est� antes del fondo
  if(profundidad > distfond)
    return;
  for(int j = 0; j < Y; j++)
  {
    int ny = distpant*(j - medy)/(distpant + profundidad) + medy;

    if(ny < Y && ny >= 0)
      for(int i = 0; i < X; i++)
        if(SN[i][j])
        {
          int nx;

          // IZQ
          nx = distpant*(i + doc - medx)/(distpant + profundidad) + medx;
          if(nx < X && nx >= 0 && ImIzq[nx][ny] > profundidad)
          {
            ImIzq[nx][ny] = profundidad;
            ColIzq[nx][ny] = color;  // Es lo �nico que cambi� aqu�
          }
          // DER
          nx = distpant*(i - doc - medx)/(distpant + profundidad) + medx;
          if(nx < X && nx >= 0 && ImDer[nx][ny] > profundidad)
          {
            ImDer[nx][ny] = profundidad;
            ColDer[nx][ny] = color;
          }
        }
  }
}
//---------------------------------------------------------------------------
void Binocular::AgregarCuadrado(int x1, int y1, int x2, int y2, int profundidad)
{
  // S�lo lo que est� antes del fondo
  if(profundidad > distfond)
    return;
  if(y1 > y2)
  {
    int t = y1;

    y1 = y2;
    y2 = t;
  }
  if(x1 > x2)
  {
    int t = x1;

    x1 = x2;
    x2 = t;
  }
  if(x1 < 0)
    x1 = 0;
  if(y1 < 0)
    y1 = 0;
  if(x2 < 0)
    x2 = 0;
  if(y2 < 0)
    y2 = 0;
  if(x1 > X)
    x1 = X;
  if(x2 > X)
    x2 = X;
  if(y1 > Y)
    y1 = Y;
  if(y2 > Y)
    y2 = Y;
  for(int j = y1; j < y2; j++)
  {
    int ny = distpant*(j - medy)/(distpant + profundidad) + medy;

    if(ny < Y && ny >= 0)
      for(int i = x1; i < x2; i++)
      {
        int nx;

        // IZQ
        nx = distpant*(i + doc - medx)/(distpant + profundidad) + medx;
        if(nx < X && nx >= 0 && ImIzq[nx][ny] > profundidad)
        {
          ImIzq[nx][ny] = profundidad;
          ColIzq[nx][ny] = profundidad;
        }
        // DER
        nx = distpant*(i - doc - medx)/(distpant + profundidad) + medx;
        if(nx < X && nx >= 0 && ImDer[nx][ny] > profundidad)
        {
          ImDer[nx][ny] = profundidad;
          ColDer[nx][ny] = profundidad;
        }
      }
  }
}
//---------------------------------------------------------------------------
void Binocular::AgregarCuadrado(int x1, int y1, int x2, int y2, int profundidad,
                                                                      int color)
{
  // S�lo lo que est� antes del fondo
  if(profundidad > distfond)
    return;
  if(y1 > y2)
  {
    int t = y1;

    y1 = y2;
    y2 = t;
  }
  if(x1 > x2)
  {
    int t = x1;

    x1 = x2;
    x2 = t;
  }
  if(x1 < 0)
    x1 = 0;
  if(y1 < 0)
    y1 = 0;
  if(x2 < 0)
    x2 = 0;
  if(y2 < 0)
    y2 = 0;
  if(x1 > X)
    x1 = X;
  if(x2 > X)
    x2 = X;
  if(y1 > Y)
    y1 = Y;
  if(y2 > Y)
    y2 = Y;
  for(int j = y1; j < y2; j++)
  {
    int ny = distpant*(j - medy)/(distpant + profundidad) + medy;

    if(ny < Y && ny >= 0)
      for(int i = x1; i < x2; i++)
      {
        int nx;

        // IZQ
        nx = distpant*(i + doc - medx)/(distpant + profundidad) + medx;
        if(nx < X && nx >= 0 && ImIzq[nx][ny] > profundidad)
        {
          ImIzq[nx][ny] = profundidad;
          ColIzq[nx][ny] = color;  // Es lo �nico que cambi� aqu�
        }
        // DER
        nx = distpant*(i - doc - medx)/(distpant + profundidad) + medx;
        if(nx < X && nx >= 0 && ImDer[nx][ny] > profundidad)
        {
          ImDer[nx][ny] = profundidad;
          ColDer[nx][ny] = color;
        }
      }
  }
}
//---------------------------------------------------------------------------
void Binocular::LlenarFondo(int color)
{
  for(int j = 0; j < Y; j++)
  {
    int ny = distpant*(j - medy)/(distpant + distfond) + medy;

    if(ny >= 0 && ny < Y)
      for(int i = 0; i < X; i++)
      {
        int nx;

        // IZQ
        nx = distpant*(i + doc - medx)/(distpant + distfond) + medx;
        if(nx < X && nx >= 0)
        {
          ImIzq[nx][ny] = distfond;
          ColIzq[nx][ny] = color;
        }
        // DER
        nx = distpant*(i - doc - medx)/(distpant + distfond) + medx;
        if(nx < X && nx >= 0)
        {
          ImDer[nx][ny] = distfond;
          ColDer[nx][ny] = color;
        }
      }
  }
}
//---------------------------------------------------------------------------
void Binocular::LlenarTabla(void)
{
  TablaColores[0] = (int)clBlack;

  for(int i = 1; i < MAXTABLA; i++)
  {
/*    for(int j = 0; j < i / 8; j++)
      num *= num;*/
    TablaColores[i] = TablaColores[i-1] + 0x002222;
  }
}
//---------------------------------------------------------------------------
void Binocular::UnirImagenes(bool ran)
{
  // Por ahora lo que voy a hacer es dibujar ambas im�genes separadas
  bmpder->Width = X;
  bmpizq->Width = X;
  bmpder->Height = Y;
  bmpizq->Height = Y;
  bmpder->PixelFormat = pf32bit;
  bmpizq->PixelFormat = pf32bit;
  for(int y = 0; y < Y; y++)
  {
    int *pi;
    pi = (int *)bmpizq->ScanLine[y];
    int *pd;
    pd = (int *)bmpder->ScanLine[y];

    for(int x = 0; x < X; x++)
    {
      if(ran) /* PROVISORIO */
      {
        pi[x] = (int)TablaColores[ColIzq[x][y]%MAXTABLA];//*random(2);
        pd[x] = (int)TablaColores[ColDer[x][y]%MAXTABLA];//*random(2);
      } else
      {
/*        pi[x] = (int)TablaColores[ImIzq[x][y]%MAXTABLA];
        pd[x] = (int)TablaColores[ImDer[x][y]%MAXTABLA];*/ // cuidado aqui abajo
        pi[x] = (x%((int)(((-ImIzq[x][y] + distfond + 1)*10/*distojos/cte*/)/distfond) + 1))?0x000000:0x0000FF;
        pd[x] = (x%((int)(((-ImDer[x][y] + distfond + 1)*10)/distfond) + 1))?0x000000:0x0000FF;
      }
    }
  }
}
//---------------------------------------------------------------------------
void Binocular::Inicializar(void)
{
  LlenarTabla();
  for(int i = 0; i < X; i++)
    for(int j = 0; j < Y; j++)
    {
      ImDer[i][j] = 0;
      ImIzq[i][j] = 0;
      ColDer[i][j] = 0;
      ColDer[i][j] = 0;
    }
  medx = X/2;
  medy = Y/2;
}
//---------------------------------------------------------------------------
void Binocular::AgregarPunto(int x, int y, int profundidad)
{
  int ny;

  if(x < 0 || x >= X || y < 0 || y >= Y || profundidad > distfond)
    return;
  ny = distpant*(y - medy)/(distpant + distfond) + medy;
  if(ny >= 0 && ny < Y)
  {
    int nx;

    // IZQ
    nx = distpant*(x + doc - medx)/(distpant + distfond) + medx;
    if(nx < X && nx >= 0 && ImIzq[nx][ny] > profundidad)
    {
      ImIzq[nx][ny] = profundidad;
      ColIzq[nx][ny] = profundidad;
    }
    // DER
    nx = distpant*(x - doc - medx)/(distpant + distfond) + medx;
    if(nx < X && nx >= 0 && ImDer[nx][ny] > profundidad)
    {
      ImDer[nx][ny] = profundidad;
      ColDer[nx][ny] = profundidad;
    }
  }
}
//---------------------------------------------------------------------------
void Binocular::AgregarPunto(int x, int y, int profundidad, int color)
{
  int ny;

  if(x < 0 || x >= X || y < 0 || y >= Y || profundidad > distfond)
    return;
  ny = distpant*(y - medy)/(distpant + distfond) + medy;
  if(ny >= 0 && ny < Y)
  {
    int nx;

    // IZQ
    nx = distpant*(x + doc - medx)/(distpant + distfond) + medx;
    if(nx < X && nx >= 0 && ImIzq[nx][ny] > profundidad)
    {
      ImIzq[nx][ny] = profundidad;
      ColIzq[nx][ny] = color;
    }
    // DER
    nx = distpant*(x - doc - medx)/(distpant + distfond) + medx;
    if(nx < X && nx >= 0 && ImDer[nx][ny] > profundidad)
    {
      ImDer[nx][ny] = profundidad;
      ColDer[nx][ny] = color;
    }
  }
}
//---------------------------------------------------------------------------
void Binocular::DefinirDistojos(int nueva)
{
  distojos = nueva;
  doc = distojos/2;
}
//---------------------------------------------------------------------------
void Binocular::DefinirDistpant(int nueva)
{
  distpant = nueva;
}
//---------------------------------------------------------------------------
void Binocular::DefinirDistfond(int nueva)
{
  distfond = nueva;
}
//---------------------------------------------------------------------------
Graphics::TBitmap *Binocular::BMPder(void)
{
  Graphics::TBitmap *ret = new Graphics::TBitmap;

  ret->Canvas->Draw(0, 0, bmpder);
  return bmpder;
}
//---------------------------------------------------------------------------
Graphics::TBitmap *Binocular::BMPizq(void)
{
  Graphics::TBitmap *ret = new Graphics::TBitmap;

  ret->Canvas->Draw(0, 0, bmpizq);
  return bmpizq;
}
//---------------------------------------------------------------------------
int Binocular::MX(void)
{
  return X;
}
//---------------------------------------------------------------------------
int Binocular::MY(void)
{
  return Y;
}
//---------------------------------------------------------------------------
int Binocular::MaxProf(void)
{
  return distfond;
}
//---------------------------------------------------------------------------
