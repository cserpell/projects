//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "binoc.h"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TPaintBox *pbIZQ;
        TPaintBox *pbDER;
        TButton *Hacer;
        TButton *Azar;
        TButton *Init;
        TButton *Pru;
        TCheckBox *crand;
        TButton *OtraPru;
        void __fastcall HacerClick(TObject *Sender);
        void __fastcall AzarClick(TObject *Sender);
        void __fastcall InitClick(TObject *Sender);
        void __fastcall pbIZQPaint(TObject *Sender);
        void __fastcall pbDERPaint(TObject *Sender);
        void __fastcall PruClick(TObject *Sender);
        void __fastcall OtraPruClick(TObject *Sender);
private:	// User declarations
        Binocular Binoc;
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
