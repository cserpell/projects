//---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "pru1.h"
// Clip X
int BMP3D::Clipx(int x)
{
  if(x < 0)
    return 0;
  if(x > maxcpx)
    return maxcpx;
  return x;
}
// Clip Y
int BMP3D::Clipy(int y)
{
  if(y < 0)
    return 0;
  if(y > maxcpy)
    return maxcpy;
  return y;
}

// Retorna la proyección de la coordenada en la pantalla
int BMP3D::Proy(int cord, int z)
{
  return (distobs*cord)/(distobs + z);
}

void BMP3D::DibujarLinea(Punto pto1, Punto pto2, int color)
{
}
void BMP3D::DibujarLinea(int pto1, int pto2, int color)
{
}
void BMP3D::DibujarLinea(int x1, int y1, int z1, int x2, int y2, int z2,
                                                                      int color)
{
}
// Agrega un punto con coordenadas (x, y, z)
void BMP3D::AgregarPto(int x, int y, int z, int color)
{
//  Pto[numptos] = new Punto;
  Pto[numptos].x = x;
  Pto[numptos].y = y;
  Pto[numptos].z = z;
  Pto[numptos].color = color;
  numptos++;
}
// Hace una copia del punto
void BMP3D::AgregarPto(Punto pto)
{
//  Pto[numptos] = new Punto;
  Pto[numptos].x = pto.x;
  Pto[numptos].y = pto.y;
  Pto[numptos].z = pto.z;
  Pto[numptos].color = pto.color;
  numptos++;
}
// Agrega y dibuja un punto en (x, y, z)
void BMP3D::DibujarPto(int x, int y, int z, int color)
{
//  Pto[numptos] = new Punto;
  Pto[numptos].x = x;
  Pto[numptos].y = y;
  Pto[numptos].z = z;
  Pto[numptos].color = color;
  numptos++;
  DibujarPto(numptos - 1);
}
// Dibuja el punto en el bmp
void BMP3D::DibujarPto(int pto)
{
  int px, py;

  px = Proy(Pto[pto].x, Pto[pto].z);
  py = Proy(Pto[pto].y, Pto[pto].z);
  px += poscerox;
  py += posceroy;
  px = Clipx(px);
  py = Clipy(py);

  // Dibujo al bmp el punto
  int *p;

  p = (int *)bmp->ScanLine[py];
  p[px] = Pto[pto].color;
}
// Hacie una copia y dibuja el punto
void BMP3D::DibujarPto(Punto pto)
{
//  Pto[numptos] = new Punto;
  Pto[numptos].x = pto.x;
  Pto[numptos].y = pto.y;
  Pto[numptos].z = pto.z;
  Pto[numptos].color = pto.color;
  numptos++;
  DibujarPto(numptos - 1);
}
// Define la distancia del observador a la pantalla (pixeles)
void BMP3D::DefVisDist(int dist)
{
  distobs = dist;
}
// Dibuja Todos los puntos
void BMP3D::DibujarTodo(void)
{
  for(int i = 0; i < numptos; i++)
    DibujarPto(i);
}
// Rota todos los puntos en el eje X
void BMP3D::Rotarx(double a)
{
  AplicarMat(1, 0, 0, 0, cos(a), sin(a), 0, -sin(a), cos(a));
}
// Rota todos los puntos en el eje Y
void BMP3D::Rotary(double a)
{
  AplicarMat(cos(a), 0, -sin(a), 0, 1, 0, sin(a), 0, cos(a));
}
// Rota todos los puntos en el eje Z
void BMP3D::Rotarz(double a)
{
  AplicarMat(cos(a), -sin(a), 0, sin(a), cos(a), 0, 0, 0, 1);
}
// Mueve todos los puntos en el eje X
void BMP3D::MoverX(int cant)
{
  for(int i = 0; i < numptos; i++)
    Pto[i].x += cant;
}
// Mueve todos los puntos en el eje Y
void BMP3D::MoverY(int cant)
{
  for(int i = 0; i < numptos; i++)
    Pto[i].y += cant;
}
// Mueve todos los puntos en el eje Z
void BMP3D::MoverZ(int cant)
{
  for(int i = 0; i < numptos; i++)
    Pto[i].z += cant;
}
// Aplica la matriz a todos los puntos
//    | a  b  c |
//    | d  e  f |
//    | g  h  i |
void BMP3D::AplicarMat(double a, double b, double c,
                     double d, double e, double f, double g, double h, double i)
{
  for(int i = 0; i < numptos; i++)
  {
    int nx, ny, nz;

    nx = a*Pto[i].x + b*Pto[i].y + c*Pto[i].z;
    ny = d*Pto[i].x + e*Pto[i].y + f*Pto[i].z;
    nz = g*Pto[i].x + h*Pto[i].y + i*Pto[i].z;

    Pto[i].x = nx;
    Pto[i].y = ny;
    Pto[i].z = nz;
  }
}
void BMP3D::Resize(int mx, int my)
{
  bmp->Width = mx;
  bmp->Height = my;
  maxcpx = mx - 1;
  maxcpy = my - 1;
  poscerox = mx/2;
  posceroy = my/2;
}
void BMP3D::Llenar(int color)
{
  for(int y = 0; y < maxcpy; y++)
  {
    int *p;

    p = (int *)bmp->ScanLine[y];
    for(int x = 0; x < maxcpx; x++)
      p[x] = color;
  }
}

Graphics::TBitmap *BMP3D::BMP(void)
{
  return bmp;
}

//---------------------------------------------------------------------------
#pragma package(smart_init)
