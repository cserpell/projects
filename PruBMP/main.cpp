//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  Dt.Resize(PaintBox1->Width, PaintBox1->Height);
  Dt.Llenar(256);
  for(int x = 0; x < 1000; x++)
  {
    Dt.AgregarPto(x, x*x, x*x*x, 33);
  }
  Dt.DefVisDist(1000);
  Dt.DibujarTodo();
  PaintBox1Paint(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBox1Paint(TObject *Sender)
{
  PaintBox1->Canvas->Draw(0, 0, Dt.BMP());
}
//---------------------------------------------------------------------------

