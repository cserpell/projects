//---------------------------------------------------------------------------
#ifndef pru1H
#define pru1H
//---------------------------------------------------------------------------
#define MAXPTOS (2048)
typedef struct Punto
{
  int x;
  int y;
  int z;

  // Para despu�s:
  int color;
} Punto;

class BMP3D
{
 private:
  Graphics::TBitmap *bmp;
  Punto Pto[MAXPTOS];
  int numptos;
  int maxcpx;
  int maxcpy;
  int Clipx(int x);
  int Clipy(int y);
  int Proy(int cord, int z);
  int distobs;
  int poscerox;
  int posceroy;
 public:
  Graphics::TBitmap *BMP(void);
  void DibujarLinea(Punto pto1, Punto pto2, int color);
  void DibujarLinea(int pto1, int pto2, int color);
  void DibujarLinea(int x1, int y1, int z1, int x2, int y2, int z2, int color);
  void AgregarPto(int x, int y, int z, int color);
  void AgregarPto(Punto pto);
  void DibujarPto(int x, int y, int z, int color);
  void DibujarPto(int pto);
  void DibujarPto(Punto pto);
  void DefVisDist(int dist);
  void DibujarTodo(void);
  void Rotarx(double a);
  void Rotary(double a);
  void Rotarz(double a);
  void MoverX(int cant);
  void MoverY(int cant);
  void MoverZ(int cant);
  void AplicarMat(double a, double b, double c, double d, double e, double f,
                                                  double g, double h, double i);
  void Resize(int mx, int my);
  void Llenar(int color);
  BMP3D()
  {
    numptos = 0;
    bmp = new Graphics::TBitmap;
  }
};
#endif
