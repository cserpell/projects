unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    PaintBox1: TPaintBox;
    Panel1: TPanel;
    Label1: TLabel;
    UpDown1: TUpDown;
    Edit1: TEdit;
    Label2: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Button1: TButton;
    TrackBar1: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure UpDown1Click(Sender: TObject; Button: TUDBtnType);
    procedure FormResize(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type
  TVertex = record
     x,y,z : Double;
     px,py,pz : Integer;
  end;
  TSide = record
     s,e : Integer;
  end;
  TFace = record
     a,b,c : Integer;
  end;

var
  Form1: TForm1;
  DrwBmp: TBitmap;
  IcoV : Array [0..100000] of TVertex;
  Udir : Array [0..100000] of TVertex;
  Vdir : Array [0..100000] of TVertex;
  IcoS : Array [0..100000] of TSide;
  IcoF : Array [0..100000] of TFace;
  _IcoV : Array [0..11] of TVertex;
  _IcoS : Array [0..29] of TSide = (
   (s:00;e:01),(s:00;e:02),(s:00;e:03),(s:00;e:04),(s:00;e:05),
   (s:01;e:02),(s:02;e:03),(s:03;e:04),(s:04;e:05),(s:05;e:01),
   (s:01;e:07),(s:02;e:06),(s:03;e:10),(s:04;e:09),(s:05;e:08),
   (s:08;e:01),(s:07;e:02),(s:06;e:03),(s:10;e:04),(s:09;e:05),
   (s:08;e:07),(s:07;e:06),(s:06;e:10),(s:10;e:09),(s:09;e:08),
   (s:08;e:11),(s:07;e:11),(s:06;e:11),(s:10;e:11),(s:09;e:11)  );
  _IcoF : Array [0..19] of TFace = (
   (a:00;b:05;c:01),(a:01;b:06;c:02),(a:02;b:07;c:03),(a:03;b:08;c:04),(a:04;b:09;c:00),
   (a:10;b:16;c:05),(a:11;b:17;c:06),(a:12;b:18;c:07),(a:13;b:19;c:08),(a:14;b:15;c:09),
   (a:15;b:10;c:20),(a:16;b:11;c:21),(a:17;b:12;c:22),(a:18;b:13;c:23),(a:19;b:14;c:24),
   (a:20;b:26;c:25),(a:21;b:27;c:26),(a:22;b:28;c:27),(a:23;b:29;c:28),(a:24;b:25;c:29) );
  alpha : Double;
  Beta : Double = 1;
  MaxWidth, MaxHeight : Integer;
  NumFaces  : Integer;
  NumSides  : Integer;
  NumVertex : Integer;
  ExpLevel : Integer;
{  CenterX,CenterY : Integer;}
  MType : Boolean = True;
  DrLines : Boolean = True;
  Zoom : Double;
implementation

{$R *.DFM}


procedure Proyecta( var vertex : TVertex; ca,sa : Double);
var
  tx,ty,tz,tm : Double;
begin
   with vertex do
   begin
      { Primera Rotaci�n }
      tx := x * ca + y * sa;
      ty := y * ca - x * sa;
      tz := z;
      { Segunda Rotaci�n }
      tx := tx;
      tm := ty * Cos(Beta) + tz * Sin(Beta);
      tz := tz * Cos(Beta) - ty * Sin(Beta);
      ty := tm;
      
      px := Round( (MaxWidth div 2)  * (1.1 + Zoom*tx) );
      pz := Round( (MaxHeight div 2) * ( Zoom*ty) +1);
      py := Round( (MaxHeight div 2) * (1.1 - Zoom*tz) );
   end;
end;

function tan( d : Extended) : Extended;
begin
   asm
      fld d
      fptan
      fstp d
      fstp d
   end;
   tan := d;
end;

function Mean( var ver1, ver2 : TVertex; a,b:Integer ) : TVertex;
var
  nv : TVertex;
  d  : Double;
  tx,ty,tz,ca,sa,ap : Double;
begin

  if MType then
  begin
     nv.x := (a*ver2.x + (b-a)*ver1.x)/b;
     nv.y := (a*ver2.y + (b-a)*ver1.y)/b;
     nv.z := (a*ver2.z + (b-a)*ver1.z)/b;
  end
  else
  begin
     tx := (ver2.x + ver1.x)/2;
     ty := (ver2.y + ver1.y)/2;
     tz := (ver2.z + ver1.z)/2;
     ca := Sqrt( Sqr(tx) + Sqr(ty) + Sqr(tz) );
     sa := Sqrt( Sqr(ver1.x - tx) + Sqr(ver1.y - ty) + Sqr(ver1.z - tz) );
     ap := ArcTan( sa / ca );

     ca := ca* tan( ap * (b-2*a)/b );

     nv.x := (ca*ver1.x + (sa-ca)*tx)/sa;
     nv.y := (ca*ver1.y + (sa-ca)*ty)/sa;
     nv.z := (ca*ver1.z + (sa-ca)*tz)/sa;
  end;

  d := Sqrt(Sqr(nv.x)+Sqr(nv.y)+Sqr(nv.z));
  nv.x := nv.x / d;
  nv.y := nv.y / d;
  nv.z := nv.z / d;

  nv.px := 0;
  nv.py := 0;
  nv.pz := 0;

  Mean := nv;

end;

procedure Explode(m : Integer);
var
  i,a,b,c,j,k : Integer;
  v1,v2 : TVertex;
begin
   NumFaces := 20;
   NumSides := 0;
   NumVertex := 12;

   { Pone los vertices b�sicos }
   for i:=0 to 11 do
     IcoV[i] := _IcoV[i];

   { Genera nuevos vertices para cada lado ya existente, dividiendolo en 'm' }
   for i:=0 to 29 do
   begin
      a := _IcoS[i].s;
      b := NumVertex;
      c := _IcoS[i].e;
      { Genera nuevos vertices }
      for j:=1 to (m-1) do
      begin
         IcoV[NumVertex] := Mean(IcoV[a], IcoV[c],j,m);
         Inc(NumVertex);
      end;

      { Genera los 'm' lados }
      IcoS[i*m].s := a;
      for j:=0 to (m-2) do
      begin
         IcoS[i*m+j].e   := b+j;
         IcoS[i*m+j+1].s := b+j;
      end;
      IcoS[i*m+m-1].e := c;

   end;
   NumSides := m * 30;

   { Genera las caras, 'm'^2 por cada cara b�sica }
   for i:=0 to 19 do
   begin
      { En la cara estan los lados antiguos }
      a := _IcoF[i].a;
{      b := _IcoF[i].b;}
      c := _IcoF[i].c;

      for j:=1 to (m-1) do
      begin
         { Busca los nuevos vertices }
         v1 := IcoV[IcoS[a*m+j].s];
         v2 := IcoV[IcoS[c*m+j].s];

{         d := NumVertex;}
         { Crea m�s vertices si es necesario }
         for k:=1 to (j-1) do
         begin
            IcoV[NumVertex] := Mean(v1, v2,k,j);
            Inc(NumVertex);
         end;

         { Y crea nuevos lados conectando los nuevos v�rtices }
{         IcoS[NumSides].s := IcoS[a*m+j].s;
         for k:=0 to (j-2) do
         begin
            IcoS[NumSides].e := d+k;
            Inc(NumSides);
            IcoS[NumSides].s := d+k;
         end;
         IcoS[NumSides].e := IcoS[c*m+j].s;
         Inc(NumSides);
}
      end;
   end;
   NumFaces := 20*m*m;

   { Ahora se crea un eje de cordenadas para cada zona (Vertice) }
{   for i:= to NumVertex - 1 do
   begin}
      { El sistema apunta siempre hacia el}

end;

procedure DrawFaces( Canvas: TCanvas);
var
   i : Integer;
   ca,sa : Double;
begin

   ca := Cos(Alpha);
   sa := Sin(Alpha);

   for i:=0 to NumVertex-1 do
     Proyecta(IcoV[i],ca,sa);


   if DrLines then
   begin
      Canvas.Pen.Color := clBlue;

      for i:=0 to (30*ExpLevel - 1) do
      begin
         if (IcoV[IcoS[i].s].pz > 0) and (IcoV[IcoS[i].e].pz > 0) then
         begin
            Canvas.MoveTo(IcoV[IcoS[i].s].px , IcoV[IcoS[i].s].py);
            Canvas.LineTo(IcoV[IcoS[i].e].px , IcoV[IcoS[i].e].py);
         end;
      end;
   end;

   for i:=0 to NumVertex-1 do
   begin
     if IcoV[i].pz > 0 then
       Canvas.Pixels[IcoV[i].px,IcoV[i].py] := $00AFFFAF;
{     Canvas.Ellipse(IcoV[i].px-2,IcoV[i].py-2,IcoV[i].px+2,IcoV[i].py+2);}
   end;


end;

procedure DrawImage;
begin
   DrwBmp.Canvas.Brush.Color := clBlack;
   DrwBmp.Canvas.FillRect(Rect(0,0,DrwBmp.Width, DrwBmp.Height));
   DrawFaces(DrwBmp.Canvas);
   Form1.PaintBox1.Canvas.Draw(0,0,DrwBmp);

   Form1.Label2.Caption := Format('NumVertex:%d',[NumVertex]);

end;

procedure TForm1.FormCreate(Sender: TObject);
var
   i  : Integer;
   sq : Extended;
begin
   { Genera los vertices de un icosaedro }
   sq := Sqrt(0.2);
   _IcoV[0].x := 0;
   _IcoV[0].y := 0;
   _IcoV[0].z := 1;
   _IcoV[1].x := 0;
   _IcoV[1].y := 2 * sq;
   _IcoV[1].z := sq;
   _IcoV[2].x := 0.5 * Sqrt( 2 + 2 * sq);
   _IcoV[2].y := 0.5 * ( 1 - sq );
   _IcoV[2].z := sq;
   _IcoV[3].x := 0.5 * Sqrt( 2 - 2 * sq);
   _IcoV[3].y := 0.5 * ( - 1 - sq );
   _IcoV[3].z := sq;
   _IcoV[4].x := - _IcoV[3].x;
   _IcoV[4].y :=   _IcoV[3].y;
   _IcoV[4].z :=   _IcoV[3].z;
   _IcoV[5].x := - _IcoV[2].x;
   _IcoV[5].y :=   _IcoV[2].y;
   _IcoV[5].z :=   _IcoV[2].z;
   for i := 6 to 11 do
   begin
      _IcoV[i].x := - _IcoV[11 - i].x;
      _IcoV[i].y := - _IcoV[11 - i].y;
      _IcoV[i].z := - _IcoV[11 - i].z;
   end;

   DrwBmp := TBitmap.Create;
   DrwBmp.Width := PaintBox1.Width;
   DrwBmp.Height := PaintBox1.Height;

   ExpLevel:=8;
   Edit1.Text := IntToStr(ExpLevel);
   Explode(ExpLevel);
   Alpha := 0;
   Timer1.Interval := 100;
   MaxWidth := PaintBox1.Width;
   MaxHeight := PaintBox1.Height;
   PaintBox1.Color := clBlacK;
   Form1.Color := clBlack;
   Zoom := 1;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
   Alpha := Alpha + 0.01;
   Timer1.Interval := 200;
   DrawImage;
end;

procedure TForm1.UpDown1Click(Sender: TObject; Button: TUDBtnType);
begin
   if Button = btNext then
   begin
      Inc(ExpLevel);
      Edit1.Text := IntToStr(ExpLevel);
      Explode(ExpLevel);
      DrawImage;
   end
   else
   begin
      Dec(ExpLevel);
      Edit1.Text := IntToStr(ExpLevel);
      Explode(ExpLevel);
      DrawImage;
   end;

end;

procedure TForm1.FormResize(Sender: TObject);
begin

   DrwBmp.Width := PaintBox1.Width;
   DrwBmp.Height := PaintBox1.Height;

   MaxWidth := PaintBox1.Width * 90 div 100;
   MaxHeight := PaintBox1.Height * 90 div 100;

   DrawImage;

end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin

   if CheckBox1.State = cbUnchecked then
     MType := True
   else
     MType := False;

   Explode(ExpLevel);
   DrawImage;

end;

procedure TForm1.PaintBox1Paint(Sender: TObject);
begin
   Canvas.Draw(0,0,DrwBmp);
end;

procedure TForm1.CheckBox2Click(Sender: TObject);
begin
   DrLines := (CheckBox2.State = cbChecked);
   DrawImage;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
     Zoom := TrackBar1.Position / 2;
end;

end.
