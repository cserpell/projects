// Borland C++ Builder
// Copyright (c) 1995, 1999 by Borland International
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Unit1.pas' rev: 4.00

#ifndef Unit1HPP
#define Unit1HPP

#pragma delphiheader begin
#pragma option push -w-
#include <ComCtrls.hpp>	// Pascal unit
#include <StdCtrls.hpp>	// Pascal unit
#include <ExtCtrls.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <SysUtils.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Unit1
{
//-- type declarations -------------------------------------------------------
class DELPHICLASS TForm1;
#pragma pack(push, 4)
class PASCALIMPLEMENTATION TForm1 : public Forms::TForm 
{
	typedef Forms::TForm inherited;
	
__published:
	Extctrls::TTimer* Timer1;
	Extctrls::TPaintBox* PaintBox1;
	Extctrls::TPanel* Panel1;
	Stdctrls::TLabel* Label1;
	Comctrls::TUpDown* UpDown1;
	Stdctrls::TEdit* Edit1;
	Stdctrls::TLabel* Label2;
	Stdctrls::TCheckBox* CheckBox1;
	Stdctrls::TCheckBox* CheckBox2;
	Stdctrls::TButton* Button1;
	Comctrls::TTrackBar* TrackBar1;
	void __fastcall FormCreate(System::TObject* Sender);
	void __fastcall Timer1Timer(System::TObject* Sender);
	void __fastcall UpDown1Click(System::TObject* Sender, Comctrls::TUDBtnType Button);
	void __fastcall FormResize(System::TObject* Sender);
	void __fastcall CheckBox1Click(System::TObject* Sender);
	void __fastcall PaintBox1Paint(System::TObject* Sender);
	void __fastcall CheckBox2Click(System::TObject* Sender);
	void __fastcall TrackBar1Change(System::TObject* Sender);
public:
	#pragma option push -w-inl
	/* TCustomForm.Create */ inline __fastcall virtual TForm1(Classes::TComponent* AOwner) : Forms::TForm(
		AOwner) { }
	#pragma option pop
	#pragma option push -w-inl
	/* TCustomForm.CreateNew */ inline __fastcall virtual TForm1(Classes::TComponent* AOwner, int Dummy
		) : Forms::TForm(AOwner, Dummy) { }
	#pragma option pop
	#pragma option push -w-inl
	/* TCustomForm.Destroy */ inline __fastcall virtual ~TForm1(void) { }
	#pragma option pop
	
public:
	#pragma option push -w-inl
	/* TWinControl.CreateParented */ inline __fastcall TForm1(HWND ParentWindow) : Forms::TForm(ParentWindow
		) { }
	#pragma option pop
	
};

#pragma pack(pop)

#pragma pack(push, 4)
struct TVertex
{
	double x;
	double y;
	double z;
	int px;
	int py;
	int pz;
} ;
#pragma pack(pop)

#pragma pack(push, 4)
struct TSide
{
	int s;
	int e;
} ;
#pragma pack(pop)

#pragma pack(push, 4)
struct TFace
{
	int a;
	int b;
	int c;
} ;
#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
extern PACKAGE TForm1* Form1;
extern PACKAGE Graphics::TBitmap* DrwBmp;
extern PACKAGE TVertex IcoV[100001];
extern PACKAGE TVertex Udir[100001];
extern PACKAGE TVertex Vdir[100001];
extern PACKAGE TSide IcoS[100001];
extern PACKAGE TFace IcoF[100001];
extern PACKAGE TVertex _IcoV[12];
extern PACKAGE TSide _IcoS[30];
extern PACKAGE TFace _IcoF[20];
extern PACKAGE double alpha;
extern PACKAGE double Beta;
extern PACKAGE int MaxWidth;
extern PACKAGE int MaxHeight;
extern PACKAGE int NumFaces;
extern PACKAGE int NumSides;
extern PACKAGE int NumVertex;
extern PACKAGE int ExpLevel;
extern PACKAGE bool MType;
extern PACKAGE bool DrLines;
extern PACKAGE double Zoom;

}	/* namespace Unit1 */
#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using namespace Unit1;
#endif
#pragma option pop	// -w-

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Unit1
