// created on 15-07-2004 at 14:42
using System;
using System.Windows.Forms;

namespace MyForm {
	public class CreatedForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox2;
		public CreatedForm()
		{
			InitializeComponent();
		}
		
		// THIS METHOD IS MAINTAINED BY THE FORM DESIGNER
		// DO NOT EDIT IT MANUALLY! YOUR CHANGES ARE LIKELY TO BE LOST
		void ButtonClick(object sender, System.EventArgs e)
		{
			cuant.Estado E = new cuant.Estado(2);
			E.Elemento[0].real = Double.Parse(textBox.Text);
			E.Elemento[0].imag = Double.Parse(textBox2.Text);
			E.Elemento[1].real = Double.Parse(textBox3.Text);
			E.Elemento[1].imag = Double.Parse(textBox4.Text);
			E.Normalizar();
			cuant.Operador O = new cuant.Operador(2, 2);
			O.Elemento[0,0] = new cuant.Complejo(0, 0);
			O.Elemento[0,1] = new cuant.Complejo(-1, 0);
			O.Elemento[1,0] = new cuant.Complejo(1, 0);
			O.Elemento[1,1] = new cuant.Complejo(0, 0);
			cuant.Estado N = O.Aplicar(E);
			label.Text = N.Elemento[0].real + " + " + N.Elemento[0].imag + "i";
			label2.Text = N.Elemento[1].real + " + " + N.Elemento[1].imag + "i";
		}
		static public void Main(string[] S)
		{
			//CreatedForm A = new CreatedForm();
			
			//A.Show();
			Application.Run(new CreatedForm());
		}
		void InitializeComponent() {
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.label = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.button = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(56, 48);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(104, 20);
			this.textBox2.TabIndex = 1;
			this.textBox2.Text = "2";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(168, 24);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(104, 20);
			this.textBox3.TabIndex = 5;
			this.textBox3.Text = "3";
			// 
			// textBox
			// 
			this.textBox.Location = new System.Drawing.Point(56, 24);
			this.textBox.Name = "textBox";
			this.textBox.Size = new System.Drawing.Size(104, 20);
			this.textBox.TabIndex = 0;
			this.textBox.Text = "1";
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(168, 48);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(104, 20);
			this.textBox4.TabIndex = 6;
			this.textBox4.Text = "4";
			// 
			// label
			// 
			this.label.Location = new System.Drawing.Point(24, 104);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(256, 23);
			this.label.TabIndex = 3;
			this.label.Text = "label";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 128);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(256, 23);
			this.label2.TabIndex = 4;
			this.label2.Text = "label2";
			// 
			// button
			// 
			this.button.Location = new System.Drawing.Point(80, 72);
			this.button.Name = "button";
			this.button.Size = new System.Drawing.Size(104, 24);
			this.button.TabIndex = 2;
			this.button.Text = "button";
			this.button.Click += new System.EventHandler(this.ButtonClick);
			// 
			// CreatedForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Controls.Add(this.textBox4);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label);
			this.Controls.Add(this.button);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox);
			this.Name = "CreatedForm";
			this.ResumeLayout(false);
		}
	}
}
