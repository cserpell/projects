// created on 20-07-2004 at 23:57
using System;
using System.Windows.Forms;

namespace MyForm {
	public class CreatedForm : System.Windows.Forms.Form
	{
		public CreatedForm()
		{
			InitializeComponent();
		}
		
		// THIS METHOD IS MAINTAINED BY THE FORM DESIGNER
		// DO NOT EDIT IT MANUALLY! YOUR CHANGES ARE LIKELY TO BE LOST
		void InitializeComponent() {
			// 
			// CreatedForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Name = "CreatedForm";
		}
	}
}
