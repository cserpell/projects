//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
int MCD(int a, int b)
{
  if(b > a)
  {
    int t = b;

    b = a;
    a = t;
  }
  do
  {
    int r = a%b;

    a = b;
    b = r;
  } while(b != 0);
  return a;
}
bool Primo(int a)
{
  int cmp;

  if(((a%2) == 0) && (a != 2))
    return false;
  cmp = a/2;
  for(int i = 3; i < cmp; i += 2)
    if((a%i) == 0)
      return false;
  return true;
}
int MCM(int a, int b)
{
  return a*b/MCD(a, b);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButMCDClick(TObject *Sender)
{
  LabMCD->Caption = MCD(StrToInt(Edit1->Text), StrToInt(Edit2->Text));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButPrimoClick(TObject *Sender)
{
  LabPrimo->Caption = Primo(StrToInt(Edit1->Text))?"S�":"No";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButMCMClick(TObject *Sender)
{
  LabMCM->Caption = MCM(StrToInt(Edit1->Text), StrToInt(Edit2->Text));
}
//---------------------------------------------------------------------------
