//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *ButMCD;
        TEdit *Edit1;
        TEdit *Edit2;
        TLabel *LabMCD;
        TButton *ButPrimo;
        TLabel *LabPrimo;
        TButton *ButMCM;
        TLabel *LabMCM;
        void __fastcall ButMCDClick(TObject *Sender);
        void __fastcall ButPrimoClick(TObject *Sender);
        void __fastcall ButMCMClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
