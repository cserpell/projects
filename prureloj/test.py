import math
import random
import tkinter


class Program(object):
    def __init__(self, width=300, height=300, velocity=0.01,
                 mini_rad=15, mini_color='#eeee00'):
        master = tkinter.Tk()
        self.height = height
        self.width = width
        self.window = tkinter.Canvas(master, width=self.width,
                                     height=self.height)
        self.window.pack()
        self.velocity = math.pi / 12  # velocity
        self.theta = 0
        self.rad = 120
        self.mini_rad = mini_rad
        self.loops = random.randint(10, 20)
        pos_x, pos_y = self.get_pos()
        self.oval = self.window.create_oval(
            pos_x - self.mini_rad, pos_y - self.mini_rad,
            pos_x + self.mini_rad, pos_y + self.mini_rad, fill=mini_color)

    def get_pos(self):
        pos_x = self.rad * math.cos(self.theta - math.pi / 2) + self.width / 2
        pos_y = self.rad * math.sin(self.theta - math.pi / 2) + self.height / 2
        return pos_x, pos_y

    def move_circle(self):
        self.theta += self.velocity
        if self.theta > 2 * math.pi:
            self.theta -= 2 * math.pi
            self.loops -= 1
            if self.loops == 0:
                self.window.delete(self.oval)
                return
        pos_x, pos_y = self.get_pos()
        self.window.coords(self.oval,
                           pos_x - self.mini_rad, pos_y - self.mini_rad,
                           pos_x + self.mini_rad, pos_y + self.mini_rad)
        self.window.after(25, self.move_circle)

    def draw_numbers(self):
        position = 100
        for number in range(1, 13):
            pos_x = position * math.cos(math.pi * (number / 6 - 0.5)) + self.width / 2
            pos_y = position * math.sin(math.pi * (number / 6 - 0.5)) + self.height / 2
            self.window.create_text(pos_x, pos_y, text=str(number))

    def initialize(self):
        self.draw_numbers()
        self.move_circle()


if __name__ == '__main__':
    program = Program()
    program.initialize()
    tkinter.mainloop()
