import java.io.*;
import java.util.*;
import java.lang.*;
import java.math.*;

public class raiz {
	public BigInteger Buscado;
	public BigInteger LastBuscado;
	public BigInteger LastAprox;
	public BigInteger Exp;
	public BigInteger Diez;
	public BigInteger Uno;
	public raiz(int buscar) {
		Buscado = new BigInteger((new Integer(buscar)).toString());
		LastBuscado = new BigInteger((new Integer(buscar)).toString());
		Exp = new BigInteger((new Integer(0)).toString());
		Diez = new BigInteger((new Integer(10)).toString());
		Uno = new BigInteger((new Integer(1)).toString());
		int ra = (int)Math.sqrt(buscar);
		LastAprox = new BigInteger((new Integer(ra)).toString());
	}
	public void Iterar() {
		LastAprox = LastAprox.multiply(Diez);
		LastBuscado = LastBuscado.multiply(Diez);
		LastBuscado = LastBuscado.multiply(Diez);
		Exp = Exp.add(Uno);
		if(LastAprox.multiply(LastAprox).compareTo(LastBuscado) == 0)
			return;
		BigInteger LBuscar = LastAprox.add(Diez);
		for(BigInteger BI = LastAprox.add(Uno); BI.compareTo(LBuscar) < 0; BI = BI.add(Uno)) {
			int cc = BI.multiply(BI).compareTo(LastBuscado);
			if(cc > 0)
				return;
			LastAprox = BI;
			if(cc == 0)
				return;
		}
	}
	public String toString() {
		String res = LastAprox.toString();
		int l = Exp.intValue();
		int rl = res.length();
		return res.substring(0, rl - l) + "," + res.substring(rl - l);
	}
	public int dig(int pos) {
		String res = LastAprox.toString();
		int l = Exp.intValue();
		int rl = res.length();
		int npos = rl - l + pos - 1;
		if(npos >= rl)
			return -1;
		return Integer.parseInt(res.substring(pos, pos + 1));
	}
	static public void main(String[] args) {
		raiz RR = new raiz(2);
		for(int i = 0; i <= 105; i++) {
			RR.Iterar();
		}
		System.out.print(RR.dig(100));
		System.out.println("");
		RR = new raiz(2008);
		for(int i = 0; i <= 5005; i++) {
			//System.out.println(RR.toString());
			RR.Iterar();
		}
		//System.out.println(RR.toString());
		System.out.print(RR.dig(5000));
		System.out.println("");
	}
}