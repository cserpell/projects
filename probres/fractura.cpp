#include <cmath>
#include <iostream>

class pieza {
public:
	int px;
	int py;
	int orient;
	pieza(int x, int y, int o) {
		px = x;
		py = y;
		orient = o;
	}
};

int canttotal;
pieza *lpiezas;

void iter(int cant, int **usado, int lpx, int lpy, int mx, int my) {
	if(cant == (mx*my)/2) {
		canttotal++;
		return;
	}
	if(lpx >= mx) {
		lpy++;
		lpx = 0;
	}
	if(lpy >= my)
		return;
	if(usado[lpx][lpy])
		iter(cant, usado, lpx + 1, lpy, mx, my);
	if((lpx != mx - 1) && !usado[lpx + 1][lpy]) {
		/*lpiezas[cant].px = lpx;
		lpiezas[cant].py = lpy;
		lpiezas[cant].orient = o;*/
		usado[lpx][lpy] = 1;
		usado[lpx + 1][lpy] = 1;
		//std::cout << "Puso pieza en " << lpx << "," << lpy << " o = 0" << std::endl;
		iter(cant + 1, usado, lpx + 2, lpy, mx, my);
		usado[lpx][lpy] = 0;
		usado[lpx + 1][lpy] = 0;
	}
	if((lpy != my - 1) && !usado[lpx][lpy + 1]) {
		/*lpiezas[cant].px = lpx;
		lpiezas[cant].py = lpy;
		lpiezas[cant].orient = o;*/
		usado[lpx][lpy] = 1;
		usado[lpx][lpy + 1] = 1;
		//std::cout << "Puso pieza en " << lpx << "," << lpy << " o = 1" << std::endl;
		iter(cant + 1, usado, lpx + 1, lpy, mx, my);
		usado[lpx][lpy] = 0;
		usado[lpx][lpy + 1] = 0;
	}
}

int main() {
	canttotal = 0;
	int mmx = 8;
	int mmy = 8;
	int **usado = new int *[mmx];
	for(int i = 0; i < mmx; i++) {
		usado[i] = new int[mmy];
		for(int j = 0; j < mmy; j++)
			usado[i][j] = 0;
	}
	iter(0, usado, 0, 0, mmx, mmy);
	std::cout << canttotal << std::endl;
	return 0;
}
