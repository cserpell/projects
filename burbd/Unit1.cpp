//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "burb.cpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
        bmp = new Graphics::TBitmap();
        llenado = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
        punto i1, i2, i3, i4;
	double a;
	int nd;

	// Defino el radio de la burbuja cte.
	a = 100;
	// Defino el n�mero de subdivisiones de las caras cte.
	nd = 7;

	// Puntos del tetraedro inicial :
	i1.x =   -sqrt(6)*a/3;
	i1.y =    sqrt(2)*a/3;
	i1.z =            a/3;

	i2.x =    sqrt(6)*a/3;
	i2.y =    sqrt(2)*a/3;
	i2.z =            a/3;

	i3.x =              0;
	i3.y = -2*sqrt(2)*a/3;
	i3.z =            a/3;

	i4.x =              0;
	i4.y =              0;
	i4.z =             -a;

        nump = 0;
	subdivTriang(i1, i2, i4, nd, a);
	subdivTriang(i1, i2, i3, nd, a);
	subdivTriang(i1, i3, i4, nd, a);
	subdivTriang(i3, i2, i4, nd, a);

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
        int clpx, clpy;

        bmp->Width = PaintBox1->Width;
        clpx = (bmp->Width - 1)/2;
        bmp->Height = PaintBox1->Height;
        clpy = (bmp->Height - 1)/2;
        bmp->PixelFormat = pf32bit;

        // Limpio el bmp
        bmp->Canvas->Brush->Color = clBlack;
        bmp->Canvas->Pen->Color = clBlack;
        bmp->Canvas->Rectangle(0, 0, bmp->Width, bmp->Height);
        /*for(int y = 0; y < bmp->Height; y++)
        {
                int *p = (int *)bmp->ScanLine[y];

                for(int x = 0; x < bmp->Width; x++)
                        p[x] = clBlack;
        } */

        // Los dibujo a la pantalla
        for(int i = 0; i < nump; i++)
        {
                punto n;
                int nx, ny, nz;

                n = Dibujarapantalla(Arreglo[i]);
                nz = -n.z;
                if(!CheckBox1->Checked && n.z < 0)
                  // S�lo los que est�n al lado del observador
                  continue;
                n.x = n.x + clpx;
                n.y = n.y + clpy;
                nx = n.x;
                ny = n.y;
                if(nx < 0)
                  continue;
                if(nx >= bmp->Width)
                  continue;
                if(ny < 0)
                  continue;
                if(ny >= bmp->Height)
                  continue;

                int *p;
                p = (int *)bmp->ScanLine[ny];
                p[nx] = (int)nz*512;
                nx++;
        }
        llenado = true;
        PaintBox1Paint(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBox1Paint(TObject *Sender)
{
        if(llenado)
                PaintBox1->Canvas->Draw(0, 0, bmp);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
        double g;

        g = M_PI/90;
        for(int i = 0; i < nump; i++)
                Arreglo[i] = AplicarMat(Arreglo[i], cos(g), -sin(g), 0, sin(g),
                                                            cos(g), 0, 0, 0, 1);

        Button2Click(Sender);
   /*     // Limpio el bmp
        bmp->Canvas->Brush->Color = clBlack;
        bmp->Canvas->Pen->Color = clBlack;
        bmp->Canvas->Rectangle(0, 0, bmp->Width, bmp->Height);

        for(int i = 0; i < nump; i++)
        {
                punto n;
                int nx, ny, nz;

                n = Dibujarapantalla(Arreglo[i]);
                nz = -n.z;
                if(!CheckBox1->Checked && n.z < 0)
                  // S�lo los que est�n al lado del observador
                  continue;
                n.x = n.x + clpx;
                n.y = n.y + clpy;
                nx = n.x;
                ny = n.y;
                if(nx < 0)
                  continue;
                if(nx >= bmp->Width)
                  continue;
                if(ny < 0)
                  continue;
                if(ny >= bmp->Height)
                  continue;

                int *p;
                p = (int *)bmp->ScanLine[ny];
                p[nx] = (int)nz*512;
                nx++;
        }*/
}
//---------------------------------------------------------------------------

