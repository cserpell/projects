/*
// Para llenar el arreglo:
CadaPunto(punto p);
{
  double det = radg/sqrt(p.x*p.x + p.y*p.y + p.z*p.z);

  xfin = det*p.x;
  yfin = det*p.y;
  zfin = det*p.z;
}


// Tengo el arreglo, ahora para dibujar cada punto debo:
Dibujarapantalla(punto p);
{
  double det = distpant/(p.z + distpant);

  xpant = p.x*det;
  ypant = p.y*det;
}

Dibujarburbuja(punto p);
{
  double det = radc/radg;

  xr = p.x*det - xcam;
  yr = p.y*det - ycam;
  zr = -p.z*det - zcam;

  det = radc/sqrt(xr*xr + yr*yr + zr*zr);

  xbur = xr*det;
  ybur = yr*det;
  zbur = zr*det;
}

// Al rotar la c�mara:
// Voy a rotar con centro el centro de todo
RotarEjeX(double ang)
{

}


   */


#include <math.h>

// Encuentra el punto medio entre las coordenadas
double ptomed(double c1, double c2)
{
	return (c1 + c2)/2;
}
// Proyecto el punto (x, y, z) a la esfera de radio r
void proy(double *x, double *y, double *z, double r);
{
	double det = r/sqrt((*x)*(*x) + (*y)*(*y) + (*z)*(*z));

	*x = (*x)*det;
	*y = (*y)*det;
	*z = (*z)*det;
}

// Idea: subdivTriang(punto 1, punto 2, punto 3, n�m. de divs, rad. de proy)
void subdivTriang(double x1, double y1, double z1,
		  double x2, double y2, double z2,
		  double x3, double y3, double z3, int n, double a)
{
	if(n > 1)
	{
		int nn = n - 1;

		// Creo los nuevos puntos...
		nx1 = ptomed(x1, x2);
		ny1 = ptomed(y1, y2);
		nz1 = ptomed(z1, z2);
		nx2 = ptomed(x2, x3);
		ny2 = ptomed(y2, y3);
		nz2 = ptomed(z2, z3);
		nx3 = ptomed(x1, x3);
		ny3 = ptomed(y1, y3);
		nz3 = ptomed(z1, z3);
		// Proyecto los nuevos puntos
		proy(&nx1, &ny1, &nz1, a, r);
		proy(&nx2, &ny2, &nz2, a, r);
		proy(&nx3, &ny3, &nz3, a, r);

		// Subdivido las caras que forme
		subdivTriang(x1, y1, z1, nx1, ny1, nz1, nx3, ny3, nz3, nn);
		subdivTriang(nx1, ny1, nz1, x2, y2, z2, nx2, ny2, nz2, nn);
		subdivTriang(nx1, ny1, nz1, nx2, ny2, nz2, nx3, ny3, nz3, nn);
		subdivTriang(nx2, ny2, nz2, nx3, ny3, nz3, x3, y3, z3, nn);
	} else
	{
        	// Esto tengo que averiguar c�mo se hace...
		// Lo que quiero hacer es agregar a la pantalla una cara
		;// AGREGARCARAAOPENGL(x1, y1, z1, x2, y2, z2, x3, y3, z3, norm)
        }

	return;
}


main()
{
	double ix1, iy1, iz1;
	double ix2, iy2, iz2;
	double ix3, iy3, iz3;
	double ix4, iy4, iz4;
	double a;
	int nd;

	// Defino el radio de la burbuja cte.
	a = 100;
	// Defino el n�mero de subdivisiones de las caras cte.
	nd = 4;

	// Puntos del tetraedro inicial :
	ix1 =   -sqrt(6)*a/3;
	iy1 =    sqrt(2)*a/3;
	iz1 =            a/3;

	ix2 =    sqrt(6)*a/3;
	iy2 =    sqrt(2)*a/3;
	iz2 =            a/3;

	ix3 =              0;
	iy3 = -2*sqrt(2)*a/3;
	iz3 =            a/3;

	ix4 =              0;
	iy4 =              0;
	iz4 =             -a;

	subdivTriang(ix1, iy1, iz1, ix2, iy2, iz2, ix4, iy4, iz4, n, a);
	subdivTriangiix1, iy1, iz1, ix2, iy2, iz2, ix3, iy3, iz3, n, a);
	subdivTriang(ix1, iy1, iz1, ix3, iy3, iz3, ix4, iy4, iz4, n, a);
	subdivTriang(ix3, iy3, iz3, ix2, iy2, iz2, ix4, iy4, iz4, n, a);
}

// ************
// Como puntos:
// ************

// Aqu� defino "punto"
typedef struct punto
{
        double x;
        double y;
        double z;
} punto;

// Encuentra el punto medio entre las coordenadas
double ptomed(double c1, double c2)
{
	return (c1 + c2)/2;
}
// Proyecto el punto (x, y, z) a la esfera de radio r
void proy(punto *p, double r);
{
	double det = r/sqrt(p->x*p->x + p->y*p->y + p->z*p->z);

	p->x = p->x*det;
	p->y = p->y*det;
	p->z = p->z*det;
}

// Idea: subdivTriang(punto 1, punto 2, punto 3, n�m. de divs, rad. de proy)
void subdivTriang(punto p1, punto p2, punto p3, int n, double a)
{
	if(n > 1)
	{
		int nn = n - 1;
                punto n1, n2, n3;

		// Creo los nuevos puntos...
		n1.x = ptomed(p1.x, p2.x);
		n1.y = ptomed(p1.y, p2.y);
		n1.z = ptomed(p1.z, p2.z);
		n2.x = ptomed(p2.x, p3.x);
		n2.y = ptomed(p2.y, p3.y);
		n2.z = ptomed(p2.z, p3.z);
		n3.x = ptomed(p1.x, p3.x);
		n3.y = ptomed(p1.y, p3.y);
		n3.z = ptomed(p1.z, p3.z);
		// Proyecto los nuevos puntos
		proy(&n1, a, r);
		proy(&n2, a, r);
		proy(&n3, a, r);

		// Subdivido las caras que forme
		subdivTriang(p1, n1, n3, nn);
		subdivTriang(n1, p2, n2, nn);
		subdivTriang(n1, n2, n3, nn);
		subdivTriang(n2, n3, p3, nn);
	} else
	{
        	// Esto tengo que averiguar c�mo se hace...
		// Lo que quiero hacer es agregar a la pantalla una cara
		;// AGREGARCARAAOPENGL(p1, p2, p3, norm)
        }

	return;
}



