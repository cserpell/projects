#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <vector>

/* This program assumes input is a loop of the form:
 * for(x1 = L1; x1 <= U1; x1++) {
 *     anything;
 *     anything2;
 *     for(x2 = L2; x2 <= U2; x2++)
 *     {
 *         anything5;
 *         anything6;
 *         for(x3 = L3; x3 <= U3; x3++) {
 *             for(x4 = L4; x4 <= U4; x4++) {
 *                 some statements1;
 *                 some other statements 2;
 *            }
 *         }
 *         anything7;
 *     }
 *     anything3;
 *     anything4;
 * }
 */

using namespace std;

string read_input() {
    stringstream buffer;
    string all_input;
    buffer << cin.rdbuf();
    all_input = buffer.str();
//    cout << "This is the input:" << endl;
//    cout << all_input << endl;
    return all_input;
}

class Condition {
    public:
        string original_for;
        string lower_condition;
        string upper_condition;
        string lower_statement;
        string upper_statement;
        Condition() { }
};

void assertCond(bool condition, const string & message) {
    if (!condition) {
        cerr << message;
        exit(1);
    }
}

string getLowerCond(const string & input, string::size_type start_pos) {
    string::size_type first_pos = input.find("(", start_pos);
    assertCond(first_pos != string::npos, "ERROR: No ( in for statement");
    string::size_type second_pos = input.find(";", first_pos);
    assertCond(second_pos != string::npos, "ERROR: No ; in for statement");
    string almost = input.substr(first_pos + 1, second_pos - first_pos - 1);
    string::size_type uneq = almost.find("=");
    assertCond(uneq != string::npos, "ERROR: Expected equality in for bound");
    return almost.replace(uneq, 1, "==");
}

string getUpperCond(const string & input, string::size_type start_pos) {
    string::size_type first_pos = input.find("(", start_pos);
    assertCond(first_pos != string::npos, "ERROR: No ( in for statement");
    string::size_type second_pos = input.find(";", first_pos);
    assertCond(second_pos != string::npos, "ERROR: No 1 ; in for statement");
    string::size_type third_pos = input.find(";", second_pos + 1);
    assertCond(third_pos != string::npos, "ERROR: No 2 ; in for statement");
    string almost = input.substr(second_pos + 1, third_pos - second_pos - 1);
    string::size_type uneq = almost.find("<=");
    assertCond(uneq != string::npos, "ERROR: Expected unequality in for bound");
    return almost.replace(uneq, 2, "==");
}

string::size_type getNextFor(const string & input, string::size_type start_pos) {
    while (start_pos < input.size()) {
        string::size_type candidate = input.find("for (", start_pos);
        if (candidate == string::npos) {
            candidate = input.find("for(", start_pos);
        }
        if (candidate == string::npos) {
            candidate = input.find("for\t(", start_pos);
        }
        if (candidate == string::npos) {
            candidate = input.find("for\n(", start_pos);
        }
        if (candidate == string::npos) {
            return candidate;
        }
        if (candidate == 0) {
            return candidate;
        }
        char prevchar = input[candidate - 1];
        if (prevchar == ' ' || prevchar == '\t' || prevchar == '\n') {
            return candidate;
        }
        start_pos = candidate + 1;
    }
    return string::npos;
}

string::size_type getNextForClose(const string & input, string::size_type end_pos) {
    return input.rfind("}", end_pos);
}

string indent(int size) {
    string i = "";
    while (size > 0) {
        i = i + "  ";
        size--;
    }
    return i;
}

string indent(int size, const string & input) {
    string i = indent(size);
    string a = i + input;
    string::size_type pos = 0;
    while(true) {
        pos = a.find("\n", pos);
        if (pos == string::npos) {
            break;
        }
        a = a.replace(pos, 1, "\n" + i);
        pos++;
    }
    return a;
}

string cleanSpace(const string & input) {
    string a = input;
    string::size_type pos;
    while(true) {
        pos = a.find(" ");
        if (pos == string::npos || pos > 0) {
            break;
        }
        a = a.substr(1);
    }
    while(true) {
        pos = a.rfind(" ");
        if (pos == string::npos || pos < a.size() - 1) {
            break;
        }
        a = a.substr(0, a.size() - 1);
    }
    return a;
}

string cleanSpaceAndColons(const string & input) {
    string a = cleanSpace(input);
    string::size_type pos;
    while(true) {
        pos = a.find("; ");
        if (pos == string::npos) {
            break;
        }
        a = a.replace(pos, 2, ";\n");
    }
    return a;
}

string improvedString(const string & input) {
    string a = input;
    string::size_type pos;
    while(true) {
        pos = a.find("\n");
        if (pos == string::npos) {
            break;
        }
        a = a.replace(pos, 1, " ");
    }
    while(true) {
        pos = a.find("\r");
        if (pos == string::npos) {
            break;
        }
        a = a.replace(pos, 1, " ");
    }
    while(true) {
        pos = a.find("\t");
        if (pos == string::npos) {
            break;
        }
        a = a.replace(pos, 1, " ");
    }
    while(true) {
        pos = a.find("  ");
        if (pos == string::npos) {
            break;
        }
        a = a.replace(pos, 2, " ");
    }
    return a;
}

void transform(vector<Condition *> & conds,
        const string & input, string::size_type start_pos, string::size_type end_pos) {
//    cout << "Starting transform from pos " << start_pos << " to pos " << end_pos << endl;
    string::size_type next_for = getNextFor(input, start_pos);

    if (next_for == string::npos) {
        // Base case, just print out
        string return_string = "";
        int inner = 0;
        for (vector<Condition *>::iterator it = conds.begin();
                it != conds.end(); it++) {
            cout << indent(inner) << (*it)->original_for << " {" << endl;
            inner++;
        }
        for (vector<Condition *>::iterator it = conds.begin();
                it != conds.end(); it++) {
            if (!(*it)->lower_statement.empty()) {
                cout << indent(inner) << "if (" << (*it)->lower_condition << ") {" << endl;
                cout << indent(inner + 1, (*it)->lower_statement) << endl;
                cout << indent(inner) << "}" << endl;
            }
        }
        string inner_statement = cleanSpaceAndColons(input.substr(start_pos, end_pos - start_pos));
        if (!inner_statement.empty()) {
            cout << indent(inner, inner_statement) << endl;
        }
        for (vector<Condition *>::iterator it = conds.begin();
                it != conds.end(); it++) {
            if (!(*it)->upper_statement.empty()) {
                cout << indent(inner) << "if (" << (*it)->upper_condition << ") {" << endl;
                cout << indent(inner + 1, (*it)->upper_statement) << endl;
                cout << indent(inner) << "}" << endl;
            }
        }
        for (vector<Condition *>::iterator it = conds.begin();
                it != conds.end(); it++) {
            inner--;
            cout << indent(inner) << "}" << endl;
        }
        return;
    }

    string::size_type next_end = getNextForClose(input, end_pos);
    string lower_condition = cleanSpaceAndColons(getLowerCond(input, start_pos));
    string upper_condition = cleanSpaceAndColons(getUpperCond(input, start_pos));
    string lower_statement = cleanSpaceAndColons(input.substr(start_pos, next_for - start_pos));
    string upper_statement = cleanSpaceAndColons(input.substr(next_end + 1, end_pos - next_end - 1));
    Condition * c = new Condition();
    c->lower_condition = "";
    c->upper_condition = "";
    c->lower_statement = lower_statement;
    c->upper_statement = upper_statement;
    conds.push_back(c);
    for (vector<Condition *>::iterator it = conds.begin(); it != conds.end(); it++) {
        if ((*it)->lower_condition.empty()) {
            (*it)->lower_condition = "(" + lower_condition + ")";
        } else {
            (*it)->lower_condition = (*it)->lower_condition + " && (" + lower_condition + ")";
        }
        if ((*it)->upper_condition.empty()) {
            (*it)->upper_condition = "(" + upper_condition + ")";
        } else {
            (*it)->upper_condition = (*it)->upper_condition + " && (" + upper_condition + ")";
        }
    }
    string::size_type next_start_pos = input.find("{", start_pos);
    assertCond(next_start_pos != string::npos, "ERROR: no matching { for for");
    c->original_for = cleanSpace(input.substr(next_for, next_start_pos - next_for));

    transform(conds, input, next_start_pos + 1, next_end - 1);
}

void callTransform(const string & input) {
    string::size_type start_pos = input.find("#pragma scop");
    string::size_type end_pos = input.find("#pragma endscop");
    if (start_pos == string::npos || end_pos == string::npos) {
        cerr << "INPUT ERROR: No #pragma scop or #pragma endscop" << endl;
        return;
    }
    string new_input = input.substr(
            start_pos + 13, end_pos - start_pos - 13);
    vector<Condition *> v;
    cout << input.substr(0, start_pos + 13) << "\n";
    transform(v, improvedString(new_input), 0, new_input.size() - 1);
    cout << "\n" << input.substr(end_pos);
}

int main() {
    string input = read_input();
//    cout << input << endl;
    callTransform(input);
    return 0;
}
