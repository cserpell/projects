#include <functional>
#include <iostream>
#include <vector>

using namespace std;

// Used to define before, after or inner statements.
typedef std::function<void (int * vars)> Statement;

// Used to define lower/upper bounds.
typedef std::function<int (int * vars)> BoundStatement;

// Gets a new statement to include in the inner loop, based on original
// before/after statement and checking conditions given lower/upper bounds.
Statement getFrom(
        vector<BoundStatement> * bounds,
        const Statement & statement,
        int pos,
        int number) {
    Statement s = [bounds, statement, pos, number] (int * vars) {
        for (int i = pos + 1; i < number; i++) {
            if (vars[i] != (*bounds)[i](vars)) {
                // Do nothing, as condition doesn't apply.
                return;
            }
        }
        statement(vars);
    };
    return s;
}

// Returns the new inner statements to be used in the basic loop
// Assuming number of lowerBounds, upperBounds is number,
// and beforeSatements, afterStatements is number - 1
vector<Statement> * getAbuSufahInnerStatements(
        vector<BoundStatement> * lowerBounds,
        vector<BoundStatement> * upperBounds,
        const vector<Statement> & beforeStatements,
        const vector<Statement> & afterStatements,
        const Statement & innerStatement,
        int number) {
    vector<Statement> * returnStatements = new vector<Statement>();
    for (int i = 0; i < number - 1; i++) {  // number should be equivalent to lowerBounds.size()
        returnStatements->push_back(getFrom(lowerBounds, beforeStatements[i], i, number));
    }
    returnStatements->push_back(innerStatement);
    for (int i = number - 2; i >= 0; i--) {  // number should be equivalent to lowerBounds.size()
        returnStatements->push_back(getFrom(upperBounds, afterStatements[i], i, number));
    }
    return returnStatements;
}

// Loop over the original loop, that has before and after conditions.
void executeNotBasicLoop(
        const vector<BoundStatement> & lowerBounds,
        const vector<BoundStatement> & upperBounds,
        const vector<Statement> & beforeStatements,
        const vector<Statement> & afterStatements,
        const Statement & innerStatement,
        int * vars,
        int pos,
        int number) {
    for (vars[pos] = lowerBounds[pos](vars); vars[pos] <= upperBounds[pos](vars); vars[pos]++) {
        if (pos < number - 1) {
            beforeStatements[pos](vars);
            executeNotBasicLoop(lowerBounds, upperBounds, beforeStatements, afterStatements, innerStatement,
                    vars, pos + 1, number);
            afterStatements[pos](vars);
        } else {
            innerStatement(vars);
        }
    }
}

// Loop over the corrected loop, that doesn't have before and after conditions.
void executeBasicLoop(
        const vector<BoundStatement> & lowerBounds,
        const vector<BoundStatement> & upperBounds,
        const vector<Statement> & innerStatements,
        int * vars,
        int pos,
        int number) {
    for (vars[pos] = lowerBounds[pos](vars); vars[pos] <= upperBounds[pos](vars); vars[pos]++) {
        if (pos < number - 1) {
            executeBasicLoop(lowerBounds, upperBounds, innerStatements,
                    vars, pos + 1, number);
        } else {
            for (vector<Statement>::const_iterator it = innerStatements.begin();
                    it != innerStatements.end(); it++) {
                (*it)(vars);
            }
        }
    }
}

// Now, let's try with an example.
int main() {
    // These are the statements of the loop
    vector<BoundStatement> lowerBounds;
    vector<BoundStatement> upperBounds;
    vector<Statement> beforeStatements;
    vector<Statement> afterStatements;
    Statement innerStatement = [](int * vars) {
        cout << "Inner! vars 0,1,2,3: " <<
                vars[0] << "," <<
                vars[1] << "," <<
                vars[2] << "," <<
                vars[3] << endl;
    };
    for (int i = 0; i < 4; i++) {
        lowerBounds.push_back([](int * vars) {
            return 0;
        });
        upperBounds.push_back([i](int * vars) {
            return i;
        });
        beforeStatements.push_back([i](int * vars) {
            cout << "Before statement " << i << endl;
        });
        afterStatements.push_back([i](int * vars) {
            cout << "After statement " << i << endl;
        });
    }

    // Now, let's define the variables.
    int * vars = new int[4];

    // Executing original loop.
    cout << "Now executing original loop" << endl;
    executeNotBasicLoop(
            lowerBounds, upperBounds, beforeStatements, afterStatements, innerStatement,
            vars, 0, 4);

    // Running transformation to get new inner statements.
    cout << "Now running Abu Sufah transformation" << endl;
    vector<Statement> * innerStatements = getAbuSufahInnerStatements(
            &lowerBounds, &upperBounds, beforeStatements, afterStatements, innerStatement,
            4);

    // Execute modified loop. Results must be the same.
    cout << "Now executing modified loop" << endl;
    executeBasicLoop(lowerBounds, upperBounds, *innerStatements, vars, 0, 4);

    return 0;
}
