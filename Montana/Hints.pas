unit Hints;

interface

uses
  SysUtils, Types, Classes, Variants, QGraphics, QControls, QForms, QDialogs,
  QComCtrls, QExtCtrls,

  Solver, QTypes;

type
  TfrmHints = class(TForm)
    StatusBar1: TStatusBar;
    ListView1: TListView;
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListView1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ListView1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    SolverThread: TSolver;
  end;

var
  frmHints: TfrmHints;

implementation

{$R *.xfm}

uses
  Juego, Hash;

procedure TfrmHints.FormShow(Sender: TObject);
begin
  SolverThread.Resume;
  Timer1.Enabled := True;
end;

procedure TfrmHints.FormHide(Sender: TObject);
begin
  Timer1.Enabled := False;
  SolverThread.Suspend;
end;

const
  NumJuegos = 15;

procedure TfrmHints.Timer1Timer(Sender: TObject);
var
  i, j, n, m, p: Integer;
  rt : TRuta;
  MejoresJuegos: Array [0..NumJuegos] of TRuta;
  Itm: TListItem;
  msg : String;
begin
  if not Visible then Exit;
  // Una vez por segundo recorre los juegos encontrados
  // y los escribe...
  n := SolverThread.GetHashCount;
  SolverThread.Suspend;

  StatusBar1.SimplePanel := True;
  StatusBar1.SimpleText := Format('Verificados: %d',[n]);

  for i:=0 to NumJuegos do
  begin
    MejoresJuegos[i].Ptj := 255;
    MejoresJuegos[i].Len := 0;
  end;

  for i := 0 to n-1 do
  begin
    rt := SolverThread.HashData.GetR(i);
    p := rt.Ptj;
    if i <= NumJuegos then
    begin
      m := 0;
      while (m < i) and (p <= MejoresJuegos[m].Ptj) do
        m := m + 1;
      for j := i downto m+1 do
      begin
        MejoresJuegos[j] := MejoresJuegos[j-1];
      end;
      MejoresJuegos[m] := rt;
    end
    else if (p > MejoresJuegos[NumJuegos].Ptj) then
    begin
      m := NumJuegos-1;
      while (m >= 0) and (p > MejoresJuegos[m].Ptj) do
        m := m - 1;
      m := m + 1;
      for j := NumJuegos downto m+1 do
      begin
        MejoresJuegos[j] := MejoresJuegos[j-1];
      end;
      MejoresJuegos[m] := rt;
    end;
  end;

  // Copia a la lista
  ListView1.Items.Clear;
  for i:=0 to NumJuegos do
  begin
    if MejoresJuegos[i].Ptj = 255 then
      Continue;
    Itm := ListView1.Items.Add;
    Itm.Caption := IntToStr( MejoresJuegos[i].Ptj );
    msg := '';
    rt := MejoresJuegos[i];
    Itm.SubItems.Add(IntToStr(rt.Len));
    for j:=0 to rt.Len-1 do
    begin
      p := rt.Data[j];
      msg := msg + Format('(%d,%d)',[(p div 13)+1,(p mod 13)+1]);
    end;
    Itm.SubItems.Add(msg);
    Itm.SubItems.Add(Chr(rt.Data[0]))
  end;

  SolverThread.Resume;
end;

procedure TfrmHints.FormCreate(Sender: TObject);
begin
  SolverThread := TSolver.Create(True);
  SolverThread.FreeOnTerminate := True;
end;

var
  HiCard : Integer = -1;

procedure TfrmHints.ListView1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Itm: TListItem;
begin
  Itm := ListView1.GetItemAt(X,Y);
  if Itm <> nil then
  begin
    if Length(Itm.SubItems[2]) > 0 then
    begin
      HiCard := Ord(Itm.SubItems[2][1]);
      // Hilights the card...
      frmMain.HiLight( HiCard div 13, HiCard mod 13 );
    end;
  end;
end;

procedure TfrmHints.ListView1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if HiCard >= 0 then
  begin
    frmMain.DeLight;
  end;
end;

end.
