unit Hash;

interface

const
  MaxPath = 45; // M�xima profundidad de la b�squeda...

type

  TTipoDato = string[52];
  TRuta = record
    Len: Byte;
    Ptj: Byte;
    Data: Array[0..MaxPath-1] of Byte;
  end;

  THashData = class(TObject)
    private
      fDatos: Array of TTipoDato;
      fTablaHash: Array of Integer;
      fNumDatos: Integer;
      fTotalDatos: Integer;
      fHashSize: Integer;
      function HashOf(const Key: string): Integer;
    public
      fRutas: Array of TRuta;
      procedure Clear;
      function Buscar(Dato: TTipoDato): Integer;
      function Add(Dato: TTipoDato; Ruta: TRuta): Boolean;
      function GetTD(i: Integer): TTipoDato;
      function GetR(i: Integer): TRuta;
      function GetCount: Integer;
      constructor Create;
      destructor Destroy; override;
  end;

implementation

uses SysUtils;

constructor THashData.Create;
begin
  fNumDatos := 0;
  fTotalDatos := 0;
  fHashSize := 0;
  fDatos := nil;
  fRutas := nil;
  fTablaHash := nil;
end;

destructor THashData.Destroy;
begin
  fDatos := nil;
  fRutas := nil;
  fTablaHash := nil;
end;

procedure THashData.Clear;
begin
  fNumDatos := 0;
  fTotalDatos := 0;
  fHashSize := 0;
  fDatos := nil;
  fRutas := nil;
  fTablaHash := nil;
end;

function THashData.HashOf(const Key: string): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 1 to Length(Key) do
    Result := ((Result shl 2) or (Result shr (SizeOf(Result) * 8 - 2)))
              xor (i*Ord(Key[i]));
  if Result < 0 then Result := - Result;
end;

function THashData.GetTD(i: Integer): TTipoDato;
begin
  if (i < 0) or (i >= fNumDatos) then
    raise  Exception.Create('Access out of bounds');
  Result := fDatos[i];
end;

function THashData.GetR(i: Integer): TRuta;
begin
  if (i < 0) or (i >= fNumDatos) then
    raise  Exception.Create('Access out of bounds');
  Result := fRutas[i];
end;

function THashData.Buscar(Dato: TTipoDato): Integer;
var
  k : Integer;
begin
  Result := -1;
  if fHashSize < 1 then
    Exit;
  k := HashOf(Dato) mod fHashSize;
  while (fTablaHash[k] <> -1) do
  begin
    if fDatos[fTablaHash[k]] = Dato then
    begin
      Result := fTablaHash[k];
      Exit;
    end;
    k := (k + 1) mod fHashSize;
  end;
end;

function THashData.Add(Dato: TTipoDato; Ruta: TRuta): Boolean;
var
  i,k : Integer;
begin
  if Buscar(Dato) >= 0 then
  begin
    Result := False;
    Exit;
  end;

  if (fTotalDatos = 0) or (fTotalDatos = fNumDatos) then
  begin
    if fTotalDatos = 0 then
      fTotalDatos := 1024
    else
      fTotalDatos := fTotalDatos * 2;
    SetLength(fDatos,fTotalDatos);
    SetLength(fRutas,fTotalDatos);
  end;

  if fHashSize = 0 then
  begin
    fHashSize := 128*1024 - 17;
    SetLength(fTablaHash,fHashSize);
    for i:=0 to fHashSize - 1 do
      fTablaHash[i] := -1;
  end;

  if fHashSize < (fNumDatos*2) then
  begin
    // Rehace el Hash!
    fHashSize := 4 * fHashSize - 29;
    SetLength(fTablaHash,fHashSize);
    for i:=0 to fHashSize - 1 do
      fTablaHash[i] := -1;
    for i:=0 to fNumDatos-1 do
    begin
      k := HashOf(fDatos[i]) mod fHashSize;
      while (fTablaHash[k] <> -1) do
        k := (k + 1) mod fHashSize;
      fTablaHash[k] := i;
    end;
  end;

  fDatos[fNumDatos] := Dato;
  fRutas[fNumDatos] := Ruta;
  k := HashOf(Dato) mod fHashSize;
  while (fTablaHash[k] <> -1) do
    k := (k + 1) mod fHashSize;
  fTablaHash[k] := fNumDatos;

  fNumDatos := fNumDatos + 1;
  Result := True;
end;

function THashData.GetCount: Integer;
begin
  Result := fNumDatos;
end;

end.
