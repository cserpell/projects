unit Juego;

interface

uses
  SysUtils, Variants, Classes, Types,
  QMenus, QTypes, QForms, QGraphics,
  QControls, QExtCtrls, QComCtrls, QImgList,
  QStdCtrls,
  
  Tablero, Hash;

type
  TfrmMain = class(TForm)
    MainMenu1: TMainMenu;
    mnuGame: TMenuItem;
    mnuNew: TMenuItem;
    mnuExit: TMenuItem;
    mnuOptions: TMenuItem;
    mnuScores: TMenuItem;
    mnuPreferences: TMenuItem;
    StatusBar1: TStatusBar;
    mnuReDeal: TMenuItem;
    pbTablero: TPaintBox;
    ImagenesPintas1: TImageList;
    ImagenesPintas2: TImageList;
    ImagenesPintas3: TImageList;
    ImagenesPintas4: TImageList;
    mnuShowHints: TMenuItem;
    N1: TMenuItem;
    mnuInputGame: TMenuItem;
    procedure StatusBar1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure mnuNewClick(Sender: TObject);
    procedure pbTableroPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mnuExitClick(Sender: TObject);
    procedure pbTableroMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pbTableroMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure mnuReDealClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mnuShowHintsClick(Sender: TObject);
    procedure mnuInputGameClick(Sender: TObject);
  private
    { Private declarations }
    Tablero : TTablero;
    Deals : Integer;
    Juegos : Integer;
    ApI, ApJ : Integer;
    ImagenesPintas: TImageList;
    procedure UpdatePanels;
    procedure DibujaCarta(c: TCanvas; X, Y: Integer; cX, cY: Integer; Carta: TCarta; Modo:Boolean = False);
    procedure BorraLugar(c: TCanvas; X, Y: Integer; cX, cY: Integer);
    procedure DibujaP(X, Y: Integer; cX, cY: Integer);
    procedure DibujaX(X, Y: Integer; cX, cY: Integer);
    procedure OnDrawCard(Sender: TObject; i,j: Integer);
    procedure OnCartasPaint(Sender: TObject);
    procedure OnCartasBorraClick(Sender: TObject);
    procedure OnCartasBlancoClick(Sender: TObject);
    procedure OnCartasMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  public
    { Public declarations }
    procedure HiLight(aI, aJ: Integer);
    procedure DeLight;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  Hints;

var
//  CaracterPinta : Array [ccTrebol..ccDiamante] of String = ( '�', '�', '�', '�' );
  CaracterNumero : Array [1..13] of String =
     ( 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K' );

procedure TfrmMain.UpdatePanels;
var
  p : Integer;
begin
  StatusBar1.Panels[2].Text := 'Deals: ' + IntToStr(Deals);
  // Calcula el puntaje...
  p := Tablero.GetPuntaje;
  StatusBar1.Panels[0].Text := 'Score: ' + IntToStr(p+Juegos*48);

  // Si tiene 48 puntos, ha ganado!
  if p = 48 then
  begin
    Application.MessageBox('Ganaste!!!'#13#10'Continua otro juego.','Ganaste');
    Juegos := Juegos + 1;
    Tablero.Revuelve;
    pbTablero.Invalidate;
    UpdatePanels;
    Exit;
  end;

  // Si no hay lugares vacios, el turno termina!
  if Tablero.GetVacios = 0 then
  begin
    if Deals > 0 then
    begin
      Application.MessageBox('No hay lugares vacios. ','Juego cerrado');
      Deals := Deals - 1;
      Tablero.ReDeal;
      pbTablero.Invalidate;
      UpdatePanels;
      Exit;
    end
    else
    begin
      Application.MessageBox(
         PChar(Format('El juego ha terminado. Su puntaje final es %d',[p+48*Juegos])),
         'Juego finalizado');
    end
  end;
  // Activa el solver...
  frmHints.SolverThread.Reset(Tablero.ExportToTD);
  if mnuShowHints.Checked then
  begin
    if not frmHints.Visible then
      frmHints.Visible := True;
    if frmHints.SolverThread.Suspended then
      frmHints.SolverThread.Suspended := False;
  end;
end;

procedure TfrmMain.StatusBar1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
 i : Integer;
begin
  i := StatusBar1.Panels[0].Width + StatusBar1.Panels[1].Width +
       StatusBar1.Panels[2].Width;
  if (X >= i) and (X < (i+StatusBar1.Panels[3].Width)) then
  begin
    mnuReDeal.Click;
  end;
end;


procedure TfrmMain.mnuNewClick(Sender: TObject);
begin
  Deals := 3;

  Tablero.Revuelve;
  pbTablero.Invalidate;
  UpdatePanels;
end;


procedure TfrmMain.BorraLugar(c: TCanvas; X, Y: Integer; cX, cY: Integer);
begin
  c.Brush.Color := Color;
  c.Brush.Style := bsSolid;
  c.FillRect(Rect(X,Y,X+cX-1,Y+cY-1));
end;

procedure TfrmMain.DibujaX(X, Y: Integer; cX, cY: Integer);
var
  w : Integer;
begin
  with pbTablero.Canvas do
  begin
    Pen.Color := clRed;
    w := cX div 8;
    if w < 1 then w := 1;
    Pen.Width := w;
    Brush.Style := bsSolid;
    MoveTo(X+1+w,Y+1+w);
    LineTo(X+cX-2-w,Y+cY-2-w);
    MoveTo(X+cX-2-w,Y+1+w);
    LineTo(X+1+w,Y+cY-2-w);
    Pen.Width := 0;
  end;
end;

procedure TfrmMain.DibujaP(X, Y: Integer; cX, cY: Integer);
var
  w : Integer;
begin
  with pbTablero.Canvas do
  begin
    Pen.Color := clGray;
    w := cX div 8;
    if w < 1 then w := 1;
    Pen.Width := w;
    Brush.Style := bsSolid;
    MoveTo(X+1+w,Y+1+w);
    LineTo(X+cX-2-w,Y+cY-2-w);
    MoveTo(X+cX-2-w,Y+1+w);
    LineTo(X+1+w,Y+cY-2-w);
    Pen.Width := 0;
  end;
end;

procedure TfrmMain.DibujaCarta(c: TCanvas; X, Y: Integer; cX, cY: Integer; Carta: TCarta; Modo: Boolean = False);
  procedure TextCenter( xx, yy : Integer; s: String);
  var
    w,h: Integer;
  begin
    w := c.TextWidth(s);
    h := c.TextHeight(s);
    c.TextOut(xx-(w div 2),yy-(h div 2),s);
  end;
begin
  with c do
  begin
    if Modo then
      Brush.Color := clGray
    else
      Brush.Color := clWhite;
    Brush.Style := bsSolid;
    RoundRect(X+1,Y+1,X+cX-1,Y+cY-1,cX div 2,cY div 4);

    ImagenesPintas.Draw(c,X+(cX-ImagenesPintas.Width) div 2,
                        Y+cY div 4-ImagenesPintas.Height div 2, Ord(Carta.Pinta));

    TextCenter(X+cX div 2,Y+cY*3 div 4,CaracterNumero[Carta.Numero]);

    Brush.Style := bsClear;
    Pen.Style := psSolid;
    Pen.Color := clBlack;
    RoundRect(X+1,Y+1,X+cX-1,Y+cY-1,cX div 2,cY div 4);
  end;
end;

procedure TfrmMain.OnDrawCard(Sender: TObject; i,j: Integer);
var
  c : TCarta;
  cX,cY : Integer;
begin
  // Dibuja la carta que cambi�
  cX := (pbTablero.Width - 2) div 13;
  cY := (pbTablero.Height - 2) div 4;

  c := Tablero[j,i];

  BorraLugar(pbTablero.Canvas,j*cX+1,i*cY+1,cX,cY);

  if c.Numero = 0 then
    DibujaX(j*cX+1,i*cY+1,cX,cY)
  else if c.Numero > 13 then
    DibujaP(j*cX+1,i*cY+1,cX,cY)
  else if c.Numero > 1 then
    DibujaCarta(pbTablero.Canvas, 1+cX * j, 1+cY * i, cX, cY, c );

end;

procedure TfrmMain.pbTableroPaint(Sender: TObject);
var
  cX, cY : Integer;
  i,j : Integer;
  R : TRect;
  c : TCarta;
begin
  // Dibuja el tablero de juego...
  cX := (pbTablero.Width - 2) div 13;
  cY := (pbTablero.Height - 2) div 4;
  R := pbTablero.BoundsRect;
  Frame3D( pbTablero.Canvas,R,cl3DDkShadow,cl3DLight,1  );
  with pbTablero.Canvas do
  begin
    pbTablero.Canvas.Font.Size := (cY div 4)+2;
    while (TextWidth('10') > (cX-3)) or
          (TextHeight('0123456789') > ((cY div 2)-3)) do
    begin
      Font.Size := Font.Size - 1;
      if Font.Size <= 0 then
      begin
        Font.Size := 8;
        Break;
      end;
    end;
  end;

  // Elige el tama�o correcto de las pintas...
  ImagenesPintas := ImagenesPintas1;
  if (ImagenesPintas.Width > (cX-3)) or (ImagenesPintas.Height > ((cY div 2)-3)) then
    ImagenesPintas := ImagenesPintas2;
  if (ImagenesPintas.Width > (cX-3)) or (ImagenesPintas.Height > ((cY div 2)-3)) then
    ImagenesPintas := ImagenesPintas3;
  if (ImagenesPintas.Width > (cX-3)) or (ImagenesPintas.Height > ((cY div 2)-3)) then
    ImagenesPintas := ImagenesPintas4;

  for i:=0 to 3 do
  begin
    for j:=0 to 12 do
    begin
      c := Tablero[j,i];
      if c.Numero = 0 then
        DibujaX(j*cX+1,i*cY+1,cX,cY)
      else if c.Numero > 13 then
        DibujaP(j*cX+1,i*cY+1,cX,cY)
      else if c.Numero > 1 then
        DibujaCarta( pbTablero.Canvas, 1+cX * j, 1+cY * i, cX, cY, Tablero[j,i] )
    end;
  end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  Deals := 3;
  ApI := -1;
  ApJ := -1;
  Tablero.Revuelve;
  UpdatePanels;
end;

procedure TfrmMain.mnuExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.HiLight(aI,aJ: Integer);
var
  cX,cY: Integer;
begin
  if (aI >= 0) and (aJ >= 0) then
  begin
    ApI := aI;
    ApJ := aJ;
    cX := (pbTablero.Width - 2) div 13;
    cY := (pbTablero.Height - 2) div 4;
    DibujaCarta(pbTablero.Canvas,cX*ApJ+1,cY*ApI+1,cX,cY,Tablero[ApJ,ApI], True);
  end;
end;

procedure TfrmMain.DeLight;
var
  cX,cY: Integer;
begin
  if (ApI >= 0) and (ApJ >= 0) then
  begin
    cX := (pbTablero.Width - 2) div 13;
    cY := (pbTablero.Height - 2) div 4;
    DibujaCarta(pbTablero.Canvas,cX*ApJ+1,cY*ApI+1,cX,cY,Tablero[ApJ,ApI]);
    ApI := -1;
    ApJ := -1;
  end;
end;

procedure TfrmMain.pbTableroMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  cX,cY : Integer;
  i,j,pi,pj : Integer;
  c : TCarta;
begin
  cX := (pbTablero.Width - 2) div 13;
  cY := (pbTablero.Height - 2) div 4;
  pj := (X - 1) div cX;
  pi := (Y - 1) div cY;

  // Ve si es necesario des-apretar
  DeLight;

  // Verifica que carta est� en el lugar del mouse
  if Tablero[pj,pi].Numero = 1 then
  begin
    // Es un lugar vacio, pinta la carta corespondiente
    if pj = 0 then
    begin
      // Busca cualquier "2"
      for i:=0 to 3 do
        for j:=1 to 12 do
      begin
          if Tablero[j,i].Numero = 2 then
          begin
            HiLight(i,j);
            Exit;
          end;
      end;
    end
    else if Tablero[pj-1,pi].Numero = 1 then
    begin
      // No hay cartas a la izquierda
    end
    else if Tablero[pj-1,pi].Numero = 13 then
    begin
      // Es un rey...
    end
    else
    begin
      c := Tablero[pj-1,pi];
      c.Numero := c.Numero + 1;
      for i:=0 to 3 do
        for j:=0 to 12 do
      begin
          if (Tablero[j,i].Pinta = c.Pinta) and
             (Tablero[j,i].Numero = c.Numero) then
          begin
            HiLight(i,j);
            Exit;
          end;
      end;
    end;
  end
  else
  begin
    // Intenta jugar...
    if Tablero.Juega(pi,pj) then
    begin
      UpdatePanels;
    end;
  end;
end;

procedure TfrmMain.pbTableroMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  // Ve si es necesario des-apretar
  DeLight;
end;

procedure TfrmMain.mnuReDealClick(Sender: TObject);
begin
  if Deals > 0 then
  begin
    Tablero.ReDeal;
    Deals := Deals - 1;
    pbTablero.Invalidate;
    UpdatePanels;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  Tablero := TTablero.Create;
  Tablero.OnDrawCard := OnDrawCard;
  ImagenesPintas := ImagenesPintas3;
end;

procedure TfrmMain.mnuShowHintsClick(Sender: TObject);
begin
  if mnuShowHints.Checked then
  begin
    // Desactiva el Solver...
    mnuShowHints.Checked := False;
    frmHints.Hide;
  end
  else
  begin
    // Activa el Solver!
    mnuShowHints.Checked := True;
    frmHints.Show;
  end;
end;

var
  Cartas: Array [0..47] of TCarta;
  CartasCx, CartasCy: Integer;
  IngresaN: Integer;

procedure TfrmMain.OnCartasPaint(Sender: TObject);
var
  i : Integer;
  cX,cY : Integer;
  frm : TForm;
begin
  frm := Sender as TForm;
  cX := CartasCx;
  cY := CartasCy;
  // Dibuja las cartas...
  for i:=0 to 47 do
  begin
    if Cartas[i].Numero > 1 then
      DibujaCarta(frm.Canvas,1+(i mod 12)*cX,1+(i div 12)*cY,cX,cY,Cartas[i])
    else
      BorraLugar(frm.Canvas,1+(i mod 12)*cX,1+(i div 12)*cY,cX,cY);
  end;
  // Y las indicaciones
  frm.Canvas.TextOut(132,4*cY+3,'Presione las cartas en �rden');
end;

procedure TfrmMain.OnCartasBorraClick(Sender: TObject);
var
  n : Integer;
  c : TCarta;
begin
  if IngresaN > 0 then
  begin
    IngresaN := IngresaN - 1;
    c := Tablero.Carta[ IngresaN mod 13, IngresaN div 13 ];
    if c.Numero > 1 then
    begin
      n := (c.Numero - 2) + Ord(c.Pinta) * 12;
      Cartas[n] := c;
    end;
    c.Numero := 14;
    Tablero.Carta[IngresaN mod 13, IngresaN div 13] := c;
    (Sender as TButton).Parent.Invalidate;
  end;
end;

procedure TfrmMain.OnCartasBlancoClick(Sender: TObject);
var
  c : TCarta;
begin
  c.Numero := 1;
  Tablero.Carta[ IngresaN mod 13, IngresaN div 13 ] := c;
  IngresaN := IngresaN + 1;
  if IngresaN > 51 then
  begin
    (Sender as TButton).Parent.Free;
    Enabled := True;
    Invalidate;
    UpdatePanels;
  end;
end;

procedure TfrmMain.OnCartasMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  xx,yy,n : Integer;
begin
  xx := (X-1) div CartasCx;
  yy := (Y-1) div CartasCy;
  if (xx >= 0) and (xx < 12) and (yy >= 0) and (yy <4) then
  begin
    n := xx + yy * 12;
    if Cartas[n].Numero > 1 then
    begin
      Tablero.Carta[ IngresaN mod 13, IngresaN div 13 ] := Cartas[n];
      Cartas[n].Numero := 1;
      IngresaN := IngresaN + 1;
      (Sender as TForm).Invalidate;
      if IngresaN > 51 then
      begin
        Sender.Free;
        Enabled := True;
        Invalidate;
        UpdatePanels;
      end;
    end;
  end;

end;

procedure TfrmMain.mnuInputGameClick(Sender: TObject);
var
  frm : TForm;
  btn : TButton;
  i : Integer;
  c : TCarta;
  cX,cY : Integer;
begin
  // Permite ingresar un juego...
  frmHints.Hide;
  Application.ProcessMessages;

  // Inicializa la lista...
  for i:=0 to 47 do
  begin
    c.Numero := 2 + (i mod 12);
    c.Pinta := TPintaCarta(i div 12);
    Cartas[i] := c;
  end;
  for i:=0 to 51 do
  begin
    c.Numero := 14;
    Tablero.Carta[i mod 13, i div 13] := c;
  end;
  IngresaN := 0;

  // Crea una forma con cartas...
  frm := TForm.CreateNew(self);
  frm.Caption := 'Cartas Disponibles';
  frm.Color := Color;
  cX := (pbTablero.Width - 2) div 13;
  cY := (pbTablero.Height - 2) div 4;
  CartasCx := cX;
  CartasCy := cY;
  frm.ClientWidth := 2 + cX * 12;
  frm.ClientHeight := 2 + cY * 4 + frm.Canvas.Font.Height + 8;
  frm.BorderStyle := fbsDialog;
  frm.Canvas.Font.Color := clBlack;
  frm.OnPaint := OnCartasPaint;
  frm.OnMouseDown := OnCartasMouseDown;

  btn := TButton.Create(frm);
  btn.Parent := frm;
  btn.Top := 2 + cY * 4;
  btn.Caption := 'Borrar';
  btn.Left := 0;
  btn.Width := 64;
  btn.Height := frm.Canvas.Font.Height + 8;
  btn.OnClick := OnCartasBorraClick;

  btn := TButton.Create(frm);
  btn.Parent := frm;
  btn.Top := 2 + cY * 4;
  btn.Caption := 'Blanco';
  btn.Left := 64;
  btn.Width := 64;
  btn.Height := frm.Canvas.Font.Height + 8;
  btn.OnClick := OnCartasBlancoClick;

  frm.Show;

  self.Enabled := false;

end;
end.
