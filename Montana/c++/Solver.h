// unit Solver


#ifndef _SOLVER_H_
#define _SOLVER_H_

#include <Classes.hpp>

#include "Hash.h"

class TSolver: public TThread
{
 private:
   bool fReset;
   TTipoDato fNuevoTablero;
   
   // Semaphores...
   TMultiReadExclusiveWriteSynchronizer *ResetSync;
   TMultiReadExclusiveWriteSynchronizer *HashSync;

   void Solve(TTipoDato Tablero);
 public:
   THashData HashData;

   int GetHashCount();
   void Reset(TTipoDato Tablero);
   virtual void __fastcall Execute(void);
   TSolver(bool CreateSuspended) : TThread(CreateSuspended) {
      ResetSync = new TMultiReadExclusiveWriteSynchronizer();
      HashSync = new TMultiReadExclusiveWriteSynchronizer();
   };
};


#endif // _SOLVER_H_