unit Tablero;

interface

uses
  Hash;

type
  TPintaCarta = ( ccTrebol, ccDiamante, ccCorazon, ccHoja );
  TNumeroCarta = 0..14;

  TCarta = record
    Pinta: TPintaCarta;
    Numero: TNumeroCarta;
  end;

  TDrawCardEvent = procedure (Sender: TObject; i,j: Integer) of object;

  TTablero = class(TObject)
    private
      Data: Array[0..3,0..12] of TCarta;
      CartaInvalida: TCarta;
      fDrawCard: TDrawCardEvent;
      function GetCarta(j,i: Integer): TCarta;
      procedure SetCarta(j,i: Integer; c: TCarta);
      procedure Swap(i,j,k,l: Integer);
    public
      procedure Revuelve;
      procedure ReDeal;
      procedure ImportFromTD( Dato: TTipoDato );
      function ExportToTD: TTipoDato;
      function Juega(pi,pj:Integer): Boolean;
      function GetPuntaje: Integer;
      function GetVacios: Integer;
      constructor Create;
      property Carta[j,i:Integer]: TCarta read GetCarta write SetCarta; default;
    published
      property OnDrawCard: TDrawCardEvent write fDrawCard;
  end;

implementation

uses SysUtils;

constructor TTablero.Create;
var
  i,j : Integer;
  c : TCarta;
begin
  for i:=0 to 3 do
    for j:=0 to 12 do
  begin
    c.Pinta := TPintaCarta(i);
    c.Numero := j + 1;
    Data[i,j] := c;
  end;
  CartaInvalida.Numero := 0;
  CartaInvalida.Pinta := ccTrebol;
end;

function TTablero.GetCarta(j,i: Integer):TCarta;
var
  c : TCarta;
begin
  if (i<0) or (i>3) or (j<0) or (j>12) then
    raise  Exception.Create('Access out of bounds');
  Result := Data[i,j];
  if (Result.Numero = 1) and (j > 0) then
  begin
    c := GetCarta(j-1,i);
    if (c.Numero = 13) or (c.Numero = 0) then
      Result := CartaInvalida;
  end;
end;

procedure TTablero.SetCarta(j,i: Integer; c:TCarta);
  procedure CallEvent(i,j: Integer);
  var
    k : Integer;
  begin
    fDrawCard(self,i,j);
    for  k := j+1 to 12 do
    begin
      if Data[i,k].Numero = 1 then
        fDrawCard(self,i,k)
      else
        Exit;
    end;
  end;
begin
  if (i<0) or (i>3) or (j<0) or (j>12) then
    raise  Exception.Create('Access out of bounds');
  Data[i,j] := c;
  if Assigned( fDrawCard ) then
    CallEvent(i,j);
end;

procedure TTablero.Swap(i,j,k,l: Integer);
var
  c : TCarta;
  procedure CallEvent(i,j: Integer);
  begin
    fDrawCard(self,i,j);
    if j < 12 then
      if Data[i,j+1].Numero = 1 then
      begin
        fDrawCard(self,i,j+1);
        if j < 11 then
          if Data[i,j+2].Numero = 1 then
          begin
            fDrawCard(self,i,j+2);
            if j < 10 then
              if Data[i,j+3].Numero = 1 then
                fDrawCard(self,i,j+2);
          end;
      end;
  end;
begin
  c := Data[i,j];
  Data[i,j] := Data[k,l];
  Data[k,l] := c;
  if Assigned( fDrawCard ) then
  begin
    CallEvent(i,j);
    CallEvent(k,l);
  end;
end;

procedure TTablero.ReDeal;
var
  i,j,k,l : Integer;
  Ocupa: Array[0..3] of Integer;
begin
  // Calcula las inm�viles
  for i:=0 to 3 do
  begin
    j:=0;
    while (j < 12) do
    begin
      if (Data[i,j].Pinta <> Data[i,0].Pinta) or
         (Data[i,j].Numero - 2 <> j) then
        Break;
      j := j + 1;
    end;
    Ocupa[i] := j;
  end;

  if (Ocupa[0] + Ocupa[1] + Ocupa[2] + Ocupa[3]) = 0 then
  begin
    Exit;
  end;

  // Mueve los espacios
  k:=0;
  for i:=0 to 3 do
  begin
    for j:=Ocupa[i]+1 to 12 do
    begin
      if Data[i,j].Numero = 1 then
      begin
        Swap(i,j,k,Ocupa[k]);
        k := k + 1;
      end;
    end;
  end;

  // Revuelve el resto
  for i:=0 to 3 do
    for j:=Ocupa[i]+1 to 12 do
  begin
    repeat
      k := Random(3);
      l := Random(13);
    until (l > Ocupa[k]);
    Swap(i,j,k,l);
  end;
end;

procedure TTablero.Revuelve;
var
  i,j,k,l : Integer;
begin
  for i:=0 to 3 do
    for j:=0 to 12 do
  begin
    k := Random(3);
    l := Random(13);
    Swap(i,j,k,l);
  end;
end;

function TTablero.Juega(pi,pj: Integer): Boolean;
var
  i,j,ni,nj:Integer;
  c : TCarta;
begin
  // Juega una carta, returna False en caso de error.
  if Data[pi,pj].Numero = 1 then
  begin
    Result := False;
    Exit;
  end
  else if Data[pi,pj].Numero = 2 then
  begin
    // Un "2", ve si hay un vacio al principio...
    ni := -1;
    nj := -1;
    for i:=0 to 3 do
    begin
      if (Data[i,0].Numero = 1) then
      begin
        ni := i;
        nj := 0;
      end;
    end;
    if (ni >= 0) and (nj >= 0) then
    begin
      Swap(pi,pj,ni,nj);
      Result := True;
    end
    else
      Result := False;
  end
  else
  begin
    // Verifica si existe un lugar para esta carta...
    ni := -1;
    nj := -1;
    c := Data[pi,pj];
    c.Numero := c.Numero-1;
    for i:=0 to 3 do
      for j:=1 to 12 do
    begin
      if (Data[i,j].Numero = 1) and
         (Data[i,j-1].Numero = c.Numero) and
         (Data[i,j-1].Pinta = c.Pinta) then
      begin
        ni := i;
        nj := j;
      end;
    end;
    if (ni >= 0) and (nj >= 0) then
    begin
      Swap(pi,pj,ni,nj);
      Result := True;
    end
    else
      Result := False;
  end;
end;

function TTablero.GetPuntaje: Integer;
var
  i,j,p: Integer;
begin
  // Calcula el puntaje...
  p := 0;
  for i:=0 to 3 do
  begin
    j:=0;
    while (j < 12) do
    begin
      if (Data[i,j].Pinta <> Data[i,0].Pinta) or
         (Data[i,j].Numero - 2 <> j) then
        Break;
      p := p + 1;
      j := j + 1;
    end;
  end;
  Result := p;
end;

function TTablero.GetVacios: Integer;
var
  i,j,v,l: Integer;
begin
  v := 0;
  for i:=0 to 3 do
  begin
    l := 1;
    for j:=0 to 12 do
    begin
      if Data[i,j].Numero > 1 then
        l := Data[i,j].Numero
      else if l <> 13 then
        v := v + 1;
    end;
  end;
  Result := v;
end;

procedure TTablero.ImportFromTD(Dato: TTipoDato);
var
  p,i,j,k,l: Integer;
  c : TCarta;
begin
  k := 1;
  p := 0;
  for i:=0 to 3 do
    for j:=0 to 12 do
  begin
    if Dato[k] <> ' ' then
    begin
      l := Ord(Dato[k])-Ord('@');
      c.Numero := 1 + (l mod 13);
      c.Pinta := TPintaCarta(l div 13);
    end
    else
    begin
      c.Numero := 1;
      c.Pinta := TPintaCarta(p);
      p := p + 1;
    end;
    Data[i,j] := c;
    k := k + 1;
  end;
end;

function TTablero.ExportToTD: TTipoDato;
var
  i,j,k,l: Integer;
begin
  k := 1;
  Result[0] := Chr(52);
  for i:=0 to 3 do
    for j:=0 to 12 do
  begin
    if Data[i,j].Numero = 1 then
      Result[k] := ' '
    else
    begin
      l := (Data[i,j].Numero - 1) + Ord(Data[i,j].Pinta) * 13;
      Result[k] := Chr( l + Ord('@') );
    end;
    k := k + 1;
  end;
end;

end.
