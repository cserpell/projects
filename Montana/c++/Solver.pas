unit Solver;

interface

uses
  SysUtils, Classes, Hash;

type
  TSolver = class(TThread)
  private
    { Private declarations }
    fReset: Boolean;
    fNuevoTablero: TTipoDato;
    ResetSync: TSimpleRWSync;
    HashSync: TSimpleRWSync;
    procedure Solve(Tablero: TTipoDato);
  protected
    procedure Execute; override;
  public
    HashData: THashData;
    constructor Create(CreateSuspended: Boolean);
    procedure Reset(Tablero: TTipoDato);
    function GetHashCount: Integer;
  end;

implementation

uses
  Tablero, Juego;

{ TSolver }
procedure TSolver.Solve(Tablero: TTipoDato);
var
  td,ntd: TTipoDato;
  rt, nrt: TRuta;
  tt: TTablero;
  lvl,i,j,k, Total, Nuevos, Antiguos, ptj, MaxPtj : Integer;
begin
  // Comienza una b�squeda por el mejor juego...
  tt := TTablero.Create;
  tt.ImportFromTD(Tablero);
  ptj := tt.GetPuntaje;
  MaxPtj := ptj;

  HashSync.BeginWrite;
  try
    HashData.Clear;
    HashData.Add( Tablero, Chr(ptj) );
  finally
    HashSync.EndWrite;
  end;

  Total := 1;
  Antiguos := 0;
  lvl := 1;
  repeat
    Nuevos := 0;
    for k:=Antiguos to Total-1 do
    begin
      td := HashData.GetTD(k);
      rt := HashData.GetR(k);
      // Solo continua esta rama si puede obtener m�s puntaje que
      // el mayor puntaje hasta ahora.
      if (Ord(rt[1]) + (MaxPath-lvl)) < MaxPtj then
        Continue;

      tt.ImportFromTD(td);
      for i:=0 to 3 do
        for j:=0 to 12 do
      begin
          nrt := rt + Chr( Ord('@') + j + i * 13 );
          if tt.Juega(i,j) then
          begin
            ntd := tt.ExportToTD;
            ptj := tt.GetPuntaje;
            nrt[1] := Chr( ptj );
            if ptj > MaxPtj then
              MaxPtj := ptj;
            if HashData.Buscar(ntd) < 0 then
            begin
              HashSync.BeginWrite;
              try
                HashData.Add(ntd,nrt)
              finally
                HashSync.EndWrite;
              end;
              Nuevos := Nuevos + 1;
            end;
            tt.ImportFromTD(td);
          end;
      end;
      if fReset then Exit;
      if Terminated then Exit;
    end;
    Antiguos := Total;
    Total := Total + Nuevos;
    lvl := lvl + 1;
  until (Nuevos = 0) or (lvl > MaxPath) or (Total > 1000000);
  // Duerme...
  repeat
    if fReset then Exit;
    if Terminated then Exit;
    Sleep(1000);
    Suspend;
  until False;
end;

procedure TSolver.Execute;
var
  Tablero: TTipoDato;
begin

  repeat

    // Obtiene el juego...
    ResetSync.BeginWrite;
    try
      fReset := False;
      Tablero := fNuevoTablero;
    finally
      ResetSync.EndWrite;
    end;

    // Comienza a resolver...
    Solve( Tablero );

  until Terminated;

end;

procedure TSolver.Reset(Tablero: TTipoDato);
begin
  ResetSync.BeginWrite;
  try
    fNuevoTablero := Tablero;
    fReset := True;
  finally
    ResetSync.EndWrite;
  end;
end;

function TSolver.GetHashCount: Integer;
begin
  HashSync.BeginRead;
  try
    Result := HashData.GetCount;
  finally
    HashSync.EndRead;
  end;
end;

constructor TSolver.Create(CreateSuspended: Boolean);
begin
  Inherited Create(CreateSuspended);
  HashData := THashData.Create;
  ResetSync := TSimpleRWSync.Create;
  HashSync := TSimpleRWSync.Create;
end;

end.
