// Unit Tablero

#include "Tablero.h"

#include "Hash.h"

#include <stdlib.h>

/* returns random integer betwen 0 and max inclusive */
int Random(int max)
{
   return (int)((max+1) * (rand() / (RAND_MAX+1.0)));
}

TTablero::TTablero()
{
   int i,j;
   
   for(i=0;i<4;i++)
     for(j=0;j<13;j++)
     {
	Data[i][j].Pinta = (enum TPintaCarta)i;
	Data[i][j].Numero = j + 1;
     }
   OnDrawCard = 0;
}

TCarta TTablero::GetCarta(int j,int i)
{
   if ((i<0) || (i>3) || (j<0) || (j>12))
     throw new EOutOfBound("Access out of bounds");
   if ((Data[i][j].Numero == 1) && (j > 0))
   {
      TCarta c;
      
      c = GetCarta(j-1,i);
      if ((c.Numero == 13) || (c.Numero == 0))
      {
	 c.Numero = 0;
	 return c;
      }
   }
   return Data[i][j];
}


void TTablero::SetCarta(int j,int i,TCarta c)
{
   
   if ((i<0) || (i>3) || (j<0) || (j>12))
     throw new EOutOfBound("Access out of bounds");
   Data[i][j] = c;
   if (OnDrawCard != 0)
   {
      int k;
      OnDrawCard(this,i,j);
      for(k=j+1;k<13;k++)
      {
	 if (Data[i][k].Numero == 1)
	   OnDrawCard(this,i,k);
	 else
	   return;
      }
   }
   
}

void TTablero::Swap(int i,int j,int k,int l)
{
   TCarta c;
   c = Data[i][j];
   Data[i][j] = Data[k][l];
   Data[k][l] = c;
   
   if (OnDrawCard != 0)
   {
      int n;
      OnDrawCard(this,i,j);
      for(n=j+1;n<13;n++)
      {
	 if (Data[i][n].Numero != 1)
	   break;
	 OnDrawCard(this,i,n);
      }
      OnDrawCard(this,k,l);
      for(n=l+1;n<13;n++)
      {
	 if (Data[k][n].Numero != 1)
	   break;
	 OnDrawCard(this,k,n);
      }
   }
}

void TTablero::ReDeal()
{
   int i,j,k,l;
   int Ocupa[4];

   // Calcula las inm�viles
   for(i=0;i<4;i++)
   {
      j=0;
      while (j < 12)
      {
	 if ( (Data[i][j].Pinta != Data[i][0].Pinta) ||
	      (Data[i][j].Numero - 2 != j) )
	   break;
	 j = j + 1;
      }
      Ocupa[i] = j;
   }
   
   if ((Ocupa[0] + Ocupa[1] + Ocupa[2] + Ocupa[3]) == 0)
     return;

   // Mueve los espacios
   k=0;
   for(i=0;i<4;i++)
     for(j=Ocupa[i]+1;j<13;j++)
     {
	if (Data[i][j].Numero == 1)
	{
	   Swap(i,j,k,Ocupa[k]);
	   k = k + 1;
	}
     }
   
   // Revuelve el resto
   for(i=0;i<4;i++)
     for(j=Ocupa[i]+1;j<13;j++)
     {
	do
	{
	   k = Random(3);
	   l = Random(12);
	}
	while (l <= Ocupa[k]);
	Swap(i,j,k,l);
     }
   
}

void TTablero::Revuelve()
{
   int i,j,k,l;
   
   for(i=0;i<4;i++)
     for(j=0;j<13;j++)
     {
	k = Random(3);
	l = Random(12);
	Swap(i,j,k,l);
     }
}

bool TTablero::Juega(int pi,int pj)
{
   int i,j,ni,nj;
   TCarta c;

   // Juega una carta, retorna False en caso de error.
   if (Data[pi][pj].Numero == 1)
     return false;
   else if (Data[pi][pj].Numero == 2)
   {
      // Un "2", ve si hay un vacio al principio...
      ni = -1;
      nj = -1;
      for(i=0;i<4;i++)
      {
	 if (Data[i][0].Numero == 1)
	 {
	    ni = i;
	    nj = 0;
	 }
      }
      if ((ni >= 0) && (nj >= 0))
      {
	 Swap(pi,pj,ni,nj);
	 return true;
      }
      else
	return false;
   }
   else
   {
      // Verifica si existe un lugar para esta carta...
      ni = -1;
      nj = -1;
      c = Data[pi][pj];
      c.Numero = c.Numero-1;
      for(i=0;i<4;i++)
	for(j=1;j<13;j++)
	{
	   if ( (Data[i][j].Numero == 1) &&
		(Data[i][j-1].Numero == c.Numero) &&
		(Data[i][j-1].Pinta == c.Pinta) )
	   {
	      ni = i;
	      nj = j;
	      break;
	   }
	}
      if ((ni >= 0) && (nj >= 0))
      {
	 Swap(pi,pj,ni,nj);
	 return true;
      }
      else
	return false;
   }
}

int TTablero::GetPuntaje()
{
   int i,j,p;
   
   // Calcula el puntaje...
   p = 0;
   for(i=0;i<4;i++)
   {
      j=0;
      while (j < 12)
      {
	 if ( (Data[i][j].Pinta != Data[i][0].Pinta) ||
	      (Data[i][j].Numero - 2 != j) )
	   break;
	 p = p + 1;
	 j = j + 1;
      }
   }
   return p;
}


int TTablero::GetVacios()
{
   int i,j,v,l;
   
   v = 0;
   for(i=0;i<4;i++)
   {
      l = 1;
      for(j=0;j<13;j++)
      {
	 if (Data[i][j].Numero > 1)
	   l = Data[i][j].Numero;
	 else if (l != 13)
	   v = v + 1;
      }
   }
   return v;
}


void TTablero::ImportFromTD(TTipoDato &Dato)
{
   int p,i,j,k,l;
   TCarta c;
   
   k = 0;
   p = 0;
   for(i=0;i<4;i++)
     for(j=0;j<13;j++)
     {
	if (Dato.data[k] != ' ')
	{
	   l = Dato.data[k] - '@';
	   c.Numero = 1 + (l % 13);
	   c.Pinta = (TPintaCarta)(l/13);
	}
	else
	{
	   c.Numero = 1;
	   c.Pinta = (TPintaCarta)p;
	   p = p + 1;
	}
	Data[i][j] = c;
	k = k + 1;
     }
}

TTipoDato TTablero::ExportToTD()
{
   int i,j,k,l;
   TTipoDato r;
   
   k = 0;
   
   for(i=0;i<4;i++)
     for(j=0;j<13;j++)
     {
	if (Data[i][j].Numero == 1)
	  r.data[k] = ' ';
	else
	{
	   l = (Data[i][j].Numero - 1) + Data[i][j].Pinta * 13;
	   r.data[k] = l + '@';
	}
	k = k + 1;
     }
   return r;
}

