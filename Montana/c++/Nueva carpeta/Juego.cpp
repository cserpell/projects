//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Juego.h"
#include "Hints.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TfrmMain *frmMain;

//---------------------------------------------------------------------------
const char *CaracterNumero[14] =
 { "", "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };

//---------------------------------------------------------------------------
void TfrmMain::UpdatePanels()
{
  int p;

  StatusBar1->Panels->Items[2]->Text = "Deals: " + IntToStr(Deals);
  // Calcula el puntaje...
  p = Tablero->GetPuntaje();
  StatusBar1->Panels->Items[0]->Text = "Score: " + IntToStr(p+Juegos*48);

  // Si tiene 48 puntos, ha ganado!
  if (p == 48)
  {
    Application->MessageBox("Ganaste!!!\nContinua otro juego.","Ganaste",0);
    Juegos = Juegos + 1;
    Tablero->Revuelve();
    pbTablero->Invalidate();
    UpdatePanels();
    return;
  }

  // Si no hay lugares vacios, el turno termina!
  if (!Tablero->GetVacios())
  {
    if (Deals > 0)
    {
      Application->MessageBox("No hay lugares vacios. ","Juego cerrado",0);
      Deals = Deals - 1;
      Tablero->ReDeal();
      pbTablero->Invalidate();
      UpdatePanels();
      return;
    }
    else
    {
      AnsiString ss;
      ss = "El juego ha terminado. Su puntaje final es " + IntToStr(p+48*Juegos);
      Application->MessageBox(ss.c_str(), "Juego finalizado",0);
    }
  }
  // Activa el solver...
  frmHints->SolverThread->Reset(Tablero->ExportToTD());
  if (mnuShowHints->Checked)
  {
    if (!frmHints->Visible)
      frmHints->Visible = true;
    if (frmHints->SolverThread->Suspended)
      frmHints->SolverThread->Suspended = false;
  }
}

//---------------------------------------------------------------------------
void TfrmMain::DibujaCarta(TCanvas *c, int X, int Y, int cX, int cY, TCarta Carta, bool Modo)
{
  if(Modo)
    c->Brush->Color = clGray;
  else
    c->Brush->Color = clWhite;

  c->Brush->Style = bsSolid;
  c->RoundRect(X+1,Y+1,X+cX-1,Y+cY-1,cX/2,cY/4);

  ImagenesPintas1->Draw(c,X+(cX-ImagenesPintas1->Width)/2,
                      Y+cY/4-ImagenesPintas1->Height/2, Carta.Pinta, true);

  int w = c->TextWidth(CaracterNumero[Carta.Numero]);
  int h = c->TextHeight(CaracterNumero[Carta.Numero]);
  c->TextOut(X+cX/2-(w/2),Y+(cY*3)/4-(h/2),CaracterNumero[Carta.Numero]);

  c->Brush->Style = bsClear;
  c->Pen->Style = psSolid;
  c->Pen->Color = clBlack;
  c->RoundRect(X+1,Y+1,X+cX-1,Y+cY-1,cX/2,cY/4);

}

//---------------------------------------------------------------------------
void TfrmMain::BorraLugar(TCanvas *c, int X, int Y, int cX, int cY)
{
  c->Brush->Color = Color;
  c->Brush->Style = bsSolid;
  c->FillRect(Rect(X,Y,X+cX-1,Y+cY-1));
}

//---------------------------------------------------------------------------
void TfrmMain::DibujaP(int X, int Y, int cX, int cY)
{
  int w;
  pbTablero->Canvas->Pen->Color = clGray;
  w = cX / 8;
  if (w < 1)
    w = 1;
  pbTablero->Canvas->Pen->Width = w;
  pbTablero->Canvas->Brush->Style = bsSolid;
  pbTablero->Canvas->MoveTo(X+1+w,Y+1+w);
  pbTablero->Canvas->LineTo(X+cX-2-w,Y+cY-2-w);
  pbTablero->Canvas->MoveTo(X+cX-2-w,Y+1+w);
  pbTablero->Canvas->LineTo(X+1+w,Y+cY-2-w);
  pbTablero->Canvas->Pen->Width = 0;
}

//---------------------------------------------------------------------------
void TfrmMain::DibujaX(int X, int Y, int cX, int cY)
{
  int w;
  pbTablero->Canvas->Pen->Color = clRed;
  w = cX / 8;
  if (w < 1)
    w = 1;
  pbTablero->Canvas->Pen->Width = w;
  pbTablero->Canvas->Brush->Style = bsSolid;
  pbTablero->Canvas->MoveTo(X+1+w,Y+1+w);
  pbTablero->Canvas->LineTo(X+cX-2-w,Y+cY-2-w);
  pbTablero->Canvas->MoveTo(X+cX-2-w,Y+1+w);
  pbTablero->Canvas->LineTo(X+1+w,Y+cY-2-w);
  pbTablero->Canvas->Pen->Width = 0;
}

//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::StatusBar1MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  int i;
  i = StatusBar1->Panels->Items[0]->Width + StatusBar1->Panels->Items[1]->Width +
       StatusBar1->Panels->Items[2]->Width;
  if ((X >= i) && (X < (i+StatusBar1->Panels->Items[3]->Width)))
    mnuReDeal->Click();
}

//---------------------------------------------------------------------------
void TfrmMain::HiLight(int aI, int aJ)
{
  int cX,cY;

  if ((aI >= 0) && (aJ >= 0))
  {
    ApI = aI;
    ApJ = aJ;
    cX = (pbTablero->Width - 2) / 13;
    cY = (pbTablero->Height - 2) / 4;
    DibujaCarta(pbTablero->Canvas,cX*ApJ+1,cY*ApI+1,cX,cY,Tablero->GetCarta(ApJ,ApI),
                true);
  }
}

//---------------------------------------------------------------------------
void TfrmMain::DeLight()
{
  int cX,cY;

  if ((ApI >= 0) && (ApJ >= 0))
  {
    cX = (pbTablero->Width - 2) / 13;
    cY = (pbTablero->Height - 2) / 4;
    DibujaCarta(pbTablero->Canvas,cX*ApJ+1,cY*ApI+1,cX,cY,Tablero->GetCarta(ApJ,ApI));
    ApI = -1;
    ApJ = -1;
  }
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::OnDrawCard(TTablero *Sender, int i, int j)
{
  TCarta c;
  int cX,cY;

  // Dibuja la carta que cambi�
  cX = (pbTablero->Width - 2) / 13;
  cY = (pbTablero->Height - 2) / 4;

  c = Tablero->GetCarta(j,i);

  BorraLugar(pbTablero->Canvas,j*cX+1,i*cY+1,cX,cY);

  if (c.Numero == 0)
    DibujaX(j*cX+1,i*cY+1,cX,cY);
  else if (c.Numero > 13)
    DibujaP(j*cX+1,i*cY+1,cX,cY);
  else if (c.Numero > 1)
    DibujaCarta(pbTablero->Canvas, 1+cX * j, 1+cY * i, cX, cY, c );

}


//---------------------------------------------------------------------------
void __fastcall TfrmMain::mnuNewClick(TObject *Sender)
{
  Deals = 3;

  Tablero->Revuelve();
  pbTablero->Invalidate();
  UpdatePanels();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::pbTableroPaint(TObject *Sender)
{
  int cX, cY;
  int i,j;
  TRect R;
  TCarta c;

  // Dibuja el tablero de juego...
  cX = (pbTablero->Width - 2) / 13;
  cY = (pbTablero->Height - 2) / 4;
  R = pbTablero->BoundsRect;
  Frame3D( pbTablero->Canvas,R,cl3DDkShadow,cl3DLight,1  );

  pbTablero->Canvas->Font->Size = (cY/4)+2;
  while ((pbTablero->Canvas->TextWidth("10") > (cX-3)) ||
        (pbTablero->Canvas->TextHeight("0123456789") > ((cY/2)-3)))
  {
    pbTablero->Canvas->Font->Size = pbTablero->Canvas->Font->Size - 1;
    if (pbTablero->Canvas->Font->Size <= 1)
    {
      pbTablero->Canvas->Font->Size = 8;
      break;
    }
  }

  // Elige el tama�o correcto de las pintas...
/*  ImagenesPintas = ImagenesPintas1;
  if (ImagenesPintas.Width > (cX-3)) or (ImagenesPintas.Height > ((cY div 2)-3)) then
    ImagenesPintas := ImagenesPintas2;
  if (ImagenesPintas.Width > (cX-3)) or (ImagenesPintas.Height > ((cY div 2)-3)) then
    ImagenesPintas := ImagenesPintas3;
  if (ImagenesPintas.Width > (cX-3)) or (ImagenesPintas.Height > ((cY div 2)-3)) then
    ImagenesPintas := ImagenesPintas4;
*/
  for(i=0;i<4;i++)
    for(j=0;j<13;j++)
  {
      c = Tablero->GetCarta(j,i);
      if (c.Numero == 0)
        DibujaX(j*cX+1,i*cY+1,cX,cY);
      else if (c.Numero > 13)
        DibujaP(j*cX+1,i*cY+1,cX,cY);
      else if (c.Numero > 1)
        DibujaCarta( pbTablero->Canvas, 1+cX * j, 1+cY * i, cX, cY,
                     Tablero->GetCarta(j,i) );
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormShow(TObject *Sender)
{
  Deals = 3;
  ApI = -1;
  ApJ = -1;
  Tablero->Revuelve();
  UpdatePanels();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuExitClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::pbTableroMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  int cX,cY;
  int i,j,pi,pj;
  TCarta c;

  cX = (pbTablero->Width - 2)/13;
  cY = (pbTablero->Height - 2)/4;
  pj = (X - 1)/cX;
  pi = (Y - 1)/cY;

  // Ve si es necesario des-apretar
  DeLight();

  // Verifica que carta est� en el lugar del mouse
  if (Tablero->GetCarta(pj,pi).Numero == 1)
  {
    // Es un lugar vacio, pinta la carta corespondiente
    if (pj == 0)
    {
      // Busca cualquier "2"
      for(i=0;i<4;i++)
        for(j=1;j<13;j++)
      {
          if (Tablero->GetCarta(j,i).Numero == 2)
          {
            HiLight(i,j);
            return;
          }
      }
    }
    else if (Tablero->GetCarta(pj-1,pi).Numero == 1)
    {
      // No hay cartas a la izquierda
    }
    else if (Tablero->GetCarta(pj-1,pi).Numero == 13)
    {
      // Es un rey...
    }
    else
    {
      c = Tablero->GetCarta(pj-1,pi);
      c.Numero = c.Numero + 1;
      for(i=0;i<4;i++)
        for(j=0;j<13;j++)
      {
          if ((Tablero->GetCarta(j,i).Pinta == c.Pinta) &&
             (Tablero->GetCarta(j,i).Numero == c.Numero) )
          {
            HiLight(i,j);
            return;
          }
      }
    }
  }
  else
  {
    // Intenta jugar...
    if (Tablero->Juega(pi,pj))
    {
      UpdatePanels();
    }
  }
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::pbTableroMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  // Ve si es necesario des-apretar
  DeLight();
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::mnuReDealClick(TObject *Sender)
{
  if (Deals > 0)
  {
    Tablero->ReDeal();
    Deals = Deals - 1;
    pbTablero->Invalidate();
    UpdatePanels();
  }
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
  Tablero = new TTablero();
  Tablero->OnDrawCard = OnDrawCard;
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::mnuShowHintsClick(TObject *Sender)
{
  if (mnuShowHints->Checked)
  {
    // Desactiva el Solver...
    mnuShowHints->Checked = false;
    frmHints->Hide();
  }
  else
  {
    // Activa el Solver!
    mnuShowHints->Checked = true;
    frmHints->Show();
  }
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::mnuInputGameClick(TObject *Sender)
{
//
}

//---------------------------------------------------------------------------

