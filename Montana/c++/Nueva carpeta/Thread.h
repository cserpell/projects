// A TThread minimal implementation...

#ifndef _THREAD_H_
#define _THREAD_H_

#include <pthread.h>

class TThread
{
 private:
   bool fTerminated;
   bool fSuspended;
   pthread_t threadID;

 public:
   virtual void Execute();
   bool GetTerminated();
   bool GetSuspended();
   void Suspend();
   void Resume();
   TThread(bool CreateSuspended);
      
};


// Not implemented!!!
class TSimpleRWSync
{
 public:
   void BeginWrite(){};
   void EndWrite(){};
   void BeginRead(){};
   void EndRead(){};
};

#endif // _THREAD_H_
