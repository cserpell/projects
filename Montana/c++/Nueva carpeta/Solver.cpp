// unit Solver


#include "Solver.h"

#include "Tablero.h"

void TSolver::Solve(TTipoDato Tablero)
{
   TTipoDato td,ntd;
   TRuta rt,nrt;
   TTablero tt;
   
   int lvl,i,j,k, Total, Nuevos, Antiguos, ptj, MaxPtj;

   // Comienza una b�squeda por el mejor juego...
   tt.ImportFromTD(Tablero);
   ptj = tt.GetPuntaje();
   MaxPtj = ptj;
   
   rt.len = 0;
   rt.ptj = ptj;
   
   HashSync->BeginWrite();
   try
   {
      HashData.Clear();
      HashData.Add( Tablero, rt );
   }
   catch(...)
   {
     HashSync->EndWrite();
   }
   HashSync->EndWrite();

   Total = 1;
   Antiguos = 0;
   lvl = 1;
   do
   {
      Nuevos = 0;
      for(k=Antiguos;k<Total;k++)
      {
	 td = HashData.GetTD(k);
	 rt = HashData.GetR(k);
	 // Solo continua esta rama si puede obtener m�s puntaje que
	 // el mayor puntaje hasta ahora.
	 if ((rt.ptj + (MaxPath-lvl)) < MaxPtj)
	   continue;

	 tt.ImportFromTD(td);
	 nrt = rt;
	 nrt.len++;
	 for(i=0;i<4;i++)
	   for(j=0;j<13;j++)
	   {
	      nrt.data[rt.len] = j + i * 13;
	      if (tt.Juega(i,j))
	      {
		 ntd = tt.ExportToTD();
		 ptj = tt.GetPuntaje();
		 nrt.ptj = ptj;
		 if (ptj > MaxPtj)
		   MaxPtj = ptj;
		 if (HashData.Buscar(ntd) < 0)
		 {
		    HashSync->BeginWrite();
		    try
		    {
		       HashData.Add(ntd,nrt);
		    }
		    catch(...)
		    {
		       HashSync->EndWrite();
		    }
		    HashSync->EndWrite();
		    Nuevos = Nuevos + 1;
		 }
		 tt.ImportFromTD(td);
	      }
	   }
	 if ( fReset || Terminated )
	   return;
      }
      Antiguos = Total;
      Total = Total + Nuevos;
      lvl = lvl + 1;
   }
   while ( (Nuevos != 0) && (lvl <= MaxPath) && (Total < 1000000) );
   
   // Duerme...
   while(1)
   {
      if (fReset || Terminated)
	return;
      // Waits...
      Sleep(1000);
   }
}

void __fastcall TSolver::Execute(void)
{
   TTipoDato Tablero;
   
   while ( ! Terminated )
   {
      // Obtiene el juego...
      ResetSync->BeginWrite();
      try
      {
	 fReset = false;
	 Tablero = fNuevoTablero;
      }
      catch(...)
      {
	 ResetSync->EndWrite();
      }
      ResetSync->EndWrite();

      // Comienza a resolver...
      Solve( Tablero );

   }
}


void TSolver::Reset(TTipoDato Tablero)
{
   ResetSync->BeginWrite();
   try
   {
      fNuevoTablero = Tablero;
      fReset = true;
   }
   catch(...)
   {
      ResetSync->EndWrite();
   }
   ResetSync->EndWrite();
}

int TSolver::GetHashCount()
{
   int r;

   HashSync->BeginRead();
   try
   {
      r = HashData.GetCount();
   }
   catch(...)
   {
      HashSync->EndRead();
   }
   HashSync->EndRead();
   return r;
}
