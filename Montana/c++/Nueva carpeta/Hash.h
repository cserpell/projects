// unit Hash

#ifndef _HASH_H_
#define _HASH_H_

#include <stdlib.h>
#include <string.h>

// M�xima profundidad de la b�squeda...
const int MaxPath = 45;

struct TTipoDato
{
   char data[52];
   int operator== (TTipoDato &d)
   {
      return 0 == memcmp(data,d.data,sizeof(data));
   }
};

struct TRuta
{
   unsigned char len;
   unsigned char ptj;
   unsigned char data[MaxPath];
};

class THashData
{
   class EOutOfBound
   {
      const char *msg;
    public:
      EOutOfBound(const char *m)
      {
	 msg = m;
      }
   }; 
 private:
   TTipoDato *fDatos;
   TRuta *fRutas;
   int *fTablaHash;
   int fNumDatos;
   int fTotalDatos;
   int fHashSize;
   
   int HashOf(const TTipoDato Key);
 public:
   void Clear();
   
   int Buscar(TTipoDato Dato);
   bool Add(TTipoDato Dato, TRuta Ruta);
   TTipoDato GetTD(int i);
   TRuta GetR(int i);
   
   int GetCount()
   {
      return fNumDatos;
   }
   
   THashData()
   {
      fNumDatos = 0;
      fTotalDatos = 0;
      fHashSize = 0;
      fDatos = 0;
      fRutas = 0;
      fTablaHash = 0;
   };
   
   ~THashData()
   {
      free(fDatos);
      free(fRutas);
      free(fTablaHash);
   }
   
};


#endif // _HASH_H_
