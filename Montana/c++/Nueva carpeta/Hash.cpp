
// Unit Hash


#include "Hash.h"

#include <stdlib.h>

void THashData::Clear()
{
   fNumDatos = 0;
   fTotalDatos = 0;
   fHashSize = 0;
   
   free(fDatos);
   free(fRutas);
   free(fTablaHash);
}


int THashData::HashOf(const TTipoDato Key)
{
   size_t i;
   int r = 0;
   
   for(i=0;i<sizeof(TTipoDato);i++)
     r = ((r>>2) | (r<<(sizeof(r) * 8 - 2))) ^ (i*Key.data[i]);
   
   if (r < 0)
     r = - r;
   return r % fHashSize;
}

TTipoDato THashData::GetTD(int i)
{
   if ((i < 0) || (i >= fNumDatos))
     throw new EOutOfBound("Access out of bounds");
   return fDatos[i];
}

TRuta THashData::GetR(int i)
{
   if ((i < 0) || (i >= fNumDatos))
     throw new EOutOfBound("Access out of bounds");
   return fRutas[i];
}

int THashData::Buscar(TTipoDato Dato)
{
   int k;
   
   if (fHashSize < 1)
     return -1;
   
   k = HashOf(Dato);
   while (fTablaHash[k] != -1)
   {
      if (fDatos[fTablaHash[k]] == Dato)
	return fTablaHash[k];
      k = (k + 1) % fHashSize;
   }
   
   return -1;
}

bool THashData::Add(TTipoDato Dato, TRuta Ruta)
{
   int i,k;
   
   if (Buscar(Dato) >= 0)
     return false;
   
   
   if ((fTotalDatos == 0) || (fTotalDatos == fNumDatos))
   {
      if (fTotalDatos == 0)
	fTotalDatos = 1024;
      else
	fTotalDatos = fTotalDatos * 2;
      
      fDatos = (TTipoDato*)realloc(fDatos,fTotalDatos * sizeof(fDatos[0]));
      fRutas = (TRuta*)realloc(fRutas,fTotalDatos * sizeof(fRutas[0]));
   }
   
   if (!fHashSize)
   {
      fHashSize = 128*1024 - 17;
      fTablaHash = (int *)malloc(fHashSize * sizeof(fTablaHash[0]));
      for(i=0;i<fHashSize;i++)
	fTablaHash[i] = -1;
   }
   
   if (fHashSize < (fNumDatos*2))
   {
      // Rehace el Hash!
      fHashSize = 4 * fHashSize - 29;
      fTablaHash = (int *)malloc(fHashSize * sizeof(fTablaHash[0]));
      for(i=0;i<fHashSize;i++)
	fTablaHash[i] = -1;
      for(i=0;i<fNumDatos;i++)
      {
	 k = HashOf(fDatos[i]);
	 while (fTablaHash[k] != -1)
	   k = (k + 1) % fHashSize;
	 fTablaHash[k] = i;
      }
   }
   
   fDatos[fNumDatos] = Dato;
   fRutas[fNumDatos] = Ruta;
   k = HashOf(Dato);
   while (fTablaHash[k] != -1)
     k = (k + 1) % fHashSize;
   fTablaHash[k] = fNumDatos;
   
   fNumDatos = fNumDatos + 1;
   return true;
}

