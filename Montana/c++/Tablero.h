// Unit Tablero

#ifndef _TABLERO_H_
#define _TABLERO_H_


enum TPintaCarta { ccTrebol, ccDiamante, ccCorazon, ccHoja };
typedef int TNumeroCarta;

struct TCarta
{
   TPintaCarta Pinta;
   TNumeroCarta Numero;
};

class TTablero;

typedef void __fastcall (__closure *TDrawCardEvent)(TTablero *Sender, int i, int j);

struct TTipoDato;

class TTablero
{
   class EOutOfBound
   {
      const char *msg;
    public:
      EOutOfBound(const char *m)
      {
	 msg = m;
      }
   }; 
 private:
   TCarta Data[4][13];
   
   void Swap(int i,int j,int k,int l);
 public:
   TCarta GetCarta(int j,int i);
   void SetCarta(int j,int i, TCarta c);
   void Revuelve();
   void ReDeal();
   void ImportFromTD( TTipoDato &Dato );
   TTipoDato ExportToTD();
   bool Juega(int pi, int pj);
   
   TDrawCardEvent OnDrawCard;
   
   int GetPuntaje();
   int GetVacios();
   
   TTablero();
   
};


#endif // _TABLERO_H_
