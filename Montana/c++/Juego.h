//---------------------------------------------------------------------------
#ifndef JuegoH
#define JuegoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>


#include "Tablero.h"
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu1;
        TMenuItem *mnuGame;
        TStatusBar *StatusBar1;
        TPaintBox *pbTablero;
        TImageList *ImagenesPintas1;
        TImageList *ImagenesPintas2;
        TImageList *ImagenesPintas3;
        TImageList *ImagenesPintas4;
        TMenuItem *mnuNew;
        TMenuItem *mnuExit;
        TMenuItem *mnuOptions;
        TMenuItem *mnuScores;
        TMenuItem *mnuPreferences;
        TMenuItem *mnuReDeal;
        TMenuItem *mnuShowHints;
        TMenuItem *mnuInputGame;
        void __fastcall StatusBar1MouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall mnuNewClick(TObject *Sender);
        void __fastcall pbTableroPaint(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall mnuExitClick(TObject *Sender);
        void __fastcall pbTableroMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall pbTableroMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall mnuReDealClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall mnuShowHintsClick(TObject *Sender);
        void __fastcall mnuInputGameClick(TObject *Sender);
private:	// User declarations
    TTablero *Tablero;
    int Deals, Juegos, ApI, ApJ;
    TImageList *ImagenesPintas;
    void UpdatePanels();
    void DibujaCarta(TCanvas *c, int X, int Y, int cX, int cY, TCarta Carta, bool Modo = false);
    void BorraLugar(TCanvas *c, int X, int Y, int cX, int cY);
    void DibujaP(int X, int Y, int cX, int cY);
    void DibujaX(int X, int Y, int cX, int cY);
    void __fastcall OnDrawCard(TTablero *Sender, int i, int j);
    void __fastcall OnCartasPaint(TObject *Sender);
    void __fastcall OnCartasBorraClick(TObject *Sender);
    void __fastcall OnCartasBlancoClick(TObject *Sender);
    void __fastcall OnCartasMouseDown(TObject *Sender, TMouseButton Button,
                                      TShiftState Shift, int X, int Y);
public:		// User declarations
    __fastcall TfrmMain(TComponent* Owner);
    void HiLight(int aI, int aJ);
    void DeLight();
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
