
#include "Thread.h"

#include <pthread.h>
#include <signal.h>

static void * thread_execute( void *arg )
{
   TThread *t = (TThread*)(arg) ;
   
   t->Execute();
   return 0;
}

TThread::TThread(bool CreateSuspended)
{
   // Creates a new thread - CreateSuspended is NOT supported!!!
   // (can be done holding a semaphore in thread_execute)
   fSuspended = false;
   fTerminated = false;
   pthread_create( &threadID, 0, thread_execute, (void *)this );
}

void TThread::Suspend()
{
   fSuspended = true;
   pthread_kill( threadID, SIGSTOP );
}

void TThread::Resume()
{
   pthread_kill( threadID, SIGCONT );
   fSuspended = false;
}

bool TThread::GetTerminated()
{
   return fTerminated;
}

bool TThread::GetSuspended()
{
   return fSuspended;
}
