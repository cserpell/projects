//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("Montana.res");
USEFORM("Juego.cpp", frmMain);
USEUNIT("Tablero.cpp");
USEUNIT("Hash.cpp");
USEUNIT("Solver.cpp");
USEFORM("..\..\..\..\..\Archivos de programa\Borland\CBuilder4\Include\Vcl\Hints.cpp", frmHints);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TfrmMain), &frmMain);
                 Application->CreateForm(__classid(TfrmHints), &frmHints);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
