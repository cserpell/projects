program Montana;

uses
  QForms,
  Juego in 'Juego.pas' {frmMain},
  Tablero in 'Tablero.pas',
  Hash in 'Hash.pas',
  Solver in 'Solver.pas',
  Hints in 'Hints.pas' {frmHints};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmHints, frmHints);
  Application.Run;
end.
