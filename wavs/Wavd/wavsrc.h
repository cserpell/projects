//---------------------------------------------------------------------------
#ifndef wavsrcH
#define wavsrcH
#include <fstream.h>
//---------------------------------------------------------------------------

class ArchivoLecturaWAV
{
 private:
  ifstream Origen;
  int size;
  int numchannels;
  int samplerate;
  int avgbytespersec;
  int block;
  int numbitspersample;
  int numbytespersample;
  int numsamples;
  int posinit;
 public:
  bool Abrir(char *Nombre);
  bool LeerEncabezado(void);
  int CalcularNS(void);
  int Leer(void);
  bool Leer(char *Dest, int cantsamples);
  bool Leer(char *Dest, int cantsamples, int nchannels);
  bool Leer(char *Dest, int cantsamples, int nchannels, int spos);
  bool IrInicio(void);
  void Cerrar(void);

  int NumChannels(void);
  int SampleRate(void);
  int NumBitsPerSample(void);
  int NumBytesPerSample(void);

  ArchivoLecturaWAV()
  {
    numchannels = 0;
    samplerate = 0;
    numbitspersample = 0;
    numbytespersample = 0;
    posinit = 0;
  }
};

class ArchivoEscrituraWAV
{
 private:
  ofstream Destino;
  int size;
  int numchannels;
  int samplerate;
  int avgbytespersec;
  int block;
  int numbitspersample;
  int numbytespersample;
  int numsamples;
  int posinit;
  bool establec;
  bool escrito;
 public:
  bool Abrir(char *Nombre);
  bool EstablecerEncabezado(int nnumchannels, int nsamplerate,
                                                         int nnumbitspersample);
  bool EscribirEncabezado(void);
  bool Escribir(char *Orig, int cantsamples, int numchannels, int spos);
  bool Escribir(char *Orig, int cantsamples, int numchannels);
  bool Escribir(char *Orig, int cantsamples);
  bool Escribir(int orig);
  void Cerrar(void);
  ArchivoEscrituraWAV()
  {
    establec = false;
    escrito = false;
    posinit = 0;
  }
};

#endif
