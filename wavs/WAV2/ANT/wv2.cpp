
#pragma hdrstop
#include <stdio.h>
#include <fstream.h>
//#include <condefs.h>
//---------------------------------------------------------------------------
#pragma argsused
/*__int16 Calc2i(char *str)
{
  __int16 ret;

  ret = str[1]*256 + str[0];
  return ret;
}
__int32 Calc4i(char *str)
{
  __int32 ret;

  ret = str[3]*256*256*256 + str[2]*256*256 + str[1]*256 + str[0];
  return ret;
}
__int32 Calc(char *str, int numbytes)
{
  __int32 ret;

  if(numbytes == 1)
    ret = str[0];
  else if(numbytes == 2)
    ret = str[0]*256 + str[1];
  else if(numbytes == 4)
    ret = str[0]*256*256*256 + str[1]*256*256 + str[2]*256 + str[3];

  return ret;
}
void Decalc(char *str, __int32 num, int numbytes)
{
  __int32 comp = 256;
  __int8 dif;

  for(int i = 1; i <= numbytes; i++)
  {
    dif = num % comp;
    str[numbytes - i] = dif;
    num >>= 8;
  }
  str[numbytes] = '\0';
} */
//---------------------------------------------------------------------------
// Versi�n original sin wavsrc
/*int main(int argc, char* argv[])
{
  ifstream Origen;
  ofstream Destino;
  char str[4];
  char c;
  int p;


  if(argc < 3)
  {
    cerr << "Uso:  dav ORIGEN.WAV DESTINO.WAV" << endl;
    return 1;
  }

  // Abro el archivo de origen
  Origen.open(argv[1], ios_base::binary);
  if(!Origen)
  {
    cerr << "Error al abrir el archivo de origen." << endl;
    return 1;
  }

  // Abro el archivo de destino
  Destino.open(argv[2], ios_base::binary);
  if(!Destino)
  {
    cerr << "Error al abrir el archivo de destino." << endl;
    Origen.close();
    return 1;
  }

  // Obtengo los datos importantes y los reescribo en el destino
  // Primero llego hasta donde empiezan los datos
  p = 0;
  do
  {
    c = Origen.get();
    p++;
    if(Origen.eof())
    {
      cerr << "El archivo de origen est� err�neo." << endl;
      Origen.close();
      Destino.close();
      return 1;
    }
    Destino.put(c);
  } while(c != 'W');

  Origen.read(str, 3);
  p += 3;
  if(str[0] != 'A'||str[1] != 'V'||str[2] != 'E')
  {
    cerr << "El archivo no es del tipo WAV." << endl;
    Origen.close();
    Destino.close();
    return 1;
  }
  Destino.write(str, 3);

  // ckID 4 The ASCII string "fmt "
  Origen.read(str, 4);
  p += 4;
  if((str[0] != 'f')||(str[1] != 'm')||(str[2] != 't')||(str[3] != ' '))
  {
    cerr << "El archivo de origen no es un archivo tipo WAV." << endl;
    Origen.close();
    Destino.close();
    return 1;
  }
  Destino.write(str, 4);

  // nChunkSize 4 This is a 32-bit unsigned integer which holds the length of
  // the entire 'fmt ' chunk in bytes.
  __int32 size;
  Origen.read(str, 4);
  p += 4;
  Destino.write(str, 4);
  size = Calc4i(str);

  // wFormatTag 2 This defines how the audio data is encoded in the WAV file.
  // This value will almost always be 1, which means Pulse Code Modulation
  // (PCM).
  Origen.read(str, 2);
  p += 2;
  Destino.write(str, 2);
  if(Calc2i(str) != 1)
  {
    cerr << "El archivo de origen no es aceptado por este programa." << endl;
    Origen.close();
    Destino.close();
    return 1;
  }

  // nChannels 2 This is the number of channels of audio present in the WAV
  // file.
  __int16 numchannels;
  Origen.read(str, 2);
  p += 2;
  Destino.write(str, 2);
  numchannels = Calc2i(str);

  // nSamplesPerSec 4 The sampling rate expressed in samples per second, or Hz.
  // The reciprocal of this number is the amount of time between samples
  // expressed in seconds.
  __int32 samplerate;
  Origen.read(str, 4);
  p += 4;
  Destino.write(str, 4);
  samplerate = Calc4i(str);

  // nAvgBytesPerSec 4 The average number of bytes per second that a player
  // program would have to process to play this audio in real time. For PCM
  // audio, this is redundant because you can calucate it by multiplying
  // together the sampling rate, number of channels, and number of bytes per
  // sample.
  __int32 avgbytespersec;
  Origen.read(str, 4);
  p += 4;
  Destino.write(str, 4);
  avgbytespersec = Calc4i(str); // samplerate * numchannels * bytespersample

  // nBlockAlign 2 This number tells you how many bytes there are to output at
  // a single time. In PCM, this is the same as the number of bytes per sample
  // multiplied by the number of audio channels.
  __int16 block;
  Origen.read(str, 2);
  p += 2;
  Destino.write(str, 2);
  block = Calc2i(str); // numbytespersample * numchannels

  // nBitsPerSample 2 This field is present only in PCM recordings. It defines
  // the number of bits per sampled audio amplitude.
  __int16 bitspersample;
  Origen.read(str, 2);
  p += 2;
  Destino.write(str, 2);
  bitspersample = Calc2i(str);

  __int16 numbytespersample;
  numbytespersample = block / numchannels;

  // Voy hasta donde empiezan los datos
  /*do
  {
    do
    {
      c = Origen.get();
      p++;
      if(Origen.eof())
      {
        cerr << "El archivo de origen est� err�neo." << endl;
        Origen.close();
        Destino.close();
        return 1;
      }
      Destino.put(c);
    } while(c != 'd');
    Origen.read(str, 3);
    p += 3;
    Destino.write(str, 3);
  } while(str[0] != 'a'||str[1]!= 't'||str[2] != 'a');

*/

  Origen.read(str, 4);
  if(str[0]!='d'||str[1]!='a'||str[2]!='t'||str[3]!='a')
  {
    cerr << "El archivo de origen est� err�neo." << endl;
    Origen.close();
    Destino.close();
    return 1;
  }
  Destino.write(str,4);

  // Veo la cantidad de samples que hay (numsamples)
  __int32 numsamples;
  int np = p;

  // numsamples = (filelength - ftell)/numbytespersample
  numsamples = 0;
  do
  {
    Origen.read(str, numbytespersample);
    numsamples++;
  } while(!Origen.eof());
  Origen.close();

  // Abro denuevo el archivo de origen y voy hasta la posici�n p
  Origen.open(argv[1], ios_base::binary);
  for(int i = 0; i < p; i++)
    Origen.get();

  // Aqu� empieza el progama, por fin!
  __int32 sample1[4];
  __int32 sample2[4];
  __int32 samplef[4];
  int num;
  bool term;

  num = 0;
  term = false;
  do
  {
    // Lee dos muestras del primer archivo
    for(int i = 0; i < numchannels; i++)
    {
      Origen.read(str, numbytespersample);
      sample1[i] = Calc(str, numbytespersample);
    }
    num += numchannels;
    if(num != numsamples)
    {
      for(int i = 0; i < numchannels; i++)
      {
        Origen.read(str, numbytespersample);
        sample2[i] = Calc(str, numbytespersample);
      }
      num += numchannels;
    } else
      term = true;

    if(!term)
    {
      // Las une en una
      __int32 comp = 1;

      for(int i = 0; i < numchannels; i++)
        samplef[i] = 0;

      for(int j = 0; j < numchannels; j++)
        for(int i = 0; i < bitspersample; i++)
        {
          if(comp & sample1[j])
            samplef[j] ^= comp;
          i++;
          if(i == bitspersample)
            break;
          comp <<= 1;
          if(comp & sample2[j])
            samplef[j] ^= comp;
        }
    } else
      // Copia la primera nom�s
      for(int i = 0; i < numchannels; i++)
        samplef[i] = sample1[i];

    // Escribe la nueva muestra
    for(int i = 0; i < numchannels; i++)
    {
      // Prueba
      if(!(num % 2))
        Decalc(str, sample1[i], numbytespersample);
      else
        Decalc(str, sample2[i], numbytespersample);

      // Esto queda mal�simo, jaja
      //Decalc(str, samplef[i], numbytespersample);
      Destino.write(str, numbytespersample);
    }
  } while(num < numsamples);

  Origen.close();
  Destino.close();
  return 0;
}
