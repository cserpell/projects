//---------------------------------------------------------------------------
#pragma hdrstop

#include "wavsrc.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

// Funciones de ArchivoLecturaWAV
bool ArchivoLecturaWAV::Abrir(char *Nombre)
{
  Origen.open(Nombre, ios_base::binary);
  if(!Origen)
    return false;
  return true;
}
bool ArchivoLecturaWAV::LeerEncabezado(void)
{
  if(!Origen)
    return false;

  char c;
  char str[4];

  posinit = 0;
  do
  {
    c = (char)Origen.get();
    posinit++;
    if(Origen.eof())
    {
      Cerrar();
      return false;
    }
  } while(c != 'W');

  Origen.read(str, 3);
  posinit += 3;
  if(str[0] != 'A'||str[1] != 'V'||str[2] != 'E')
  {
    Cerrar();
    return false;
  }

  // ckID 4 The ASCII string "fmt "
  Origen.read(str, 4);
  posinit += 4;
  if((str[0] != 'f')||(str[1] != 'm')||(str[2] != 't')||(str[3] != ' '))
  {
    Cerrar();
    return false;
  }

  // nChunkSize 4 This is a 32-bit unsigned integer which holds the length of
  // the entire 'fmt ' chunk in bytes.
  Origen.read(str, 4);
  posinit += 4;
  size = Calc4i(str);

  // wFormatTag 2 This defines how the audio data is encoded in the WAV file.
  // This value will almost always be 1, which means Pulse Code Modulation
  // (PCM).
  Origen.read(str, 2);
  posinit += 2;
  if(Calc2i(str) != 1)
  {
    Cerrar();
    return false;
  }

  // nChannels 2 This is the number of channels of audio present in the WAV
  // file.
  Origen.read(str, 2);
  posinit += 2;
  numchannels = Calc2i(str);

  // nSamplesPerSec 4 The sampling rate expressed in samples per second, or Hz.
  // The reciprocal of this number is the amount of time between samples
  // expressed in seconds.
  Origen.read(str, 4);
  posinit += 4;
  samplerate = Calc4i(str);

  // nAvgBytesPerSec 4 The average number of bytes per second that a player
  // program would have to process to play this audio in real time. For PCM
  // audio, this is redundant because you can calucate it by multiplying
  // together the sampling rate, number of channels, and number of bytes per
  // sample.
  Origen.read(str, 4);
  posinit += 4;
  avgbytespersec = Calc4i(str); // samplerate * numchannels * bytespersample

  // nBlockAlign 2 This number tells you how many bytes there are to output at
  // a single time. In PCM, this is the same as the number of bytes per sample
  // multiplied by the number of audio channels.
  Origen.read(str, 2);
  posinit += 2;
  block = Calc2i(str); // numbytespersample * numchannels

  // nBitsPerSample 2 This field is present only in PCM recordings. It defines
  // the number of bits per sampled audio amplitude.
  Origen.read(str, 2);
  posinit += 2;
  numbitspersample = Calc2i(str);

  numbytespersample = block / numchannels;

  // Al final debe llevar la palabra "data"
  Origen.read(str, 4);
  posinit += 4;
  if(str[0]!='d'||str[1]!='a'||str[2]!='t'||str[3]!='a')
  {
    Cerrar();
    return false;
  }
  return true;
}
__int32 ArchivoLecturaWAV::CalcularNS(void)
{
  if(!Origen)
    return 0;

  // Veo la cantidad de samples que hay (numsamples)
  char str[4];

  // numsamples = (filelength - ftell)/numbytespersample
  Origen.seekg(posinit);
  numsamples = 0;
  do
  {
    Origen.read(str, numbytespersample);
    numsamples++;
  } while(!Origen.eof());
  return numsamples;
}
bool ArchivoLecturaWAV::Leer(char *Dest, __int32 cantsamples)
{
  if(!Origen || Origen.eof())
    return false;
  Origen.read(Dest, cantsamples*numbytespersample);
  return true;
}
bool ArchivoLecturaWAV::Leer(char *Dest, __int32 cantsamples, __int16 nchannels)
{
  if(!Origen || Origen.eof())
    return false;
  Origen.read(Dest, cantsamples*numbytespersample*nchannels);
  return true;
}
bool ArchivoLecturaWAV::Leer(char *Dest, __int32 cantsamples, __int16 nchannels,
                                                                   __int16 spos)
{
  if(!Origen || Origen.eof())
    return false;
  if(spos > numsamples)
    return false;
  Origen.seekg(spos*numbytespersample + posinit);
  Origen.read(Dest, cantsamples*numbytespersample*nchannels);
  return true;
}
bool ArchivoLecturaWAV::IrInicio(void)
{
  if(!Origen)
    return false;
  if((!numsamples)||(!posinit))
    return false;

  Origen.seekg(posinit);
  return true;
}
void ArchivoLecturaWAV::Cerrar(void)
{
  if(!Origen)
    return;
  Origen.close();
}

__int16 ArchivoLecturaWAV::NumChannels(void)
{
  return numchannels;
}
__int32 ArchivoLecturaWAV::SampleRate(void)
{
  return samplerate;
}
__int16 ArchivoLecturaWAV::NumBitsPerSample(void)
{
  return numbitspersample;
}
__int16 ArchivoLecturaWAV::NumBytesPerSample(void)
{
  return numbytespersample;
}


// Funciones de ArchivoEscrituraWAV
bool ArchivoEscrituraWAV::Abrir(char *Nombre)
{
  if(!Destino)
  {
    Destino.open(Nombre, ios_base::binary);
    if(!Destino)
      return false;
    return true;
  }
  return false;
}
bool ArchivoEscrituraWAV::EstablecerEncabezado(__int16 nnumchannels,
                                 __int32 nsamplerate, __int16 nnumbitspersample)
{
  if(establec)
    return false;
  numchannels = nnumchannels;
  samplerate = nsamplerate;
  numbitspersample = nnumbitspersample;
  avgbytespersec = (numchannels*samplerate*numbitspersample)/8;
  block = (numchannels*numbitspersample)/8;
  if(numbitspersample <= 8)
    numbytespersample = 1;
  else if(numbitspersample <= 16)
    numbytespersample = 2;
  else
    return false;
  establec = true;
  return true;
}
bool ArchivoEscrituraWAV::EscribirEncabezado(void)
{
  if(!establec)
    return false;
  if(!Destino)
    return false;
  if(escrito)
    return false;

  char str[4];

  posinit = 0;
  Destino.write("WAVE", 4);
  posinit += 4;

  Destino.write("fmt ", 4);
  posinit += 4;

  Decalc(str, 16, 4);
  Destino.write(str, 4);
  posinit += 4;

  Decalc(str, 1, 2);
  Destino.write(str, 2);
  posinit += 2;

  Decalc(str, numchannels, 2);
  Destino.write(str, 2);
  posinit += 2;

  Decalc(str, samplerate, 4);
  Destino.write(str, 4);
  posinit += 4;

  Decalc(str, avgbytespersec, 4);
  Destino.write(str, 4);
  posinit += 4;

  Decalc(str, block, 2);
  Destino.write(str, 2);
  posinit += 2;

  Decalc(str, numbitspersample, 2);
  Destino.write(str, 2);
  posinit += 2;

  Destino.write("data", 4);
  posinit += 4;


  return true;
}
bool ArchivoEscrituraWAV::Escribir(char *Orig, __int32 cantsamples,
                                              __int32 numchannels, __int16 spos)
{
  if(!escrito)
    return false;
  if(!Destino)
    return false;
  if(spos + cantsamples*numchannels > numsamples)
    return false;

  Destino.seekp(spos*numbytespersample + posinit);
  Destino.write(Orig, cantsamples*numbytespersample*numchannels);
  numsamples += numchannels*cantsamples;
  return true;
}
bool ArchivoEscrituraWAV::Escribir(char *Orig, __int32 cantsamples,
                                                            __int32 numchannels)
{
  if(!escrito)
    return false;
  if(!Destino)
    return false;

  Destino.write(Orig, cantsamples*numbytespersample*numchannels);
  numsamples += numchannels*cantsamples;
  return true;
}
bool ArchivoEscrituraWAV::Escribir(char *Orig, __int32 cantsamples)
{
  if(!escrito)
    return false;
  if(!Destino)
    return false;

  Destino.write(Orig, cantsamples*numbytespersample);
  numsamples += cantsamples;
  return true;
}
void ArchivoEscrituraWAV::Cerrar(void)
{
  if(!Destino)
    return;
  Destino.close();
}

