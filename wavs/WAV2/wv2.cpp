
#pragma hdrstop
#include <stdio.h>
#include <fstream.h>
//#include <condefs.h>
#include "wavsrc.h"
//---------------------------------------------------------------------------
//USEUNIT("wavsrc.cpp");
//---------------------------------------------------------------------------
//#pragma argsused
// Versi�n con wavsrc
int main(int argc, char* argv[])
{
  ArchivoLecturaWAV Origen;
  ArchivoEscrituraWAV Destino;
  char str[4];


  if(argc < 3)
  {
    cerr << "Uso:  dav ORIGEN.WAV DESTINO.WAV" << endl;
    return 1;
  }

  // Abro el archivo de origen
  if(!Origen.Abrir(argv[1]))
  {
    cerr << "Error al abrir el archivo de origen." << endl;
    return 1;
  }

  // Abro el archivo de destino
  if(!Destino.Abrir(argv[2]))
  {
    cerr << "Error al abrir el archivo de destino." << endl;
    Origen.Cerrar();
    return 1;
  }

  // Obtengo los datos importantes y los reescribo en el destino
  if(!Origen.LeerEncabezado())
  {
    cerr << "El archivo de origen est� err�neo." << endl;
    Origen.Cerrar();
    Destino.Cerrar();
    return 1;
  }
  if(!Destino.EstablecerEncabezado(Origen.NumChannels(), Origen.SampleRate(),
                                                     Origen.NumBitsPerSample()))
  {
    cerr << "Los datos del archivo de origen son inv�lidos." << endl;
    Origen.Cerrar();
    Destino.Cerrar();
    return 1;
  }
  if(!Destino.EscribirEncabezado())
  {
    cerr << "Error al escribir el encabezado en el archivo de destino." << endl;
    Origen.Cerrar();
    Destino.Cerrar();
    return 1;
  }

  __int32 numsamples = Origen.CalcularNS();
  if(!numsamples)
  {
    cerr << "Error al calcular numero de muestras o no hay muestras." << endl;
    Origen.Cerrar();
    Destino.Cerrar();
    return 1;
  }
  
  if(!Origen.IrInicio())
  {
    cerr << "Error al ubicar el inicio en el archivo de origen." << endl;
    Origen.Cerrar();
    Destino.Cerrar();
    return 1;
  }


  // Aqu� empieza el progama, por fin!
  __int32 sample1[4];
  __int32 sample2[4];
  __int32 samplef[4];
  int num;
  bool term;

  num = 0;
  term = false;
  do
  {
    // Lee dos muestras del primer archivo
    for(int i = 0; i < Origen.NumChannels(); i++)
    {
      Origen.Leer(str, 1);
      sample1[i] = Calc(str, Origen.NumBytesPerSample());
    }
    num += Origen.NumChannels();
    if(num != numsamples)
    {
      for(int i = 0; i < Origen.NumChannels(); i++)
      {
        Origen.Leer(str, 1);
        sample2[i] = Calc(str, Origen.NumBytesPerSample());
      }
      num += Origen.NumChannels();
    } else
      term = true;

    if(!term)
    {
      // Las une en una
      __int32 comp = 1;

      for(int i = 0; i < Origen.NumChannels(); i++)
        samplef[i] = 0;

      for(int j = 0; j < Origen.NumChannels(); j++)
        for(int i = 0; i < Origen.NumBitsPerSample(); i++)
        {
          if(comp & sample1[j])
            samplef[j] ^= comp;
          i++;
          if(i == Origen.NumBitsPerSample())
            break;
          comp <<= 1;
          if(comp & sample2[j])
            samplef[j] ^= comp;
        }
    } else
      // Copia la primera nom�s
      for(int i = 0; i < Origen.NumChannels(); i++)
        samplef[i] = sample1[i];

    // Escribe la nueva muestra
    for(int i = 0; i < Origen.NumChannels(); i++)
    {
      // Prueba
      if(!(num % 2))
        Decalc(str, sample1[i], Origen.NumBytesPerSample());
      else
        Decalc(str, sample2[i], Origen.NumBytesPerSample());

      // Esto queda mal�simo, jaja
      //Decalc(str, samplef[i], numbytespersample);
      Destino.Escribir(str, 1);
    }
  } while(num < numsamples);

  Origen.Cerrar();
  Destino.Cerrar();
  return 0;
}

