//
//   makewav.cpp - by Don Cross <dcross@intersrv.com>
//
//   Get latest version of this file at:
//   http://www.intersrv.com/~dcross/makewav
//
//   Get latest version of C++ code for class WaveFile at:
//   http://www.intersrv.com/~dcross/wavio.html
//   

#include <stdio.h>
#include <stdlib.h>
#include <fstream.h>

#include "riff.h"


const int MAX_CHANNELS = 4;   // make larger if needed


int main ( int argc, const char *argv[] )
{
    if ( argc != 6 )
    {
        cerr <<
            "Use:  MAKEWAV inFile outWave samplingRate bitsPerSample numChannels" <<
            endl;

        return 1;
    }

    const char *inFilename = argv[1];
    ifstream infile ( inFilename );
    if ( !infile )
    {
        cerr << "Cannot open file '" << inFilename << "' for read." << endl;
        return 1;
    }

    const char *outFilename = argv[2];

    long samplingRate = atol ( argv[3] );
    int bitsPerSample = atoi ( argv[4] );

    int numChannels = atoi ( argv[5] );
    if ( numChannels > MAX_CHANNELS )
    {
        cerr << "Number of channels given is too large" << endl;
        return 1;
    }

    WaveFile outWave;
    DDCRET rc = outWave.OpenForWrite (
        outFilename,
        samplingRate,
        bitsPerSample,
        numChannels );

    if ( rc != DDC_SUCCESS )
    {
        cerr <<
            "Error creating output WAV file '" << outFilename << "'" <<
            endl;

        return 1;
    }

    INT16 data [MAX_CHANNELS];
    int keepGoing = 1;
    while ( keepGoing )
    {
        for ( int c=0; c < numChannels; ++c )
        {
            infile >> data[c];
            if ( !infile )
            {
                keepGoing = 0;
                break;
            }
        }

        while ( c < numChannels )
            data[c++] = 0;

        if ( bitsPerSample == 8 )
        {
            UINT8 data8 [MAX_CHANNELS];
            for ( c=0; c < numChannels; ++c )
            {
                if ( data[c] < 0 || data[c] > 255 )
                {
                    cerr << "Error:  data did not fit in 8 bits!" << endl;
                    return 1;
                }
                data8[c] = UINT8 ( data[c] );
                rc = outWave.WriteData ( data8, numChannels );
            }
        }
        else if ( bitsPerSample == 16 )
        {
            rc = outWave.WriteData ( data, numChannels );
        }
        else
        {
            cerr << "Don't know how to process " << bitsPerSample <<
                "bits per sample." << endl;

            return 1;
        }

        if ( rc != DDC_SUCCESS )
        {
            cerr <<
                "Error writing data to output file '" <<
                outFilename <<
                "'." <<
                endl;

            return 1;
        }
    }

    rc = outWave.Close();
    if ( rc != DDC_SUCCESS )
    {
        cerr << "Error closing output WAV file '" << outFilename << "'" << endl;
        return 1;
    }

    return 0;
}


