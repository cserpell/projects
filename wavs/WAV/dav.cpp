
#pragma hdrstop
#include <stdio.h>
#include <fstream.h>
/*#include <string.h>
#include <condefs.h>*/
#include "riff.h"

//---------------------------------------------------------------------------
/*USEUNIT("Riff.cpp");
USEUNIT("Ddc.cpp");*/
//---------------------------------------------------------------------------
#pragma argsused
int main(int argc, char *argv[])
{
  WaveFile Origen;
  WaveFile Destino;
//  string temp;
  DDCRET ret;

  if(argc < 3)
  {
    cerr << "Uso:  dav ORIGEN.WAV DESTINO.WAV" << endl;
    return 1;
  }

  // Abro el archivo de origen
  ret = Origen.OpenForRead(argv[1]);
  if(ret != DDC_SUCCESS)
  {
    cerr << "Error al abrir el archivo de origen." << endl;
    return 1;
  }
  // Obtengo los datos importantes
  UINT32 samplerate = Origen.SamplingRate();    // [Hz]
  UINT16 bitspersample = Origen.BitsPerSample();
  UINT16 numchannels = Origen.NumChannels();
  UINT32 numsamples = Origen.NumSamples();

  // Abro el archivo de destino
  ret = Destino.OpenForWrite(argv[2], samplerate, bitspersample, numchannels);
  if(ret != DDC_SUCCESS)
  {
    cerr << "Error al abrir el archivo de destino." << endl;
    return 1;
  }

  INT16 m1;
  INT16 m2;
  INT16 mf;
  UINT32 num = 0;
  bool term = false;

  do
  {
    // Lee dos muestras del primer archivo
    ret = Origen.ReadMonoSample(&m1);
    if(ret != DDC_SUCCESS)
    {
      cerr << "Error al leer el archivo de origen." << endl;
      return 1;
    }
    num++;
    if(num != numsamples)
    {
      ret = Origen.ReadMonoSample(&m2);
      if(ret != DDC_SUCCESS)
      {
        cerr << "Error al leer el archivo de origen." << endl;
        return 1;
      }
      num++;
    } else
      term = true;

    // Las une en una
    if(!term)
    {
      INT16 comp = 1;

      mf = 0;
      for(int i = 0; i < bitspersample; i++)
      {
        if(comp & m1)
          mf ^= comp;
        i++;
        if(i == bitspersample)
          break;
        comp <<= 1;
        if(comp & m2)
          mf ^= comp;
      }
    }
    else
      mf = m1;

    // Escribe la nueva muestra
    ret = Destino.WriteMonoSample(mf);
    if(ret != DDC_SUCCESS)
    {
      cerr << "Error al escribir el archivo de destino." << endl;
      return 1;
    }
  } while(num < numsamples);
  Origen.Close();
  Destino.Close();
  return 0;
}
