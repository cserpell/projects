//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
const int Mx = 400;
const int My = 400;
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TPaintBox *PaintBox1;
        TButton *Button1;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *Edit1;
        TLabel *Label3;
        TButton *Button2;
        TLabel *Label4;
        TButton *Button3;
        TEdit *Edit2;
        TLabel *Label5;
        TEdit *Edit3;
        TCheckBox *CheckBox1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall PaintBox1Paint(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
        double ObtenerDatos(void);
        void EnArchivo(void);
        int tamx;
        int tamy;
        double divd;
        Graphics::TBitmap *bmp; // *****
        Byte aplicarpto(double h, int x, int y, Byte Arreglo[Mx][My], int t);
        Byte aplicarpto2(double h, int x, int y, Byte Arreglo[Mx][My], int t);
        Byte aplicarptomed(double h, int x, int y, Byte Arreglo[Mx][My], int t);
public:	     	// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
