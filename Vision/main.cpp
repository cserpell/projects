//---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#include <fstream.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
  bmp = new Graphics::TBitmap;           // *****
}
//---------------------------------------------------------------------------
// SE LLAMAN CON X^2
double func(double h, double x)
{
  return h*exp(-h*h*x)*0.564189583547756286948; // M_1_SQRTPI
}
//---------------------------------------------------------------------------
double func2(double h, double x)
{
  return -h*h*h*(2*h*h*x - 1)*exp(-h*h*x)*1.12837916709551257390; // M_2_SQRTPI
}
//---------------------------------------------------------------------------
double CalcInt(double h, int t)
{
  double sum = 0;

  for(int ix = -t; ix <= t; ix++)
    for(int iy = -t; iy <= t; iy++)
    {
      int d = ix*ix + iy*iy;

      if(sqrt(d) < t)
        sum += func(h, d);
    }
  return sum;
}
//---------------------------------------------------------------------------
Byte TForm1::aplicarpto(double h, int x, int y, Byte arreglo[Mx][My], int t)
{
  int minx = x - t;
  int miny = y - t;
  int maxx = x + t;
  int maxy = y + t;
  double valorpto = 0;

  if(minx < 0)
    minx = 0;
  if(maxx >= tamx)
    maxx = tamx - 1;
  if(miny < 0)
    miny = 0;
  if(maxy >= tamy)
    maxy = tamy - 1;
  for(int ix = minx; ix <= maxx; ix++)
    for(int iy = miny; iy <= maxy; iy++)
    {
      int d = (x - ix)*(x - ix) + (y - iy)*(y - iy);

      if(sqrt(d) < t)
        valorpto += func(h, d)*arreglo[ix][iy];
    }
  return (valorpto/divd);
}
//---------------------------------------------------------------------------
Byte TForm1::aplicarptomed(double h, int x, int y, Byte arreglo[Mx][My], int t)
{
  int minx = x - t;
  int miny = y - t;
  int maxx = x + t;
  int maxy = y + t;
  double valorpto = 0;

  if(minx < 0)
    minx = 0;
  if(maxx >= tamx)
    maxx = tamx - 1;
  if(miny < 0)
    miny = 0;
  if(maxy >= tamy)
    maxy = tamy - 1;

  int mmy = maxy - miny;
  int mmx = maxx - minx;
  int mmm = mmx*mmy;
  int Arreg[20*20];
  int n = 0;

  for(int i = 0; i < mmm; i++)
    Arreg[i] = 0;
  for(int ix = minx; ix <= maxx; ix++)
    for(int iy = miny; iy <= maxy; iy++)
    {
      n++;
      for(int i = 0; i < n + 1; i++)
        if(Arreg[i] < arreglo[ix][iy])
        {
          for(int j = n + 1; j > i; j--)
            Arreg[j] = Arreg[j - 1];
          Arreg[i] = arreglo[ix][iy];
          break;
        }
    }
  return Arreg[n/2];
}
//---------------------------------------------------------------------------
double CalcInt2(double h, int t)
{
  double sum = 0;

  for(int ix = -t; ix <= t; ix++)
    for(int iy = -t; iy <= t; iy++)
    {
      int d = ix*ix + iy*iy;

      if(sqrt(d) < t)
        sum += func2(h, d);
    }
  return sum;
}
//---------------------------------------------------------------------------
Byte TForm1::aplicarpto2(double h, int x, int y, Byte arreglo[Mx][My], int t)
{
  int minx = x - t;
  int miny = y - t;
  int maxx = x + t;
  int maxy = y + t;
  double valorpto = 0;

  if(minx < 0)
    minx = 0;
  if(maxx >= tamx)
    maxx = tamx - 1;
  if(miny < 0)
    miny = 0;
  if(maxy >= tamy)
    maxy = tamy - 1;
  for(int ix = minx; ix <= maxx; ix++)
    for(int iy = miny; iy <= maxy; iy++)
    {
      int d = (x - ix)*(x - ix) + (y - iy)*(y - iy);

      if(sqrt(d) < t)
        valorpto += func2(h, d)*arreglo[ix][iy];
    }
  return (valorpto/divd);
}
//---------------------------------------------------------------------------
double TForm1::ObtenerDatos(void)
{
  double h;

  h = StrToFloat(Edit3->Text);       // *****
  bmp->LoadFromFile(Edit2->Text);    // *****
  bmp->PixelFormat = pf32bit;        // *****
  tamx = bmp->Width;                 // *****
  tamy = bmp->Height;                // *****
  Label1->Caption = IntToStr(tamy);  // *****
  Label2->Caption = IntToStr(tamx);  // *****
  PaintBox1->Height = tamy;          // *****
  PaintBox1->Width = tamx;           // *****
  return h;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  Byte Arreglo1[Mx][My];
  Byte Arreglo2[Mx][My];
  Byte Arreglo3[Mx][My];
  Byte Arreglo4[Mx][My];
  double h = ObtenerDatos();
  int t;

  t = StrToInt(Edit1->Text);          // *****
  if(tamy > My || tamx > Mx)
    return;
  divd = CalcInt(h, t);
  for(int y = 0; y < tamy; y++)  // Lleno el arreglo
  {
    int *p = (int *)bmp->ScanLine[y]; // *****

    for(int x = 0; x < tamx; x++)
    {
      Arreglo1[x][y] = (Byte)(p[x] & 0x0000FF);
      Arreglo2[x][y] = (Byte)((p[x] & 0x00FF00) >> 8);
      Arreglo3[x][y] = (Byte)((p[x] & 0xFF0000) >> 16);
      Arreglo4[x][y] = (Byte)((p[x] & 0xFF000000) >> 24);
    }
  }
  if(!CheckBox1->Checked)
  {
  for(int y = 0; y < tamy; y++)
  {
    int *p = (int *)bmp->ScanLine[y]; // *****

    for(int x = 0; x < tamx; x++)
      p[x] = aplicarpto(h, x, y, Arreglo1, t)
           + (aplicarpto(h, x, y, Arreglo2, t) << 8)
           + (aplicarpto(h, x, y, Arreglo3, t) << 16)
           + (aplicarpto(h, x, y, Arreglo4, t) << 24);
  }
  } else
  {
  for(int y = 0; y < tamy; y++)
  {
    int *p = (int *)bmp->ScanLine[y]; // *****

    for(int x = 0; x < tamx; x++)
      p[x] = aplicarptomed(h, x, y, Arreglo1, t)
           + (aplicarptomed(h, x, y, Arreglo2, t) << 8)
           + (aplicarptomed(h, x, y, Arreglo3, t) << 16)
           + (aplicarptomed(h, x, y, Arreglo4, t) << 24);
  }
  }
  PaintBox1Paint(this);               // *****
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBox1Paint(TObject *Sender)
{
  PaintBox1->Canvas->Draw(0, 0, bmp); // *****
}
//---------------------------------------------------------------------------
void TForm1::EnArchivo(void)
{
  Byte Arreglo[Mx][My];
  double h = 0.707106781186547524401; // M_SQRT_2
//  double h = 1;
  int t;
  ifstream Entrada;
  ofstream Salida;
  char str[20];

  try
  {
    Entrada.open("in.txt");
    Entrada >> str;
    t = StrToInt(str);                  // *****
    Entrada >> str;
    tamx = StrToInt(str);               // *****
    Entrada >> str;
    tamy = StrToInt(str);               // *****
    if(tamy > My || tamx > Mx)
    {
      Label4->Caption = "Tama�o fuera de rango.\n";
      return;
    }
    divd = CalcInt(h, t);

    for(int y = 0; y < tamy; y++)  // Lleno el arreglo
      for(int x = 0; x < tamx; x++)
      {
        Entrada >> str;
        Arreglo[x][y] = (Byte)StrToInt(str);  // *****
      }
    Entrada.close();
  } catch(...)
  {
    Label4->Caption = "Error en el archivo de entrada 'in.txt'.\n"; // *****
    Entrada.close();
    return;
  }
  try
  {
    Salida.open("out.txt");
    for(int y = 0; y < tamy; y++)  // Lleno el arreglo
    {
      for(int x = 0; x < tamx; x++)
      {
        Salida << IntToStr(aplicarpto(h, x, y, Arreglo, t)).c_str(); // *****
        Salida << " ";
      }
      Salida << "\n";
    }
    Salida.close();
  } catch(...)
  {
    Label4->Caption = "Error en el archivo de salida 'out.txt'.\n"; // *****
    Salida.close();
    return;
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
  EnArchivo();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  Byte Arreglo1[Mx][My];
  Byte Arreglo2[Mx][My];
  Byte Arreglo3[Mx][My];
  double h = ObtenerDatos();
  int t;

  t = StrToInt(Edit1->Text);         // *****
  if(tamy > My || tamx > Mx)
    return;
  divd = CalcInt2(h, t);
  for(int y = 0; y < tamy; y++)  // Lleno el arreglo
  {
    int *p = (int *)bmp->ScanLine[y]; // *****

    for(int x = 0; x < tamx; x++)
    {
      Arreglo1[x][y] = (Byte)(p[x] & 0x0000FF);
      Arreglo2[x][y] = (Byte)((p[x] & 0x00FF00) >> 8);
      Arreglo3[x][y] = (Byte)((p[x] & 0xFF0000) >> 16);
    }
  }
  for(int y = 0; y < tamy; y++)
  {
    int *p = (int *)bmp->ScanLine[y]; // *****

    for(int x = 0; x < tamx; x++)
    {
      int a = aplicarpto2(h, x, y, Arreglo1, t);
      int b = aplicarpto2(h, x, y, Arreglo2, t);
      int c = aplicarpto2(h, x, y, Arreglo3, t);
      if(sqrt(a*a + b*b + c*c) < 32)
        p[x] = 0;
      else
        p[x] = 0xFFFFFF;
    }
  }
  PaintBox1Paint(this);               // *****
}
//---------------------------------------------------------------------------

