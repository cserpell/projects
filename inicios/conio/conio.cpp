//---------------------------------------------------------------------------
#include <conio.h>
#include <condefs.h>
#pragma hdrstop
#pragma argsused
//---------------------------------------------------------------------------
int main( int argc, char* argv[] )
{
   char c;
   short buf[16 * 16];
   int i, j;

   clrscr();
   textmode( C4350 );
   
   gotoxy( 12, 1 );
   cprintf( "PROGRAMA DE PRUEBA DE LAS FUNCIONES DE CONIO.H" );

   gotoxy( 7, 3 );
   cprintf( "COLORES" );

   gotoxy( 3, 5 );
   cprintf( "0123456789012345" );
   for( i = 6; i < 22; i++ )
   {
      gotoxy( 2, i );
      if( i < 16 )
         cprintf( "%c", ( i + 42 ) );
      else
         cprintf( "%c", ( i + 32 ) );
   }
   for( i = 0; i < 16; i++ )
   {
      for( j = 0; j < 16; j++ )
         buf[( i * 16 ) + j] = ( short )( 'A' + ( j * 0x100 ) + ( i * 0x1000 ) );
   }
   puttext( 3, 6, 18, 21, buf );

   textcolor( BLACK );
   textbackground( LIGHTGRAY );

   gotoxy( 29, 5 );
   cprintf( "Las letras 'A' han sido escritas en un buffer que " );
   gotoxy( 29, 6 );
   cprintf( "ha sido llevado a la pantalla con la funcion      " );
   gotoxy( 29, 7 );
   cprintf( "puttext( __left, __top, __right, __bottom,        " );
   gotoxy( 29, 8 );
   cprintf( "__source ). El color se puso sumando al caracter  " );
   gotoxy( 29, 9 );
   cprintf( "'A' el numero de color (0 a 15) * 0x100 para la   " );
   gotoxy( 29, 10 );
   cprintf( "letra y * 0x1000 para el fondo. Ademas el color de" );
   gotoxy( 29, 11 );
   cprintf( "este texto se cambio con textcolor( __newcolor ) y" );
   gotoxy( 29, 12 );
   cprintf( "textbackground( __newcolor ). El cursor se mueve  " );
   gotoxy( 29, 13 );
   cprintf( "con gotoxy( __x, __y ). Cada vez que se presiona  " );
   gotoxy( 29, 14 );
   cprintf( "la barra, que es leida con getch(),  el texto se  " );
   gotoxy( 29, 15 );
   cprintf( "se mueve gracias a la funcion movetext( __left,   " );
   gotoxy( 29, 16 );
   cprintf( "__top, __right, __bottom, __destleft, __desttop )." );
   gotoxy( 29, 17 );
   cprintf( "La pantalla fue borrada al empezar con clrscr().  " );
   gotoxy( 29, 18 );
   cprintf( "El modo fue cambiado con textmode( newmode ).     " );
   gotoxy( 29, 19 );
   cprintf( "Ademas se creo una ventana para escribir con      " );
   gotoxy( 29, 20 );
   cprintf( "window( __left, __top, __right, __bottom ), donde " );
   gotoxy( 29, 21 );
   cprintf( "cada caracter se escribe con putch( __c ). La     " );
   gotoxy( 29, 22 );
   cprintf( "posicion del cursor se puede saber con wherex() y " );
   gotoxy( 29, 23 );
   cprintf( "wherey(). El cursor fue cambiado con la funcion   " );
   gotoxy( 29, 24 );
   cprintf( "_setcursortype( __cur_t ). Para salir presiona ESC" );

   textcolor( RED );
   textbackground( CYAN );
   _setcursortype( _SOLIDCURSOR );
   window( 10, 26, 50, 45 );

   i = 0;
   do
   {
      c = ( char )getch();
      if( c == 0x20 )
      {
         if( i == 0 )
            movetext( 28, 4, 79, 25, 27, 3 );
         if( i == 1 )
            movetext( 27, 3, 78, 24, 28, 4 );
         if( i == 0 )
            i = 1;
         else
            i = 0;
      } else
         putch( c );

   } while ( c != 0x1B );

   textmode( _ORIGMODE );
   return 0;
}
//---------------------------------------------------------------------------
