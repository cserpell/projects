// Esta es la Unit de la Linea!
//---------------------------------------------------------------------------
#include <string.stl>
#pragma hdrstop

#include "linea.h"
#pragma package(smart_init)

using namespace std;
//---------------------------------------------------------------------------
// Retorna el Caracter de la posici�n pos
char Linea::EntregaCaracter( int pos )
{
   if( pos < 0 ) pos = 0;
   if( pos > largo ) pos = largo;

   return datos[pos];
}

// Agrega el caracter c en la posici�n pos en Linea
void Linea::AgregaCaracter( int pos, char c )
{
   if( pos < 0 ) pos = 0;
   if( pos >= largo )
      datos.append( 1, c );
   else
      datos.insert( pos, 1,  c );
   largo++;
}

// Agrega el string s en la posici�n pos en Linea
void Linea::AgregaString( int pos, string s )
{
   if( pos < 0 ) pos = 0;
   if( pos >= largo )
      datos += s;
   else
      datos.insert( pos, s );
   largo += s.length();
}

// Elimina num caracteres de la posici�n pos de Linea
void Linea::EliminaCaracter( int pos, int num )
{
   if( pos < 0 ) return;
   if( pos >= largo ) return;

   if( ( num + pos ) > largo ) num = ( largo - pos );

   datos.erase( pos, num );
   largo -= num;
}

// Retorna el largo de Linea
int Linea::EntregaLargo()
{
   return largo;
}

//Retorna los datos de la linea
string Linea::Datos()
{
   return datos;
}
//---------------------------------------------------------------------------
