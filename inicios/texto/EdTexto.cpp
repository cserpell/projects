//
//  Programa de TEXTO en C++
//
// Cristi�n Serpell
//  Diciembre 1999 a Enero 2000
//
//---------------------------------------------------------------------------
#include <condefs.h>
#pragma hdrstop

#include "ctrltext.h"

USEUNIT("linea.cpp");
USEUNIT("texto.cpp");
USEUNIT("Pantalla.cpp");
USEUNIT("ctrltext.cpp");
//---------------------------------------------------------------------------
// EL PROGRAMA PRINCIPAL!!!!!!!!!
int main(/* int argc, char* argv[] */)
{
   int c = 0, textoactual = 0, TAB = 8;
   bool must_end = FALSE;
   ControlTexto ctrltext;

   do
   {
      // Escribe la pantalla y la muestra
      ctrltext.ModificarBarra( "F1:Ayuda" );
      ctrltext.EscribirBarra( textoactual, c, 9 );
      ctrltext.MostrarTexto( textoactual );

      // Lee el teclado
      c = ctrltext.LeeTecla();

      switch( c )
      {
        // TECLAS PARA MOVER EL CURSOR
        case 0x4800: // Arriba
         ctrltext.Arriba( textoactual );
         break;

        case 0x5000: // Abajo
         ctrltext.Abajo( textoactual );
         break;

        case 0x4B00: // Izquierda
         ctrltext.Izquierda( textoactual );
         break;

        case 0x4D00: // Derecha
         ctrltext.Derecha( textoactual );
         break;

        case 0x4900: // Pag. Arriba
         ctrltext.Arriba( textoactual, difv );
         break;

        case 0x5100: // Pag. Abajo
         ctrltext.Abajo( textoactual, difv );
         break;

        case 0x4700: // Inicio
         ctrltext.Inicio( textoactual );
         break;

        case 0x4F00: // Fin
         ctrltext.Fin( textoactual );
         break;

        case 0x7500: // Ctrl. Fin
         ctrltext.CtrlFin( textoactual );
         break;

        case 0x7700: // Ctrl. Inicio
         ctrltext.CtrlInic( textoactual );
         break;

        // TECLAS PARA BORRAR
        case 0x08:   // Backspace
         ctrltext.BackSpace( textoactual );
         break;

        case 0x5300: // Suprimir
         ctrltext.Supr( textoactual );
         break;

        // TECLAS ESPECIALES
        case 0x5200: // Insert
         ctrltext.Insert();
         break;

        case 0x09:   // TAB
         ctrltext.TAB( textoactual, TAB );
         break;

        case 0x0D:   // Enter
         ctrltext.Enter( textoactual );
         break;

        // Fs
        case 0x3B00: // F1 -- Ayuda
         ctrltext.ModificarBarra( "F1:Mas Ayuda  F2:Nuevo  F3:Buscar  F4:Reemplazar" );
         ctrltext.MuestraBarra();
         if( ctrltext.LeeTecla() == c )
         {
            ctrltext.ModificarBarra( "F1:Mas Ayuda  F5:Cargar  F6:Grabar  F7:Grabar Como  F8:Salir" );
            ctrltext.MuestraBarra();
            if( ctrltext.LeeTecla() == c )
            {
               ctrltext.ModificarBarra( "Ventanas:  F9:Cerrar  F10:Anterior  F11:Siguiente" );
               ctrltext.MuestraBarra();
               ctrltext.LeeTecla();
            }
         }
         break;

        case 0x3C00: // F2 -- Nuevo
         textoactual = ctrltext.Nuevo( textoactual );
         break;

        case 0x3D00: // F3 -- Buscar
         ctrltext.Buscar( textoactual, c );
         break;

        case 0x3E00: // F4 -- Reemplazar
         ctrltext.Buscar( textoactual, c, TRUE );
         break;

        case 0x3F00: // F5 -- Cargar Archivo
         textoactual = ctrltext.Cargar( textoactual );
         break;

        case 0x4000: // F6 -- Guardar Archivo
         ctrltext.Guardar( textoactual );
         break;

        case 0x4100: // F7 -- Guardar Como Archivo
         ctrltext.Guardar( textoactual, TRUE );
         break;

        case 0x4200: // F8 -- Salir
         for( int i = 0; i < ctrltext.NumeroTextos(); i++ )
         {
            if( ctrltext.CompruebaMod( i ) == 0 )
            {
               must_end = 0;
               break;
            }
         }
         must_end = 1;
         break;

        case 0x4300: // F9 -- Cerrar Ventana
         if( ctrltext.NumeroTextos() != 1 )
         {
            if( ctrltext.CompruebaMod( textoactual ) )
               ctrltext.EliminaTexto( textoactual );
            if( textoactual >= ctrltext.NumeroTextos() )
               textoactual = ( ctrltext.NumeroTextos() - 1 );
         }
         break;

        case 0x4400: // F10 -- Anterior Ventana
         textoactual--;
         if( textoactual < 0 )
            textoactual = ( ctrltext.NumeroTextos() - 1 );
         break;

        case 0x8500: // F11 -- Siguiente Ventana
         textoactual++;
         if( textoactual >= ctrltext.NumeroTextos() )
            textoactual = 0;
         break;

        // TECLAS NORMALES
        default:
         if( c < 0x0100 )
            ctrltext.AgregarCaracter( textoactual, ( char )c );
         break;
      }

      // Continua o sale
   } while( must_end == FALSE );

   return 0;
}
//---------------------------------------------------------------------------
