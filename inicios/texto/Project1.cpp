//
//  Programa de TEXTO en C++
//
#include <iostream.h>
#include <fstream.h>
#include <string.stl>
#include <conio.h>
#pragma hdrstop

#include <condefs.h>

using namespace std;

//---------------------------------------------------------------------------
class Linea
{
   int largo;
   string datos;
  public:
   void CargaDesdeArchivo( istream is );
   void AgregaCaracter( int pos, char c );
   void EliminaCaracter( int pos );
   Linea() {
      largo = 0;
      datos = "";
   }
};

#define MAX_LINEAS (10000)

class Texto
{
   int num_lineas;
   Linea *lin[MAX_LINEAS];

  public:
   void CargaDesdeArchivo( istream is );
   void AgregaLinea( int pos );
   void EliminaLinea( int pos );
   int NumeroLineas();
   Linea * linea(int n);
   Texto() {
      num_lineas=0;
      AgregaLinea(0);
   }
};

void Linea::CargaDesdeArchivo( istream is )
{


}

void Linea::AgregaCaracter( int pos, char c )
{
   if(pos<0) pos = 0;
   if(pos>largo) pos = largo;

   datos.insert(pos, 1,  c );

}

void Linea::EliminaCaracter( int pos )
{
   if(pos<0) return;
   if(pos>=largo) return;

   datos.erase(pos,1);

}


void Texto::CargaDesdeArchivo( istream is )
{


}

void Texto::AgregaLinea( int pos )
{
   if(pos<0) pos = 0;
   if(pos>num_lineas) pos = num_lineas;

   int i;
   for(i = num_lineas; i>pos; i--)
   {
      lin[i] = lin[i-1];
   }
   lin[pos] = new Linea();
   num_lineas ++;
}

void Texto::EliminaLinea( int pos )
{
   if(pos<0) return;
   if(pos>=num_lineas) return;

   delete lin[pos];
   num_lineas--;

   int i;
   for(i = pos; i<num_lineas; i++)
   {
      lin[i] = lin[i+1];
   }
}

Linea * Texto::linea( int n )
{
   if(n<0) n = 0;
   if(n>=num_lineas) n = num_lineas-1;

   return lin[n];
}

int Texto::NumeroLineas()
{
   return num_lineas;
}

int main(int argc, char* argv[])
{
   ifstream fil;
   Texto mitexto;
   int c,x,y,px,py;

   do
   {
      // Lee teclado
      c=(getch()&0xFF);
      if(!c)
        c = (getch()&0xFF) * 256;

      printf("Tecla: %04x\n",c);

      // Realiza la acci�n
      switch( c )
      {
         case 0x4800: // Arriba

         case 0x5000: // Abajo

         case 0x4B00: // Derecha

         case 0x4D00: // Izquierda

         case 0x4900: // Pag. Arriba

         case 0x5100: // Pag. Abajo

         case 0x0D:   // Enter

         case 0x08:   // Backspace

         case 0x2E:   // Delete

         break;
         default:     // Tecla normal!!!

      }

      // Escribe a la pantalla


      // Continua
   } while (1);

   return 0;
}
