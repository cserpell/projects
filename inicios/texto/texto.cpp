// Esta es la Unit del Texto!
//---------------------------------------------------------------------------
#include <string.stl>
#pragma hdrstop

#include "texto.h"
#include "linea.h"
#pragma package(smart_init)

using namespace std;
//---------------------------------------------------------------------------
// Agrega una linea a Texto en la posici�n pos
void Texto::AgregaLinea( int pos )
{
   if( pos < 0 ) pos = 0;
   if( pos >= num_lineas ) pos = ( num_lineas - 1 );

   for( int i = num_lineas; i > pos; i-- )
      lin[i] = lin[i - 1];
   lin[pos] = new Linea();
   num_lineas++;
}

// Agrega una linea a Texto en la posici�n pos, conteniendo "nueva"
void Texto::AgregaLinea( int pos, string nueva )
{
   AgregaLinea( pos );
   lin[pos]->AgregaString( 0, nueva );
}

// Elimina la linea de la posici�n pos a Texto
void Texto::EliminaLinea( int pos )
{
   if( pos < 0 ) return;
   if( pos >= num_lineas ) return;

   delete lin[pos];
   num_lineas--;

   for( int i = pos; i < num_lineas; i++ )
      lin[i] = lin[i + 1];
}

// Retorna la linea n de Texto para poder verla
Linea * Texto::linea( int n )
{
   if( n < 0 ) n = 0;
   if( n >= num_lineas ) n = ( num_lineas - 1 );

   return lin[n];
}

// Retorna el n�mero de lineas de Texto
int Texto::NumeroLineas()
{
   return num_lineas;
}

// Reinicia Texto
void Texto::Iniciar()
{
   while( num_lineas > 0 )
   {
      EliminaLinea( 0 );
   }
   AgregaLinea( 0 );
}
// Une una linea con la siguiente
void Texto::UneLineas( int pos )
{
   if( pos >= num_lineas ) pos = ( num_lineas - 1 );
   if( pos < 0 ) pos = 0;

   lin[pos]->AgregaString( lin[pos]->EntregaLargo(), lin[pos + 1]->Datos() );
   EliminaLinea( pos + 1 );
}

// Separa una linea en pos y crea la siguiente desde ah�
void Texto::SeparaLineas( int linea, int pos )
{
   if( linea < 0 ) linea = 0;
   if( linea >= num_lineas ) linea = ( num_lineas - 1 );

   if( pos < 0 ) pos = 0;

   if( pos < lin[linea]->EntregaLargo() )
   {
      AgregaLinea( ( linea + 1 ), lin[linea]->Datos().substr( pos, lin[linea]->EntregaLargo() ) );
      lin[linea]->EliminaCaracter( pos, ( lin[linea]->EntregaLargo() - pos ) );
   } else
      AgregaLinea( linea + 1 );
}

void Texto::CambiarTitulo( string nuevo )
{
   titulo = nuevo;
}

string Texto::Titulo()
{
   return titulo;
}
//---------------------------------------------------------------------------

