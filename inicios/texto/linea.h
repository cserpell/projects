//---------------------------------------------------------------------------
#ifndef lineaH
#define lineaH
//---------------------------------------------------------------------------
#include <string.stl>

using namespace std;
//---------------------------------------------------------------------------
class Linea
{
   int largo;
   string datos;
  public:
   char EntregaCaracter( int pos );
   void AgregaCaracter( int pos, char c );
   void AgregaString( int pos, string s );
   void EliminaCaracter( int pos, int num = 1 );
   string Datos();
   int EntregaLargo();
   Linea()
   {
      largo = 0;
      datos = "";
   }
};

#endif
//---------------------------------------------------------------------------
