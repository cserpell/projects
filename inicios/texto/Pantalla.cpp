// Esta es la Unit de la Pantalla!
//---------------------------------------------------------------------------
#include <conio.h>
#include <string.stl>
#pragma hdrstop

#include "Pantalla.h"
#pragma package(smart_init)

using namespace std;
//---------------------------------------------------------------------------
// Muestra la pantalla
void Pantalla::MostrarPantalla()
{
   puttext( MIN_HORIZONTAL, MIN_VERTICAL, MAX_HORIZONTAL, MAX_VERTICAL, buffer );
}

// Limpia la linea
void Pantalla::LimpiarLinea( int linea )
{
   if( linea > difv )
      linea = difv;
   if( linea < 0 ) linea = 0;

   for( int i = 0; i <= difh; i++ )
      buffer[( linea * ( difh + 1 ) ) + i] = ( ' ' + COLOR_TEXTO );
}

// Escribe en la linea el string s, encima de lo anterior
void Pantalla::EscribirEnPantalla( int linea, string s, int pos )
{
   if( linea > difv )
      linea = difv;
   if( linea < 0 ) linea = 0;
   LimpiarLinea( linea );

   if( pos > difh )
      pos = difh;
   if( pos < 0 ) pos = 0;

   if( ( int )s.length() > ( difh + 1 - pos ) )
      s.resize( difh + 1 - pos );

   for( int i = pos; i < ( int )s.length(); i++ )
      buffer[( linea * ( difh + 1 ) ) + i] = ( short )( ( s[i] & 0xFF ) + COLOR_TEXTO );
}

// Env�a el cursor al lugar indicado.
void Pantalla::MoverCursor( int x, int y )
{
   if( x < 0 ) x = 0;
   if( x > difh )
      x = difh;
   x += MIN_HORIZONTAL;

   if( y < 0 ) y = 0;
   if( y > difv )
      y = difv;
   y += MIN_VERTICAL;

   gotoxy( x, y );
}

// Limpia la pantalla
void Pantalla::LimpiaPantalla()
{
   for( int i = 0; i <= difv; i++ )
      LimpiarLinea( i );
}
//---------------------------------------------------------------------------
