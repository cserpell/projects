// Esta es la Unit de ControlTexto
//---------------------------------------------------------------------------
#include <string.stl>
#include <conio.h>
#include <fstream>
#pragma hdrstop

#include "ctrltext.h"
#include "texto.h"
#include "Pantalla.h"
#pragma package(smart_init)

using namespace std;
//---------------------------------------------------------------------------
// Retorna la cantidad de textos existentes
int ControlTexto::NumeroTextos()
{
   return num_textos;
}

// Escribe texto a la pantalla segun pl y pt
void ControlTexto::MostrarTexto( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   for( int i = 0; i < difv; i++ )
   {
      if( ( i + postexto[texto] ) < textos[texto]->NumeroLineas() )
      {
         if( poslinea[texto] < textos[texto]->linea( i + postexto[texto] )->EntregaLargo() )
            pantallas[texto]->EscribirEnPantalla( i, textos[texto]->linea( i + postexto[texto] )->Datos().substr( poslinea[texto], textos[texto]->linea( i + postexto[texto] )->EntregaLargo() ) );
         else
            pantallas[texto]->EscribirEnPantalla( i );
      } else
         pantallas[texto]->EscribirEnPantalla( i );
   }
   pantallas[texto]->MostrarPantalla();
   pantallas[texto]->MoverCursor( curx[texto], cury[texto] );
}

// Lee una tecla y la devuelve como un entero
int ControlTexto::LeeTecla()
{
   int c = ( getch() & 0xFF );

   if( !c ) c = ( ( getch() & 0xFF ) * 256 );
   return c;
}

// Escribe la barra normal
void ControlTexto::EscribirBarra( int texto, int tecla, int pos )
{
   char s[difh];
   bool LB = FALSE;

   for( int i = 0; i < 79; i++ )
      s[i] = '\0';

   if( pos < 0 ) pos = 0;
   if( pos > ( difh - 34 ) ) pos = ( difh - 34 );
   if( pos == 0 ) LB == TRUE;

   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   sprintf( s, "CH:%04x X:%04d Y:%04d Lineas:%04d", tecla, ( curx[texto] + poslinea[texto] ), ( cury[texto] + postexto[texto] ), textos[texto]->NumeroLineas() );
   ModificarBarra( ( string )s, pos, LB );
   if( modif[texto] == TRUE )
      ModificarBarra( "*", ( pos + 33 ), FALSE );
   if( ins == TRUE )
      ModificarBarra( "Insert", ( pos + 35 ), FALSE );
   ModificarBarra( textos[texto]->Titulo(), ( difh - textos[texto]->Titulo().length() + 1 ), FALSE );
   MuestraBarra();
}

// Pregunta si o no en la barra en pos y la borra segun borrar
int ControlTexto::SINO( int pos, bool borrar )
{
   if( pos < 0 ) pos = 0;
   if( pos > ( difh - 18 ) ) pos = ( difh - 18 );

   ModificarBarra( "Esta seguro? (S/N)", pos, borrar );
   MuestraBarra();

   do
   {
      int tecla = LeeTecla();

      if( ( tecla == 0x73 ) || ( tecla == 0x53 ) || ( tecla == 0x0D ) )
         return 1;
      if( ( tecla == 0x6E ) || ( tecla == 0x4E ) || ( tecla == 0x1B ) )
         return 0;
   } while( 1 );
}

// Pregunta un string en la barra
int ControlTexto::Pregunta( char *destino, string titulo )
{
   char c;
   int n = 0;

   ModificarBarra( titulo, 2 );
   MuestraBarra();

   do
   {
      char a[1];
      string s;

      pantallas[0]->MoverCursor( ( n + titulo.length() + 3 ), difv );
      int i = LeeTecla();
      if( i < 0x100 )
      {
         c = ( char )i;
         if( ( c != 0x08 ) && ( c != 0x0D ) )
         {
            *( destino + n ) = c;
            a[0] = c;
            s = ( string )a;
            s.erase( 1, 2 );
            ModificarBarra( s, ( n + titulo.length() + 3 ), FALSE );
            n++;
         }
         if( ( c == 0x08 ) && ( n > 0 ) )
         {
            n--;
            ModificarBarra( ( n + titulo.length() + 2 ), 1 );
            *( destino + n ) = '\0';
         }
         if( c == 0x1B )
            return 0;
         MuestraBarra();
      }
   } while( c != 0x0D );

   return 1;
}

// Elimina el texto de pos
void ControlTexto::EliminaTexto( int pos )
{
   if( pos < 0 ) return;
   if( pos >= num_textos ) return;

   delete textos[pos];
   delete pantallas[pos];
   num_textos--;

   for( int i = pos; i < num_textos; i++ )
   {
      textos[i] = textos[i + 1];
      pantallas[i] = pantallas[i + 1];
      curx[i] = curx[i + 1];
      cury[i] = cury[i + 1];
      modif[i] = modif[i + 1];
      poslinea[i] = poslinea[i + 1];
      postexto[i] = postexto[i + 1];
   }
}

// Mueve el cursor hacia arriba num espacios
void ControlTexto::Arriba( int texto, int num )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( num == 0 ) return;
   if( num > ( cury[texto] + postexto[texto] ) )
      num = ( cury[texto] + postexto[texto] );

   if( ( cury[texto] + postexto[texto] ) > num )
   {
      if( cury[texto] == 0 )
         postexto[texto] -= num;
      else
      {
         cury[texto] -= num;
         if( cury[texto] < 0 )
         {
            postexto[texto] += cury[texto];
            cury[texto] = 0;
         }
      }
      if( ( curx[texto] + poslinea[texto] ) > textos[texto]->linea( postexto[texto] + cury[texto] )->EntregaLargo() )
         Fin( texto );
   }
}

// Mueve el cursor hacia abajo num espacios
void ControlTexto::Abajo( int texto, int num )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( num == 0 ) return;
   if( ( num + cury[texto] + postexto[texto] ) > textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() )
      num = ( textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() - cury[texto] - postexto[texto] );

   if( ( cury[texto] + postexto[texto] + num ) < textos[texto]->NumeroLineas() )
   {
      if( ( cury[texto] + num ) >= difv )
         postexto[texto] += num;
      else
      {
         cury[texto] += num;
         if( cury[texto] > difv )
         {
            postexto[texto] += ( cury[texto] - difv );
            cury[texto] = ( difv - 1 );
         }
      }
      if( ( curx[texto] + poslinea[texto] ) > textos[texto]->linea( postexto[texto] + cury[texto] )->EntregaLargo() )
         Fin( texto );
   }
}

// Mueve el cursor a la Izquierda un espacio
void ControlTexto::Izquierda( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   curx[texto]--;
   if( ( ( curx[texto] + poslinea[texto] ) < 0 ) && ( ( cury[texto] + postexto[texto] ) > 0 ) )
   {
      curx[texto] = ( textos[texto]->linea( cury[texto] + postexto[texto] - 1 )->EntregaLargo() + 1 );
      Arriba( texto );
   }
   if( curx[texto] < 0 )
   {
      poslinea[texto]--;
      curx[texto] = 0;
   }
}

// Mueve el cursor a la Derecha un espacio
void ControlTexto::Derecha( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   curx[texto]++;
   if( ( ( curx[texto] + poslinea[texto] ) > textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() ) && ( ( cury[texto] + postexto[texto] + 1 ) < textos[texto]->NumeroLineas() ) )
   {
      curx[texto] = 0;
      poslinea[texto] = 0;
      Abajo( texto );
   }
   if( curx[texto] > difh )
   {
      poslinea[texto]++;
      curx[texto] = difh;
   }
}

// Mueve el cursor al inicio de la linea
void ControlTexto::Inicio( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   poslinea[texto] = 0;
   curx[texto] = 0;
}

// Mueve el cursor al final de la linea
void ControlTexto::Fin( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   poslinea[texto] = ( textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() - difh );
   curx[texto] = difh;
   if( poslinea[texto] < 0 )
   {
      poslinea[texto] = 0;
      curx[texto] = textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo();
   }
}

// Mueve el cursor al final de la ultima linea
void ControlTexto::CtrlFin( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   do
   {
      Abajo( texto, difv );
   } while( ( cury[texto] + postexto[texto] + 1 ) == textos[texto]->NumeroLineas() );
   Fin( texto );
}

// Mueve el cursor al principio de la primera linea
void ControlTexto::CtrlInic( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   cury[texto] = 0;
   postexto[texto] = 0;
   Inicio( texto );
}

// Borra num espacios
void ControlTexto::Borrar( int texto, int num )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( ( num + curx[texto] + poslinea[texto] ) >= textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() )
      num = ( textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() - curx[texto] - poslinea[texto] - 1 );
   if( num == 0 ) return;

   for( int i = ( curx[texto] + poslinea[texto] ); ( i + curx[texto] + poslinea[texto] ) < num; i++)
      textos[texto]->linea( postexto[texto] + cury[texto] )->EliminaCaracter( curx[texto] + poslinea[texto] );

   modif[texto] = TRUE;
}

// Borra un caracter al estilo BackSpace
void ControlTexto::BackSpace( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( ( ( curx[texto] + poslinea[texto] ) == 0 ) && ( ( postexto[texto] + cury[texto] ) > 0 ) )
   {
      Arriba( texto );
      Fin( texto );
      textos[texto]->UneLineas( postexto[texto] + cury[texto] );
      modif[texto] = TRUE;
   } else
   {
      if( ( curx[texto] + poslinea[texto] ) > 0 )
      {
         Izquierda( texto );
         Borrar( texto );
      }
   }
}

// Borra un caracter al estilo Suprimir
void ControlTexto::Supr( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( ( ( curx[texto] + poslinea[texto] ) == textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() ) && ( textos[texto]->NumeroLineas() > ( cury[texto] + postexto[texto] + 1 ) ) )
   {
      textos[texto]->UneLineas( postexto[texto] + cury[texto] );
      modif[texto];
   } else
   {
      if( ( curx[texto] + poslinea[texto] ) < textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() )
         Borrar( texto );
   }
}

// Agrega el caracter ch
void ControlTexto::AgregarCaracter( int texto, char ch )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   textos[texto]->linea( cury[texto] + postexto[texto] )->AgregaCaracter( ( curx[texto] + poslinea[texto] ), ch );
   if( ins == FALSE )
      textos[texto]->linea( cury[texto] + postexto[texto] )->EliminaCaracter( curx[texto] + poslinea[texto] );
   Derecha( texto );
   modif[texto] = TRUE;
}

// Mueve a la derecha el cursor num caracteres como TAB
void ControlTexto::TAB( int texto, int num )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( num == 0 ) return;

   if( ins == FALSE )
   {
      if( ( curx[texto] + poslinea[texto] + num ) > textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo() )
      {
         Fin( texto );
         for( int i = textos[texto]->linea( cury[texto] + postexto[texto] )->EntregaLargo(); ( i < ( poslinea[texto] + curx[texto] + num ) ); i++ )
            AgregarCaracter( texto, ' ' );
         modif[texto] = TRUE;
      }
   }
   if( ins == TRUE )
   {
      for( int i = 0; i < num; i++ )
         AgregarCaracter( texto, ' ' );
      modif[texto] = TRUE;
   }
}

// Cambia el estado de Insert
void ControlTexto::Insert()
{
   if( ins == TRUE ) ins = FALSE;
   else ins = TRUE;
}

// Enter
void ControlTexto::Enter( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( ins == TRUE  )
      textos[texto]->SeparaLineas( ( cury[texto] + postexto[texto] ), ( curx[texto] + poslinea[texto] ) );
   if( ( ins == FALSE ) && ( ( cury[texto] + postexto[texto] ) == textos[texto]->NumeroLineas() ) )
      textos[texto]->AgregaLinea( cury[texto] + postexto[texto] );
   Abajo( texto );
   Inicio( texto );
   modif[texto];
}

// Agrega el string s en la posici�n pos y limpia o no la barra seg�n LB
void ControlTexto::ModificarBarra( string s, int pos, bool LB )
{
   if( pos > difh ) pos = difh;
   if( pos < 0 ) pos = 0;

   if( LB == TRUE ) LimpiarBarra();
   if( pos > ( int )barra.length() )
   {
      while( ( int )barra.length() < pos )
         barra += " ";
   }

   barra.insert( pos, s );
   if( barra.length() > ( difh + 1 ) )
      barra.resize( difh + 1 );
}

// Borra num caracteres de la barra en pos
void ControlTexto::ModificarBarra( int pos, int num )
{
   if( pos > ( int )barra.length() ) pos = ( ( int )barra.length() - 1 );
   if( pos > difh ) pos = difh;
   if( pos < 0 ) pos = 0;

   if( ( num + pos ) > difh ) num = ( difh - pos );
   if( ( num + pos ) > ( int )barra.length() )
      num = ( ( int )barra.length() - pos );
   if( num < 1 ) num = 1;

   barra.erase( ( pos + 1 ), ( num + 1 ) );
}

// Limpia el contenido de la barra
void ControlTexto::LimpiarBarra()
{
   barra = "";
}

// Muestra solo la barra
void ControlTexto::MuestraBarra()
{
   short bar[difh];

   for( int i = 0; i < ( int )barra.length(); i++ )
      bar[i] = ( short )( barra[i] + COLOR_BARRA );

   puttext( 1, ( difv + 2 ), ( difh + 1 ), ( difv + 2 ), bar );
}

// Agrega un Texto al final y retorna la posicion de el o texto si no pudo
int ControlTexto::Nuevo( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( num_textos != MAX_TEXTOS )
   {
      textos[num_textos] = new Texto();
      pantallas[num_textos] = new Pantalla();
      curx[num_textos] = 0;
      cury[num_textos] = 0;
      modif[num_textos] = FALSE;
      poslinea[num_textos] = 0;
      postexto[num_textos] = 0;
      num_textos++;
      return ( num_textos - 1 );
   } else
   {
      ModificarBarra( "Hay demasiadas ventanas abiertas, debe cerrar alguna" );
      MuestraBarra();
      LeeTecla();
      return texto;
   }
}

// Busca o reemplaza segun reemplazar en el texto y espera despues con tecla
void ControlTexto::Buscar( int texto, int tecla, bool reemplazar )
{
   char s[difh];

   for( int i = 0; i < 79; i++ )
      s[i] = '\0';
   if( Pregunta( s, "Buscar:" ) )
   {
      string buscar = s;
      string reemplazo;

      if( reemplazar == TRUE )
      {
         for( int i = 0; i < 79; i++ )
            s[i] = '\0';
         if( !Pregunta( s, "Reemplazar por:" ) )
            return;
         reemplazo = s;
      }
      for( int i = 0; i < textos[texto]->NumeroLineas(); i++ )
      {
         if( ( int )buscar.length() < textos[texto]->linea( i )->EntregaLargo() )
         {
            for( int j = 0; ( j + ( int )buscar.length() ) < textos[texto]->linea( i )->EntregaLargo(); j++ )
            {
               if( buscar == textos[texto]->linea( i )->Datos().substr( j, ( buscar.length() + j - 1 ) ) )
               {
                  if( reemplazar == TRUE )
                  {
                     textos[texto]->linea( i )->EliminaCaracter( j, buscar.length() );
                     textos[texto]->linea( i )->AgregaString( j, reemplazo );
                  }
                  poslinea[texto] = j;
                  curx[texto] = 0;
                  postexto[texto] = i;
                  cury[texto] = 0;

                  MostrarTexto( texto );

                  ModificarBarra( "Presione F" );
                  sprintf( s, "%d", ( ( tecla / 0x100 ) - 0x3A ) );
                  ModificarBarra( s, 10, FALSE );
                  ModificarBarra( "para buscar siguiente.", 12, FALSE );
                  MuestraBarra();

                  pantallas[0]->MoverCursor( curx[texto], cury[texto] );

                  int t = LeeTecla();

                  if( t != tecla )
                     return;
               }
            }
         }
      }
      ModificarBarra( "Termino la busqueda. Presione una tecla." );
      MuestraBarra();
      LeeTecla();
   }
}

// Carga un archivo al texto
int ControlTexto::Cargar( int texto )
{
   char s[difh];

   for( int i = 0; i < 79; i++ )
      s[i] = '\0';
   if( Pregunta( s, "Nombre:" ) )
   {
      ifstream fil;

      fil.open( s );
      if( !fil )
      {
         ModificarBarra( "No se puede abrir ese archivo! Presione una tecla." );
         MuestraBarra();
         LeeTecla();
         return texto;
      }

      int ntexto = Nuevo( texto );

      //Lee el archivo
      if( ntexto != texto )
      {
         int x = 1, m = 0;

         ModificarBarra( "Leyendo el archivo...", 2 );
         MuestraBarra();
         do
         {
            int t = fil.get();

            if( t == 0x0A )
            {
               x++;
               m = 0;
               textos[ntexto]->AgregaLinea( x );
            } else
            {
               if( !fil.eof() )
                  textos[ntexto]->linea( x )->AgregaCaracter( m, ( char )t );
            }
            m++;
         } while( !fil.eof() );
         fil.close();
         poslinea[ntexto] = 0;
         postexto[ntexto] = 0;
         curx[ntexto] = 0;
         cury[ntexto] = 0;
         textos[ntexto]->CambiarTitulo( ( string )s );
         modif[ntexto] = FALSE;
      }
      return ntexto;
   }
   return texto;
}

// Guarda simple o como segun como el texto
void ControlTexto::Guardar( int texto, bool como )
{
   char s[difh];

   for( int i = 0; i < 79; i++ )
   {
      if( ( como == TRUE ) || ( ( como == FALSE ) && ( textos[texto]->Titulo() == "Sin Titulo" ) ) || ( ( como == FALSE ) && ( i > ( int )textos[texto]->Titulo().length() ) ) )
         s[i] = '\0';
      else
         s[i] = textos[texto]->Titulo()[i];
   }
   if( s[0] == '\0' )
   {
      if( !Pregunta( s, "Nombre:" ) )
         return;
   }
   if( como == TRUE )
   {
      ifstream fil;

      fil.open( s );
      if( fil )
      {
         ModificarBarra( "Ese archivo ya existe." );
         fil.close();
         if( !SINO( 23, FALSE ) )
            return;
      }
   }
   ofstream fil;

   fil.open( s );
   if( !fil )
   {
      ModificarBarra( "No se puede abrir ese archivo! Presione una tecla." );
      MuestraBarra();
      LeeTecla();
      return;
   }

   ModificarBarra( "Escribiendo el archivo...", 2 );
   MuestraBarra();

   //Escribe el archivo
   for( int x = 0; x < textos[texto]->NumeroLineas(); x++ )
   {
      for( int m = 0; m < textos[texto]->linea( x )->EntregaLargo(); m++ )
         fil.put( textos[texto]->linea( x )->EntregaCaracter( m ) );
      fil.put( 0x0A );
   }
   fil.close();
   textos[texto]->CambiarTitulo( ( string )s );
   modif[texto] = FALSE;
}

// Comprueba el estado de modif y pregunta si se quiere guardar o no
int ControlTexto::CompruebaMod( int texto )
{
   if( texto < 0 ) texto = 0;
   if( texto >= num_textos ) texto = ( num_textos - 1 );

   if( modif[texto] == TRUE )
   {
      char s[1];
      int tecla;

      ModificarBarra( "El texto" );
      sprintf( s, "%c", ( texto + 0x30 ) );
      ModificarBarra( ( string )s, 9, FALSE );
      ModificarBarra( "ha sido modificado. Desea guardarlo? (S/N/C)", 11, FALSE );
      MuestraBarra();
      do
      {
         tecla = LeeTecla();
         if( ( tecla == 0x73 ) || ( tecla == 0x53 ) || ( tecla == 0x0D ) )
         {
            Guardar( texto, TRUE );
            return 1;
         }
         if( ( tecla == 0x63 ) || ( tecla == 0x43 ) || ( tecla == 0x1B ) )
            return 0;

      } while( ( tecla != 0x6E ) && ( tecla != 0x4E ) );
   }
   return 1;
}
//---------------------------------------------------------------------------
