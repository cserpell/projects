//---------------------------------------------------------------------------
#ifndef ctrltextH
#define ctrltextH
//---------------------------------------------------------------------------
#define MAX_TEXTOS (10)
#define COLOR_BARRA (0x3000)
#include <string.stl>
#include "texto.h"
#include "Pantalla.h"

using namespace std;
//---------------------------------------------------------------------------
class ControlTexto
{
   int curx[MAX_TEXTOS];
   int cury[MAX_TEXTOS];
   int poslinea[MAX_TEXTOS];
   int postexto[MAX_TEXTOS];
   int num_textos;
   bool modif[MAX_TEXTOS];
   bool ins;
   Texto *textos[MAX_TEXTOS];
   Pantalla *pantallas[MAX_TEXTOS];
   string barra;
  public:
   int SINO( int pos, bool borrar );
   int Pregunta( char *destino, string titulo );
   int LeeTecla();
   void MostrarTexto( int texto );
   void EliminaTexto( int pos );
   void EscribirBarra( int texto, int tecla, int pos = 0 );
   void MuestraBarra();
   void ModificarBarra( int pos, int num = 1);
   void ModificarBarra( string s, int pos = 0, bool LB = TRUE );
   void LimpiarBarra();
   void Insert();
   void AgregarCaracter( int texto, char ch );
   void Arriba( int texto, int num = 1 );
   void Abajo( int texto, int num = 1 );
   void Izquierda( int texto );
   void Derecha( int texto );
   void Inicio( int texto );
   void Fin( int texto );
   void Borrar( int texto, int num = 1 );
   void BackSpace( int texto );
   void Supr( int texto );
   void TAB( int texto, int num );
   void Enter( int texto );
   void CtrlFin( int texto );
   void CtrlInic( int texto );
   int Nuevo( int texto = 0 );
   void Buscar( int texto, int tecla, bool reemplazar = FALSE );
   int Cargar( int texto );
   void Guardar( int texto, bool como = FALSE );
   int CompruebaMod( int texto );
   int NumeroTextos();
   ControlTexto()
   {
      num_textos = 0;
      ins = TRUE;
      Nuevo();
   }
};

#endif
//---------------------------------------------------------------------------
