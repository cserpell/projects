//---------------------------------------------------------------------------
#ifndef textoH
#define textoH
//---------------------------------------------------------------------------
#define MAX_LINEAS (10000)
#include <string.stl>
#include "linea.h"

using namespace std;
//---------------------------------------------------------------------------
class Texto
{
   int num_lineas;
   Linea *lin[MAX_LINEAS];
   string titulo;
   void AgregaLinea( int pos, string nueva );
   void EliminaLinea( int pos );
  public:
   void AgregaLinea( int pos );
   void UneLineas( int pos );
   void SeparaLineas( int linea, int pos );
   void Iniciar();
   void CambiarTitulo( string nuevo );
   int NumeroLineas();
   string Titulo();
   Linea * linea( int n );
   Texto()
   {
      titulo = "Sin Titulo";
      num_lineas = 0;
      AgregaLinea( 0 );
   }
};

#endif
//---------------------------------------------------------------------------
