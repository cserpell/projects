//---------------------------------------------------------------------------
#ifndef PantallaH
#define PantallaH
//---------------------------------------------------------------------------
#define MAX_VERTICAL (24)
#define MAX_HORIZONTAL (80)
#define MIN_VERTICAL (1)
#define MIN_HORIZONTAL (1)
#define difv (MAX_VERTICAL - MIN_VERTICAL)
#define difh (MAX_HORIZONTAL - MIN_HORIZONTAL)
#define COLOR_TEXTO (0x1700)
#include <string.stl>

using namespace std;
//---------------------------------------------------------------------------
class Pantalla
{
   short buffer[( difh + 1 ) * ( difv + 1 )];
   void LimpiarLinea( int linea );
   void LimpiaPantalla();
  public:
   void MoverCursor( int x, int y );
   void MostrarPantalla();
   void EscribirEnPantalla( int linea, string s = "", int pos = 0 );
   Pantalla()
   {
      LimpiaPantalla();
   }
};

#endif
//---------------------------------------------------------------------------
