//---------------------------------------------------------------------------
#ifndef piezasH
#define piezasH
#define postablero ( 7 * 80 + 30 )
//---------------------------------------------------------------------------
class pieza
{
   int rotacion;
   int posx;
   int posy;
   int tipo;
   char figura1;
   char figura2;
   char figura3;
  public:
   void DefinirFiguras( char fig1, char fig2 );
   void DefinirFiguras( char fig1, char fig2, char fig3 );
   void Mover( char hacia, int cantidad = 1 );
   void Rotar();
   int Calzar( char *tablero );
   int Figura( int figura );
   int px();
   int py();
   int px2();
   int py2();
   int px3();
   int py3();
   int Tablero();
   pieza()
   {
      posx = 1;
      posy = 1;
      rotacion = 0;
   }
};
#endif
