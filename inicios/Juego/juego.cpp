// Juego ... Por Cristi�n Serpell 2000
// En este juego hay que llenar el tablero con las fichas dadas
//---------------------------------------------------------------------------
#include <conio.h>
#include <condefs.h>
#pragma hdrstop

#include "todas.h"
//---------------------------------------------------------------------------
USEUNIT("piezas.cpp");
USEUNIT("todas.cpp");
//---------------------------------------------------------------------------
#pragma argsused
//---------------------------------------------------------------------------
// RUTINAS GR�FICAS
// Muestra el tablero base de color verde
void MostrarTablero( char *tablero )
{
   int destino[28];

   for( int i = 0; i < 28; i++ )
      destino[i] = *( tablero + i ) + 11 * 0x100 + 9 * 0x1000;

   puttext( 31, 8, 37, 15, destino );
}
// Muestra el figura de pieza en px, py con color (0 = blanco , 1 = rojo)
void MostrarFiguraPieza( int figura, int px, int py, int color )
{
   int clr = 15, clr2 = 11;
   int punto = py * 80 + px;

   if( color ) clr = 4;
   if( punto < postablero ) clr2 = 0;
   if( punto >= postablero + 80 * 3 + 7 ) clr2 = 0;
   for( int i = 0; i < 3; i++ )
   {
      if( punto >= postablero + 7 + 80 * i && punto < postablero + 80 * ( i + 1 ) )
         clr2 = 0;
   }

   textbackground( clr );
   textcolor( clr2 );
   gotoxy( px + 1, py + 1 );
   putch( figura + clr * 0x100 + clr2 * 0x1000 );
   textbackground( 0 );
   textcolor( 15 );
}
// Muestra la pieza dada con color (0 = blanco , 1 = rojo)
void MostrarPieza( pieza *Pieza, int color )
{
   MostrarFiguraPieza( Pieza->Figura( 1 ), Pieza->px(), Pieza->py(), color );
   MostrarFiguraPieza( Pieza->Figura( 2 ), Pieza->px2(), Pieza->py2(), color );
   if( Pieza->px3() != -1 )
      MostrarFiguraPieza( Pieza->Figura( 3 ), Pieza->px3(), Pieza->py3(), color );
}
//---------------------------------------------------------------------------
// PROGRAMA PRINCIPAL
int main(/*int argc, char* argv[]*/)
{
   nivel Juego;
   char tablero[28] = { "oo+++o-o---+-+-o+oo-o-o+-++o" };
   char niveles[13] = { "ABBCCCDDEFFKJ" };
   int piezaactual = -1, salir = 0, cx = 1, cy = 1, n = 0;

   // Crea las piezas
   for( int i = 0; i < 13; i++ )
      Juego.NuevaPieza( niveles[i] );

   do
   {
      int tecla;

      // Muestra el tablero y las piezas
      if( !n )
      {
         clrscr();
         MostrarTablero( tablero );
         for( int i = 0; i < 13; i++ )
         {
            if( i != piezaactual )
               MostrarPieza( Juego.Pieza( i ), 0 );
         }
         if( piezaactual != -1 )
            MostrarPieza( Juego.Pieza( piezaactual ), 1 );
      }

      // Mueve el cursor
      gotoxy( cx + 1, cy + 1 );
      n = 1;

      // Lee una tecla
      tecla = getch() & 0xFF;
      if( !tecla ) tecla = ( getch() & 0xFF ) * 256;

      switch( tecla )
      {
        case 0x4800: // Arriba
         cy--;
         if( cy < 1 ) cy = 1;
         if( piezaactual != -1 )
         {
            Juego.Pieza( piezaactual )->Mover( 'a' );
            n = 0;
         }
         break;

        case 0x5000: // Abajo
         cy++;
         if( cy > 23 ) cy = 23;
         if( piezaactual != -1 )
         {
            Juego.Pieza( piezaactual )->Mover( 'b' );
            n = 0;
         }
         break;

        case 0x4B00: // Izquierda
         cx--;
         if( cx < 1 ) cx = 1;
         if( piezaactual != -1 )
         {
            Juego.Pieza( piezaactual )->Mover( 'i' );
            n = 0;
         }
         break;

        case 0x4D00: // Derecha
         cx++;
         if( cx > 78 ) cx = 78;
         if( piezaactual != -1 )
         {
            Juego.Pieza( piezaactual )->Mover( 'd' );
            n = 0;
         }
         break;

        case 0x20:   // Barra -- Tomar o soltar pieza
         if( piezaactual == -1 )
         {
            piezaactual = Juego.ComprobarPunto( cx, cy );
            if( piezaactual == -1 )
               break;
         }
         else
         {
            if( !Juego.ComprobarPieza( piezaactual ) )
               break;
            if( Juego.Pieza( piezaactual )->Tablero() && !Juego.Pieza( piezaactual )->Calzar( tablero ) )
               break;
            piezaactual = -1;

            // Comprueba si se gan� el juego
            int r = 0;
            for( int i = 0; i < 13; i++ )
               r += Juego.Pieza( i )->Calzar( tablero );
            if( r == 13 )
               salir++;
         }
         n = 0;
         break;

        case 0x0D:   // ENTER -- Rotar pieza
         if( piezaactual != -1 )
         {
            Juego.Pieza( piezaactual )->Rotar();
            n = 0;
         }
         break;

        case 0x1B:   // ESC   -- Salir
         salir++;
         break;
      }
   } while( !salir );
   return 0;
}
