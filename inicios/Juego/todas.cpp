// Unit de la clase nivel
//---------------------------------------------------------------------------
#pragma hdrstop

#include "todas.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
// Agrega una nueva pieza del modo 'tipo'
void nivel::NuevaPieza( int tipo )
{
   if( tipo < 'A' || tipo > 'N' ) return;

   todas[num_piezas] = new pieza();

   switch( tipo )
   {
     case 'A':
      todas[num_piezas]->DefinirFiguras( 'o', '+' );
      break;
     case 'B':
      todas[num_piezas]->DefinirFiguras( '-', '+' );
      break;
     case 'C':
      todas[num_piezas]->DefinirFiguras( 'o', '-' );
      break;
     case 'D':
      todas[num_piezas]->DefinirFiguras( '+', '+' );
      break;
     case 'E':
      todas[num_piezas]->DefinirFiguras( '-', '-' );
      break;
     case 'F':
      todas[num_piezas]->DefinirFiguras( 'o', 'o' );
      break;
     case 'G':
      todas[num_piezas]->DefinirFiguras( '-', '-', 'o' );
      break;
     case 'H':
      todas[num_piezas]->DefinirFiguras( '+', '-', 'o' );
      break;
     case 'I':
      todas[num_piezas]->DefinirFiguras( 'o', '+', '-' );
      break;
     case 'J':
      todas[num_piezas]->DefinirFiguras( '+', '-', '+' );
      break;
     case 'K':
      todas[num_piezas]->DefinirFiguras( 'o', '-', 'o' );
      break;
     case 'L':
      todas[num_piezas]->DefinirFiguras( 'o', 'o', '-' );
      break;
     case 'M':
      todas[num_piezas]->DefinirFiguras( '+', 'o', '-' );
      break;
     case 'N':
      todas[num_piezas]->DefinirFiguras( '-', 'o', '+' );
      break;
   }
   for( int i = 0; i < num_piezas; i++ )
      todas[num_piezas]->Mover( 'd', 3 );

   num_piezas++;
}
// Comprueba si donde est� la pieza hay otra (si no hay retorna 0)
int nivel::ComprobarPieza( int pieza )
{
   if( pieza < 0 ) pieza = 0;
   if( pieza >= num_piezas ) pieza = num_piezas - 1;

   if( ComprobarPunto( todas[pieza]->px(), todas[pieza]->py(), pieza ) == -1 )
   {
      if( ComprobarPunto( todas[pieza]->px2(), todas[pieza]->py2(), pieza ) == -1 )
      {
         if( todas[pieza]->px3() != -1 )
         {
            if( ComprobarPunto( todas[pieza]->px3(), todas[pieza]->py3(), pieza ) == -1 )
               return 1;
            return 0;
         }
         return 1;
      }
   }
   return 0;
}
// Comprueba si hay una pieza en el punto px,py, menos si es 'pieza'
int nivel::ComprobarPunto( int px, int py, int pieza )
{
   if( pieza < -1 ) pieza = -1;
   if( pieza >= num_piezas ) pieza = num_piezas - 1;

   for( int i = 0; i < num_piezas; i++ )
   {
      if( i != pieza )
      {
         int r = -1;

         if( px == todas[i]->px() && py == todas[i]->py() )
            r = i;
         if( px == todas[i]->px2() && py == todas[i]->py2() )
            r = i;
         if( px == todas[i]->px3() && py == todas[i]->py3() )
            r = i;
         if( r != -1 )
            return r;
      }
   }
   return -1;
}
// Retorna un puntero a la pieza 'num'
pieza *nivel::Pieza( int num )
{
   if( num < 0 ) num = 0;
   if( num >= num_piezas ) num = num_piezas - 1;

   return todas[num];
}
