//---------------------------------------------------------------------------
#ifndef todasH
#define todasH
#include "piezas.h"
//---------------------------------------------------------------------------
class nivel
{
   int num_piezas;
   pieza *todas[13];
  public:
   void NuevaPieza( int tipo );
   int ComprobarPieza( int pieza );
   int ComprobarPunto( int px, int py, int pieza = -1 );
   pieza *Pieza( int num );
   nivel()
   {
      num_piezas = 0;
   }
};
#endif
