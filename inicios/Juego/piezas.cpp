// Unit de la clase pieza
//---------------------------------------------------------------------------
#pragma hdrstop

#include "piezas.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
// Define figura1 y figura2. fig1 y fig2 deben ser '+', 'o' o '-'.
void pieza::DefinirFiguras( char fig1, char fig2 )
{
   if( fig1 != '+' && fig1 != 'o' && fig1 != '-' ) return;
   if( fig2 != '+' && fig2 != 'o' && fig2 != '-' ) return;

   figura1 = fig1;
   figura2 = fig2;

   tipo = 0;
}
// Define las figuras para la pieza de tres figuras
void pieza::DefinirFiguras( char fig1, char fig2, char fig3 )
{
   DefinirFiguras( fig1, fig2 );

   if( fig3 != '+' && fig3 != 'o' && fig3 != '-' ) return;
   figura3 = fig3;

   tipo = 1;
}
// Mueve la figura (cambia posx o posy) seg�n hacia.
void pieza::Mover( char hacia, int cantidad )
{
   for( int i = 0; i < cantidad; i++ )
   {
      if( hacia == 'a' ) // Arriba
         posy--;
      if( hacia == 'i' ) // Izquierda
         posx--;
      if( hacia == 'b' ) // Abajo
         posy++;
      if( hacia == 'd' ) // Derecha
         posx++;
   }
   if( posy < 1 ) posy = 1;
   if( posx < 1 ) posx = 1;
   if( posy > 23 ) posy = 23;
   if( posx > 78 ) posx = 78;
}
// Rota la figura(cambia rotacion)
void pieza::Rotar()
{
   rotacion++;
   if( rotacion > 3 ) rotacion = 0;
}
// Comprueba si la pieza calza con el tablero
int pieza::Calzar( char *tablero )
{
   if( !Tablero() ) return 0;

   int px = posx - 30;
   int py = posy - 7;
   int psx2 = px2() - 30;
   int psy2 = py2() - 7;
   int psx3 = px3() - 30;
   int psy3 = py3() - 7;

   int r1 = 0, r2 = 0, r3 = 0;

   if( figura1 == *( tablero + py * 7 + px ) )
      r1 = 1;
   if( figura2 == *( tablero + psy2 * 7 + psx2 ) )
      r2 = 1;

   if( r1 && r2 && !tipo )
      return 1;

   if( tipo )
   {
      if( figura3 == *( tablero + psy3 * 7 + psx3 ) )
         r3 = 1;

      if( r1 && r2 && r3 )
         return 1;
   }
   return 0;
}
// Retorna la figura n�mero figura
int pieza::Figura( int figura )
{
   if( figura < 1 ) figura = 1;
   if( figura > 3 ) figura = 3;
   if( !tipo && figura == 3 ) return 0;

   if( figura == 1 ) return figura1;
   if( figura == 2 ) return figura2;
   return figura3;
}
// Retorna posx
int pieza::px()
{
   return posx;
}
// Retorna posy
int pieza::py()
{
   return posy;
}
// Retorna posx2
int pieza::px2()
{
   if( rotacion == 0 ) return posx + 1;
   if( rotacion == 2 ) return posx - 1;
   return posx;
}
// Retorna posy2
int pieza::py2()
{
   if( rotacion == 1 ) return posy - 1;
   if( rotacion == 3 ) return posy + 1;
   return posy;
}
// Retorna posx3, si tipo es 1
int pieza::px3()
{
   if( !tipo ) return -1;

   if( rotacion == 1 ) return posx - 1;
   if( rotacion == 3 ) return posx + 1;
   return posx;
}
// Retorna posy3, si tipo es 1
int pieza::py3()
{
   if( !tipo ) return -1;

   if( rotacion == 0 ) return posy - 1;
   if( rotacion == 2 ) return posy + 1;
   return posy;
}
// Comprueba si la pieza est� dentro del tablero
int pieza::Tablero()
{
   int r1 = 1, r2 = 1, r3 = 1;

   if( posx + posy * 80 < postablero ) r1 = 0;
   if( posx + posy * 80 >= postablero + 80 * 3 + 7 ) r1 = 0;
   for( int i = 0; i < 3; i++ )
   {
      if( posx + posy * 80 >= postablero + 7 + 80 * i && posx + posy * 80 < postablero + 80 * ( i + 1 ) )
         r1 = 0;
   }

   if( px2() + py2() * 80 < postablero ) r2 = 0;
   if( px2() + py2() * 80 >= postablero + 80 * 3 + 7 ) r2 = 0;
   for( int i = 0; i < 3; i++ )
   {
      if( px2() + py2() * 80 >= postablero + 7 + 80 * i && px2() + py2() * 80 < postablero + 80 * ( i + 1 ) )
         r2 = 0;
   }

   if( !tipo )
   {
      if( r1 && r2 )
         return 1;
      else
         return 0;
   }

   if( px3() + py3() * 80 < postablero ) r3 = 0;
   if( px3() + py3() * 80 >= postablero + 80 * 3 + 7 ) r3 = 0;
   for( int i = 0; i < 3; i++ )
   {
      if( px3() + py3() * 80 >= postablero + 7 + 80 * i && px3() + py3() * 80 < postablero + 80 * ( i + 1 ) )
         r3 = 0;
   }

   if( r1 && r2 && r3 )
      return 1;
   return 0;
}
