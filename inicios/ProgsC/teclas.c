#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

/* Muestra los c�digos de las teclas!! */

void main()
{
   int c;

   do
   {
     c=getch()&0xFF;
     if(!c)
        c=(getch()&0xFF)*256;

     printf("Tecla presionada: %04x\n",c);

   } while(1);

}
