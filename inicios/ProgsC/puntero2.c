# include <stdio.h>
# include <stdlib.h>

void llenar_datos( int * datos, int n)
{
        int i;
        for(i=0; i<n; i++)
              datos[i]=random();
}

void mostrar_datos( int *datos, int n)
{
        int i;

        printf("Datos:");
        for(i=0; i<n; i++)
              printf(" %d ,",datos[i]);
        printf("\n");
}

int buscar_mayor( int *datos, int n)
{
        int i, max=-1;

        for(i=0; i<n; i++)
              if (datos[i]>max )
                    max=datos[i];

        return max;
}

main()
{
        int num,*mat;

        printf("Cuantos datos quiere :");
        scanf("%d",&num);

        mat=malloc(num*sizeof(int));

        if(!mat)
        {
                printf("Error, no hay memoria suficiente\n");
                return 0;
        }

        llenar_datos(mat,num);
        mostrar_datos(mat,num);
        printf("El mayor es :%d\n",buscar_mayor(mat,num));

        free(mat);

        return 0;
}

