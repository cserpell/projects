#include <stdio.h>

int compara_archivos(FILE *f1, FILE *f2, int cc, int *ln, int ea)
{
    int bt=0;
    char ca1,ca2;
    FILE *result;

    if(ea==1)
        result=fopen("diferenc.dat","wt");
    if( !result)
    {
        perror("Error al crear el archivo de resultados");
        return 0;
    }

    while( !feof(f1)&&!feof(f2) )
    {
        ca1=getc(f1);
        ca2=getc(f2);
        if(ca1!=ca2)
        {
          (*ln)++;
          if(cc==1)
            printf("Hay diferencia en el byte %d:   '%c' <> '%c'\n",bt,ca1,ca2);
          else if(ea==1)
          {
            fputc(ca1,result);
            fputc(ca2,result);
            fputc(',',result);
          }
        }
        bt++;
    }

    if(ea==1)
    {
        fprintf(result, "\nBytes diferentes: %d\n",*ln);
        fprintf(result, "Largo: %d\n",bt-1);

        fclose(result);

        printf("Los resultados quedaron en el archivo 'diferenc.dat'\n");
    }

    return bt-1;
}

int pregunta()
{
    char ch;
    int r;

    do
    {
        ch=getchar();
    } while(ch!='S'&&ch!='s'&&ch!='N'&&ch!='n');

    if(ch=='S'||ch=='s')
        r=1;
    else
        r=0;

    return r;
}

int main()
{
    int i,len=0,e=1,c;
    char nombre1[80],nombre2[80];
    FILE *archivo1,*archivo2;

    while(e==1)
    {
        printf("\nIngrese el nombre del primer archivo:");
        gets(nombre1);
        printf("Ingrese el nombre del segundo archivo:");
        gets(nombre2);

        printf("�Mostrar las diferencias entre los archivos o s�lo el resultado?(S/N)");
        c=pregunta();

        if(c==0)
        {
                printf("�Desea escribir las diferencias a un archivo?(S/N)");
                e=pregunta();
        }
        else
                e=0;

        archivo1=fopen(nombre1,"rb");
        if( !archivo1)
        {
                perror("Error al abrir el primer archivo");
                return 0;
        }

        archivo2=fopen(nombre2,"rb");
        if( !archivo2)
        {
                perror("Error al abrir el segundo archivo");
                return 0;
        }

        i=compara_archivos(archivo1,archivo2,c,&len,e);

        if(len==0)
        {
                printf("Los archivos %s y %s son id�nticos.\n",nombre1,nombre2);
                printf("El largo de ellos era %d bytes.\n",i);
        }
        else
        {
                printf("Los archivos %s y %s son diferentes.\n",nombre1,nombre2);
                printf("Tienen %d bytes diferentes\n",len);
        }

        printf("\nLa comparaci�n ha terminado en el byte %d.\n",i);

        fclose(archivo1);
        fclose(archivo2);

        printf("�Desea comparar otro archivo?(S/N)");
        e=pregunta();
    }

    return 0;
}

