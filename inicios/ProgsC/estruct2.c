#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct spal
{
        char *nombre;
        int tipo;
} spal ;

spal palabras[500];
int  numpalabras=0;

char *tipos[5] = { "Articulo", "Sustantivo", "Adjetivo", "Verbo", "Otro" };

int busca_palabra(char *str)
{
        int i;

        for(i=0; i<numpalabras; i++)
           if(!strcmp(palabras[i].nombre,str) )
             break;
        return(i);
}

void agrega_palabra(char *str,int tp)
{
        char *pnt;

        pnt=malloc((strlen(str)+1) * sizeof(char));
        if(!pnt)
        {
                puts("Error, no hay memoria.");
                exit(0);
        }

        strcpy(pnt,str);
        palabras[numpalabras].nombre = pnt;
        palabras[numpalabras].tipo = tp;

        numpalabras++;
}

int main()
{
        char s1[80],s2[80];
        int i;

        do
        {
                printf("Ingresa una palabra:");
                gets(s1);

                if(!strcmp(s1,"fin"))
                   break;

                i=busca_palabra(s1);

                if(i==numpalabras)
                {
                        printf("No conozco esa palabra.");
                        printf(" Qu� es?:\n");
                        for(i=0;i<5;i++)
                           printf("  %d) %s\n",i,tipos[i]);
                        printf("    Ingresa el n�mero:");
                        gets(s2);
                        i=atoi(s2);
                        agrega_palabra(s1,i);
                } else
                        printf("La palabra '%s' es %s\n",s1,tipos[palabras[i].tipo]);

        } while (strcmp(s1,"fin"));

        return(0);

}

