#include <stdio.h>
#include <math.h>

#define DEG_RAD (3.1415926535897932384623/180)
#define MAX 12
#define INC (360 / MAX)


main()
{
        double angulo,rad;
        printf("Tabla de funciones trigonom�tricas :\n");
        printf(" �ngulo   Seno     Coseno        Tangente\n");
        for(angulo=0; angulo<360; angulo+=INC)
        {
                rad=angulo*DEG_RAD;
                printf("  %3.0f    %+.4f   %+.4f   %+.4f\n", angulo, sin(rad), cos(rad), tan(rad));
        }
        return 0;
}

