/*   Copiador de un archivo en varios discos
     Cristi�n Serpell - Enero 1999             */

#include <stdio.h>
#include <string.h>

/* Espera a que sea presionada la tecla 't' */

int tecla(char t)
{
        char c;

        while(c!=t)
                c=getchar();

        return 0;
}

/* Devuelve 1 si la respuesta es s� y 0 si la respuesta es no */

int pregunta()
{
        int r;
        char c='c';

        printf("(S/N)");

        while(c!='S'&&c!='s'&&c!='N'&&c!='n')
                c=getchar();

        if(c=='S'||c=='s')
                r=1;
        else
                r=0;

        return r;
}

/* *dat = string donde quedar�n los datos
   len  = cantidad de bytes a leer
   *origen = string con el nombre del archivo a leer
   *pnt = string donde se guarda la extensi�n del archivo

   Devuelve la cantidad de bytes le�dos
   o 0 si no pudo abrir el archivo                        */

int lee_datos_disket(char *dat, int len, char *origen, char *pnt)
{
        char c;
        int ln,i=0;
        FILE *orig;

        printf("Leyendo el archivo %s...\n",origen);
        orig=fopen(origen,"rb");
        if( !orig)
        {
                perror("Error al abrir el archivo de origen");
                return 0;
        }

        if(len==0);
        {
                c=fgetc(orig);
                switch(c)
                {
                    case '1':
                       ln=720;
                       break;
                    case '2':
                       ln=800;
                       break;
                    case '3':
                       ln=1200;
                       break;
                    case '4':
                       ln=1360;
                       break;
                    case '5':
                       ln=1440;
                       break;
                    case '6':
                       ln=1600;
                       break;
                    case '7':
                       ln=1680;
                       break;
                }
                ln*=1024;
                ln-=4;

                for(i=0;i<3;i++)
                   pnt[i]=fgetc(orig);
        }
        else
                ln=len;

        while( !feof(orig))
        {
                dat[i]=fgetc(orig);
                i++;
        }

        if(i<ln)
                ln=i;
        if(len==0)
                ln+=4;

        fclose(orig);
        return ln;
}

/* *dat = string donde quedar�n los datos
   len = cantidad de bytes a leer
   *origen = nombre del archivo que se lee
   *orig = archivo que se lee

   Devuelve la cantidad de bytes le�dos
   o 0 si el archivo no fue le�do completamente */

int lee_datos_dd(char *dat, int len, char *origen, FILE *orig)
{
        int i=0;

        printf("Leyendo el archivo %s...\n",origen);
        while( !feof(orig))
        {
                if(i>len)
                         return 0;
                dat[i]=fgetc(orig);
                i++;
        }

        return i;
}

/* *dat = lugar donde est�n los datos
   len = cantidad de bytes a escribir
   *destino = nombre del archivo a escribir
   l = caracter que indica la cantidad de bytes a leer cuando se lea el archivo
   *pnt = extension del archivo original

   Devuelve 0 si escribi� correctamente
   y 1 cuando no pudo escribir el archivo                                       */

int escribe_datos_disket(char *dat, int len, int nar, char *destino, char l, char *pnt)
{
        int i,r=1;
        char c='0';
        FILE *dest;

        printf("Inserte el disket de destino vac�o n�%d\n",nar);
        printf(" y presione ENTER.");
        c=tecla('\n');

        while(r==1)
        {
                r=0;
                dest=fopen(destino,"wb");
                if( !dest)
                {
                        perror("Error al abrir el archivo de destino");
                        printf("�Volver a intentar?");
                        r=pregunta();
                        if(r==0)
                                return 1;
                }
        }

        printf("Escribiendo el archivo %s...\n",destino);

        if(nar==0)
        {
                fputc(l, dest);
                len-=4;

                for(i=0;i<3;i++)
                   fputc(pnt[i], dest);
        }

        for(i=0; i<len; i++)
                fputc(dat[i], dest);

        fclose(dest);
        printf("Fueron escritos %d bytes.\n",len);

        return 0;
}

/* *dat = lugar donde est�n los datos
   len = cantidad de datos a escribir
   *destino = nombre del archivo a escribir
   *dest = archivo a escribir               */

int escribe_datos_dd(char *dat, int len, char *destino, FILE *dest)
{
        int i;

        printf("Escribiendo el archivo %s...\n",destino);
        for(i=0;i<len;i++)
                fputc(dat[i], dest);

        printf("Fueron escritos %d bytes.\n",i);
        return 0;
}

/* Programa principal */

int main()
{
        int i=1, ln=0,r=0,n=0;
        char nombre[80],c,dat[1720320],sl,fn[80],ext[3];
        FILE *orig;

        printf("---Copiador de un archivo a varios discos---\n\n");

        while(i==1)
        {
        printf("Desea:\n\n");
        printf("1) Archivo de disco duro a varios disketes");
        printf("2) Archivo de varios discos a disco duro");
        sl=getchar();

        if(sl==1)
        {
          printf("Ingrese el nombre del archivo:");
          gets(nombre);

          printf("Largo de cada archivo:\n\n");
          printf("1) 720 k.\n");
          printf("2) 800 k.\n");
          printf("3) 1.2 m.\n");
          printf("4) 1.36 m.\n");
          printf("5) 1.44 m.\n");
          printf("6) 1.6 m.\n");
          printf("7) 1.68 m.\n\n");

          while(ln==0)
          {
                  printf("Selecci�n:");
                  c=getchar();
                  switch(c)
                  {
                    case '1':
                       ln=720;
                       break;
                    case '2':
                       ln=800;
                       break;
                    case '3':
                       ln=1200;
                       break;
                    case '4':
                       ln=1360;
                       break;
                    case '5':
                       ln=1440;
                       break;
                    case '6':
                       ln=1600;
                       break;
                    case '7':
                       ln=1680;
                       break;
                  }
                  ln*=1024;
          }

          orig=fopen(nombre,"rb");
          if( !orig)
          {
                  perror("Error al abrir el archivo de origen");
                  return 0;
          }

          i=0;

          while(n==0)
          {
                  char *u;
                  r=lee_datos_dd(dat,ln,nombre,orig);
                  if(r!=0)
                          {
                                 ln=r;
                                 n=1;
                          }

                  u=strchr(nombre,'.');
                  if(u!=0)
                          *u=0;

                  sprintf(fn, "A:%s.cs%d",nombre,i);

                  r=escribe_datos_disket(dat,ln,i,fn,c,ext);
                  if(r==1)
                          return 0;

                  i++;
          }

          fclose(orig);
          printf("Archivo copiado sin problemas.\n");
        }
        else
        {
          printf("Ingrese el nombre del archivo:");
          gets(nombre);

          while(r==0)
          {
           printf("Ingrese el disket n�%d y presione ENTER.",i);
           c=tecla('\n');

           sprintf(fn, "A:%s.cs%d",nombre,i);
           r=lee_datos_disket(dat,ln,fn,ext);


          }
        }
        }
        return 0;
}


