# projects

A lot of programs I developed during years of learning. Contact me if you need help!

It seems oldest code here dates from 1999.

When I uploaded the repository to gitlab and cloned it locally again, it seems to have
erased the last modified time for all files. In order to not miss that information,
I added a list of each folder in a file called `dates.txt`.

## Known Issues

I am currently unable to compile old Borland projects, because I don't have a Borland
compiler with `vcl.h` header. I included compiled binary of those I had.

I haven't tried compiling C\# code, and I couldn't run it in Ubuntu yet.

3D particle simulators were part of an internship with Patricio Cordero at
Physics Departmanet, Universidad de Chile. Part of it needs a library
I don't have: `libplot.so.2`.