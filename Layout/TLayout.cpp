//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "TLayout.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TLayout *)
{
  new TLayout(NULL);
}
//---------------------------------------------------------------------------
__fastcall TLayout::TLayout(TComponent* Owner) : TComponent(Owner)
{
  DockSite = true;
}
//---------------------------------------------------------------------------
namespace Tlayout
{
  void __fastcall PACKAGE Register()
  {
    TComponentClass classes[1] = {__classid(TLayout)};
    RegisterComponents("Samples", classes, 0);
  }
}
//---------------------------------------------------------------------------
