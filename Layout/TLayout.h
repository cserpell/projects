//---------------------------------------------------------------------------
#ifndef TLayoutH
#define TLayoutH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class PACKAGE TLayout : public TCustomControl
{
 private:
 protected:
 public:
  __fastcall TLayout(TComponent* Owner);
 __published:
};
//---------------------------------------------------------------------------
#endif
 