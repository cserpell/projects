//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "PaintBox1.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
static inline void ValidCtrCheck(TTablero *)
{
  new TTablero(NULL);
}
//---------------------------------------------------------------------------
__fastcall TTablero::TTablero(TComponent* Owner)
         : TPaintBox(Owner)
{
  for(int i = 0; i < MAXTAM; i++)
    for(int j = 0; j < MAXTAM; j++)
      ocup[i][j] = -1;
  tamactx = MAXTAM;
  tamacty = MAXTAM;
  numpiezas = 0;
}
//---------------------------------------------------------------------------
void TTablero::MoverAbajo(int pieza)
{
  if(pieza < 0 || pieza >= numpiezas)
    return;
  for(int i = 0; i < P[pieza].numpedazos; i++)
  {
    if(P[pieza].y[i] < tamacty - 1)
    {
      if(ocup[P[pieza].x[i]][P[pieza].y[i] + 1] != pieza &&
         ocup[P[pieza].x[i]][P[pieza].y[i] + 1] != -1)
        return;
    } else
    {
      if(ocup[P[pieza].x[i]][0] != pieza && ocup[P[pieza].x[i]][0] != -1)
        return;
    }
  }
  for(int i = 0; i < P[pieza].numpedazos; i++)
  {
    ocup[P[pieza].x[i]][P[pieza].y[i]] = -1;
    if(P[pieza].y[i] < tamacty - 1)
      P[pieza].y[i]++;
    else
      P[pieza].y[i] = 0;
  }
  for(int i = 0; i < P[pieza].numpedazos; i++)
    ocup[P[pieza].x[i]][P[pieza].y[i]] = pieza;
}
//---------------------------------------------------------------------------
void TTablero::MoverDerecha(int pieza)
{
  if(pieza < 0 || pieza >= numpiezas)
    return;
  for(int i = 0; i < P[pieza].numpedazos; i++)
  {
    if(P[pieza].x[i] < tamactx - 1)
    {
      if(ocup[P[pieza].x[i] + 1][P[pieza].y[i]] != pieza &&
         ocup[P[pieza].x[i] + 1][P[pieza].y[i]] != -1)
        return;
    } else
    {
      if(ocup[0][P[pieza].y[i]] != pieza && ocup[0][P[pieza].y[i]] != -1)
        return;
    }
  }
  for(int i = 0; i < P[pieza].numpedazos; i++)
  {
    ocup[P[pieza].x[i]][P[pieza].y[i]] = -1;
    if(P[pieza].x[i] < tamactx - 1)
      P[pieza].x[i]++;
    else
      P[pieza].x[i] = 0;
  }
  for(int i = 0; i < P[pieza].numpedazos; i++)
    ocup[P[pieza].x[i]][P[pieza].y[i]] = pieza;
}
//---------------------------------------------------------------------------
void TTablero::MoverArriba(int pieza)
{
  if(pieza < 0 || pieza >= numpiezas)
    return;
  for(int i = 0; i < P[pieza].numpedazos; i++)
  {
    if(P[pieza].y[i] > 0)
    {
      if(ocup[P[pieza].x[i]][P[pieza].y[i] - 1] != pieza &&
         ocup[P[pieza].x[i]][P[pieza].y[i] - 1] != -1)
        return;
    } else
    {
      if(ocup[P[pieza].x[i]][tamacty - 1] != pieza &&
         ocup[P[pieza].x[i]][tamacty - 1] != -1)
        return;
    }
  }
  for(int i = 0; i < P[pieza].numpedazos; i++)
  {
    ocup[P[pieza].x[i]][P[pieza].y[i]] = -1;
    if(P[pieza].y[i] > 0)
      P[pieza].y[i]--;
    else
      P[pieza].y[i] = tamacty - 1;
  }
  for(int i = 0; i < P[pieza].numpedazos; i++)
    ocup[P[pieza].x[i]][P[pieza].y[i]] = pieza;
}
//---------------------------------------------------------------------------
void TTablero::MoverIzquierda(int pieza)
{
  if(pieza < 0 || pieza >= numpiezas)
    return;
  for(int i = 0; i < P[pieza].numpedazos; i++)
  {
    if(P[pieza].x[i] > 0)
    {
      if(ocup[P[pieza].x[i] - 1][P[pieza].y[i]] != pieza &&
         ocup[P[pieza].x[i] - 1][P[pieza].y[i]] != -1)
        return;
    } else
    {
      if(ocup[tamactx - 1][P[pieza].y[i]] != pieza &&
         ocup[tamactx - 1][P[pieza].y[i]] != -1)
        return;
    }
  }
  for(int i = 0; i < P[pieza].numpedazos; i++)
  {
    ocup[P[pieza].x[i]][P[pieza].y[i]] = -1;
    if(P[pieza].x[i] > 0)
      P[pieza].x[i]--;
    else
      P[pieza].x[i] = tamactx - 1;
  }
  for(int i = 0; i < P[pieza].numpedazos; i++)
    ocup[P[pieza].x[i]][P[pieza].y[i]] = pieza;
}
//---------------------------------------------------------------------------
void TTablero::MoverAbajo(int posx, int posy)
{
  if(posx < 0 || posx >= tamactx)
    return;
  if(posy < 0 || posy >= tamacty)
    return;
  MoverAbajo(ocup[posx][posy]);
}
//---------------------------------------------------------------------------
void TTablero::MoverDerecha(int posx, int posy)
{
  if(posx < 0 || posx >= tamactx)
    return;
  if(posy < 0 || posy >= tamacty)
    return;
  MoverDerecha(ocup[posx][posy]);
}
//---------------------------------------------------------------------------
void TTablero::MoverArriba(int posx, int posy)
{
  if(posx < 0 || posx >= tamactx)
    return;
  if(posy < 0 || posy >= tamacty)
    return;
  MoverArriba(ocup[posx][posy]);
}
//---------------------------------------------------------------------------
void TTablero::MoverIzquierda(int posx, int posy)
{
  if(posx < 0 || posx >= tamactx)
    return;
  if(posy < 0 || posy >= tamacty)
    return;
  MoverIzquierda(ocup[posx][posy]);
}
//---------------------------------------------------------------------------
bool TTablero::AgregarPedazodePieza(int numpieza, int posx, int posy)
{
  if(posx < 0 || posx >= tamactx)
    return false;
  if(posy < 0 || posy >= tamacty)
    return false;
  if(ocup[posx][posy] != -1)
    return false;
  if(numpieza < 0 || numpieza > numpiezas)
    return false;
  ocup[posx][posy] = numpieza;
  P[numpieza].AgregarPedazo(posx, posy);
  return true;
}
//---------------------------------------------------------------------------
// Retorna el n�mero de la pieza agregada
int TTablero::AgregarPieza()
{
  P[numpiezas].numpedazos = 0;
  numpiezas++;
  return numpiezas - 1;
}
//---------------------------------------------------------------------------
void Pieza::AgregarPedazo(int xr, int yr)
{
  x[numpedazos] = xr;
  y[numpedazos] = yr;
  numpedazos++;
}
//---------------------------------------------------------------------------
bool TTablero::AgregarPedazodeBloque(int posx, int posy)
{
  if(posx < 0 || posx >= tamactx)
    return false;
  if(posy < 0 || posy >= tamacty)
    return false;
  if(ocup[posx][posy] != -1)
    return false;
  ocup[posx][posy] = -2;
  return true;
}
//---------------------------------------------------------------------------
void TTablero::DefinirTam(int nuevo)
{
  if(nuevo < 0)
  {
    tamactx = 0;
    tamacty = 0;
    return;
  }
  if(nuevo > MAXTAM)
  {
    tamactx = MAXTAM;
    tamacty = MAXTAM;
    return;
  }
  tamactx = nuevo;
  tamacty = nuevo;
}
//---------------------------------------------------------------------------
void TTablero::DefinirTam(int nx, int ny)
{
  if(nx < 0)
    tamactx = 0;
  if(ny < 0)
    tamacty = 0;
  if(nx > MAXTAM)
    tamactx = MAXTAM;
  if(nx > MAXTAM)
    tamacty = MAXTAM;
  if(nx >= 0 && nx <= MAXTAM)
    tamactx = nx;
  if(ny >= 0 && ny <= MAXTAM)
    tamacty = ny;
}
//---------------------------------------------------------------------------
namespace Tablero
{
  void __fastcall PACKAGE Register()
  {
    TComponentClass classes[1] = {__classid(TTablero)};
    RegisterComponents("Samples", classes, 0);
  }
}
//---------------------------------------------------------------------------
