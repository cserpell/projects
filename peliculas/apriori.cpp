// CRISTIAN SERPELL 2006

/* Se supone el siguiente formato para la tabla de entrada:

    maximo_de_usuarios
    usuario_1 pelicula_1 ranking_1
    usuario_2 pelicula_2 ranking_2
                 ...

   Donde todos son numeros enteros.

   Un mayor ranking se interpreta repitiendo una mayor
   cantidad de veces la misma tupla.

   Solo se consideran rankings > u_min .

   Relacion con soporte menor a umbral no se entrega.
   
   Salidas ordenadas por soporte.
   
   Las salidas estan repetidas hacia los dos lados
   para facilitar la posterior busqueda.  */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
    int u_min = 0;
    int umbral = 1;
    int maximo_usuarios = 0;
    int maximo_peliculas = 0;
    int cantidad_peliculas = 0;
    int cantidad_usuarios = 0;
    int i, j, k;
    if(argc > 1)
        u_min = atoi(argv[1]);
    if(argc > 2) {
        umbral = atoi(argv[2]);
        if(umbral <= 0) {
            cout << "ERROR: Umbral debe ser mayor que 0." << endl;
            return 0;
        }
    }
    cin >> maximo_usuarios;
    if(maximo_usuarios < 0) {
        cout << "ERROR: Cantidad invalida de usuarios." << endl;
        return 0;
    }
    cin >> maximo_peliculas;
    if(maximo_peliculas < 0) {
        cout << "ERROR: Cantidad invalida de peliculas." << endl;
        return 0;
    }
    int usuariopelicula[maximo_usuarios][maximo_peliculas];
    int apariciones[maximo_peliculas];
    for(j = 0; j < maximo_peliculas; j++) {
        apariciones[j] = 0;
        for(i = 0; i < maximo_usuarios; i++)
            usuariopelicula[i][j] = 0;
    }
    int usuario, pelicula, ranking, dif;
    while(!cin.eof()) {
        cin >> usuario;
        if(cin.eof())
            break;
        if(usuario < 0 || usuario >= maximo_usuarios) {
            cout << "ERROR: Tabla erronea. Id de usuario fuera de limites." << endl;
            return 0;
        }
        cin >> pelicula;
        if(cin.eof())
            break;
        if(pelicula < 0 || pelicula >= maximo_peliculas) {
            cout << "ERROR: Tabla erronea. Id de pelicula negativo." << endl;
            return 0;
        }
        cin >> ranking;
        if(ranking > u_min) {
            dif = ranking - u_min;
            usuariopelicula[usuario][pelicula] += dif;
            apariciones[pelicula] += dif;
            if(pelicula >= cantidad_peliculas)
                cantidad_peliculas = pelicula + 1;
            if(usuario >= cantidad_usuarios)
                cantidad_usuarios = usuario + 1;
        }
    }
    int sumatotal[cantidad_peliculas];
    int cualsuma, maxsuma;
    for(i = 0; i < cantidad_peliculas; i++) {
        if(apariciones[i] < umbral)
            continue;
        ostringstream nombre;
        nombre << "spel" << i;
        ofstream salida(nombre.str().c_str(), ios::out);
//        for(j = i + 1; j < cantidad_peliculas; j++)
        for(j = 0; j < cantidad_peliculas; j++)
            sumatotal[j] = 0;
        for(j = 0; j < cantidad_usuarios; j++)
            if(usuariopelicula[j][i] > 0)
//                for(k = i + 1; k < cantidad_peliculas; k++)
                for(k = 0; k < cantidad_peliculas; k++)
                    if(k != i && apariciones[k] > umbral)
                        sumatotal[k] += usuariopelicula[j][k]*usuariopelicula[j][i];
        while(1) {
            maxsuma = 0;
            cualsuma = -1;
//            for(j = i + 1; j < cantidad_peliculas; j++)
            for(j = 0; j < cantidad_peliculas; j++)
                if(j != i && sumatotal[j] >= umbral && sumatotal[j] > maxsuma) {
                    maxsuma = sumatotal[j];
                    cualsuma = j;
                }
            if(cualsuma == -1)
                break;
            salida << i << " " << cualsuma << " " << sumatotal[cualsuma]
                 << " " << ((double)sumatotal[cualsuma])/apariciones[i] << endl;
            sumatotal[cualsuma] = -1;
        }
        salida.close();
    }
}
