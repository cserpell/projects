use warnings;
use strict;

if(@ARGV < 1) {
  print STDERR "Uso: parear keywords\n";
  exit 0;
}

#open(G, "<".$ARGV[0]);  # Categorias
open(H, "<".$ARGV[0]);  # Keywords

my $lastcatfromkeys = "";

while(my $line = <H>) {
  chomp $line;
  
  if(length($line) <= 0) {
    next;
  }
  if(substr($line, 0, 1) eq "*") {
    $lastcatfromkeys = substr($line, 2);
    print $lastcatfromkeys."\n";
    next;
  }
  print $lastcatfromkeys."\t".$line."\n";
}
