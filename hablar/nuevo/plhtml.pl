use strict;
use warnings;

use HTML::Parser;
use HTML::TreeBuilder;
use HTML::Element;

use Data::Dumper;

if(@ARGV < 2) {
  print "Usage: plhtml file_to_process base_num_new_file\n";
  exit 0;
}

#my @queue = ();

open(CATF, ">>categories_output.txt");
open(KEYF, ">>keywords_output.txt");

open(F, "<".$ARGV[0]) or die("-1");
my $next_queue = $ARGV[1];

#while(my $line = <F>) {
#  chomp($line);
#  push(@queue, $line);
#}

#while(my $baseurl = shift(@queue)) {
my $line = <F>;
if(!$line) {
  print STDERR "ERROR: empty file $ARGV[0]\n";
  print "-1";
  exit 0;
}
chomp($line);
my $baseurl = $line;

print STDERR $baseurl."\n";

my $durl = $baseurl;

system("rm index.html");
system("wget http://www.linkedwords.com/".$durl);

my $tree = HTML::TreeBuilder->new;
$tree->parse_file("index.html");

print CATF $durl."\n";

my @elements = $tree->find("ul");
foreach my $element (@elements) {
  if(!defined($element->attr("class"))) {
    next;
  }
  next if($element->attr("class") ne "cat");
  my @subcats = $element->find("a");
  foreach my $subcat (@subcats) {
    if(!defined($subcat->attr("href"))) {
      print STDERR "Warning: Sub category missing url?\n";
      next;
    }
    if(!defined($subcat->attr("title"))) {
      print STDERR "Warning: Sub category missing title?\n";
      next;
    }
    my $nexturl = $subcat->attr("href");
    my $name = $subcat->attr("title");
    #print "CAT: ".$nexturl." -- ".$name."\n";
 #   print CATF $durl.$nexturl."\n";

    open(G, ">tempdir/file".$next_queue);
    print G $durl.$nexturl;
    close(G);
    $next_queue++;
    #push(@queue, $durl.$nexturl);
  }
#  print $element->attr("class")."\n";
}

print KEYF "* ".$durl."\n";

@elements = $tree->find("li");
my $fli = 0;
foreach my $element (@elements) {
  if(!defined($element->attr("style"))) {
    next;
  }
  if(!($element->attr("style") =~ /page1/g)) {
    next;
  }
  my @subcats = $element->find("a");
  foreach my $subcat (@subcats) {
    if(!defined($subcat->attr("title"))) {
      print STDERR "Warning: Keyword (1) missing title?\n";
      next;
    }
    my $name = $subcat->attr("title");
    #print "KEY: ".$name."\n";
    print KEYF $name."\n";
  }
  $fli = 1;
#  print $element->attr("class")."\n";
}

@elements = $tree->find("ul");
foreach my $element (@elements) {
  if(!defined($element->attr("class"))) {
    next;
  }
  next if($element->attr("class") ne "kwp");
  if($fli) {
    print STDERR "Warning: Possible mistake when parsing...\n";
    $fli = 0;
  }
  my @subcats = $element->find("a");
  foreach my $subcat (@subcats) {
    if(!defined($subcat->attr("title"))) {
      print STDERR "Warning: Keyword (2) missing title?\n";
      next;
    }
    my $name = $subcat->attr("title");
    #print "KEY: ".$name."\n";
    print KEYF $name."\n";
  }
#  print $element->attr("class")."\n";
}

#}

print $next_queue;

exit 0;

my $categories = 0;

sub get_starts {
  my ($attr_ref, $attrseq, $text, $tagname) = @_;
#  my $attr_ref = shift;
  my %attr = %{$attr_ref};
#  my $attrseq_ref = shift;
  my @attrseq = @{$attr_ref};
#  my $text = shift;
#  my $tagname = shift;
print $tagname."\n";
  if($tagname eq "ul") {
    print "Found ul\n";
    if(defined($attr{"class"}) && ($attr{"class"} eq "cat")) {
      $categories = 1;
    }
  }
}

sub get_ends {
  my $attr_ref = shift;
  my %attr = %{$attr_ref};
  my $attrseq_ref = shift;
  my @attrseq = @{$attr_ref};
  my $text = shift;
  my $tagname = shift;

print $tagname."\n";
  if($tagname eq "ul") {
    $categories = 0;
    print "End ul\n";
  }
}
