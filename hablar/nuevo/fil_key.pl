use warnings;
use strict;

my $thiscat = "";
my $savethis = 0;

while(my $line = <STDIN>) {
  chomp $line;
  if(length($line) <= 0) {
    next;
  }
  if(substr($line, 0, 1) eq "*") {
    my $newname = substr($line, 2);
    if($newname =~ /\/\//g) {
      $savethis = 0;
    } else {
      $savethis = 1;
      print STDERR $newname."\n";
      $thiscat = $newname;
    }
    next;
  }
  if($savethis) {
    print $thiscat."\t".$line."\n";
  }
}
