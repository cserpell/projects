<?php

/*
	Netsplit irc crawler. Tries to retrieve every channel indexed.
*/
function scanServers($html) {
	$pattern = "/<tr> <td nowrap=\"nowrap\">(\S+)<\/td> <td>(\S+)<\/td><td>off<\/td><\/tr>/";
	preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
	return $matches;
}
function normalize($string) {
				$string = preg_replace("/\s+/", " ", $string);
				$string = preg_replace("/&nbsp;/", "", $string);
				return $string;
}
function scanChannels($html) {
	$STRING = ".*?";
	$NAME = "(.*?)";
	$TOPIC = "(.*?)";
	$NUMBER = "(\d+)";
	$pattern = "/<span class=\"cs-channel\"><strong><a href=\"$STRING\" title=\"$STRING\">$NAME<\/a><\/strong><\/span> <br> <span class=\"cs-network\">$STRING<\/span> <\/td> <td align=\"center\" valign=\"top\" class=\"cs-users\"> $NUMBER <\/td> <td$STRING><span class=\"cs-topic\">$TOPIC<\/span>/";
	preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
	return $matches;
}
function scanNetworks($html) {
	$pattern = "/<a class=\"\w+\" href=\"\/networks\/(\S+)\/\"[^>]*>/";
	preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
	return $matches;
}
function download($url, $out) {
	$url = escapeshellcmd($url); 
	$out = escapeshellcmd($out); 
	file_exists($out) or exec("wget $url -O $out");
	return file_get_contents($out);
}
function scanChannelCount($html) {
	$pattern = "/(\d+) matching entries found/";
	preg_match($pattern, $html, $matches);
	return $matches[1];
}
function downloadPage($network, $offset) {
	
	$html = download("http://irc.netsplit.de/channels/?net=$network&chat=&num=$offset", "networks/$network/$offset.html");
	$html = normalize($html);
	return $html;
}
function createDir($path) {
				file_exists($path) or mkdir($path) or die("error creating $path");
}

$link = mysql_connect('localhost', 'fruribe', 'Da1mytupry')
	or die('Not connected : ' . mysql_error());

$db_selected = mysql_select_db('fruribe', $link) 
	or die ('Can\'t use foo : ' . mysql_error());

					
//retrieve list of networks
$html_networks = file_get_contents('index.html') or die("network file");
$networks = scanNetworks($html_networks);
createDir("networks");
foreach ($networks as $network_) {
	$network = $network_[1];
	$network = mysql_real_escape_string($network);
	echo "#scanning: $network\n";
	createDir("networks/$network/");
	$html_network = download("http://irc.netsplit.de/networks/$network/", "networks/$network/info.html");
	if (!$html_network) {
		continue;
	}
	$html_network = normalize($html_network);
	$servers = scanServers($html_network);
	foreach ($servers as $server_) {
	//print_r($server_);
		$host = $server_[1];
		$host = mysql_real_escape_string($host);
		$port = $server_[2];
		echo "INSERT INTO servers(network, host, port) 
					VALUES ('$network', '$host', $port);\n";
	}
	$offset = 0;
	do {
		$html_channels = downloadPage($network, $offset);
		$channel_count = scanChannelCount($html_channels);
		$channels = scanChannels($html_channels);
		foreach ($channels as $channel) {
			$name = $channel[1];
			$user_count = $channel[2];
			$topic = $channel[3];
			$name = mysql_real_escape_string($name);
			$user_count = mysql_real_escape_string($user_count);
			$topic = mysql_real_escape_string($topic);
			echo "INSERT INTO channels_(network, name, user_count, topic) 
				VALUES ('$network', '$name', $user_count, '$topic');\n";
		}
		$offset += 9;

	} while ($offset < $channel_count);
}
mysql_close($link)
?> 

