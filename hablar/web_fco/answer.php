<?php
require_once("database.php");

function get_channels_list() {
  $conn = openDatabaseConnection();
  $query = "SELECT network, server, channel, topic, user_count FROM channels";
  $res = mysql_query($query, $conn) or die("ERROR0 !! ".mysql_error());
  $newarray = array();
  $i = 0;
  while($newchannel = mysql_fetch_array($res)) {
    $kk = array_keys($newchannel);
    array_push($newarray, array());
    foreach($kk as $kkey) {
        $newarray[$i][$kkey] = $newchannel[$kkey];
    }
//    foreach($kk as $kkey) {
  //      echo $newarray[$i][$kkey]."<br />";
    //}
    $i++;
  }
  return $newarray;
}

function get_random_user() {
  $conn = openDatabaseConnection();
  $query = "SELECT uid, topic FROM uid ORDER BY RAND() LIMIT 1";
  $res = mysql_query($query, $conn) or die("ERROR1 !! ".mysql_error());
  $newuser = mysql_fetch_array($res);
  if(!$newuser) {
//    die("ERROR2 !!");
      $newuser = "YOOOOO";
  }
  return $newuser["user"];
}

function compare_channel($a, $b) {
//  print_r($a);
  //echo "<br />";
//  print_r($b);
  //echo "<br />";
//  echo ($a[1] - $b[1])."<br />";
  $difff = $a[1] - $b[1];
  if($difff < 0)
    return 1;
  if($difff > 0)
    return -1;
  return 0;
}

function match_channels_keys($keywords, $channellist) {
  $STR_REPLACE_FROM = "áéíóúñÁÉÍÓÚÑüÜ";
  $STR_REPLACE_TO   = "aeiounAEIOUNuU";
  $STR_TOKEN_STOPS  = " \n\t'\"#%()/&$·\"!?+*^~\\{}[]-_,.;:<>";
      // Agregar mas casos especiales

  $scorearray = array();
  $chalen = count($channellist);
  
//  echo $chalen."<br />";
  
  // Build keywords array
  $keysarray = array();
  $keys = strtr($keywords, $STR_REPLACE_FROM, $STR_REPLACE_TO);
//  $keys = $keywords;
//  echo "Keywords: ".$keys."<br />";
  $num_keys = 0;
  $tok = strtok($keys, $STR_TOKEN_STOPS);
  while($tok) {
    $keysarray[$num_keys] = $tok;
 //   echo $tok."<br />";
    $num_keys++;
    $tok = strtok($STR_TOKEN_STOPS);
  }
//  $keysarray = split($STR_TOKEN_STOPS, $keys);
  //$num_keys = size($keysarray);
  
  // Revisar canales y asignar puntaje
  $i = 0;
  while($i < $chalen) {
    $topic = strtr($channellist[$i]["topic"]." ".$channellist[$i]["channel"], $STR_REPLACE_FROM, $STR_REPLACE_TO);
//    $topic = $channellist[$i]["topic"];
    $newscore = 0;
    $num_words = 0;
    $tok = strtok($topic, $STR_TOKEN_STOPS);
    while($tok) {
      $word = $tok;
      // distancia de edicion
      $j = 0;
      while($j < $num_keys) {
        $dist = @levenshtein($word, $keysarray[$j]);
        if($dist != -1) {
//          echo $num_keys." : ".$word." <-> ".$keysarray[$j]." = ".$dist."<br />";
          $newscore = $newscore + exp(-$dist*100);
        }
        $j++;
      }
      $tok = strtok($STR_TOKEN_STOPS);
      $num_words++;
    }
    array_push($scorearray, array());
    $scorearray[$i][0] = $channellist[$i]["channel"]."@".
                         $channellist[$i]["server"];
    if($num_words != 0)
      $scorearray[$i][1] = log($channellist[$i]["user_count"] + 1)*$newscore/log($num_words + 1);
    else
      $scorearray[$i][1] = 0;
    $scorearray[$i][2] = $channellist[$i]["server"];
    $scorearray[$i][3] = $channellist[$i]["topic"];
    $i++;
  }
  usort($scorearray, "compare_channel");
  return $scorearray;
}

require_once("database.php");

$conn = openDatabaseConnection();

require_once("files.php");

// Recibe los keywords ingresados
if(!isset($_POST["keywords"])) {
  // Vuelve a la p'agina original, consulta vac'ia
  header("Location: ".$INI_FILE);
	die("");
}
$keywords = $_POST["keywords"];

$channellist = get_channels_list();

$topchannels = match_channels_keys($keywords, $channellist);

$newuser = "";
// Reutiliza un usuario que ya estaba conectado en IRC
if(isset($_POST["olduser"])) {
  $olduser = $_POST["olduser"];
  $newuser = $olduser;
} else {
  $newuser = get_random_user();
}


?>

<html>
<body>
<SCRIPT LANGUAGE="JavaScript">
function append(line) {
var cont = document.getElementById('content').innerHTML;
document.getElementById('content').innerHTML = line +'<br/>'+cont;
}
</SCRIPT>
<table>
<tr><td>Canal</td><td>Puntaje</td><td>Descripci&oacute;n</td></tr>
<?php
$showed = 0;
foreach($topchannels as $chan) {
?>
<tr>
<td><? echo htmlentities($chan[0]); ?></td>
<td><? echo htmlentities($chan[1]); ?></td>
<td><? echo htmlentities($chan[3]); ?></td>
</tr>
<?
$showed++;
if($showed > 100)
  break;
}

//die("</body></html>");

$nickname = "blahh123";
$password = "blahh123";
//$server = "irc.freenode.net";
$server = $topchannels[0][2];
$port = "6667";
//$channels = "#topichat1@irc.freenode.net,#topichat2@irc.freenode.net,#topichat1@irc.fef.net";
//$channels = "#topichat1@irc.freenode.net,#topichat2@irc.freenode.net";
$channels = $topchannels[0][0];

?>
</table>
<P>
<APPLET archive="signed.jar" code="topichat/IrcApplet.class" width=350 height=50 MAYSCRIPT>
        <PARAM NAME="channels" VALUE="<?=$channels?>"> 
        <!-- <PARAM NAME="server" VALUE="<?=$server?>"> -->
        <!-- <PARAM NAME="port" VALUE="<?=$port?>"> -->
        <PARAM NAME="nickname" VALUE="<?=$nickname?>"> 
        <PARAM NAME="password" VALUE="<?=$password?>"> 
        <PARAM NAME="debug" VALUE="1"> 
</APPLET>
<DIV id="content">
</DIV>
</P>
</body>
</html>
