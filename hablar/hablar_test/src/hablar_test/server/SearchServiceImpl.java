package hablar_test.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import hablar_test.client.SearchService;
import hablar_test.client.Result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.lang.System;
import java.lang.String;

public class SearchServiceImpl extends RemoteServiceServlet implements SearchService {
  private static final String[] FIRST_NAMES = new String[] {
      "Inman", "Sally", "Omar", "Teddy", "Jimmy", "Cathy", "Barney", "Fred",
      "Eddie", "Carlos"};

  private static final String[] LAST_NAMES = new String[] {
      "Smith", "Jones", "Epps", "Gibbs", "Webber", "Blum", "Mendez",
      "Crutcher", "Needler", "Wilson", "Chase", "Edelstein"};

  private static final String[] SUBJECTS = new String[] {
      "Chemistry", "Phrenology", "Geometry", "Underwater Basket Weaving",
      "Basketball", "Computer Science", "Statistics", "Materials Engineering",
      "English Literature", "Geology"};

  private Random rnd;

  public SearchServiceImpl() {
    rnd = new Random(System.currentTimeMillis());
  }

  public Result[] getResults(String query, String session, int maxCount) {
    if(maxCount < 0)
      return null;
    Result[] res = new Result[maxCount];
    for(int i = 0; i < maxCount; i++) {
      String name = pickRandomString(FIRST_NAMES) + " " + pickRandomString(FIRST_NAMES);
      String desc = pickRandomString(SUBJECTS);
      res[i] = new Result(name, desc);
    }
    return res;
  }

  private String pickRandomString(String[] a) {
    int i = rnd.nextInt(a.length);
    return a[i];
  }
}
