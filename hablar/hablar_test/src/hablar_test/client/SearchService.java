package hablar_test.client;

import com.google.gwt.user.client.rpc.RemoteService;

import java.lang.String;

public interface SearchService extends RemoteService {
  Result[] getResults(String query, String session, int maxCount);
}
