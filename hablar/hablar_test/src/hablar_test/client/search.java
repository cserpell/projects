package hablar_test.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Label;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class search implements EntryPoint {
/*  private class Buscador {
      private search caller;
      public search getCaller() {
        return caller;
      }
      public void setCaller(search newCaller) {
        caller = newCaller;
      }
  }*/
  private class BButton extends Button {
    public search caller;
  }
  private class BTextBox extends TextBox {
    public search caller;
  }
//  BButton button;
//  BTextBox tBox;
//  SearchWidget sWidg;
  /**
   * This is the entry point method.
   */
  public void onModuleLoad() {
    Image img = new Image("http://code.google.com/webtoolkit/logo-185x175.png");
/*    button = new BButton("Click me");
    tBox = new BTextBox();
    sWidg = new SearchWidget();*/
    Button button = new Button("Click me");
    final TextBox tBox = new TextBox();
    final SearchWidget sWidg = new SearchWidget();

    VerticalPanel vPanel = new VerticalPanel();
    HorizontalPanel hPanel = new HorizontalPanel();
    // We can add style names.
    vPanel.addStyleName("widePanel");
    hPanel.addStyleName("widePanel");
    vPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
    hPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
    vPanel.add(img);
    hPanel.add(new Label("Quiero hablar de"));
    hPanel.add(tBox);
    hPanel.add(button);
    vPanel.add(hPanel);
    vPanel.add(sWidg);

    // Add image and button to the RootPanel
    RootPanel.get().add(vPanel);

    // Create the dialog box
    final DialogBox dialogBox = new DialogBox();
    dialogBox.setText("Pronto podrás hablar!");
    dialogBox.setAnimationEnabled(true);
    Button closeButton = new Button("Cerrar");
    VerticalPanel dialogVPanel = new VerticalPanel();
    dialogVPanel.setWidth("100%");
    dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
    dialogVPanel.add(closeButton);

    closeButton.addClickListener(new ClickListener() {
      public void onClick(Widget sender) {
        dialogBox.hide();
      }
    });

    // Set the contents of the Widget
    dialogBox.setWidget(dialogVPanel);

    button.addClickListener(new ClickListener() {
      public void onClick(Widget sender) {
//        String query = caller.tBox.getText();
        String query = tBox.getText();
        if(query.equals("")) {
          dialogBox.center();
          dialogBox.show();
        } else
//          caller.sWidg.updateRowData(query, "sesion01", 8);
          sWidg.updateRowData(query, "sesion01", 8);
      }
    });
    tBox.addKeyboardListener(new KeyboardListener() {
      public void onKeyDown(Widget sender, char keyCode, int modifiers) {
        if(keyCode == KeyboardListener.KEY_ENTER) {
//          String query = caller.tBox.getText();
          String query = tBox.getText();
          if(query.equals("")) {
            dialogBox.center();
            dialogBox.show();
          } else
//            caller.sWidg.updateRowData(query, "sesion01", 8);
            sWidg.updateRowData(query, "sesion01", 8);
        }
      }
      public void onKeyPress(Widget sender, char keyCode, int modifiers) {
      }
      public void onKeyUp(Widget sender, char keyCode, int modifiers) {
      }
    });
  }
}
