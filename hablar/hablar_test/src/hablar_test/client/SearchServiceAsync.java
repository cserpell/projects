package hablar_test.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.lang.String;

public interface SearchServiceAsync {
  void getResults(String query, String session, int maxCount, AsyncCallback callback);
}
