package hablar_test.client;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.lang.String;

public class Result implements IsSerializable {
  private String userName;
  private String description;
  public Result(String newUserName, String newDescription) {
    userName = newUserName;
    description = newDescription;
  }
  public Result() {
  }
  public int compareTo(Result another) {
    return userName.compareTo(another.userName);
  }
  public String getName() {
    return userName;
  }
  public String getDescription() {
    return description;
  }
}
