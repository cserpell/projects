package hablar_test.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.FlexTable;

import java.lang.IndexOutOfBoundsException;

/**
 * A Composite widget that abstracts a DynaTableWidget and a data provider tied
 * to the <@link SchoolCalendarService> RPC endpoint.
 */
public class SearchWidget extends FlexTable {
  private final SearchServiceAsync calService;
  private int lastMaxCount;
  private Result[] lastResults;
  private String lastQuery;

  public void updateRowData(final String query, final String session, final int maxCount) {
    // Check the simple cache first.
    if(maxCount == lastMaxCount) {
      if(query.equals(lastQuery)) {
        // Use the cached batch.
        pushResults(lastResults);
        return;
      }
    }
    // Fetch the data remotely.
    calService.getResults(query, session, maxCount, new AsyncCallback() {
      public void onFailure(Throwable caught) {
        int newRow = insertRow(0);
        addCell(newRow);
        setText(newRow, 0, caught.getMessage());
        addCell(newRow);
        setText(newRow, 1, caught.toString());
//        System.out.println(caught.getMessage());
//        System.out.println(caught.printStackTrace());
        caught.printStackTrace();
      }
      public void onSuccess(Object resultObject) {
        Result[] result = (Result[]) resultObject;
        lastQuery = query;
        lastMaxCount = maxCount;
        lastResults = result;
        pushResults(result);
      }
    });
  }

  private void pushResults(Result[] results) {
    while(getRowCount() > 1) {
      removeRow(1);
    }
    for (int i = 0, n = results.length; i < n; i++) {
      Result person = results[i];
      int index = this.insertRow(i + 1);
      this.addCell(index);
      this.setText(index, 0, person.getName());
      this.addCell(index);
      this.setText(index, 1, person.getDescription());
    }
  }

  public SearchWidget() {
    lastMaxCount = -1;
    lastQuery = "";
    int newRow = this.insertRow(0);
    this.addCell(newRow);
    this.setText(newRow, 0, "Name");
    this.addCell(newRow);
    this.setText(newRow, 1, "Description");
    // Initialize the service.
    calService = (SearchServiceAsync) GWT.create(SearchService.class);
    // By default, we assume we'll make RPCs to a servlet, but see
    // updateRowData(). There is special support for canned RPC responses.
    // (Which is a totally demo hack, by the way :-)
    ServiceDefTarget target = (ServiceDefTarget) calService;
    // Use a module-relative URLs to ensure that this client code can find
    // its way home, even when the URL changes (as might happen when you
    // deploy this as a webapp under an external servlet container).
    String moduleRelativeURL = GWT.getModuleBaseURL() + "/results";
    target.setServiceEntryPoint(moduleRelativeURL);
  }
}
