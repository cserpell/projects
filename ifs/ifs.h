#include <iostream>
#include <cmath>

class punto
{
 public:
  double x;
  double y;
  double z;
  punto(punto &p)
  {
    x = p.x;
    y = p.y;
    z = p.z;
  }
  punto()
  {
    x = 0;
    y = 0;
    z = 0;
  }
  punto(double a, double b, double c)
  {
    x = a;
    y = b;
    z = c;
  }
  void copiar(punto &p)
  {
    x = p.x;
    y = p.y;
    z = p.z;
  }
  punto mas(punto &p)
  {
    punto r(x + p.x, y + p.y, z + p.z);

    return r;
  }
};

class matriz
{
 public:
  double a11;
  double a12;
  double a13;
  double a21;
  double a22;
  double a23;
  double a31;
  double a32;
  double a33;
  matriz(matriz &m)
  {
    a11 = m.a11;
    a12 = m.a12;
    a13 = m.a13;
    a21 = m.a21;
    a22 = m.a22;
    a23 = m.a23;
    a31 = m.a31;
    a32 = m.a32;
    a33 = m.a33;
  }
  matriz()
  {
    a11 = 0;
    a12 = 0;
    a13 = 0;
    a21 = 0;
    a22 = 0;
    a23 = 0;
    a31 = 0;
    a32 = 0;
    a33 = 0;
  }
  matriz(double b11, double b12, double b13, double b21, double b22, double b23, double b31, double b32, double b33)
  {
    a11 = b11;
    a12 = b12;
    a13 = b13;
    a21 = b21;
    a22 = b22;
    a23 = b23;
    a31 = b31;
    a32 = b32;
    a33 = b33;
  }
  punto porpunto(punto &p)
  {
	punto r(a11*p.x + a12*p.y + a13*p.z, a21*p.x + a22*p.y + a23*p.z, a31*p.x + a32*p.y + a33*p.z);

	return r;
  }
};

class IFS
{
 private:
  matriz *trans;
  punto *afinid;
  int numtrans;
  int maxtrans;
 public:
  IFS(int nt)
  {
    numtrans = 0;
    maxtrans = nt;
    trans = new matriz[nt];
    afinid = new punto[nt];
  }
  void DefinirTrans(matriz &nuevam, punto &nuevaa)
  {
    if(numtrans < maxtrans)
    {
      trans[numtrans].a11 = nuevam.a11;
      trans[numtrans].a12 = nuevam.a12;
      trans[numtrans].a13 = nuevam.a13;
      trans[numtrans].a21 = nuevam.a21;
      trans[numtrans].a22 = nuevam.a22;
      trans[numtrans].a23 = nuevam.a23;
      trans[numtrans].a31 = nuevam.a31;
      trans[numtrans].a32 = nuevam.a32;
      trans[numtrans].a33 = nuevam.a33;
      afinid[numtrans].x = nuevaa.x;
      afinid[numtrans].y = nuevaa.y;
      afinid[numtrans].z = nuevaa.z;
      numtrans++;
    }
  }
  punto *Iterar(punto &p)
  {
    punto *res;

    res = new punto[maxtrans];
    for(int i = 0; i < numtrans; i++)
    {
	  punto *pp = &(trans[i].porpunto(p).mas(afinid[i]));
		
	  res[i].copiar(*pp);
	}
    return res;
  }
  int NumT()
  {
    return numtrans;
  }
};

class curva9 : public IFS
{
 public:
  curva9() : IFS(2)
  {
    matriz m1(1.0/2, sqrt(3)/6, 0, sqrt(3)/6, -1.0/2, 0, 0, 0 ,0);
    matriz m2(2.0/3, 0, 0, 0, -2/3, 0, 0, 0, 0);
	punto p1(0, 0, 0);
	punto p2(1.0/3, 0, 0);

	DefinirTrans(m1, p1);
    DefinirTrans(m2, p2);
  }
};

class curva8 : public IFS
{
 public:
  curva8() : IFS(4)
  {
    matriz m1(0, 0, 0, 0, 0.16, 0, 0, 0, 0);
    matriz m2(0.2, 0.26, 0, -0.23, 0.22, 0, 0, 0, 0);
    matriz m3(-0.15, -0.28, 0, -0.26, 0.24, 0, 0, 0 ,0);
    matriz m4(0.85, -0.04, 0, 0.04, 0.85, 0, 0, 0 ,0);
	punto p1(0.203, -0.035, 0);
	punto p2(0.173, 1.628, 0);
	punto p3(0.222, 0.465, 0);
	punto p4(0.029, 1.597, 0);

	DefinirTrans(m1, p1);
    DefinirTrans(m2, p2);
    DefinirTrans(m3, p3);
    DefinirTrans(m4, p4);
  }
};
