#include <iostream>
#include <fstream>
#include "ifs.h"

void Ite(IFS &f, punto &p, int orden, int ordmax)
{
  if(orden == ordmax)
  {
    std::cout << p.x << "\t" << p.y << std::endl;
    return;
  }
  punto *a1;
  
  a1 = f.Iterar(p);
  for(int i = 0; i < f.NumT(); i++)
  {
    Ite(f, a1[i], orden + 1, ordmax);
  }
}

int main(int argc, char *argv[])
{
  int it, N, mt;
  char str[20];
  std::ifstream ain;

  if(argc == 1)
    return 0;

  ain.open(argv[1]);
  str[0] = '\0';
  ain >> str;
  mt = atoi(str);
  IFS f(mt);
  for(int i = 0; i < mt; i++)
  {
    double a1, a2, a3, a4, a5, a6, a7, a8, a9;

    str[0] = '\0';
    ain >> str;
    a1 = atof(str);
    str[0] = '\0';
    ain >> str;
    a2 = atof(str);
    str[0] = '\0';
    ain >> str;
    a3 = atof(str);
    str[0] = '\0';
    ain >> str;
    a4 = atof(str);
    str[0] = '\0';
    ain >> str;
    a5 = atof(str);
    str[0] = '\0';
    ain >> str;
    a6 = atof(str);
    str[0] = '\0';
    ain >> str;
    a7 = atof(str);
    str[0] = '\0';
    ain >> str;
    a8 = atof(str);
    str[0] = '\0';
    ain >> str;
    a9 = atof(str);
    matriz mm(a1, a2, a3, a4, a5, a6, a7, a8, a9);
    str[0] = '\0';
    ain >> str;
    a1 = atof(str);
    str[0] = '\0';
    ain >> str;
    a2 = atof(str);
    str[0] = '\0';
    ain >> str;
    a3 = atof(str);
	punto pp(a1, a2, a3);
	f.DefinirTrans(mm, pp);
  }
  str[0] = '\0';
  ain >> str;
  it = atoi(str);
  str[0] = '\0';
  ain >> str;
  N = atoi(str);
  for(int i = 0; i < N ; i++)
  {
    double p1, p2, p3;  // Ojo: (x,y,z)

    str[0] = '\0';
	ain >> str;
	p1 = atof(str);
	str[0] = '\0';
    ain >> str;
    p2 = atof(str);
	str[0] = '\0';
    ain >> str;
    p3 = atof(str);

    punto pp(p1, p2, p3);

    Ite(f, pp, 0, it);
  }
  ain.close();
}
