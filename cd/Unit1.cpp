//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
        num = 0;
        bmp = new Graphics::TBitmap;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
        Dragon.BorrarCurva();
        num = Dragon.CalcularCurva(StrToInt(Edit1->Text));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
        Dragon.TraspasarCurva(StrToInt(Edit2->Text));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
        int ax;
        int ay;
        float grid = 8;
        float cte;

        if(CheckBox1->Checked)
        {
                float cx, cy;

                cx = (PaintBox1->Width - 2)/Dragon.RangoX();
                cy = (PaintBox1->Height - 2)/Dragon.RangoY();

                if(cx < cy)
                        grid = cx;
                else
                        grid = cy;
        }

        cte = grid / 9;
        ax = grid*Dragon.RangoMinX() + 1;
        ay = grid*Dragon.RangoMinY() + 1;
        bmp->Width = PaintBox1->Width;
        bmp->Height = PaintBox1->Height;

        bmp->Canvas->Pen->Color = clBlack;
        bmp->Canvas->Brush->Color = clBlack;
        bmp->Canvas->Rectangle(0, 0, PaintBox1->Width, PaintBox1->Height);

        bmp->Canvas->LineTo(
                (Dragon.OrigX[0]*6 + Dragon.DestX[0]*3)*cte + ax,
                (Dragon.OrigY[0]*6 + Dragon.DestY[0]*3)*cte + ay);
        bmp->Canvas->Pen->Color = clWhite;
        for(int i = 0; i <= num; i++)
        {
                bmp->Canvas->LineTo(
                (Dragon.OrigX[i]*6 + Dragon.DestX[i]*3)*cte + ax,
                (Dragon.OrigY[i]*6 + Dragon.DestY[i]*3)*cte + ay);
                bmp->Canvas->LineTo(
                (Dragon.OrigX[i]*3 + Dragon.DestX[i]*6)*cte + ax,
                (Dragon.OrigY[i]*3 + Dragon.DestY[i]*6)*cte + ay);
        }
        PaintBox1Paint(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBox1Paint(TObject *Sender)
{
        PaintBox1->Canvas->Draw(0, 0, bmp);        
}
//---------------------------------------------------------------------------

