//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "curva.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
// Calcula la curva del orden dado y la deja como string
int curva::CalcularCurva(int orden)
{
        int esc;

        CURVA[0] = true;
        esc = 1;
        while(orden > 1)
        {
                int t;

                t = esc;
                CURVA[esc] = true;
                esc++;
                for(int i = 0; i < t; i++)
                {
                        CURVA[esc] = CURVA[i];
                        esc++;
                }
                CURVA[(3*t + 1)/2] = false;
                orden--;
        }
        llena = true;
        np = esc;
        return np;
}
// Traspasa la curva del string a posiciones
void curva::TraspasarCurva(int di)
{
        int px, py, dir;

        minx = 0;
        miny = 0;
        maxx = 0;
        maxy = 0;

        px = 0;
        py = 0;
        dir = di;  // dir : 0 = arriba 1 = derecha 2 = abajo 3 = izquierda

        OrigX[0] = px;
        OrigY[0] = py;
        if(dir == 0)
                py--;
        else if(dir == 1)
                px++;
        else if(dir == 2)
                py++;
        else
                px--;
        if(px < minx)
                minx = px;
        else if(px > maxx)
                maxx = px;
        if(py < miny)
                miny = py;
        else if(py > maxy)
                maxy = py;
        DestX[0] = px;
        DestY[0] = py;
        for(int i = 1; i <= np; i++)
        {
                OrigX[i] = px;
                OrigY[i] = py;
                if(CURVA[i-1])
                {
                        dir++;
                        if(dir == 4)
                                dir = 0;
                } else
                {
                        dir--;
                        if(dir == -1)
                                dir = 3;
                }
                if(dir == 0)
                        py--;
                else if(dir == 1)
                        px++;
                else if(dir == 2)
                        py++;
                else
                        px--;
                if(px < minx)
                        minx = px;
                else if(px > maxx)
                        maxx = px;
                if(py < miny)
                        miny = py;
                else if(py > maxy)
                        maxy = py;
                DestX[i] = px;
                DestY[i] = py;
        }
        traspasada = true;
}
// Establece los valores iniciales
void curva::BorrarCurva(void)
{
        np = 0;
        llena = false;
        traspasada = false;
        minx = 0;
        miny = 0;
        maxx = 0;
        maxy = 0;
}
int curva::RangoMinX(void)
{
        return -minx;
}
int curva::RangoX(void)
{
        return -minx + maxx;
}
int curva::RangoMinY(void)
{
        return -miny;
}
int curva::RangoY(void)
{
        return -miny + maxy;
}

