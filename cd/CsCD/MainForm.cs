// project created on 14-10-2003 at 22:47
using System;
using System.Windows.Forms;

namespace MyFormProject 
{
	class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.Button button;
		private System.Windows.Forms.Splitter splitter;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.CheckBox checkBox;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.TextBox textBox;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.GroupBox groupBox;
		private System.Windows.Forms.PictureBox pictureBox;
		private int num;
		private System.Drawing.Bitmap bmp;
		private CsCD.curva Dragon;
//		private CsCD.curva Dragon;
//		private CsCD.curva Dragon;
//		private CsCD.curva Dragon;
		public MainForm()
		{
			InitializeComponent();
			Dragon = new CsCD.curva();
			num = 0;
		}
	
		// THIS METHOD IS MAINTAINED BY THE FORM DESIGNER
		// DO NOT EDIT IT MANUALLY! YOUR CHANGES ARE LIKELY TO BE LOST
		void ButtonClick(object sender, System.EventArgs e)
		{
        	Dragon.BorrarCurva();
        	//num = Dragon.CalcularCurva((int)Double.Parse(textBox.Text));
        	num = Dragon.CalcularCurva2((int)Double.Parse(textBox.Text));
			label.Text = "" + num;
	        Dragon.TraspasarCurva((int)Double.Parse(textBox2.Text));
		}
		
		void Button3Click(object sender, System.EventArgs e)
		{
        	float ax, ay;
        	float grid = 8;
			float ci, cd;
			System.Drawing.Graphics g;
			System.Drawing.Pen lapiz = new System.Drawing.Pen(System.Drawing.Color.Black);
			
            bmp = new System.Drawing.Bitmap(pictureBox.Width, pictureBox.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			g = System.Drawing.Graphics.FromImage(bmp);
			g.FillRectangle(new System.Drawing.SolidBrush(System.Drawing.Color.White), new System.Drawing.RectangleF(0, 0, bmp.Width, bmp.Height));
        	if(checkBox.Checked)
        	{
                float cx, cy;

                cx = (bmp.Width - 2)/Dragon.RangoX();
                cy = (bmp.Height - 2)/Dragon.RangoY();

                if(cx < cy)
                    grid = cx;
                else
                    grid = cy;
        	}
			ci = (float)Double.Parse(textBox3.Text);
			cd = 1 - ci;
    	    ax = grid*Dragon.RangoMinX() + 1;
	       	ay = grid*Dragon.RangoMinY() + 1;
        	for(int i = 0; i <= num; i++)
        	{
        		g.DrawLine(lapiz,
                	(Dragon.OrigX[i]*ci + Dragon.DestX[i]*cd)*grid + ax,
                	(Dragon.OrigY[i]*ci + Dragon.DestY[i]*cd)*grid + ay,
                	(Dragon.OrigX[i]*cd + Dragon.DestX[i]*ci)*grid + ax,
                	(Dragon.OrigY[i]*cd + Dragon.DestY[i]*ci)*grid + ay);
        		if(i != num)
	        		g.DrawLine(lapiz,
    	            	(Dragon.OrigX[i]*cd + Dragon.DestX[i]*ci)*grid + ax,
        	        	(Dragon.OrigY[i]*cd + Dragon.DestY[i]*ci)*grid + ay,
           	    (Dragon.OrigX[i + 1]*ci + Dragon.DestX[i + 1]*cd)*grid + ax,
               	(Dragon.OrigY[i + 1]*ci + Dragon.DestY[i + 1]*cd)*grid + ay);
        	}
        	this.pictureBox.Invalidate();
		}
		void Button4Click(object sender, System.EventArgs e)
		{
        	float ax, ay;
        	int grid = 8;
			float ci, cd;
			System.Drawing.Graphics g;
			System.Drawing.Pen lapiz = new System.Drawing.Pen(System.Drawing.Color.Black);
			System.Drawing.Bitmap bm = new System.Drawing.Bitmap(Dragon.RangoX()*grid + 2, Dragon.RangoY()*grid + 2, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			
			g = System.Drawing.Graphics.FromImage(bm);
			g.FillRectangle(new System.Drawing.SolidBrush(System.Drawing.Color.White), new System.Drawing.RectangleF(0, 0, bm.Width, bm.Height));
			ci = (float)Double.Parse(textBox3.Text);
			cd = 1 - ci;
    	    ax = grid*Dragon.RangoMinX() + 1;
	       	ay = grid*Dragon.RangoMinY() + 1;
        	for(int i = 0; i <= num; i++)
        	{
        		g.DrawLine(lapiz,
                	(Dragon.OrigX[i]*ci + Dragon.DestX[i]*cd)*grid + ax,
                	(Dragon.OrigY[i]*ci + Dragon.DestY[i]*cd)*grid + ay,
                	(Dragon.OrigX[i]*cd + Dragon.DestX[i]*ci)*grid + ax,
                	(Dragon.OrigY[i]*cd + Dragon.DestY[i]*ci)*grid + ay);
        		if(i != num)
	        		g.DrawLine(lapiz,
    	            	(Dragon.OrigX[i]*cd + Dragon.DestX[i]*ci)*grid + ax,
        	        	(Dragon.OrigY[i]*cd + Dragon.DestY[i]*ci)*grid + ay,
           	    (Dragon.OrigX[i + 1]*ci + Dragon.DestX[i + 1]*cd)*grid + ax,
               	(Dragon.OrigY[i + 1]*ci + Dragon.DestY[i + 1]*cd)*grid + ay);
        	}
			bm.Save("out.gif", System.Drawing.Imaging.ImageFormat.Gif);
		}
		
		void MainFormResize(object sender, System.EventArgs e)
		{
			this.pictureBox.Size = new System.Drawing.Size(this.ClientSize.Width - groupBox.Size.Width - splitter.Size.Width, this.ClientSize.Height);			
		}
		void PictureBoxPaint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			pictureBox.Image = bmp;
		}
		
		void InitializeComponent() {
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.groupBox = new System.Windows.Forms.GroupBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.checkBox = new System.Windows.Forms.CheckBox();
			this.button4 = new System.Windows.Forms.Button();
			this.splitter = new System.Windows.Forms.Splitter();
			this.button = new System.Windows.Forms.Button();
			this.label = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox
			// 
			this.pictureBox.Dock = System.Windows.Forms.DockStyle.Right;
			this.pictureBox.Location = new System.Drawing.Point(168, 0);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(448, 421);
			this.pictureBox.TabIndex = 6;
			this.pictureBox.TabStop = false;
			this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBoxPaint);
			// 
			// groupBox
			// 
			this.groupBox.Controls.Add(this.button4);
			this.groupBox.Controls.Add(this.label3);
			this.groupBox.Controls.Add(this.textBox3);
			this.groupBox.Controls.Add(this.label2);
			this.groupBox.Controls.Add(this.label);
			this.groupBox.Controls.Add(this.button);
			this.groupBox.Controls.Add(this.button3);
			this.groupBox.Controls.Add(this.textBox);
			this.groupBox.Controls.Add(this.textBox2);
			this.groupBox.Controls.Add(this.checkBox);
			this.groupBox.Dock = System.Windows.Forms.DockStyle.Left;
			this.groupBox.Location = new System.Drawing.Point(0, 0);
			this.groupBox.Name = "groupBox";
			this.groupBox.Size = new System.Drawing.Size(152, 421);
			this.groupBox.TabIndex = 7;
			this.groupBox.TabStop = false;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(72, 128);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(40, 20);
			this.textBox2.TabIndex = 4;
			this.textBox2.Text = "0";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(72, 152);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(40, 20);
			this.textBox3.TabIndex = 8;
			this.textBox3.Text = "0.7";
			// 
			// textBox
			// 
			this.textBox.Location = new System.Drawing.Point(72, 104);
			this.textBox.Name = "textBox";
			this.textBox.Size = new System.Drawing.Size(40, 20);
			this.textBox.TabIndex = 3;
			this.textBox.Text = "5";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(32, 64);
			this.button3.Name = "button3";
			this.button3.TabIndex = 2;
			this.button3.Text = "Dibujar";
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// checkBox
			// 
			this.checkBox.Checked = true;
			this.checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox.Location = new System.Drawing.Point(16, 264);
			this.checkBox.Name = "checkBox";
			this.checkBox.Size = new System.Drawing.Size(72, 16);
			this.checkBox.TabIndex = 5;
			this.checkBox.Text = "Escalada";
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(32, 184);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(72, 24);
			this.button4.TabIndex = 10;
			this.button4.Text = "Archivo";
			this.button4.Click += new System.EventHandler(this.Button4Click);
			// 
			// splitter
			// 
			this.splitter.Location = new System.Drawing.Point(152, 0);
			this.splitter.Name = "splitter";
			this.splitter.Size = new System.Drawing.Size(8, 421);
			this.splitter.TabIndex = 8;
			this.splitter.TabStop = false;
			// 
			// button
			// 
			this.button.Location = new System.Drawing.Point(32, 32);
			this.button.Name = "button";
			this.button.TabIndex = 0;
			this.button.Text = "Calcular";
			this.button.Click += new System.EventHandler(this.ButtonClick);
			// 
			// label
			// 
			this.label.Location = new System.Drawing.Point(16, 104);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(40, 16);
			this.label.TabIndex = 6;
			this.label.Text = "Orden:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 128);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 16);
			this.label2.TabIndex = 7;
			this.label2.Text = "Direccion:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 152);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 16);
			this.label3.TabIndex = 9;
			this.label3.Text = "Linea:";
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 421);
			this.Controls.Add(this.splitter);
			this.Controls.Add(this.groupBox);
			this.Controls.Add(this.pictureBox);
			this.Name = "MainForm";
			this.Text = "Curva de Dragon";
			this.Resize += new System.EventHandler(this.MainFormResize);
			this.groupBox.ResumeLayout(false);
			this.ResumeLayout(false);
		}
			
		[STAThread]
		public static void Main(string[] args)
		{
			Application.Run(new MainForm());
		}
	}			
}
