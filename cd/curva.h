//---------------------------------------------------------------------------
#ifndef curvaH
#define curvaH
//---------------------------------------------------------------------------
class curva
{
    private:
        bool CURVA[65536];
        bool llena;
        bool traspasada;
        int minx;
        int miny;
        int maxx;
        int maxy;
        int np;
    public:
        int CalcularCurva(int orden);
        void TraspasarCurva(int di = 0);
        void BorrarCurva(void);
        int RangoMinX(void);
        int RangoX(void);
        int RangoMinY(void);
        int RangoY(void);
        int OrigX[65537];
        int OrigY[65537];
        int DestX[65537];
        int DestY[65537];
        curva()
        {
                np = 0;
                llena = false;
                traspasada = false;
                minx = 0;
                miny = 0;
                maxx = 0;
                maxy = 0;
        }
};
#endif
