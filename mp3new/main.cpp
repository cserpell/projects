#include <fstream>
#include <iostream>
#include <string>
#include <list>
//#include <stdlib.h>

using namespace std;

int main(int argc, char *argv[])
{
    if(argc < 2)
        return 0;
    list<string> listaentera;
    try
    {
    for(int i = 1; i < argc; i++)
    {
        ifstream archivoentrada;
        archivoentrada.open(argv[i]);
        if(!archivoentrada)
        {
            cout << "El archivo " << argv[i] << " no existe." << endl;
            return 0;
        }
        cout << "Leyendo el archivo " << argv[i] << endl;
        while(!archivoentrada.eof())
        {
            string str;
            getline(archivoentrada, str); // La idea era hacerlo de otra manera
            if(str.length() != 0 && str[0] != '#')
            {
                transform(str.begin(), str.end(), str.begin(), (int(*)(int))tolower);
                listaentera.push_back(str);
            }
        }
        archivoentrada.close();
    }
    } catch(...)
    {
        cout << "Error al leer los archivos de origen." << endl;
        return 0;
    }
    cout << "Ordenando la lista." << endl;
    listaentera.sort();
    cout << "RESULTADOS:" << endl;
    string guarda = "";
    int count = 0;
    while(true)
    {
        string comp;
        if(listaentera.empty())
        {
            if(count == 1)
                cout << guarda << endl;
            break;
        }
        comp = listaentera.back();
        listaentera.pop_back();
        if(comp != guarda)
        {
            if(count == 1)
                cout << guarda << endl;
            guarda = comp;
            count = 0;
        }
        count++;
    }
    return 0;
}
