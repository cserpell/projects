NUMBERS = [3, 4, 9, 14, 15, 19, 28, 37, 47, 50, 54, 56, 59, 61, 70, 73, 78, 81, 92, 95, 97, 99]
NUMBERS_SET = set(NUMBERS)
NUMBERS_LENGTH = len(NUMBERS)


def test_from_pos(pos, sum_so_far, list_so_far):
  if sum_so_far >= 100:
    # Impossible, so stop.
    return
  if pos == NUMBERS_LENGTH:
    if sum_so_far in NUMBERS_SET and len(list_so_far) >= 2:
      print ' + '.join([str(n) for n in list_so_far]) + ' = ' + str(sum_so_far)
    return
  test_from_pos(pos + 1, sum_so_far, list_so_far)
  test_from_pos(pos + 1, sum_so_far + NUMBERS[pos], list_so_far + [NUMBERS[pos]])


test_from_pos(0, 0, [])
