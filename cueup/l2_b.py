# TEST_N = 227000

primes = [2, 3]

primes_set = set(primes)

fibonaccis = [1, 1, 2, 3]

while True:
  current = fibonaccis[-1] + fibonaccis[-2]
  fibonaccis.append(current)
#  if current < TEST_N:
#    continue
  while primes[-1] < current:
    candidate = primes[-1] + 2
    while True:
      #print 'testing candidate %d' % candidate
      is_prime = True
      for prime in primes:
        if prime*prime > candidate:
          break
        if candidate % prime == 0:
          #print 'failed against %d' % prime
          candidate = candidate + 2
          is_prime = False
          break
      if is_prime:
        # print 'new prime %d' % candidate
        primes.append(candidate)
        primes_set.add(candidate)
        break
  ndivs = 1
  cd = current
  if cd in primes_set:
    ndivs = ndivs * 2
  else:
    for p in primes:
      if p * p > cd:
        break
      np = 0
      while cd % p == 0:
        cd = cd / p
        np = np + 1
      if np > 0:
        ndivs = ndivs * (np + 1)
        if cd in primes_set:
          ndivs = ndivs * 2
          break
  print 'Current fib: %d  --  %d' % (current, ndivs)
