TEST_N = 227000

primes = [2, 3]

fibonaccis = [1, 1, 2, 3]

while True:
  current = fibonaccis[-1] + fibonaccis[-2]
  fibonaccis.append(current)
  if current < TEST_N:
    continue
  print 'Current fib: %d' % current
  while primes[-1] < current:
    candidate = primes[-1] + 2
    while True:
      #print 'testing candidate %d' % candidate
      is_prime = True
      for prime in primes:
        if prime*prime > candidate:
          break
        if candidate % prime == 0:
          #print 'failed against %d' % prime
          candidate = candidate + 2
          is_prime = False
          break
      if is_prime:
        print 'new prime %d' % candidate
        primes.append(candidate)
        break
  if primes[-1] == current:
    print 'Found: %d' % current
    break
