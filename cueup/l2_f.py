fibonaccis = [1, 1, 2, 3]

ndivisors = {}
max_n = 1


def mcd(n1, n2):
  if n2 > n1:
    a = n2
    n2 = n1
    n1 = a
  while True:
    d = n1 / n2
    r = n1 - n2 * d
    if r == 0:
      return n2
    n1 = n2
    n2 = d
  

while True:
  current = fibonaccis[-1] + fibonaccis[-2]
  fibonaccis.append(current)
  while max_n < current:
    num_divisors = 2
    n = 2
    while n * n <= max_n:
      if max_n % n == 0:
        num_divisors = ndivisors[n]
    for n in xrange(2, current / 2 + 2):
    if current % n == 0:
      number_of_divisors += 1
  print 'Current fib: %d  --  %d' % (current, number_of_divisors)
  ndivisors[current] = number_of_divisors
  if number_of_divisors > 1000:
    break
