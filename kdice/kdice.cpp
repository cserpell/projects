#include <set>
#include <iostream>
#include <fstream>

using namespace std;

class node {
  public:
    set<node *> neighbors; // Lista de vecinos del nodo
    int owner;      // Jugador que es dueño del terreno
    int dices;
    node(int ow, int di) {
        owner = ow;
        dices = di;
    }
    node * copy() {
        node * n = new node(this.owner, this.dices);
        // COPIAR VECINOS, PERO VECINOS NUEVOS???
    }
};

// QUE LATA, HAY QUE HACER MUCHO MAS

class table {
  public:
    set<node *> all; // Todas las casillas del tablero
    int players; // Cuantos jugadores en total hay
    int *state; // El estado de cada jugar (se fue o no)
    int current; // A quien le toca jugar
    table(int pla, int *sta, int cur) {
        players = pla;
        state = sta;
        cur = current;
    }
    set<movement *> * generate_movements() {
        set<movement *> * res = new set<movement *>();
        for(set<node *>::iterator it = all.begin(); it != all.end(); it++) {
            node * n1 = *it;
            if(n1->dices == 1)
                continue;
            for(set<node *>::iterator it2 = neighbors.begin(); it2 != neighbors.end(); it2++) {
                node * n2 = *it2;
                if(n2->dices == 1)
                    continue;
                if(n1->owner != n2->owner) {
                    movement * nueva = new movement(n1, n2, this);
                }
            }
        }
    }
    table * copy() {
        table * t = new table(players, state, current);
        for(set<node *>::iterator it = all.begin(); it != all.end(); it++)
            t->all.insert((*it)->copy());
    }
};

class movement {
  public:
    node *from;
    node *to;
    table * posible1; // Los distintos resultados posibles de la movida
    table * posible2; // Los distintos resultados posibles de la movida
    double probability1; // La probabilidad que ocurra cada uno de esos resultados
    double probability2; // La probabilidad que ocurra cada uno de esos resultados
    // La idea es que sea lo mas general posible, como si fuera cualquier juego
    movement(node * f, node * t, table * orig) {
        from = f;
        to = t;
        posible1 = orig->copy();
        // SI LAS COSAS SON TRATADAS COMO ARREGLOS Y NO SETS, SE PUEDE ACCEDER A LA POSICION
        // QUE NO CAMBIARIA EN UNA COPIA
    }
};

int NTERRAINS = 50;
int NPLAYERS = 7;

node node_array[NTERRAINS]; // Arreglo de nodos que representan el tablero
set<node *> terrain_owned[NPLAYERS]; // Los terrenos de cada jugador

int main(int argc, char *argv[]) {
    if(argc < 2) {
        cout << "Uso: " << argv[0] << " distribucion" << endl;
        return 0;
    }
    ifstream distrib;
    distrib.open(argv[1]);
    if(!distrib) {
        cout << "Error al abrir el archivo de distribucion " << argv[1] << endl;
        return 0;
    }
    int n_terrenos;
    distrib >> n_terrenos;
    if(n_terrenos > NTERRAINS) {
        cout << "Numero de terrenos invalido o mayor al aceptado" << endl;
        return 0;
    }
    int n_enlaces;
    distrib >> n_enlaces;
    for(int i = 0; i < n_enlaces; i++) {
        int a, b;
        distrib >> a;
        distrib >> b;
        if(a >= n_terrenos || b >= n_terrenos || a < 0 || b < 0) {
            cout << "Terreno a enlazar invalido en enlace numero " << (i + 1) << endl;
            return 0;
        }
        if(node_array[a].neighbors.find(&(node_array[b])) == node_array[a].neighbors.end())
            node_array[a].neighbors.insert(&(node_array[b]));
        if(node_array[b].neighbors.find(&(node_array[a])) == node_array[b].neighbors.end())
            node_array[b].neighbors.insert(&(node_array[a]));
    }
}

