//---------------------------------------------------------------------------
#ifndef tableroH
#define tableroH
//---------------------------------------------------------------------------
#include <vcl.h>
#define MAX (32)
//---------------------------------------------------------------------------
class Tablero
{
 private:
  int tamx;
  int tamy;
  int Estado[MAX][MAX];
  bool Descubrir[MAX][MAX];
 public:
  bool Ganado(void);
  void Marcar(int x, int y);
  int Abrir(int x, int y);
  int MostrarLugar(int x, int y);
  AnsiString CalcFila(int y);
  AnsiString CalcColumna(int x);
  void Limpiar(void);
  bool Llenar(int numpuz);
  void LlenarAzar(int mx, int my, int bombas);
  void AbrirTodo(void);
  int TX(void);
  int TY(void);
  Tablero()
  {
    tamx = 0;
    tamy = 0;
  }
};
#endif
 