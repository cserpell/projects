// Esta es la unit del tablero
//---------------------------------------------------------------------------
#include <vcl.h>
#include <fstream.h>
#pragma hdrstop

#include "tablero.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
// Revisa si el tablero ha sido abierto en todas las posiciones
bool Tablero::Ganado(void)
{
  if(!tamy) return false;
  if(!tamx) return false;

 /* Despu�s tengo que cambiar esto por un n�mero binario que se va restando al
    abrir las casillas y cuando llega a cero termin�.*/
  for(int y = 0; y < tamy; y++)
    for(int x = 0; x < tamx; x++)
      if(Descubrir[x][y]&&(Estado[x][y] != 1))
        return false;

  return true;
}
//---------------------------------------------------------------------------
void Tablero::Marcar(int x, int y)
{
  if(x < 0) return;
  if(x >= tamx) return;
  if(y < 0) return;
  if(y >= tamy) return;

  if(Estado[x][y] == 1)
    return;
  if(Estado[x][y] == 0)
  {
    Estado[x][y] = 2;
    return;
  }
  Estado[x][y] = 0;
}
//---------------------------------------------------------------------------
/* Retorna: 0 si no hizo nada
            1 si lo abri� y no termin�
            2 si lo abri� y termin�
            3 si se equivoc� */
int Tablero::Abrir(int x, int y)
{
  if(x < 0) return 0;
  if(x >= tamx) return 0;
  if(y < 0) return 0;
  if(y >= tamy) return 0;

  if(Estado[x][y] == 1)
    return 0;
  if(!Descubrir[x][y])
  {
    Estado[x][y] = 2;
    return 3;
  }
  Estado[x][y] = 1;
  if(Ganado())
    return 2;
  return 1;
}
//---------------------------------------------------------------------------
/* Retorna: 0 = cerrado
            1 = abierto
            2 = cerrado con marca
            3 = error */
int Tablero::MostrarLugar(int x, int y)
{
  if(x < 0) return 3;
  if(x >= tamx) return 3;
  if(y < 0) return 3;
  if(y >= tamy) return 3;

  return Estado[x][y];
}
//---------------------------------------------------------------------------
// Retorna un string con los n�meros del lado de la fila y
AnsiString Tablero::CalcFila(int y)
{
  if(y < 0) return "";
  if(y >= tamy) return "";

  AnsiString Retorno;
  int ultimo = 0;
  bool ant = false;

  for(int x = 0; x < tamx; x++)
  {
    if(Descubrir[x][y])
    {
      ultimo++;
      ant = true;
    } else if(ant)
    {
      Retorno += IntToStr(ultimo) + '\n';
      ultimo = 0;
      ant = false;
    }
  }

  if(ant)
  {
    Retorno += IntToStr(ultimo) + '\n';
    ant = false;
  }/* else if((Retorno.Length() > 1)&&(Retorno[Retorno.Length()] == '\n'))
    Retorno.SetLength(Retorno.Length() - 1);*/

  if(!Retorno.Length())
    Retorno = "0\n";

  return Retorno;
}
//---------------------------------------------------------------------------
// Retorna un string con los n�meros del tope de la columna y, separados por \n
AnsiString Tablero::CalcColumna(int x)
{
  if(x < 0) return "";
  if(x >= tamx) return "";

  AnsiString Retorno;
  int ultimo = 0;
  bool ant = false;

  for(int y = 0; y < tamy; y++)
  {
    if(Descubrir[x][y])
    {
      ultimo++;
      ant = true;
    } else if(ant)
    {
      Retorno += IntToStr(ultimo) + '\n';
      ultimo = 0;
      ant = false;
    }
  }

  if(ant)
  {
    Retorno += IntToStr(ultimo) + '\n';
    ant = false;
  }/* else if((Retorno.Length() > 1)&&(Retorno[Retorno.Length()] == '\n'))
    Retorno.SetLength(Retorno.Length() - 1);*/

  if(!Retorno.Length())
    Retorno = "0\n";

  return Retorno;
}
//---------------------------------------------------------------------------
void Tablero::Limpiar(void)
{
  for(int y = 0; y < tamy; y++)
    for(int x = 0; x < tamx; x++)
      Estado[x][y] = 0;
}
//---------------------------------------------------------------------------
bool Tablero::Llenar(int numpuz)
{
  /* Aqu� va la incre�ble rutina que lee un archivo y carga el puzle numpuz */
  ifstream Archivo;
  string str;
  int ly;
  int lx;

  if(numpuz < 0) return false;

  Archivo.open("TAB.DAT");
  if(!Archivo)
    return false;
  try
  {
    Archivo >> str;
    if(numpuz >= StrToInt(str.data() ))
    {
      Archivo.close();
      return false;
    }
    for(int i = 0; i < numpuz; i++)
    {
      Archivo >> str;
      ly = StrToInt(str.data());
      Archivo >> str;
      for(int y = 0; y < ly; y++)
        Archivo >> str;
    }
    Archivo >> str;
    ly = StrToInt(str.data());
    Archivo >> str;
    lx = StrToInt(str.data());
    for(int y = 0; y < ly; y++)
    {
      Archivo >> str;
      for(int x = 0; x < lx; x++)
        Descubrir[x][y] = str[x] - '0';
    }
  } catch(...)
  {
    Archivo.close();
    return false;
  }
  tamx = lx;
  tamy = ly;
  Limpiar();
  return true;
}
//---------------------------------------------------------------------------
void Tablero::LlenarAzar(int mx, int my, int bombas)
{
  // Genera un tablero de "mx" * "my" con n "bombas"
  if(mx > MAX || my > MAX || mx <= 1 || my <= 1)
    return;

  if(bombas > mx*mx || bombas < 1)
    return;

  for(int y = 0; y < MAX; y++)
    for(int x = 0; x < MAX; x++)
    {
      Descubrir[x][y] = 0;
      Estado[x][y] = 0;
    }
 bombas = mx*my - bombas;
 for(int i = 0; i < bombas; i++)
 {
   int x,y;

   do
   {
     x = int(rand() / (1.0 + RAND_MAX) * mx);
     y = int(rand() / (1.0 + RAND_MAX) * my);
   } while(Descubrir[x][y]);
   Descubrir[x][y] = 1;
 }

 tamx = mx;
 tamy = my;
}
//---------------------------------------------------------------------------
int Tablero::TX(void)
{
  return tamx;
}
//---------------------------------------------------------------------------
int Tablero::TY(void)
{
  return tamy;
}
//---------------------------------------------------------------------------
void Tablero::AbrirTodo(void)
{
  for(int y = 0; y < tamy; y++)
    for(int x = 0; x < tamx; x++)
      Abrir(x, y);
}
//---------------------------------------------------------------------------

