//---------------------------------------------------------------------------
#ifndef juegoH
#define juegoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "tablero.h"
#include <ExtCtrls.hpp>
#include "CSPIN.h"
/*#define CABx (96)
#define CABy (96)*/
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *Nuevo;
        TEdit *EP;
        TPaintBox *PaintBox;
        TButton *btnAzar;
        TLabel *Label1;
        TLabel *Label2;
        TCSpinEdit *csTamanox;
        TCSpinEdit *csBombas;
        TLabel *lblTiempo;
        TTimer *Timer1;
        TPanel *Panel1;
        TGroupBox *GroupBox1;
        TGroupBox *GroupBox2;
        TGroupBox *GroupBox3;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *lblErrores;
        TCSpinEdit *csTamanoy;
        TLabel *Label5;
        void __fastcall NuevoClick(TObject *Sender);
        void __fastcall PaintBoxPaint(TObject *Sender);
        void __fastcall PaintBoxMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall PaintBoxMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall PaintBoxMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall csTamanoxChange(TObject *Sender);
        void __fastcall btnAzarClick(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
private:	// User declarations
        bool atdo;
        int ax;
        int ay;
        int cbx;
        int cby;
        int cmx;
        int cmy;
        int CABx;
        int CABy;
        int Tiempo;
        int Errores;
        bool Jugando;
        Tablero Tab;
        Graphics::TBitmap *bmp1;
        Graphics::TBitmap *bmp2;
        Graphics::TBitmap *bmp3;
        Graphics::TBitmap *pant;
        void Escalar(void);
        void DibujarTodo(void);
        void DibujarLugar(int lugx, int lugy);
        void LlenarErrores(void);
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
