//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "juego.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
  bmp1 = new Graphics::TBitmap;
  bmp2 = new Graphics::TBitmap;
  bmp3 = new Graphics::TBitmap;
  pant = new Graphics::TBitmap;

  bmp1->LoadFromFile("B1.BMP");
  bmp2->LoadFromFile("B2.BMP");
  bmp3->LoadFromFile("B3.BMP");
  cbx = bmp1->Width;
  cby = bmp1->Height;
  Jugando = false;
  Tiempo = 0;
  Errores = 0;
  if((cbx != bmp2->Width)||(cbx != bmp3->Width))
  {
    Application->MessageBoxA("Los bmps est�n mal hechos.", "Error", 0);
    Close();
  }
  if((cby != bmp2->Height)||(cby != bmp3->Height))
  {
    Application->MessageBoxA("Los bmps est�n mal hechos.", "Error", 0);
    Close();
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::NuevoClick(TObject *Sender)
{
  if(!Tab.Llenar(StrToInt(EP->Text)))
  {
    Application->MessageBoxA(
      "El archivo con los tableros est� da�ado. Modificar el archivo 'TAB.DAT'."
                                                                  , "Error", 0);
    return;
  }
  Tiempo = 0;
  Errores = 0;
  LlenarErrores();
  Jugando = true;
  Escalar();
}
//---------------------------------------------------------------------------
void TForm1::Escalar(void)
{
  int tx, ty, ml, x, y;

  tx = Tab.TX();
  if(!tx) return;
  ty = Tab.TY();
  if(!ty) return;

  // Arreglo la fuente para los n�meros
  pant->Canvas->Font->Name = "Arial";
  pant->Canvas->Font->Size = cby*3/4 - 2;
  pant->Canvas->Pen->Style = psSolid;
  pant->Canvas->Pen->Width = 1;
  pant->Canvas->Pen->Color = clBlack;
  cmx = cbx*5/4;
  cmy = pant->Canvas->TextHeight("0123456789");

  // Ve los valores de CABx y CABy
  for(ml = 0, y = 0; y < ty; y++)
  {
    AnsiString b = Tab.CalcFila(y);
    int l = 0;

    for(int i = 1; i <= b.Length(); i++)
      if(b[i] == '\n')
        l++;
    if(l > ml)
    {
      CABx = l*cmx;
      ml = l;
    }
  }
  for(ml = 0, x = 0; x < tx; x++)
  {
    AnsiString b = Tab.CalcColumna(x);
    int l = 1;

    for(int i = 1; i <= b.Length(); i++)
      if(b[i] == '\n')
        l++;
    if(l > ml)
    {
      CABy = l*cmy;
      ml = l;
    }
  }
  CABx -= 5;
  CABy -= 5;
  tx = cbx*tx + CABx;
  ty = cby*ty + CABy;

  ClientWidth = tx + Panel1->Width;
  PaintBox->Width = tx;
  pant->Width = tx;
  if(ty > 270)
    ClientHeight = ty;
  else
    ClientHeight = 270;
  pant->Height = ty;
  pant->Canvas->Brush->Color = clBtnFace;
  pant->Canvas->FillRect(Rect(0, 0, tx, ty));
  DibujarTodo();
}
//---------------------------------------------------------------------------
void TForm1::DibujarTodo(void)
{
  int tx, ty, lx, ly, x, y;

  tx = Tab.TX();
  if(!tx) return;
  ty = Tab.TY();
  if(!ty) return;

  // Los encabezados verticales
  lx = CABx;
  for(x = 0; x < tx; x++)
  {
    int xc, yc, mx, my, i;
    AnsiString b = Tab.CalcColumna(x), lc = "";

    //La l�nea de separaci�n
    pant->Canvas->MoveTo(lx, 0);
    pant->Canvas->LineTo(lx, CABy);

    mx = lx;
    my = 0;
    xc = mx;
    yc = 0;
    for(i = 1; i <= b.Length(); i++)
    {
      if(b[i] == '\n')
      {
        pant->Canvas->TextOutA((3*mx - xc + cbx)/2, yc, lc);
        lc = "";
        xc = mx;
        yc += my + 2;
      } else
      {
        if(cmy > my)
          my = cmy;
        xc += pant->Canvas->TextWidth(b[i]);
        lc += b[i];
      }
    }
    lx += cbx;
  }
  for(ly = CABy, y = 0; y < ty; y++, ly += cby)
  {
    // El encabezado horizontal
    int xc, mx, my, i;
    AnsiString b = Tab.CalcFila(y), lc = "";

    mx = 0;
    my = (cby - cmy)/2 + ly;
    xc = 0;
    for(i = 1; i <= b.Length(); i++)
    {
      if(b[i] == '\n')
      {
        pant->Canvas->TextOutA((3*mx - xc + cbx)/2, my, lc);
        lc = "";
        xc += cmx;
        mx += cmx;
      } else
      {
        xc += pant->Canvas->TextWidth(b[i]);
        lc += b[i];
      }
    }

    // L�neas de separaci�n
    pant->Canvas->MoveTo(0, ly);
    pant->Canvas->LineTo(CABx, ly);
    for(lx = CABx, x = 0; x < tx; x++, lx+= cbx)
    {
      // Todos los cuadritos
      int i = Tab.MostrarLugar(x, y);

      if(!i)
        pant->Canvas->Draw(lx, ly, bmp1);
      else if(i == 1)
        pant->Canvas->Draw(lx, ly, bmp2);
      else if(i == 2)
        pant->Canvas->Draw(lx, ly, bmp3);
    }
  }
  // Por �ltimo, las l�neas de ayuda a cada 5
  pant->Canvas->Pen->Color = clRed;
  for(x = 0; x < tx; x += 5)
  {
    pant->Canvas->MoveTo(x*cbx + CABx, 0);
    pant->Canvas->LineTo(x*cbx + CABx, ty*cby + CABy);
  }
  for(y = 0; y < ty; y += 5)
  {
    pant->Canvas->MoveTo(0, y*cby + CABy);
    pant->Canvas->LineTo(tx*cbx + CABx, y*cby + CABy);
  }

  PaintBoxPaint(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBoxPaint(TObject *Sender)
{
  PaintBox->Canvas->Draw(0, 0, pant);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBoxMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  int tx, ty, lx, ly;

  tx = Tab.TX();
  if(!tx) return;
  ty = Tab.TY();
  if(!ty) return;
  if(X < CABx) return;
  if(Y < CABy) return;
  lx = (X - CABx)/cbx;
  if(lx >= tx) return;
  ly = (Y - CABy)/cby;
  if(ly >= ty) return;

  if(Button == mbLeft)
  {
    // Apreta el bot�n...
    pant->Canvas->Draw(lx * cbx + CABx, ly * cby + CABy, bmp2);
    ax = lx;
    ay = ly;
    atdo = true;
  } else
  {
    Tab.Marcar(lx, ly);
    DibujarLugar(lx, ly);
    atdo = false;
  }
  PaintBoxPaint(this);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBoxMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(Button == mbLeft)
  {
    int tx, ty, lx, ly, i;

    tx = Tab.TX();
    if(!tx) return;
    ty = Tab.TY();
    if(!ty) return;
    if(X < CABx) return;
    if(Y < CABy) return;
    lx = (X - CABx)/cbx;
    if(lx >= tx) return;
    ly = (Y - CABy)/cby;
    if(ly >= ty) return;

    i = Tab.Abrir(lx, ly);
    if(i == 2)
    {
      Jugando = false;
      Application->MessageBoxA("Has terminado el tablero... �FELICITACIONES!",
                                                                  "Ganaste", 0);
    }
    else if(i == 3)
    {
      Errores++;
      LlenarErrores();
      Application->MessageBoxA("�Te equivocaste!", "Cuidado", 0);
      if(Errores == 5)
      {
        Tab.AbrirTodo();
        DibujarTodo();
        Jugando = false;
      }
    }

    DibujarLugar(lx, ly);
    atdo = false;
    PaintBoxPaint(this);
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PaintBoxMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
  if(Shift.Contains(ssRight))
  {
    int tx, ty, lx, ly;

    tx = Tab.TX();
    if(!tx) return;
    ty = Tab.TY();
    if(!ty) return;
    if(X < CABx) return;
    if(Y < CABy) return;
    lx = (X - CABx)/cbx;
    if(lx >= tx) return;
    ly = (Y - CABy)/cby;
    if(ly >= ty) return;

    if(Tab.MostrarLugar(lx, ly) == 0)
    {
      Tab.Marcar(lx, ly);
      DibujarLugar(lx, ly);
      PaintBoxPaint(this);
    }
    atdo = false;
  } else if(Shift.Contains(ssLeft))
  {
    // Est� el bot�n apretado
    int tx, ty, lx, ly;

    tx = Tab.TX();
    if(!tx) return;
    ty = Tab.TY();
    if(!ty) return;

    lx = (X - CABx)/cbx;
    ly = (Y - CABy)/cby;

    if((ax != lx || ay != ly || X < CABx || Y < CABy) && atdo)
      DibujarLugar(ax, ay);
    atdo = false;
    PaintBoxPaint(this);
    if(X < CABx) return;
    if(Y < CABy) return;
    if(lx >= tx) return;
    if(ly >= ty) return;

    pant->Canvas->Draw(lx * cbx + CABx, ly * cby + CABy, bmp2);
    ax = lx;
    ay = ly;
    atdo = true;
    PaintBoxPaint(this);
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::csTamanoxChange(TObject *Sender)
{
   try
   {
      csBombas->MaxValue = csTamanox->Value * csTamanoy->Value;
   } catch (...)
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnAzarClick(TObject *Sender)
{
  Tab.LlenarAzar(csTamanox->Value, csTamanoy->Value, csBombas->Value);
  Tiempo = 0;
  Timer1Timer(Sender);
  Errores = 0;
  LlenarErrores();
  Jugando = true;
  Escalar();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
  if(Jugando)
  {
    lblTiempo->Caption = IntToStr(Tiempo);
    while(lblTiempo->Caption.Length() < 4)
      lblTiempo->Caption = "0" + lblTiempo->Caption;
    Tiempo++;
  }
}
//---------------------------------------------------------------------------
void TForm1::DibujarLugar(int lugx, int lugy)
{
  int tx, ty, i;

  tx = Tab.TX();
  if(!tx) return;
  ty = Tab.TY();
  if(!ty) return;
  if(lugx < 0) return;
  if(lugy < 0) return;
  if(lugx > tx) return;
  if(lugy > ty) return;

  i = Tab.MostrarLugar(lugx, lugy);
  if(!i)
    pant->Canvas->Draw(lugx*cbx + CABx, lugy*cby + CABy, bmp1);
  else if(i == 1)
    pant->Canvas->Draw(lugx*cbx + CABx, lugy*cby + CABy, bmp2);
  else if(i == 2)
    pant->Canvas->Draw(lugx*cbx + CABx, lugy*cby + CABy, bmp3);

  if(!(lugx % 5))
  {
//    pant->Canvas->Pen->Color = clRed;
    pant->Canvas->MoveTo(lugx*cbx + CABx, lugy*cby + CABy);
    pant->Canvas->LineTo(lugx*cbx + CABx, lugy*cby + CABy + cby);
  }
  if(!(lugy % 5))
  {
  //  pant->Canvas->Pen->Color = clRed;
    pant->Canvas->MoveTo(lugx*cbx + CABx, lugy*cby + CABy);
    pant->Canvas->LineTo(lugx*cbx + CABx + cbx, lugy*cby + CABy);
  }
}
//---------------------------------------------------------------------------
void TForm1::LlenarErrores(void)
{
  lblErrores->Caption = IntToStr(Errores);
  while(lblErrores->Caption.Length() < 4)
    lblErrores->Caption = "0" + lblErrores->Caption;
}
//---------------------------------------------------------------------------

