//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Solver.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
int Solver::AyudaCant(int color, Tablero O)
{
  if(color != 1 && color != 2)
    return false;

  Tablero A;
  int c = 0, may = -1, pm = -1;

  A.CopiarA(O);
  while(c < TAMTAB*TAMTAB)
  {
    if(A.Jugar(c%TAMTAB, (int)(c/TAMTAB), color))
    {
      int aux = 0;

      for(int i = 0; i < TAMTAB; i++)
        for(int j = 0; j < TAMTAB; j++)
        {
          if(A.Lugar[i][j] == color)
            aux++;
          A.Lugar[i][j] = O.Lugar[i][j];
        }
      if(aux > may)
      {
        pm = c;
        may = aux;
      }
    }
    c++;
  }
  return pm;
  // Retorna -1 si no hay jugada posible
}
int Solver::AyudaCantN(int color, Tablero O)
{
  if(color != 1 && color != 2)
    return false;

  Tablero A;
  int c = 0, may = -1, pm = -1;

  A.CopiarA(O);
  while(c < TAMTAB*TAMTAB)
  {
    if(A.Jugar(c%TAMTAB, (int)(c/TAMTAB), color))
    {
      int oc = (color & 1) + 1;
      int cc = AyudaCant(oc, A);
      int aux = 0;

      A.Jugar(cc%TAMTAB, (int)(cc/TAMTAB), oc);
      for(int i = 0; i < TAMTAB; i++)
        for(int j = 0; j < TAMTAB; j++)
        {
          if(A.Lugar[i][j] == color)
            aux++;
          A.Lugar[i][j] = O.Lugar[i][j];
        }
      if(aux > may)
      {
        pm = c;
        may = aux;
      }
    }
    c++;
  }
  return pm;
  // Retorna -1 si no hay jugada posible
}
int Solver::AyudaCantN2(int color, Tablero O)
{
  if(color != 1 && color != 2)
    return false;

  Tablero A;
  int c = 0, may = -1, pm = -1;

  A.CopiarA(O);
  while(c < TAMTAB*TAMTAB)
  {
    if(A.Jugar(c%TAMTAB, (int)(c/TAMTAB), color))
    {
      int oc = (color & 1) + 1;
      int cc = AyudaCantN(oc, A);
      int aux = 0;

      A.Jugar(cc%TAMTAB, (int)(cc/TAMTAB), oc);
      for(int i = 0; i < TAMTAB; i++)
        for(int j = 0; j < TAMTAB; j++)
        {
          if(A.Lugar[i][j] == color)
            aux++;
          A.Lugar[i][j] = O.Lugar[i][j];
        }
      if(aux > may)
      {
        pm = c;
        may = aux;
      }
    }
    c++;
  }
  return pm;
  // Retorna -1 si no hay jugada posible
}
int Solver::AyudaUlt(int colorajugar, int paso, Tablero O)
{
  if(paso >= 6)
    return O.Contar(1) + O.Contar(2)*TAMTAB*TAMTAB;
  int may = -1, cc = -1, auxpos = -1, caj = (colorajugar & 1) + 1;
  Tablero A;

  A.CopiarA(O);
  for(int x = 0; x < TAMTAB; x++)
    for(int y = 0; y < TAMTAB; y++)
    {
      if(A.JugarRAP(x, y, colorajugar, caj))
      {
        int p = AyudaUlt(caj, paso + 1, A);
        int c = SacarColor(colorajugar, p);

        if(c > may)
        {
          may = c;
          cc = p;
          auxpos = y*TAMTAB + x;
        }
        A.CopiarA(O);
      }
    }
  if(!paso)
    return (auxpos == -1)?O.Contar(1) + O.Contar(2)*TAMTAB*TAMTAB:auxpos;
  return (cc == -1)?O.Contar(1) + O.Contar(2)*TAMTAB*TAMTAB:cc;
}
int Solver::AyudaMar(int color, Tablero O)
{
  int max = -1, mp = -1, caj = (color & 1) + 1;
  Tablero A;

  A.CopiarA(O);
  for(int x = 0; x < TAMTAB; x++)
    for(int y = 0; y < TAMTAB; y++)
    {
      int n = 0;

      if(A.JugarRAP(x, y, color, caj))
      {
        for(int x = 0; x < TAMTAB; x++)
          for(int y = 0; y < TAMTAB; y++)
          {
            if(A.Lugar[x][y] == color)
            {
              n++;
              if(x == 0)
            }
          }
      }
}
int Solver::AyudaJug(int colorajugar, int coloracontar, int pos, Tablero O)
{
  if(pos < 6)  // Profundidad maxima
  {
  // Aqu� hay que hacer algo recursivo...
  int jugadashechas = 0;
  int max = -1, caj = (colorajugar & 1) + 1, npos = pos + 1;
  Tablero A;

  // Lleno el tablero Auxiliar
  A.CopiarA(O);
  A.mmax = O.mmax;
  for(int x = 0; x < TAMTAB; x++)
    for(int y = 0; y < TAMTAB; y++)
    {
      if(A.JugarRAP(x, y, colorajugar, caj))
      {
        int n;

        jugadashechas++;
        n = AyudaJug(caj, caj, npos, A);
        if(n > O.mmax)
        {
          ArregloJugadas[pos] = y*TAMTAB + x;
          O.mmax = n;
          max = n;
        }
        // Lleno el tablero Auxiliar
        A.CopiarA(O);
        A.mmax = O.mmax;
      }
    }
  if(jugadashechas != 0)
    return max;
  } // Se sale si la profundidad es m�xima
  // No se pudo jugar, caso base de la recursividad
  int cuenta = 0;

  // Cuento el tablero
  for(int i = 0; i < TAMTAB; i++)
    for(int j = 0; j < TAMTAB; j++)
      if(O.Lugar[i][j] == coloracontar)
        cuenta++;
  return cuenta;
}
int Solver::AyudaJug2(int color, int profactual, int proffinal, Tablero O)
{
  if(profactual == proffinal)
    return O.Contar(1) + O.Contar(2)*TAMTAB*TAMTAB;

  Tablero A;
  bool jugada = false;
  int max = 0;

  A.CopiarA(O);
  for(int x = 0; x < TAMTAB; x++)
    for(int y = 0; y < TAMTAB; y++)
      if(A.Jugar(x, y, color))
      {
        bool enc = false;

        jugada = true;
/*        for(int i = 0; i < NTabs; i++)
          if(A.IgualA(ArregloTABS[i]))
          {
            enc = true;
            break;
          }      */
        if(!enc)
        {
/*          ArregloTABS[NTabs].CopiarA(A);
//          NUMTABS[NTabs] = A.ContarColor(1) + O.ContarColor(2)*TAMTAB*TAMTAB;
          NTabs++;*/

          int d = AyudaJug2((color&1)+1,profactual+1, proffinal, A);

          if(SacarColor(color, d) > max)
          {
            max = d;
            NUMTABS[profactual] = y*TAMTAB + x;
          }
        }
        A.CopiarA(O);
      }
  if(!jugada)
    return O.Contar(1) + O.Contar(2)*TAMTAB*TAMTAB;
  return max;
}
int Solver::SacarColor(int color, int d)
{
  if(color == 1)
    return d%(TAMTAB*TAMTAB);
  return (int)(d/(TAMTAB*TAMTAB));
}
int Solver::AyudaO(int color)
{
  NTabs = 0;
  AyudaJug2(color, 0, 7, ESTE());
  return NUMTABS[0];
}
Tablero Solver::ESTE()
{
  Tablero A;

  for(int i = 0; i < TAMTAB; i++)
    for(int j = 0; j < TAMTAB; j++)
      A.Lugar[i][j] = Lugar[i][j];
  A.mmax = mmax;
  return A;
}
// Para llamar a AyudaJug:
int Solver::Ayuda(int color)
{
  mmax = 0;
  for(int i = 0; i < TAMTAB*TAMTAB; i++)
    ArregloJugadas[i] = -1;
  AyudaJug(color, color, 0, ESTE());
  return ArregloJugadas[0];
}
int Solver::AyudaHum(int color, Tablero O)
{
  // Pruebo todas las jugadas posibles
  // y veo en cu�l le doy menos posibilidades al otro
  Tablero A;
  int caj = (color&1) + 1;
  int min = 64;
  int pm = -1;

  A.CopiarA(O);
        int na = A.Contar(caj);
  for(int x = 0; x < TAMTAB; x++)
    for(int y = 0; y < TAMTAB; y++)
      if(A.Jugar(x, y, color))
      {
        int c = AyudaCant(caj, A);
        int np;

        A.Jugar(c%TAMTAB, (int)(c/TAMTAB), caj);
        np = A.Contar(caj);
        if(np - na < min)
        {
          min = np - na;
          pm = y*TAMTAB + x;
        }
        A.CopiarA(O);
      }
  return pm;
}
