//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Solver.h"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TMemo *Memo;
        TButton *Button1;
        TEdit *EX;
        TEdit *EY;
        TEdit *ECOLOR;
        TButton *Button2;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TButton *Button3;
        TButton *Hint;
        TLabel *POSHINT;
        TButton *Hint2;
        TButton *Hint3;
        TButton *Hint4;
        TButton *Hint5;
        TButton *Hint6;
        TButton *HacerJugar;
        TButton *Hint7;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall HintClick(TObject *Sender);
        void __fastcall Hint2Click(TObject *Sender);
        void __fastcall Hint3Click(TObject *Sender);
        void __fastcall Hint4Click(TObject *Sender);
        void __fastcall Hint5Click(TObject *Sender);
        void __fastcall Hint6Click(TObject *Sender);
        void __fastcall HacerJugarClick(TObject *Sender);
        void __fastcall Hint7Click(TObject *Sender);
private:	// User declarations
        Solver tab;
        void MostrarTodo(void);
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
