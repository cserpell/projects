//---------------------------------------------------------------------------
#ifndef SolverH
#define SolverH
#include "Tablero.h"
#define CT (TAMTAB*TAMTAB*TAMTAB)
//---------------------------------------------------------------------------
class Solver : public Tablero
{
 private:
  int ArregloJugadas[TAMTAB*TAMTAB];
  Tablero ArregloTABS[CT];
  int NUMTABS[TAMTAB];
  int NTabs;
 public:
  int SacarColor(int color, int d);
  Tablero ESTE();
  int AyudaCant(int color, Tablero O);
  int AyudaCantN(int color, Tablero O);
  int AyudaCantN2(int color, Tablero O);
  int AyudaJug(int colorajugar, int coloracontar, int pos, Tablero O);
  int Ayuda(int color);
  int AyudaJug2(int color, int profactual, int proffinal, Tablero O);
  int AyudaO(int color);
  int AyudaHum(int color, Tablero O);
  int AyudaUlt(int colorajugar, int paso, Tablero O);
};
#endif

