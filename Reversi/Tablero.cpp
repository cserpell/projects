//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Tablero.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
bool Tablero::Jugar(int x, int y, int color)
{
  int rx, ry, cmp;
  bool enc, td;

  if(x < 0 || x >= TAMTAB || y < 0 || y >= TAMTAB)
    return false;
  if(color != 1 && color != 2)
    return false;
  if(Lugar[x][y] != 0)
    return false;

  cmp = (color & 1) + 1;
  td = false;
  enc = false;   // <- arriba
  rx = x - 1;
  ry = y - 1;
  while(rx >= 0 && ry >= 0)
  {
    if(Lugar[rx][ry] != cmp)
    {
      if(enc && Lugar[rx][ry] == color)
      {
        while(rx < x)
          Lugar[++rx][++ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx--;
    ry--;
  }
  enc = false;       // arriba
  ry = y - 1;
  while(ry >= 0)
  {
    if(Lugar[x][ry] != cmp)
    {
      if(enc && Lugar[x][ry] == color)
      {
        while(ry < y)
          Lugar[x][++ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    ry--;
  }
  enc = false;        // -> arriba
  rx = x + 1;
  ry = y - 1;
  while(rx < TAMTAB && ry >= 0)
  {
    if(Lugar[rx][ry] != cmp)
    {
      if(enc && Lugar[rx][ry] == color)
      {
        while(ry < y)
          Lugar[--rx][++ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx++;
    ry--;
  }
  enc = false;          // ->
  rx = x + 1;
  while(rx < TAMTAB)
  {
    if(Lugar[rx][y] != cmp)
    {
      if(enc && Lugar[rx][y] == color)
      {
        while(rx > x)
          Lugar[--rx][y] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx++;
  }
  enc = false;          // -> abajo
  rx = x + 1;
  ry = y + 1;
  while(rx < TAMTAB && ry < TAMTAB)
  {
    if(Lugar[rx][ry] != cmp)
    {
      if(enc && Lugar[rx][ry] == color)
      {
        while(ry > y)
          Lugar[--rx][--ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx++;
    ry++;
  }
  enc = false;             // abajo
  ry = y + 1;
  while(ry < TAMTAB)
  {
    if(Lugar[x][ry] != cmp)
    {
      if(enc && Lugar[x][ry] == color)
      {
        while(ry > y)
          Lugar[x][--ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    ry++;
  }
  enc = false;            // <- abajo
  rx = x - 1;
  ry = y + 1;
  while(rx >= 0 && ry < TAMTAB)
  {
    if(Lugar[rx][ry] != cmp)
    {
      if(enc && Lugar[rx][ry] == color)
      {
        while(ry > y)
          Lugar[++rx][--ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx--;
    ry++;
  }
  enc = false;                // <-
  rx = x - 1;
  while(rx >= 0)
  {
    if(Lugar[rx][y] != cmp)
    {
      if(enc && Lugar[rx][y] == color)
      {
        while(rx < x)
          Lugar[++rx][y] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx--;
  }
  return td;
}
bool Tablero::JugarRAP(int x, int y, int color, int cmp)
{
  int rx, ry;
  bool enc, td;

  if(Lugar[x][y] != 0)
    return false;
  td = false;
  enc = false;   // <- arriba
  rx = x - 1;
  ry = y - 1;
  while(rx >= 0 && ry >= 0)
  {
    if(Lugar[rx][ry] != cmp)
    {
      if(enc && Lugar[rx][ry] == color)
      {
        while(rx < x)
          Lugar[++rx][++ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx--;
    ry--;
  }
  enc = false;       // arriba
  ry = y - 1;
  while(ry >= 0)
  {
    if(Lugar[x][ry] != cmp)
    {
      if(enc && Lugar[x][ry] == color)
      {
        while(ry < y)
          Lugar[x][++ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    ry--;
  }
  enc = false;        // -> arriba
  rx = x + 1;
  ry = y - 1;
  while(rx < TAMTAB && ry >= 0)
  {
    if(Lugar[rx][ry] != cmp)
    {
      if(enc && Lugar[rx][ry] == color)
      {
        while(ry < y)
          Lugar[--rx][++ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx++;
    ry--;
  }
  enc = false;          // ->
  rx = x + 1;
  while(rx < TAMTAB)
  {
    if(Lugar[rx][y] != cmp)
    {
      if(enc && Lugar[rx][y] == color)
      {
        while(rx > x)
          Lugar[--rx][y] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx++;
  }
  enc = false;          // -> abajo
  rx = x + 1;
  ry = y + 1;
  while(rx < TAMTAB && ry < TAMTAB)
  {
    if(Lugar[rx][ry] != cmp)
    {
      if(enc && Lugar[rx][ry] == color)
      {
        while(ry > y)
          Lugar[--rx][--ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx++;
    ry++;
  }
  enc = false;             // abajo
  ry = y + 1;
  while(ry < TAMTAB)
  {
    if(Lugar[x][ry] != cmp)
    {
      if(enc && Lugar[x][ry] == color)
      {
        while(ry > y)
          Lugar[x][--ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    ry++;
  }
  enc = false;            // <- abajo
  rx = x - 1;
  ry = y + 1;
  while(rx >= 0 && ry < TAMTAB)
  {
    if(Lugar[rx][ry] != cmp)
    {
      if(enc && Lugar[rx][ry] == color)
      {
        while(ry > y)
          Lugar[++rx][--ry] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx--;
    ry++;
  }
  enc = false;                // <-
  rx = x - 1;
  while(rx >= 0)
  {
    if(Lugar[rx][y] != cmp)
    {
      if(enc && Lugar[rx][y] == color)
      {
        while(rx < x)
          Lugar[++rx][y] = color;
        td = true;
      }
      break;
    }
    enc = true;
    rx--;
  }
  return td;
}
void Tablero::Inicializar(void)
{
  int ix, iy;

  for(int x = 0; x < TAMTAB; x++)
    for(int y = 0; y < TAMTAB; y++)
      Lugar[x][y] = 0;
  if(TAMTAB < 2)
    return;
  ix = (int)(TAMTAB/2) - 1;
  iy = (int)(TAMTAB/2) - 1;
  Lugar[ix][iy] = 1;
  Lugar[ix + 1][iy + 1] = 1;
  Lugar[ix + 1][iy] = 2;
  Lugar[ix][iy + 1] = 2;
}
int Tablero::RetornarLugar(int x, int y)
{
  if(x < 0 || x >= TAMTAB || y < 0 || y >= TAMTAB)
    return 0;
  return Lugar[x][y];
}
bool Tablero::IgualA(Tablero X)
{
  for(int i = 0; i < TAMTAB; i++)
    for(int j = 0; j < TAMTAB; j++)
      if(Lugar[i][j] != X.Lugar[i][j])
        return false;
  return true;
}
void Tablero::CopiarA(Tablero X)
{
  for(int i = 0; i < TAMTAB; i++)
    for(int j = 0; j < TAMTAB; j++)
      Lugar[i][j] = X.Lugar[i][j];
}
int Tablero::Contar(int color)
{
  int n = 0;

  for(int i = 0; i < TAMTAB; i++)
    for(int j = 0; j < TAMTAB; j++)
      if(Lugar[i][j] == color)
        n++;
  return n;
}
