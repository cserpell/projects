//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("Reversi.res");
USEFORM("main.cpp", Form1);
USEUNIT("Tablero.cpp");
USEUNIT("Solver.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TForm1), &Form1);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
