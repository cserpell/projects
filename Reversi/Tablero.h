//---------------------------------------------------------------------------
#ifndef TableroH
#define TableroH
//---------------------------------------------------------------------------
#define TAMTAB (8)
class Tablero
{
 public:
  int Lugar[TAMTAB][TAMTAB];
  int mmax;
  bool Jugar(int x, int y, int color); // Color puede ser 1 o 2
  bool JugarRAP(int x, int y, int color, int cmp); // Color puede ser 1 o 2
  void Inicializar(void);
  Tablero()
  {
    Inicializar();
  }
  int RetornarLugar(int x, int y);
  bool IgualA(Tablero X);
  void CopiarA(Tablero X);
  int Contar(int color);
};
#endif
