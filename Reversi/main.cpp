// Mi objetivo es encontrar una estrategia ganadora del reversi
// que me permita ganarle al computador en el otro juego
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  tab.Jugar(StrToInt(EX->Text), StrToInt(EY->Text), StrToInt(ECOLOR->Text));
  while(Memo->Lines->Count > 0)
    Memo->Lines->Delete(0);
  MostrarTodo();
}
//---------------------------------------------------------------------------
void TForm1::MostrarTodo(void)
{
  for(int y = 0; y < TAMTAB; y++)
  {
    AnsiString S = "";

    for(int x = 0; x < TAMTAB; x++)
      S += IntToStr(tab.RetornarLugar(x, y)) + "  ";
    Memo->Lines->Append(S);
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  while(Memo->Lines->Count > 0)
    Memo->Lines->Delete(0);
  MostrarTodo();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
  while(Memo->Lines->Count > 0)
    Memo->Lines->Delete(0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::HintClick(TObject *Sender)
{
  int p = tab.AyudaCant(StrToInt(ECOLOR->Text), tab);

  POSHINT->Caption = "(" + IntToStr(p%TAMTAB) + "," + IntToStr((int)(p/TAMTAB))
                                                                          + ")";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Hint2Click(TObject *Sender)
{
  int p = tab.Ayuda(StrToInt(ECOLOR->Text));

  POSHINT->Caption = "(" + IntToStr(p%TAMTAB) + "," + IntToStr((int)(p/TAMTAB))
                                                                          + ")";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Hint3Click(TObject *Sender)
{
  int p = tab.AyudaCantN(StrToInt(ECOLOR->Text), tab);

  POSHINT->Caption = "(" + IntToStr(p%TAMTAB) + "," + IntToStr((int)(p/TAMTAB))
                                                                          + ")";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Hint4Click(TObject *Sender)
{
  int p = tab.AyudaCantN2(StrToInt(ECOLOR->Text), tab);

  POSHINT->Caption = "(" + IntToStr(p%TAMTAB) + "," + IntToStr((int)(p/TAMTAB))
                                                                          + ")";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Hint5Click(TObject *Sender)
{
  int p = tab.AyudaO(StrToInt(ECOLOR->Text));

  POSHINT->Caption = "(" + IntToStr(p%TAMTAB) + "," + IntToStr((int)(p/TAMTAB))
                                                                          + ")";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Hint6Click(TObject *Sender)
{
  int p = tab.AyudaHum(StrToInt(ECOLOR->Text), tab);

  POSHINT->Caption = "(" + IntToStr(p%TAMTAB) + "," + IntToStr((int)(p/TAMTAB))
                                                                          + ")";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Hint7Click(TObject *Sender)
{
  int p = tab.AyudaUlt(StrToInt(ECOLOR->Text), 0, tab);

  POSHINT->Caption = "(" + IntToStr(p%TAMTAB) + "," + IntToStr((int)(p/TAMTAB))
                                                                          + ")";
}                            
//---------------------------------------------------------------------------
void __fastcall TForm1::HacerJugarClick(TObject *Sender)
{
  while(Memo->Lines->Count > 0)
    Memo->Lines->Delete(0);
  int jugando = 1;
  int p;

  while(((jugando == 1)?
         p = tab.AyudaUlt(jugando, 0, tab)
        :p = tab.AyudaUlt(jugando, 0, tab))
         != -1 && tab.Jugar(p%TAMTAB, (int)(p/TAMTAB), jugando))
  {
    jugando = (jugando & 1) + 1;
    Memo->Lines->Append("Buscando ayuda para jugador " + IntToStr(jugando));
  }
  int n1 = tab.Contar(1);
  int n2 = tab.Contar(2);
  if(n1 > n2)
    Memo->Lines->Append("Gan� el que parti� (n�mero 1).");
  else if(n2 > n1)
    Memo->Lines->Append("Gan� el que jug� segundo (n�mero 2).");
  else
    Memo->Lines->Append("Empate!");
}
//---------------------------------------------------------------------------

