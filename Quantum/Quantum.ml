(* Cristián Serpell 2004 *)

let cplxsum (ax,ay) (bx,by) =
    (ax +. bx , ay +. by);;

let cplxprod (ax,ay) (bx,by) =
    (ax *. bx -. ay *. by , ay *. bx +. ax *. by);;
    
let cplxprod2 q i (bx,by) =
	cplxprod q.(i) (bx,by);;

let cplxnorm (ax,ay) =
    sqrt(ax *. ax +. ay *. ay);;

let pointprod p q =
    let c = Array.mapi (cplxprod2 q) p in
    Array.fold_left cplxsum (0.,0.) c;;

let matprod p m =
	Array.map (pointprod p) m;;


(*let leer_linea ci =
    let str = input_line ci in
    Str.split (Str.regexp "[ ]+") str;;*)

let read3 l p = 
    ((float_of_string (List.nth l p)),
     (float_of_string (List.nth l (p + 1))),
     (float_of_string (List.nth l (p + 2))))
;;
(*
let rec llenar (listam, listap) n cin =
    let lm = leer_linea cin in
    let lp = leer_linea cin in
    let mat = (read3 lm 0, read3 lm 3, read3 lm 6) in
    let pnt = read3 lp 0 in
    if n > 0 then
        llenar (mat :: listam, pnt :: listap) (n - 1) cin
    else
        (mat :: listam, pnt :: listap)
;;

let rec iter n (px, py, pz) =
    let cond = dibujapunto (px,py,pz) in
    if (n < maxiter) && (not cond) then
        let producto = List.map (matprod (px,py,pz)) matrixlist in
        let nuevos = List.map2 (suma3) producto pointlist in
        List.iter (iter (n + 1)) nuevos;
;;
*)
