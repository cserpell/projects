//---------------------------------------------------------------------------
#ifndef PaintBox1H
#define PaintBox1H
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Controls.hpp>
#include <Classes.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
#define MAXTAM     (256)
#define MAXPEDAZOS (65536)
class Pieza
{
 public:
  int x[MAXPEDAZOS];
  int y[MAXPEDAZOS];
  int numpedazos;
  void AgregarPedazo(int xr, int yr);
};
class PACKAGE TTablero : public TPaintBox
{
 private:
  Pieza P[MAXPEDAZOS];
  int ocup[MAXTAM][MAXTAM];                // [x][y]
  int tamactx;
  int tamacty;
  int numpiezas;
 public:
  void MoverAbajo(int posx, int posy);
  void MoverDerecha(int posx, int posy);
  void MoverArriba(int posx, int posy);
  void MoverIzquierda(int posx, int posy);
  void MoverAbajo(int pieza);
  void MoverDerecha(int pieza);
  void MoverArriba(int pieza);
  void MoverIzquierda(int pieza);
  void DefinirTam(int nuevo);
  void DefinirTam(int nx, int ny);
  int AgregarPieza();
  bool AgregarPedazodePieza(int numpieza, int posx, int posy); // numpieza
  bool AgregarPedazodeBloque(int posx, int posy);              // -2
  __fastcall TTablero(TComponent* Owner);                      // -1 todo
__published:
};
//---------------------------------------------------------------------------
#endif
