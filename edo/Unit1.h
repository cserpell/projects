//---------------------------------------------------------------------------
#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TTimer *TimerO;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TMemo *Memo1;
        TMemo *Memo2;
        TMemo *Memo3;
        TMemo *Memo4;
        TMemo *Memo5;
        TMemo *Memo6;
        TMemo *Memo7;
        TMemo *Memo8;
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
        TButton *Button4;
        TButton *Button5;
        TButton *Button6;
        TButton *Button7;
        TButton *Button8;
        TButton *Inicio;
        TEdit *Edit1;
        TLabel *Label6;
        TEdit *Edit2;
        TEdit *Edit3;
        TLabel *Label7;
        void __fastcall TimerOTimer(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall InicioClick(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall Button6Click(TObject *Sender);
        void __fastcall Button7Click(TObject *Sender);
        void __fastcall Button8Click(TObject *Sender);
private:	// User declarations
        AnsiString STR(void);
        AnsiString STR1(void);
        AnsiString STR2(void);
        void Llenar1(int num);
        void Llenar2(int num);
        int NumSeg;
        int NumMin;
        int NumHor;
        int NumSegL1;
        int NumMinL1;
        int NumHorL1;
        int NumSegL2;
        int NumMinL2;
        int NumHorL2;
        int ACTIVO1;
        int ACTIVO2;
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
