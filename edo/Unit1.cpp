//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
  NumSeg = 0;
  NumMin = 0;
  NumHor = 0;
  NumSegL1 = 0;
  NumMinL1 = 0;
  NumHorL1 = 0;
  NumSegL2 = 0;
  NumMinL2 = 0;
  NumHorL2 = 0;
  ACTIVO1 = 0;
  ACTIVO2 = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TimerOTimer(TObject *Sender)
{
  NumSeg++;
  if(NumSeg == 60)
  {
    NumMin++;
    NumSeg = 0;
    if(NumMin == 60)
    {
      NumHor++;
      NumMin = 0;
      Label3->Caption = IntToStr(NumHor);
    }
    Label2->Caption = IntToStr(NumMin);
  }
  Label1->Caption = IntToStr(NumSeg);
}
//---------------------------------------------------------------------------
AnsiString TForm1::STR()
{
  AnsiString ret = "";

  if(NumHor < 10)
    ret += "0";
  ret += IntToStr(NumHor) + ":";
  if(NumMin < 10)
    ret += "0";
  ret += IntToStr(NumMin) + ":";
  if(NumSeg < 10)
    ret += "0";
  ret += IntToStr(NumSeg);
  return ret;
}
//---------------------------------------------------------------------------
AnsiString TForm1::STR1()
{
  AnsiString ret = "";

  if(NumHorL1 < 10)
    ret += "0";
  ret += IntToStr(NumHorL1) + ":";
  if(NumMinL1 < 10)
    ret += "0";
  ret += IntToStr(NumMinL1) + ":";
  if(NumSegL1 < 10)
    ret += "0";
  ret += IntToStr(NumSegL1) + " => ";
  ret += STR() + " -d- ";
  ret += IntToStr((NumHor - NumHorL1)*3600 + (NumMin - NumMinL1)*60 + NumSeg -
                                                                      NumSegL1);
  return ret;
}
//---------------------------------------------------------------------------
AnsiString TForm1::STR2()
{
  AnsiString ret = "";

  if(NumHorL2 < 10)
    ret += "0";
  ret += IntToStr(NumHorL2) + ":";
  if(NumMinL2 < 10)
    ret += "0";
  ret += IntToStr(NumMinL2) + ":";
  if(NumSegL2 < 10)
    ret += "0";
  ret += IntToStr(NumSegL2) + " => ";
  ret += STR() + " -d- ";
  ret += IntToStr((NumHor - NumHorL2)*3600 + (NumMin - NumMinL2)*60 + NumSeg -
                                                                      NumSegL2);
  return ret;
}
//---------------------------------------------------------------------------
void TForm1::Llenar1(int num)
{
  if(ACTIVO1 != num)
  {
  switch(ACTIVO1)
  {
   case 1:
    Memo1->Lines->Append(STR1());
    break;
   case 2:
    Memo2->Lines->Append(STR1());
    break;
   case 3:
    Memo3->Lines->Append(STR1());
    break;
   case 4:
    Memo4->Lines->Append(STR1());
    break;
  }
  ACTIVO1 = num;
  NumSegL1 = NumSeg;
  NumMinL1 = NumMin;
  NumHorL1 = NumHor;
  }
}
//---------------------------------------------------------------------------
void TForm1::Llenar2(int num)
{
  if(ACTIVO2 != num)
  {
  switch(ACTIVO2)
  {
   case 5:
    Memo5->Lines->Append(STR2());
    break;
   case 6:
    Memo6->Lines->Append(STR2());
    break;
   case 7:
    Memo7->Lines->Append(STR2());
    break;
   case 8:
    Memo8->Lines->Append(STR2());
    break;
  }
  ACTIVO2 = num;
  NumSegL2 = NumSeg;
  NumMinL2 = NumMin;
  NumHorL2 = NumHor;
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::InicioClick(TObject *Sender)
{
  TimerO->Interval = 1000/StrToInt(Edit3->Text);
  ACTIVO1 = StrToInt(Edit1->Text);
  ACTIVO2 = StrToInt(Edit2->Text);
  NumSeg = 0;
  NumMin = 0;
  NumHor = 0;
  NumSegL1 = 0;
  NumMinL1 = 0;
  NumHorL1 = 0;
  NumSegL2 = 0;
  NumMinL2 = 0;
  NumHorL2 = 0;
  Label3->Caption = IntToStr(NumHor);
  Label2->Caption = IntToStr(NumMin);
  Label1->Caption = IntToStr(NumSeg);
  TimerO->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  Llenar1(1);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  Llenar1(2);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
  Llenar1(3);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
  Llenar1(4);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
  Llenar2(5);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button6Click(TObject *Sender)
{
  Llenar2(6);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button7Click(TObject *Sender)
{
  Llenar2(7);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button8Click(TObject *Sender)
{
  Llenar2(8);
}
//---------------------------------------------------------------------------

