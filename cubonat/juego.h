//---------------------------------------------------------------------------
#ifndef juegoH
#define juegoH
//---------------------------------------------------------------------------
/*class ficha
{
 private:
  int posx;
  int posy;
  int posz;
 public:
  ficha(int x, int y, int z)
  {
    posx = x;
    posy = y;
    posz = z;
  }
};*/
class juego
{
 private:
  int rotacionunion[26]; // Representa el estado de rotacion de la union
  int tipoficha[27];  /* 0 = union recta
                         1 = union codo
                         2 = orilla
                         3 = la otra orilla */
  int tipofichafinal[27]; /* De donde puede ser la ficha
                             1 = medio
                             2 = eaquina
                             4 = arista
                             8 = medio lado */
  int fichaenpos[27];      /* 0  1  2    9 10 11   18 19 20   x ->
                             3  4  5   12 13 14   21 22 23   y \/
                             6  7  8   15 16 17   24 25 26   z (�) */
  int tipopos[27];  // tipo que debe ir en cada posicion
 public:
  juego()
  {
    tipopos = {2,4,2,4,8,4,2,4,2,4,8,4,8,1,8,4,8,4,2,4,2,4,8,4,2,4,2};
/* 4 aristas = {1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25};
   2 esquinas = {0, 2, 6, 8, 18, 20, 24, 26};
   8 mediolados = {4, 10, 12, 14, 16, 22};
   1 medio = 13;*/
  }
  void LlenarJuego();
  bool ResolverJuego(int pospartida);
  bool RevisarFichaPos(int numficha, int pos);
  bool recurs(int num, int posact, int dir);
};
#endif
