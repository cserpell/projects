//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "juego.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
void juego::LlenarJuego()
{
  rotacionunion = {0,0,2,0,0,2,0,0,2,0,2,0,0,0,2,0,2,0,0,2,0,0,0,2,0,0};
  tipoficha = {2,0,1,1,1,0,1,1,0,1,1,1,0,1,0,1,1,1,1,0,1,0,1,0,1,0,3};
  // Ahora hay que llenar lo de donde son
  for(int i = 0; i < 27; i++)
  {
    tipofichafinal[i] = 15;
  }
  // Lo primero es que solo puede ser impar el medio
  for(int i = 0; i < 27; i++}
  {
    tipofichafinal[i]--;    // no es del medio del cubo
    tipofichafinal[i] -= 4; // no es de las aristas
    i++;
    tipofichafinal[i] -= 2; // no es esquina
    tipofichafinal[i] -= 8; // no es del medio del lado
  }
  // Adem�s el medio no puede ser en las orillas de los "grupos de tres"
  // y los medios de esos grupos no pueden ser esquinas
  for(int i = 0; i < 27; i++)
  {
    if(tipoficha[i] = 0)
    {
      tipofichafinal[i] -= tipofichafinal[i] & 2;
      if(i > 0)
        tipofichafinal[i - 1] -= tipofichafinal[i - 1] & 1;
      if(i < 26)
        tipofichafinal[i + 1] -= tipofichafinal[i + 1] & 1;
    }
  }
}

bool juego::ResolverJuego(int pospartida)
{
  if(pospartida < 0 || pospartida > 26)
    return false;
  for(int i = 0; i < 27; i++)
    fichaenpos[i] = -1;
  fichaenpos[pospartida] = 0;
  RevisarFichaPos(0, pospartida);
}

int nuevapos(int posact, int dir)
{
  if(dir == 1)
  {
    if(posact < 18)
      return posact + 9;
    return -1;
  }
  if(dir == 2)
  {
    if(posact > 8)
      return posact - 9;
    return -1;
  }
  if(dir == 3)
  {
    if(posact % 9 < 6)
      return posact + 3;
    return -1;
  }
  if(dir == 4)
  {
    if(posact % 9 > 2)
      return posact - 3;
    return -1;
  }
  if(dir == 5)
  {
    if(posact % 3 < 2)
      return posact + 1;
    return -1;
  }
  if(posact % 3 > 0)
    return posact - 1;
  return -1;
}

bool juego::recurs(int num, int posact, int dir)
{
  // dir: 1 +z, 2 -z, 3 +y, 4 -y, 5 +x, 6 -x
  if(!RevisarFichaPos(num, posact))  // Si una de ese tipo puede ir ah�
    return false;
  if(fichaenpos[posact] != -1) // Hay una ficha donde debe estar esta!
    return false;
  fichaenpos[posact] = num;  // La pone en el arreglo
  if(num == 27)
    return true;  // LO HIZO!!!
  if(tipoficha[num] == 0)  // Recta, no hay pa que probar posibilidades
  {
    int nposact = nuevapos(posact, dir);

    if(nposact == -1)
    {
      fichaenpos[posact] = -1;
      return false;
    }
    if(!recurs(num + 1, nposact, dir))
    {
      fichaenpos[posact] = -1;
      return false;
    }
    return true;
  }

  for(int i = 0; i < 4; i++)
  {
    // Pruebo cada rotaci�n posible
    int ndir = i;
    int nposact

    rotacionunion[num - 1] = i;
    // ****************************************
    // ESTA ES LA PARTE QUE HAY QUE HACER AHORA


    nposact = nuevapos(posact, ndir);
    if(nposact == -1)
      return false;
    if(recurs(num + 1, nposact, ndir))
      return true;
  }
  return false;
}

bool juego::RevisarFichaPos(int numficha, int pos)
{
  if(!tipofichafinal[numficha]&tipopos[pos])
    return false;
  return true;
  // NOTA: esto solo dice si PUEDE ser, no es seguro
}

