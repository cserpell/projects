//---------------------------------------------------------------------------
#ifndef Unit2H
#define Unit2H
#define MAXTAM (8)
//---------------------------------------------------------------------------
// Los __int64 es porque antes ten�a MAXTAM como 64, pero creo que
// 64x64 es mucha memoria
class Ficha
{
  __int64 ocupacion[MAXTAM];
  int posx, posy, posactual;
  __int64 movidas[MAXTAM*MAXTAM][MAXTAM*MAXTAM][MAXTAM];
  int cuantasposibles[MAXTAM*MAXTAM];
  int nposx[MAXTAM*MAXTAM];
  int nposy[MAXTAM*MAXTAM];
  bool Mover(int nummovida);
  void LlenarMovidas(__int64 posibles[MAXTAM*MAXTAM][MAXTAM]);
};
class Tablero
{
 private:
  int tamx, tamy;
  int numfichas;
  Ficha Fichas[MAXTAM*MAXTAM];
  __int64 comp[MAXTAM];
  __int64 ocupado[MAXTAM];
  bool EliminarFicha(int cual);
 public:
  int FichaEn(int x, int y);
  bool MoverFicha(int cual, int nummovida);
  bool AgregarFicha(Ficha Cual);
  bool DefinirTamano(int nuevo);
  bool DefinirTamano(int nuevox, int nuevoy);
  void BorrarTablero(void);
  Tablero()
  {
    for(int i = 0; i < MAXTAM; i++)
    {
      ocupado[i] = 0;
      if(i != 0)
        comp[i] = comp[i - 1] << 1;
      else
        comp[0] = 1;
    }
    tamx = 0;
    tamy = 0;
    numfichas = 0;
  }
};
#endif
