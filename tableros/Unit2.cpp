//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit2.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
// Retorna si la ficha se pudo mover
bool Ficha::Mover(int nummovida)
{
  if(nummovida < 0 || nummovida >= cuantasposibles[posactual])
    return false;
  for(int i = 0; i < MAXTAM; i++)
    ocupacion[i] = movidas[posactual][nummovida][i];
  posx = nposx[nummovida];
  posy = nposy[nummovida];
  posactual = posx + MAXTAM*posy;
  return true;
}
// Llena las movidas posibles desde todas las posiciones de acuerdo al patr�n
void LlenarMovidas(__int64 posibles[MAXTAM*MAXTAM][MAXTAM])
{
}
// Retorna si pudo mover la ficha cual
bool Tablero::MoverFicha(int cual, int nummovida)
{
  if(cual < 0 || cual >= numfichas)
    return false;
  if(nummovida < 0 || nummovida >= MAXTAM)
    return false;
  for(int i = 0; i < MAXTAM; i++)
    if((ocupado[i] & Fichas[cual].movidas[Fichas[cual].posactual][nummovida][i]) != 0)
      return false;
  return Fichas[cual].Mover(nummovida);
}
// Elimina la ficha de las posicion cual
bool Tablero::EliminarFicha(int cual)
{
  if(cual < 0 || cual >= numfichas)
    return false;
  for(int i = 0; i < tamy; i++)
    ocupado[i] -= Fichas[cual].ocupacion[i];
  return true;
}
// Retorna qu� ficha est� en la posici�n dada
int Tablero::FichaEn(int x, int y)
{
  if((ocupado[y] & comp[x]) == 0)
    return -1;
  for(int i = 0; i < numfichas; i++)
    if((Fichas[i].ocupacion[y] & comp[x]) != 0)
      return i;
  return -1;
}
// Agrega la ficha al tablero
bool Tablero::AgregarFicha(Ficha Cual)
{
  if(numfichas == MAXTAM*MAXTAM)
    return false;
  for(int i = 0; i < tamy; i++)
    if((Cual.ocupacion[i] & ocupado[i]) != 0)
      return false;
  for(int i = 0; i < tamy; i++)
    ocupado[i] += Cual.ocupacion[i];
  Fichas[numfichas] = Cual;
  numfichas++;
  return true;
}
// Define el tama�o del tablero cuadrado
bool Tablero::DefinirTamano(int nuevo)
{
  if(nuevo < 0 || nuevo >= MAXTAM)
    return false;
  tamx = nuevo;
  tamy = nuevo;
}
// Define el tama�o del tablero rectangular
bool Tablero::DefinirTamano(int nuevox, int nuevoy)
{
  if(nuevox < 0 || nuevox >= MAXTAM || nuevoy < 0 || nuevoy >= MAXTAM)
    return false;
  tamx = nuevox;
  tamy = nuevoy;
}
// Borra el contenido del tablero
void Tablero::BorrarTablero(void)
{
  for(int i = 0; i < MAXTAM; i++)
    ocupado[i] = 0;
  numfichas = 0;
}