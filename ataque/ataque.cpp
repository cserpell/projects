#include <iostream>
#include <cstdlib>

void recur(double ***tabla, int pos, int ataque, int hasta, int *llenado, int *aux) {
    if(pos == hasta) {
        int mpos = ataque;
        int perdidos = 0;
        if(hasta - ataque < mpos)
            mpos = hasta - ataque;
        for(int k = 0; k < hasta; k++)
            aux[k] = 0;
        for(int k = 0; k < mpos; k++) {
            int mat = -1;
            int mde = -1;
            int cmat = -1;
            int cmde = -1;
            for(int i = 0; i < ataque; i++)
                if(!aux[i] && llenado[i] > mat) {
                    mat = llenado[i];
                    cmat = i;
                }
            for(int i = ataque; i < hasta; i++)
                if(!aux[i] && llenado[i] > mde) {
                    mde = llenado[i];
                    cmde = i;
                }
            aux[cmat] = 1;
            aux[cmde] = 1;
            if(mat <= mde)
                perdidos++;
        }
        tabla[ataque - 1][hasta - ataque - 1][perdidos]++;
        return;
    }
    for(int i = 1; i <= 6; i++) {
        llenado[pos] = i;
        recur(tabla, pos + 1, ataque, hasta, llenado, aux);
    }
}

double ***generar_tabla(int maxataque, int maxdefensa) {
    if(maxataque <= 0 || maxdefensa <= 0)
        return 0;
    int minimo = maxataque;
    if(maxdefensa < minimo)
        minimo = maxdefensa;
    double ***p = new double**[maxataque];
    for(int i = 0; i < maxataque; i++) {
        p[i] = new double *[maxdefensa];
        for(int j = 0; j < maxdefensa; j++) {
            p[i][j] = new double[minimo + 1];
            for(int k = 0; k <= minimo; k++)
                p[i][j][k] = 0;
        }
    }
    int *llenado = new int[maxataque + maxdefensa];
    int *aux = new int[maxataque + maxdefensa];
    for(int i = 1; i <= maxataque; i++)
        for(int j = 1; j <= maxdefensa; j++)
            recur(p, 0, i, i + j, llenado, aux);
    for(int i = 0; i < maxataque; i++)
        for(int j = 0; j < maxdefensa; j++) {
            double suma = 0;
            for(int k = 0; k <= minimo; k++)
                suma += p[i][j][k];
            for(int k = 0; k <= minimo; k++) {
                p[i][j][k] = p[i][j][k]/suma;
                std::cout << "p[" << i << "][" << j << "][" << k << "] = " << p[i][j][k] << std::endl;
            }
        }
    delete llenado;
    delete aux;
    return p;
}

void calcular(double **f, double ***p, int ataque, int defensa, int maxataque, int maxdefensa) {
    int mat = maxataque;
    if(ataque < mat)
        mat = ataque;
    int mde = maxdefensa;
    if(defensa < mde)
        mde = defensa;
    f[ataque][defensa] = 0;
    int mpos = mat;
    if(mde < mpos)
        mpos = mde;
    for(int i = 0; i <= mpos; i++)
        f[ataque][defensa] += p[mat - 1][mde - 1][i]*f[ataque - i][defensa + i - mpos];
}

void inicializar(int MAXCALCULO, int MAXATAQUE, int MAXDEFENSA) {
    double **f = new double *[MAXCALCULO];
    for(int i = 0; i < MAXCALCULO; i++)
        f[i] = new double[MAXCALCULO];
    double ***p = generar_tabla(MAXATAQUE, MAXDEFENSA);
    for(int i = 1; i < MAXCALCULO; i++) {
        f[i][0] = 0;
        f[0][i] = 1;
    }
    f[0][0] = 0;
    for(int i = 1; i < MAXCALCULO; i++)
        for(int j = 1; j < MAXCALCULO; j++)
            calcular(f, p, i, j, MAXATAQUE, MAXDEFENSA);
    for(int i = 0; i < MAXCALCULO; i++) {
        for(int j = 0; j < MAXCALCULO; j++)
            std::cout /*<< i << " , " << j << " : "*/ << (1 - f[i][j]) << " ";//std::endl;
        std::cout << std::endl;
    }
}

int main(int argc, char *argv[]) {
    if(argc < 4) {
        std::cout << "Uso: " << argv[0] << " maximocalculo maxataque maxdefensa" << std::endl;
        return 0;
    }
    int MAXCALCULO = atoi(argv[1]);
    int MAXATAQUE = atoi(argv[2]);
    int MAXDEFENSA = atoi(argv[3]);
    if(MAXCALCULO <= 0 || MAXATAQUE <= 0 || MAXDEFENSA <= 0) {
        std::cout << "Valores deben ser positivos" << std::endl;
        return 0;
    }
    inicializar(MAXCALCULO, MAXATAQUE, MAXDEFENSA);
    return 0;
}

