#include <string>

using namespace std;

class Nodo
{
  public:
	Nodo *sgte;
	void *val;
	Nodo(Nodo *s, void *v)
	{
		val = v;
		sgte = s;
	}
};

class Elemento
{
  private:
	Nodo *uHijo;
  public:
	Nodo *Hijos;
	string nombre;
	bool sexo;  // F = femenino
	string fechanac;
	string fechadef;
	Elemento *Padre;
	Elemento *Madre;
	Elemento(string nom, string fnac, bool sex)
	{
		nombre = "" + nom;
		fechanac = "" + fnac;
		fechadef = "";
		sexo = sex;
		Padre = NULL;
		Madre = NULL;
	}
	Elemento(string nom, string fnac, string fdef, bool sex)
	{
		nombre = "" + nom;
		fechanac = "" + fnac;
		fechadef = "" + fdef;
		sexo = sex;
		Padre = NULL;
		Madre = NULL;
	}
	static void AgregarHijo(Elemento *acual, Elemento *nuevo)
	{
		if(nuevo == NULL || acual == NULL)
			return;
		if(acual->sexo)
		{
			if(nuevo->Padre != NULL)
				BorrarContPadre(nuevo);
			nuevo->Padre = acual;
		} else
		{
			if(nuevo->Madre != NULL)
				BorrarContMadre(nuevo);
			nuevo->Madre = acual;
		}
		Nodo *nhijo = new Nodo(NULL, nuevo);

		if(acual->Hijos == NULL)
		{
			acual->Hijos = nhijo;
			acual->uHijo = acual->Hijos;
			return;
		}
		acual->uHijo->sgte = nhijo;
		acual->uHijo = nhijo;
	}
	static void BorrarContPadre(Elemento *cual)
	{
		if(cual->Padre != NULL)
		{
			Nodo *res;

			res = cual->Padre->Hijos;
    		if(res->val == cual)
    		{
    			if(cual->Padre->uHijo == cual->Padre->Hijos)
    			{
    				cual->Padre->Hijos = NULL;
    				cual->Padre->uHijo = NULL;
    			} else
    				cual->Padre->Hijos = cual->Padre->Hijos->sgte;
    		} else
	    		while(true)
	    		{
	    			if(res->sgte->val == cual)
	    			{
	    				if(cual->Padre->uHijo == res->sgte)
	    					cual->Padre->uHijo = res;
	    				res->sgte = res->sgte->sgte;
	    				break;
	    			}
    				res = res->sgte;
    			}
			cual->Padre = NULL;
		}
	}
	static void BorrarContMadre(Elemento *cual)
	{
		if(cual->Madre != NULL)
		{
			Nodo *res;

			res = cual->Madre->Hijos;
    		if(res->val == cual)
    		{
    			if(cual->Madre->uHijo == cual->Madre->Hijos)
    			{
    				cual->Madre->Hijos = NULL;
    				cual->Madre->uHijo = NULL;
    			} else
    				cual->Madre->Hijos = cual->Madre->Hijos->sgte;
    		} else
	    		while(true)
	    		{
	    			if(res->sgte->val == cual)
	    			{
	    				if(cual->Madre->uHijo == res->sgte)
	    					cual->Madre->uHijo = res;
	    				res->sgte = res->sgte->sgte;
	    				break;
	    			}
    				res = res->sgte;
    			}
			cual->Madre = NULL;
		}
	}
	static void BorrarContactos(Elemento *cual)
	{
		BorrarContPadre(cual);
		BorrarContMadre(cual);
		if(cual->Hijos != NULL)
		{
			Nodo *res;

			res = cual->Hijos;
			while(true)
			{
				if(cual->sexo)
					((Elemento *)res->val)->Padre = NULL;
				else
					((Elemento *)res->val)->Madre = NULL;
				if(res->sgte == NULL)
					break;
				res = res->sgte;
			}
		}
	}
	Elemento *HijoNumero(int n)
	{
		if(Hijos == NULL || n < 1)
			return NULL;

		Nodo *res;

		res = Hijos;
		for(int i = 1; i < n; i++)
		{
			if(res->sgte == NULL)
				return NULL;
			res = res->sgte;
		}
		return (Elemento *)res->val;
	}
};
