#include <gtkmm.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include <iostream>
#include "proggen.h"

using namespace std;

class Principal : public Gtk::Window
{
  public:
    Principal() : b_padre("Ir al padre"),
    				b_madre("Ir a la madre"),
					b_agregarlista("Agregar nuevo a la lista"),
					b_eliminarlista("Eliminar de la lista"),
					b_irlista("Ir a elemento seleccionado"),
					b_tomarpro("Elegirlo como progenitor"),
					b_salir("Salir"),
					b_nhijo("Ir al hijo número"),
					l_mostrar(""),
					l_1("Nombre nuevo elemento:"),
					l_2("Fecha nacimiento:"),
					l_3("Fecha defunción:"),
					l_4("Sexo:"),
					rb_masc("Masculino"),
					rb_feme("Femenino"),
					b_aux("Seleccionar")
/*					tv_lis("")
					tb_nom(""),
					tb_fnac(""),
					tb_fdef("")*/
    {
    	lista = NULL;
    	ulista = NULL;
    	sellista = NULL;
    	actual = NULL;
        set_border_width(10);
        b_padre.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_padre_clicked));
        b_madre.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_madre_clicked));
        b_agregarlista.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_agregarlista_clicked));
        b_eliminarlista.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_eliminarlista_clicked));
        b_irlista.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_irlista_clicked));
        b_tomarpro.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_tomarpro_clicked));
        b_salir.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_salir_clicked));
        b_nhijo.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_nhijo_clicked));
        b_aux.signal_clicked().connect(
		                     sigc::mem_fun(*this, &Principal::on_b_aux_clicked));
		tv_lis.set_editable(false);
//		rb_feme.set_group(rb_masc.get_group());
		llenarlista();
		actualizar();
		add(m_Box);
		m_Box.pack_start(l_mostrar);
        m_Box.pack_start(b_padre);
        m_Box.pack_start(b_madre);
        m_Box.pack_start(b_nhijo);
        m_Box.pack_start(tb_nume);
        m_Box.pack_start(b_tomarpro);
        m_Box.pack_start(l_1);
        m_Box.pack_start(tb_nom);
        m_Box.pack_start(l_2);
        m_Box.pack_start(tb_fnac);
        m_Box.pack_start(l_3);
        m_Box.pack_start(tb_fdef);
        m_Box.pack_start(l_4);
		m_Box.pack_start(rb_masc);
		m_Box.pack_start(rb_feme);
        m_Box.pack_start(b_agregarlista);
        m_Box.pack_start(b_eliminarlista);
        m_Box.pack_start(b_irlista);
        m_Box.pack_start(b_aux);
        m_Box.pack_start(tb_aux);
		m_Box.pack_start(tv_lis);
        m_Box.pack_start(b_salir);
		show_all_children();
    }
	~Principal()
	{
	}
  protected:
	Elemento *actual;
	Elemento *sellista;
	Nodo *lista;
	Nodo *ulista;
    Gtk::Button b_padre;
    Gtk::Button b_madre;
    Gtk::Button b_agregarlista;
    Gtk::Button b_eliminarlista;
    Gtk::Button b_irlista;
    Gtk::Button b_tomarpro;
    Gtk::Button b_salir;
    Gtk::Button b_nhijo;
    Gtk::Label l_mostrar;
    Gtk::Label l_1;
    Gtk::Label l_2;
    Gtk::Label l_3;
    Gtk::Label l_4;
    Gtk::RadioButton rb_masc;
    Gtk::RadioButton rb_feme;
	Gtk::VBox m_Box;  // Cambiar por Gtk::Table
    Gtk::Entry tb_nom;
    Gtk::Entry tb_fnac;
    Gtk::Entry tb_fdef;
    Gtk::Entry tb_nume;
    Gtk::Entry tb_sex;
    Gtk::Button b_aux;
    Gtk::Entry tb_aux;
    Gtk::TextView tv_lis;
    
    virtual void on_b_salir_clicked()
	{	// Opción salir
		Gtk::Main::quit();
	}
    virtual void on_b_padre_clicked()
	{	// Opción padre
		if(actual != NULL && actual->Padre != NULL)
		{
			actual = actual->Padre;
			actualizar();
        	llenarlista();
		}
	}
    virtual void on_b_madre_clicked()
	{	// Opción madre
		if(actual != NULL && actual->Madre != NULL)
		{
			actual = actual->Madre;
			actualizar();
        	llenarlista();
		}
	}
    virtual void on_b_nhijo_clicked()
	{	// Opción número hijo
		if(actual != NULL)
		{
			Elemento *hijosel;
			int n = atoi(tb_nume.get_text().c_str()); // Bueno

			hijosel = actual->HijoNumero(n);
			if(hijosel != NULL)
				actual = hijosel;
			actualizar();
        	llenarlista();
		}
	}
    virtual void on_b_eliminarlista_clicked()
	{	// Eliminar elemento de la lista
		if(sellista != NULL)
		{
    		Nodo *buf = lista;
    		
    		if(buf->val == sellista)
    		{
    			if(ulista == lista)
    			{
    				lista = NULL;
    				ulista = NULL;
    			} else
    				lista = lista->sgte;
    		} else
	    		while(true)
	    		{
	    			if(buf->sgte->val == sellista)
	    			{
	    				if(ulista == buf->sgte)
	    					ulista = buf;
	    				buf->sgte = buf->sgte->sgte;
	    				break;
	    			}
    				buf = buf->sgte;
    			}
    		if(actual == sellista)
    		{
    			if(lista == NULL)
    				actual = NULL;
    			else
	    			actual = (Elemento *)lista->val;
    		}
    		Elemento::BorrarContactos(sellista);
    		sellista = NULL;
   			actualizar();
			llenarlista();
		}
	}
    virtual void on_b_agregarlista_clicked()
	{	// Agregar elemento a la lista
		string a = tb_nom.get_text().data();  // Bueno
		string b = tb_fnac.get_text().data(); // Bueno
		string c = tb_fdef.get_text().data(); // Bueno
		Elemento *nuevo;
				
		if(c.length() == 0)
			nuevo = new Elemento(a, b, true);
		else
			nuevo = new Elemento(a, b, c, true);
		
		Nodo *nel = new Nodo(NULL, nuevo);

		if(lista == NULL)
			lista = nel;
		else
			ulista->sgte = nel;
		ulista = nel;
		llenarlista();
	}
    virtual void on_b_tomarpro_clicked()
	{	// Seleccionar padre o madre
		if(actual != NULL && sellista != NULL && actual != sellista)
		{
			Elemento::AgregarHijo(sellista, actual);
			actualizar();
		}
	}
    virtual void on_b_irlista_clicked()
    {
		if(sellista != NULL)
		{
	    	actual = sellista;
        	llenarlista();
	    	actualizar();
	    }
    }
    virtual void on_b_aux_clicked()
    {
		int n = atoi(tb_aux.get_text().c_str());
		Nodo *nel = lista;
		
		if(ulista != NULL)
		{
			for(int i = 1; i < n; i++)
			{
				if(nel->sgte != NULL)
					nel = nel->sgte;
				else
				{
					sellista = NULL;
					return;
				}
			}
			sellista = (Elemento *)nel->val;
			llenarlista();
		}
    }
  private:
	void actualizar()
	{
    	if(actual != NULL)
		{
			stringstream fff;

			fff << actual->nombre;
			fff << "\nFecha nac: ";
			fff << actual->fechanac;
			fff << " Fecha def: ";
			fff << actual->fechadef;
			fff << " Sexo: ";
			fff << (actual->sexo?"M":"F");
			if(actual->Padre != NULL)
				fff << "\nPadre: " << actual->Padre->nombre;
			if(actual->Madre != NULL)
				fff << "\nMadre: " << actual->Madre->nombre;
			if(actual->Hijos != NULL)
			{
				Nodo *res = actual->Hijos;
				int i = 1;
				
				while(true)
				{
					fff << "\nHijo Nº";
					fff << i;
					fff << ": " << ((Elemento *)res->val)->nombre;
					if(res->sgte == NULL)
						break;
					i++;
					res = res->sgte;
				}
			}
			l_mostrar.set_text(fff.str());
		} else
			l_mostrar.set_text("No se ha elegido ningún elemento");
	}
	void llenarlista()
	{
		stringstream l;
		if(ulista != NULL)
		{
			Nodo *rec = lista;
			int i = 1;

			while(true)
			{
				if((Elemento *)rec->val == actual)
					l << "ACTUAL ";
				if((Elemento *)rec->val == sellista)
					l << "SELECCIONADO ";
				l <<  i;
				l << ".- " << ((Elemento *)rec->val)->nombre;
				if(rec->sgte == NULL)
					break;
				l << "\n";
				i++;
				rec = rec->sgte;
			}
		} else
			l << "No hay elementos en la lista";
		tv_lis.get_buffer()->set_text(l.str());
	}
};

int main(int argc, char *argv[])
{
    Gtk::Main kit(argc, argv);

    Principal ventana;

    Gtk::Main::run(ventana);
    return 0;
}

