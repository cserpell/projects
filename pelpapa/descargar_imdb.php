<?php
require_once("database.php");

function obtenerArreglo($nuevapag, $texto, $opos, $errort, $lista, $codigo) {
    $director = array();
    $ndirector = 0;
    $dpos = strpos($nuevapag, $texto, $opos);
    $paso = 8;
    $pregunta = "</a>";
    if($lista) {
        $paso = 6;
        $pregunta = "<br/>";
    }
    if($dpos !== FALSE) {
        $dpos = strpos($nuevapag, "</h5>", $dpos);
        if($dpos === FALSE) {
            file_put_contents($codigo, $nuevapag);
            die("Error1 al obtener ".$errort." de la pelicula ".$codigo."\n");
        }
        $dpos += 7;
        while(TRUE) {
            $ch = substr($nuevapag, $dpos, 1);
            if(strcmp($ch, "a") != 0)
                break;
            $dpos = strpos($nuevapag, ">", $dpos + 19); // Busca el ">" del a href
            if($dpos === FALSE) {
                file_put_contents($codigo, $nuevapag);
                die("Error2 al obtener ".$errort." de la pelicula ".$codigo."\n");
            }
            $dpos++;
            $dfpos = strpos($nuevapag, "<", $dpos); // Busca el "<" del /a
            if($dfpos === FALSE) {
                file_put_contents($codigo, $nuevapag);
                die("Error3 al obtener ".$errort." de la pelicula ".$codigo."\n");
            }
            $dfpos--;
            $candidato = substr($nuevapag, $dpos, $dfpos - $dpos + 1);
            $estaba = FALSE;
            for($k = 0; $k < $ndirector; $k++) {
                if(strcmp(strtolower($candidato), strtolower($director[$k])) == 0) {
                    $estaba = TRUE;
                    break;
                }
            }
            if(!$estaba) {
                $director[$ndirector] = $candidato;
                $ndirector++;
            }
            $dpos = strpos($nuevapag, $pregunta, $dfpos);
            if($dpos === FALSE)
                break;
            $dpos += $paso;
        }
    }
    return $director;
}

for($i = 75389; $i <= 1167694; $i++) {
//for($i = 354899; $i <= 1167694; $i++) {
    $codigo = "tt";
    $base = 1000000;
    while($base > $i) {
        $codigo .= "0";
        $base = $base/10;
    }
    $codigo .= $i;
    $conn = openDatabaseConnection();
    $consulta = "SELECT MID FROM MOVIE WHERE IMDB = '".$codigo."'";
    $res = mysql_query($consulta, $conn);
    $mid = mysql_fetch_array($res);
    if($mid)
        continue;
    $nuevapag = @file_get_contents("http://www.imdb.com/title/".$codigo."/");
    if($nuevapag === FALSE) {
        die("Error, no existe pagina de la pelicula ".$codigo."\n");
    }
    $tpos = strpos($nuevapag, "<div id=\"tn15title\">");
    if($tpos === FALSE) {
        file_put_contents($codigo, $nuevapag);
        die("Error, no tiene titulo la pelicula ".$codigo."\n");
    }
    $tpos += 25;
    $fpos = strpos($nuevapag, "<", $tpos);
    if($fpos === FALSE) {
        file_put_contents($codigo, $nuevapag);
        die("Error al extraer titulo de la pelicula ".$codigo."\n");
    }
    $fpos -= 2;
    $titulo = substr($nuevapag, $tpos, $fpos - $tpos + 1);
    $ypos = $fpos + 34;
    $year = substr($nuevapag, $ypos, 4);
    $opos = strpos($nuevapag, "<h3>Overview</h3>", $ypos);
    if($opos === FALSE) {
        file_put_contents($codigo, $nuevapag);
        die("Error, no tiene overview la pelicula ".$codigo."\n");
    }
    $director = obtenerArreglo($nuevapag, "<h5>Director", $opos, "director", TRUE, $codigo);
    $escritor = obtenerArreglo($nuevapag, "<h5>Writer", $opos, "escritor", TRUE, $codigo);
    $genero = obtenerArreglo($nuevapag, "<h5>Genre", $opos, "genero", FALSE, $codigo);

    $tagline = "";
    $ppos = strpos($nuevapag, "<h5>Tagline", $opos);
    if($ppos !== FALSE) {
        $ppos += 18;
        $pfpos = strpos($nuevapag, "<", $ppos);
        if($pfpos === FALSE) {
            file_put_contents($codigo, $nuevapag);
            die("Error al obtener tag line de la pelicula ".$codigo."\n");
        }
        $pfpos -= 2;
        $tagline = substr($nuevapag, $ppos, $pfpos - $ppos + 1);
    }
    $outline = "";
    $ppos = strpos($nuevapag, "<h5>Plot Outline", $opos);
    if($ppos !== FALSE) {
        $ppos += 24;
        $pfpos = strpos($nuevapag, "<", $ppos);
        if($pfpos === FALSE) {
            file_put_contents($codigo, $nuevapag);
            die("Error al obtener plot outline de la pelicula ".$codigo."\n");
        }
        $pfpos -= 2;
        $outline = substr($nuevapag, $ppos, $pfpos - $ppos + 1);
    }
    $cpos = strpos($nuevapag, "<h3>Cast</h3>", $opos);
    $cast = array();
    $ncast = 0;
    if($cpos !== FALSE) {
        $cfpos = strpos($nuevapag, "</table>", $cpos);
        if($cfpos === FALSE) {
            file_put_contents($codigo, $nuevapag);
            die("Error al obtener cast de la pelicula ".$codigo."\n");
        }
        while(TRUE) {
            $cpos = strpos($nuevapag, "<a", $cpos + 1);
            if($cpos === FALSE || $cpos > $cfpos)
                break;
            $cpos = strpos($nuevapag, "<a", $cpos + 1);
            if($cpos === FALSE || $cpos > $cfpos) {
                file_put_contents($codigo, $nuevapag);
                die("Error al obtener cast de la pelicula ".$codigo."\n");
            }
            $cpos = strpos($nuevapag, ">", $cpos);
            if($cpos === FALSE || $cpos > $cfpos) {
                file_put_contents($codigo, $nuevapag);
                die("Error al obtener cast de la pelicula ".$codigo."\n");
            }
            $cpos++;
            $clpos = strpos($nuevapag, "<", $cpos);
            if($clpos === FALSE || $cpos > $cfpos) {
                file_put_contents($codigo, $nuevapag);
                die("Error al obtener cast de la pelicula ".$codigo."\n");
            }
            $clpos--;
            $candidato = substr($nuevapag, $cpos, $clpos - $cpos + 1);
            $estaba = FALSE;
            for($k = 0; $k < $ncast; $k++) {
                if(strcmp(strtolower($candidato), strtolower($cast[$k])) == 0) {
                    $estaba = TRUE;
                    break;
                }
            }
            if(!$estaba) {
                $cast[$ncast] = $candidato;
                $ncast++;
            }
            $cpos = strpos($nuevapag, "</tr", $cpos);
            if($cpos === FALSE || $cpos > $cfpos) {
                file_put_contents($codigo, $nuevapag);
                die("Error al obtener cast de la pelicula ".$codigo."\n");
            }
//            echo $cast[$ncast - 1]." *\n";
        }
    } else
        $cpos = $opos;
    $duracion = "";
    $ppos = strpos($nuevapag, "<h5>Runtime", $cpos);
    if($ppos !== FALSE) {
        $ppos += 18;
        $pfpos = strpos($nuevapag, "min", $ppos);
        $pmaxpos = strpos($nuevapag, "\n", $ppos);
        if($pfpos === FALSE) {
            file_put_contents($codigo, $nuevapag);
            die("Error al obtener duracion de la pelicula ".$codigo."\n");
        }
        if($pmaxpos !== FALSE && $pmaxpos < $pfpos) {
            file_put_contents($codigo, $nuevapag);
            die("Error al obtener duracion de la pelicula ".$codigo."\n");
        }        
        $pfpos -= 2;
        $nppos = strpos($nuevapag, ":", $ppos);
        if($nppos !== FALSE && $nppos < $pfpos) {
            $ppos = $nppos;
            $ppos++;
        }
        $duracion = substr($nuevapag, $ppos, $pfpos - $ppos + 1);
    }
    $pais = obtenerArreglo($nuevapag, "<h5>Countr", $cpos, "pais", FALSE, $codigo);
    $idioma = obtenerArreglo($nuevapag, "<h5>Language", $cpos, "idioma", FALSE, $codigo);

  /*  echo($titulo." (".$year.")\n");
    foreach($director as $j)
        echo $j."\n";
    echo "-----\n";
    foreach($escritor as $j)
        echo $j."\n";
    echo "-----\n";
    foreach($genero as $j)
        echo $j."\n";
    echo "-----\n";
    echo "TAG: ".$tagline."\n";
    echo "OUT: ".$outline."\n";
    echo $duracion."\n";
    echo "-----\n";
    foreach($cast as $j)
        echo $j."\n";
    echo "-----\n";
    foreach($pais as $j)
        echo $j."\n";
    echo "-----\n";
    foreach($idioma as $j)
        echo $j."\n";*/
    
    $consulta = "INSERT INTO MOVIE (TITLE, YEAR, TAGLINE, PLOTOUTLINE, RUNTIME, IMDB) VALUES ".
                "('".mysql_real_escape_string($titulo)."', ".
                "'".$year."', ".
                "'".mysql_real_escape_string($tagline)."', ".
                "'".mysql_real_escape_string($outline)."', ".
                "'".$duracion."', ".
                "'".$codigo."')";
    $res = mysql_query($consulta, $conn) or die($consulta."\n");
    $mid = mysql_insert_id();
    $directores = "";
    $emp = FALSE;
    foreach($director as $j) {
        if($emp === TRUE)
            $directores .= ", ";
        $emp = TRUE;
        $directores .= "('".mysql_real_escape_string($j)."', "."'".$mid."')";
    }
    if($emp === TRUE) {
        $consulta = "INSERT INTO DIRECTOR_MOVIE (DNAME, MID) VALUES ".$directores;
        $res = mysql_query($consulta, $conn) or die($consulta."\n");
    }
    $directores = "";
    $emp = FALSE;
    foreach($genero as $j) {
        if($emp === TRUE)
            $directores .= ", ";
        $emp = TRUE;
        $directores .= "('".mysql_real_escape_string($j)."', "."'".$mid."')";
    }
    if($emp === TRUE) {
        $consulta = "INSERT INTO GENRE_MOVIE (GENRE, MID) VALUES ".$directores;
        $res = mysql_query($consulta, $conn) or die($consulta."\n");
    }
    $directores = "";
    $emp = FALSE;
    foreach($cast as $j) {
        if($emp === TRUE)
            $directores .= ", ";
        $emp = TRUE;
        $directores .= "('".mysql_real_escape_string($j)."', "."'".$mid."')";
    }
    if($emp === TRUE) {
        $consulta = "INSERT INTO CAST_MOVIE (CNAME, MID) VALUES ".$directores;
        $res = mysql_query($consulta, $conn) or die($consulta."\n");
    }
    $directores = "";
    $emp = FALSE;
    foreach($escritor as $j) {
        if($emp === TRUE)
            $directores .= ", ";
        $emp = TRUE;
        $directores .= "('".mysql_real_escape_string($j)."', "."'".$mid."')";
    }
    if($emp === TRUE) {
        $consulta = "INSERT INTO WRITER_MOVIE (WNAME, MID) VALUES ".$directores;
        $res = mysql_query($consulta, $conn) or die($consulta."\n");
    }
    $directores = "";
    $emp = FALSE;
    foreach($pais as $j) {
        if($emp === TRUE)
            $directores .= ", ";
        $emp = TRUE;
        $directores .= "('".mysql_real_escape_string($j)."', "."'".$mid."')";
    }
    if($emp === TRUE) {
        $consulta = "INSERT INTO COUNTRY_MOVIE (COUNTRY, MID) VALUES ".$directores;
        $res = mysql_query($consulta, $conn) or die($consulta."\n");
    }
    $directores = "";
    $emp = FALSE;
    foreach($idioma as $j) {
        if($emp === TRUE)
            $directores .= ", ";
        $emp = TRUE;
        $directores .= "('".mysql_real_escape_string($j)."', "."'".$mid."')";
    }
    if($emp === TRUE) {
        $consulta = "INSERT INTO LANGUAGE_MOVIE (LANGUAGE, MID) VALUES ".$directores;
        $res = mysql_query($consulta, $conn) or die($consulta."\n");
    }
//    die("");
    if($i % 1000 == 0)
        echo $i."\n";
}

?>
