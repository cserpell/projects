<?php

function bajar_intro($codigo) {
include("configuracion.php");
	$archivo_temporal = "plotsummary";
	if(file_exists($archivo_temporal));
		unlink($archivo_temporal);
	system("wget -O ".$archivo_temporal." http://www.imdb.com/title/".$codigo."/plotsummary");
	$pagina_imdb = file_get_contents($archivo_temporal);
	$lugar = strpos($pagina_imdb, "\"plotpar\"");
	$lugar_fin = strpos($pagina_imdb, "<i>", $lugar);
	if($lugar != FALSE && $lugar_fin != FALSE)
		return substr($pagina_imdb, $lugar + 11, $lugar_fin - $lugar - 12);
	return "";
}

function bajar_todos() {
include("configuracion.php");
	unlink($archivo_intros);
	$archivo_salida = fopen($archivo_intros, "w");
	if($archivo_salida == FALSE)
		exit("No se pudo abrir archivo de salida");
	$lista_peliculas = file($archivo_excel);
	foreach($lista_peliculas as $pelicula) {
		$campo = explode(";", $pelicula);
		fwrite($archivo_salida, $campo[0]."*"); // Numero de catalogo
		fwrite($archivo_salida, htmlentities($campo[1])."*"); // Nombre
		fwrite($archivo_salida, $campo[2]."*"); // Duracion
		fwrite($archivo_salida, $campo[3]."*"); // Estado
		if(strcmp(substr($campo[4], 0, 1), "t") != 0) {
			fwrite($archivo_salida, "\n");
			continue;
		}
		fwrite($archivo_salida, bajar_intro(trim($campo[4])));
		fwrite($archivo_salida, "\n");
	}
	fclose($archivo_salida);
}

function actualizar_info_intro($num, $nombre, $duracion, $estado, $codigo) {
include("configuracion.php");
	$archivo_salida = fopen("tmp".$archivo_intros, "w");
	$lista_peliculas = file($archivo_intros);
	$guardada = 0;
	foreach($lista_peliculas as $pelicula) {
		$campo = explode("*", $pelicula);
		if($campo[0] == $num) {
			$guardada = 1;
			fwrite($archivo_salida, $num."*"); // Numero de catalogo
			fwrite($archivo_salida, htmlentities($nombre)."*"); // Nombre
			fwrite($archivo_salida, $duracion."*"); // Duracion
			fwrite($archivo_salida, $estado."*"); // Estado
			fwrite($archivo_salida, bajar_intro($codigo));
		} else {
			fwrite($archivo_salida, $campo[0]."*"); // Numero de catalogo
			fwrite($archivo_salida, $campo[1]."*"); // Nombre
			fwrite($archivo_salida, $campo[2]."*"); // Duracion
			fwrite($archivo_salida, $campo[3]."*"); // Estado
			fwrite($archivo_salida, trim($campo[4]));
		}
		fwrite($archivo_salida, "\n");
	}
	if($guardada == 0) {
		fwrite($archivo_salida, $num."*"); // Numero de catalogo
		fwrite($archivo_salida, htmlentities($nombre)."*"); // Nombre
		fwrite($archivo_salida, $duracion."*"); // Duracion
		fwrite($archivo_salida, $estado."*"); // Estado
		fwrite($archivo_salida, bajar_intro($codigo));
		fwrite($archivo_salida, "\n");
	}
	fclose($archivo_salida);
	unlink($archivo_intros);
	rename("tmp".$archivo_intros, $archivo_intros);
}

function nueva_pelicula($num, $nombre, $duracion, $codigo) {
include("configuracion.php");
	$archivo_salida = fopen("tmp".$archivo_excel, "w");
	$lista_peliculas = file($archivo_excel);
	foreach($lista_peliculas as $pelicula) {
		$campo = explode(";", $pelicula);
		if($campo[0] == $num) {
			fclose($archivo_salida);
			unlink("tmp".$archivo_excel);
			return;
		}
		fwrite($archivo_salida, $campo[0].";"); // Numero de catalogo
		fwrite($archivo_salida, $campo[1].";"); // Nombre
		fwrite($archivo_salida, $campo[2].";"); // Duracion
		fwrite($archivo_salida, $campo[3].";"); // Estado
		fwrite($archivo_salida, $campo[4]);
		fwrite($archivo_salida, "\n");
	}
	fwrite($archivo_salida, $num.";"); // Numero de catalogo
	fwrite($archivo_salida, $nombre.";"); // Nombre
	fwrite($archivo_salida, $duracion.";"); // Duracion
	fwrite($archivo_salida, "N;"); // Estado
	fwrite($archivo_salida, $codigo);
	fwrite($archivo_salida, "\n");
	fclose($archivo_salida);
	unlink($archivo_excel);
	rename("tmp".$archivo_excel, $archivo_excel);
	actualizar_info_intro($num, $nombre, $duracion, "N", $codigo);
}

function editar_pelicula($num, $nombre, $duracion, $estado, $codigo) {
include("configuracion.php");
	$archivo_salida = fopen("tmp".$archivo_excel, "w");
	$lista_peliculas = file($archivo_excel);
	foreach($lista_peliculas as $pelicula) {
		$campo = explode(";", $pelicula);
		fwrite($archivo_salida, $campo[0].";"); // Numero de catalogo
		if($campo[0] == $num) {
			fwrite($archivo_salida, $nombre.";"); // Nombre
			fwrite($archivo_salida, $duracion.";"); // Duracion
			fwrite($archivo_salida, $estado.";"); // Estado
			fwrite($archivo_salida, $codigo);
		} else {
			fwrite($archivo_salida, $campo[1].";"); // Nombre
			fwrite($archivo_salida, $campo[2].";"); // Duracion
			fwrite($archivo_salida, $campo[3].";"); // Estado
			fwrite($archivo_salida, trim($campo[4]));
		}
		fwrite($archivo_salida, "\n");
	}
	fclose($archivo_salida);
	unlink($archivo_excel);
	rename("tmp".$archivo_excel, $archivo_excel);
	actualizar_info_intro($num, $nombre, $duracion, $estado, $codigo);
}

?>
