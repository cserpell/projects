<?
include("configuracion.php");
include("bajarintros.php");
if(isset($_GET["desde"]))
	$desde = $_GET["desde"];
else
	$desde = 0;
$numitems = -1;

$num = $_POST["num"];
$nombre = $_POST["nombre"];
$duracion = $_POST["duracion"];
$estado = $_POST["estado"];
$codigo = $_POST["codigo"];

if(isset($_POST["nueva"]))
	nueva_pelicula($num, $nombre, $duracion, $codigo);
if(isset($_POST["modificar"]))
	editar_pelicula($num, $nombre, $duracion, $estado, $codigo);
if(isset($_POST["todonuevo"]))
	bajar_todos();
?>
<html>
<head>
<title>Cat&aacute;logo de Pel&iacute;culas</title>
</head>
<body>
<div id="wrap">
<div id="content">
<h2 align="left">Cat&aacute;logo de Pel&iacute;culas</h2>   
<?
	$listacompleta = "Lista de pel&iacute;culas:<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\" width=\"100%\"><tr bgcolor=\"#0066CC\"><td width=\"4%\"><font color=\"#FFFFFF\">C&oacute;digo</font></td><td width=\"30%\"><font color=\"#FFFFFF\">Nombre</font></td><td width=\"3%\"><font color=\"#FFFFFF\">Durac&oacute;n</font></td><td width=\"3%\"><font color=\"#FFFFFF\">Estado</font></td><td width=\"60%\"><font color=\"#FFFFFF\">Descripci&oacute;n</font></td></tr>";
	$lista_peliculas = file($archivo_intros);
	foreach($lista_peliculas as $pelicula) {
		$numitems++;
		if($numitems < $desde || $numitems >= $desde + $cantidad_mostrada)
			continue;
		$campo = explode("*", $pelicula);
		$listacompleta .= "<tr bgcolor=\"#DDEEFF\">";
		$listacompleta .= "<td>".$campo[0]."</td>"; // Numero de catalogo
		$listacompleta .= "<td>".$campo[1]."</td>"; // Nombre
		$listacompleta .= "<td>".$campo[2]."</td>"; // Duracion
		if(strcmp($campo[3], "N") == 0)
			$listacompleta .= "<td></td>"; // Estado
		else
			$listacompleta .= "<td>".$campo[3]."</td>"; // Estado
		$listacompleta .= "<td>".$campo[4]."</td>"; // Duracion
		$listacompleta .= "</tr>";
	}
	echo $listacompleta."</table>";
	
	if($desde > 0) {
		$num = $desde - $cantidad_mostrada;
		if($num < 0)
			$num = 0;
		echo "<br > <a href=\"index.php?desde=".$num."\">Anteriores ".($desde -  $num)."</a>";
	}
	if($numitems > $desde + $cantidad_mostrada)
		echo "<br > <a href=\"index.php?desde=".($desde + $cantidad_mostrada)."\">Siguientes ".$cantidad_mostrada."</a>";
?>
</div>
</div>
</body>
</html>
