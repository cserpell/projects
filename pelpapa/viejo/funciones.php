<?php
session_start();
include("database.php");

function fechaValida($fecha) {
    // Supondremos que la fecha esta guardada como timestamp
    return (is_numeric($fecha) && ($fecha > 0));
}

function subirProblema() {
    if(!isset($_POST["fecha_publicacion"])
    || !isset($_POST["fecha_limite"])
    || !isset($_POST["titulo"])
    || !isset($_POST["metodo_resolucion"])
    || !isset($_POST["respuesta"])
    || !isset($_POST["puntaje"])
    || !isset($_POST["texto"]))
        return -1;

    if(!fechaValida($_POST["fecha_publicacion"])
    || !fechaValida($_POST["fecha_publicacion"]))
        return -1;

    if($_POST["metodo_resolucion"] != 'A' && $_POST["metodo_resolucion"] != 'D')
        return -1;

    if(!is_numeric($_POST["puntaje"]))
        return -1;

    $conn = openDatabaseConnection();
    $query = "INSERT INTO problema (fecha_publicacion, fecha_limite, titulo, texto, metodo_resolucion, respuesta, puntaje) VALUES ('".$_POST["fecha_publicacion"]."', '".$_POST["fecha_limite"]."', '".$_POST["titulo"]."', '".$_POST["texto"]."', '".$_POST["metodo_resolucion"]."', '".$_POST["respuesta"]."', '".$_POST["puntaje"]."')";
    if(!mysql_query($query, $conn))
        return -1;
    mysql_close();
    return 1;
}

function subirRespuesta() {
    if(!isset($_POST["id_participante"])
    || !isset($_POST["id_problema"])
    || !isset($_POST["respuesta"])
        return -1;

    if(!is_numeric($_POST["id_participante"]) || !is_numeric($_POST["id_problema"]))
        return -1;

    $conn = openDatabaseConnection();
    $query = "SELECT (id_problema, metodo_resolucion, respuesta, fecha_limite) FROM problema WHERE id_problema = '".$_POST["id_problema"]."'";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    $problema = mysql_fetch_array($res);
    if($problema["metodo_resolucion"] == 'A'
     && ($_POST["respuesta"] < 'A' || $_POST["respuesta"] > 'E')) {
        mysql_close();
        return -1;
    }
    // Verifica que exista el participante en cuestion
    $query = "SELECT id_participante FROM participante WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    $query = "SELECT (id_participante, id_problema, correcta) FROM respuestas WHERE id_participante = '".$_POST["id_participante"]."' AND id_problema = '".$_POST["id_problema"]."'";
    if($res = mysql_query($query, $conn)) {
        $anterior = mysql_fetch_array($res);
        if(time() < $problema["fecha_limite"] || $anterior["correcta"] == '0') {
            // Todavia se esta en periodo que no se sabe si la respuesta es correcta o no
            // O la respuesta que habia era errada
            $query = "DELETE FROM respuestas WHERE id_problema = '".$_POST["id_problema"]."' AND id_participante = '".$_POST["id_participante"]."'";
            if(!mysql_query($query, $conn)) {
                mysql_close();
                return -1;
            }
        } else {
            mysql_close();
            return -1; // No es seguro si 1 o -1 ??
        }
    }
    $correcta = 0;
    if($_POST["respuesta"] == $problema["respuesta"])
        $correcta = 1;
    // La verificacion de correcta no es estrictamente necesaria aqui, pero facilita el trabajo
    // Al contar el puntaje
    $query = "INSERT INTO respuestas (id_participante, id_problema, respuesta, fecha, correcta) VALUES ('".$_POST["id_participante"]."', '".$_POST["id_problema"]."', '".$_POST["respuesta"]."', '".time()."', '".$correcta."')";
    if(!mysql_query($query, $conn)) {
        mysql_close();
        return -1;
    }
    mysql_close();
    return 1;
}

function calcularPuntaje() {
    if(!isset($_POST["id_participante"]))
        return -1;

    if(!is_numeric($_POST["id_participante"]))
        return -1;

    $conn = openDatabaseConnection();
    // Verifica que exista el participante en cuestion
    $query = "SELECT id_participante FROM participante WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    $query = "SELECT (id_problema, correcta, fecha) FROM respuestas WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    $puntajetotal = 0;
    $puntajetotal_fecha = 0;
    while($respuesta = mysql_fetch_array($res)) {
        if($respuesta["correcta"] == 1) {
            $query = "SELECT (fecha_limite, puntaje) FROM problema WHERE id_problema = '".$respuesta["id_problema"]."'";
            if(!($res2 = mysql_query($query, $conn))) {
                mysql_close();
                return -1;
            }
            $problema = mysql_fetch_array($res2);
            // Para contar en el puntaje se debe considerar solo los problemas que ya termino su periodo
            // de incertidumbre
            if(time() > $problema["fecha_limite"]) {
                if($problema["fecha_limite"] > $respuesta["fecha"])
                    $puntajetotal_fecha = $puntajetotal_fecha + $problema["puntaje"];
                $puntajetotal = $puntajetotal + $problema["puntaje"];
            }
        }
    }
    $query = "UPDATE participante SET puntaje = '".$puntajetotal."' WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!mysql_query($query, $conn)) {
        mysql_close();
        return -1;
    }
    $query = "UPDATE participante SET puntaje_fecha = '".$puntajetotal_fecha."' WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!mysql_query($query, $conn)) {
        mysql_close();
        return -1;
    }
    mysql_close();
    return 1;
}

function puntajeRelativo($puntaje, $participantestotal, $correctastotal) {
    // Puntaje decae linealmente de manera que si uno lo ha respondido bien, obtiene $puntaje,
    // y si todos los tienen bien, obtienen 1 pto.
    if($participantestotal != 1)
        return ($puntaje*$participantestotal - 1 + (1 - $puntaje)*$correctastotal)/($participantestotal - 1);
    return $puntaje;
}

function calcularPuntajeRelativo() {
    if(!isset($_POST["id_participante"]))
        return -1;

    if(!is_numeric($_POST["id_participante"]))
        return -1;

    $conn = openDatabaseConnection();
    // Verifica que exista el participante en cuestion
    $query = "SELECT id_participante FROM participante WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    $query = "SELECT (id_problema, correcta, fecha) FROM respuestas WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    $query = "SELECT COUNT(*) FROM participante";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    $participantestotal = mysql_fetch_array($res);
    $puntajetotal = 0;
    $puntajetotal_fecha = 0;
    while($respuesta = mysql_fetch_array($res)) {
        if($respuesta["correcta"] == 1) {
            $query = "SELECT (fecha_limite, puntaje) FROM problema WHERE id_problema = '".$respuesta["id_problema"]."'";
            if(!($res2 = mysql_query($query, $conn))) {
                mysql_close();
                return -1;
            }
            $problema = mysql_fetch_array($res2);
            // Para contar en el puntaje se debe considerar solo los problemas que ya termino su periodo
            // De incertidumbre
            if(time() > $problema["fecha_limite"]) {
                $query = "SELECT COUNT(*) FROM respuestas WHERE id_problema = '".$respuesta["id_problema"]."' AND correcta = '1'";
                if(!($res2 = mysql_query($query, $conn))) {
                    mysql_close();
                    return -1;
                }
                $correctastotal = mysql_fetch_array($res2);
                $puntajebuscado = puntajeRelativo($problema["puntaje"], $participantestotal, $correctastotal);
                if($problema["fecha_limite"] > $respuesta["fecha"])
                    $puntajetotal_fecha = $puntajetotal_fecha + $puntajebuscado;
                $puntajetotal = $puntajetotal + $puntajebuscado;
            }
        }
    }
    $query = "UPDATE participante SET puntaje_relativo = '".$puntajetotal."' WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!mysql_query($query, $conn)) {
        mysql_close();
        return -1;
    }
    $query = "UPDATE participante SET puntaje_relativo_fecha = '".$puntajetotal_fecha."' WHERE id_participante = '".$_POST["id_participante"]."'";
    if(!mysql_query($query, $conn)) {
        mysql_close();
        return -1;
    }
    mysql_close();
    return 1;
}

function actualizarPuntajes() {
    $conn = openDatabaseConnection();
    $query = "SELECT id_participante FROM participante";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    mysql_close();
    while($participante = mysql_fetch_array($res)) {
        $_POST["id_participante"] = $participante["id_participante"];
        if(calcularPuntaje() == -1)
            return -1;
        if(calcularPuntajeRelativo() == -1)
            return -1;
    }
    return 1;
}


/*


function corregirProblema() {
    if(!isset($_POST["id_problema"]))
        return -1;

    if(!is_numeric($_POST["id_problema"]))
        return -1;

    $conn = openDatabaseConnection();
    $query = "SELECT (respuesta, puntaje) FROM problema WHERE id_problema = '".$_POST["id_problema"]."'";
    if(!($res = mysql_query($query, $conn))) {
        mysql_close();
        return -1;
    }
    $problema = mysql_fetch_array($res);
    $query = "SELECT (id_participante, id_problema, atiempo, respuesta) FROM respuestas WHERE id_problema = '".$_POST["id_problema"]."'";
    $res = mysql_query($query, $conn);
    while($respuesta = mysql_fetch_array($res)) {
        if($problema["respuesta"] == $respuesta["respuesta"]) {
            $query = "UPDATE respuestas SET correcta = 1 WHERE id_participante = '".$respuesta["id_participante"]."' AND id_problema = '".$respuesta["id_problema"]."'";
            if(!mysql_query($query, $conn)) {
                mysql_close();
                return -1;
            }
        } else {
            $query = "UPDATE respuestas SET correcta = 0 WHERE id_participante = '".$respuesta["id_participante"]."' AND id_problema = '".$respuesta["id_problema"]."'";
            if(!mysql_query($query, $conn)) {
                mysql_close();
                return -1;
            }
        }
	}
    return 1;
    // Se puede hacer valer los puntajes aqui mismo, segun lo conversado
    // pueden ser segun como le va a todos el puntaje asignado, sino constante.
}
*/
?>
