<?php
require_once("database.php");
    $seleccionada = $_GET["seleccionada"];
    $desdepos = $_GET["desdepos"];
    $ordenarpor = $_GET["ordenarpor"];
    $maxpag = 100;// $_GET["maxpag"];
    $titulo = $_POST["titulo"];
    $actor = $_POST["actor"];
    $haytitulo = FALSE;
    $hayactor = FALSE;
    if($titulo)
        $haytitulo = TRUE;
    if($actor)
        $hayactor = TRUE;
?>
<html>
<head>
<title>Lista de Pel&iacute;culas</title>
</head>
<body>
<h2>Lista de Pel&iacute;culas</h2>
<form name="buscar" method="post" action="listapel.php">
Ingrese texto para buscar por t&iacute;tulo: <input type="text" size="20" maxlength="100" name="titulo" value="<? echo $titulo; ?>"></input>
<br />
Ingrese texto para buscar por actor: <input type="text" size="20" maxlength="100" name="actor" value="<? echo $actor; ?>"></input>
<br />
<input type="submit" name="Accion" value="Buscar">
</form>
<?
    $conn = openDatabaseConnection();

    if($seleccionada) {
        $query = "SELECT TITLE, TITULO, YEAR, TAGLINE, PLOTOUTLINE, CODE, RUNTIME FROM ";
        $query .= "MOVIE WHERE MID = '".mysql_real_escape_string($seleccionada)."'";
        $res = mysql_query($query, $conn);
        $peli = mysql_fetch_array($res);
        if($peli) {
?>
T&iacute;tulo original:<br />
<strong><? echo $peli["TITLE"]; ?></strong><br />
T&iacute;tulo en espa&ntilde;ol (lista pap&aacute;):<br />
<strong><? echo $peli["TITULO"]; ?></strong><br />
<br />
A&ntilde;o: <? echo $peli["YEAR"]; ?><br />
C&oacute;digo en lista pap&aacute;: <? echo $peli["CODE"]; ?><br />
Duraci&oacute;n: <? echo $peli["RUNTIME"]; ?> min<br />
<br />
Rese&ntilde;a:<br /><? echo $peli["TAGLINE"]; ?><br />
Sinopsis:<br /><? echo $peli["PLOTOUTLINE"]; ?><br />
<br />
Pa&iacute;s:
<br />
<?
            $query = "SELECT COUNTRY FROM COUNTRY_MOVIE WHERE MID = '".mysql_real_escape_string($seleccionada)."'";
            $res = mysql_query($query, $conn);
            while($row = mysql_fetch_array($res))
                echo $row["COUNTRY"]."<br />";
?>
<br />
Director:
<br />
<?
            $query = "SELECT DNAME FROM DIRECTOR_MOVIE WHERE MID = '".mysql_real_escape_string($seleccionada)."'";
            $res = mysql_query($query, $conn);
            while($row = mysql_fetch_array($res))
                echo $row["DNAME"]."<br />";
?>
<br />
G&eacute;nero:
<br />
<?
            $query = "SELECT GENRE FROM GENRE_MOVIE WHERE MID = '".mysql_real_escape_string($seleccionada)."'";
            $res = mysql_query($query, $conn);
            while($row = mysql_fetch_array($res))
                echo $row["GENRE"]."<br />";
?>
<br />
Idioma:
<br />
<?
            $query = "SELECT LANGUAGE FROM LANGUAGE_MOVIE WHERE MID = '".mysql_real_escape_string($seleccionada)."'";
            $res = mysql_query($query, $conn);
            while($row = mysql_fetch_array($res))
                echo $row["LANGUAGE"]."<br />";
?>
<br />
Escritor:
<br />
<?
            $query = "SELECT WNAME FROM WRITER_MOVIE WHERE MID = '".mysql_real_escape_string($seleccionada)."'";
            $res = mysql_query($query, $conn);
            while($row = mysql_fetch_array($res))
                echo $row["WNAME"]."<br />";
?>
<br />
Reparto:
<table border="0" cellspacing="2" cellpadding="2" width="100%">
<tr bgcolor="#0066CC">
<td width="90%"><font color="#FFFFFF">Nombre</font></td>
<td width="10%"><font color="#FFFFFF">Ver</font></td>
</tr>
<?
           $query = "SELECT CNAME FROM CAST_MOVIE WHERE MID = '".mysql_real_escape_string($seleccionada)."'";
           $res = mysql_query($query, $conn);
           while($row = mysql_fetch_array($res)) {
?>
<tr bgcolor=\"#DDEEFF\">
<td><? echo $row["CNAME"]; ?></td>
<td><a href="listapel.php?ordenarpor=<? echo urlencode($ordenarpor); ?>&desdepos=<? echo urlencode($desdepos); ?>&seleccionada=<? echo urlencode($seleccionada); ?>"></a></td>
</tr>
<?
            }
?>
</table>
<?
        }
    }
?>
<br />
<?
    $query = "SELECT MID, CODE, TITLE, RUNTIME FROM MOVIE WHERE RUNTIME > 0";
    if($haytitulo) {
        if($hayactor)
            $query = "SELECT MOVIE.MID, CODE, TITLE, RUNTIME FROM MOVIE, CAST_MOVIE WHERE MOVIE.MID = CAST_MOVIE.MID AND RUNTIME > 0 AND TITLE LIKE '%".mysql_real_escape_string($titulo)."%' AND CNAME LIKE '%".mysql_real_escape_string($actor)."%'";        
        else
            $query = "SELECT MID, CODE, TITLE, RUNTIME FROM MOVIE WHERE RUNTIME > 0 AND TITLE LIKE '%".mysql_real_escape_string($titulo)."%'";
    } else {
        if($hayactor)
            $query = "SELECT MOVIE.MID, CODE, TITLE, RUNTIME FROM MOVIE, CAST_MOVIE WHERE MOVIE.MID = CAST_MOVIE.MID AND RUNTIME > 0 AND CNAME LIKE '%".mysql_real_escape_string($actor)."%'";        
    }
    if($ordenarpor)
        $query .= " ORDER BY ".mysql_real_escape_string($ordenarpor);
    if(!$desdepos)
        $desdepos = 1;
    $query .= " LIMIT ".mysql_real_escape_string($desdepos).", ".mysql_real_escape_string($maxpag)."";
    $res = mysql_query($query, $conn) or die($query);
?>
Lista de pel&iacute;culas:
<br />
<table border="0" cellspacing="2" cellpadding="2" width="100%">
<tr bgcolor="#0066CC">
<td width="5%"><font color="#FFFFFF">C&oacute;digo</font></td>
<td width="85%"><font color="#FFFFFF">T&iacute;tulo</font></td>
<td width="5%"><font color="#FFFFFF">Duraci&oacute;n</font></td>
<td width="5%"><font color="#FFFFFF">Ver</font></td>
</tr>
<?
    $ipos = 1;
    while($row = mysql_fetch_array($res)) {
?>
<tr bgcolor=\"#DDEEFF\">
<td><? echo $row["CODE"]; ?></td>
<td><? echo $row["TITLE"]; ?></td>
<td><? echo $row["RUNTIME"]; ?></td>
<td><a href="listapel.php?ordenarpor=<? echo urlencode($ordenarpor); ?>&desdepos=<? echo urlencode($desdepos); ?>&seleccionada=<? echo urlencode($row["MID"]); ?>">Abrir</a></td>
</tr>
<?
        $ipos++;
    }
    mysql_close();
?>
</table>
</div>
</div>
</body>
</html>
