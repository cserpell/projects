//---------------------------------------------------------------------------
#include <clx.h>
#include <math.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.xfm"
TFormPrincipal *FormPrincipal;
//---------------------------------------------------------------------------
__fastcall TFormPrincipal::TFormPrincipal(TComponent* Owner)
        : TForm(Owner)
{
        NumeroParticular = 0;
        bmp = new Graphics::TBitmap();
        bmp->Canvas->Pen->Color = clBlue;
        Seguir = false;
        Altura = 0;
}
//---------------------------------------------------------------------------
void TFormPrincipal::DibujarParticulas()
{
        int MaxX = PaintBoxIzquierdo->Width - Radio;
        int MaxY = PaintBoxIzquierdo->Height - Radio;

        bmp->Width = PaintBoxIzquierdo->Width;
        bmp->Height = PaintBoxIzquierdo->Height;
        bmp->Canvas->Brush->Color = clBlack;
        bmp->Canvas->DrawRect(TRect(0, 0, bmp->Width, bmp->Height));
        bmp->Canvas->Brush->Color = clYellow;
        for(int i = 0; i < NumeroParticulas; i++)
        {
                if((int)(Todas[i]->posx) < Radio ||
                   (int)(Todas[i]->posx) >= MaxX - Radio) ||
                   (int)(Todas[i]->posy) < Radio ||
                   (int)(Todas[i]->posy) >= MaxY - Radio)
                        continue;
                bmp->Canvas->DrawCirc(Todas[i]->posx - Radio,
                                      Todas[i]->posy - Radio,
                                      Todas[i]->posx + Radio,
                                      Todas[i]->posy + Radio);
        }
        bmp->Canvas->DrawLine(0, bmp->Height - Altura,
                              bmp->Width, bmp->Height - Altura);
        PaintBoxIzquierdoPaint(this);
}
//---------------------------------------------------------------------------
void __fastcall TFormPrincipal::ButtonEmpezarClick(TObject *Sender)
{
        Tiempo = 0;
        Seguir = false;
        NumeroParticulas = StrToInt(EditParticulas);
        Radio = StrToInt(EditParticulas);
        AlturaMaxima = StrToInt(EditAmplitud);
        Altura = AlturaMaxima;
        Dt = StrToFloat(EditDt);
        if(Radio < 0 || NumeroParticulas < 0)
                return;
        Frecuencia = StrToFloat(EditFrecuencia);
        for(int i = 0; i < NumeroParticulas; i++)
                Todas[i] = new Particula(Radio, Radio + 2*i*Radio);
        while(Seguir)
        {
                Procesar(Dt);
                DibujarParticulas();
                Application->ProcessMessages();
        }
}
//---------------------------------------------------------------------------
void TFormPrincipal::Colisionar(Particula *Una, Particula *Otra)
{
}
//---------------------------------------------------------------------------
void TFormPrincipal::Procesar(double dift)
{
        Tiempo++;
        Altura = AlturaMaxima*sin(dift*Tiempo) + AlturaMaxima;
        for(int i = 0; i < NumeroParticulas; i++)
        {
                Todas[i]->Paso(dift);
        }
}
//---------------------------------------------------------------------------
void __fastcall TFormPrincipal::PaintBoxIzquierdoPaint(TObject *Sender)
{
        PaintBoxIzquierdo->Canvas->Draw(0, 0, bmp);
}
//---------------------------------------------------------------------------
void __fastcall TFormPrincipal::ButtonTerminarClick(TObject *Sender)
{
        Seguir = false;
}
//---------------------------------------------------------------------------
void __fastcall TFormPrincipal::ButtonModificarClick(TObject *Sender)
{
        Frecuencia = StrToFloat(EditFrecuencia);
}
//---------------------------------------------------------------------------

