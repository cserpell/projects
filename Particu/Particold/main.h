//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <QControls.hpp>
#include <QStdCtrls.hpp>
#include <QForms.hpp>
#include <QExtCtrls.hpp>
#define Fzax (0)
#define Fzay (-0.1)
//---------------------------------------------------------------------------
class Particula
{
private:
        double posxold;
        double posyold;
public:
        double posx;
        double posy;
        double velx(double dift)
        {
                return (posx-posxold)/dift;
        }
        double vely(double dift)
        {
                return (posy-posyold)/dift;
        }
        Particula(double px, double py)
        {
                posxold = px;
                posyold = py;
                posx = px;
                posy = py;
        }
        void Paso(double dift);
        {
                double pxa = 2*posx - posxold + dift*dift*Fzax; // /masa
                double pya = 2*posy - posyold + dift*dift*Fzay;

                posxold = posx;
                posyold = posy;
                posx = pxa;
                posy = pya;
        }
};
class TFormPrincipal : public TForm
{
__published:	// IDE-managed Components
        TPaintBox *PaintBoxIzquierdo;
        TSplitter *Splitter;
        TPanel *PanelDerecho;
        TEdit *EditFrecuencia;
        TLabel *LabelFrecuencia;
        TLabel *LabelHz;
        TButton *ButtonEmpezar;
        TEdit *EditParticulas;
        TLabel *LabelParticulas;
        TEdit *EditRadio;
        TLabel *LabelRadio;
        TEdit *EditDt;
        TLabel *LabelDt;
        TLabel *LabelSeg;
        TButton *ButtonTerminar;
        TButton *ButtonModificar;
        TEdit *EditAmplitud;
        TLabel *LabelAmplitud;
        void __fastcall ButtonEmpezarClick(TObject *Sender);
        void __fastcall PaintBoxIzquierdoPaint(TObject *Sender);
        void __fastcall ButtonTerminarClick(TObject *Sender);
        void __fastcall ButtonModificarClick(TObject *Sender);
private:	// User declarations
        void DibujarParticulas();
        int NumeroParticulas;
        Particula *Todas;
        int Radio;
        double Frecuencia;
        double Dt;
        double AlturaPiso;
        double AlturaMaxima;
        bool Seguir;
        int Tiempo;
        void Procesar(double dift);
        void Colisionar(Particula *Una, Particula *Otra);
        Graphics::TBitmap *bmp;
public:		// User declarations
        __fastcall TFormPrincipal(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormPrincipal *FormPrincipal;
//---------------------------------------------------------------------------
#endif
