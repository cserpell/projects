using System;
using System.Windows.Forms;

namespace MyFormProject 
{
	class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox muestra;
		private System.Windows.Forms.Label labeltotal;
		private ControlParticulas juego;
		private static int TAMANO = 200;
		private static int TAMPART = 3;
		private double tiempototal;
		private double tiempoProximoDespliegue;
		private double tiempoespera;
		public MainForm()
		{
			InitializeComponent();
		}
		public MainForm(double ntes, int np, double elas)
		{
			juego = new ControlParticulas(0, 9.8/3200, 0, elas);
			if(np > ControlParticulas.MAXPART)
				np = ControlParticulas.MAXPART;
			vcr vert1 = new vcr();
			vcr vert2 = new vcr();
			vcr n1 = new vcr();
			vcr n2 = new vcr();
			vcr n3 = new vcr();
		
			vert1.x = 0;
			vert1.y = 0;
        	vert1.z = 0;
    	    vert2.x = TAMANO;
			vert2.y = TAMANO;
        	vert2.z = TAMANO;
			n1.x = 0;
	        n1.y = 0;
        	n1.z = 1;
    	    n2.x = 0;
	        n2.y = 1;
        	n2.z = 0;
    	    n3.x = 1;
	        n3.y = 0;
        	n3.z = 0;
    	    juego.AgregarMuro(vert1, n1);
	        juego.AgregarMuro(vert1, n2);
        	juego.AgregarMuro(vert1, n3);
    	    juego.AgregarMuro(vert2, n1);
	        juego.AgregarMuro(vert2, n2);
        	juego.AgregarMuro(vert2, n3);
    	    for(int i = 0; i < np; i++)
	        {
	        	Random AA = new Random();
            	double px, py, pz, vx, vy, vz;

        	    px = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
    	        py = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
	            pz = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
            	vx = 0;
        	    vy = 0;
				vz = 0;
			
	            while(!juego.AgregarParticula(px, py, pz, vx, vy, vz, TAMPART, 1))
				{
				    px = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
        	        py = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
    	            pz = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
	            }
        	}
			tiempoespera = ntes;
        	tiempototal = 0;
			tiempoProximoDespliegue = 0;
			InitializeComponent();
			muestra.Size = new System.Drawing.Size((int)(TAMANO*1.5), (int)(TAMANO*1.5));
        	/*m_button.signal_clicked().connect(
		                     SigC::slot(*this, &Programita::on_button_clicked));*/
//			Glib::signal_idle().connect(SigC::slot(*this, &Programita::Loop), Glib::PRIORITY_DEFAULT_IDLE);			
		}
	
		// THIS METHOD IS MAINTAINED BY THE FORM DESIGNER
		// DO NOT EDIT IT MANUALLY! YOUR CHANGES ARE LIKELY TO BE LOST
		void InitializeComponent() {
			this.labeltotal = new System.Windows.Forms.Label();
			this.muestra = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// labeltotal
			// 
			this.labeltotal.Location = new System.Drawing.Point(72, 24);
			this.labeltotal.Name = "labeltotal";
			this.labeltotal.TabIndex = 0;
			this.labeltotal.Text = "0";
			// 
			// muestra
			// 
			this.muestra.Location = new System.Drawing.Point(72, 112);
			this.muestra.Name = "muestra";
			this.muestra.Size = new System.Drawing.Size(100, 88);
			this.muestra.TabIndex = 1;
			this.muestra.TabStop = false;
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.muestra);
			this.Controls.Add(this.labeltotal);
			this.Name = "MainForm";
			this.Text = "This is my form";
			this.ResumeLayout(false);
		}
			
		[STAThread]
		public static void Main(string[] args)
		{
			double ntes = 0;
			int np = 150;
			double elas = 1;

			if(args[1] != null)
            	np = Int32.Parse(args[1]);
    		if(args[2] != null)
        		elas = Double.Parse(args[2]);
    		if(args[3] != null)
				ntes = Double.Parse(args[3]);
			Application.Run(new MainForm(ntes, np, elas));
		}
		public bool Loop()
    	{
        	double t;
			t = juego.EjecutarProxEvento();
			tiempototal += t;
			if(t == 0)
			{
				//cerr << "Llama a metodoEmergencia\n";
				juego.MetodoEmergencia();
			}
			if(tiempototal > tiempoProximoDespliegue)
			{
				tiempoProximoDespliegue += tiempoespera;
		//		on_button_clicked();
			}
			return true;
		}/*
    	virtual void on_button_clicked()
    	{
			string s5;
    		Int32 ii = new Int32(tiempototal);

			s5 = ii.ToString();
			labeltotal.set_text(s5.str());		
			Glib::RefPtr<Gdk::Window> win = get_window();
  			Glib::RefPtr<Gdk::GC> gc = get_style()->get_black_gc();
			for(int i = 0; i < cp.NumeroParticulas(); i++)
				win->draw_arc(gc, true, (int)cp[i].Posicion().x,
                                    (int)cp[i].Posicion().y,
                      (int)(2*cp[i].Radio()), (int)(2*cp[i].Radio()), 0, 23040);
            muestra.redibujar();
  			return true;
    	}*/
	}

class vcr
{
	public double x;
    public double y;
	public double z;
}

class Muro
{
  	private vcr pto = new vcr();
    private vcr normalunit = new vcr();
    private double frec;
    private double amplitud;
    public Muro()
    {
		pto.x = 0;
		pto.y = 0;
		pto.z = 0;
		normalunit.x = 0;
		normalunit.y = 1;
		normalunit.z = 0;
		frec = 0;
		amplitud = 0;
    }
    public Muro(vcr p, vcr n)
    {
		double normn;
		
		pto.x = p.x;
		pto.y = p.y;
		pto.z = p.z;
		if(n.x == 0 && n.y == 0 && n.z == 0)
			n.y = 1;
		normn = Math.Sqrt(n.x*n.x + n.y*n.y + n.z*n.z); // Normal DEBE ser unitaria
		normalunit.x = n.x/normn;
        normalunit.y = n.y/normn;
        normalunit.z = n.z/normn;
        frec = 0;
		amplitud = 0;
    }
	public vcr Punto()
	{
		return pto;
	}
	public vcr Normal()
	{
		return normalunit;
	}
	public void DefinirMovimiento(double nuevafrec, double nuevaamplit)
	{
    	frec = nuevafrec;
    	amplitud = nuevaamplit;
	}
	public void Mover(double tiempo)
	{
	}
	public void CopiarMuro(Muro otro)
	{
		pto.x = otro.pto.x;
		pto.y = otro.pto.y;
		pto.z = otro.pto.z;
		normalunit.x = otro.normalunit.x;
    	normalunit.y = otro.normalunit.y;
    	normalunit.z = otro.normalunit.z;
    	frec = otro.frec;
		amplitud = otro.amplitud;
	}
};

class Partic
{
    private vcr pos = new vcr();
    private vcr vel = new vcr();
    private vcr acc = new vcr();
    private double radp;
    private double masa;
    public Partic()
    {
	    radp = 0;
		masa = 0;
	}
    public Partic(double px, double py, double pz, double vx, double vy, double vz,
                                                         double r, double m)
    {
        pos.x = px;
        pos.y = py;
        pos.z = pz;
        vel.x = vx;
        vel.y = vy;
        vel.z = vz;
        radp = Math.Sqrt(r*r);
        masa = Math.Sqrt(m*m);
        acc.x = 0;
        acc.y = 0;
		acc.z = 0;
    }
    public double Distancia(Partic otra)
	{
		double a = pos.x - otra.pos.x;
    	double b = pos.y - otra.pos.y;
		double c = pos.z - otra.pos.z;

    	return Math.Sqrt(a*a + b*b + c*c);
	}
    public bool VerificarColision(Muro elmuro)
	{
		 double distanciaamuro, xx, yy, zz;

    	xx = (pos.x - elmuro.Punto().x)*elmuro.Normal().x;
		yy = (pos.y - elmuro.Punto().y)*elmuro.Normal().y;
		zz = (pos.z - elmuro.Punto().z)*elmuro.Normal().z;
		distanciaamuro = xx*xx + yy*yy + zz*zz;
    	if(distanciaamuro <= radp*radp)
        	return true;
    	return false;
	}
    public bool VerificarColision(Partic otra)
	{
		if(Distancia(otra) <= radp + otra.radp)
        	return true;
    	return false;
	}
    public void ModificacionForzada(double nuevax, double nuevay, double nuevaz)
	{   // Se usa para las colisiones entre ellas
    	vel.x += nuevax;
    	vel.y += nuevay;
		vel.z += nuevaz;
	}
    public void ColisionMuro(Muro muro)
    {
		double cte = 2*(vel.x*muro.Normal().x + vel.y*muro.Normal().y
                                                       + vel.z*muro.Normal().z);
	
		vel.x -= cte*muro.Normal().x;
    	vel.y -= cte*muro.Normal().y;
    	vel.z -= cte*muro.Normal().z;
    }
    public void DefinirFuerza(double fx, double fy, double fz)
    {
	    acc.x = fx/masa;
    	acc.y = fy/masa;
		acc.z = fz/masa;
    }
    public void CopiarParticula(Partic otra)
	{
		pos.x = otra.pos.x;
    	pos.y = otra.pos.y;
	    pos.z = otra.pos.z;
    	vel.x = otra.vel.x;
	    vel.y = otra.vel.y;
    	vel.z = otra.vel.z;
    	radp = otra.radp;
    	masa = otra.masa;
    	acc.x = otra.acc.x;
    	acc.y = otra.acc.y;
		acc.z = otra.acc.z;
	}
	public vcr Posicion() { return Posicion(0); }
	public vcr Posicion(double temp)
	{
	    vcr res = new vcr();

    	res.x = pos.x + vel.x*temp + acc.x*temp*temp/2;
    	res.y = pos.y + vel.y*temp + acc.y*temp*temp/2;
    	res.z = pos.z + vel.z*temp + acc.z*temp*temp/2;
    	return res;
	}
    public vcr Velocidad() { return Velocidad(0); }
	public vcr Velocidad(double temp)
	{
    	vcr res = new vcr();

	    res.x = vel.x + acc.x*temp;
    	res.y = vel.y + acc.y*temp;
		res.z = vel.z + acc.z*temp;
    	return res;
	}
    public vcr Aceleracion()
    {
    	return acc;
    }
    public double Radio()
    {
    	return radp;
    }
    public double Masa()
    {
    	return masa;
    }
    public void Mover(double temp)
	{
	    pos.x = pos.x + vel.x*temp + acc.x*temp*temp/2;
    	pos.y = pos.y + vel.y*temp + acc.y*temp*temp/2;
	    pos.z = pos.z + vel.z*temp + acc.z*temp*temp/2;
    	vel.x = vel.x + acc.x*temp;
	    vel.y = vel.y + acc.y*temp;
		vel.z = vel.z + acc.z*temp;
	}
}

// Éste con fuerza externa igual para todas las partículas...
class ControlParticulas
{
    public static int MAXPART = (512);
    public static int MAXMURO = (16);
 	private Partic [] Todas = new Partic[MAXPART];
    private Muro [] Muros = new Muro[MAXMURO];
    private int NumParticulas;
    private int NumMuros;
    private int proxeventoA;
    private int proxeventoB;
    private int ultimoEventoA;
    private int ultimoEventoB;
    private double elastic;
    private vcr ff = new vcr();
	private void ColisionarParticulas(Partic p1, Partic p2)
	{
		// Primero calculamos la direccion del choque
		vcr normal = new vcr();
		double dist;

		normal.x = p2.Posicion().x - p1.Posicion().x;
		normal.y = p2.Posicion().y - p1.Posicion().y;
		normal.z = p2.Posicion().z - p1.Posicion().z;
		dist = Math.Sqrt(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);
		normal.x /= dist;
		normal.y /= dist;
		normal.z /= dist;

		// Ahor proyectamos cada velocidad en esa direccion
		double pr1, pr2, y1, y2, m1dm2;

		pr1 = normal.x*p1.Velocidad().x + normal.y*p1.Velocidad().y
                                                    + normal.z*p1.Velocidad().z;
    	pr2 = normal.x*p2.Velocidad().x + normal.y*p2.Velocidad().y
                                                    + normal.z*p2.Velocidad().z;
	
    	// Despejandolo todo... Estas son las nuevas velocidades en esa dir.
		if(p1.Masa() != p2.Masa())
		{
	    	m1dm2 = p1.Masa()/p2.Masa();

	    	y1 = (2*pr2       + (m1dm2 - 1)*pr1)/(m1dm2 + 1);
	    	y2 = (2*m1dm2*pr1 + (1 - m1dm2)*pr2)/(m1dm2 + 1);
		} else
		{
			y1 = pr2;
			y2 = pr1;
		}
		y1 *= elastic;
		y2 *= elastic;

		// Adapto y1, y2 para que simbolicen la diferencia, y luego proyectamos...
		y1 -= pr1;
		y2 -= pr2;
    	p1.ModificacionForzada(y1*normal.x, y1*normal.y, y1*normal.z);
    	p2.ModificacionForzada(y2*normal.x, y2*normal.y, y2*normal.z);
	}
	private void Mover(double tiempo) // Mueve todo el sistema
	{
    	for(int i = 0; i < NumParticulas; i++)
	        Todas[i].Mover(tiempo);
    	for(int i = 0; i < NumMuros; i++)
	        Muros[i].Mover(tiempo);
	}
    public ControlParticulas(double fzax, double fzay, double fzaz,
                                                         double elasticidad)
    {
        proxeventoA = -1;
        proxeventoB = -1;
		ultimoEventoA = -1;
		ultimoEventoB = -1;
        NumParticulas = 0;
        NumMuros = 0;
        ff.x = fzax;
        ff.y = fzay;
		elastic = elasticidad;
    }
	public void MetodoEmergencia() { Mover(1); } // *****
    public bool AgregarParticula(double px, double py, double pz,
                                 double vx, double vy, double vz,
                                 double r, double m)
	{
		if(NumParticulas == MAXPART)
			return false;
		
    	Partic nueva = new Partic(px, py, pz, vx, vy, vz, r*1.5, m);

    	for(int i = 0; i < NumParticulas; i++)
        	if(nueva.VerificarColision(Todas[i]))
				return false;
		nueva = new Partic(px, py, pz, vx, vy, vz, r, m);
    	nueva.DefinirFuerza(ff.x, ff.y, ff.z);
    	Todas[NumParticulas].CopiarParticula(nueva);
    	NumParticulas++;
    	return true;                    
	}
	public bool AgregarMuro(vcr pto, vcr norm)
	{
		if(NumMuros == MAXMURO)
			return false;
		
	    Muro nuevo = new Muro(pto, norm);

    	for(int i = 0; i < NumParticulas; i++)
        	if(Todas[i].VerificarColision(nuevo))
            	return false;
    	Muros[NumMuros].CopiarMuro(nuevo);
    	NumMuros++;
    	return true;                    
	}
	public void ModificarFuerzas(double nfx, double nfy, double nfz)
	{
    	for(int i = 0; i < NumParticulas; i++)
        	Todas[i].DefinirFuerza(nfx, nfy, nfz);
    	ff.x = nfx;
		ff.y = nfy;
		ff.z = nfz;
	}
	public double EjecutarProxEvento() // Retorna tiempo movido
	{
    	double t = SiguienteEvento();

    	if(t <= 0)
		{
	//		std::cerr << "tiempo indeterminado - " << t << "!!!\n";
			t = 0.1;
        	Mover(t);
			return t;
		}
    	Mover(t);
    	if(proxeventoA >= 0 && proxeventoB >= 0)
		{
	        ColisionarParticulas(Todas[proxeventoA], Todas[proxeventoB]);
			ultimoEventoA = proxeventoA;
			ultimoEventoB = proxeventoB;
		} else
		{   // Los muros estan guardados como -2 - pB = j
        	Todas[proxeventoA].ColisionMuro(Muros[-2 - proxeventoB]);
			ultimoEventoA = proxeventoA;
			ultimoEventoB = proxeventoB;
		}
    	return t;
	}
	public double SiguienteEvento() // Ve próximo evento a ocurrir
	{
    	double mintiempo = -1;

    	proxeventoA = -1;
    	proxeventoB = -1;
    	for(int i = 0; i < NumParticulas; i++)
    	{
    	for(int j = i + 1; j < NumParticulas; j++)
    	{
	        double dvz, dvy, dvx, dz, dy, dx, sumrad, deter, ndv, ndx, dxdv;
        	double tiempo1, tiempo2;

			if(i == ultimoEventoA && j == ultimoEventoB)
		        continue;

   	    	dvx = Todas[i].Velocidad().x - Todas[j].Velocidad().x; // Dv
        	dvy = Todas[i].Velocidad().y - Todas[j].Velocidad().y;
        	dvz = Todas[i].Velocidad().z - Todas[j].Velocidad().z;
        	if(dvx == 0 && dvy == 0 && dvz == 0)
           		continue;
        	dx = Todas[i].Posicion().x - Todas[j].Posicion().x;    // Dx
        	dy = Todas[i].Posicion().y - Todas[j].Posicion().y;
        	dz = Todas[i].Posicion().z - Todas[j].Posicion().z;
        	sumrad = Todas[i].Radio() + Todas[j].Radio();
			dxdv = -(dx*dvx + dy*dvy + dz*dvz); // -Dx·Dv
			ndv = dvx*dvx + dvy*dvy + dvz*dvz;  // |Dv|*|Dv|
			ndx = dx*dx + dy*dy + dz*dz;        // |Dx|*|Dx|
		
        	// Lo siguiente despejando que chocan si estan a distancia r1 + r2
        	deter = (sumrad*sumrad - ndx)*ndv - dxdv*dxdv;
        	if(deter < 0)
	            continue;
			deter = Math.Sqrt(deter);
        	tiempo1 = (dxdv + deter)/ndv;
        	tiempo2 = (dxdv - deter)/ndv;
        	if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
        	{
	            proxeventoA = i;
            	proxeventoB = j;
            	mintiempo = tiempo1;
        	}
        	if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
        	{
	            proxeventoA = i;
            	proxeventoB = j;
            	mintiempo = tiempo2;
        	}
    	} // Eso entre ellas, ahora con los muros
    	for(int j = 0; j < NumMuros; j++)
    	{
			double vn, xn, an, cr;
			double deter1, deter2, tiempo1, tiempo2, tiempo3, tiempo4;
	        
			if(ultimoEventoA == i && ultimoEventoB == (-2 - j))
				continue;
			xn = (Todas[i].Posicion().x - Muros[j].Punto().x)*Muros[j].Normal().x +
		     	 (Todas[i].Posicion().y - Muros[j].Punto().y)*Muros[j].Normal().y +
		     	 (Todas[i].Posicion().z - Muros[j].Punto().z)*Muros[j].Normal().z;
		     	 // (X-P)·Nu   Nu = Normal unit.
	    	vn = -(Todas[i].Velocidad().x*Muros[j].Normal().x +
		       	   Todas[i].Velocidad().y*Muros[j].Normal().y +
		       	   Todas[i].Velocidad().z*Muros[j].Normal().z); // -V·Nu
			an = Todas[i].Aceleracion().x*Muros[j].Normal().x +
		     	 Todas[i].Aceleracion().y*Muros[j].Normal().y +
		     	 Todas[i].Aceleracion().z*Muros[j].Normal().z;  //  A·Nu
			cr = Todas[i].Radio()/Math.Sqrt(3);     // Aunque ud. no lo crea...
			if(an != 0)
			{
			    deter1 = Math.Sqrt(vn*vn - 2*an*(xn + cr));
		    	deter2 = Math.Sqrt(vn*vn - 2*an*(xn - cr));
	
		    	if(deter1 >= 0)
		    	{
				    deter1 = Math.Sqrt(deter1);
			    	tiempo1 = (vn + deter1)/an;
			    	tiempo2 = (vn - deter1)/an;
		        	if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
                	{
	                    proxeventoA = i;
    	                proxeventoB = -2 - j;
        	            mintiempo = tiempo1;
            	    }
	            	if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
    	        	{
        	        	proxeventoA = i;
            	    	proxeventoB = -2 - j;
                		mintiempo = tiempo2;
	            	}
				}
				if(deter2 >= 0)
				{
					deter2 = Math.Sqrt(deter2);
					tiempo3 = (vn + deter2)/an;
					tiempo4 = (vn - deter2)/an;
		    		if(tiempo3 > 0 && (mintiempo <= 0 || mintiempo > tiempo3))
            		{
	                	proxeventoA = i;
	    	            proxeventoB = -2 - j;
    	    	        mintiempo = tiempo3;
        	    	}
            		if(tiempo4 > 0 && (mintiempo <= 0 || mintiempo > tiempo4))
            		{
	                	proxeventoA = i;
    	            	proxeventoB = -2 - j;
        	        	mintiempo = tiempo4;
            		}
				}
			} else if(vn != 0)
			{
				tiempo1 = (xn + cr)/vn;
				tiempo2 = (xn - cr)/vn;
				if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
        	    {
            	    proxeventoA = i;
                	proxeventoB = -2 - j;
	                mintiempo = tiempo1;
    	        }
        	    if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
            	{
	               	proxeventoA = i;
    	           	proxeventoB = -2 - j;
        	       	mintiempo = tiempo2;
            	}
			}
    	}
    	}
	//    if(mintiempo == 0)
	//        mintiempo = -1;
    	return mintiempo;
	}
	/*public Partic operator [] (int i)
	{
		return Todas[i];
	}*/
	public int NumeroParticulas()
	{
    	return NumParticulas;
	}
	public int NumeroMuros()
	{
    	return NumMuros;
	}
}

}


/*

class DA : public Gtk::DrawingArea
{
  public:
	DA(int xsize, int ysize, ControlParticulas &origen) : cp(origen)
    {
		set_size_request(xsize, ysize);
	}
	bool on_expose_event(GdkEventExpose *)
	{
		Glib::RefPtr<Gdk::Window> win = get_window();
  		Glib::RefPtr<Gdk::GC> gc = get_style()->get_black_gc();
		for(int i = 0; i < cp.NumeroParticulas(); i++)
			win->draw_arc(gc, true, (int)cp[i].Posicion().x,
                                    (int)cp[i].Posicion().y,
                      (int)(2*cp[i].Radio()), (int)(2*cp[i].Radio()), 0, 23040);
  		return true;
	}
  private:
	ControlParticulas &cp;
};

class Programita : public Gtk::Window
{
  public:
    Programita(double ntes, double np, double elas) :
           juego(0, 9.8/3200, 0, elas), m_button("Recalcular"), labeltotal("0"),
                            muestra((int)(TAMANO*1.5), (int)(TAMANO*1.5), juego)
    {
		if(np > MAXPART)
			np = MAXPART;
		vcr vert1, vert2, n1, n2, n3;
		
		vert1.x = 0;
		vert1.y = 0;
        vert1.z = 0;
        vert2.x = TAMANO;
		vert2.y = TAMANO;
        vert2.z = TAMANO;
		n1.x = 0;
        n1.y = 0;
        n1.z = 1;
        n2.x = 0;
        n2.y = 1;
        n2.z = 0;
        n3.x = 1;
        n3.y = 0;
        n3.z = 0;
        juego.AgregarMuro(vert1, n1);
        juego.AgregarMuro(vert1, n2);
        juego.AgregarMuro(vert1, n3);
        juego.AgregarMuro(vert2, n1);
        juego.AgregarMuro(vert2, n2);
        juego.AgregarMuro(vert2, n3);
        for(int i = 0; i < np; i++)
        {
            double px, py, pz, vx, vy, vz;

            px = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
            py = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
            pz = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
            vx = 0;
            vy = 0;
			vz = 0;
			
            while(!juego.AgregarParticula(px, py, pz, vx, vy, vz, TAMPART))
			{
			    px = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
                py = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
                pz = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
            }
        }
		tiempoespera = ntes;
        tiempototal = 0;
		tiempoProximoDespliegue = 0;
        set_border_width(10);
        m_button.signal_clicked().connect(
		                     SigC::slot(*this, &Programita::on_button_clicked));
		add(m_Box);
		m_Box.pack_start(labeltotal);
        m_Box.pack_start(m_button);
		m_Box.add(muestra);
		show_all_children();
		Glib::signal_idle().connect(SigC::slot(*this, &Programita::Loop), Glib::PRIORITY_DEFAULT_IDLE);
    }
	~Programita()
	{
	}
  protected:
	ControlParticulas juego;
    double tiempototal;
    bool Loop()
    {
        double t;
		t = juego.EjecutarProxEvento();
		tiempototal += t;
		if(t == 0)
		{
			cerr << "Llama a metodoEmergencia\n";
			juego.MetodoEmergencia();
		}
		if(tiempototal > tiempoProximoDespliegue)
		{
			tiempoProximoDespliegue += tiempoespera;
			on_button_clicked();
		}
		return true;
	}
    virtual void on_button_clicked()
    {
		stringstream s5;

		s5 << tiempototal;
		labeltotal.set_text(s5.str());		
		muestra.queue_draw();
    }
	Gtk::VBox m_Box;
    Gtk::Button m_button;
    Gtk::Label labeltotal;
	DA muestra;
	double tiempoProximoDespliegue;
	double tiempoespera;
};

int main (int argc, char *argv[])
{
    Gtk::Main kit(argc, argv);
	double ntes = 0;
	double np = 150;
	double elas = 1;

	if(argc > 1)
        np = atoi(argv[1]);
    if(argc > 2)
        elas = atof(argv[2]);
    if(argc > 3)
		ntes = atof(argv[3]);

    Programita ventana(ntes, np, elas);

    Gtk::Main::run(ventana);
    return 0;
}*/
