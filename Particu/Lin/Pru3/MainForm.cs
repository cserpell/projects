// project created on 06-09-2003 at 22:01
using System;
using System.Windows.Forms;

namespace MyFormProject 
{
	class MainForm : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.Timer timer;
		private System.Windows.Forms.Button button;
		private System.Windows.Forms.Button cambiarg;
		private System.Windows.Forms.PictureBox muestra;
		private pru3.ControlParticulas juego;
		private double tiempototal;
		private double tiempoProximoDespliegue;
		private double tiempoespera;
		private	System.Drawing.Pen lapiz;
		private static int TAMANO = 200;
		private static int TAMPART = 3;
		public MainForm(double ntes, int np, double elas)
		{
			lapiz = lapiz = new System.Drawing.Pen(System.Drawing.Color.Black);
			juego = new pru3.ControlParticulas(0, 9.8/3200, 0, elas);
			if(np > pru3.ControlParticulas.MAXPART)
				np = pru3.ControlParticulas.MAXPART;
			
			pru3.vcr vert1 = new pru3.vcr();
			pru3.vcr vert2 = new pru3.vcr();
			pru3.vcr n1 = new pru3.vcr();
			pru3.vcr n2 = new pru3.vcr();
			pru3.vcr n3 = new pru3.vcr();
			
			vert1.x = 0;
			vert1.y = 0;
        	vert1.z = 0;
    	    vert2.x = TAMANO;
			vert2.y = TAMANO;
        	vert2.z = TAMANO;
			n1.x = 0;
	        n1.y = 0;
        	n1.z = 1;
    	    n2.x = 0;
	        n2.y = 1;
        	n2.z = 0;
    	    n3.x = 1;
	        n3.y = 0;
        	n3.z = 0;
    	    juego.AgregarMuro(vert1, n1);
	        juego.AgregarMuro(vert1, n2);
        	juego.AgregarMuro(vert1, n3);
    	    juego.AgregarMuro(vert2, n1);
	        juego.AgregarMuro(vert2, n2);
        	juego.AgregarMuro(vert2, n3);
    	    for(int i = 0; i < np; i++)
	        {
	        	Random AA = new Random();
            	double px, py, pz, vx, vy, vz;

        	    px = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
    	        py = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
	            pz = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
            	vx = 2*(AA.NextDouble()-0.5);
    	        vy = 2*(AA.NextDouble()-0.5);
	/*            vz = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
            	vx = 0;
        	    vy = 0;*/
				vz = 0;
			
				pz = TAMANO/2;
	            while(!juego.AgregarParticula(px, py, pz, vx, vy, vz, TAMPART, 1))
				{
				    px = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
        	        py = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
    	            pz = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
					pz = TAMANO/2;
	            }
        	}
			tiempoespera = ntes;
        	tiempototal = 0;
			tiempoProximoDespliegue = 0;
			InitializeComponent();
			muestra.Size = new System.Drawing.Size((int)(TAMANO*1.5), (int)(TAMANO*1.5));
		}
	
		// THIS METHOD IS MAINTAINED BY THE FORM DESIGNER
		// DO NOT EDIT IT MANUALLY! YOUR CHANGES ARE LIKELY TO BE LOST
		void ButtonClick(object sender, System.EventArgs e)
		{
			Double dd = tiempototal;
			
		    label.Text = dd.ToString();
			muestra.Invalidate();
		}
		void TimerTick(object sender, System.EventArgs e)
		{
        	double t;
			t = juego.EjecutarProxEvento();
			tiempototal += t;
			if(t == 0)
			{
				//cerr << "Llama a metodoEmergencia\n";
				juego.MetodoEmergencia();
			}
			if(tiempototal > tiempoProximoDespliegue)
			{
				tiempoProximoDespliegue += tiempoespera;
		    	ButtonClick(sender, e);
			}
		}
		void PictureBoxValidated(object sender, System.EventArgs e)
		{
 		}
		void MuestraPaint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			System.Drawing.Graphics g = e.Graphics;

			for(int i = 0; i < juego.NumeroParticulas(); i++)
				g.DrawEllipse(lapiz, (int)juego.Particu(i).Posicion().x,
                           		     (int)juego.Particu(i).Posicion().y,
                      				 (int)(2*juego.Particu(i).Radio()),
                      				 (int)(2*juego.Particu(i).Radio()));
		}
		
		void CambiargClick(object sender, System.EventArgs e)
		{
			juego.ModificarFuerzas(9.8/3200, 0, 0);
		}
		
		void MainFormResize(object sender, System.EventArgs e)
		{
			
		}
		
		void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.muestra = new System.Windows.Forms.PictureBox();
			this.cambiarg = new System.Windows.Forms.Button();
			this.button = new System.Windows.Forms.Button();
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.label = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// muestra
			// 
			this.muestra.Location = new System.Drawing.Point(32, 80);
			this.muestra.Name = "muestra";
			this.muestra.Size = new System.Drawing.Size(100, 96);
			this.muestra.TabIndex = 1;
			this.muestra.TabStop = false;
			this.muestra.Paint += new System.Windows.Forms.PaintEventHandler(this.MuestraPaint);
			this.muestra.Validated += new System.EventHandler(this.PictureBoxValidated);
			// 
			// cambiarg
			// 
			this.cambiarg.Location = new System.Drawing.Point(32, 40);
			this.cambiarg.Name = "cambiarg";
			this.cambiarg.TabIndex = 3;
			this.cambiarg.Text = "Gravedad";
			this.cambiarg.Click += new System.EventHandler(this.CambiargClick);
			// 
			// button
			// 
			this.button.Location = new System.Drawing.Point(32, 8);
			this.button.Name = "button";
			this.button.TabIndex = 0;
			this.button.Text = "Recalcular";
			this.button.Click += new System.EventHandler(this.ButtonClick);
			// 
			// timer
			// 
			this.timer.Enabled = true;
			this.timer.Interval = 1;
			this.timer.Tick += new System.EventHandler(this.TimerTick);
			// 
			// label
			// 
			this.label.Location = new System.Drawing.Point(120, 8);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(160, 23);
			this.label.TabIndex = 2;
			this.label.Text = "0";
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(328, 325);
			this.Controls.Add(this.cambiarg);
			this.Controls.Add(this.label);
			this.Controls.Add(this.muestra);
			this.Controls.Add(this.button);
			this.Name = "MainForm";
			this.Text = "This is my form";
			this.Resize += new System.EventHandler(this.MainFormResize);
			this.ResumeLayout(false);
		}
			
		[STAThread]
		public static void Main(string[] args)
		{
			double ntes = 0.5;
			int np = 75;
			double elas = 1;

			if(args.Length > 0)
            	np = Int32.Parse(args[0]);
    		if(args.Length > 1)
        		elas = Double.Parse(args[1]);
    		if(args.Length > 2)
				ntes = Double.Parse(args[2]);
			Application.Run(new MainForm(ntes, np, elas));
		}
	}			
}
