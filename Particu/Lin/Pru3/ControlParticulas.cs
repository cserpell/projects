//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.573
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace pru3
{
	using System;
	
	public class ControlParticulas : object
	{
		public ControlParticulas()
		{
			ff = new vcr();
    	 	Todas = new Partic[MAXPART];
    	    Muros = new Muro[MAXMURO];
		}
	    public static int MAXPART = (512);
    	public static int MAXMURO = (16);
	 	private Partic [] Todas;
    	private Muro [] Muros;
	    private int NumParticulas;
    	private int NumMuros;
	    private int proxeventoA;
    	private int proxeventoB;
	    private int ultimoEventoA;
    	private int ultimoEventoB;
	    private double elastic;
    	private vcr ff;
		private void ColisionarParticulas(Partic p1, Partic p2)
		{
			// Primero calculamos la direccion del choque
			double dist;
			vcr normal = new vcr(p2.Posicion().x - p1.Posicion().x,
			                     p2.Posicion().y - p1.Posicion().y,
			                     p2.Posicion().z - p1.Posicion().z);
			
			dist = Math.Sqrt(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);
			if(dist == 0)
				return;
			normal.x /= dist;
			normal.y /= dist;
			normal.z /= dist;

			// Ahor proyectamos cada velocidad en esa direccion
			double pr1, pr2, y1, y2, m1dm2;

			pr1 = normal.x*p1.Velocidad().x + normal.y*p1.Velocidad().y
                                                    + normal.z*p1.Velocidad().z;
   		 	pr2 = normal.x*p2.Velocidad().x + normal.y*p2.Velocidad().y
                                                    + normal.z*p2.Velocidad().z;
	
 			// Despejandolo todo... Estas son las nuevas velocidades en esa dir.
			if(p1.Masa() != p2.Masa())
			{
		    	m1dm2 = p1.Masa()/p2.Masa();

		    	y1 = (2*pr2       + (m1dm2 - 1)*pr1)/(m1dm2 + 1);
		    	y2 = (2*m1dm2*pr1 + (1 - m1dm2)*pr2)/(m1dm2 + 1);
			} else
			{
				y1 = pr2;
				y2 = pr1;
			}
			y1 *= elastic;
			y2 *= elastic;

			// Adapto y1, y2 para que simbolicen la diferencia, y luego proyectamos...
			y1 -= pr1;
			y2 -= pr2;
    		p1.ModificacionForzada(y1*normal.x, y1*normal.y, y1*normal.z);
    		p2.ModificacionForzada(y2*normal.x, y2*normal.y, y2*normal.z);
		}
		private void Mover(double tiempo) // Mueve todo el sistema
		{
	    	for(int i = 0; i < NumParticulas; i++)
	        	Todas[i].Mover(tiempo);
    		for(int i = 0; i < NumMuros; i++)
		        Muros[i].Mover(tiempo);
		}
    	public ControlParticulas(double fzax, double fzay, double fzaz,
                                                         double elasticidad)
    	{
    		ff = new vcr(fzax, fzay, fzaz);
    	 	Todas = new Partic[MAXPART];
    	    Muros = new Muro[MAXMURO];
        	proxeventoA = -1;
        	proxeventoB = -1;
			ultimoEventoA = -1;
			ultimoEventoB = -1;
        	NumParticulas = 0;
        	NumMuros = 0;
			elastic = elasticidad;
    	}
		public void MetodoEmergencia() { Mover(1); } // *****
 		public bool AgregarParticula(double px, double py, double pz,
                                 double vx, double vy, double vz,
                                 double r, double m)
		{
			if(NumParticulas == MAXPART)
				return false;
		
	    	Partic nueva = new Partic(px, py, pz, vx, vy, vz, r*1.5, m);

	    	for(int i = 0; i < NumParticulas; i++)
	        	if(nueva.VerificarColision(Todas[i]))
					return false;
			Todas[NumParticulas] = new Partic(px, py, pz, vx, vy, vz, r, m);
    		Todas[NumParticulas].DefinirFuerza(ff.x, ff.y, ff.z);
    		NumParticulas++;
    		return true;                    
		}
		public bool AgregarMuro(vcr pto, vcr norm)
		{
			if(NumMuros == MAXMURO)
				return false;
		
	    	Muro nuevo = new Muro(pto, norm);

    		for(int i = 0; i < NumParticulas; i++)
        		if(Todas[i].VerificarColision(nuevo))
            		return false;
			Muros[NumMuros] = new Muro(pto, norm);
    		NumMuros++;
    		return true;                    
		}
		public void ModificarFuerzas(double nfx, double nfy, double nfz)
		{
    		for(int i = 0; i < NumParticulas; i++)
        		Todas[i].DefinirFuerza(nfx, nfy, nfz);
    		ff.x = nfx;
			ff.y = nfy;
			ff.z = nfz;
		}
		public double EjecutarProxEvento() // Retorna tiempo movido
		{
    		double t = SiguienteEvento();

    		if(t <= 0)
			{
		//		std::cerr << "tiempo indeterminado - " << t << "!!!\n";
				t = 0.1;
        		Mover(t);
				return t;
			}
    		Mover(t);
    		if(proxeventoA >= 0 && proxeventoB >= 0)
			{
		        ColisionarParticulas(Todas[proxeventoA], Todas[proxeventoB]);
				ultimoEventoA = proxeventoA;
				ultimoEventoB = proxeventoB;
			} else
			{   // Los muros estan guardados como -2 - pB = j
        		Todas[proxeventoA].ColisionMuro(Muros[-2 - proxeventoB]);
				ultimoEventoA = proxeventoA;
				ultimoEventoB = proxeventoB;
			}
    		return t;
		}
		public double SiguienteEvento() // Ve próximo evento a ocurrir
		{
    		double mintiempo = -1;

    		proxeventoA = -1;
	    	proxeventoB = -1;
    		for(int i = 0; i < NumParticulas; i++)
    		{
	    	for(int j = i + 1; j < NumParticulas; j++)
    		{
		        double dvz, dvy, dvx, dz, dy, dx, sumrad, deter, ndv, ndx, dxdv;
	        	double tiempo1, tiempo2;

				if(i == ultimoEventoA && j == ultimoEventoB)
			        continue;

   		    	dvx = Todas[i].Velocidad().x - Todas[j].Velocidad().x; // Dv
	        	dvy = Todas[i].Velocidad().y - Todas[j].Velocidad().y;
        		dvz = Todas[i].Velocidad().z - Todas[j].Velocidad().z;
        		if(dvx == 0 && dvy == 0 && dvz == 0)
    	       		continue;
	        	dx = Todas[i].Posicion().x - Todas[j].Posicion().x;    // Dx
        		dy = Todas[i].Posicion().y - Todas[j].Posicion().y;
        		dz = Todas[i].Posicion().z - Todas[j].Posicion().z;
    	    	sumrad = Todas[i].Radio() + Todas[j].Radio();
				dxdv = -(dx*dvx + dy*dvy + dz*dvz); // -Dx·Dv
				ndv = dvx*dvx + dvy*dvy + dvz*dvz;  // |Dv|*|Dv|
				ndx = dx*dx + dy*dy + dz*dz;        // |Dx|*|Dx|
		
    	    	// Lo siguiente despejando que chocan si estan a distancia r1 + r2
	        	deter = (sumrad*sumrad - ndx)*ndv + dxdv*dxdv;
        		if(deter < 0)
	    	        continue;
				deter = Math.Sqrt(deter);
	        	tiempo1 = (dxdv + deter)/ndv;
        		tiempo2 = (dxdv - deter)/ndv;
        		if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
    	    	{
		            proxeventoA = i;
            		proxeventoB = j;
            		mintiempo = tiempo1;
        		}
    	    	if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
	        	{
	        	    proxeventoA = i;
        	    	proxeventoB = j;
    	        	mintiempo = tiempo2;
	        	}
    		} // Eso entre ellas, ahora con los muros
    		for(int j = 0; j < NumMuros; j++)
    		{
				double vn, xn, an, cr;
				double deter1, deter2, tiempo1, tiempo2, tiempo3, tiempo4;
	        
				if(ultimoEventoA == i && ultimoEventoB == (-2 - j))
					continue;
	    		vn = -(Todas[i].Velocidad().x*Muros[j].Normal().x +
			       	   Todas[i].Velocidad().y*Muros[j].Normal().y +
			       	   Todas[i].Velocidad().z*Muros[j].Normal().z); // -V·Nu
				an = Todas[i].Aceleracion().x*Muros[j].Normal().x +
			     	 Todas[i].Aceleracion().y*Muros[j].Normal().y +
			     	 Todas[i].Aceleracion().z*Muros[j].Normal().z;  //  A·Nu
				if(an != 0)
				{
				xn = (Todas[i].Posicion().x - Muros[j].Punto().x)*Muros[j].Normal().x +
			     	 (Todas[i].Posicion().y - Muros[j].Punto().y)*Muros[j].Normal().y +
			     	 (Todas[i].Posicion().z - Muros[j].Punto().z)*Muros[j].Normal().z;
		     	 // (X-P)·Nu   Nu = Normal unit.
				cr = Todas[i].Radio();     // Aunque ud. no lo crea...
				    deter1 = vn*vn - 2*an*(xn + cr);
			    	deter2 = vn*vn - 2*an*(xn - cr);

			    	if(deter1 >= 0)
			    	{
					    deter1 = Math.Sqrt(deter1);
				    	tiempo1 = (vn + deter1)/an;
				    	tiempo2 = (vn - deter1)/an;
			        	if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
	                	{
	                	    proxeventoA = i;
    	        	        proxeventoB = -2 - j;
        		            mintiempo = tiempo1;
        	    	    }
		            	if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
	    	        	{
        	        		proxeventoA = i;
            	    		proxeventoB = -2 - j;
                			mintiempo = tiempo2;
	        	    	}
					}
					if(deter2 >= 0)
					{
						deter2 = Math.Sqrt(deter2);
						tiempo3 = (vn + deter2)/an;
						tiempo4 = (vn - deter2)/an;
			    		if(tiempo3 > 0 && (mintiempo <= 0 || mintiempo > tiempo3))
	            		{
	                		proxeventoA = i;
	    	        	    proxeventoB = -2 - j;
    	    		        mintiempo = tiempo3;
        		    	}
        	    		if(tiempo4 > 0 && (mintiempo <= 0 || mintiempo > tiempo4))
    	        		{
		                	proxeventoA = i;
    	            		proxeventoB = -2 - j;
        	        		mintiempo = tiempo4;
            			}
					}
				} else if(vn != 0)
				{
				xn = (Todas[i].Posicion().x - Muros[j].Punto().x)*Muros[j].Normal().x +
			     	 (Todas[i].Posicion().y - Muros[j].Punto().y)*Muros[j].Normal().y +
			     	 (Todas[i].Posicion().z - Muros[j].Punto().z)*Muros[j].Normal().z;
		     	 // (X-P)·Nu   Nu = Normal unit.
				cr = Todas[i].Radio();     // Aunque ud. no lo crea...
					tiempo1 = (xn + cr)/vn;
					tiempo2 = (xn - cr)/vn;
					if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
        		    {
    	        	    proxeventoA = i;
	                	proxeventoB = -2 - j;
	            	    mintiempo = tiempo1;
    	    	    }
        		    if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
    	        	{
		               	proxeventoA = i;
    	           		proxeventoB = -2 - j;
        	    	   	mintiempo = tiempo2;
            		}
				}
    		}
	    	}
		//    if(mintiempo == 0)
		//        mintiempo = -1;
    		return mintiempo;
		}
		public Partic Particu(int i)
		{
			return Todas[i];
		}
		public int NumeroParticulas()
		{
    		return NumParticulas;
		}
		public int NumeroMuros()
		{
    		return NumMuros;
		}

	}
}
