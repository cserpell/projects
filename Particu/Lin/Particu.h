#include <math.h>

typedef struct vcr
{
    double x;
    double y;
	double z;
} vcr;

class Muro
{
  private:
	vcr pto;
    vcr normalunit;
    double frec;
    double amplitud;
  public:
    Muro()
    {
		pto.x = 0;
		pto.y = 0;
		pto.z = 0;
		normalunit.x = 0;
		normalunit.y = 1;
		normalunit.z = 0;
		frec = 0;
		amplitud = 0;
    }
    Muro(vcr p, vcr n)
    {
		double normn;
		
		pto.x = p.x;
		pto.y = p.y;
		pto.z = p.z;
		if(n.x == 0 && n.y == 0 && n.z == 0)
			n.y = 1;
		normn = sqrt(n.x*n.x + n.y*n.y + n.z*n.z); // Normal DEBE ser unitaria
		normalunit.x = n.x/normn;
        normalunit.y = n.y/normn;
        normalunit.z = n.z/normn;
        frec = 0;
		amplitud = 0;
    }
	vcr Punto();
	vcr Normal();
    double Interseccion(double tiempo = 0);
    void CopiarMuro(Muro &otro);
	void DefinirMovimiento(double nuevafrec, double nuevaamplit);
    void Mover(double tiempo);
};

class Partic
{
  private:
    vcr pos;
    vcr vel;
    vcr acc;
    double radp;
    double masa;
  public:
	Partic()
    {
	    radp = 0;
		masa = 0;
	}
    Partic(double px, double py, double pz, double vx, double vy, double vz,
                                                         double r, double m = 1)
    {
        pos.x = px;
        pos.y = py;
        pos.z = pz;
        vel.x = vx;
        vel.y = vy;
        vel.z = vz;
        radp = sqrt(r*r);
        masa = sqrt(m*m);
        acc.x = 0;
        acc.y = 0;
		acc.z = 0;
    }
    double Distancia(Partic &otra);  // Desde los centros
    bool VerificarColision(Muro &elmuro);
    bool VerificarColision(Partic &otra);
    void ModificacionForzada(double nuevax, double nuevay, double nuevaz);
    void ColisionMuro(Muro &muro);
    void DefinirFuerza(double fx, double fy, double fz);
    void CopiarParticula(Partic &otra);
	vcr Posicion(double temp = 0);
    vcr Velocidad(double temp = 0);
    vcr Aceleracion();
    double Radio();
    double Masa();
    void Mover(double temp);
};

#define MAXPART (512)
#define MAXMURO (16)

// �ste con fuerza externa igual para todas las part�culas...
class ControlParticulas
{
  private:
    Partic Todas[MAXPART];
    Muro Muros[MAXMURO];
    int NumParticulas;
    int NumMuros;
    int proxeventoA;
    int proxeventoB;
    int ultimoEventoA;
    int ultimoEventoB;
    double elastic;
    vcr ff;
    void ColisionarParticulas(Partic &p1, Partic &p2);
    void Mover(double tiempo);
  public:
	void MetodoEmergencia() { Mover(1); } // *****
    ControlParticulas(double fzax, double fzay, double fzaz,
                                                         double elasticidad = 1)
    {
        proxeventoA = -1;
        proxeventoB = -1;
		ultimoEventoA = -1;
		ultimoEventoB = -1;
        NumParticulas = 0;
        NumMuros = 0;
        ff.x = fzax;
        ff.y = fzay;
		elastic = elasticidad;
    }
    bool AgregarParticula(double px, double py, double pz,
                       double vx, double vy, double vz, double r, double m = 1);
    bool AgregarMuro(vcr pto, vcr norm);
    void ModificarFuerzas(double nfx, double nfy, double nfz);
    double EjecutarProxEvento(); // Retorna tiempo movido
    double SiguienteEvento();
    int NumeroParticulas();
    int NumeroMuros();
	Partic &operator [] (int i);
};
