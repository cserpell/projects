#include <gtkmm.h>
#include <string>
#include <sstream>
#include <iostream>
#include "Particu.h"

#define TAMANO (200)
#define TAMPART (3)

using namespace std;

class DA : public Gtk::DrawingArea
{
  public:
	DA(int xsize, int ysize, ControlParticulas &origen) : cp(origen)
    {
		set_size_request(xsize, ysize);
	}
	bool on_expose_event(GdkEventExpose *)
	{
		Glib::RefPtr<Gdk::Window> win = get_window();
  		Glib::RefPtr<Gdk::GC> gc = get_style()->get_black_gc();
		for(int i = 0; i < cp.NumeroParticulas(); i++)
			win->draw_arc(gc, true, (int)cp[i].Posicion().x,
                                    (int)cp[i].Posicion().y,
                      (int)(2*cp[i].Radio()), (int)(2*cp[i].Radio()), 0, 23040);
  		return true;
	}
  private:
	ControlParticulas &cp;
};

class Programita : public Gtk::Window
{
  public:
    Programita(double ntes, double np, double elas) :
           juego(0, 9.8/3200, 0, elas), m_button("Recalcular"), labeltotal("0"),
                            muestra((int)(TAMANO*1.5), (int)(TAMANO*1.5), juego)
    {
		if(np > MAXPART)
			np = MAXPART;
		vcr vert1, vert2, n1, n2, n3;
		
		vert1.x = 0;
		vert1.y = 0;
        vert1.z = 0;
        vert2.x = TAMANO;
		vert2.y = TAMANO;
        vert2.z = TAMANO;
		n1.x = 0;
        n1.y = 0;
        n1.z = 1;
        n2.x = 0;
        n2.y = 1;
        n2.z = 0;
        n3.x = 1;
        n3.y = 0;
        n3.z = 0;
        juego.AgregarMuro(vert1, n1);
        juego.AgregarMuro(vert1, n2);
        juego.AgregarMuro(vert1, n3);
        juego.AgregarMuro(vert2, n1);
        juego.AgregarMuro(vert2, n2);
        juego.AgregarMuro(vert2, n3);
        for(int i = 0; i < np; i++)
        {
            double px, py, pz, vx, vy, vz;

            px = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
            py = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
            pz = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
            vx = 0;
            vy = 0;
			vz = 0;
			
            while(!juego.AgregarParticula(px, py, pz, vx, vy, vz, TAMPART))
			{
			    px = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
                py = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
                pz = TAMPART + (TAMANO - 2*TAMPART)*(rand()/(1.0+RAND_MAX));
            }
        }
		tiempoespera = ntes;
        tiempototal = 0;
		tiempoProximoDespliegue = 0;
        set_border_width(10);
        m_button.signal_clicked().connect(
		                     SigC::slot(*this, &Programita::on_button_clicked));
		add(m_Box);
		m_Box.pack_start(labeltotal);
        m_Box.pack_start(m_button);
		m_Box.add(muestra);
		show_all_children();
		Glib::signal_idle().connect(SigC::slot(*this, &Programita::Loop), Glib::PRIORITY_DEFAULT_IDLE);
    }
	~Programita()
	{
	}
  protected:
	ControlParticulas juego;
    double tiempototal;
    bool Loop()
    {
        double t;
		t = juego.EjecutarProxEvento();
		tiempototal += t;
		if(t == 0)
		{
			cerr << "Llama a metodoEmergencia\n";
			juego.MetodoEmergencia();
		}
		if(tiempototal > tiempoProximoDespliegue)
		{
			tiempoProximoDespliegue += tiempoespera;
			on_button_clicked();
		}
		return true;
	}
    virtual void on_button_clicked()
    {
		stringstream s5;

		s5 << tiempototal;
		labeltotal.set_text(s5.str());		
		muestra.queue_draw();
    }
	Gtk::VBox m_Box;
    Gtk::Button m_button;
    Gtk::Label labeltotal;
	DA muestra;
	double tiempoProximoDespliegue;
	double tiempoespera;
};

int main (int argc, char *argv[])
{
    Gtk::Main kit(argc, argv);
	double ntes = 0;
	double np = 150;
	double elas = 1;

	if(argc > 1)
        np = atoi(argv[1]);
    if(argc > 2)
        elas = atof(argv[2]);
    if(argc > 3)
		ntes = atof(argv[3]);

    Programita ventana(ntes, np, elas);

    Gtk::Main::run(ventana);
    return 0;
}
