#include "Particu.h"

#include <iostream>

double Partic::Distancia(Partic &otra)  // Desde los centros
{
    double a = pos.x - otra.pos.x;
    double b = pos.y - otra.pos.y;
	double c = pos.z - otra.pos.z;

    return sqrt(a*a + b*b + c*c);
}
bool Partic::VerificarColision(Muro &elmuro)
{
    double distanciaamuro, xx, yy, zz;

    xx = (pos.x - elmuro.Punto().x)*elmuro.Normal().x;
	yy = (pos.y - elmuro.Punto().y)*elmuro.Normal().y;
	zz = (pos.z - elmuro.Punto().z)*elmuro.Normal().z;
	distanciaamuro = xx*xx + yy*yy + zz*zz;
    if(distanciaamuro <= radp*radp)
        return true;
    return false;
}
bool Partic::VerificarColision(Partic &otra)
{
    if(Distancia(otra) <= radp + otra.radp)
        return true;
    return false;
}
void Partic::ColisionMuro(Muro &muro)
{
	double cte = 2*(vel.x*muro.Normal().x + vel.y*muro.Normal().y
                                                       + vel.z*muro.Normal().z);
	
	vel.x -= cte*muro.Normal().x;
    vel.y -= cte*muro.Normal().y;
    vel.z -= cte*muro.Normal().z;
}
void Partic::DefinirFuerza(double fx, double fy, double fz)
{
    acc.x = fx/masa;
    acc.y = fy/masa;
	acc.z = fz/masa;
}
vcr Partic::Posicion(double temp)
{
    vcr res;

    res.x = pos.x + vel.x*temp + acc.x*temp*temp/2;
    res.y = pos.y + vel.y*temp + acc.y*temp*temp/2;
    res.z = pos.z + vel.z*temp + acc.z*temp*temp/2;
    return res;
}
vcr Partic::Velocidad(double temp)
{
    vcr res;

    res.x = vel.x + acc.x*temp;
    res.y = vel.y + acc.y*temp;
	res.z = vel.z + acc.z*temp;
    return res;
}
vcr Partic::Aceleracion()
{
    return acc;
}
double Partic::Radio()
{
    return radp;
}
double Partic::Masa()
{
    return masa;
}
void Partic::Mover(double temp)
{
    pos.x = pos.x + vel.x*temp + acc.x*temp*temp/2;
    pos.y = pos.y + vel.y*temp + acc.y*temp*temp/2;
    pos.z = pos.z + vel.z*temp + acc.z*temp*temp/2;
    vel.x = vel.x + acc.x*temp;
    vel.y = vel.y + acc.y*temp;
	vel.z = vel.z + acc.z*temp;
}
void Partic::ModificacionForzada(double nuevax, double nuevay, double nuevaz)
{   // Se usa para las colisiones entre ellas
    vel.x += nuevax;
    vel.y += nuevay;
	vel.z += nuevaz;
}
void Partic::CopiarParticula(Partic &otra)
{
	pos.x = otra.pos.x;
    pos.y = otra.pos.y;
    pos.z = otra.pos.z;
    vel.x = otra.vel.x;
    vel.y = otra.vel.y;
    vel.z = otra.vel.z;
    radp = otra.radp;
    masa = otra.masa;
    acc.x = otra.acc.x;
    acc.y = otra.acc.y;
	acc.z = otra.acc.z;
}
// -----------------------------------------------------------------------------
vcr Muro::Punto()
{
    return pto;
}
vcr Muro::Normal()
{
    return normalunit;
}
void Muro::DefinirMovimiento(double nuevafrec, double nuevaamplit)
{
    frec = nuevafrec;
    amplitud = nuevaamplit;
}
void Muro::Mover(double tiempo)
{
}
void Muro::CopiarMuro(Muro &otro)
{
	pto.x = otro.pto.x;
	pto.y = otro.pto.y;
	pto.z = otro.pto.z;
	normalunit.x = otro.normalunit.x;
    normalunit.y = otro.normalunit.y;
    normalunit.z = otro.normalunit.z;
    frec = otro.frec;
	amplitud = otro.amplitud;
}
// -----------------------------------------------------------------------------
void ControlParticulas::ColisionarParticulas(Partic &p1, Partic &p2)
{
	// Primero calculamos la direccion del choque
	vcr normal;
	double dist;

	normal.x = p2.Posicion().x - p1.Posicion().x;
	normal.y = p2.Posicion().y - p1.Posicion().y;
	normal.z = p2.Posicion().z - p1.Posicion().z;
	dist = sqrt(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);
	normal.x /= dist;
	normal.y /= dist;
	normal.z /= dist;

	// Ahor proyectamos cada velocidad en esa direccion
	double pr1, pr2, y1, y2, m1dm2;

	pr1 = normal.x*p1.Velocidad().x + normal.y*p1.Velocidad().y
                                                    + normal.z*p1.Velocidad().z;
    pr2 = normal.x*p2.Velocidad().x + normal.y*p2.Velocidad().y
                                                    + normal.z*p2.Velocidad().z;
	
    // Despejandolo todo... Estas son las nuevas velocidades en esa dir.
	if(p1.Masa() != p2.Masa())
	{
	    m1dm2 = p1.Masa()/p2.Masa();

	    y1 = (2*pr2       + (m1dm2 - 1)*pr1)/(m1dm2 + 1);
	    y2 = (2*m1dm2*pr1 + (1 - m1dm2)*pr2)/(m1dm2 + 1);
	} else
	{
		y1 = pr2;
		y2 = pr1;
	}
	y1 *= elastic;
	y2 *= elastic;

	// Adapto y1, y2 para que simbolicen la diferencia, y luego proyectamos...
	y1 -= pr1;
	y2 -= pr2;
    p1.ModificacionForzada(y1*normal.x, y1*normal.y, y1*normal.z);
    p2.ModificacionForzada(y2*normal.x, y2*normal.y, y2*normal.z);
}
void ControlParticulas::Mover(double tiempo) // Mueve todo el sistema
{
    for(int i = 0; i < NumParticulas; i++)
        Todas[i].Mover(tiempo);
    for(int i = 0; i < NumMuros; i++)
        Muros[i].Mover(tiempo);
}
bool ControlParticulas::AgregarParticula(double px, double py, double pz,
                            double vx, double vy, double vz, double r, double m)
{
    Partic *nueva = new Partic(px, py, pz, vx, vy, vz, r*1.5, m);

    for(int i = 0; i < NumParticulas; i++)
        if(nueva->VerificarColision(Todas[i]))
		{
			delete nueva;
            return false;
		}
	delete nueva;
    nueva = new Partic(px, py, pz, vx, vy, vz, r, m);
    nueva->DefinirFuerza(ff.x, ff.y, ff.z);
    Todas[NumParticulas].CopiarParticula(*nueva);
    NumParticulas++;
    return true;                    
}
bool ControlParticulas::AgregarMuro(vcr pto, vcr norm)
{
    Muro *nuevo = new Muro(pto, norm);

    for(int i = 0; i < NumParticulas; i++)
        if(Todas[i].VerificarColision(*nuevo))
		{
			delete nuevo;
            return false;
		}
    Muros[NumMuros].CopiarMuro(*nuevo);
    NumMuros++;
    return true;                    
}
void ControlParticulas::ModificarFuerzas(double nfx, double nfy, double nfz)
{
    for(int i = 0; i < NumParticulas; i++)
        Todas[i].DefinirFuerza(nfx, nfy, nfz);
    ff.x = nfx;
	ff.y = nfy;
	ff.z = nfz;
}
double ControlParticulas::EjecutarProxEvento() // Retorna tiempo movido
{
    double t = SiguienteEvento();

    if(t <= 0)
	{
		std::cerr << "tiempo indeterminado - " << t << "!!!\n";
		t = 0.1;
        Mover(t);
		return t;
	}
    Mover(t);
    if(proxeventoA >= 0 && proxeventoB >= 0)
	{
        ColisionarParticulas(Todas[proxeventoA], Todas[proxeventoB]);
		ultimoEventoA = proxeventoA;
		ultimoEventoB = proxeventoB;
	} else
	{   // Los muros estan guardados como -2 - pB = j
        Todas[proxeventoA].ColisionMuro(Muros[-2 - proxeventoB]);
		ultimoEventoA = proxeventoA;
		ultimoEventoB = proxeventoB;
	}
    return t;
}
double ControlParticulas::SiguienteEvento() // Ve pr�ximo evento a ocurrir
{
    double mintiempo = -1;

    proxeventoA = -1;
    proxeventoB = -1;
    for(int i = 0; i < NumParticulas; i++)
    {
    for(int j = i + 1; j < NumParticulas; j++)
    {
        double dvz, dvy, dvx, dz, dy, dx, sumrad, deter, ndv, ndx, dxdv;
        double tiempo1, tiempo2;

		if(i == ultimoEventoA && j == ultimoEventoB)
	        continue;

        dvx = Todas[i].Velocidad().x - Todas[j].Velocidad().x; // Dv
        dvy = Todas[i].Velocidad().y - Todas[j].Velocidad().y;
        dvz = Todas[i].Velocidad().z - Todas[j].Velocidad().z;
        if(dvx == 0 && dvy == 0 && dvz == 0)
            continue;
        dx = Todas[i].Posicion().x - Todas[j].Posicion().x;    // Dx
        dy = Todas[i].Posicion().y - Todas[j].Posicion().y;
        dz = Todas[i].Posicion().z - Todas[j].Posicion().z;
        sumrad = Todas[i].Radio() + Todas[j].Radio();
		dxdv = -(dx*dvx + dy*dvy + dz*dvz); // -Dx�Dv
		ndv = dvx*dvx + dvy*dvy + dvz*dvz;  // |Dv|*|Dv|
		ndx = dx*dx + dy*dy + dz*dz;        // |Dx|*|Dx|
		
        // Lo siguiente despejando que chocan si estan a distancia r1 + r2
        deter = (sumrad*sumrad - ndx)*ndv - dxdv*dxdv;
        if(deter < 0)
            continue;
		deter = sqrt(deter);
        tiempo1 = (dxdv + deter)/ndv;
        tiempo2 = (dxdv - deter)/ndv;
        if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
        {
            proxeventoA = i;
            proxeventoB = j;
            mintiempo = tiempo1;
        }
        if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
        {
            proxeventoA = i;
            proxeventoB = j;
            mintiempo = tiempo2;
        }
    } // Eso entre ellas, ahora con los muros
    for(int j = 0; j < NumMuros; j++)
    {
		double vn, xn, an, cr;
		double deter1, deter2, tiempo1, tiempo2, tiempo3, tiempo4;
        
		if(ultimoEventoA == i && ultimoEventoB == (-2 - j))
			continue;
		xn = (Todas[i].Posicion().x - Muros[j].Punto().x)*Muros[j].Normal().x +
		     (Todas[i].Posicion().y - Muros[j].Punto().y)*Muros[j].Normal().y +
		     (Todas[i].Posicion().z - Muros[j].Punto().z)*Muros[j].Normal().z;
		     // (X-P)�Nu   Nu = Normal unit.
	    vn = -(Todas[i].Velocidad().x*Muros[j].Normal().x +
		       Todas[i].Velocidad().y*Muros[j].Normal().y +
		       Todas[i].Velocidad().z*Muros[j].Normal().z); // -V�Nu
		an = Todas[i].Aceleracion().x*Muros[j].Normal().x +
		     Todas[i].Aceleracion().y*Muros[j].Normal().y +
		     Todas[i].Aceleracion().z*Muros[j].Normal().z;  //  A�Nu
		cr = Todas[i].Radio()/sqrt(3);     // Aunque ud. no lo crea...
		if(an != 0)
		{
		    deter1 = sqrt(vn*vn - 2*an*(xn + cr));
		    deter2 = sqrt(vn*vn - 2*an*(xn - cr));

		    if(deter1 >= 0)
		    {
			    deter1 = sqrt(deter1);
			    tiempo1 = (vn + deter1)/an;
			    tiempo2 = (vn - deter1)/an;
		        if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo1;
                }
            	if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
            	{
                	proxeventoA = i;
                	proxeventoB = -2 - j;
                	mintiempo = tiempo2;
            	}
			}
			if(deter2 >= 0)
			{
				deter2 = sqrt(deter2);
				tiempo3 = (vn + deter2)/an;
				tiempo4 = (vn - deter2)/an;
		    	if(tiempo3 > 0 && (mintiempo <= 0 || mintiempo > tiempo3))
            	{
	                proxeventoA = i;
    	            proxeventoB = -2 - j;
        	        mintiempo = tiempo3;
            	}
            	if(tiempo4 > 0 && (mintiempo <= 0 || mintiempo > tiempo4))
            	{
                	proxeventoA = i;
                	proxeventoB = -2 - j;
                	mintiempo = tiempo4;
            	}
			}
		} else if(vn != 0)
		{
			tiempo1 = (xn + cr)/vn;
			tiempo2 = (xn - cr)/vn;
			if(tiempo1 > 0 && (mintiempo <= 0 || mintiempo > tiempo1))
            {
                proxeventoA = i;
                proxeventoB = -2 - j;
                mintiempo = tiempo1;
            }
            if(tiempo2 > 0 && (mintiempo <= 0 || mintiempo > tiempo2))
            {
               	proxeventoA = i;
               	proxeventoB = -2 - j;
               	mintiempo = tiempo2;
            }
		}
    }
    }
//    if(mintiempo == 0)
//        mintiempo = -1;
    return mintiempo;
}
Partic &ControlParticulas::operator [] (int i)
{
	return Todas[i];
}
int ControlParticulas::NumeroParticulas()
{
    return NumParticulas;
}
int ControlParticulas::NumeroMuros()
{
    return NumMuros;
}
