// project created on 06-09-2003 at 15:53
using System;
using System.Windows.Forms;

namespace MyFormProject 
{
	class MainForm : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.PictureBox muestra;
		private System.Windows.Forms.Timer timer;
		private System.Windows.Forms.Button m_button;
		private System.Windows.Forms.Label labeltotal;
		private prueba2.ControlParticulas juego;
		private static int TAMANO = 200;
		private static int TAMPART = 3;
		private double tiempototal;
		private double tiempoProximoDespliegue;
		private double tiempoespera;
		public MainForm(double ntes, int np, double elas)
		{
			juego = new prueba2.ControlParticulas(0, 9.8/3200, 0, elas);
			if(np > prueba2.ControlParticulas.MAXPART)
				np = prueba2.ControlParticulas.MAXPART;
			prueba2.vcr vert1 = new prueba2.vcr();
			prueba2.vcr vert2 = new prueba2.vcr();
			prueba2.vcr n1 = new prueba2.vcr();
			prueba2.vcr n2 = new prueba2.vcr();
			prueba2.vcr n3 = new prueba2.vcr();
		
			vert1.x = 0;
			vert1.y = 0;
        	vert1.z = 0;
    	    vert2.x = TAMANO;
			vert2.y = TAMANO;
        	vert2.z = TAMANO;
			n1.x = 0;
	        n1.y = 0;
        	n1.z = 1;
    	    n2.x = 0;
	        n2.y = 1;
        	n2.z = 0;
    	    n3.x = 1;
	        n3.y = 0;
        	n3.z = 0;
    	    juego.AgregarMuro(vert1, n1);
	        juego.AgregarMuro(vert1, n2);
        	juego.AgregarMuro(vert1, n3);
    	    juego.AgregarMuro(vert2, n1);
	        juego.AgregarMuro(vert2, n2);
        	juego.AgregarMuro(vert2, n3);
    	    for(int i = 0; i < np; i++)
	        {
	        	Random AA = new Random();
            	double px, py, pz, vx, vy, vz;

        	    px = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
    	        py = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
	            pz = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
            	vx = 0;
        	    vy = 0;
				vz = 0;
			
	            while(!juego.AgregarParticula(px, py, pz, vx, vy, vz, TAMPART, 1))
				{
				    px = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
        	        py = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
    	            pz = TAMPART + (TAMANO - 2*TAMPART)*AA.NextDouble();
	            }
        	}
			tiempoespera = ntes;
        	tiempototal = 0;
			tiempoProximoDespliegue = 0;
			InitializeComponent();
			muestra.Size = new System.Drawing.Size((int)(TAMANO*1.5), (int)(TAMANO*1.5));
		}
	
		// THIS METHOD IS MAINTAINED BY THE FORM DESIGNER
		// DO NOT EDIT IT MANUALLY! YOUR CHANGES ARE LIKELY TO BE LOST
		void M_buttonClick(object sender, System.EventArgs e)
		{
			string s5;
			Double dd = (tiempototal);
			
		    s5 = dd.ToString();
			labeltotal.Text = s5;
		    //MuestraPaint(sender);	
		}
		void MuestraClick(object sender, System.EventArgs e)
		{
			
		}
		void MuestraPaint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			for(int i = 0; i < juego.NumeroParticulas(); i++)
		;/*	muestra.draw_arc((int)cp[i].Posicion().x,
                             (int)cp[i].Posicion().y,
                      (int)(2*cp[i].Radio()), (int)(2*cp[i].Radio()), 0, 23040);
		*/}
		public void Loop(object sender, System.EventArgs e)
    	{
        	double t;
			t = juego.EjecutarProxEvento();
			tiempototal += t;
			if(t == 0)
			{
				//cerr << "Llama a metodoEmergencia\n";
				juego.MetodoEmergencia();
			}
			if(tiempototal > tiempoProximoDespliegue)
			{
				tiempoProximoDespliegue += tiempoespera;
		    	M_buttonClick(sender, e);
			}
		}
		
		void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.labeltotal = new System.Windows.Forms.Label();
			this.m_button = new System.Windows.Forms.Button();
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.muestra = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// labeltotal
			// 
			this.labeltotal.Location = new System.Drawing.Point(96, 16);
			this.labeltotal.Name = "labeltotal";
			this.labeltotal.Size = new System.Drawing.Size(16, 16);
			this.labeltotal.TabIndex = 2;
			this.labeltotal.Text = "0";
			// 
			// m_button
			// 
			this.m_button.Location = new System.Drawing.Point(72, 40);
			this.m_button.Name = "m_button";
			this.m_button.TabIndex = 1;
			this.m_button.Text = "Recalcular";
			this.m_button.Click += new System.EventHandler(this.M_buttonClick);
			// 
			// timer
			// 
			this.timer.Enabled = true;
			this.timer.Interval = 1;
			this.timer.Tick += new System.EventHandler(this.Loop);
			// 
			// muestra
			// 
			this.muestra.Location = new System.Drawing.Point(48, 96);
			this.muestra.Name = "muestra";
			this.muestra.Size = new System.Drawing.Size(200, 168);
			this.muestra.TabIndex = 0;
			this.muestra.TabStop = false;
			this.muestra.Click += new System.EventHandler(this.MuestraClick);
			this.muestra.Paint += new System.Windows.Forms.PaintEventHandler(this.MuestraPaint);
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(312, 301);
			this.Controls.Add(this.labeltotal);
			this.Controls.Add(this.m_button);
			this.Controls.Add(this.muestra);
			this.Name = "MainForm";
			this.Text = "This is my form";
			this.ResumeLayout(false);
		}
			
		[STAThread]
		public static void Main(string[] args)
		{
			double ntes = 0;
			int np = 150;
			double elas = 1;

			if(args[1] != null)
            	np = Int32.Parse(args[1]);
    		if(args[2] != null)
        		elas = Double.Parse(args[2]);
    		if(args[3] != null)
				ntes = Double.Parse(args[3]);
			Application.Run(new MainForm(ntes, np, elas));
		}
	}			
}
