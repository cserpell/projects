#include <iostream>
#include "Particu.h"

using namespace std;

int main(int argc, char *argv[])
{
    ControlParticulas *juego = new ControlParticulas(0, -9.8/3200);
    double tiempototal;
    
    for(int i = 0; i < 4; i++)
    {
        double px, py, vx, vy;

        px = 5*(i + 1);
        py = 5*(i + 1);
        vx = i;
        vy = i + 1;
        juego->AgregarParticula(px, py, vx, vy, 3);
    }
    juego->AgregarMuro(false, 0, 0);
    juego->AgregarMuro(true, 0);
    juego->AgregarMuro(false, 50, 0);
    juego->AgregarMuro(true, 50);
    tiempototal = 0;
    while(true)
    {
        char c;

        cout << juego->Part(0).Posicion().x << "\t" <<
                                            juego->Part(0).Posicion().y << endl;
        cout << juego->Part(1).Posicion().x << "\t" <<
                                            juego->Part(1).Posicion().y << endl;
        cout << juego->Part(2).Posicion().x << "\t" <<
                                            juego->Part(2).Posicion().y << endl;
        cout << juego->Part(3).Posicion().x << "\t" <<
                                            juego->Part(3).Posicion().y;
        cout << "'s' para salir: ";
        cin >> c;
        if(c == 's')
             break;
        tiempototal += juego->EjecutarProxEvento();
    }
    cout << "Tiempo total: " << tiempototal << endl;
    return 0;
}

