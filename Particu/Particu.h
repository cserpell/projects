#include <math.h>

typedef struct vcr
{
    double x;
    double y;
} vcr;

class Muro
{
  private:
    bool vertical;
    double ang;
    double intery;
    double interorig;
    double frec;
    double amplitud;
//    double estactual;
  public:
    Muro(bool vtcal, double intey, double angulo = M_PI_2)
    {
        ang = angulo;
        intery = intey;
        interorig = intey;
        vertical = vtcal;
        frec = 0;
        amplitud = 0;
//        estactual = 0;
    }
    bool Vertical();
    double Angulo();
    double Interseccion(double tiempo = 0);
    void DefinirMovimiento(double nuevafrec, double nuevaamplit);
    void Mover(double tiempo);
};

class Partic
{
  private:
    vcr pos;
    vcr vel;
    vcr fza;
    double radp;
    double masa;
  public:
    Partic(double px, double py, double vx, double vy, double r, double m = 1)
    {
        pos.x = px;
        pos.y = py;
        vel.x = vx;
        vel.y = vy;
        radp = fabs(r);
        masa = fabs(m);
        fza.x = 0;
        fza.y = 0;
    }
    double Distancia(Partic *otra);  // Desde los centros
    double Angulo(Partic *otra);
    bool VerificarColision(Muro *elmuro);
    bool VerificarColision(Partic *otra);
    void ModificacionForzada(double nuevax, double nuevay);
    void ColisionMuro(double angulomuro);
    void DefinirFuerza(double fx, double fy);
    vcr Posicion(double temp = 0);
    vcr Velocidad(double temp = 0);
    vcr Aceleracion();
    double Radio();
    double Masa();
    void Mover(double temp);
};

#define MAXPART (256)
#define MAXMURO (16)

// �ste con fuerza externa igual para todas las part�culas...
class ControlParticulas
{
  private:
    Partic *Todas[MAXPART];
    Muro *Muros[MAXMURO];
    int NumParticulas;
    int NumMuros;
    int proxeventoA;
    int proxeventoB;
    vcr ff;
    void ColisionarPaticulas(Partic *p1, Partic *p2);
    void Mover(double tiempo);
  public:
    ControlParticulas(double fzax, double fzay)
    {
        proxeventoA = -1;
        proxeventoB = -1;
        NumParticulas = 0;
        NumMuros = 0;
        ff.x = fzax;
        ff.y = fzay;
    }
    bool AgregarParticula(double px, double py, double vx, double vy, double r,
                                                                  double m = 1);
    bool AgregarMuro(bool vcal, double intey, double ang = M_PI_2);
    void ModificarFuerzas(double nfx, double nfy);
    double EjecutarProxEvento(); // Retorna tiempo movido
    double SiguienteEvento();
    int NumeroParticulas();
    int NumeroMuros();
    Partic Part(int i); // La retorna solo para verla, no para modificarla
};

class Nodo
{
  public:
    Nodo *izq;
    Nodo *der;
    Nodo *contenido;
    int valor;
    double tiempo;
    Nodo(int val, double tie, Nodo *nizq, Nodo *nder, Nodo *cont)
    {
        valor = val;
        tiempo = tie;
        izq = nizq;
        der = nder;
        contenido = cont;
    }
    void Actualizar()
    {
        if(izq == null && der == null)
            return;
        if(izq == null)
        {
            der.Actualizar();
            tiempo = der.tiempo;
            valor = der.valor;
            return;
        }
        if(der == null)
        {
            izq.Actualizar();
            tiempo = izq.tiempo;
            valor = izq.valor;
            return;
        }
        izq.Actualizar();
        der.Actualizar();
        if(izq.tiempo < der.tiempo && izq.tiempo != -1)
        {
            tiempo = izq.tiempo;
            valor = izq.valor;
            return;
        }
        tiempo = der.tiempo;
        valor = der.valor;
    }
};

class ListaEventos
{
  private:
    Nodo *inicial;
    Nodo *particulas[MAXPART];
    int NumParticulas;
  public:
    ListaEventos(int cantidadpartic)
    {
        for(int i = 0; i < cantidadpartic; i++)
            particulas[MAXPART] = new Nodo(i, -1, null, null);
    }
    void Actualizar()
    {
        inicial.Actualizar();
    }
};

