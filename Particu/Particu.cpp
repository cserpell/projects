#include "Particu.h"

double Partic::Distancia(Partic *otra)  // Desde los centros
{
    double a = pos.x - otra->pos.x;
    double b = pos.y - otra->pos.y;

    return sqrt(a*a + b*b);
}
double Partic::Angulo(Partic *otra)    // Desde el eje x
{
    return atan2((otra->pos.y - pos.y),(otra->pos.x - pos.x));
}
bool Partic::VerificarColision(Muro *elmuro)
{
    double distanciaamuro;

    if(!elmuro->Vertical())
    {
        double m;
        
        m = tan(elmuro->Angulo());
        distanciaamuro = fabs(m*pos.x - pos.y + elmuro->Interseccion())
                                                                 /sqrt(1 + m*m);
    } else
        distanciaamuro = fabs(pos.y - elmuro->Interseccion());
    if(distanciaamuro <= radp)
        return true;
    return false;
}
bool Partic::VerificarColision(Partic *otra)
{
    if(Distancia(otra) <= radp + otra->radp)
        return true;
    return false;
}
void Partic::ColisionMuro(double angulomuro)
{
    vel.y -= 2*vel.y*cos(angulomuro);
    vel.x -= 2*vel.x*sin(angulomuro);
}
void Partic::DefinirFuerza(double fx, double fy)
{
    fza.x = fx;
    fza.y = fy;
}
vcr Partic::Posicion(double temp)
{
    vcr res;

    res.x = pos.x + vel.x*temp + fza.x*temp*temp/(2*masa);
    res.y = pos.y + vel.y*temp + fza.y*temp*temp/(2*masa);
    return res;
}
vcr Partic::Velocidad(double temp)
{
    vcr res;

    res.x = vel.x + fza.x*temp/masa;
    res.y = vel.y + fza.y*temp/masa;
    return res;
}
vcr Partic::Aceleracion()
{
    vcr res;
    
    res.x = fza.x/masa;
    res.y = fza.y/masa;
    return res;
}
double Partic::Radio()
{
    return radp;
}
double Partic::Masa()
{
    return masa;
}
void Partic::Mover(double temp)
{
    pos.x = pos.x + vel.x*temp + fza.x*temp*temp/(2*masa);
    pos.y = pos.y + vel.y*temp + fza.y*temp*temp/(2*masa);
    vel.x = vel.x + fza.x*temp/masa;
    vel.y = vel.y + fza.y*temp/masa;
}
void Partic::ModificacionForzada(double nuevax, double nuevay)
{   // Se usa para las colisiones entre ellas
    vel.x += nuevax;
    vel.y += nuevay;
}
// -----------------------------------------------------------------------------
bool Muro::Vertical()
{
    return vertical;
}
double Muro::Angulo()
{
    return ang;
}
double Muro::Interseccion(double tiempo)
{
    return intery;
}
void Muro::DefinirMovimiento(double nuevafrec, double nuevaamplit)
{
    frec = nuevafrec;
    amplitud = nuevaamplit;
//    estactual = 0;
}
void Muro::Mover(double tiempo)
{
    intery = interorig + amplitud*sin(frec*tiempo);
}
// -----------------------------------------------------------------------------
void ControlParticulas::ColisionarPaticulas(Partic *p1, Partic *p2)
{
    double angulo = p1->Angulo(p2);
    double v1i, v2i, m1, m2, v1f, v2f;

    v1i = p1->Velocidad().y*sin(angulo) + p1->Velocidad().x*cos(angulo);
    v2i = p2->Velocidad().y*sin(angulo) + p2->Velocidad().x*cos(angulo);
    m1 = p1->Masa();
    m2 = p2->Masa();

    v1f = (m1*v1i - v1i*m2 + 2*m2*v2i)/(m1 + m2);
    v2f = (m2*v2i + 2*m1*v1i - v2i*m1)/(m1 + m2);

    p1->ModificacionForzada((v1f - v1i)*cos(angulo), (v1f - v1i)*sin(angulo));
    p2->ModificacionForzada((v2f - v2i)*cos(angulo), (v2f - v2i)*sin(angulo));
}
void ControlParticulas::Mover(double tiempo) // Mueve todo el sistema
{
    for(int i = 0; i < NumParticulas; i++)
        Todas[i]->Mover(tiempo);
    for(int i = 0; i < NumMuros; i++)
        Muros[i]->Mover(tiempo);
}
bool ControlParticulas::AgregarParticula(double px, double py, double vx,
                                                  double vy, double r, double m)
{
    Partic *nueva = new Partic(px, py, vx, vy, r, m);

    for(int i = 0; i < NumParticulas; i++)
        if(nueva->VerificarColision(Todas[i]))
            return false;
    nueva->DefinirFuerza(ff.x, ff.y);
    Todas[NumParticulas] = nueva;    
    NumParticulas++;
    return true;                    
}
bool ControlParticulas::AgregarMuro(bool vcal, double intey, double ang)
{
    Muro *nuevo = new Muro(vcal, intey, ang);

    for(int i = 0; i < NumParticulas; i++)
        if(Todas[i]->VerificarColision(nuevo))
            return false;
    Muros[NumMuros] = nuevo;    
    NumMuros++;
    return true;                    
}
void ControlParticulas::ModificarFuerzas(double nfx, double nfy)
{
    for(int i = 0; i < NumParticulas; i++)
        Todas[i]->DefinirFuerza(nfx, nfy);
}
double ControlParticulas::EjecutarProxEvento() // Retorna tiempo movido
{
    double t = SiguienteEvento();

    if(t == -1)
        return -1;
    Mover(t);
    if(proxeventoA >= 0 && proxeventoB >= 0)
        ColisionarPaticulas(Todas[proxeventoA], Todas[proxeventoB]);
    else
        Todas[proxeventoA]->ColisionMuro(Muros[-2 - proxeventoB]->Angulo());
    Mover(1e-12);      // *****
    return t;
}
double ControlParticulas::SiguienteEvento() // Ve pr�ximo evento a ocurrir
{
    double mintiempo = -1;

    proxeventoA = -1;
    proxeventoB = -1;
    for(int i = 0; i < NumParticulas; i++)
    {
    for(int j = i + 1; j < NumParticulas; j++)
    {
        double dvy, dvx, dy, dx, sumrad, deter, ladoder, ladoizq, denom;
        double tiempo1, tiempo2;

        dvx = Todas[i]->Velocidad().x - Todas[j]->Velocidad().x;
        dvy = Todas[i]->Velocidad().y - Todas[j]->Velocidad().y;
        if(dvy == 0 && dvx == 0)
            continue;
        dx = Todas[i]->Posicion().x - Todas[j]->Posicion().x;
        dy = Todas[i]->Posicion().y - Todas[j]->Posicion().y;
        sumrad = Todas[i]->Radio() + Todas[j]->Radio();
        // Lo siguiente despejando que chocan si estan a distancia r1 + r2
        denom = dy*dvx - dx*dvy;
        deter = sumrad*sumrad*(dvy*dvy + dvx*dvx) - denom*denom;
        if(deter < 0)
            continue;
        ladoizq = -(dvy*dy + dvx*dx);
        ladoder = sqrt(deter);
        denom = dvy*dvy + dvx*dvx;
        tiempo1 = (ladoizq + ladoder)/denom;
        if(tiempo1 >= 0 && (mintiempo < 0 || mintiempo > tiempo1))
        {
            proxeventoA = i;
            proxeventoB = j;
            mintiempo = tiempo1;
        }
        tiempo2 = (ladoizq - ladoder)/denom;
        if(tiempo2 >= 0 && (mintiempo < 0 || mintiempo > tiempo2))
        {
            proxeventoA = i;
            proxeventoB = j;
            mintiempo = tiempo2;
        }
    } // Eso entre ellas, ahora con los muros
    for(int j = 0; j < NumMuros; j++)
    {
        if(!Muros[j]->Vertical())
        {
            double m, da, dv, dx, deter1, deter2, ladoizq, denom;

            m = tan(Muros[j]->Angulo());
            da = m*Todas[i]->Aceleracion().x - Todas[i]->Aceleracion().y;
            denom = sqrt(1 + m*m);
            dv = m*Todas[i]->Velocidad().x - Todas[i]->Velocidad().y;
            dx = m*Todas[i]->Posicion().x - Todas[i]->Posicion().y;
            if(da == 0)
            {
                double tiempo1, tiempo2;
                
                if(dv == 0)
                    continue;
                tiempo1 = ( Todas[i]->Radio()*denom - Muros[j]->Interseccion()
                                                                       - dx)/dv;
                tiempo2 = (-Todas[i]->Radio()*denom - Muros[j]->Interseccion()
                                                                       - dx)/dv;
                if(tiempo1 >= 0 && (mintiempo < 0 || mintiempo > tiempo1))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo1;
                }
                if(tiempo2 >= 0 && (mintiempo < 0 || mintiempo > tiempo2))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo2;
                }
                continue;
            }
            ladoizq = -dv/da;
            deter1 = dv*dv + 2*da*(-Todas[i]->Radio()*denom
                                               - Muros[j]->Interseccion() - dx);
            if(deter1 >= 0)
            {
                double ladoder, tiempo1, tiempo2;

                ladoder = sqrt(deter1)/da;
                tiempo1 = ladoizq + ladoder;
                tiempo2 = ladoizq - ladoder;
                if(tiempo1 >= 0 && (mintiempo < 0 || mintiempo > tiempo1))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo1;
                }
                if(tiempo2 >= 0 && (mintiempo < 0 || mintiempo > tiempo2))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo2;
                }
            }
            deter2 = dv*dv + 2*da*( Todas[i]->Radio()*denom
                                               - Muros[j]->Interseccion() - dx);
            if(deter2 >= 0)
            {
                double ladoder, tiempo1, tiempo2;

                ladoder = sqrt(deter2)/da;
                tiempo1 = ladoizq + ladoder;
                tiempo2 = ladoizq - ladoder;
                if(tiempo1 >= 0 && (mintiempo < 0 || mintiempo > tiempo1))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo1;
                }
                if(tiempo2 >= 0 && (mintiempo < 0 || mintiempo > tiempo2))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo2;
                }
            }
        } else  // Muro vertical
        {
            double ladoizq, deter1, deter2;
            
            if(Todas[i]->Aceleracion().x == 0)
            {
                double tiempo1, tiempo2;

                if(Todas[i]->Velocidad().x == 0)
                    continue;
                tiempo1 = (Muros[j]->Interseccion() + Todas[i]->Radio()
                              - Todas[i]->Posicion().x)/Todas[i]->Velocidad().x;
                tiempo2 = (Muros[j]->Interseccion() - Todas[i]->Radio()
                              - Todas[i]->Posicion().x)/Todas[i]->Velocidad().x;
                if(tiempo1 >= 0 && (mintiempo < 0 || mintiempo > tiempo1))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo1;
                }
                if(tiempo2 >= 0 && (mintiempo < 0 || mintiempo > tiempo2))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo2;
                }
                continue;
            }
            ladoizq = -Todas[i]->Velocidad().x/Todas[i]->Aceleracion().x;
            deter1 = Todas[i]->Velocidad().x*Todas[i]->Velocidad().x
                     - 2*Todas[i]->Aceleracion().x*(Todas[i]->Posicion().x
                                - Muros[j]->Interseccion() + Todas[i]->Radio());
            if(deter1 >= 0)
            {
                double ladoder, tiempo1, tiempo2;

                ladoder = sqrt(deter1)/Todas[i]->Aceleracion().x;
                tiempo1 = ladoizq + ladoder;
                tiempo2 = ladoizq - ladoder;
                if(tiempo1 >= 0 && (mintiempo < 0 || mintiempo > tiempo1))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo1;
                }
                if(tiempo2 >= 0 && (mintiempo < 0 || mintiempo > tiempo2))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo2;
                }
            }
            deter2 = Todas[i]->Velocidad().x*Todas[i]->Velocidad().x
                     - 2*Todas[i]->Aceleracion().x*(Todas[i]->Posicion().x
                                - Muros[j]->Interseccion() + Todas[i]->Radio());
            if(deter2 >= 0)
            {
                double ladoder, tiempo1, tiempo2;

                ladoder = sqrt(deter2)/Todas[i]->Aceleracion().x;
                tiempo1 = ladoizq + ladoder;
                tiempo2 = ladoizq - ladoder;
                if(tiempo1 >= 0 && (mintiempo < 0 || mintiempo > tiempo1))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo1;
                }
                if(tiempo2 >= 0 && (mintiempo < 0 || mintiempo > tiempo2))
                {
                    proxeventoA = i;
                    proxeventoB = -2 - j;
                    mintiempo = tiempo2;
                }
            }
        }
    }
    }
    if(mintiempo == 0)
        mintiempo = -1;
    return mintiempo;
}
Partic ControlParticulas::Part(int i)
{
    return *(Todas[i]);
}
int ControlParticulas::NumeroParticulas()
{
    return NumParticulas;
}
int ControlParticulas::NumeroMuros()
{
    return NumMuros;
}

